<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<!--
Creates a depencency text file based on the configured IVY.xml for a particualar module
-->
<xsl:output method="text" encoding="ISO-8859-9" />


<xsl:template match="/">
	<xsl:text>Dependencies to use with this JAR&#x0A;</xsl:text>
	<xsl:text>---------------------------------&#x0A;</xsl:text>
    <xsl:apply-templates select="/ivy-module/dependencies/dependency[@conf='lib']"/>
</xsl:template>

<xsl:template match="dependency[@conf='lib']">
	<xsl:choose>
		<xsl:when test="@rev = 'latest.integration'">
			<xsl:value-of select="@name" /><xsl:text>.jar (latest integration)</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="@name" /><xsl:text>_</xsl:text><xsl:value-of select="@rev" /><xsl:text>.jar</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text>&#x0A;</xsl:text>
</xsl:template>

</xsl:stylesheet>
