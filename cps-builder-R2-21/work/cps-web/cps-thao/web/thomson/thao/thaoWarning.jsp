<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="warningBlockDisplay" value="newWarningBlock hide"/>
<c:if test="${not empty bookingComponent.errorMessage}">
  <c:set var="warningBlockDisplay" value="newWarningBlock show"/>
</c:if>
<div id="ValidationWarningBlock" class="<c:out value='${warningBlockDisplay}'/>">
  <span class="warningIcon">&nbsp;</span>
  <div class="newWarningBlockContent">
    <p><strong>Please check if all the details entered are correct</strong></p>
    <ul class="warningValidationMessages" id="paymentPageWarnings">
      <c:if test="${not empty bookingComponent.errorMessage}">
        <li><c:out value="${bookingComponent.errorMessage}"/></li>
      </c:if>
    </ul>
  </div>
 </div><!-- END Warning Block -->

