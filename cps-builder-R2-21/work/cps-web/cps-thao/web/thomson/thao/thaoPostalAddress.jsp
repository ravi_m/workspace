<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div class="paxContactDetails">
  <p class="intro">Please provide lead passenger details</p>

  <ul class="contactDetailsForm">
    <li>
      <p>House Name/No.</p>
      <input type="text" id="houseName" name="houseName"  maxlength="20" value="<c:out value="${bookingComponent.nonPaymentData['houseName']}"/>"  />
      <span>*</span>
    </li>
    <li>
      <p>Address</p>
      <input type="text" id="houseAddress1" name="addressLine1" maxlength="25" value="<c:out value="${bookingComponent.nonPaymentData['addressLine1']}"/>" />
      <span>*</span>
    </li>
    <li class="townCity">
      <p>Town/City</p>
      <input type="text" id="townOrCity" name="city" value="<c:out value="${bookingComponent.nonPaymentData['city']}"/>"  maxlength="25"/>
      <span>*</span>
    </li>
    <li>
      <p>County</p>
      <input type="text" id="countyName" name="county" value="<c:out value="${bookingComponent.nonPaymentData['county']}"/>" maxlength="16"/>
      <span>*</span>
    </li>
    <li class="postcode">
      <p>Postcode</p>
      <input type="text" id="postCode" name="postCode" value="<c:out value="${bookingComponent.nonPaymentData['postCode']}"/>" maxlength="8"/>
      <span>*</span>
    </li>
    <li class="country">
	  <input type="hidden" id="countryCode" name="countryCode" value="GB"/>
      <p><input type="hidden" id="countryName" name="countryName" value="United Kingdom"/>Country</p>
      <select name="country" id="countryName1" onchange="document.getElementById('countryName').value=this.options[this.selectedIndex].text;
	  document.getElementById('countryCode').value=this.options[this.selectedIndex].value;">
        <c:choose>
          <c:when test="${bookingComponent.nonPaymentData['countryCode'] != null && bookingComponent.nonPaymentData['countryCode'] != '-1'}">
            <c:set var="selectedCountryCode" value="${bookingComponent.nonPaymentData['countryCode']}" scope="page"/>
          </c:when>
          <c:otherwise>
            <c:set var="selectedCountryCode" value="GB" scope="page"/>
          </c:otherwise>
        </c:choose>
        <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
          <c:set var="countryCode" value="${CountriesList.key}" />
          <c:set var="countryName" value="${CountriesList.value}" />
          <option value='<c:out value="${countryCode}"/>'
            <c:if test='${countryCode==selectedCountryCode}'>selected='selected'</c:if>>
            <c:out value="${countryName}" />
          </option>
        </c:forEach>
      </select>
    </li>
  </ul>

  <ul class="contactDetailsForm">
    <li>
      <p>Contact telephone</p>
      <input type="text" id="contactTelephone" name="dayTimePhone" maxlength="15" value='<c:out value="${bookingComponent.nonPaymentData['dayTimePhone']}"/>' />
      <span>*</span>
    </li>
    <li>
      <p>Mobile telephone</p>
      <input type="text" id="mobileTelephone" name="mobilePhone" value='<c:out value="${bookingComponent.nonPaymentData['mobilePhone']}"/>' maxlength="15"/>
    </li>
    <li>
      <p>Email</p>
      <input type="text" id="EmailAddress" name="emailAddress" value="<c:out value="${bookingComponent.nonPaymentData['emailAddress']}"/>" maxlength="100"/>
      <span>*</span>
    </li>
    <li>
      <p>Confirm email</p>
      <input type="text" id="EmailAddress2" name="emailAddress1" value="<c:out value="${bookingComponent.nonPaymentData['emailAddress1']}"/>"  maxlength="100"/>
      <span>*</span>
    </li>
  </ul>
</div><!-- END paxContactDetails -->