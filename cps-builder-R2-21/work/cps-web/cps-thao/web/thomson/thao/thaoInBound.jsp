<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<h3>Returning</h3>
<c:forEach items="${bookingComponent.flightSummary.inboundFlight}" var="inboundSectors">
 <c:set var="stops" value="${stops+1}" scope="page"/>
  <ul class="flightOptions">
    <li>
      <div class="flightHeader">Depart&#58;</div>
      <div class="flightInfo">
        <p><c:out value="${inboundSectors.departureAirportName}"/>&nbsp;(<c:out value="${inboundSectors.departureAirportCode}"/>)</p>
        <span class="flightDate">
          <fmt:formatDate value="${inboundSectors.departureDateTime}" type="time" pattern="dd/MM/yy"/>
        </span>
        <span class="flightTime">
          <fmt:formatDate value="${inboundSectors.departureDateTime}" type="time" pattern="HH:mm"/>
        </span>
      </div>
    </li>
  </ul>
  <ul class="flightOptions">
    <li>
      <div class="flightHeader">Arrive&#58;</div>
      <div class="flightInfo">
        <p><c:out value="${inboundSectors.arrivalAirportName}"/>&nbsp;(<c:out value="${inboundSectors.arrivalAirportCode}"/>)</p>
        <span class="flightDate">
          <fmt:formatDate value="${inboundSectors.arrivalDateTime}" type="time" pattern="dd/MM/yy"/>
        </span>
        <span class="flightTime">
          <fmt:formatDate value="${inboundSectors.arrivalDateTime}" type="time" pattern="HH:mm"/>
        </span>
      </div>
    </li>
  </ul>
  <ul class="flightOptions">
    <li>
    <div class="flightHeader">Carrier&#58;</div>
    <div class="flightInfo">
      <p><c:out value="${inboundSectors.carrier}" escapeXml="false"/></p>
      <span class="flightCarrier"><c:out value="${inboundSectors.operatingAirlineCode}${inboundSectors.flightNumber}"/></span>
    </div>
    </li>
  </ul><!-- END Flight Arrival -->
</c:forEach>

<c:if test="${stops > 1}">
  <p>
    <a class="arrow-link-right" title="View flight itinerary" href="javascript:Popup('<c:out value="${clientUrl}"/>/accom/page/details/flightitinerarydetails.page',440,440,'scrollbar=yes');">  View flight itinerary </a>
  </p>
</c:if>
