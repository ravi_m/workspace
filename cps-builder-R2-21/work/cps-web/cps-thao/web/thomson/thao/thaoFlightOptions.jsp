<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="clientDomainUrl" value="${bookingComponent.clientDomainURL}"/>
<fmt:setLocale value="en_GB" />

<div id="flightOptions" class="optionSummaryDetails">
  <h2>Flight options</h2>
  <div class="panelContainer">
    <div id="flightandhotelsection">
      <jsp:include page="thaoOutBound.jsp"/>
      <jsp:include page="thaoInBound.jsp"/>
    </div><!-- END #flightandhotelsection -->
  </div><!-- END panelContainer -->
</div><!-- END flightOptions -->
