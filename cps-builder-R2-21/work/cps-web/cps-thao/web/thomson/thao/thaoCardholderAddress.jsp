<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div class="paxContactDetails">
  <p class="intro">Please provide card holder address</p>
  <div id="confirmLeadPassengerAddress">
  <p class="cardAddress"> To help ensure your card details remain secure, please confirm the address of the cardholder.
  <br/>Please note: the cardholders address <b>must</b> be the address that the card is registered to.
  <br/>If this is the same as the lead passenger address, you just need to check the box.</p>

<p class="sameAddress">
		<input type="checkbox"
			id="LeadPassengerAddressCopy"
			name="AddressCopy"
			onclick="javascript:copyLeadPassengerAddress()" />
                    <b>Use same address as lead passenger</b>
</p>
</div>
  <ul class="contactDetailsForm">
    <li>
      <p>House Name/No.</p>
      <input type="text" id="CDhouseName" name="payment_0_street_address1"  maxlength="20" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/>"  />
      <span>*</span>
    </li>
    <li>
      <p>Address</p>
      <input type="text" id="CDhouseAddress1" name="payment_0_street_address2" maxlength="25" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>" />
      <span>*</span>
    </li>
    <li class="townCity">
      <p>Town/City</p>
      <input type="text" id="CDtownOrCity" name="payment_0_street_address3" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>"  maxlength="25"/>
      <span>*</span>
    </li>
    <li>
      <p>County</p>
      <input type="text" id="CDcountyName" name="payment_0_street_address4" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address4']}"/>" maxlength="16"/>
      <span>*</span>
    </li>
    <li class="postcode">
      <p>Postcode</p>
      <input type="text" id="CDpostCode" name="payment_0_postCode" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/>" maxlength="8"/>
      <span>*</span>
    </li>
     <li class="country">
      <p>Country</p>
	  <select id="CDcountryName" name="payment_0_selectedCountryCode">
	  <c:choose>
        <c:when test="${bookingInfo.paymentDataMap['payment_0_selectedCountryCode'] != null && bookingInfo.paymentDataMap['payment_0_selectedCountryCode'] != '-1'}">
           <c:set var="selectedCountryCode" value="${bookingInfo.paymentDataMap['payment_0_selectedCountryCode']}" scope="page"/>
        </c:when>
        <c:otherwise>
           <c:set var="selectedCountryCode" value="GB" scope="page"/>
	    </c:otherwise>
	   </c:choose>

	   <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
         <c:set var="countryCode" value="${CountriesList.key}" />
         <c:set var="countryName" value="${CountriesList.value}" />
         <option value='<c:out value="${countryCode}"/>'
            <c:if test='${countryCode==selectedCountryCode}'>selected='selected'</c:if>>
            <c:out value="${countryName}" />

         </option>
       </c:forEach>

	  </select>
	   <input type="hidden" name="payment_0_selectedCountry" id="payment_0_selectedCountry"  value="" />
    </li>
  </ul>
  </div>
    <p class="cardHolderMandatoryText">All fields marked with * must be completed</p>

