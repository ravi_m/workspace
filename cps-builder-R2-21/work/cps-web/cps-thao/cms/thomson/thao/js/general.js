function updateFormElementFromCheckBoxMarketing(checkBoxObj, formElement)
{
	if ($(formElement))
     {
	   if (checkBoxObj.checked)
         {
	      $(formElement).value = false;
	     }
	   else
         {
		  $(formElement).value = true;
		 }
     }
 }

   //Auto Completion of surnames
function autoCompletionSurname(passengerCountBasedOnRooms, startAutoCompletion)
{
   if($('autoCheck['+startAutoCompletion+']').checked) {
      for(index=startAutoCompletion; index<=(passengerCountBasedOnRooms); index++)
      {
         if ($('surName_'+index))
         {
         $('surName_'+index).value = $('surName_1').value;
      }
   }
}
}

function showDataProtection(elemId)
{
   obj = $(elemId);
   if (currentStyle(obj, 'display')=="block")
   {
      obj.style.display = "none";
   }else{
      obj.style.display = "block";
   }
}

function PopupForFcao(popURL,popW,popH,attr)
{
	 if (!popH) { popH = 350 }
	   if (!popW) { popW = 600 }
	   var winLeft = (screen.width-popW)/2;
	   var winTop = (screen.height-popH-30)/2;
	   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
	   popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
	   popupWin.close()
	  if(popURL.indexOf("www.360travelguide.net")> -1)
	  {
	     popupWin=window.open(popURL,"popupWindowVideoTour",winProp);
	  }
	  else
	  {
	     popupWin=window.open(popURL,"popupWindow",winProp);
	   }
	     popupWin.window.focus()
}

function copyLeadPassengerAddress()
{
   if($('LeadPassengerAddressCopy').checked) {
         $('CDhouseName').value = $('houseName').value;
         $('CDhouseAddress1').value = $('houseAddress1').value;
         $('CDtownOrCity').value = $('townOrCity').value;
         $('CDcountyName').value = $('countyName').value;
         $('CDpostCode').value = $('postCode').value;
         $('CDcountryName').value = $('countryName1').value;

   }
}

function showHiddenInformation(elemId){
   div = $(elemId);
   if(div.style.display=='none')
   {
      div.style.display = "block";
   }
   else
   {
      div.style.display = "none";
   }
}