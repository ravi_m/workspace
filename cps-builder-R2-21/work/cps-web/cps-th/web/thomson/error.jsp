<%@ page import="com.tui.uk.config.ConfReader;"%> 
<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
<link href="/cms-cps/thomson/css/main.css" rel="stylesheet"
	type="text/css" />
<link href="/cms-cps/thomson/css/main_header.css" rel="stylesheet"
	type="text/css" />
<c:choose>
    <c:when test="${not empty sessionScope=='true'}">
    <title>Thomson | Error</title>
   </c:when>
   <c:otherwise>
   <title>Thomson | Session Timed Out</title>
   </c:otherwise>
</c:choose>
<body>
	<div id="wrapper">
		<jsp:include page="sprocket/header.jsp" />

	<div id="BodyWide">
	<div class="clear"></div>
	   <div class="bodyPadder">
	   <c:choose>
	    <%--Tech Difficulties Error Message --%>
	    <c:when test="${not empty sessionScope=='true'}">
	      <div class="span error-page">
			<div class="span error-page">
				<h1>We're really sorry, we're having some technical problems.</h1>
			</div>
				<%
			 String getHomePageURL =(String)ConfReader.getConfEntry("thomson.homepage.url","");
				  pageContext.setAttribute("getHomePageURL",getHomePageURL);	
			%>
			<div class="span clearfix error-page">
				<div class="fl message">
				 <c:choose>
					 <c:when test="${not empty bookingComponent.clientURLLinks.homePageURL}">
					 <c:set var='homePageURL' value='${bookingComponent.clientURLLinks.homePageURL}' />
				      </c:when>
				     <c:otherwise>
				  	  <c:set var='homePageURL' value='${getHomePageURL}'/>
				     </c:otherwise>
				</c:choose>
				   <p>You can go back to viewing  <a class="history-reload" href="${homePageURL}">holidays matching your search</a> </p>
					<p>or give us a call on 0844 871 0878</p>
					<a class="logo" href="${homePageURL}"><div><img src="/cms-cps/thomson/images/logo.png"></div></a>
				</div>
				<img height="276" width="460" alt="Technical Difficulties" src="/cms-cps/thomson/images/error-tech-diff.png" class="fr error-image">
			</div>
	   </c:when>
	   <%--End Tech Difficulties Error Message --%>

	   <%--Session Time out Error Message --%>
	   <c:otherwise>
			<div class="span error-page">
			<h1>We are really sorry, your page has timed out</h1>
			</div>
			<div class="span clearfix error-page">
				<div class="fl message">
				   <p>You can go back to viewing  <a class="history-reload" href="${homePageURL}">holidays matching your search</a> </p>
					<p>or give us a call on 0844 871 0878</p>
					<a class="logo" href="${homePageURL}"><img height="80" width="240" src="/cms-cps/thomson/images/logo-pad.png"></a>
				</div>
				<img height="276" width="460" alt="Technical Difficulties" src="/cms-cps/thomson/images/error-tech-diff.png" class="fr error-image">
			</div>
	   </c:otherwise>
	   <%--End of Session Time out Error Message --%>
	   </c:choose>
	  </div> <%--end  bodyPadder--%>
	<div class="clear"></div>
	</div>

	</div>
	<jsp:include page="sprocket/footer.jsp" />
</body>
</html>