<%@ page import="com.tui.uk.config.ConfReader"%>
<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" id="firstchoice" class="ie6 "> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" id="firstchoice" class="ie7 "> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" id="firstchoice" class="ie8 "> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" id="firstchoice" class="ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="" id="firstchoice"> <!--<![endif]-->
<head>
	<title></title>
	<!--[if IE 7 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /><![endif]-->
	<!--[if IE 8 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /><![endif]-->
	<!--[if IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" /><![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><!--<![endif]-->
	<meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<meta name="viewport" content="width=device-width,  itial-scale=1">


			<%
			 String manageMyBookingURL =(String)ConfReader.getConfEntry("manageth.homepage.url","");
				  pageContext.setAttribute("manageMyBookingURL",manageMyBookingURL);
				%>
	<link rel="icon" type="image/png" href="/cms-cps/thomson/images/favicon.png" />

		<!--[if lte IE 9]>
			<link rel="shortcut icon" href="/cms-cps/thomson/images/favicon.ico" type="image/x-icon" />
			<link rel="icon" href="/cms-cps/thomson/images/favicon.ico" />
			<![endif]-->

			<!-- HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->

			<!-- javascript -->

			<script>
			var ensLinkTrack = function(){};
			</script>
		</head>
		<body>
			<div id="wrapper">
				<div id="header">
					<div class="message-window" style="display:none" id="tellUsWhatYouThink">
						<a href="#" class="close ensLinkTrack"> <span class="text">Close</span></a>
						<div class="message-content small-pop beta-pop">
							<h3>Before you go...</h3>
							<p>We'd love to know what you made of the new site:</p>
							<ul>
								<li><a target="_blank" href="${bookingComponent.clientURLLinks.homePageURL}" class="ensLinkTrack">Tell us what you think</a></li>
								<li><a target="_blank" href="http://www.thomson.co.uk/?customer=switch" class="ensLinkTrack">Switch to our main site</a></li>
							</ul>
						</div>
					</div>
					<div id="utils">
						<ul>
							<!-- <li><a target="_blank" href="http://www.thomson.co.uk/destinations/faqCategories">Ask a Question</a></li> -->
						    <li><img width="58" height="77" src="/cms-cps/thomson/images/WOT_Endorsement_V2_3C.png" id="header-wtui" alt="Welcome to the world of TUI"></li>
						</ul>
					</div>

					<div id="logo">
						<a target="_blank" class="ensLinkTrack" href="${bookingComponent.clientURLLinks.homePageURL}">Thomson</a>

					</div>
					
				</div>
			</div>
		</body>
	</html>