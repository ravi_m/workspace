<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%
String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.tuith" , "");
pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);
String firstchoiceHomepage = ConfReader.getConfEntry("firstchoice.homepage.url" , "");
pageContext.setAttribute("firstchoiceHomepage", firstchoiceHomepage, PageContext.REQUEST_SCOPE);
String tuiBrochure = ConfReader.getConfEntry("tui.brochure.url" , "");
pageContext.setAttribute("tuiBrochure", tuiBrochure, PageContext.REQUEST_SCOPE);
%>

<div id="footer">
	 <div class="wrapper ski">
		  <div aria-label="group" class="b falcon" id="group">
		    <div class="content-width">
		      <div class="container">
		        <!--<span id="world-of-tui" aria-label="world of tui"><img src="//digital.thomson.co.uk/wot.png" alt="World of TUI" /></span>-->
		        <p>&copy; 2021 Crystal Ireland. Crystal Ski Holidays is a trading name of TUI Holidays Ireland Limited, a member of TUI Group.</p>
				<p>Registered address: One Spencer Dock, North Wall Quay, Dublin 1, Ireland. Registered Company No. 116977.</p>
		        <span class="logos">
		          <a href="//www.aviationreg.ie/" id="c-ar" rel="noopener" target="_blank" title="Commission for Aviation Regulation" data-di-id="#c-ar">RIO</a> 
		        </span>
		    	<span class="commission">
					<c:choose>
						<c:when test="${bookingComponent.nonPaymentData['oscarEnabled'] eq 'true' }">
							Licenced by the Commission for Aviation Regulation T.O. 272
						</c:when>
						<c:otherwise>
							Licenced by the Commission for Aviation Regulation T.O. 272
						 </c:otherwise>
					</c:choose> 
				</span>
		      </div>
		    </div>
		  </div>
		  <div aria-label="terms" class="terms">
		    <div class="content-width contentList">
		      <span class="title">More from Crystal</span>
		      <p>
		        <a rel="noopener" href="https://www.crystalski.ie/contact-us/">Contact us</a>
		        <a rel="noopener" href="https://www.crystalski.ie/about-crystal/">About us</a>
		        <a rel="noopener" href="https://www.crystalski.ie/our-policies/terms-and-conditions/">Terms and conditions</a>
		        <a rel="noopener" href="https://www.crystalski.ie/our-policies/privacy-policy/">Privacy & cookies</a>
		        <a rel="noopener" href="https://www.crystalski.ie/help/">Get help</a>
		        <a rel="noopener" href="https://www.crystalski.ie/your-account/">Manage booking</a>      
		        <a rel="noopener" href="https://www.crystalski.ie/the-crystal-snow-promise/">The Crystal snow promise</a>
		        <a rel="noopener" href="https://www.crystalski.ie/book-with-confidence/ ">Book with confidence</a>
		      </p>
		    </div>
		  </div>
		  <div class="knowBefore" area-label="Travel Aware">
		    <div class="content-width">
		      <div>
		        <span class="title">Travel aware - staying safe and healthy abroad</span>
		      </div>
		      <p>The Department of Foreign Affairs has up-to-date advice for Irish citizens on staying safe and healthy abroad.</p>
		      <p>
		        For more on security, local laws, health, passport and visa information see
		        <a rel="noopener" href="//www.dfa.ie/travel/travel-advice" target="_blank" class="stay-safe" data-di-id="di-id-fc3c7ca9-f3dbb6e">www.dfa.ie/travel/travel-advice</a>
		        and follow <a rel="noopener" href="https://twitter.com/dfatravelwise" target="_blank" class="stay-safe" data-di-id="di-id-dd1e2210-877ae492">@dfatravelwise</a>
		      </p>
		      <p>Northern Ireland citizens should refer to <a rel="noopener" href="//www.gov.uk/foreign-travel-advice" target="_blank" class="stay-safe" data-di-id="di-id-fc3c7ca9-339e7ef9">www.gov.uk/foreign-travel-advice</a></p>
		      <p>More information is available by checking <a href="https://www.tuiholidays.ie/f/info/travel-aware" target="_blank" class="stay-safe" data-di-id="di-id-d63e399e-21911abc">www.tuiholidays.ie/f/info/travel-aware</a></p>
		      <p> Keep informed of current travel health news by visiting  <a rel="noopener" href="//www.hse.ie/eng/health/az" target="_blank" class="stay-safe" data-di-id="di-id-1c34a939-975c6b2d">www.hse.ie/eng/health/az</a></p>
		      <p>The advice can change so check regularly for updates.</p>
		    </div>
		  </div>
		  <div id="disclaimer">
		   
		     <c:choose>
				<c:when test="${bookingComponent.nonPaymentData['oscarEnabled'] eq 'true' }">
				 <div class="content-width disclaim">
				<p>Adehy Limited is licenced by the Commission for Aviation Regulation under Licence T.O. 272 we have arranged an approved bond, therefore your money is secure with us.</p>
				<p>Adehy Limited is the part of the TUI Group of Companies.</p>
				</div>
				</c:when>
				<c:otherwise>
				<div class="content-width disclaim two-columns">
					<p>TUI Holidays Ireland Limited is licenced by the Commission for Aviation Regulation under Licence T.O. 272 we have arranged an approved bond, therefore your money is secure with us.</p> 
      				<p>TUI Holidays Ireland Limited is the part of the TUI Group of Companies.</p>
                 </div>
				 </c:otherwise>
			</c:choose>
		    
		  </div>
		</div>
</div>


