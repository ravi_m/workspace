<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="cardChargeDetails" value="${bookingInfo.cardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<c:set var="cardCharge" value="${cardChargeArray[0]}" />
<c:set var="maxCardCharge" value="${cardChargeArray[2]}" />
<script type="text/javascript">
    var currencySymbol="�";
    var newHoliday = "<c:out value='${bookingInfo.newHoliday}'/>";
    var paymenttypeoptions = new Array();
    var paymenttypeoptionsHopla = new Array();
    var payment_CP = new Array();
    var payment_CNP = new Array();
    var payment_CPNA = new Array();
     var payment_PPG = new Array();

    var requiredFields_cp='';
    var requiredFields_cnp=''
    var requiredFields_cpna='';
    var requiredFields_ppg='';
     var dispCardText = checkForDispCardText('',3);

    var PaymentInfo = new Object();
    PaymentInfo.payableAmount  = null;
    PaymentInfo.partialPaymentAmount = null;
    <c:if test="${(bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_PEG') && bookingComponent.accommodationSummary.prepay != 'true'}">
    PaymentInfo.payableAmount  = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
    PaymentInfo.partialPaymentAmount = 'true';
    </c:if>
    PaymentInfo.totalAmount = '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>';
    PaymentInfo.calculatedPayableAmount = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
    PaymentInfo.calculatedTotalAmount = '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>';
    PaymentInfo.maxCardChargeAmount = null;
    PaymentInfo.minCardChargeAmount = null;
    //Let us give default values as null
    PaymentInfo.calculatedDiscount = null;
    PaymentInfo.chargePercent = null;
    PaymentInfo.totalCardCharge = null;
    var depositAmountsMap = new Map();
     <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}">
       depositAmountsMap.put('<c:out value="${depositComponent.depositType}"/>', '<c:out value="${depositComponent.depositAmount.amount}"/>');
     </c:forEach>
     depositAmountsMap.put('fullCost', '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>');
  var cardChargeMap = new Map();
  cardChargeMap.put('PleaseSelect', 0);
     <c:forEach var="cardCharges" items="${bookingInfo.cardChargeMap}">
       cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');
     </c:forEach>
     var bookingConstants = {FULL_COST:"fullCost", TOTAL_CLASS:"total", PAYABLEAMOUNT_CLASS:"", PAY_CLASS:"pay", PAY_DESCRIPTION:"Pay"};

	 var threeDCards = new Map();
         <c:forEach var="threeDCards" items="${bookingInfo.payDescription}">
             threeDCards.put("<c:out value="${threeDCards.key}"/>","<c:out value="${threeDCards.value}"/>");
         </c:forEach>
</script>
<c:choose>
   <c:when test="${hoplaBonded == null}">
      <c:choose>
         <c:when test="${accomInventory == 'HOPLA_THM' || accomInventory == 'HOPLA_PEG'}">
 <input type='hidden'  name='payment_0_paymentmethod' id='payment_0_paymentmethod' value='Postpayment'/>
 <input  type='hidden' id='payment_totalTrans' name='payment_totalTrans' value='0'/>
</c:when>
<c:otherwise>
 <input type='hidden'  name='payment_0_paymentmethod' id='payment_0_paymentmethod' value='Dcard'/>
 <input  type='hidden' id='payment_totalTrans' name='payment_totalTrans' value='1'/>
</c:otherwise>
</c:choose>
   </c:when>
   <c:otherwise>
      <c:choose>
         <c:when test="${hoplaBonded}">
            <input type='hidden'  name='payment_0_paymentmethod' id='payment_0_paymentmethod' value='Dcard'/>
            <input  type='hidden' id='payment_totalTrans' name='payment_totalTrans' value='1'/>
         </c:when>
         <c:otherwise>
            <input type='hidden'  name='payment_0_paymentmethod' id='payment_0_paymentmethod' value='Postpayment'/>
            <input  type='hidden' id='payment_totalTrans' name='payment_totalTrans' value='0'/>
         </c:otherwise>
      </c:choose>
   </c:otherwise>
</c:choose>
<div id="paymentFields_required">
<input type='hidden'  id='panelType' value='CNP'/>
<c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
      <c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'     id='${paymentType.paymentCode}_enabled' name='payment_code_${paymentType.paymentCode} '/>"    escapeXml="false"/>
      <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>
      <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>
      <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>
</c:forEach>
<c:forEach var="paymentType" items="${bookingComponent.paymentType['PPG']}">
      <c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled'  name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>
      <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>
      <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>
      <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>
</c:forEach>
</div>
<div id="card_details">
<div class="borderpaydt">
      <h2>Card Details</h2>
      <div class="headtext_carddetails"></div>
      <div class="highlight paddingbtm5" id="paymenttypeid" >
         <div id="paymentSelections" class="payment_type_blk">
            <div id='paymentFor0'>
                 <div id='transactionId_0'>
                 <div id='paymentPanelTransaction' class='paymentPanelTransaction'>
                 <div id='selectBox0' class='selectBox'>
                 <span id='cardTypeCaption' class='cardTypeCaption' >Card Type</span>
                 <%--***************************Card Type Select box*************************************--%>
                 <select id='payment_type_0' class='payment_type' name='payment_0_type'
                 onchange='handleCardSelection(0); changePayButton();'>
                 <option id="pleaseSelect" value='PleaseSelect' selected="selected">Card Type</option>
                 <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
                 <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>
                 </c:forEach>
                 <c:forEach var="paymentType" items="${bookingComponent.paymentType['PPG']}">
                 <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/> '><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>
                 </c:forEach>
                 </select>
          <span class="star">*</span>
         </div>
                  <%--***************************End Card Type Select box*************************************--%>

                 <div id='innerContainer0'>
                 <br clear='all'/>
                 <div class='trans_amt hideIt'>
         </div>

                 <input class='medium hideIt' type='text'  id='transamt0' onblur='transamtHandler(0,currOption,this)' alt='' value=''/>
         </div>
                 <div  class='container_val' id='cardCharges0'></div>
                 <div  class='container_val topMargin1 hideIt'>
                 <div class='trans_amt'></div>
                 <div class='tottransamt1  topMargin1' id='tottransamt0'></div>
                 </div><br clear='all'/>
                 <input type='hidden' id='total_amount_0' name='payment_0_amountPaid' value='0.0'/>
                 <div id='format0' class='blockcontain'>
                 <input type='hidden' value='' name='payment_0_paymenttypecode' id='payment_0_paymenttypecode'/>
                 <div id='card_payment'>
                 <div class='cnpCardLeftSection' >
                 <dl class='hangleft'>
                 <dt>Card Number</dt>
                 <dd>
                 <input type='text' class='large uniform' alt='Your card number is required to complete your booking. Please enter your card number. |Y|CARDNO|PaymentCardType' maxlength='20' id='payment_0_cardNumberId' name='payment_0_cardNumber' value='' onblur='validatePaymentFields(this)' autocomplete="off"/>
                 <span class='mandatoryitem star'>*</span>
                 <input type='hidden' alt='payment_0_cardNumberId|Y|LUHN|payment_0_cardNumberId'/>
                 </dd>
                 <dt>Name on Card</dt>
                 <dd>
                 <input type='text' class='medium uniform' alt='The name on your card is required to complete your booking. Please enter the name on your card. |Y|NAME' maxlength='25' id='payment_0_nameOnCardId' name='payment_0_nameOnCard' value='' onblur='validatePaymentFields(this)' autocomplete="off"/>
                 <span class='mandatoryitem star'>*</span>
                 </dd>
                 </dl>
                 <div style="background-color:#EEEEEE">
                     <div>
                      <span class="cardcharge_left">
                         <a onclick="openPopupCreditCardPrivacy('https://www.thomson.co.uk/editorial/legal/credit-card-payments.html?ico=AOPayment_CardCharges&amp;popup=true&amp;section=holidays&amp;topic=secure#4');return false;" href="javascript:void(0)">
                            There are no additional charges when paying by Maestro, MasterCard Debit or Visa/ Delta debit cards.
                            A fee of <fmt:formatNumber value="${cardCharge}" type="number" var="confCardCharge" maxFractionDigits="1" minFractionDigits="0"/><c:out value="${confCardCharge}"/>% applies to credit card payments, which is capped at �<c:out value="${maxCardCharge}"/> per transaction when using American Express, MasterCard Credit or Visa Credit cards
                      </a>
                     </span>
                    </div>
	            </div>
                 </div>
<div class='cnpCardRightSection' >
                 <dl id='hangrightList0' class='hangright'>
                 <dt>Expiry Date</dt>
                 <dd class='smallMargin'>
                 <select class='date_field uniform' id='payment_0_expiryMonthId'  name='payment_0_expiryMonth'>
                     <option value=''>MM</option>
                     <option value='01'>01</option>
                     <option value='02'>02</option>
                     <option value='03'>03</option>
                     <option value='04'>04</option>
                     <option value='05'>05</option>
                     <option value='06'>06</option>
                     <option value='07'>07</option>
                     <option value='08'>08</option>
                     <option value='09'>09</option>
                     <option value='10'>10</option>
                     <option value='11'>11</option>
                     <option value='12'>12</option>
                 </select>
                 <select class='date_field_expiry_year uniform' id='ExpiryYear' name='payment_0_expiryYear'>
                 <option value=''>YY</option>
                 <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
                     <option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>
                  </c:forEach>
                  </select>
                  <span class='mandatoryitem star lessMargin'>*</span>
                  </dd>
                  <dt class='extraheight'>Security code<br/>
                  <c:if test="${dispCardText == null}">
                     <c:set var="dispCardText" value="last 3 digits in the signature strip on the reverse of your card"/>
                  </c:if>
                  <span id='securityCodeHelp0' class='smalltext'>(<c:out value="${dispCardText}"/>)</span>
                  </dt>
                  <dd class='extraheight'>
                  <input type='text' class='small' alt='The 3 digit security number is required to complete your booking. This can be found to the right on the reverse of your card. Please enter your 3 digit security number.|Y|SECURITY|PaymentCardType' maxlength='4' id='payment_0_securityCodeId'  name='payment_0_securityCode' value='' onblur='validatePaymentFields(this)' autocomplete="off"/>
                  <span class='star'>*</span>
                  </dd>
                  </dl>

                  <div id="threeDsecureimages">
                 <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
                    <c:if test="${threeDLogos == 'mastercardgroup'}">
                         <c:set var="masterCardLogoText" value="true"/>
                         <a href="javascript:void(0);" id="masterCardDetails" class="logosticky stickyOwner"><img title="MasterCard" src="/cms-cps/thomson/byo/images/th-mastercard-secure.gif"/></a>
						 <%@ include file="/common/mastercardLearnMoreSticky.jspf"%>
                    </c:if>
                 </c:forEach>
                 <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
                    <c:if test="${threeDLogos == 'visagroup'}">
                       <c:set var="visaLogoText" value="true"/>
                       <span class="visaLogo">
	                   <a href="javascript:void(0);" id="visaDetails" class="logosticky stickyOwner"><img title="Visa" src="/cms-cps/thomson/byo/images/th-visa.gif"/></a>
					   <%@ include file="/common/visaLearnMoreSticky.jspf"%>
                       </span>
	                   </c:if>
                  </c:forEach>
                  </div>

				  <div id="learnMoreLogos">
                  <c:if test="${masterCardLogoText}">
				  <a href="javascript:void(0);" id="masterCardDetails" class="sticky stickyOwner">Learn more</a>
	              <%@ include file="/common/mastercardLearnMoreSticky.jspf"%>
	               </c:if>

	              <c:if test="${visaLogoText}">
	              <span class="visaLogo">
	              <a href="javascript:void(0);" id="visaDetails" class="sticky stickyOwner">Learn more</a>
			      <%@ include file="/common/visaLearnMoreSticky.jspf"%>
			      </span>
                  </c:if>
	               </div>

                  </div>


                  <div class='debit_cards' style='display:none;'  id='debitcards0'>
                  <p id='debitCardCaption'>For Maestro/Solo Card users - if your card has an issue number, please enter this now.
                 </p>
                  <dl class='hangleft debitCardLeftpart'>
                  <dt>
                  <label>Issue Number</label></dt>
                  <dd class='issueStyle'>
                  <input type='text' class='small issueNoInput uniform' alt='Please enter an issue number for your card.|N|NUMERIC' maxlength='2' id='IssueNumberInput' name='payment_0_issueNumber' value='' autocomplete="off"/>

                  </dd>
                  </dl>
                 </div><%--end of right panel div--%>
                 </div><%--end of card payment div--%>

         </div><%--end of format div--%>

         <div style='display:none;' id='chkboxcontainer0'  class='checkbx_cont'>
                  <input name='payment_0_cardCharge' type='hidden' id='cardChargeAmount_0'/><input name='payment_0_cardLevyPercentage' type='hidden' id='cardLevyCharge_0' value='0.0'/>
                  <br clear='all'/>
                 </div>
             </div>
          </div>
  </div><br clear='all'/><br clear='all'/>
   </div>

</div>
<%@ include file="cardHolderAddress.jspf" %>
  </div><%--end of payment details section--%>



  </div><%--end of card details div--%>
<script type="text/javascript">
   window.onload=
   function()
   {
      if(document.getElementById("paymentTrans")!=undefined || document.getElementById("paymentSelections")==undefined)
      {
         return;
      }
      //Make an ajax call to check for booking button
      checkBookingButton("confirmButton");
      setToDefaultSelection();
      if (newHoliday == "true")
      {
      initializeDepositSelection();
      }
      remainingHolidayCost();
      document.getElementById("securityCodeHelp0").innerHTML = "("+checkForDispCardText('',3)+")";
   }
   jQuery(document).ready(function()
	{
	   //toggleOverlay();
	   toggleBYOOverlay();
	});
</script>
<%-- *******Essential fields ********************  --%>
<input type="hidden" name="payment_0_chargeAmount" id="payment_0_chargeIdAmount"  value="0" />
<input type="hidden" name="payment_0_transamt"  id="payment_0_transamt"  value="0" />
<input type="hidden" name="total_transamt"  id="total_transamt"  value='NA' />
<%-- *******End Essential fields ********************  --%>