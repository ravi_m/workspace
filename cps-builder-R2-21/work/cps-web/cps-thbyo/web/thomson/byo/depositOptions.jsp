<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="lowDepositBoolean" value="false"/>
<div id="payment_amount">
   <h2>Payment Amount</h2>
   <p>
      Your total holiday cost is <strong>&pound;<span id="totalholidaycost" class="total"><fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/></span >*</strong>
      <br />
      A deposit for this holiday is due today, but you can choose to pay the full balance if you wish. Please select your preferred payment amount:<br/>
   </p>
   <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}" varStatus="status">
         <c:if test="${depositComponent.depositType == 'lowDeposit' || depositComponent.depositType == 'LOWDEPOSIT'}">
            <c:set var="lowDepositBoolean" value="true"/>
            <div>
            <div class="rowDiv highlightDepositAll">
                  <div class="colDiv depositRadio">
                     <input type="radio" class="deposit" id="radioLowDeposit" value="<c:out value='${depositComponent.depositType}'/>" name="depositType" onclick="handleDepositSelection('<c:out value='${depositComponent.depositType}'/>');" <c:if test="${status.first}">checked="checked"</c:if>/>
                  </div>
                  <div class="colDiv padder">
                     <span class="depositText">Low Deposit:</span>
                     <span class="hangSpanRight">
                        <strong>
                           &pound;<span id="lowDeposit" class="depositAmount"><fmt:formatNumber value="${depositComponent.depositAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/></span>*</strong> due
                        <strong>today</strong>
                     </span>
                  </div>
                  <div class="rowDiv"><span class="ruler">&nbsp;</span></div>
                  <div class="rowDiv">
                     <div class="rowDiv padder">
                        <div id="outstandingText">The outstanding balance of your deposit is: </div>
                        <div class="colDiv hangSpanRight3">
                           <strong>&pound;<span id="lowDepositOutstandingbalance"><fmt:formatNumber value="${depositComponent.outstandingBalance.amount}"	type="number" maxFractionDigits="2" minFractionDigits="2"/></span>*</strong> due on
                           <strong>
                              <span id="balanceDueDate">
                                 <fmt:formatDate value="${depositComponent.depositDueDate}" pattern="dd MMMMMMMMM yyyy"/>
                              </span>
                           </strong>
                        </div>
                     </div>
               </div>
            </div>
         </c:if>
         <c:if test="${depositComponent.depositType == 'deposit' && lowDepositBoolean == 'true'}">
	     <div class="rowDiv highlightDeposit">
	          <div class="rowDiv padder">
                       <div id="outstandingText">The remaining balance of the holiday is: </div>
                         <div class="colDiv hangSpanRight3">
                           <strong>&pound;<span id="outstandingbalance"><fmt:formatNumber value="${depositComponent.outstandingBalance.amount}"	type="number" maxFractionDigits="2" minFractionDigits="2"/></span>*</strong> due on
                           <strong>
                           <span id="balanceDueDate">
                           <fmt:formatDate value="${depositComponent.depositDueDate}" pattern="dd MMMMMMMMM yyyy"/>
                           </span>
                           </strong>
                         </div>
                      </div>
             </div>
	     </div>
         </c:if>

         <c:if test="${depositComponent.depositType == 'deposit' || depositComponent.depositType == 'DEPOSIT'}">
            <div>
               <div class="rowDiv highlightDepositAll">
                  <div class="colDiv depositRadio">
                        <input type="radio" class="deposit" id="radioDeposit" value="<c:out value='${depositComponent.depositType}'/>" name="depositType" onclick="handleDepositSelection('<c:out value='${depositComponent.depositType}'/>');"
                        <c:if test="${status.first}">checked="checked"</c:if>/>
                  </div>
                  <div class="colDiv padder">
                     <span class="depositText">Full Deposit:</span>
                     <span class="hangSpanRight">
                        <strong>
                           &pound;<span id="deposit" class="depositAmount"><fmt:formatNumber value="${depositComponent.depositAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/></span>*</strong> due
                        <strong>today</strong>
                     </span>
                  </div>
                  <div class="rowDiv"><span class="ruler">&nbsp;</span></div>
                  <div class="rowDiv">
                     <div class="rowDiv padder">
                        <div id="outstandingText">The remaining balance of the holiday is: </div>
                        <div class="colDiv hangSpanRight3">
                           <strong>&pound;<span id="outstandingbalance1"><fmt:formatNumber value="${depositComponent.outstandingBalance.amount}"	type="number" maxFractionDigits="2" minFractionDigits="2"/></span>*</strong> due on
                           <strong>
                              <span id="balanceDueDate">
                                 <fmt:formatDate value="${depositComponent.depositDueDate}" pattern="dd MMMMMMMMM yyyy"/>
                              </span>
                           </strong>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </c:if>
   </c:forEach>
   <div>
      <div class="rowDiv highlightDepositAll" style="_width:703px;">
         <div class="colDiv depositRadio">
            <input  type="radio" class="deposit" id="radioFullAmount" value="fullCost" name="depositType" onclick="handleDepositSelection('fullCost');"/>
         </div>
         <div class="colDiv padder">
            <span class="depositText">Full Balance:</span>
            <span class="hangSpanRight">
               <strong>
                  &pound;<span id="fullCost" class="depositAmount"><fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/></span>*</strong> due
               <strong>today</strong>
            </span>
         </div>
      </div>
   </div>
   <p>* For each payment by credit card a credit card charge will apply. There is no charge for debit card payments.</p>
</div>

