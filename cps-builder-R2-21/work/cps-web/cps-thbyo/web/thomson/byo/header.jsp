<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="url" value='${bookingComponent.clientDomainURL}'/>
<div id="toyBFhdrDiv">
<a href="http://www.thomson.co.uk"><img class="hdrLogo" src="/cms-cps/thomson/byo/images/miniHdrLogo.gif" alt="Thomson Holidays" border="0"/></a>
	<ul>
		<li class="hdrPHolidays"><a href="http://www.thomson.co.uk/package-holidays.html" title="Package holidays">Package holidays</a></li>
		<li class="hdrDiv">&nbsp;</li>
		<li><a href="http://flights.thomson.co.uk/en/index.html" title="Flights">Flights</a></li>
		<li class="hdrDiv">&nbsp;</li>
		<li><a href="http://www.thomson.co.uk/hotels/hotels.html" title="Hotels">Hotels</a></li>
		<li class="hdrDiv">&nbsp;</li>
		<li><a href="http://www.thomson.co.uk/city-breaks.html" title="City breaks">City breaks</a></li>
		<li class="hdrDiv">&nbsp;</li>
		<li><a href="http://www.thomson.co.uk/cruise.html" title="Cruises">Cruises</a></li>
		<li class="hdrDiv">&nbsp;</li>
		<li><a href="http://www.thomson.co.uk/villas.html" title="Villas">Villas</a></li>
		<li class="hdrDiv">&nbsp;</li>
		<li><a href="http://www.thomson.co.uk/deals.html" title="Deals">Deals</a></li>
		<li class="hdrDiv">&nbsp;</li>
		<li><a href="http://www.thomson.co.uk/holiday-extras.html" title="Extras">Extras</a></li>
		<li class="hdrDiv">&nbsp;</li>
		<li class="hdrDestinations"><a href="http://www.thomson.co.uk/holiday-destinations.html">Destinations</a></li>
	</ul>
</div>
<script type="text/javascript">
  var pqry = '<c:out value="${bookingComponent.intelliTrackSnippet.intelliTrackerString}"/>';
  <c:choose>
    <c:when test="${fn:contains(bookingComponent.errorMessage, 'Sorry! Your card was declined')}">
       pqry = pqry + "%26CpsServer%3D<c:out value='${param.tomcat}'/>%26PayDesign%3DA%26declined%3Dtrue";
    </c:when>
    <c:otherwise>
       pqry = pqry + "%26CpsServer%3D<c:out value='${param.tomcat}'/>%26PayDesign%3DA%26declined%3Dfalse";
    </c:otherwise>
  </c:choose>
</script>