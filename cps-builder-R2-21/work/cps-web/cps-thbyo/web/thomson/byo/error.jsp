<%@include file="/common/commonTagLibs.jspf"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Build Your Own Holidays  - Error!!</title>

    <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byo/css/headfoot.css" />
    <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byo/css/error.css" />


    <script type="text/javascript" src="/cms-cps/thomson/byo/js/popups.js"></script>
    <script type="text/javascript" src="/cms-cps/thomson/byo/js/footer.js"></script>

  </head>
  <body>
    <div id="header">
      <jsp:include page="header.jsp" />
    </div>
    <div id="bodysection">
      <div id="content">
        <h1>We're sorry...</h1>
        <div class="warning">
          <h2>We are currently unable to process your request.</h2>
          <p>This may be due to technical issues. Please try again later, or alternatively
             you can call us on <B>0871 231 4691</B>. We're open Monday-Friday 09.00-9.00pm,
             Saturday 09.00-8.00pm and Sunday 10.00-8.00pm.</p>
          <p>Please note that our website is unavailable for bookings between 1.00am and
             6.00am while our holiday database is being updated.</p>
        </div>
        <div id="middlecontainer">
          <div id="picturebar">
            <h3 id="family"><a href="http://www.thomson.co.uk/families/family-club.html">Family Holidays</a></h3>
            <h3 id="luxury"><a href="http://www.thomson.co.uk/luxury-holidays.html">Luxury Holidays</a></h3>
            <h3 id="couples"><a href="http://www.thomson.co.uk/package-holidays.html">Couples Holidays</a></h3>
          </div>
          <div id="extraoptions">
            <dl id="holidays">
              <dt><a href="http://www.thomson.co.uk/">Holidays</a></dt>
              <dd><a href="http://www.thomson.co.uk/">Holiday Search</a></dd>
              <dd><a href="http://www.thomson.co.uk/editorial/deals/deals.html">Late Deals</a></dd>
              <dd><a href="http://www.thomson.co.uk/editorial/features/brochures.html">Brochures</a></dd>
              <dd><a href="http://www.thomsonbeach.co.uk/th/beach/reviewHome.do">Customer reviews</a></dd>
            </dl>
            <dl id="flights">
              <dt><a href="http://www.thomsonfly.com/en/index.html">Flights</a></dt>
             <dd><a href="http://www.thomsonfly.com/en/index.html">Thomsonfly</a></dd>
            </dl>
            <dl id="hotels">
              <dt><a href="http://www.thomson.co.uk/thomson/page/ao/home/home.page">Hotels</a></dt>
              <dd><a href="http://www.thomson.co.uk/thomson/page/ao/home/home.page">Hotel Search</a></dd>
            </dl>
            <dl id="citybreaks">
              <dt><a href="http://www.thomsoncities.co.uk/">City breaks</a></dt>
              <dd><a href="http://www.thomsoncities.co.uk/">Search cities</a></dd>
              <dd> <a href="http://www.thomsoncities.co.uk/destinations.asp">Browse cities</a></dd>
              <dd><a href="http://www.thomsoncities.co.uk/SplOffers.asp">City offers</a></dd>
            </dl>
            <dl id="cruise">
              <dt><a href="http://www.thomsonbeach.co.uk/th/cruise/getHomePageCruise.do">Cruise</a></dt>
              <dd><a href="http://www.thomsonbeach.co.uk/th/cruise/getHomePageCruise.do">Booking a cruise</a></dd>
              <dd><a href="http://www.thomsonbeach.co.uk/th/cruise/viewSailingDatesCalendar.do">Sailings</a></dd>
              <dd><a href="http://www.thomsonbeach.co.uk/th/cruise/viewItineraryList.do">Itineraries</a></dd>
              <dd><a href="http://www.thomsonbeach.co.uk/th/cruise/showCruiseSpecialOffers.do">Offers</a></dd>
            </dl>
            <dl id="villas">
              <dt><a href="http://www.thomson.co.uk/villas/villa-holidays.html">Villas</a></dt>
              <dd><a href="http://www.thomson.co.uk/villas/villa-holidays.html">Villas Search</a> </dd>
              <dd><a href="http://www.thomson.co.uk/editorial/deals/villa-special-offers.html">Villa Deals</a></dd>
            </dl>
            <dl id="deals">
              <dt><a href="http://www.thomson.co.uk/editorial/deals/deals.html">Deals</a></dt>
              <dd><a href="http://www.thomson.co.uk/offers/winter-offers.html">Deals Search</a></dd>
            </dl>
            <dl id="thomsoninfo">
              <dt><a href="http://www.thomson.co.uk/editorial/legal/about-thomson.html">Thomson info</a></dt>
              <dd><a href="http://www.thomson.co.uk/editorial/legal/about-thomson.html">About Thomson</a></dd>
              <dd><a href="http://www.thomson.co.uk/editorial/legal/contact-us.html">Contact us</a></dd>
              <dd><a href="http://tui01.metafaq.com/templates/tui01/main/mainPage">Ask us a question</a></dd>
              <dd><a href="http://www.thomson.co.uk/shopfinder/shop-finder.html">Shop finder</a></dd>
              <dd><a href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html">Privacy</a></dd>
              <dd><a href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html">Terms &amp; conditions</a></dd>
            </dl>
            <br clear="all" />
          </div>
        </div>
        <div id="rightbar">
          <h2>Thomson Destinations</h2>
          <div>
            <dl>
                <dt><a href="http://www.thomson.co.uk/destinations/africa/africa.html">Africa</a></dt>
                <dd><a href="http://www.thomson.co.uk/destinations/africa/cape-verde/cape-verde-islands/holidays-cape-verde-islands.html">Cape Verde</a></dd>
                <dd><a href="http://www.thomson.co.uk/destinations/africa/egypt/holidays-egypt.html">Egypt</a></dd>
                <dd><a href="http://www.thomson.co.uk/destinations/africa/gambia/holidays-gambia.html">Gambia</a></dd>
                <dd><a href="http://www.thomson.co.uk/destinations/africa/kenya/kenya/holidays-kenya.html">Kenya</a></dd>
                <dd><a href="http://www.thomson.co.uk/destinations/africa/morocco/holidays-morocco.html">Morocco</a></dd>
                <dd><a href="http://www.thomson.co.uk/destinations/africa/tunisia/holidays-tunisia.html">Tunisia</a></dd>
            </dl>
          </div>
          <div>
            <dl>
              <dt><a href="http://www.thomson.co.uk/destinations/asia/asia.html">Asia</a></dt>
              <dd><a href="http://www.thomson.co.uk/destinations/asia/thailand/holidays-thailand.html">Thailand</a></dd>
            </dl>
          </div>
          <div>
            <dl>
              <dt><a href="http://www.thomson.co.uk/editorial/australasia.html">Australasia</a></dt>
              <dd>&nbsp;</dd>
            </dl>
          </div>
          <div>
            <dl>
              <dt><a href="http://www.thomson.co.uk/destinations/caribbean/caribbean.html">Caribbean</a></dt>
              <dd><a href="http://www.thomson.co.uk/destinations/caribbean/barbados/holidays-barbados.html">Barbados</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/caribbean/cuba/holidays-cuba.html">Cuba</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/caribbean/dominican-republic/holidays-dominican-republic.html">Dominican Republic</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/caribbean/jamaica/holidays-jamaica.html">Jamaica</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/caribbean/mexico/holidays-mexico.html">Mexico</a></dd>
            </dl>
          </div>
          <div>
            <dl>
              <dt><a href="http://www.thomson.co.uk/destinations/europe/europe.html">Europe</a></dt>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/bulgaria/holidays-bulgaria.html">Bulgaria</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/croatia/holidays-croatia.html">Croatia</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/cyprus/holidays-cyprus.html">Cyprus</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/france/holidays-france.html">France</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/greece/holidays-greece.html">Greece</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/italy/holidays-italy.html">Italy</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/malta/holidays-malta.html">Malta</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/portugal/holidays-portugal.html">Portugal</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/serbia-and-montenegro/holidays-serbia-and-montenegro.html">Serbia and Montenegro</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/slovenia/holidays-slovenia.html">Slovenia</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/spain/holidays-spain.html">Spain</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/europe/turkey/holidays-turkey.html">Turkey</a></dd>
            </dl>
          </div>
          <div>
            <dl>
              <dt><a href="http://www.thomson.co.uk/destinations/indian-ocean/indian-ocean.html">Indian Ocean</a></dt>
              <dd><a href="http://www.thomson.co.uk/destinations/indian-ocean/india/holidays-india.html">India</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/indian-ocean/maldives/holidays-maldives.html">Maldives</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/indian-ocean/sri-lanka/holidays-sri-lanka.html">Sri Lanka</a></dd>
            </dl>
          </div>
          <div>
            <dl>
              <dt><a href="http://www.thomson.co.uk/destinations/the-americas/the-americas.html">The Americas</a></dt>
              <dd> <a href="http://www.thomson.co.uk/destinations/the-americas/brazil/holidays-brazil.html">Brazil</a>, </dd>
              <dd><a href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/holidays-united-states-of-america.html">United States of America</a></dd>
            </dl>
          </div>
        </div>
        <%-- Empty Panel --%>
        <%-- Empty Panel --%>
      </div>
      <div id="leftpanel">
        <ul id="leftnav_extras">
          <li class="open"><a href="#" onclick="toggleSec('sideFirst')">Travel</a>
            <ul id="sideFirst">
              <li><a href="http://www.thomson.co.uk">Holidays</a></li>
              <li><a href="http://www.thomsonfly.com">Flights</a></li>
              <li><a href="http://www.thomson.co.uk/buildyourown">Build Your
                  Own</a></li>
              <li><a href="http://www.thomson.co.uk/thomson/page/ao/home/home.page">Hotels</a></li>
              <li><a href="http://www.thomsoncruise.co.uk">Cruise</a></li>
              <li><a href="http://www.thomson.co.uk/villas/villa-holidays.html">Villas</a></li>
              <li><a href="http://www.thomsoncities.co.uk">City Breaks</a></li>
              <li><a href="http://www.thomsonworldwide.co.uk">Worldwide</a></li>
              <li><a href="http://www.thomsonski.co.uk">Ski</a></li>
              <li><a href="http://www.thomsonalfresco.co.uk">Mobile Homes</a></li>
            </ul>
          </li>
          <li class="open"><a href="#" onclick="toggleSec('sideSecond')">Inspiration</a>
            <ul id="sideSecond">
              <li><a href="http://www.thomson.co.uk/editorial/destinations.html">Destinations</a></li>
              <li><a href="http://www.thomson.co.uk/editorial/features/features.html">Holiday Features</a></li>
              <li><a href="http://www.thomson.co.uk/editorial/features/holiday-inspiration.html">Our
                  Best Content</a></li>
              <li><a href="http://www.thomson.co.uk/destinations/videos/videos.html">Videos</a></li>
              <li><a href="http://www.thomson.co.uk/editorial/features/brochures.html">Brochures</a></li>
            </ul>
          </li>
          <li class="open"><a href="#" onclick="toggleSec('sideThird')">Deals</a>
            <ul id="sideThird">
              <li><a href="http://destinations.thomson.co.uk/offers/offers.html">Packages</a></li>
              <li><a href="http://www.thomsonfly.com/en/offers.jsp">Flights</a></li>
              <li><a href="http://www.thomson.co.uk/dp-deals/build-your-own-deals.html">Build Your Own</a></li>
              <li><a href="http://www.thomsonbeach.co.uk/th/cruise/showCruiseSpecialOffers.do">Cruise</a></li>
              <li><a href="http://www.thomson.co.uk/thomson/page/ao/offers/offers.page">Hotels</a></li>
              <li><a href="http://www.thomson.co.uk/editorial/deals/deals.html">Other Deals</a></li>
            </ul>
          </li>
          <li class="open"><a href="#" onclick="toggleSec('sideFourth')">Extras</a>
            <ul id="sideFourth">
              <li><a href="http://www.thomsonexcursions.co.uk">Excursions</a></li>
              <li><a href="http://www.thomson.co.uk/editorial/extras/foreign-exchange.html">Foreign Exchange</a></li>
              <li><a href="http://www.thomson.co.uk/editorial/extras/airportparking.html">Airport Parking</a></li>
              <li><a href="http://www.thomson.co.uk/editorial/extras/carhire.html">Car Hire</a></li>
              <li><a href="http://www.thomson.co.uk/editorial/extras/extras.html">Other Extras</a></li>
            </ul>
          </li>
          <li class="open"><a href="#" onclick="toggleSec('sideFifth')">Useful links</a>
            <ul id="sideFifth">
              <li><a href="http://www.thomson.co.uk/destinations/sitemap/site-map.html">Sitemap</a></li>
              <li><a href="http://www.thomsonbeach.co.uk/th/beach/invokeAccommodationSearch.do">Brochure
                  code search</a></li>
              <li><a href="http://www.thomsonbeach.co.uk/th/BalancePaymentHome.do">Online
                  balance payments</a></li>
              <li><a href="http://www.thomson.co.uk/editorial/features/sustainable-tourism/sustainable-tourism-introduction.html">Green
                  Travel</a></li>
            </ul>
          </li>
          <li><a href="http://www.thomsontv.com">Thomson TV</a></li>
        </ul>
        <%-- Empty Panel --%>
        <%-- Empty Panel --%>
      </div>
    </div>
    <div id="footer">
      <jsp:include page="footer.jsp" />
    </div>
    <script type="text/javascript">
     function toggleSec(section)
     {
	    var secName = document.getElementById(section);
	    if (secName.style.display != 'none')
	    {
		   secName.style.display = 'none';
		   secName.parentNode.className = 'closed'
	    }
	    else
	    {
		   secName.style.display = 'block';
		   secName.parentNode.className = 'open'
	    }
     }
    </script>
  </body>
</html>