<%@include file="/common/commonTagLibs.jspf"%>

<div id="container_paymentdetails">
	<h2>Payment Details</h2>
	<div class="paymentdetails_margin">
	<c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'true'}">
		<div>
			<c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
				<c:if test="${costingLine.amount.amount != 0}">
					  <c:if test="${costingLine.itemDescription == 'Total Base Price'}">
						<span>
							Accommodation payable on checkout*&#58;
						</span>
						<span>
							&euro;<c:set var="basePrice" value="${costingLine.amount.amount}"/><c:out value="${costingLine.amount.amount}"/>
			         </span>
			         </c:if>
						<c:if test="${costingLine.itemDescription == 'Payable on checkout'}">
							(&pound;<c:out value="${costingLine.amount.amount}"/>&dagger;)
						</c:if>
				</c:if>
			</c:forEach>
		</div><br />
	</c:if>
	<c:if test="${bookingComponent.flightSummary.flightSelected == 'true'}">
		<div>
			<span>
				Flight payable now&#58;
			</span>
			<span>
				<c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
				<c:if test="${costingLine.itemDescription == 'Flight'}">
	              	&pound;<c:out value="${costingLine.amount.amount}"/>
					</c:if>
				</c:forEach>
			</span>
		</div><br />
	</c:if>
	<div>
	  <strong>
	      <span>
	          Total payable now&#58;
	      </span>
		  <span>&pound;<span id="totalholidaycost" class="flightPayableAmount"><c:out value="${bookingInfo.calculatedPayableAmount.amount}"/></span>*</span>
      </strong>
      <c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'true'}">
	      <span>
		     	<br /><br />&dagger; The pound sterling equivalent may vary depending on the currency conversion exchange rate applicable on the date of payment.
		  </span>
	  </c:if>
	  <br /><br />
	  <span>
	     * Some travel suppliers may request full or part payment of balance at time of booking. Local taxes and charges may apply please check the Supplier's terms and conditions above for more information.
	   </span>
    </div>
  </div>
</div>