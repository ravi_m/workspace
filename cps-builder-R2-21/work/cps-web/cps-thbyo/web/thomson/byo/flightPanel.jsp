<%@include file="/common/commonTagLibs.jspf"%>

<br clear="all" />

 <div id="flightandhotelsection">
      <h3>Flights</h3>
      <c:if test="${not empty bookingComponent.flightSummary}">
   <div  <c:if test="${bookingComponent.flightSummary.flightSelected == 'false'}"> class="inactive"  </c:if>>
   <h4>Leaving&#58;</h4>
         <c:forEach var="outboundSector" items="${bookingComponent.flightSummary.outboundFlight}">
            <c:if test="${not empty outboundSector}">
               <ul class="flight_info">
                  <li>Depart&#58;
                     <ul>
                        <li><c:out value="${outboundSector.departureAirportName}" /></li>
                        <li class="date"><fmt:formatDate value="${outboundSector.departureDateTime}" pattern="dd-MMM-yy"/></li>
                        <li class="time"><fmt:formatDate value="${outboundSector.departureDateTime}" type="TIME" pattern="HH:mm"/></li>
                     </ul>
                  </li>
                  <li>Arrive&#58;
                     <ul>
                        <li><c:out value="${outboundSector.arrivalAirportName}" /></li>
                        <li class="date"><fmt:formatDate value="${outboundSector.arrivalDateTime}" pattern="dd-MMM-yy"/></li>
                        <li class="time"><fmt:formatDate value="${outboundSector.arrivalDateTime}" type="TIME" pattern="HH:mm"/></li>
                     </ul>
                  </li>
                  <li>Carrier&#58;
                   <ul>
                       <li>
                           <c:out value="${outboundSector.carrier}" escapeXml="false"/><br/>
                           <c:out value="${outboundSector.operatingAirlineCode}${outboundSector.flightNumber}"/>
                           <br />
                       </li>
                   </ul>
               </li>
            </ul>
       </c:if>
       </c:forEach>


      <h4>Returning&#58;</h4>
         <c:forEach var="inboundSector" items="${bookingComponent.flightSummary.inboundFlight}">
            <c:if test="${not empty inboundSector}">
               <ul class="flight_info">
                  <li>Depart&#58;
                     <ul>
                        <li><c:out value="${inboundSector.departureAirportName}" /></li>
                        <li class="date"><fmt:formatDate value="${inboundSector.departureDateTime}" pattern="dd-MMM-yy"/></li>
                        <li class="time"><fmt:formatDate value="${inboundSector.departureDateTime}" type="TIME" pattern="HH:mm"/></li>
                     </ul>
                  </li>
                  <li>Arrive&#58;
                     <ul>
                        <li><c:out value="${inboundSector.arrivalAirportName}" /></li>
                        <li class="date"><fmt:formatDate value="${inboundSector.arrivalDateTime}" pattern="dd-MMM-yy"/></li>
                        <li class="time"><fmt:formatDate value="${inboundSector.arrivalDateTime}" type="TIME" pattern="HH:mm"/></li>
                     </ul>
                  </li>

                  <li>Carrier&#58;
                  <ul>
                       <li>
                           <c:out value="${inboundSector.carrier}" escapeXml="false"/><br/>
                           <c:out value="${inboundSector.operatingAirlineCode}${inboundSector.flightNumber}"/>
                           <br />
                       </li>
                   </ul>
               </li>
           </ul>
           </c:if>
           </c:forEach>
		   </div>
		   </c:if>
   </div>



