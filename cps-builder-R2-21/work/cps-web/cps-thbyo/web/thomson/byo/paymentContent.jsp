<%@include file="/common/commonTagLibs.jspf"%>
<%@include file="breadCrumb.jsp" %>
<div class="contenthead filterpage"><%-- add "filterpage" class if this page is a filtered results page --%>
   <h1>Payment Details</h1>
</div>
<div id="pricediff1" class="warning_box"  <c:if test="${fn:contains(bookingComponent.errorMessage, 'increased') || fn:contains(bookingComponent.errorMessage, 'decreased') || fn:contains(bookingComponent.errorMessage, 'Promotional') || fn:contains(bookingComponent.errorMessage, 'PROMOTIONAL') || fn:contains(bookingComponent.errorMessage, 'promotional')}">
   style="color:#3366CC;"
   </c:if> <c:if test="${empty bookingComponent.errorMessage}"> style="display:none" </c:if>>
   <c:out value="${bookingComponent.errorMessage}" escapeXml="false"/>
</div>
<div id="warnings"></div><%-- Promotional code exception messages here --%>

<script type="text/javascript">
   if(document.getElementById("pricediff1") != null || document.getElementById("error") != null)
   {
      document.getElementById('warnings').style.display = "none";
   }
</script>

<script type="text/javascript">
   var lessSeniorPassengersAged65To75Message="The number of passengers aged 65 - 74 are less than those advised on the previous page. Please check and amend.";
   var moreSeniorPassengersAged65To75Message="The number of passengers aged 65 - 74 exceed those advised on the previous page. Please check and amend.";
   var lessSeniorPassengersAged76To84Message="The number of passengers aged 75+ are less than those advised on the previous page. Please check and amend.";
   var moreSeniorPassengersAged76To84Message="The number of passengers aged 75+ exceed those advised on the previous page. Please check and amend.";
   var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />";
   var token = "<c:out value='${param.token}' />";
</script>

<%-- Terms and Conditions url--%>
<c:catch>
   <c:set var="tAndCDomain"  value="${bookingComponent.termsAndCondition.domain.clientApplicationName}" scope="page"/>
   <c:set var="relativeTAndCUrl" value="${bookingComponent.termsAndCondition.relativeTAndCUrl}" />
</c:catch>
<%
   String tAndCDomainKey = (String)pageContext.getAttribute("tAndCDomain");
   String tAndCDomainValue = com.tui.uk.config.ConfReader.getConfEntry(tAndCDomainKey+".TAndCUrl", "");
   pageContext.setAttribute("tAndCDomainValue", tAndCDomainValue);
%>
<%-- End of Terms and Conditions url--%>

<c:set var="hoplaBonded" value="${bookingInfo.bookingComponent.accommodationSummary.hoplaBonded}" scope="request" />
<%-- added the div mainContent to display the border for the body section. --%>

<%-- This whole section is added for backward compatability of 12.00.00. To be removed once
the subsequent FHPI code with insurance changes go live. --%>
<c:set var="isInsuranceChangesApplicable" value="false"/>
<c:forEach var="passenger" items="${bookingComponent.passengerRoomSummary}" varStatus="status">
   <c:forEach var="passengerRoom" items="${passenger.value}" varStatus="passen">
      <c:if test="${not empty passengerRoom.isInsuranceSelected}">
         <c:set var="isInsuranceChangesApplicable" value="true"/>
      </c:if>
   </c:forEach>
</c:forEach>
<script type="text/javascript">
   var isInsuranceChangesApplicable = <c:out value='${isInsuranceChangesApplicable}'/>
   var tandCurl = "<c:out value='${tAndCDomainValue}'  escapeXml='false'/>"+"<c:out value='${relativeTAndCUrl}' escapeXml='false'/>";
</script>
<%--End of the section. --%>
<div id="mainContent">
   <c:choose>
      <c:when test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_HOTELBEDS' || (not empty hoplaBonded && hoplaBonded)}">
         <c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'true'}">
            <jsp:include page="bondedImportantInformation.jsp" />
         </c:if>
      </c:when>
      <c:when test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
         <c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'true'}">
            <jsp:include page="hoplaImportantInformation.jsp" />
         </c:if>
      </c:when>
      <c:otherwise>
         <jsp:include page="importantInformation.jsp" />
      </c:otherwise>
   </c:choose>
   <c:choose>
      <c:when test="${isInsuranceChangesApplicable}">
         <jsp:include page="personalDetails.jsp" />
      </c:when>
      <c:otherwise>
         <jsp:include page="personalDetailsWithoutindividualInsurance.jsp" />
      </c:otherwise>
   </c:choose>
   <c:if test="${bookingComponent.packageType.code != 'DP' && bookingComponent.flightSummary.flightSupplierSystem != 'NAV'}">
      <jsp:include page="promotionalCode.jsp" />
   </c:if>
   <jsp:include page="cardDetails.jsp" />
   <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
      <c:if test="${bookingComponent.accommodationSummary.prepay != 'true'}">
         <jsp:include page="paymentDetails.jsp" />
      </c:if>
      <c:if test="${bookingComponent.accommodationSummary.prepay == 'true'}">
         <jsp:include page="fullPayment.jsp" />
      </c:if>
   </c:if>
   <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem != 'HOPLA_THM' && bookingComponent.accommodationSummary.accommodationInventorySystem != 'HOPLA_PEG'}">
      <c:choose>
         <c:when test="${not empty bookingComponent.depositComponents && bookingComponent.packageType.code != 'DP' &&
                          bookingComponent.flightSummary.flightSupplierSystem != 'NAV'}">
            <jsp:include page="depositOptions.jsp" />
         </c:when>
         <c:otherwise>
            <jsp:include page="fullPayment.jsp" />
         </c:otherwise>
      </c:choose>
   </c:if>
   <jsp:include page="termsAndCondition.jsp" />
</div>