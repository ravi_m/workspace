<%@include file="/common/commonTagLibs.jspf"%>

<div id="t_and_c">
	<h2>Terms and Conditions</h2>
	<%-- On any error below class="highlight" becomes class="highlight errormessage"--%>

	<%-- Start of the key formation --%>
	<c:set var="accommodationInventorySystem" value="" scope="page"/>
	<c:set var="flightInventorySystem" value="" scope="page"/>
    <c:set var="clientDomainURL" value="${bookingComponent.clientDomainURL}"/>
	<c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'true'}">
		<c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem != null}">
			<c:set var="accommodationInventorySystem" value="${bookingComponent.accommodationSummary.accommodationInventorySystem}" scope="page"/>
		</c:if>
	</c:if>
	<c:if test="${bookingComponent.flightSummary.flightSelected == 'true'}">
		<c:if test="${bookingComponent.flightSummary.flightSupplierSystem != null}">
			<c:set var="flightInventorySystem" value="${bookingComponent.flightSummary.flightSupplierSystem}" scope="page"/>
		</c:if>
	</c:if>

	<%-- Start of Bugzilla: 32890 --%>
       <c:forEach var="outboundFlight" items="${bookingComponent.flightSummary.outboundFlight}">
         <fmt:formatDate var="departureDate" value="${outboundFlight.departureDateTime}" pattern="yyyy-MM-dd"/>
       </c:forEach>

	<%-- End of Bugzilla: 32890 --%>
	<%-- End of key formation--%>

	<%-- Start of first para --%>
	<div class="highlight">
		<p>Please read our
			<a href="javascript:void(0);"
			onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',755,584,'scrollbars=yes,resizable=yes');" >
			Privacy Policy</a>
			and
			<a href="javascript:void(0);"
			onclick="javascript:showHiddenInformation('dataprotectionnoticediv')" >
			Data Protection Notice</a>
		and confirm you agree to our use of your information provided above (which may in special situations include sensitive personal data) by ticking the box below. </p>
	</div>
	<%-- End of first para--%>
	<%-- Start of data protection text (hidden by default)--%>
    <div class="highlight" id="dataprotectionnoticediv" style="display:none;">
	  <strong>Data Protection Notice:</strong><br />
      We may from time to time contact you by post or e-communications with information on offers of goods and services, brochures, new products, forthcoming events or competitions from our holiday divisions and our holiday group companies. If you have not already done so, by providing your address, e-mail and phone number you agree to its use.

      <div class="dataprotection1">
         <input name="chkTuiMarketingAllowed" id="chkTuiMarketingAllowed" type="checkbox" value="true" onClick="updateFormElementFromCheckBoxMarketing(this,'tuiMarketingAllowed');"/>
         <input type="hidden" name="tuiMarketingAllowed" id="tuiMarketingAllowed"/>
         If you would not like to receive <strong>e-communications</strong> including information on discounts from <a href="https://www.thomson.co.uk" target="_blank">thomson.co.uk</a>, please
         <u>tick</u> this box.
      </div>
      <div class="dataprotection2">
         <input name="chkThirdPartyMarketingAllowed" id="chkThirdPartyMarketingAllowed" type="checkbox"
         value="true" onClick="updateFormElementFromCheckBoxMarketing(this,'thirdPartyMarketingAllowed');"/>
         <input type="hidden" name="thirdPartyMarketingAllowed" id="thirdPartyMarketingAllowed"/>
         Our business partners and carefully selected companies outside our holiday group would like to send you
         information about their products and services <strong>by post</strong>. If you would not like to hear from them, please <u>tick</u> this box.
      </div>
	</div>
<%-- End of data protection text (hidden by default)--%>

	<%-- Start of passport text--%>
		<div class="highlight">
			<p>Please note that all members of your party require valid passports and any applicable visas before travelling. Visit the Foreign Office website at
			<a href="http://www.fco.gov.uk/travel" target="_blank">http://www.fco.gov.uk/travel</a> to see visa and travel advice, or the Passport Office website at
			<a href="http://www.passport.gov.uk" target="_blank">http://www.passport.gov.uk</a> for passport information.</p>
		</div>
	<c:if test="${flightInventorySystem != null && flightInventorySystem != ''}">
	   <c:if test="${!(bookingComponent.flightSummary.flightSupplierSystem =='Amadeus' ||
         bookingComponent.flightSummary.flightSupplierSystem =='NAV') }">
         <div class="highlight">
			   <p>To view the notice summarising the liability rules applied by Community air carriers as required by Community legislation and the Montreal Convention, please view the
			   <a href="javascript:void(0);" onclick="Popup('http://www.thomson.co.uk/editorial/legal/montreal-convention.html',795,595,'location=0,status=0,left=100,top=50,toolbar=0,menubar=0,titlebar=0,resizable=1,scrollbars=1')">
			   Air Passenger Notice.</a></p>
		   </div>
      </c:if>
	</c:if>
	<%-- End of passport text--%>
	<%-- Start of Booking Conditions and Privacy Policy --%>
	<div class="highlight termscheck_height">
		<input name="tourOperatorTermsAccepted" type="checkbox" value="on"  alt="You must read and accept our Terms and Conditions before confirming your booking.|Y|CHECK" />
			<div class="width_add1">
				<label for="book_cond">
					I confirm that :<br />
               (a) as the lead name/passenger, I am at least 18 years old at the time of booking.<br />
               (b) I have contacted the relevant travel supplier(s) regarding any special needs before booking and <br />
               (c) I have read and accept all of the following:
                </label>
				<ul class="bookingconds">
				   <li><a href="javascript:void(0)" onclick="popUpBookingConditions('<c:out value="${accommodationInventorySystem}"/>','<c:out value="${flightInventorySystem}"/>','<c:out value="${departureDate}"/>','<c:out value="${clientDomainURL}"/>' )">Booking Conditions</a></li>
				   <li><a href="javascript:void(0);" onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',755,584,'scrollbars=yes,resizable=yes');" >Privacy Policy</a>
				   and use of my information</li>
				</ul>
			</div>
	</div>
	<%-- need to check this it is not getting displayed correctly --%>
	<div id="legalinformationdiv" class="highlight highlight_protectionnotice" style="display:none;">
		<p id="dataprotectionnoticebox" ><strong>Contract party</strong><br/>
		Your flight is booked through Budget Air Limited, Registered Office: TUI UK Limited, Wigmore House, Wigmore Place, Wigmore Lane, Luton, LU2 9TN.  Registered No. 4484398 ("Budget Air").  Budget Air acts as agent only and will not be responsible for your flight. Your agreement in respect of the flight is with Thomsonfly Limited (hereinafter "Thomsonfly"). <br/>
		Please note that your agreement with Thomsonfly will be subject to:</p>
		<ul>
			<li><a href="javascript:void(0)" onclick="popUpBookingConditions('<c:out value="${accommodationInventorySystem}"/>','<c:out value="${flightInventorySystem}"/>','<c:out value="${departureDate}"/>','<c:out value="${clientDomainURL}"/>')">
			Booking Conditions</a></li>
			<li><a href="javascript:void(0);" onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',755,584,'scrollbars=yes');" >
			Terms of Use</a></li>
			<li><a href="javascript:void(0);" onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',755,584,'scrollbars=yes');" >
			Conditions of Carriage of Thomsonfly</a></li>
			<li><a href="javascript:void(0);" onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',755,584,'scrollbars=yes,resizable=yes');" >
			Privacy Policy</a>&nbsp;
			and use of my information</li>
		</ul>
	</div>

	<%-- Start of pay info text --%>
	<div class="width_add">
	<c:set var="clickpayinfotext" value="${bookingComponent.termsAndCondition.payInfoText}" scope="page"/>
	 <p><c:out value="${clickpayinfotext}" escapeXml="false"/></p>
	</div>
	<%-- End of pay info text--%>

	<%-- Start of back and pay buttons--%>
	<div class="width_add">
		<span class="backlink">
			<a href="<c:out value='${travelOptionsPageUrl}'/>" class="backlink" title="Back">
			<img src="/cms-cps/thomson/byo/images/img_dp_button_back.gif"  border="0"/>Back</a>
		</span>
		<c:if test="${bookingInfo.newHoliday}">
		   <span id="confirmButton">
			  <a href="javascript:void(0);" onclick="javascript:updateEssentialFields();javascript:CheckSeniorPassengerAndValidate();"  id="threeDPay">
			  <img src="/cms-cps/thomson/byo/images/pay-btn.gif" alt="Pay" title="Pay" border="0"/>
			  </a>
		   </span>
       </c:if>
		<br clear="all" />
	</div>
	<%-- End of back and pay buttons--%>
	<br clear="all" />
</div>

