<%@include file="/common/commonTagLibs.jspf"%>

<div id="payment_amount">
   <h2>Payment Amount</h2>
   <p>
      Your total holiday cost is <strong>&pound;<span id="totalholidaycost" class="total"><c:out value="${bookingInfo.calculatedTotalAmount.amount}"/></span >*.</strong>
      <br clear="all"/>
   </p>
</div>
<div>
   <c:choose>
      <c:when test="${lowDepositFeatureAvailable == 'true'}">
         <p>* For each payment by credit card a credit card charge will apply. There is no charge for debit card payments.</p>
      </c:when>
      <c:otherwise>
         <p>*If you pay by credit card the charges will be updated in the price panel once you have selected your method of payment.</p>
      </c:otherwise>
   </c:choose>
</div>