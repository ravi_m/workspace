<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      <version-tag:version/>
      <title>Build Your Own Holidays  - Flight and Hotels - Payment</title>
      <c:set var="clientapp" value="${bookingInfo.bookingComponent.clientApplication.clientApplicationName}"/>
      <c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
      <%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
      %>
      <%-- Include Payment related CSS  here --%>
      <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byo/css/headfoot.css" />
      <link rel="stylesheet" type="text/css"  href="/cms-cps/thomson/byo/css/payment.css"  />
      <link rel="stylesheet"  type="text/css" href="/cms-cps/thomson/byo/css/travel_opts.css" />
      <link rel="stylesheet"  type="text/css" href="/cms-cps/thomson/byo/css/integrated_payment.css" />
      <link rel="shortcut icon" href="/cms-cps/thomson/byo/images/favicon.ico"/>
	  <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byo/css/mbox.css" />
      <!--[if IE 6]>
	     <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byo/css/integrated_payments_ie6.css"/>
      <![endif]-->
	  <!--[if IE 7]>
	     <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byo/css/travel_opts_ie7.css"/>
	  <![endif]-->
      <%-- End of css files --%>
      <%-- Include Payment related JS Here --%>
      <script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/paymentPanelContent.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/paymentPanelValidation.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>

      <script type="text/javascript" src="/cms-cps/thomson/byo/js/popups.js"></script>
      <script type="text/javascript" src="/cms-cps/thomson/byo/js/general.js"></script>
      <script type="text/javascript" src="/cms-cps/thomson/byo/js/formvalidation.js"></script>
      <script type="text/javascript" src="/cms-cps/thomson/byo/js/footer.js"></script>
      <script type="text/javascript" src="/cms-cps/thomson/byo/js/paymentUpdation.js"></script>
	  <script type="text/javascript" src="/cms-cps/thomson/byo/js/mbox.js"></script>
      <%-- End of JS files --%>
   </head>
   <body>
   <style>
   @font-face {
      font-family: 'TUITypeLightRegular';
      src: url('/cms-cps/thomson/byo/css/webfont/webfont.tuitypelt.eot');
      src: local('TUITypeLightRegular'),
      url('/cms-cps/thomson/byo/css/webfont/webfont.tuitypelt.woff') format('woff'),
      url('/cms-cps/thomson/byo/css/webfont/webfont.tuitypelt.ttf') format('truetype'),
      url('/cms-cps/thomson/byo/css/webfont/webfont.tuitypelt.svg#webfontXOtQpBc2') format('svg');
      font-weight: normal;
      font-style: normal;
   }
   @font-face {
      font-family: 'TUITypeRegular';
      src: url('/cms-cps/thomson/byo/css/webfont/webfont.tuitype.eot');
      src: local('TUITypeRegular'),
      url('/cms-cps/thomson/byo/css/webfont/webfont.tuitype.woff') format('woff'),
      url('/cms-cps/thomson/byo/css/webfont/webfont.tuitype.ttf') format('truetype'),
      url('/cms-cps/thomson/byo/css/webfont/webfont.tuitype.svg#webfontXOtQpBc2') format('svg');
      font-weight: normal;
      font-style: normal;
   }
</style>
   <!-- Start of DoubleClick Floodlight Tag: Please do not remove
   Activity name of this tag: Thomson Payment Page
   URL of the webpage where the tag is expected to be placed: http://www.thomson.co.uk
   This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
   Creation Date: 05/05/2010 -->
   <script type="text/javascript">
     var axel = Math.random() + "";
     var a = axel * 10000000000000;
     document.write('<iframe src="https://fls.doubleclick.net/activityi;src=2205258;type=thoms460;cat=thoms149;ord=1;num=' + a + '?" width="1" height="1" frameborder="0"></iframe>');
   </script>
   <noscript>
     <iframe src="https://fls.doubleclick.net/activityi;src=2205258;type=thoms460;cat=thoms149;ord=1;num=1?" width="1" height="1" frameborder="0"></iframe>
   </noscript>
   <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
   <c:set var="visiturl" value="yes" scope="session" />
      <div id="header">
         <jsp:include page="header.jsp" />
         <div class="mboxDefault">
         </div>
    <script type="text/javascript">
       mboxCreate("T_PackagePayment_Text");
    </script>

      </div>
      <%-- Body section --%>
      <form name="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />">
         <div id="bodysection">
            <div id="content"><%--Right panel   --%>
               <jsp:include page="paymentContent.jsp" />
            </div><%--End of Right panel  --%>
            <div id="leftpanel"><%--Left panel   --%>
               <jsp:include page="summaryPanel.jsp"/>
            </div><%--End of Left panel  --%>
         </div>
      </form>
      <%-- End of body section --%>
      <div id="footer"><%-- Footer --%>
         <jsp:include page="footer.jsp"/>
      </div><%-- End of  Footer --%>
      <script type="text/javascript" src="/cms-cps/thomson/byo/js/vs.js"></script>
   </body>
</html>