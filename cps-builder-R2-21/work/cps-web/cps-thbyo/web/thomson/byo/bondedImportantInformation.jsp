<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="displayErrataSection" value="false" scope="page"/>

<c:if test="${not empty bookingComponent.importantInformation.errata}">
   <c:forEach var="accomerrata" items="${bookingComponent.importantInformation.errata}">
      <c:if test="${not empty accomerrata && accomerrata !='No errata'}">
         <c:set var="displayErrataSection" value="true" scope="page"/>
      </c:if>
   </c:forEach>
</c:if>
<c:if test="${displayErrataSection}">
   <div id="imp_info">
      <h2>Important Information</h2>
	  <div class="errormessage">
	     <c:forEach var="accomerrata" items="${bookingComponent.importantInformation.errata}">
		    <c:if test="${accomerrata != 'No errata'}">
			   <c:out value="${accomerrata}" escapeXml="false"/>
			</c:if>
		 </c:forEach><br clear="all"/>
	  </div>
   </div>
</c:if>
<c:if test="${(bookingComponent.flightSummary.flightSupplierSystem)=='Amadeus' && (bookingComponent.flightSummary.flightSelected) == 'true'}">
   <div class="amadeustermsandconditions">
      <p>Full terms and conditions of your flight product component supplier are available on request.  Any cancellation, amendments or changes may incur a charge of up to 100% of your booking price plus supplier&#146;s administrative expenses.</p>
      <p>If the cancellation fee is 100% and you wish to re-book, the price of your new flight will usually be based on the prices applicable on the day you ask to re-book, which may not be the same as when you first booked.</p>
      <p>International travel is subject to the liability rules of the Warsaw Convention and the Montreal Convention. To view the notice summarising the liability rules applied by EU community air carriers please read the <a href="javascript:void(0);" onclick="Popup('http://www.thomson.co.uk/editorial/legal/montreal-convention.html',795,595,'location=0,status=0,left=100,top=50,toolbar=0,menubar=0,titlebar=0,resizable=1,scrollbars=1')">Air Passenger Notice.</a></p>
   </div>
</c:if>
<c:choose>
   <c:when test= "${displayErrataSection}">
      <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
	     <c:set var="redundntInsTxt" value="${description.value}" />
		 <c:if test="${redundntInsTxt != null && description.key == 'redundntInsuranceTxt'}">
		    <p><c:out value="${redundntInsTxt}" escapeXml="false"/></p>
		 </c:if>
	  </c:forEach>
   </c:when>
   <c:otherwise>
      <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
	     <c:set var="redundntInsTxt" value="${description.value}" />
         <c:if test ="${redundntInsTxt != null && description.key == 'redundntInsuranceTxt'}">
		    <div id="imp_info">
	   	       <h2>Important Information</h2>
	   	       <p>
			      <c:out value="${redundntInsTxt}" escapeXml="false"/>
			   </p>
            </div>
		 </c:if>
	  </c:forEach>
   </c:otherwise>
</c:choose>