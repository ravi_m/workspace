<%@include file="/common/commonTagLibs.jspf"%>

   <div id="container_importantinformation">
      <h2>Important Information</h2>

      <p>We offer your selection as a choice of separate components which you can choose individually and purchase at the same time or subsequently as separate items but not as a combination of services at an inclusive price.</p>
      <div id="container_importantinformation_text1" class="bodytext_normal">
         <p>Here is some additional information about the holiday you have chosen. If you are happy to proceed please click to accept or <a href="<c:out value='${resultsPageUrl}'/>"><strong>return to your results</strong></a> to select an alternative. <a href="<c:out value='${resultsPageUrl}'/>" target="_blank"></a></p>
      </div>
      <div class="container_importantinformation_text2">
            <p>In addition to any amendment or cancellation fees charged by your travel suppliers, Thomson charges a &pound;25 per person fee for amendments and cancellations. Please refer to the Terms and Conditions for full details of your contractual arrangements with Thomson and your travel suppliers.<br/></p>
      </div>
      <div class="highlight">
       <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
          <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
                      <c:set var="extrainfo" value="${description.value}" />
                      <c:if test ="${extrainfo != null && description.key == 'Cancelletion policy:'}">
                       <p><strong><c:out value="${extrainfo}" escapeXml="false"/></strong></p>
                      </c:if>
				      </c:forEach>

        </c:if>
        <c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'true'}">
               <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_THM'}">
                 <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
			         <c:if test="${description.value != null && description.key == 'cancellationcharges'}">
			                <p><c:out value="${description.value}" escapeXml="false"/></p>
			         </c:if>
			      </c:forEach>
		       </c:if>
	    </c:if>
	    <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
          <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
                      <c:set var="extrainfo" value="${description.value}" />
                      <c:if test ="${extrainfo != null && description.key == 'Guarantee policy:'}">
                       <p><strong><c:out value="${extrainfo}" escapeXml="false"/></strong></p>
                      </c:if>
				      </c:forEach>

        </c:if>
        <c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'true'}">
          <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
           	    <p><strong>Supplier's terms and conditions: -</strong></p>
                <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
                      <c:set var="extrainfo" value="${description.value}" />
                      <c:if test ="${extrainfo != null && description.key == 'Room type:'}">
                       <p><strong><c:out value="${description.key}"/></strong>
                      <c:out value="${extrainfo}" escapeXml="false"/></p><br/>
                      </c:if>
				      </c:forEach>
                <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
                      <c:set var="extrainfo" value="${description.value}" />
                      <c:if test ="${extrainfo != null && description.key == 'Rate plan:'}">
                       <p><strong><c:out value="${description.key}"/></strong>
                      <c:out value="${extrainfo}" escapeXml="false"/></p><br/>
                      </c:if>
				      </c:forEach>
                   <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
                      <c:set var="extrainfo" value="${description.value}" />
                      <c:if test ="${extrainfo != null && description.key == 'Deposit policy:'}">
                       <p><strong><c:out value="${description.key}"/></strong>
                      <c:out value="${extrainfo}" escapeXml="false"/></p><br/>
                      </c:if>
				      </c:forEach>

                  <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
                  <c:set var="extrainfo" value="${description.value}" />
                  <c:if test ="${extrainfo != null && description.key == 'Cancelletion policy:'}">
                  <p><strong><c:out value="${description.key}"/></strong><c:out value="${extrainfo}" escapeXml="false"/></p><br/></c:if></c:forEach>
                  <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
                      <c:set var="extrainfo" value="${description.value}" />
                      <c:if test ="${extrainfo != null && description.key == 'Tax information:'}">
                       <p><strong><c:out value="${description.key}"/></strong>
                      <c:out value="${extrainfo}" escapeXml="false"/></p><br/>
                      </c:if>
				  </c:forEach>
                  <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
                      <c:set var="extrainfo" value="${description.value}" />
                      <c:if test ="${extrainfo != null && description.key == 'Guarantee policy:'}">
                       <p><strong><c:out value="${description.key}"/></strong>
                      <c:out value="${extrainfo}" escapeXml="false"/></p><br/>
                      </c:if>
				  </c:forEach>
                <p>We aim to provide you with the most accurate rate information, but we urge you to check with the individual hotel for child policies and possible extra charges</p>
        </c:if>
		</c:if>
		</div>
			 <c:if test="${(bookingComponent.flightSummary.flightSupplierSystem)=='Amadeus' && (bookingComponent.flightSummary.flightSelected) == 'true'}">
         <div class="amadeustermsandconditions">
            <p>Full terms and conditions of your flight product component supplier are available on request.  Any cancellation, amendments or changes may incur a charge of up to 100% of your booking price plus supplier&#146;s administrative expenses.</p>
            <p>If the cancellation fee is 100% and you wish to re-book, the price of your new flight will usually be based on the prices applicable on the day you ask to re-book, which may not be the same as when you first booked.</p>
            <p>International travel is subject to the liability rules of the Warsaw Convention and the Montreal Convention. To view the notice summarising the liability rules applied by EU community air carriers please read the <a href="javascript:void(0);" onclick="Popup('http://www.thomson.co.uk/editorial/legal/montreal-convention.html',795,595,'location=0,status=0,left=100,top=50,toolbar=0,menubar=0,titlebar=0,resizable=1,scrollbars=1')">Air Passenger Notice.</a></p>
         </div>
      </c:if>
      <div class="highlight container_importantinformation_form">
         <span class=" termscheck_height_ao">
            <input name="importantInformationChecked" type="checkbox" alt="You must read and accept our important additional information before confirming your booking|Y|CHECK" value="on" />
         </span>
         <span class="bodytext_normal_margintop">
            <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
               I have read and accept the suppliers terms and conditions.
            </c:if>
            <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem != 'HOPLA_PEG'}">
               I have read and accept the important additional information.
            </c:if>
         </span>
      </div>
   </div>