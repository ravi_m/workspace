<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="depositPerPerson" scope="page" value="${bookingComponent.nonPaymentData['summaryPanelDepositAmount']}"/>

<%-- price section start --%>
<div id="price">
    <div class="logo_img">
       <img src='/cms-cps/thomson/byo/images/ATOL_summary_panel_logo.gif'  title="ATOL Logo" border="0"/>
    </div>
    <%-- Retrieve the Costlinenames article --%>
          <c:set var="PBTSDescription" value=""/>
       <dl class="price_summary">
           <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
                       <c:choose>
                              <c:when test="${costingLine.itemDescription=='Promotional Discount'}">
                                           <c:if test="${costingLine.itemDescription=='Promotional Discount'}">
                       <c:set var="PromoDiscountText" scope="page" value="Promotional Discount"/>
                       <fmt:formatNumber value="${costingLine.amount.amount}" var="PromoDiscountAmount" type="number" maxFractionDigits="2" minFractionDigits="2"/>
                                           </c:if>
                                          <fmt:formatNumber value="${costingLine.amount.amount}" var="PBTSAmount" type="number" maxFractionDigits="2" minFractionDigits="2"/>
                               </c:when>
                               <c:when test="${costingLine.itemDescription=='Discount %'}">
                                          <c:set var="DiscountPercDescr" value="${costingLine.itemDescription }"/>
                                           <fmt:formatNumber value="${costingLine.amount.amount}" var="DiscountPerc" type="number" maxFractionDigits="2" minFractionDigits="2"/>
                                </c:when>
                       </c:choose>
             <c:if test="${costingLine.amount.amount != 0.0}">
                   <c:set var="description" value='${costingLine.itemDescription}'/>
                   <c:set var="description" value="${costingLine.itemDescription}"/>
                       <c:choose>
                           <c:when test="${bookingComponent.accommodationSummary.caption == 'FLY_DRIVE' && description == 'Flight + Hotel'}">
                                <dt> Fly Drive</dt>
                           </c:when>
                           <c:otherwise>
                               <c:choose>
                                   <c:when test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
                                   <c:if test="${description !='Total Base Price'}">
                                     <c:choose>
                   <c:when test='${description == "Payable on checkout"}'>
                                           <dt <c:if test="${description =='Payable on checkout'}"> class="boldme widthadjustdt"  </c:if>>
                                                    <span  <c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'false'}">class="inactive" </c:if>>
                                                        Hotel:
                                                   </span>
                                           </dt>
                                        </c:when>
                                       <c:when test="${description == 'Flight'}">
                                       <dt <c:if test="${description =='Flight'}"> class="boldme widthadjustdt"  </c:if>>
                                          <span  <c:if test="${bookingComponent.flightSummary.flightSelected == 'false'}">   class="inactive"  </c:if>>
                                              <c:out value="${description}"/>:
                                           </span>
                                          </dt>
                                       </c:when>
                                        <c:otherwise>
                                             <dt>
                                                  <c:if test="${costingLine.quantity != null}">
                                                     <c:out value="${costingLine.quantity}"/> x
                                                 </c:if>
                                                 <c:out value="${description}"/>: 
                                              </dt>
                                        </c:otherwise>
                                        </c:choose>
                                        </c:if>
                                   </c:when>
                                   <c:otherwise><%-- For PDP( and other non HOPLA packages)  --%>
                                          <c:if test="${description!='PriceBeat' && description!='DiscretionaryAmount'&& description!='Discount %' && description!='Promotional Discount'}">
                                                   <dt <c:if test="${description =='Flight + Hotel' || description == 'Flight' || description == 'Hotel'}"> class="boldme widthadjustdt"  </c:if>>
                                                   <span   <c:if test="${(description == 'Flight' && bookingComponent.flightSummary.flightSelected == 'false')
                                                    || ((description == 'Hotel' || description == 'Payable on checkout')&& bookingComponent.accommodationSummary.accommodationSelected == 'false')}">   class="inactive"  </c:if>>
                                                    <c:if test="${costingLine.quantity != null}">
                                                         <c:out value="${costingLine.quantity}"/> x
                                                     </c:if>
                                                     <c:choose>
                                                     <c:when test="${(description == 'Flight' || description == 'Hotel') && (bookingComponent.packageType.code =='DP')}">
                                                      <c:out value="${description}"/>:
                                                     </c:when>
                                                     <c:when test="${(description == 'Payable on checkout') && (bookingComponent.packageType.code =='DP')}">
                                                      <b>Hotel:</b>
                                                     </c:when>
                                                     <c:otherwise>
                                                     <c:if test ="${description !='Total Base Price'}">
                                                     <c:out value="${description}"/>:
                                                     </c:if>
                                                     </c:otherwise>
                                                     </c:choose>
                                                     </span>
                                                   </dt>
                                          </c:if>
                                  </c:otherwise>
                               </c:choose>
                           </c:otherwise>
                       </c:choose>
                       <c:if test="${description=='web saving'}">
                          <dd> <c:if test="${costingLine.amount.amount > 0}">-</c:if>&pound;<fmt:formatNumber value="${costingLine.amount.amount}" var="linetotal" type="number" maxFractionDigits="2" minFractionDigits="2"/><c:out value="${linetotal}"/></dd>
                       </c:if>
                           <c:if test="${description !='Total Base Price' && description!='web saving'}">
                           <fmt:formatNumber value="${costingLine.amount.amount}" var="linetotal" type="number" maxFractionDigits="2" minFractionDigits="2"/>
                           <c:choose>
                                       <c:when test="${description == 'Flight'}">
                                        <dd>
                                          <span  <c:if test="${bookingComponent.flightSummary.flightSelected == 'false'}"> class="inactive inactive_strike"  </c:if>>&pound;<c:out value="${linetotal}"/>
                                              </span>
                                           </dd>
                                       </c:when>
                     <c:when test="${description == 'Hotel'}">
                                        <dd>
                                          <span  <c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'false'}"> class="inactive inactive_strike"  </c:if>>&pound;<c:out value="${linetotal}"/>
                                              </span>
                                           </dd>
                                       </c:when>
                                        <c:when test='${description == "Payable on checkout"}'>
                                        <dd>
                                            <span  <c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'false'}">class="inactive inactive_strike" </c:if>>&pound;<c:out value="${linetotal}"/>
                                           </span>
                                           </dd>
                                        </c:when>
                                        <c:otherwise>
                                              <c:if test="${description!='PriceBeat' && description!='DiscretionaryAmount'&& description!='Discount %' && description!='Promotional Discount'}">
                                       <dd <c:if test="${description =='Flight + Hotel' || description == 'Hotel'}"> class="boldme widthadjustdd"  </c:if>>&pound;<c:out value="${linetotal}"/></dd>
                                              </c:if>
                                        </c:otherwise>
                            </c:choose>
                       </c:if>
                    </c:if>
           </c:forEach>

           <c:if test="${bookingInfo.newHoliday == 'false' && (bookingInfo.calculatedCardCharge.amount != null && bookingInfo.calculatedCardCharge.amount != 0)}">
  <dt>Card Charges:</dt>
  <dd>&pound;<c:out value="${bookingInfo.calculatedCardCharge.amount}"/></dd>
          </c:if>

           <dt id='cardChargeText' style='display:none'>Card Charges:</dt>
           <dd id='cardChargeAmount'  style='display:none'></dd>
           <dt id='promoText' <c:if test="${PromoDiscountText=='' || PromoDiscountText==null}">style='display:none' </c:if>>
        <c:out value="${PromoDiscountText}"/>:
       </dt>
           <dd id='promoCodeDiscount' <c:if test="${PromoDiscountText=='' || PromoDiscountText==null}">style='display:none' </c:if>>-&pound;<c:out value="${PromoDiscountAmount}"/>
       </dd>
       <c:if test="${depositPerPerson == null}">
            <dt id='totalText'>Total&#58;</dt>
                <dd id='totalAmount' class="total">&pound;<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" var="totalCostingLineWithoutDiscount" maxFractionDigits="2" minFractionDigits="2"/><c:out value="${totalCostingLineWithoutDiscount}"/><br clear="all"/></dd>
       </c:if>
       </dl>
     <br clear="all">
       <c:if test="${depositPerPerson != null}">
         <div id="summaryPanelTotalAmount">
           <span id="totalText">Total&#58;</span>
           <span id='totalAmount' class="total">&pound;<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" var="totalCostingLineWithoutDiscount" maxFractionDigits="2" minFractionDigits="2"/><c:out value="${totalCostingLineWithoutDiscount}"/></span><br clear="all">
       <fmt:formatNumber value="${depositPerPerson}" type="number" var="depositPerPersonFormatted" maxFractionDigits="0" minFractionDigits="0"/>
           <span id="summaryPanelDepositText">Pay only <span id="displayAmountInRed">&pound;<c:out value="${depositPerPersonFormatted}"/></span> deposit per person</span>
         </div>
     </c:if>

   </div><%--End of price div --%>
