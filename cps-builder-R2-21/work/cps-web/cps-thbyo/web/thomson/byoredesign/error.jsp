<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request"/>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      <title>Payments - Error!!</title>

	  <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byoredesign/css/reset.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byoredesign/css/global.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byoredesign/css/flexibox.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byoredesign/css/payment_details.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byoredesign/css/footer.css"/>
      <!--[if IE 7]>
      <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byoredesign/css/styles-ie7.css"/>
      <![endif]-->
      <link rel="shortcut icon" href="/cms-cps/thomson/byoredesign/images/favicon.ico"/>
      <%@include file="javascript.jspf" %>
      <%@include file="tag.jsp"%>
   </head>
   <body>
      <div class="pageContainer" id="payments">
	     <%@include file="header.jspf" %>
		 <div id="contentSection">
		    <!--<%@include file="breadCrumb.jspf" %>-->
			
		    <div class="error_outer">
			   <div class="flexiBox greyContainer">
			      <div class="tp"><span class="border">&nbsp;</span></div>
				  <div class="midle">
				     <span class="shadow">&nbsp;</span>
					 <div class="body">
                        <c:choose>
                           <c:when test="${not empty bookingInfo.trackingData.sessionId}">
                              <%@include file="errorTechDifficulties.jspf"%>
                           </c:when>
                           <c:otherwise>
                              <%@include file="errorSessionTimeOut.jspf"%>
                           </c:otherwise>
                        </c:choose>
			         </div>
			      </div>
				  <div class="botom"><span class="border">&nbsp;</span></div>
			   </div>
		    </div>
            <div id="contentCol1">
			   <div class="flexiBox leftPanel">
			   </div>
	        </div>
			
         </div>
		 <div id="footer_outer">
		 <div class="clearboth"></div>
			<div id="footerSection">
              <%@include file="footer.jspf"%>
            </div>
			</div>
      </div>
   </body>
</html>