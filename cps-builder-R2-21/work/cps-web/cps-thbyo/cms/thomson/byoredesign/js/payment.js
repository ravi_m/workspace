var PaymentView =
{
   /**
    * Show alert and focus the field.
    */
   showAlert:function(Msg, fieldObj)
   {
      alert(Msg);
	  if (fieldObj != null)
	  {
         fieldObj.focus();
	  }
      return false;
   },

   /**
    * refresh the field
    */
   refreshTheField:function(fieldObj)
   {
      if (fieldObj.type == 'text')
      {
	     fieldObj.value = "";
	  }
	  else if (fieldObj.type == 'checkbox')
	  {
	     fieldObj.checked = false;
	  }
   },

   /**
    * clear the text in a input field.
    *
    * @param fieldObj the field read as DOM
    */
   emptyTheField : function(fieldObj)
   {
      fieldObj.val("");
   },

   /**
    * sets dropDown to default selection(i.e. selected index will be 0)
    *
    * @param dropDownObj the drop down read as DOM
    */
   setDropDownsToDefault : function(dropDownObj)
   {
      dropDownObj.selectedIndex = 0;
   },

   /**
    ** Highlights the container whose id is provided
    ** passed in as a parameter.
    ** @param containerID the id of the container to be highlighted
    ** @param highlightClass the css class which highlights the section
    ** @param isToBeHighlighted switch to highlight/unhighlight a section
   **/
       highlightContainer:function (containerID, highlightClass,isToBeHighlighted)
       {
           if(isToBeHighlighted)
           {
              $("#"+containerID).addClass(highlightClass);
           }
           else
           {
              $("#"+containerID).removeClass(highlightClass);
           }
       },

	 /**This function shall be responsible for updating the button caption of the Submit button*/
	 changeCaption: function(caption)
	{
		 $("#submit").val(caption);
		 $("#submit").attr("title",caption);
	},

	/**This function hides/unhides a field based on the flag sent*/
	displayContainer:function(containerID,isToBeHidden)
	{
		if(isToBeHidden)
		{
		   $("#"+containerID).addClass("hide");
		}
		else
		{
		   $("#"+containerID).removeClass("hide");
		}

	}
};

/*var AddressFinderHandler =
{
   handle:function()
   {
      var token = $("#token").val();
	  var tomcatInstance = $("#tomcatInstance").val();
      var postCode = $("#leadPostcode").val().toUpperCase();
      Pat1 = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
	  if(StringUtils.isBlank(postCode))
	  {
	     PaymentView.showAlert("Please enter a Postcode.",$("#leadPostcode"))
		 return;
	  }
	  else if(!Pat1.test(postCode))
      {
         PaymentView.showAlert("We can not validate the post code you have entered. Please check and re-enter.",$("#leadPostcode"));
		 return;
      }
      var url="/cps/addressFinder?postcode="+postCode+"&token="+token;
	  url = uncache(url);
	  url += "&tomcat="+tomcatInstance;
	  var request = $.ajax({url: url, type:'get', async: false});
	  var xmlDocument = request.responseXML;
	  if(xmlDocument.getElementsByTagName('addresses').length == 0){
	     $("#addressFinderError").removeClass("hide");
		 this.manualAddress();}
	  else{
	     this.showAddresses(xmlDocument);}
   },

   showAddresses:function(xmlDocument)
   {
	  $("#addressFinderError").addClass("hide");
	  var selectData = this.getselectStart();
	  var housenumbers=xmlDocument.getElementsByTagName("houseNumber");
      var housenames=xmlDocument.getElementsByTagName("houseName");
      var streetnames=xmlDocument.getElementsByTagName("street");
      var areanames=xmlDocument.getElementsByTagName("locality");
      var townnames=xmlDocument.getElementsByTagName("town");
      var countynames=xmlDocument.getElementsByTagName("county");
      var postcodes=xmlDocument.getElementsByTagName("postcode");
      var housename="";
      var housenumber="";
      var streetname="";
      var areaname="";
      var townname="";
      var countyname="";
      var postcode="";
	  for(var i=0; i<xmlDocument.getElementsByTagName('address').length; i++)
      {
         try{
         housenumber=housenumbers[i].childNodes[0].nodeValue;
         }catch(e) { housenumber=""; }
         try{
         housename=housenames[i].childNodes[0].nodeValue;
         }catch(e) { housename=""; }
         try{
         streetname=streetnames[i].childNodes[0].nodeValue;
         }catch(e) { streetname=""; }
         try{
         areaname=areanames[i].childNodes[0].nodeValue;
         }catch(e) {areaname=""; }
         try{
         townname=townnames[i].childNodes[0].nodeValue;
         }catch(e) {townname=""; }
         try{
         countyname=countynames[i].childNodes[0].nodeValue;
         }catch(e) {countyname=""; }
         try{
         postcode=postcodes[i].childNodes[0].nodeValue;
         }catch(e) {postcode=""; }
         selectData += this.getOptionData(housenumber,housename,streetname,areaname,townname,countyname, postcode);
      }
	  selectData += this.getselectEnd();
	  $("#addressList").html('<label for="mainAddressList">Select your address</label>'+selectData);
	  $("#addressList").removeClass("hide")
   },

   getselectStart:function() {
      return("<select id='mainAddressList' name='mainAddressList' class='addressList' size='1' onChange='AddressFinderHandler.fillAddress(this)'><option>Please select</option>");
   },

   getOptionData:function(housenumber,housename,streetname,areaname,townname,countyname, postcode)
   {
 	  buffer = new Array();
 	  buffer[buffer.length] =housenumber+" "+housename;
	  var comma="";
 	  if(StringUtils.isNotBlank(streetname)){
	     if(StringUtils.isNotBlank(housenumber) || StringUtils.isNotBlank(housename)){comma=", ";}
 	     buffer[buffer.length] = comma+streetname;}
 	  if(StringUtils.isNotBlank(areaname)){
      buffer[buffer.length] = ", "+areaname;}
 	  if(StringUtils.isNotBlank(townname)){
      buffer[buffer.length] = ", "+townname;}
 	  if(StringUtils.isNotBlank(countyname)){
      buffer[buffer.length] = ", "+countyname;}
 	  if(StringUtils.isNotBlank(postcode)){
      buffer[buffer.length] = ", "+postcode;}
 	  var addressOptions = buffer.join("");
      return("<option>"+addressOptions+"</option>");
   },

   getselectEnd:function()
   {
      return("</select>");
   },

   fillAddress:function(addressList)
   {
      var index=addressList.selectedIndex;
	  if(index == 0)
	     $("#currentAddressBlock").addClass("hide");
	  else
	  {
	     var selectedAddress = addressList.value.replace(/, /g,"<br/>");
	     $("#currentAddress").html(selectedAddress);
	     $("#currentAddressBlock").removeClass("hide");
	  }
   },

   manualAddress:function()
   {
      PaymentView.emptyTheField($("#leadPostcode"));
      $("#addressFinder").addClass("hide");
	  $("#addressList").addClass("hide");
	  $("#currentAddressBlock").addClass("hide");
	  $("#manualAddress").removeClass("hide");
   },

   clearAndStart:function()
   {
   	  PaymentView.emptyTheField($("#leadPostcode"));
      $("#addressList").addClass("hide");
	  $("#currentAddressBlock").addClass("hide");

   },

   changeAddress:function()
   {
      $("#currentAddressBlock").addClass("hide");
	  document.getElementById("mainAddressList").selectedIndex=0;
   }
};*/

var DepositTypeChangeHandler =
{
   /**
   * Function for setting the selected deposit value
   * into PaymentInfo object and update the values
   * in UI according to selection made.
   * @param selectedDepositType Selected deposit value, ex- LowDeposit, Deposit
   */
   handle:function(selectedDepositType)
   {
	 this.removeFocusOfPreviouslySelectedDepositType();
	 PaymentInfo.depositType = selectedDepositType;
	 this.focusSelectedDepositType();
	 this.updatePaymentSection();
   },

   /** This function updates the amount in the payment section to the selected deposit amount.**/
   updatePaymentSection: function()
	{
	 	if(PaymentInfo.partialPaymentAmount == null || PaymentInfo.partialPaymentAmount == undefined)
	 	   {
	 	      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.totalAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
	 	   }
	 	   else
	 	   {
	 	      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
	 	   }
	   var calCardCharge = cardChargeHandler.getApplicableCardCharge(PaymentInfo.selectedDepositAmount,"Credit");
	   PaymentInfo.totalCardCharge = calCardCharge;
	   var amtWithCardCharge = MathUtils.roundOff(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge , 2);
	   $("#spanAmountWithCardCharge").html(PaymentInfo.currencySymbol + amtWithCardCharge);

	   var debitCardCharge = cardChargeHandler.getApplicableCardCharge(PaymentInfo.selectedDepositAmount,"Debit");
	   $("#spanAmountWithoutCardCharge").html(PaymentInfo.currencySymbol + MathUtils.roundOff(1*PaymentInfo.selectedDepositAmount + 1*debitCardCharge , 2));
	   if(PaymentInfo.selectedCardType)
	   {
		 var cardCharge = cardChargeHandler.calculateCardCharge(PaymentInfo.selectedDepositAmount);
		 PaymentInfo.calculatedPayableAmount = MathUtils.roundOff(1*PaymentInfo.selectedDepositAmount + 1*cardCharge , 2);
	   }

	},

   /**
    * This function removes the focus of previously selected deposit type.
    */
   removeFocusOfPreviouslySelectedDepositType:function()
   {
	 $("#"+PaymentInfo.depositType).removeClass("selected");
   },

   /**
    * This function adds focus to the selected deposit type.
    */
   focusSelectedDepositType:function()
   {
      $("#"+PaymentInfo.depositType).addClass("selected");
   },

   /**
    * Refreshes the PaymentInfo. A central function responsible for
    * keeping data integrity of different amounts in
    * in this payment for client side validation.
    */
    updatePaymentInfo:function()
    {
 	if(PaymentInfo.partialPaymentAmount == null || PaymentInfo.partialPaymentAmount == undefined)
 	   {
 	      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.totalAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
 	   }
 	   else
 	   {
 	      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
 	   }
 	   calCardCharge = cardChargeHandler.calculateCardCharge(PaymentInfo.selectedDepositAmount);
 	   PaymentInfo.totalCardCharge = calCardCharge;
 	   PaymentInfo.calculatedPayableAmount = MathUtils.roundOff(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge , 2);
 	   PaymentInfo.calculatedTotalAmount = MathUtils.roundOff((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge , 2);
    }

};

var cardChargeHandler=
{
		/**
		 ** Calculates the card charges for the amount
		 ** passed in as a parameter.
		 ** @param amount Amount for which card charges to be calculated.
		 ** @return cardCharge Card charge for the amount sent in.
		 **/
		calculateCardCharge:function(amount)
		{
		     if (cardChargeMap.get(PaymentInfo.selectedCardType) == 0)
           {
              return 0.0;
           }
		     var cardChargeData = cardChargeMap.get(PaymentInfo.selectedCardType).split(",");
		     PaymentInfo.chargePercent = cardChargeData[0];
		     PaymentInfo.minCardChargeAmount = cardChargeData[1];
		     PaymentInfo.maxCardChargeAmount = cardChargeData[2];
		     var cardCharge = MathUtils.roundOff(((amount * PaymentInfo.chargePercent)/100),2);

		     if (PaymentInfo.minCardChargeAmount != null)
		     {
		       cardCharge = (cardCharge < 1*PaymentInfo.minCardChargeAmount) ? 1*PaymentInfo.minCardChargeAmount : cardCharge;
		     }

		     if (PaymentInfo.maxCardChargeAmount != null && PaymentInfo.maxCardChargeAmount != "")
		     {
		      cardCharge = (cardCharge > 1*PaymentInfo.maxCardChargeAmount) ? 1*PaymentInfo.maxCardChargeAmount : cardCharge;
		     }
		     return cardCharge;
		},

        getApplicableCardCharge:function(amount,chargeType)
        {
			var cardChargeDetails = "";
			if (chargeType == "Credit")
			{
				cardChargeDetails = creditCardChargeDetails;
			}
			else
			{
				cardChargeDetails = debitCardChargeDetails;
			}

			var cardChargeArr = cardChargeDetails.split(",");
          var cardCharge = MathUtils.roundOff(((amount * cardChargeArr[0])/100),2);

          if (cardChargeArr[1] != null)
          {
             cardCharge = (cardCharge < 1*cardChargeArr[1]) ? 1*cardChargeArr[1] : cardCharge;
          }

          if (cardChargeArr[2] != null && cardChargeArr[2] != "")
          {
           cardCharge = (cardCharge > 1*cardChargeArr[2]) ? 1*cardChargeArr[2] : cardCharge;
          }
         return cardCharge;
       }
};

var PromotionalCodeHandler=
{
    errorMessage : "",
    errorfieldId : "",
    setErrorMessage : function(errorMessage)
	{
	  this.errorMessage = errorMessage;
	},

    getErrorMessage : function()
	{
	  return this.errorMessage;
	},
    setErrorfieldId : function(errorfieldId)
	{
	  this.errorfieldId = errorfieldId;
	},
    getErrorfieldId : function()
	{
	  return this.errorfieldId;
	},

	handle:function()
    {
      return this.validatePromoCode();

    },

    validatePromoCode:function()
    {
       var promoCode = $("#discountCode").val();
       var promoCodeLength = promoCode.length;
       if((promoCodeLength > 8 && promoCodeLength < 21) || promoCodeLength == 0)
       {
          this.setErrorMessage("Please enter a valid Promotional Code");
    	  this.setErrorfieldId("promoCodeSection");
          return false;
       }
       else if(promoCodeLength == 21)
       {
		  var name = StringUtils.stripChars(($("#surName_0").val()).toString(),".'");
          var foreName = $("#foreName_0").val();
          //Use this pattern if promocode of length 21 requires first 17 as numeric and last 3 as alpha.
          //var Pat1 = /^[[0-9]{17}[a-zA-Z]{3}]*$/;
          var Pat1 = /^[0-9a-zA-Z]*$/;
          if (!Pat1.test(promoCode))
          {
			 this.setErrorMessage("Please enter a valid Promotional Code");
        	 this.setErrorfieldId("promoCodeSection");
             return false;
          }
       /*   else if(foreName == '' || foreName == null)
          {
        	 this.setErrorMessage("Please enter a First Name");
        	 this.setErrorfieldId("leadPassenger");
             return false;
          }
          else if(name == '' || name == null)
          {
        	 this.setErrorMessage("Please enter a Surname");
         	 this.setErrorfieldId("leadPassenger");
             return false;
          }
          else
          {
             var nameLen = name.length;
             var promoSurName = (promoCode).substring(17);
             if(nameLen < 3)
             {
                promoSurName = (promoCode).substring(17, 17+nameLen);
             }
             if((name).substring(0,3).toLowerCase() != promoSurName.toLowerCase())
             {
                this.setErrorMessage("Passenger Details do not match promotion code");
        	    this.setErrorfieldId("promoCodeSection");
                return false;
             }
          }*/
       }
	   else if(promoCode.length <= 8)
       {
          var Pat2 = /^[0-9]*$/;
          if (!Pat2.test(promoCode))
          {
        	  this.setErrorMessage("Please enter a valid Promotional Code");
         	  this.setErrorfieldId("promoCodeSection");
              return false;
          }
       }
       var balanceType = this.getBalanceType();
       if(newHoliday == "true")
       {
          var url="/cps/promCodeServlet?promotionalCode="+promoCode+"&balanceType="+balanceType+"&token="+token;
          url=uncache(url);
          url = url+tomcatInstance;
          var request = $.ajax({url: url, type:'get', async: false});
          return this.updatePromotionalCodeDiscount(request);
       }
       else
       {
          return false;
       }
    },

    getBalanceType:function()
    {
        var balanceType = PaymentInfo.depositType;
        if(StringUtils.equalsIgnoresCase(balanceType,'fullCost'))
         {
           balanceType='FullBalance';
         }
        return balanceType;
    },

    updatePromotionalCodeDiscount:function(request)
    {
    	if ((request.readyState == 4) && (request.status == 200))
    	   {
    	      var temp=request.responseText;
    	      temp=temp.split("|");
    	      var promCodeDiscount= temp[0];
    	      var totalAmount=temp[1];
    	      var promotionalCode=temp[2];
    	      if(parseFloat(promCodeDiscount))
    	      {
				 this.handlePromoCodeSuccess(promCodeDiscount, totalAmount, promotionalCode);
				 return true;
    	      }
    	      else
    	      {
				$('#isPromoCodeApplied').val('false');
    	    	this.setErrorMessage(promCodeDiscount);
         	    this.setErrorfieldId("promoCodeSection");
                return false;
    	      }
    	   }
    },

    handlePromoCodeSuccess:function(promCodeDiscount, totalAmount, promotionalCode)
    {
		$('#promoDiscount').val(promCodeDiscount);
        $('#isPromoCodeApplied').val('true');
        this.displayPromoDiscountInSummaryPanel(promCodeDiscount);
        this.updateAmountFields(totalAmount);
        this.setErrorMessage(promCodeDiscount);
    },

    displayPromoDiscountInSummaryPanel:function(promCodeDiscount)
    {
      $('#promoMain').removeClass('hide');
      $('#promoText').html('Promotional Discount');
  	  $('#promoText').css("display","block");
  	  $('#promoCodeDiscount').html('<strong>-&pound;'+ MathUtils.roundOff(1*promCodeDiscount,2) + '</strong>');
  	  $('#promoCodeDiscount').removeClass('hide');
    },

    updateAmountFields:function(totalAmount)
    {
    	PaymentInfo.totalAmount = totalAmount;
    	PaymentInfo.calculatedTotalAmount = totalAmount;
    	depositAmountsMap.put(bookingConstants.FULL_COST, totalAmount);
    	//TODO:Need to add the implementation for below.
    	//updateCardChargesInSummaryPanel(PaymentInfo.totalCardCharge);
    	this.updateAmountsInDepositSection(totalAmount);
    },

    updateAmountsInDepositSection:function(totalAmount)
    {
	   var applicableCardCharge = cardChargeHandler.getApplicableCardCharge(totalAmount,"Credit");
	   var totalamtwithapplicablecharge = MathUtils.roundOff(1*totalAmount+1*applicableCardCharge,2);
       $("#fullamt").html(MathUtils.roundOff(1*totalAmount,2));
	   $(".totalAmount").html(MathUtils.roundOff(1*totalAmount,2));
	   $("#applicablechargeforfullamt").html(applicableCardCharge);
	   $(".totalamtpayable").html(MathUtils.roundOff(1*totalAmount,2));
	   if($("#deposit"))
	   {
		 var depositAmt = 1*parseFloat(depositAmountsMap.get("deposit"));
         var remaingBalance = this.setCommaSeperated(MathUtils.roundOff(totalAmount - depositAmt, 2));
         $(".remaingBalance").html(remaingBalance);
		 var outstandingBalanceCardCharge = cardChargeHandler.getApplicableCardCharge(MathUtils.roundOff(totalAmount - depositAmt, 2), "Credit");
		 $(".outstandingBalanceCardCharge").html(outstandingBalanceCardCharge);
	   }
	   DepositTypeChangeHandler.updatePaymentSection();
    },

    formatDepositAmount:function(amountForFormatting)
    {
    	var amount = this.setCommaSeperated(amountForFormatting);
    	return amount;
    },

    setCommaSeperated:function(value)
    {
       if((MathUtils.roundOff(1*value,2)).length>6)
       {
          var valueLength = value.length;
          var count = (""+parseInt(value/1000)).length;
          value=parseInt(""+(value/1000))+","+value.substring(count, valueLength);
       }
       return value;
    }
};


var CardTypeChangeHandler =
{
/**
 ** Highlights the appropriate amount section and updates essential fields
**/
    handleCardSelection:function ()
    {
       if(newHoliday == 'true')
       {
    	   //var selectedCardValue = $("#cardType").val();
		   var selectedCardValue = document.getElementById("cardType").value;
    	   var selectedCardArray = selectedCardValue.split("|");
    	   PaymentInfo.selectedCardType = (selectedCardArray!=""&&selectedCardArray) ? selectedCardArray[0] : null;
		   CardTypeChangeHandler.handleIssueNumberSection(PaymentInfo.selectedCardType);
           CardTypeChangeHandler.updateSectionToBeHighlighted();
           CardTypeChangeHandler.updateButtonCaption();
           DepositTypeChangeHandler.updatePaymentInfo();
       }
    },

  /**hides/unhides the issue number section.*/
  handleIssueNumberSection:function(cardType)
  {
	  if (cardType=="SWITCH" || cardType=="SOLO")
	  {
          PaymentView.displayContainer("issueNumber",false)
	  }
	  else
	  {
          PaymentView.displayContainer("issueNumber",true);
	  }
  },


  /**
   ** Highlights the appropriate amount section based on the card charge applicability
   **/
  updateSectionToBeHighlighted:function()
  {
    if (PaymentInfo.selectedCardType)
	{
	  if(cardChargeMap.get(PaymentInfo.selectedCardType).split(",")[4] == "Credit")
       {
      	PaymentView.highlightContainer("amountWithCardCharge","selected",true)
      	PaymentView.highlightContainer("amountWithoutCardCharge","selected",false);
       }
      else
       {
      	PaymentView.highlightContainer("amountWithCardCharge","selected",false);
      	PaymentView.highlightContainer("amountWithoutCardCharge","selected",true);
       }
     }
	 else
	 {
	    PaymentView.highlightContainer("amountWithCardCharge","selected",false);
        PaymentView.highlightContainer("amountWithoutCardCharge","selected",false);
	 }
  },

	/**Updates the caption of the submit button based on 3DS*/
     updateButtonCaption:function()
	{

      var payButtonDescription = threeDCards.get(PaymentInfo.selectedCardType);
      if(payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup" || payButtonDescription == "americanexpressgroup")
     {
		  PaymentView.changeCaption("Proceed to payment");
     }
     else
     {
          PaymentView.changeCaption("Book your holiday");
	 }

	}
};

var Personaldetails =
{
	noOfPassengers : "",
	setNoOfPassengers : function(noOfPassengers)
	{
	this.noOfPassengers = noOfPassengers;
	},

	getNoOfPassengers : function()
	{
      return this.noOfPassengers;
	},

	/**
	 * This function copies the surname of the lead passenger to the
	 * rest of the passengers when the user checks 'same surname as lead passenger' check box.
	 */
	autoCompleteSurname : function(passengerCount)
	{
		if($("#sameSurname").attr('checked'))
		{
		  for(index=1; index < passengerCount ; index++)
		   {
			 $('#surName_'+index).val($('#surName_0').val());
			 if(!StringUtils.isBlank($('#surName_0').val()))
			 {
			    $('#surName_'+index).blur();
			 }
		   }
		}
	},

	/**
	 * This function unchecks the 'same surname as lead passenger' check box when the user
	 * changes the surname of other passengers.
	 */
	unCheck : function()
	{
	  if($("#sameSurname"))
	  {
	     $("#sameSurname").attr('checked', false);
	  }
	}
};

/**
 ** Responsible for showing/hiding the overlays
 **/
var stickyOverlay =
{
   /**
    ** Responsible for showing/hiding the 3D overlays
    **/
   threeDOverlay: function()
   {
      var overlayZIndex = 99;
      var zIndex = 100;
      var prevOverlay;
      var stickyOpened = false;
      jQuery("a.threeDSstickyOwner").click(function(e){
		 var overlay = "#" + this.id + "Overlay";
    	 if (!stickyOpened)
    	 {
    	    prevOverlay = overlay;
    	 }
    	 if (prevOverlay != overlay)
    	 {
    	    jQuery(prevOverlay).hide();
    	    stickyOpened = false;
    	 }
    	 var pos = jQuery("#"+this.id).offset();
    	 jQuery(overlay).show().removeClass('hide');
    	 prevOverlay = overlay;
    	 stickyOpened = true;
    	 jQuery(overlay + ".genericOverlay").css("z-index",zIndex);
    	 zIndex++;
    	 if (jQuery(overlay).parent(".overlay") != null){
    	    jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
    	    overlayZIndex++;
         }
         return false;
      });
      jQuery("a.close").click(function(){
         var overlay = this.id.replace("Close","Overlay");
         jQuery("#" + overlay).hide();
         return false;
      });
   },

   /**
    ** Responsible for showing/hiding the summary panel overlays
    **/
   summaryPanelOverlay :function()
   {
      var overlayZIndex = 99;
      var zIndex = 100;
      var prevOverlay;
      var stickyOpened = false;
	  // Don't remove this. Required for benefits overlay content.
	  jQuery('.summaryPanelMid-s li:last-child').css("padding", "8px 0");
	  jQuery('.summaryPanelMid-s li:last-child').css("background-image", "none");
      jQuery("a.stickyOwnerA").hover(function(){
		var overlay = "#" + this.id + "Overlay";
		jQuery(overlay).removeClass('hide').show();
	   },function(){
		var overlay = "#" + this.id + "Overlay";
		jQuery(overlay).addClass('hide').hide();
	  });
   }
};

var ImportantInformation=
{
   showDataProtectionNotice:function()
   {
      if ($("#dataProtectionNotice").hasClass("hide"))
	     $("#dataProtectionNotice").removeClass("hide");
	  else
	     $("#dataProtectionNotice").addClass("hide");
   },

   impInfoCheck:function()
   {
      if ($("#tourOperatorTermsAccepted").val() === 'false')
	     $("#tourOperatorTermsAccepted").attr('value',true);
	  else
	     $("#tourOperatorTermsAccepted").attr('value',false);
   },

   Popup : function(popURL,popW,popH,attr)
   {
      if (!popH) { popH = 350 }
      if (!popW) { popW = 600 }
      var winLeft = (screen.width-popW)/2;
      var winTop = (screen.height-popH-30)/2;
      var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;

      popupWin=window.open(popURL,"popupWindow",winProp);
      popupWin.window.focus()
   },

   PopupBookingConditions : function(popURL,popW,popH,attr)
   {
      if (!popH) { popH = 350 }
      if (!popW) { popW = 600 }
      var winLeft = (screen.width-popW)/2;
      var winTop = (screen.height-popH-30)/2;
      var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;

      popupWin=window.open(popURL,"bookingWindow",winProp);
      popupWin.window.focus()
   },

   updateFormElementFromCheckBoxMarketing : function(checkBoxObj, formElement)
   {
      if($("#" + checkBoxObj).attr('checked'))
         $("#" + formElement).val(false);
      else
         $("#" + formElement).val(true);
   }
};

/***This function shall populate the cv2avs section with the personal details section.*/
var AddressPopulationHandler =
{
   handle:function()
   {
      if(document.getElementById("useAddress").checked)
      {
	     $("#cardHouseName").val($("#houseName").val());
		 $("#cardAddress1").val($("#addressLine1").val());
		 $("#cardAddress2").val($("#addressLine2").val());
		 $("#cardTownCity").val($("#city").val());
		 $("#cardCounty").val($("#county").val());
		 $("#cardPostCode").val($("#postCode").val());

		 if(!StringUtils.isBlank($("#cardHouseName").val()))
		 {
            $("#cardHouseName").blur();
		 }

		 if(!StringUtils.isBlank($("#cardAddress1").val()))
		 {
            $("#cardAddress1").blur();
		 }

		 if(!StringUtils.isBlank($("#cardAddress2").val()))
		 {
		    $("#cardAddress2").blur();
		 }

		 if(!StringUtils.isBlank($("#cardTownCity").val()))
		 {
		    $("#cardTownCity").blur();
		 }

		 if(!StringUtils.isBlank($("#cardCounty").val()))
		 {
		    $("#cardCounty").blur();
		 }

		 if(!StringUtils.isBlank($("#cardPostCode").val()))
		 {
		    $("#cardPostCode").blur();
		 }

      }
   }
};

function openWindow(url)
{
	window.open(url,'blank');
	return false;
}

/** Function to redraw the specified section to avoid IE border breaking issue. */
function forceRedraw(){
   if ($.browser.msie){
      var element = $("#contentCol2")[0];
	  var emptyTextNode = document.createTextNode(' ');
	  element.appendChild(emptyTextNode);
	  emptyTextNode.parentNode.removeChild(emptyTextNode)
   }
}

/** Common implementation for client side on blur and submit validation error display */
function commonSubmitBlurErrorDisplay(args){
   var contentClass = $("." +args[0].attr("id")+ "Error");
   var id=args[0].attr("id")+ "Error";
   var topText = args[0].attr("alt");
   var topContainer = $("#topError");
   contentClass.addClass("formError");
   $("#errorSummary").removeClass("hide");
   if($("."+ id+" #"+id).length != 1)
   {
      if(id=="expiryDateMMError" || id=="expiryDateYYError")
      {
         if($("li ."+id+" p.formErrorMessage").length != 1)
	     {
	        $("<p class='formErrorMessage' id='"+id+"'>"+args[2] +"</p>").prependTo(contentClass);
		    $("<li id='"+args[0].attr("id")+"TopError'>"+topText+"</li>").appendTo(topContainer);
			forceRedraw();
	     }
	  }
	  else
	  {
	     if(id.indexOf("surName") != -1)
	     {
	        if($("li ."+id+" p.formErrorMessage").length == 1)
		    {
		       var pClass = $("." +args[0].attr("id")+ "Error p")
			   pClass.after($("<p class='formErrorMessage' id='"+id+"'>"+args[2] +"</p>"));
			}
			else
			{
			   $("<p class='formErrorMessage' id='"+id+"'>"+args[2] +"</p>").prependTo(contentClass);
			}
	     }
		 else
		    $("<p class='formErrorMessage' id='"+id+"'>"+args[2] +"</p>").prependTo(contentClass);
		 $("<li id='"+args[0].attr("id")+"TopError'>"+topText+"</li>").appendTo(topContainer);
		 forceRedraw();
	  }
   }
   $("#commonError").addClass("hide");
}