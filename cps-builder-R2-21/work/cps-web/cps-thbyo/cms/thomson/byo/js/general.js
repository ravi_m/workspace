
var ns4 = (document.layers);
var ie4 = (document.all && !$);
var ie5 = (document.all && $);
var ie6 = (navigator.userAgent.indexOf("MSIE 6.0")!=-1);
var ns6 = (!document.all && $);
var mac = (navigator.userAgent.indexOf("Mac") != -1);
var moz = (navigator.userAgent.indexOf("Netscape") == -1);
var popupWin = '';

function getElem(elemname) {
   // Explorer 4
   if(ie4) {
      return document.all[elemname]
   }
   // W3C - Explorer 5+ and Netscape 6+
   else if(ie5 || ns6) {
      return $(elemname)
   }
}

function getElemName(elemname) {
   return document.getElementsByName(elemname)
}

function Popup(popURL,popW,popH,attr)
{
   if (!popH) { popH = 350 }
   if (!popW) { popW = 600 }
   var winLeft = (screen.width-popW)/2;
   var winTop = (screen.height-popH-30)/2;
   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;

   popupWin=window.open(popURL,"popupWindow",winProp);
   popupWin.window.focus()
}

/* toCamelCase: example "font-size".toCamelCase() -> "fontSize" */
String.prototype.toCamelCase = function() {
   return this.replace(/-([a-z])/g, function(a, b) { return b.toUpperCase(); });
};

function currentStyle(el, property) {
   var d = document;
   var v = null;
   if (d.defaultView && d.defaultView.getComputedStyle) {
      var style = d.defaultView.getComputedStyle(el, null);
      v = style && style.getPropertyValue(property);
   } else if (el.currentStyle) {
      v = el.currentStyle[property.toCamelCase()];
   }
   if (v == null && el.style) {
      v = el.style[property.toCamelCase()];
   }
   return v;
}

function ShowDropDownList(show,obj) {

    if(obj == 'panel') {

       select_array=new Array ("ratingselect","accommodationType","selectrooms");}
   if (document.all && select_array.length>0) {
      for (var i=0; i<select_array.length; i++) {
        $(select_array[i]).style.visibility=(show==0)?"hidden":"visible";

      }
   }
}


//26164 change
function updateFormElementFromCheckBoxMarketing(checkBoxObj, formElement)
{
	if ($(formElement))
     {
	   if (checkBoxObj.checked)
         {
	      $(formElement).value = false;
	     }
	   else
         {
		  $(formElement).value = true;
		 }
     }
 }

    //Auto Completion of surnames
function autoCompletionSurname(passengerCountBasedOnRooms, startAutoCompletion)
{
   if($('autoCheck_'+startAutoCompletion).checked) {
      for(index=startAutoCompletion; index<(startAutoCompletion+passengerCountBasedOnRooms-1); index++)
      {
         if ($('lastname_'+index))
         {
         $('lastname_'+index).value = $('lastname_0').value;
      }
   }
}
}

function showHiddenInformation(elemId)
{
   div = $(elemId);
   if (currentStyle(div, 'display')=="block")
   {
   	  div.style.display = "none";
   }else{
      div.style.display = "block";
   }
}

function toggleBYOOverlay()
{
  var overlayZIndex = 99;
  var zIndex = 100;
  var prevOverlay;
  var stickyOpened = false;
  jQuery("a.stickyOwner").click(function(e){
    var overlay = "#" + this.id + "Overlay";
	if (!stickyOpened)
	{
		prevOverlay = overlay;
	}
	if (prevOverlay != overlay)
	{
		jQuery(prevOverlay).hide();
		stickyOpened = false;
	}
	var pos = jQuery("#"+this.id).offset();
	jQuery(overlay).show();
	prevOverlay = overlay;
	stickyOpened = true;
	jQuery(overlay + ".genericOverlay").css("z-index",zIndex);
	zIndex++;

	if (jQuery(overlay).parent(".overlay") != null){
	  jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
	  overlayZIndex++;
	}
    return false;
  });

  jQuery("a.close").click(function(){
    var overlay = this.id.replace("Close","Overlay");
    jQuery("#" + overlay).hide();
    return false;
  });
}