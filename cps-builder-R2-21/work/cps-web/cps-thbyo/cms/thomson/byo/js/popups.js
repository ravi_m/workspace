var bookingmode = false;

function editorial(actionname,section,topic,bookmark,isLegal) {

if (!bookmark || bookmark==null) bookmark=''
if (!topic || topic==null) topic=''
pageaction = (!isLegal)?'https://www.thomson.co.uk/po/' + actionname + '.do?':'https://www.thomson.co.uk/editorial/legal/insurance-' + actionname + '.html?'
if (bookingmode) {
	pageaction += 'popup=true'
	} else {
	pageaction += 'popup=false'
	}
if (section && section != '' && section != null) { pageaction += '&section=' + section }
if (topic && topic != '' && topic != null) { pageaction += '&topic=' + topic }
if (bookmark != '') {	pageaction += '#' + bookmark }
if (bookingmode) {
	Popup(pageaction,780,500,'scrollbars=1,resizable=1,left=25,top=25')
	} else {
	document.location.href = pageaction
	}
}

// Set Booking Mode
function BookingMode() {
 bookingmode = true
}

function popUpBookingConditions(accInv, flightInv, departureDate,clientDomainURL)
{
	var d = new Date();
	var currentDate = d.getDate();
	var currentMonth = d.getMonth();
	currentMonth++;
	currentDate = currentDate+"";
	currentMonth = currentMonth+"";
	if (currentDate.length == 1)
	{
		currentDate = "0"+currentDate;
	}

	if (currentMonth.length == 1)
	{
		currentMonth = "0"+currentMonth;
	}
		if (accInv=="ThomsonSun")
	{
		accInv="TRACS"
	}
	if (accInv =="HOPLA_THM" || accInv =="HOPLA_PEG")
	{
		accInv="HOPLA"
	}
	if (flightInv=="Amadeus")
	{
		flightInv = flightInv.toUpperCase();
	}
	if (flightInv=="TRACSA")
	{
		flightInv = "TRACS";
	}
	if (flightInv=="NAV")
	{
		flightInv="NAVITARE";
	}
	var formattedDate = d.getFullYear()+"-"+currentMonth +"-"+currentDate;

    var url=clientDomainURL+"/thomson/page/byo/tandc/tandc.page?"+"date="+formattedDate+"&"+"lang="+"en"+"&"+"depDate="+departureDate;

    if (accInv.length > 0)
    {
       url += "&accommInvSys=" + accInv;
    }
    if (flightInv.length > 0)
    {
       url += "&flightInvSys=" + flightInv;
    }

  //Check if T & c url is not empty
    if(tandCurl!="")
    {
      url = tandCurl;
    }
	window.open(url, "", "width=750,height=500,scrollbars=yes,resizable=yes");
}

function openPopupCreditCardPrivacy(link)
{
  Popup(link,795,595,'scrollbars=1,resizable=1');
  return false;
}