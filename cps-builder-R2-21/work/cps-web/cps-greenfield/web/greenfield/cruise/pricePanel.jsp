<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />

<table cellspacing="0" cellpadding="0" border="0" class="pricepanel">
    <tbody><tr>
      <th><h2>Price</h2></th>
    </tr>
    <tr>
      <td>
<div id="divPricePanel">
 <table cellspacing="0" cellpadding="0" border="0" class="costing">
  <tbody>
  <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
     <c:choose>
            <c:when test="${costingLine.itemDescription=='Total'}">
                <c:set var="TotalDescription" value="${costingLine.itemDescription }"/>
                  <fmt:formatNumber value="${costingLine.amount.amount}" var="TotalPrice" type="number" maxFractionDigits="2" minFractionDigits="2"/>
            </c:when>
            <c:when test="${costingLine.itemDescription=='On-line Booking Discount'}">
                <c:set var="OnLineDiscountDescription" value="${costingLine.itemDescription }"/>
                <c:set var="OnLineDiscountQuantity" value="${costingLine.quantity }"/>
                  <fmt:formatNumber value="${costingLine.amount.amount}" var="OnLineDiscountPrice" type="number" maxFractionDigits="2" minFractionDigits="2"/>
            </c:when>
      <c:when test="${costingLine.itemDescription=='Promotional Discount'}">
            <c:set var="promotionalDiscount" value="${costingLine.itemDescription }"/>
            <fmt:formatNumber value="${costingLine.amount.amount}" var="promotionalDiscountAmount" type="number" maxFractionDigits="2" minFractionDigits="2"/>
      </c:when>
        </c:choose>
      <c:if test="${costingLine.itemDescription!='Promotional Discount'}">
      <c:if test="${costingLine.itemDescription != 'Total' && costingLine.amount.amount != 0.0
       && costingLine.itemDescription != 'On-line Booking Discount'}">
        <tr>
                   <td>
            <fmt:formatNumber value="${costingLine.amount.amount}" var="linetotal" type="number" maxFractionDigits="2" minFractionDigits="2"/>
                        <span style="float: right;">&pound;<c:out value="${linetotal}"/></span>
                        <c:out value="${costingLine.itemDescription}"/>
          <c:if test="${costingLine.quantity != null}">
            (x<c:out value="${costingLine.quantity}"/>)
          </c:if>
           </td>
                </tr>
      </c:if>
      </c:if>
    </c:forEach>
    <c:if test="${TotalDescription == 'Total'}">
    <tr>
      <td>
        <span style="float: right;">&pound;<c:out value="${TotalPrice}"/></span>
                         Total
      </td>
    </tr>
    </c:if>
    <c:if test="${OnLineDiscountDescription == 'On-line Booking Discount'}">
    <tr>
      <td>
        <span style="float: right;">&pound;<c:out value="${OnLineDiscountPrice}"/></span>
        On-line Booking Discount
        <c:out value="${OnLineDiscountQuantity}"/>
        <c:if test="${OnLineDiscountQuantity != null}">
        (x<c:out value="${OnLineDiscountQuantity}"/>)
        </c:if>
      </td>
    </tr>
    </c:if>
     <tr id="cardChargeDiv" style="display:none;">
     <td>
     <span id="cardChargeAmount" style="float:right;margin-left: 53px;_margin-left: 0px;">
       <c:out escapeXml="false" value="${currency}"/>
     </span>
     <span>Credit card charge</span>
     </td>
     </tr>
   <c:if test="${fn:contains(promotionalDiscount,'Promotional')}">
    <tr>
      <td>
        <span style="float: right;">&pound;-<c:out value="${promotionalDiscountAmount}"/></span>
        <c:out value="${promotionalDiscount}"/>
      </td>
    </tr>
    </c:if>
      <tr id="promoDiscountDiv" style="display:none;">
        <td>
          <span id="promoCodeDiscount" style="float:right;margin-right: -62px;_margin-right: 0px">
      </span>
          <span id ="PromoDiscountText" style="display:none">Promotional Discount</span>
        </td>
      </tr>
    <tr>
           <td class="price" style="padding-top: 5px;">
              <span style="float: right;" id="totalAmount" class="total">
                 <fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}"
                    type="number" var="totalCostingLineWithoutDiscount" maxFractionDigits="2" minFractionDigits="2"/>
                 &pound;<c:out value="${totalCostingLineWithoutDiscount}"/>
              </span>Total
           </td>
          </tr>
  </tbody></table>
       </div>
      </td>
    </tr>
  </tbody>
</table>