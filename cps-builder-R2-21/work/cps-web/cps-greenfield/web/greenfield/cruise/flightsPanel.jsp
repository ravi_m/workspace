<%@include file="/common/commonTagLibs.jspf"%>

<c:if test="${not empty bookingComponent.flightSummary.outboundFlight}">
  <c:forEach var="outboundFlight" items="${bookingComponent.flightSummary.outboundFlight}">
    <c:if test="${not empty outboundFlight}">
      <c:set var="departureAirportName" value="${outboundFlight.departureAirportName}" />
      <c:set var="departureDateTime" value="${outboundFlight.departureDateTime}" />
      <c:set var="arrivalAirportName" value="${outboundFlight.arrivalAirportName}" />
      <c:set var="arrivalDateTime" value="${outboundFlight.arrivalDateTime}" />
    </c:if>
  </c:forEach>
</c:if>
<c:if test="${not empty bookingComponent.flightSummary.inboundFlight}">
  <c:forEach var="inboundFlight" items="${bookingComponent.flightSummary.inboundFlight}">
    <c:if test="${not empty inboundFlight}">
      <c:set var="inBoundFlightDepartureAirportName" value="${inboundFlight.departureAirportName}" />
      <c:set var="inBoundFlightDepartureDateTime" value="${inboundFlight.departureDateTime}" />
      <c:set var="inBoundFlightArrivalAirportName" value="${inboundFlight.arrivalAirportName}" />
      <c:set var="inBoundFlightArrivalDateTime" value="${inboundFlight.arrivalDateTime}" />
    </c:if>
  </c:forEach>
</c:if>
  <table cellspacing="0" cellpadding="0" border="0" class="pricepanel">
      <tbody><tr>
         <th><h2>Flights</h2></th>
      </tr>
      <tr>
         <td>
            <table cellspacing="2" cellpadding="0" border="0">
               <tbody><tr>
                  <th colspan="2">Out</th>
               </tr>
               <tr>
                  <td width="70">
                     <strong>Depart</strong>
                  </td>
                  <td>
               <c:out value="${departureAirportName}"/><br />
            <fmt:formatDate value="${departureDateTime}" pattern="dd/MM/yy"/>,
            <fmt:formatDate value="${departureDateTime}" type="TIME" pattern="HH:mm"/>
                  </td>
               </tr>
               <tr>
                  <td>
                     <strong>Arrive</strong>
                  </td>
                  <td>
            <c:out value="${arrivalAirportName}"/><br />
            <fmt:formatDate value="${arrivalDateTime}" pattern="dd/MM/yy"/>,
            <fmt:formatDate value="${arrivalDateTime}" type="TIME" pattern="HH:mm"/>

                  </td>
               </tr>
            </tbody></table>
         </td>
      </tr>
      <!-- If the itineraries is Aiport to Airport then the table tag is closed in the inboundTravelLeg instanceof FlightLegParticle block-->

            <tr>
               <td height="1" colspan="3">
                  <div style="border-top: 1px solid rgb(102, 153, 255); height: 1px; font-size: 0px;"/>
               </td>
            </tr>

      <tr>
         <td>
            <table cellspacing="2" cellpadding="0" border="0">
               <tbody><tr>
                  <th colspan="2">Return</th>
               </tr>
               <tr>
                  <td width="70">
                     <strong>Depart</strong>
                  </td>
                  <td>
            <c:out value="${inBoundFlightDepartureAirportName}"/><br />
            <fmt:formatDate value="${inBoundFlightDepartureDateTime}" pattern="dd/MM/yy"/>,
            <fmt:formatDate value="${inBoundFlightDepartureDateTime}" type="TIME" pattern="HH:mm"/>
                  </td>
               </tr>
               <tr>
                  <td>
                     <strong>Arrive</strong>
                  </td>
                  <td>
              <c:out value="${inBoundFlightArrivalAirportName}"/><br />
            <fmt:formatDate value="${inBoundFlightArrivalDateTime}" pattern="dd/MM/yy"/>,
            <fmt:formatDate value="${inBoundFlightArrivalDateTime}" type="TIME" pattern="HH:mm"/>

          </td>
               </tr>
            </tbody></table>
         </td>
      </tr>
      <!-- This table tag is the closing table tag for table tag opened in
         1. the outboundTravelLeg instanceof FlightLegParticle block when the itineraries is Airport - Airport or
         2. the outboundTravelLeg instanceof CruiseLegParticle block when the itineraties is Port - Airport
      -->
      </tbody></table>