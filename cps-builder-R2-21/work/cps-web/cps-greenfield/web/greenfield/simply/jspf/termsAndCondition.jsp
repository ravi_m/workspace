<%@ include file="/common/commonTagLibs.jspf"%>
<c:catch>
  <c:set var="tAndCDomain"  value="${bookingComponent.termsAndCondition.domain.clientApplicationName}" scope="page"/>
  <c:set var="relativeTAndCUrl" value="${bookingComponent.termsAndCondition.relativeTAndCUrl}" scope="page"/>
</c:catch>
<%
  String tAndCDomainKey = (String)pageContext.getAttribute("tAndCDomain");
  String tAndCDomainValue = com.tui.uk.config.ConfReader.getConfEntry(tAndCDomainKey+".TAndCUrl", "");
  tAndCDomainValue += (String)pageContext.getAttribute("relativeTAndCUrl");
  pageContext.setAttribute("tAndCDomainValue", tAndCDomainValue);
 %>
<!-- Terms and Conditions -->
<div class="bookingpanel">
  <h3>Terms &amp; Conditions</h3>
  <p>Please read our <a href="javascript:editorial('showPrivacy')">Privacy Policy</a> and <a href="javascript:showDataProtection()">Data Protection Notice</a>
  and confirm you agree to our use of your information provided above
  (which may in special situations include sensitive personal data) by
  checking the box below.</p>
  <div class="bookinginput">
    <p>
    <input name="privacyPolicyAccepted" tabindex="401" value="on" alt="You must read and accept our Privacy Policy before confirming your booking|Y|CHECK" type="checkbox"> I Agree
    <br>&nbsp;<br>
    <input name="leadPassengerContactDetails.ecommunicationsRequested" tabindex="405" value="on" checked="checked" style="margin: 10px 0px; float: left;" type="checkbox">
    If you would not like to receive information on the latest offers,
    discounts, products and services by e-communications from Simply Travel and
    other divisions within the TUI Travel group, please untick this box.
    </p>
  </div>
  <div id="data_protection" style="display: none;">
    <p>
      <strong>Data Protection Notice:</strong><br>
      We may from time to time contact you by post with further
      information on the latest offers, brochures, products or services which we believe
      may be of interest to you, from Simply Travel (a division of TUI UK Ltd), other holiday
      divisions within and group companies of TUI UK Ltd.
      <br>&nbsp;
      <br>
    </p>
    <div class="bookinginput">
      <p>
        <input name="leadPassengerContactDetails.noThirdPartyMarketingRequested" tabindex="406" value="on" style="margin: 7px 0px; float: left;" type="checkbox">
        Our business partners and carefully selected companies outside our holiday
        group would like to send you information about their products and services <strong>by
        post.</strong><br>
        If you <u>would not</u> like to hear from them, please tick this box.
      </p>
    </div>
  </div>
  <p>Please note that all members of your party require valid passports and any applicable visas before travelling.
  Visit the Foreign Office website at <a href="https://www.fco.gov.uk/travel" target="_blank">https://www.fco.gov.uk/travel</a>
  for full details of any travel restrictions, entry and stay
  requirements and visa and travel advise to your particular destination
  and the passport office website at <a href="https://www.passport.gov.uk/" target="_blank">https://www.passport.gov.uk </a>  for passport information.</p>
  <p>To view the notice summarising the liability rules applied by Community air carriers
  as required by Community legislation and the Montreal Convention, please view the
    <a href="javascript:editorial('showMontreal')">Air Passenger Notice</a>.</p>

 <c:choose>
	  	  <c:when test="${( bookingComponent.nonPaymentData['atol_flag'] != null ) &&( bookingComponent.nonPaymentData['atol_flag'] )}">
				  <c:if test="${( bookingComponent.nonPaymentData['atol_protected'] !=null ) &&( bookingComponent.nonPaymentData['atol_protected'] )}">
  <p>This booking is authorised under the ATOL licence of TUI UK Limited, ATOL 2524, and is protected under the ATOL scheme, as set out in the ATOL certificate to be supplied.</p>
				</c:if>
    </c:when>
</c:choose>
<p>You must also read and accept the Simply Travel Terms &amp; Conditions before we can process your booking.</p>
  <div class="bookinginput">
    <p>
    <input name="tourOperatorTermsAccepted" tabindex="407" value="on" alt="You must read and accept the Simply Travel Terms &amp; Conditions before confirming your holiday|Y|CHECK" type="checkbox">&nbsp;I accept&nbsp;the
    <a href="javascript:void Popup('<c:out value='${tAndCDomainValue}'  escapeXml='false'/>',600,450,'scrollbars=1')">
    Terms and Conditions of Simply Travel
    </a>
    </p>
  </div>
  <p>   By clicking <span id="confirmText">'pay'</span> you are confirming your booking and your card will be debited with the due amount.    All transactions are conducted over our secure server.
  Please note that cancellation/amendment changes are applicable once your holiday has been confirmed.
  <br></p>
</div>