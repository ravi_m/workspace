<%@ include file="/common/commonTagLibs.jspf"%>
<!-- Passenger Details -->
  <div class="bookingpanel">
  <c:set var="tabIndex" value="1" scope="page"/>

    <h3>Passenger Details</h3>
      <p>Please enter passenger names to match those shown on your passports.</p>
      <!-- Passenger details -->
      <div>
        <table border="0" cellpadding="0" cellspacing="0">
          <tbody>
          <tr>
                <td width="70"></td>
                <td width="55">Title</td>
                <td width="100">First Name</td>
                <td align="center" width="50">Initial</td>
                <td width="100">Surname</td>
                <td>&nbsp;</td>
              </tr>

              <c:set var="numberOfPassengers" />
              <c:set var="passengerCount" value="0" />
            <c:forEach var="passenger" items="${bookingComponent.passengerRoomSummary}">
              <c:forEach var="passengerRoom" items="${passenger.value}" varStatus="passen">
              <tr>
                  <c:set var="numberOfPassengers" value="${passen.index}"/>
                <td>
                  <c:out value="${passengerRoom.label}"/>
                </td>
                <c:set var="identifier" value="passenger_${passengerCount}_identifier"/>
                <input type = "hidden" name="<c:out value='${identifier}'/>" value="<c:out value='${passengerRoom.identifier}'/>"/>
                  <c:if test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR'}">
                  <c:if test = "${passengerCount == 0}">
                    <input name="leadPassenger" type="hidden"
                           value="<c:out value="${passengerRoom.identifier}"/>" />
                  </c:if>
                </c:if>
                <td>
                <c:set var="titlekey" value="personaldetails_${passengerCount}_title"/>
                <select name="${titlekey}" tabindex="${tabIndex}"><c:set var="tabIndex" value="${tabIndex+1}" scope="page"/>
                  <c:choose>
                    <c:when test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR' || passengerRoom.ageClassification == 'SENIOR2'}">
                      <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mr'}">selected</c:if>>Mr</option>
                      <option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mrs'}">selected</c:if>>Mrs</option>
                      <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
                      <option value="Ms" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Ms'}">selected</c:if>>Ms</option>
                      <option value="Dr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Dr'}">selected</c:if>>Dr</option>
                      <option value="Rev" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Rev'}">selected</c:if>>Rev</option>
                      <option value="Prof" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Prof'}">selected</c:if>>Prof</option>
                   </c:when>
                   <c:otherwise>
                     <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mr'}">selected</c:if>>Mr</option>
                     <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
                     <option value="Mstr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mstr'}">selected</c:if>>Mstr</option>
                      <option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mrs'}">selected</c:if>>Mrs</option>
                   </c:otherwise>
                 </c:choose>
               </select>
                </td>
                 <input type="hidden" name="country" value="United Kingdom" />
                <td nowrap="nowrap">
                  <c:set var="foreNameKey" value="personaldetails_${passengerCount}_foreName"/>
                  <input name="<c:out value='${foreNameKey}'/>" maxlength="15" size="15" tabindex="${tabIndex}" value="<c:out value='${bookingComponent.nonPaymentData[foreNameKey]}'/>" alt="name details as shown on your passport.|Y|NAME" type="text">&nbsp;<span class="price">*</span>
                  <c:set var="tabIndex" value="${tabIndex+1}" scope="page"/>
                </td>
                <td align="center">
                  <c:set var="middleNameKey" value="personaldetails_${passengerCount}_middleInitial"/>
                  <input name="<c:out value='${middleNameKey}'/>" maxlength="1" size="2" tabindex="${tabIndex}" value="<c:out value='${bookingComponent.nonPaymentData[middleNameKey]}'/>" alt="Initial|N|ALPHA" type="text">
                  <c:set var="tabIndex" value="${tabIndex+1}" scope="page"/>
                </td>
                <td nowrap="nowrap">
                  <c:set var="lastNameKey" value="personaldetails_${passengerCount}_surName"/>
                  <input name="<c:out value='${lastNameKey}'/>" maxlength="15" size="15" tabindex="${tabIndex}" value="<c:out value='${bookingComponent.nonPaymentData[lastNameKey]}'/>" id="lastname_<c:out value='${passengerCount}'/>" alt="name details as shown on your passport.|Y|NAME" type="text">&nbsp;<span class="price">*</span>
                  <c:set var="tabIndex" value="${tabIndex+1}" scope="page"/>
                </td>
                <td width="110" style="padding-top: 15px;">
                <c:if test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR'}">
                    <c:if test = "${passengerCount == 0}">
                       <strong>Lead Passenger</strong>
                       <strong>(Must be 18 years or over)</strong>
                    </c:if>
                </c:if>
                </td>
              </tr>
              <c:set var="passengerCount" value="${passengerCount+1}" />
            </c:forEach>
            </c:forEach>
          </tbody>
        </table>
      </div>
      <p>Please enter contact details for the lead passenger</p>
      <input name="deliveredToLeadPassenger" value="true" type="hidden">
      <div class="bookinginput">

          <table border="0" cellpadding="0" cellspacing="0">
            <tbody>
              <tr>
                <td nowrap="nowrap">House Name</td>
                <td>
                   <input name="houseName" maxlength="20" size="20" tabindex="98"
                      value="<c:out value="${bookingComponent.nonPaymentData['houseName']}"/>" id="HouseName"
                      alt="House Name|N|ADDRESS" type="text">
                </td>
                <td nowrap="nowrap">Daytime Phone No.</td>
                <td><input name="dayTimePhone" maxlength="23" size="15" tabindex="105" value="<c:out value="${bookingComponent.nonPaymentData['dayTimePhone']}"/>" alt="valid phone details|Y|PHONE" type="text">&nbsp;<span class="mandatory">*</span></td>
              </tr>
              <input alt="Please enter a house name or number|Y|EITHER_OR|HouseName|HouseNo" type="hidden">
              <tr>
                <td>House No.</td>
                <td><input name="houseNumber" maxlength="4" size="4" tabindex="99" value="<c:out value="${bookingComponent.nonPaymentData['houseNumber']}" />" id="HouseNo" alt="House No|N|ADDRESS" type="text"></td>
                <td nowrap="nowrap">Mobile Phone No.</td>
                <td><input name="mobilePhone" maxlength="23" size="15" tabindex="106" value="<c:out value="${bookingComponent.nonPaymentData['mobilePhone']}"/>" alt="valid phone details|N|PHONE" type="text">&nbsp;</td>
              </tr>
              <tr>
                <td>Address</td>
                <td nowrap="nowrap"><input name="addressLine1" id="AddressLine1" maxlength="25" size="25" tabindex="100" value="<c:out value="${bookingComponent.nonPaymentData['addressLine1']}"/>" alt="all required address fields|Y|ADDRESS" type="text">&nbsp;<span class="mandatory">*</span>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input name="addressLine2" id="AddressLine2" maxlength="25" size="25" tabindex="101" value="<c:out value="${bookingComponent.nonPaymentData['addressLine2']}"/>" alt="Address Line 2|N|ADDRESS" type="text"></td>
                <td>Email Address</td>
                <td nowrap="nowrap"><input name="emailAddress" maxlength="100" size="23" tabindex="107" value="<c:out value="${bookingComponent.nonPaymentData['emailAddress']}"/>" id="EmailAddress" alt="email address|Y|EMAIL" type="text">&nbsp;<span class="mandatory">*</span>&nbsp;</td>
              </tr>
              <tr>
                <td>Town/City&nbsp;</td>
                <td><input name="city" id="City" maxlength="25" size="25" tabindex="102" value="<c:out value="${bookingComponent.nonPaymentData['city']}"/>" alt="all required address fields|Y|ADDRESS" type="text">&nbsp;<span class="mandatory">*</span></td>
                <td valign="top">Re-enter Email Address</td>
                <td valign="top"><input name="emailAddressReconciliation" maxlength="100" size="23" tabindex="108" value="<c:out value="${bookingComponent.nonPaymentData['emailAddressReconciliation']}"/>" id="EmailAddress2" alt="your email address again|Y|EMAIL" type="text">&nbsp;<span class="mandatory">*</span></td>
              </tr>
              <tr>
                <td>County</td>
                <td><input name="county" id="County" maxlength="16" size="16" tabindex="103" value="<c:out value="${bookingComponent.nonPaymentData['county']}"/>" alt="all required address fields|N|ADDRESS" type="text"></td>
                <td valign="top">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Post Code</td>
                <td><input name="postCode" id="PostCode" maxlength="8" size="8" tabindex="104" value="<c:out value="${bookingComponent.nonPaymentData['postCode']}"/>" alt="Post Code|Y|POSTCODE" type="text">&nbsp;<span class="mandatory">*</span></td>
                <td colspan="2">&nbsp;</td>
                <input type="hidden" name="payment_0_selectedCountry" value="United Kingdom" />
              </tr>
            </tbody>
          </table>
        </div>
        <input alt="The email addresses entered do not match. Please re-enter your details|-|MATCH|EmailAddress|EmailAddress2" type="hidden">
      </div>
  <!-- / Passenger details -->