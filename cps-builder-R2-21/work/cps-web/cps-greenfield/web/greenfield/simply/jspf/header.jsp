<%@ include file="/common/commonTagLibs.jspf"%>
<!-- Header -->
<div id="header">
	<div id="headerwrap">
  	<p id="offertext">Extra savings when you book online</p>
    <table id="headermenu" border="0" cellpadding="0" cellspacing="1">
      <tbody><tr>
       <td id="home"><a href="javascript:linkcheck('/st/getHomePageSun.do')">Home</a></td>
       <td id="destinations"><a href="javascript:linkcheck('/st/sun/viewDestinations.do')">Our Destinations</a></td>
       <td id="specialoffers"><a href="javascript:linkcheck('/st/showSpecialOffers.do')">Offers</a></td>
       <td id="about"><a href="javascript:editorial('showAbout')">About Us</a></td>
       <td id="brochure"><a href="javascript:linkcheck('/st/showContent.do?content=brochurerequest&popup=false')">Brochure Request</a></td>
     <td id="property"><a href="javascript:linkcheck('/st/showContent.do?content=villasearch&popup=false')">Property Search</a></td>
      </tr>
    </tbody></table>
  </div>
  <img id="brandlogopopup" src="/cms-cps/greenfield/simply/images/stlogo.gif" alt="" border="0">
  <a href="javascript:linkcheck('/st/getHomePageSun.do')"><img id="brandlogo" src="/cms-cps/greenfield/simply/images/stlogo.gif" alt="" border="0"></a>
  <img id="brandimg" src="/cms-cps/greenfield/simply/images/text/hand_picked_properties.gif" alt="" border="0">
  <div id="headerbottom">&nbsp;</div>
</div>
<!-- / Header -->