<!-- Booking Step -->
  <div class="bookingstep">
    <table summary="" border="0" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
          <td>
            <p>&nbsp;&nbsp;Room Upgrade</p>
          </td>
          <td width="16"><img
          src="/cms-cps/greenfield/simply/images/booking_step_divider.gif"
          alt="" border="0"></td>
          <td>
            <p>Options</p>
          </td>
          <th width="16"><img
          src="/cms-cps/greenfield/simply/images/booking_step_divider_active.gif"
          alt="" border="0"></th>
          <th>
            <p>Customer Details &amp; Payment</p>
          </th>
          <th width="16"><img
          src="/cms-cps/greenfield/simply/images/booking_step_divider.gif"
          alt="" border="0"></th>
          <td>
            <p>Confirmation</p>
          </td>
          <td width="16"><img
          src="/cms-cps/greenfield/simply/images/booking_step_divider_end.gif"
          alt="" border="0"></td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- / Booking Step -->