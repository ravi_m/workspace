<title>Thomson.co.uk - Booking Step 3 - Personal Details and Payment</title>

   <link rel="stylesheet" type="text/css" href="/cms-cps/greenfield/beach/css/thomson_global.css" />
   <link rel="stylesheet" type="text/css" href="/cms-cps/greenfield/beach/css/footer_styles.css"  />
   <link rel="stylesheet" type="text/css" href="/cms-cps/greenfield/beach/css/thomson_booking.css" />
    <!-- New 3d secure css -->
	<link rel="stylesheet" type="text/css" href="/cms-cps/greenfield/beach/css/3dsec.css" />
	<link rel="stylesheet" type="text/css" href="/cms-cps/greenfield/beach/css/secure.css" />
	<!-- New 3d secure css -->
   <link rel="stylesheet" type="text/css" href="/cms-cps/greenfield/beach/css/print.css" media="print" />

   <link type="image/x-icon" href="/cms-cps/greenfield/common/images/favicon.ico" rel="icon"/>
   <link type="image/x-icon" href="/cms-cps/greenfield/common/images/favicon.ico" rel="shortcut icon"/>
   
	    
<script type="text/javascript" src="/cms-cps/greenfield/beach/js/print.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/dynamic_core.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/dynamic_price.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/formvalidation.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/personaldetailsandpayment.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/general.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/popups.js"></script>


   <script type="text/javascript" src="/cms-cps/greenfield/common/js/paymentUpdate.js"></script>

   <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
   <script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
   <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
   
   <style type="text/css">
  #poh_header_container { padding: 0px 5px 0px 5px; height: 62px; margin: 0px; } /* height should not exceed 62px due to menu sizing restrictions */
  #poh_header_table p { margin: 0px; padding: 0px; }

  a.poh_headlink, a.poh_headlink:link, a.poh_headlink:active, a.poh_headlink:visited, a.poh_headlink:hover { font: normal 10px verdana,arial,sans-serif; color: #fff; }
  a.poh_headlink:hover { color: #ef0000; }

  .po_header_logo { padding-top: 10px; padding-bottom: 10px; }
  .po_header_logo img { display: block; }
  #po_header_book_section { height: 15px; border-right: 1px solid #3366cc; margin-top: 32px; background-image: url(/cms-cps/greenfield/beach/images/icon_phone_17x13.gif); background-repeat: no-repeat; background-position: 0px 0px; }
  #po_header_book_section p { padding-left: 23px; font: bold 10px verdana,arial,sans-serif; color: #3366cc; }
  #po_header_register_section { margin-top: 32px; height: 15px; }
  #po_header_register_section p { padding-left: 15px; font: normal 10px verdana,arial,sans-serif; color: #3366cc; }

</style>