<%@include file="/common/commonTagLibs.jspf"%>

 <div id="accommodation">
<table class="pricepanel" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <th>
    <h2>Accommodation</h2>
    </th>
  </tr>
  <tr>
    <td>
    <table cellpadding="0" cellspacing="2" border="0">
      <tr>
        <th colspan="2">
        <div style="float: right;">
       <c:set var="rating" value="${bookingComponent.accommodationSummary.accommodation.rating}"/>
       <c:set var="additionalValue" value="${bookingComponent.accommodationSummary.accommodation.additionalValue}"/>
       <c:forEach begin="1" end="${rating}"><img src='/cms-cps/greenfield/beach/images/<c:out value="${bookingComponent.accommodationSummary.accommodation.ratingImage}"/>'/></c:forEach>
       <c:set var="img" value="${bookingComponent.accommodationSummary.accommodation.ratingImage}"/>
       <c:if test="${additionalValue}">
       <img src='/cms-cps/greenfield/beach/images/thomsonplus.gif' />
       </c:if>
     </div>
      <div style="text-align: left; margin-bottom: 7px"><c:out escapeXml="false" value='${bookingComponent.accommodationSummary.accommodation.hotel}'/>
         </div>
        </th>
      </tr>
      <tr>
        <td width="80"><strong>Duration</strong></td>
        <td><c:out value='${bookingComponent.accommodationSummary.duration}'/> nights</td>
      </tr>
      <tr>
        <td><strong>Board Basis</strong></td>
        <td><c:out escapeXml="false" value='${bookingComponent.accommodationSummary.accommodation.boardBasis}'/></td>
      </tr>
      <c:if test="${not empty bookingComponent.rooms }">
      <tr>
        <td><strong>Room Type:&nbsp;</strong> </td>
        <td>
          <c:out value="${bookingComponent.rooms[0].roomDescription}"/>
        </td>
      </tr>
    </c:if>
    </table>
    </td>

  </tr>
</table>

<table>
  <tr>
    <td>
    <p style="margin: 10px 0px 0px 10px">
    <a align="center" href="javascript:void Popup('<c:out value='${clientUrl}'/><c:out value='${bookingComponent.accommodationSummary.AToZUri}'/>',725,400,'scrollbars=yes');">A-Z Booking Information</a>
    </p>
    </td>
  </tr>

</table>
</div>
<script type="text/javascript">
   var bookingmode=true;
</script>



