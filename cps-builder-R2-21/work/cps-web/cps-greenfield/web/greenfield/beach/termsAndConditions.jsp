<%@ include file="/common/commonTagLibs.jspf"%>
<c:catch>
  <c:set var="tAndCDomain"  value="${bookingComponent.termsAndCondition.domain.clientApplicationName}" scope="page"/>
  <c:set var="relativeTAndCUrl" value="${bookingComponent.termsAndCondition.relativeTAndCUrl}" scope="page"/>
</c:catch>
<%
  String tAndCDomainKey = (String)pageContext.getAttribute("tAndCDomain");
  String tAndCDomainValue = com.tui.uk.config.ConfReader.getConfEntry(tAndCDomainKey+".TAndCUrl", "");
  tAndCDomainValue += (String)pageContext.getAttribute("relativeTAndCUrl");
  pageContext.setAttribute("tAndCDomainValue", tAndCDomainValue);
 %>
<div class="border">
      <!-- Terms and Conditions -->
      <h6>Terms & Conditions</h6>
      <p class="goldpanel marginfive">Please read our <a href="javascript:editorial('showPrivacy')">Privacy Policy</a> and <a href="javascript:showDataProtection()">Data Protection Notice</a> and confirm you agree to our use of your information provided above (which may in special situations include sensitive personal data) by checking the box below.
      <br/>
      <input type="checkbox" alt="You must read and accept our Privacy Policy before confirming your booking|Y|CHECK" value="on" tabindex="401" name="privacyPolicyAccepted"/>I Agree
      <br/> <br/>
      <input type="checkbox" class="iAgree" checked="checked" value="on" tabindex="405" name="leadPassengerContactDetails.ecommunicationsRequested"/>
     If you would not like to receive information on the latest offers, discounts, products and services by e-communications from thomson.co.uk, please untick this box.
      </p>
      <div style="display: none;" id="data_protection">
         <p class="goldpanel marginfive">
            <strong>Data Protection Notice:</strong><br/>
            We may from time to time contact you by post with further
            information on the latest offers, brochures, products or services which we believe
            may be of interest to you, from Thomson Holidays (a division of TUI UK Ltd), other holiday
            divisions within and group companies of TUI UK Ltd.
            <br/> <br/>
            <input type="checkbox" style="margin: 7px 0px; float: left;" value="on" tabindex="406" name="noThirdPartyMarketingRequested"/>
            Our business partners and carefully selected companies outside our holiday
            group would like to send you information about their products and services <strong>by
            post.</strong><br/>
            If you <u>would not</u> like to hear from them, please tick this box.
         </p>
      </div>
      <p>Please note that all members of your party require valid passports and any applicable visas before travelling.
      Visit the Foreign Office website at <a target="_blank" href="https://www.fco.gov.uk/travel">https://www.fco.gov.uk/travel</a> for full details of any travel restrictions, entry and stay requirements and visa and travel advise to your particular destination and the passport office website at <a target="_blank" href="https://www.passport.gov.uk ">https://www.passport.gov.uk </a>  for passport information.</p>
      <p>To view the notice summarising the liability rules applied by Community air carriers
      as required by Community legislation and the Montreal Convention, please view the
      <a href="javascript:editorial('showMontreal')">Air Passenger Notice</a>.</p>
	  <c:choose>
	  	  <c:when test="${( bookingComponent.nonPaymentData['atol_flag']  !=null) &&( bookingComponent.nonPaymentData['atol_flag'] )}">
				  <c:if test="${(bookingComponent.nonPaymentData['atol_protected'] !=null ) &&(bookingComponent.nonPaymentData['atol_protected'] )}">
						 <p>
						 This booking is authorised under the ATOL licence of TUI UK Limited, ATOL 2524, and is protected under the ATOL scheme, as set out in the ATOL certificate to be
						 supplied.</p>
					</c:if>
			</c:when>
 </c:choose>
      <p class="goldpanel marginfive">You must also read and accept the Thomson Holidays Terms & Conditions before we can process your booking.
      <br/>
      <input type="checkbox" alt="You must read and accept the Thomson Holidays Terms &amp; Conditions before confirming your holiday|Y|CHECK" value="on" tabindex="407" name="tourOperatorTermsAccepted"/> I accept the
      <a href="javascript:void Popup('<c:out value='${tAndCDomainValue}'  escapeXml='false'/>',600,400,'scrollbars=1');">
      Terms and Conditions.</a>
      </p>
      <p>By clicking <span id="confirmText">'pay'</span> you are confirming your booking and your card will be debited with the due amount. All transactions are deducted over our secure server. Please note that cancellation / amendment changes are applicable once your holiday has been confirmed.
<br/></p>
      </div>
    <br/>

    <c:if test="${bookingInfo.newHoliday}">
      <span id="confirmButton" style="float: right ! important; margin-right: 3px; margin-bottom: 5px;">
       <a class="Pay" target="" href="javascript:updateEssentialFields();javascript:ValidateForm(document.forms.paymentdetails);"><img border="0" title="Pay" alt="Pay" src="/cms-cps/greenfield/beach/images/pay.gif"/></a>
      </span>
    </c:if>

    <img class="marginlefttwo" border="0" align="absmiddle" title="Back" alt="Back" src="/cms-cps/greenfield/beach/images/icon_back.gif"/>
    <a href="<c:out value='${bookingComponent.clientDomainURL}'/>/th/beach/book/BackToExtraOptionsInsurance.do">Back</a>
    <img class="marginleftten" border="0" align="absmiddle" style="padding-bottom: 6px;" title="Print page" alt="Print Page" src="/cms-cps/greenfield/beach/images/icon_print.gif"/>
    <a href="javascript:window.print();">Print page</a>


