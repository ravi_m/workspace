<%@include file="/common/commonTagLibs.jspf"%>
<!-- Payment Amount -->
<div class="border">
      <h6>Payment Amount</h6>
      <table cellpadding="0" cellspacing="0" border="0">
         <tr>
            <th colspan="2">
            <input type="hidden" id="totalHolidayCostH" name="totalHolidayCostH" value='<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>'/>
            <input type="hidden" id="totalholidaycost" name="totalholidaycost" value='<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>'/>
            Your total holiday cost is&nbsp;<span class="total" id="TotalCostText"><c:out value="${bookingInfo.calculatedTotalAmount.amount}"/></span>*
               <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}" varStatus="vardepost">
                 <%--<c:if test="${depositComponent.depositType=='DEPOSIT' }">--%>
                 <c:if test="${empty bookingComponent.depositComponents}">
                    <strong>and full payment is due today.</strong><span style="display:none" id="FullCostText"></span>
                    <td height="23" style="visibility:hidden">
                       <input type="radio" tabindex="300" onclick="DynamicPriceUpdate(this.form, 'Payment')" value="FULL" />
                    </td>
                 </c:if>
               </c:forEach>
            </th>
         </tr>
 <c:if test="${not empty bookingComponent.depositComponents}">
             <tr>
                <th colspan="2">
                   The deposit below is due today, but you can choose to pay the full balance if you wish.
                   Please select your preferred payment amount:<br />&nbsp;
                </th>
             </tr>
         <!-- If deposit is available -->
      <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}" varStatus="vardepost">
          <c:set var="depositType" value="${depositComponent.depositType} " scope="page"/>
        <c:set var="deposit" scope="page" value="${depositComponent.depositAmount.amount}"/>
        <fmt:formatDate value="${depositComponent.depositDueDate}" var="depositDueDate" pattern="dd/MM/yy"/>
             <tr valign="top">
                <td colspan="2">
           <c:if test="${depositComponent.depositType == 'DEPOSIT'}">
                   <input type="radio" class="deposit" checked="checked" name="depositType" id="radioDeposit" onclick="handleDepositSelection('DEPOSIT')" value="DEPOSIT" />
                   <input type="hidden" id="depositH" value="<fmt:formatNumber value="${depositComponent.depositAmount.amount}"  type="number" maxFractionDigits="2"/>"/>
                   Deposit:&nbsp;<span class="price"><span id="DepositCostText" name="depositAmounts" class="depositAmount"><c:out value='${deposit}'/></span></span>*
                   <br />The outstanding balance of your holiday is
                   <strong><span id="OutStandingBalanceText">&pound;<c:out value='${depositComponent.outstandingBalance.amount}'/></span></strong>**
                   and is due on
                   <strong><c:out value='${depositDueDate}'/></strong>
          </c:if>
                </td>
             </tr>
      </c:forEach>
             <tr>
                <td colspan="2">
                    <input name="depositType" id="radioFullAmount" type="radio" class="deposit" tabindex="301" onclick="handleDepositSelection('fullCost')" value="fullCost" />
                   Full Balance:&nbsp;<span class="price"><span id="FullCostText" name="depositAmounts" class="depositAmount"><c:out value="${bookingInfo.calculatedTotalAmount.amount}"/></span></span>*
                </td>
             </tr>
</c:if>
      <!-- /If deposit is available -->
      </table>
          <p>* If you pay by credit card the charges will be updated in the price panel once you have selected your method of payment. To pay with Visa Electron please phone 0871 231 4691.</p></br>
      <p>** Please note if you choose to pay the outstanding balance by credit card, an additional credit card charge will apply at the time of payment.</p>
      </div>
<!-- / Payment Amount -->



