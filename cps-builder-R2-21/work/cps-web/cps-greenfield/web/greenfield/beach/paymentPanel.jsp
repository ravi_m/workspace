<%@include file="/common/commonTagLibs.jspf"%>
<%@ include file="breadcrumb.jsp" %>
<table class="bookingpanel bordertopnone outertable" cellspacing="0" cellpadding="0" border="0">
  <tbody>
    <tr>
    <td>
  <div id="pricediff1" class="pageinfotext" <c:if test="${not empty bookingComponent.errorMessage}">style="color:#3366CC;"</c:if><c:if test="${empty bookingComponent.errorMessage}"> style="display:none" </c:if>>
         <c:out value="${bookingComponent.errorMessage}"/>
  </div>
    </td>
    </tr>

    <tr>
      <td>
        <jsp:include page="passengerDetails.jsp"></jsp:include>
        <jsp:include page="discountCode.jsp"></jsp:include>
        <jsp:include page="cardDetails.jsp"></jsp:include>
        <jsp:include page="addressDetails.jsp"></jsp:include>
        <jsp:include page="paymentAmount.jsp"></jsp:include>
        <jsp:include page="termsAndConditions.jsp"></jsp:include>
      </td>
    </tr>
  </tbody>
</table>