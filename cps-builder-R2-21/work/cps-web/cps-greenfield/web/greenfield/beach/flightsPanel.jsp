<%@include file="/common/commonTagLibs.jspf"%>

<div id="flights">
<table class="pricepanel" cellpadding="0" cellspacing="0" border="0">
   <tr>
      <th><h2>Flights</h2></th>
   </tr>
   <tr>
      <td>
         <table cellpadding="0" cellspacing="2" border="0">
            <tr>
              <th colspan="2">Out</th>
            </tr>
            <tr>
               <td width="80">
                  <strong>Depart</strong>
               </td>
               <td>
          <c:if test="${not empty bookingComponent.flightSummary.outboundFlight}">
                <c:forEach var="outboundFlight" items="${bookingComponent.flightSummary.outboundFlight}">
                  <c:if test="${not empty outboundFlight}">
                    <c:out value="${outboundFlight.departureAirportName}" /><br/>
              <fmt:formatDate value="${outboundFlight.departureDateTime}" pattern="dd/MM/yy"/>,
              <fmt:formatDate value="${outboundFlight.departureDateTime}" type="TIME" pattern="HH:mm"/>
              </c:if>
                </c:forEach>
            </c:if>
               </td>
            </tr>
            <tr>
               <td>
                  <strong>Arrive</strong>
               </td>
               <td>
          <c:if test="${not empty bookingComponent.flightSummary.outboundFlight}">
                <c:forEach var="outboundFlight" items="${bookingComponent.flightSummary.outboundFlight}">
                  <c:if test="${not empty outboundFlight}">
                    <c:out value="${outboundFlight.arrivalAirportName}" /><br/>
              <fmt:formatDate value="${outboundFlight.arrivalDateTime}" pattern="dd/MM/yy"/>,
              <fmt:formatDate value="${outboundFlight.arrivalDateTime}" type="TIME" pattern="HH:mm"/>
              </c:if>
                </c:forEach>
            </c:if>
               </td>
            </tr>
            <tr>
               <td colspan="3" height="1">
                  <hr width="100%" color="#6699ff" size="1">
               </td>
            </tr>
            <tr>
               <th colspan="2">Return</th>
            </tr>
            <tr>
               <td>
                  <strong>Depart</strong>
               </td>
               <td>
          <c:if test="${not empty bookingComponent.flightSummary.inboundFlight}">
                <c:forEach var="inboundFlight" items="${bookingComponent.flightSummary.inboundFlight}">
                  <c:if test="${not empty inboundFlight}">
                    <c:out value="${inboundFlight.departureAirportName}" /><br/>
              <fmt:formatDate value="${inboundFlight.departureDateTime}" pattern="dd/MM/yy"/>,
              <fmt:formatDate value="${inboundFlight.departureDateTime}" type="TIME" pattern="HH:mm"/>
              </c:if>
                </c:forEach>
            </c:if>
               </td>
            </tr>
            <tr>
               <td>
                  <strong>Arrive</strong>
               </td>
                <td>
          <c:if test="${not empty bookingComponent.flightSummary.inboundFlight}">
                <c:forEach var="inboundFlight" items="${bookingComponent.flightSummary.inboundFlight}">
                  <c:if test="${not empty inboundFlight}">
                    <c:out value="${inboundFlight.arrivalAirportName}" /><br/>
              <fmt:formatDate value="${inboundFlight.arrivalDateTime}" pattern="dd/MM/yy"/>,
              <fmt:formatDate value="${inboundFlight.arrivalDateTime}" type="TIME" pattern="HH:mm"/>
              </c:if>
                </c:forEach>
            </c:if>
               </td>
            </tr>
         </table>
      </td>
   </tr>
</table>
</div>