var totCardCharge=0;

var indexForCCResponse;



/************************************Promotional code ajax call************************************/
/*
 * This function calls promCodeServlet to calculate
 * Promotional code discounts and gets calculated amount
 * and discount amount.
*/
function updatePromotionalCode()
{
      var name = "Promotional Code";
      var promCode = document.getElementById("promotionalCode").value;

      if(promCode == '')
      {
         return;
      }

      var balanceType = getBalanceType();
      balanceType = TrimSpaces(balanceType);
      if(newHoliday == "true")
      {
        var url="/cps/promCodeServlet?promotionalCode="+promCode+"&balanceType="+balanceType+"&token="+token;
        url=uncache(url);
        url = url+"&tomcatInstance="+tomcatInstance;
        var request = new Ajax.Request(url,{method:"GET", onSuccess:responseUpdatePromotionalCodeDiscount});
      }
      else
      {
        return false;
      }

}

function responseUpdatePromotionalCodeDiscount(request)
{
   if ((request.readyState == 4) && (request.status == 200))
   {
     var temp=request.responseText;
     temp=temp.split("|");
     var promCodeDiscount= temp[0];
     var totalAmount=temp[1];
     if(parseFloat(promCodeDiscount))
     {
         $('promoDiscount').value = promCodeDiscount;
         commonPromotionalDiscount('true', 'block', "-&pound;"+promCodeDiscount, totalAmount);
         displayPromoCodeSuccessMessage(promCodeDiscount);
     }
     else
     {
       commonPromotionalDiscount('false', 'none', 0, totalAmount);
       displayPromoCodeErrorMessage(promCodeDiscount);
     }
   }
}

/** The common methods used when a valid or invalid promotional code is applied.*/
function commonPromotionalDiscount(booleanValue, identifier, promoCodeDiscount, totalAmount)
{
   $('isPromoCodeApplied').value =booleanValue;
   displayPromoCodeInSummaryPanel(promoCodeDiscount, identifier);
   PaymentInfo.totalAmount = totalAmount;
   PaymentInfo.calculatedTotalAmount = totalAmount;
   depositAmountsMap.put(bookingConstants.FULL_COST, totalAmount);
   updatePaymentInfo();
   updateCardChargesInSummaryPanel(PaymentInfo.totalCardCharge);
   displayTotalAmounts(bookingConstants.TOTAL_CLASS);
   if($("fullCost"))
      displayFullCost(totalAmount);
   remainingHolidayCost();
   $("promotionalCode").focus();
}

function displayPromoCodeInSummaryPanel(promCodeDiscount,isDisplay)
{
  if(document.getElementById('promoCodeDiscount'))
  {
    if(document.getElementById('promoDiscountDiv'))
    {
       document.getElementById('promoDiscountDiv').style.display=isDisplay;
       document.getElementById('promoCodeDiscount').innerHTML= promCodeDiscount;
       document.getElementById('promoCodeDiscount').style.display=isDisplay;
       document.getElementById('PromoDiscountText').style.display=isDisplay;
    }

    if(document.getElementById('promoText'))
    {
    document.getElementById('promoText').innerHTML = "Promotional Discount";
    document.getElementById('promoText').style.display=isDisplay;
    }
  }
}

/*Displays error message in payment page when promo code discount is not available
*
*/
function displayPromoCodeErrorMessage(promCodeDiscount)
{
   document.getElementById('pricediff1').style.display='block';
   document.getElementById('pricediff1').innerHTML= promCodeDiscount;
   alert(promCodeDiscount);
}

/* Displays promotional success message. */
function displayPromoCodeSuccessMessage(promCodeDiscount)
{
   var divElement = $('pricediff1');
   if (divElement.innerHTML != "" && (divElement.innerHTML.indexOf('Promotional') != -1 || divElement.innerHTML.indexOf('PROMOTIONAL') != -1 || divElement.innerHTML.indexOf('promotional') != -1))
   {
      $('pricediff1').innerHTML= "";
      $('pricediff1').style.display='none';
   }
}

function clearDiv(id)
{
   if($(id).innerHTML != "")
   {
      $(id).innerHTML="";
      $(id).style.display='none';
   }
}

/////////////////BROWSER UNCACHE////////////////////////////////
/*
 * Do not allow browser to cache request by appending time stamp
*/

function uncache(url)
{
   var d = new Date();
   var time = d.getTime();
   var newUrl = url + "&time="+time;
   return newUrl;
}
/****************************************End of AJAX calls*************************************************/
/********************************************************************************************************/
/********************************************************************************************************/
//////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////FUNCTIONS RELATED TO UPDATING PAYMENT PAGE//////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////functions related to Updating Summary panel ////////////////////
/** This function is responsible for updating totalAmount in summary panel
*   @param totalAmount -totalamount to be updated in summary panel
*/
function updateTotalAmtInSummaryPanel(totalAmount)
{
   if (document.getElementById('totalAmount'))
   {
     if((parseFloat(totalAmount).toFixed(2)).length>6)
     {
       var totalAmountLength = totalAmount.length;
       var count = (""+parseInt(totalAmount/1000)).length;
       totalAmount=parseInt(""+(totalAmount/1000))+","+totalAmount.substring(count, totalAmountLength);
      }
    document.getElementById('totalAmount').innerHTML = "&pound;"+totalAmount;
   }
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////Card selection related functions//////////////////////////////////////
function collectPayment(index)
{
  updateAllCardCharges(index);
}


function issueNoDisplay(displayVal)
{
  document.getElementById("IssueNumberTitle").style.display=displayVal;
  document.getElementById("IssueNumber").style.display=displayVal;
}


//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////Getter functions/////////////////////////////////////////////////

/* Returns card type e.g- MASTERCARD, AMERICAN_EXPRESS etc
*/
function getSelectedCardType()
{

 // Find currently selected card
    cardType = document.getElementById( "payment_type_0");

   if(cardType != null)
   {

    var selectedIndex = cardType.selectedIndex;

  // Get array item corresponding to drop-down selection, otherwise element 0
    if(selectedIndex > 0)
    {
       return cardType.value.split("|")[0];
    }
    else
    {
      return "";
    }
   }

    return "";

}

/*Returns payment method e.g- Dcard, Card etc
*/
function getPaymentMethod()
{
  if(document.getElementById('payment_type_0').value.split("|"))
  {
     return document.getElementById('payment_type_0').value.split("|")[1];
  }
  else
  {
    return null;
  }
}

/* returns card type e.g- MASTERCARD, AMERICAN_EXPRESS etc */
function getPaymentCode()
{
  return(getSelectedCardType());
}


/* Function to get deposit type
*
*/
function getBalanceType()
{
   var bal=document.getElementsByName("depositType");
   var balanceTypeText='';
   var balanceType='';
   if(bal.length&&bal.length>0)
   {
      for(i=0;i<bal.length;i++)
      {

         if(bal[i].checked==true)
         {
            balanceType=bal[i].value;
         }
     }
      if(balanceTypeText=='full')
      {
        balanceType='FullBalance';
      }
   }//End if(outer if)
   else
   {
       balanceType='FullBalance';
   }
   return  balanceType;
}

function getSelectedDeposit()
{
   var bal=document.getElementsByName("depositType");

     if(bal.length&&bal.length>0)
   {
     for(i=0;i<bal.length;i++)
     {
        if(bal[i].checked==true)
        {
           return bal[i].value;
        }
     }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////// Deposit section related functions////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
function onLoadSetUp()
{
  depositSelectionHandler();
}

function depositSelectionHandler()
{
  var depositsTypes = document.getElementsByName("depositType");
  var depositValue;
    if(depositsTypes && depositsTypes !=undefined && depositsTypes !=null)
    {
      for(var i=0;i<depositsTypes.length; i++)
      {
         if(depositsTypes[i].checked)
         {
           depositValue = depositsTypes[i].value;
         }
      }
    }
  if (newHoliday == "true")
  {
     updateTransAmtForDepositAmountChange(depositValue);
  }
  else
  {
       return false;
  }
}

/**Refresh to default when page loads etc ***/
function setToDefaultSelection()
{
  if(document.getElementById('payment_type_0') != undefined)
  {
     document.getElementById('payment_type_0').selectedIndex = 0;
  }
  if(document.getElementById('ExpiryYear') != undefined)
  {
    document.getElementById('ExpiryYear').selectedIndex = 0;
  }
  if(document.getElementById('payment_0_expiryMonthId') != undefined)
  {
    document.getElementById('payment_0_expiryMonthId').selectedIndex = 0;
  }
  if(document.getElementById('payment_0_cardNumberId') != undefined)
  {
    document.getElementById('payment_0_cardNumberId').value="";
  }
  if(document.getElementById('payment_0_nameOnCardId') != undefined)
  {
    document.getElementById('payment_0_nameOnCardId').value="";
  }
  if(document.getElementById('payment_0_postCodeId') != undefined)
  {
    document.getElementById('payment_0_postCodeId').value="";
  }
  if(document.getElementById('payment_0_securityCodeId') != undefined)
  {
    document.getElementById('payment_0_securityCodeId').value="";
  }
  if(document.getElementById('issueNumberInput') != undefined)
  {
   document.getElementById('issueNumberInput').value="";
  }
  if(document.getElementById('StartYear') != undefined)
  {
   document.getElementById('StartYear').selectedIndex = 0;
  }
  if(document.getElementById('payment_0_startMonthId') != undefined)
  {
   document.getElementById('payment_0_startMonthId').selectedIndex = 0;
  }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////Essential Fields related functions/////////////////////////////

/**Reset to default when page loads etc ***/
function resetEssentialFields(calculatedPayableAmount)
{
  document.getElementById("payment_0_chargeIdAmount").value = 0;
  document.getElementById("payment_0_transamt").value = 0 ;
  document.getElementById("payment_totamtpaid").value = calculatedPayableAmount;
}

/* Updates essential fileds when user clicks submit button.
* Holds essential information for cps server to process
*/
function updateEssentialFields()
{
  var val =  getTransactionAmountWithOutcardCharge(0);
  document.getElementById("payment_0_transamt").value = stripChars(val,String.fromCharCode(163)+''+','+' '+'�');
}

/** Function to calculate transaction amount without card charge.
* When multiple payment selections are allowed transaction amount is taken from transamt field
* When only one payment is allowed transaction amount is taken from hidden fields
*
* @params index- Index of the transaction.
*/

function getTransactionAmountWithOutcardCharge(index)
{
   var totamtWithoutCardCharge=0;
   //If deposit payments are not applicable
   totamtWithoutCardCharge = document.getElementById('totamtpaidWithoutCardCharge').value;

   //When deposit is applicable
   if(document.getElementById('radioDeposit') && document.getElementById('radioDeposit').checked )
   {
     totamtWithoutCardCharge = document.getElementById('depositH').value;
   }
   //When full deposit is applicable
   else if(document.getElementById('radioFullAmount') && document.getElementById('radioFullAmount').checked)
   {
      totamtWithoutCardCharge = document.getElementById('totalHolidayCostH').value;
   }
   else
   {
      totamtWithoutCardCharge = document.getElementById('totalHolidayCostH').value;
   }

   totamtWithoutCardCharge = stripChars(totamtWithoutCardCharge,String.fromCharCode(163)+''+','+' '+'�');
   return totamtWithoutCardCharge;
}


function refreshEssentialFields()
{
  updatePaymentChargeIdAmount(0, 0);
  updateEssentialFields();
}


/* Updates card charge in chargeIdAmount
* @param index- index of this transaction
* @param cardChargeAmt - card Charge to be updated
*/
function updatePaymentChargeIdAmount(index, cardChargeAmt)
{
   //Update hidden field
   if(document.getElementById('payment_'+index+'_chargeIdAmount'))
   {
      document.getElementById('payment_'+index+'_chargeIdAmount').value = cardChargeAmt;
   }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////util functions////////////////////////////////////////////////

function setInnerHTMLValue(fieldId,value)
{
  if(document.getElementById(fieldId))
  {
    document.getElementById(fieldId).innerHTML = value;
  }
}

/** This function is responsible for  Displaying card charges and transactionAmount in payment panel
*  @param index -represents index of the transaction
*         cardChargeAmt -represents cardChargeAmt to be displayed for the transaction
*         transactionAmount -represents transactionAmount to be displayed for the transaction
*/
function updateCardChargesAndTransAmount(index,cardChargeAmt,transactionAmount)
{
   //Code to display card charge and transaction amount in payment panel
   if (document.getElementById('payment_'+index+'_chargeIdAmount'))
   {
      //var maximumcardcharges =  parseFloat(document.getElementById("applicablecapvalue").value);
       //cardChargeAmt = parseFloat(cardChargeAmt).toFixed(2);
       cardChargeAmt = roundOff(parseFloat(cardChargeAmt),2);
       roundOff
      if(cardChargeAmt<0)
      {
        //If card charge amount is -ve, we need absolute value
        cardChargeAmt = Math.abs(cardChargeAmt);
      }

      //Update card charge in chargeIdAmount field
      updatePaymentChargeIdAmount(index, cardChargeAmt);

      //Display card charge if applicable
      showCardChargeInPaymentPanel(cardChargeAmt);

   }
}

/* Displays card charge in payment panel if card charge has to be shown
* @param index- index of this transaction
* @param cardChargeAmt - card Charge to be displayed in payment panel
*/
function showCardChargeInPaymentPanel(index, cardChargeAmt)
{
 if( document.getElementById('cardChargePercentage'+index))
 {
  //Display on payment panel
  document.getElementById('cardChargePercentage'+index).innerHTML = poundsymbol+cardChargeAmt;
 }
}

/* function to update amounts with card charge in deposit section.
*
*@param cardCharge
*
*/
function updateDepositOptionsBycardCharges(totalAmount,deposit,fullCost)
{
   totalAmount = setCommaSeperated(totalAmount);
     setInnerHTMLValue("TotalCostText", "&pound;"+totalAmount);

     deposit = setCommaSeperated(deposit);
     setInnerHTMLValue("DepositCostText","&pound;"+deposit);

     fullCost = setCommaSeperated(fullCost);
     setInnerHTMLValue("FullCostText","&pound;"+fullCost);


    var totalHolidayCost = (1*stripChars(document.getElementById("TotalCostText").innerHTML,String.fromCharCode(163)+''+','+' '+'�'));
    var depositAmount = (1*stripChars(document.getElementById("DepositCostText").innerHTML,String.fromCharCode(163)+''+','+' '+'�'));
    var OutStandingBalance = totalHolidayCost - depositAmount;
    //OutStandingBalance = "&pound;"+ parseFloat(OutStandingBalance).toFixed(2);
    OutStandingBalance = "&pound;"+ roundOff(parseFloat(OutStandingBalance),2);
    setInnerHTMLValue("OutStandingBalanceText",OutStandingBalance);

}

function contactUsPop() {
  if(bookingmode){
    window.open('http://www.thomson.co.uk/po/showContent.do?content=contact_us.htm','blank','width=805,height=685,toolbar =yes,scrollbars=yes,resizable=yes');
  } else {
    document.location.href='http://www.thomson.co.uk/po/showContent.do?content=contact_us.htm';
  }
}

function aboutUsPop() {
  if(bookingmode){
    window.open('http://www.thomson.co.uk/po/showContent.do?content=about_us.htm','blank','width=805,height=685,toolbar =yes,scrollbars=yes,resizable=yes');
  } else {
    document.location.href='http://www.thomson.co.uk/po/showContent.do?content=about_us.htm';
  }
}

function termsUsPop() {
  if(bookingmode){
    window.open('http://www.thomson.co.uk/po/showContent.do?content=terms_conditions.htm','blank','width=805,height=685,toolbar =yes,scrollbars=yes,resizable=yes');
  } else {
    document.location.href='http://www.thomson.co.uk/po/showContent.do?content=terms_conditions.htm';
  }
}

function helpPagesPop() {
  if(bookingmode){
    window.open('http://www.thomson.co.uk/po/showContent.do?content=help.html','_blank');
  } else {
    document.location.href='http://www.thomson.co.uk/po/showContent.do?content=help.html';
  }
}
function privacyPop() {
  if(bookingmode){
    window.open('http://www.thomson.co.uk/po/showContent.do?content=privacy.htm','_blank','width=805,height=685,toolbar =yes,scrollbars=yes,resizable=yes');
  } else {
    document.location.href='http://www.thomson.co.uk/po/showContent.do?content=privacy.htm';
  }
}

function creditCardChargesPop() {
  if(bookingmode){
    window.open('http://www.thomson.co.uk/editorial/legal/credit-card-payments.html','blank','width=805,height=685,toolbar =yes,scrollbars=yes,resizable=yes');
  } else {
    document.location.href='http://www.thomson.co.uk/editorial/legal/credit-card-payments.html';
  }
}

function setCommaSeperated(value)
{
   if((parseFloat(value).toFixed(2)).length>6)
   {
      var valueLength = value.length;
      var count = (""+parseInt(value/1000)).length;
      value=parseInt(""+(value/1000))+","+value.substring(count, valueLength);
   }
   return value;
}

/****************************Non-ajaxs Functions for updating the deposit and card charges****************/
function displayFieldsWithValuesForDeposit()
{
   displayTotalAmounts(bookingConstants.TOTAL_CLASS);
   //Following function is an existing brand specific function for displaying card charges in summary panel.
   updateCardChargesInSummaryPanel(PaymentInfo.totalCardCharge);
}

/** This function is responsible for updating card charges in summary panel
*   @params cardCharge -represents cardChargeAmt to be displayed for the transaction
*/
function updateCardChargesInSummaryPanel(cardCharge)
{
  if (cardCharge > 0)
  {
    if (document.getElementById('cardChargeDiv'))
    {
      document.getElementById('cardChargeDiv').style.display = 'block';
      document.getElementById('cardChargeAmount').style.display = 'block';
      //document.getElementById('cardChargeAmount').innerHTML = "&pound;"+parseFloat(1*cardCharge).toFixed(2);
      document.getElementById('cardChargeAmount').innerHTML = "&pound;"+roundOff(parseFloat(1*cardCharge),2);

      if (document.getElementById('cardChargeText'))
      {
        document.getElementById('cardChargeText').style.display = 'block';
      }
    }
  }
  else
  {
    document.getElementById('cardChargeDiv').style.display = 'none';
  }
}

function displayFieldsWithValuesForCard()
{
   displayCardRelatedFields();
   displayAllDepositOptionsWithCardCharge();
   displayTotalAmounts(bookingConstants.TOTAL_CLASS);
   displayPayableAmounts("flightPayableAmount");
   //Following function is an existing brand specific function for displaying card charges in summary panel.
   updateCardChargesInSummaryPanel(PaymentInfo.totalCardCharge);

}
/** This method is called on change of the select box. */
function displayCardRelatedFields()
{
   // Show or hide issue number field
   $("payment_0_paymenttypecode").value=PaymentInfo.selectedCardType;

  if(document.getElementById(PaymentInfo.selectedCardType+'_issueNo') &&
      document.getElementById(PaymentInfo.selectedCardType+'_issueNo').value == 'true')
  {
      document.getElementById("IssueNumberTitle").style.display='block';
      document.getElementById("IssueNumber").style.display='block';
  }
  else
  {
      document.getElementById("IssueNumberTitle").style.display='none';
      document.getElementById("IssueNumber").style.display='none';
  }
  if(PaymentInfo.selectedCardType == 'Switch/Maestro')
  {
     document.getElementById("payment_0_paymenttypecode").value = 'SWITCH';
  }
  if(PaymentInfo.selectedCardType == "PleaseSelect")
  {
    return;
  }
  else
  {
    var securityNumLen = $(PaymentInfo.selectedCardType+'_securityCodeLength').value;
  }
}

/** Function for updating payable amount based on the
 ** style class applied to it.
**/
function displayPayableAmounts(payableAmountClassName)
{
   //Display the total amounts with calculated total amount.
   var payableAmts = $$('.'+payableAmountClassName);
   for(var i=0; i<payableAmts.length; i++)
   {
       if (payableAmts[i].innerHTML.indexOf(currencySymbol) != -1)
        payableAmts[i].innerHTML = currencySymbol + formatDepositAmount(PaymentInfo.calculatedPayableAmount);
      else
        payableAmts[i].innerHTML = formatDepositAmount(PaymentInfo.calculatedPayableAmount);
   }
}
/*Update the amount payable based on deposit radio button selection */
function initializeDepositSelection()
{
  var depositsTypes = document.getElementsByName("depositType");
  if(depositsTypes && depositsTypes != undefined && depositsTypes != null)
  {
    for(var i=0;i<depositsTypes.length; i++)
    {
       if(depositsTypes[i].checked)
       {
         PaymentInfo.depositType = trimSpaces(depositsTypes[i].value);
       }
    }
  }
}
function formatDepositAmount(amountForFormatting)
{
  var amount = setCommaSeperated(amountForFormatting);
  return amount;
}

function setCommaSeperated(value)
{
   if((parseFloat(value).toFixed(2)).length>6)
   {
      var valueLength = value.length;
      var count = (""+parseInt(value/1000)).length;
      value=parseInt(""+(value/1000))+","+value.substring(count, valueLength);
   }
   return value;
}
function displayFullCost(amount)
{
   var cardCharge = calculateCardCharges(amount);
   //var fullCost = parseFloat(1*amount+1*cardCharge).toFixed(2);
   var fullCost = roundOff(parseFloat(1*amount+1*cardCharge),2);

   $("fullCost").innerHTML = formatDepositAmount(fullCost);
   depositAmountsMap.put(bookingConstants.FULL_COST, amount);
}

/**
 * This function is used to update the remaining holiday cost.
 */
function remainingHolidayCost()
{
   var totalamount = PaymentInfo.totalAmount;
   if($("DepositCostText"))
   {
      var depositAmount = (1*stripChars(document.getElementById("DepositCostText").innerHTML,String.fromCharCode(163)+''+','+' '+'�'));
      var remaingBalance = setCommaSeperated(roundOff(totalamount - depositAmount, 2));
      setInnerHTMLValue("OutStandingBalanceText",remaingBalance);
   }
}