// Dynamic Price Panel

function DynamicPriceUpdate(form, PageName) {
 Params = GenerateRequest(form);
 panelheight = getElem('divPricePanel').offsetHeight;
 getElem('divPricePanel').innerHTML = '<table style="height:'+panelheight+'px" border="0" cellpadding="0" cellspacing="0"><tr><td style="text-align:center;vertical-align:middle">Recalculating...</td></tr></table>'
 // Identify Page and select appropriate action
 if (PageName) {
    if(PageName == 'Payment') {
       LoadDIV(Params,'divPricePanel','UpdateCostText()','','/th/'+ ProductType +'/book/UpdateCardCharges.do')
    }
    else if(PageName == 'RoomUpgrade') {
        Params = GenerateRequestForRoomUpgrade(form);
        LoadDIV(Params,'divPricePanel','','','/th/'+ProductType+'/book/UpdateRoomUpgrade.do')
    }

 } else {
    LoadDIV(Params,'divPricePanel','','','/th/'+ProductType+'/book/UpdatePrices.do')
  }
}

function GenerateRequest(form) {
 var params = [];
 for (i = 0; i < form.elements.length; i++) {
  obj = form.elements[i];
  if (obj.type == 'radio') {
   if (obj.checked) {
    params.push(escape(obj.name) + '=' + obj.value);
   }
  } else if (obj.type == 'select-one' ||  obj.type == 'hidden') {
    params.push(escape(obj.name) + '=' + obj.value);
  } else if (obj.type == 'checkbox') {
   if (obj.checked) {
    params.push(escape(obj.name) + '=' + obj.value);
   }
  }
 }
 return params.join('&');
}



function GenerateRequestForRoomUpgrade(form) {
 var params = [];
 for (i = 0; i < form.elements.length; i++) {
  obj = form.elements[i];
  if (obj.type == 'radio') {
   if (obj.checked) {
    params.push(escape(obj.name) + '=' + obj.value);
   }
  } else if (obj.type == 'select-one' ||  obj.type == 'hidden') {
    var objName = obj.name
    var roomCode = new RegExp("roomDetail.roomCode");
    var quantity = new RegExp("roomDetail.quantityAvailable");
    var totalPrice = new RegExp("totalPrice_");
    var displayRoomSelection = new RegExp("displayRoomSelection.value");
    var passengers = new RegExp("roomAllocation.passengers");
    // Passing all the hidden parameters is causing the url length to exceed more than 1KB and iframe 
    // is unable to submit the request. Remove the hidden parameters which are not required for price calculation
    if( !objName.match(roomCode) &&  !objName.match(quantity) && !objName.match(totalPrice)
       && !objName.match(displayRoomSelection) && !objName.match(passengers))
     {
          params.push(escape(obj.name) + '=' + obj.value);
     }
  } else if (obj.type == 'checkbox') {
   if (obj.checked) {
    params.push(escape(obj.name) + '=' + obj.value);
   }
  }
 }
 return params.join('&');
}