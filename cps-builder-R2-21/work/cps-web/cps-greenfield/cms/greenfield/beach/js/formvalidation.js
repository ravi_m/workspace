// Validate form for submission
function ValidateForm(formobj) {
  if (formobj.isHolidayNew.value == 'true') {
    for (i = 0; i < formobj.elements.length; i++) {
      obj = formobj.elements[i]

      if (TrimSpaces(obj.value) == 'select') {
        return SetFocus('Please select the nature of your e-mail.', obj)
      }

      // Identify Validation required - check for disabled
      if (obj.getAttribute("alt")) {

        disabledlayer = false
        obj1 = obj

        // Find if obj is on a DIV
        while (obj1.tagName != 'BODY') {
          if (obj1.tagName == 'DIV') {
            if (obj1.style.display == 'none') {
              disabledlayer = true
              break;
            }
          }
          obj1 = obj1.parentNode
        }

        if ((obj.getAttribute("alt") != '') && (!disabledlayer)) {
          valset = obj.getAttribute("alt").split("|")

          // Trim input
          obj.value = TrimSpaces(obj.value)

          obj_value = obj.value
          obj_name = valset[0]

          if (valset[1] == 'Y') {
            obj_mandatory = true
          } else {
            obj_mandatory = false
          }
          obj_type = valset[2]
          obj_params = valset.slice(3)
          // alert(obj_mandatory)
          // Mandatory
          if (obj_mandatory) {
            if ((obj_value == '')
                && ((obj.type == 'text')
                    || (obj.type == 'textarea') || (obj.type == 'password')))
              return SetFocus('Please enter ' + obj_name, obj)
            if ((!obj.checked) && (obj.type == 'checkbox'))
              return SetFocus(obj_name, obj)

              // Check selection has ben made on selection boxes
            if (obj.type.substr(0, 6) == 'select') {
              if (obj.options.selectedIndex == 0) {
                return SetFocus('Please select ' + obj_name,
                    obj)
              }
            }
          }

          // Validation Type
          if (obj_type) {
            switch (obj_type.toUpperCase()) {
            // Alphanumeric 0-9 a-z A-Z Space Hyphen / \
            case 'ALPHA':
              var Pat1 = /^[a-zA-Z -]*$/;
              if (!Pat1.test(obj_value)) {
                return SetFocus('Please enter valid details',
                    obj)
              }
              break;

            case 'ALPHANUMERIC':
              var Pat1 = /^[-a-zA-Z0-9 \\\/.,]*$/;
              if (!Pat1.test(obj_value)) {
                return SetFocus('Please enter valid details',
                    obj)
              }
              break;

            // Alphanumeric with Extended characters
            case 'ANEXTENDED':
              var Pat1 = /^[-a-zA-Z0-9 \\\/.,\x27!;@:"�\$%\^&\*()]*$/;
              if (!Pat1.test(obj_value)) {
                return SetFocus('Please enter valid details',
                    obj)
              }
              break;

            // Alphanumeric with Extended characters for Password
            case 'PASSWORD':
              var Pat1 = /^[-a-zA-Z0-9 \\\/.,\x27!;@:"#�\$%\^&\*()]*$/;
              if (!Pat1.test(obj_value)) {
                return SetFocus('Please enter valid details',
                    obj)
              }
              break;

            // Insurer Name
            case 'INSURERDETAILS':
              var Pat1 = /^[^&]*$/;
              if (!Pat1.test(obj_value)) {
                return SetFocus(
                    'Please enter valid details.\n(Invalid characters are &)',
                    obj)
              }
              break;

            // Numeric 0-9 NUMERIC|Min|Max
            case 'NUMERIC':
              var Pat1 = /^[0-9]*$/;
              if (!Pat1.test(obj_value)) {
                if (valset[3] == 'olbp')
                  return SetFocus(
                      'The details you have entered have not been recognised - please re-enter',
                      obj);
                else
                  return SetFocus(
                      'Please enter a valid number', obj);
              }
              // Perform Range Checking
              if (obj_params != '') {
                obj_value = parseFloat(obj_value)
                // Minimum
                if (obj_params[0]) {
                  if (obj_value < obj_params[0]) {
                    return SetFocus(
                        'Please enter a valid number',
                        obj)
                  }
                }
                // Maximum
                if (obj_params[1]) {
                  if (obj_value > obj_params[1]) {
                    return SetFocus(
                        'Please enter a valid number',
                        obj)
                  }
                }
              }

              break;

            // Email Address xxx@xxx.xxx
            case 'EMAIL':
              obj_value = obj_value.toLowerCase();
              obj.value = obj_value;
              Pat1 = /^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;
              if (!Pat1.test(obj_value)) {
                return SetFocus(
                    'Please enter a valid email address \n\ne.g. name@place.com',
                    obj)
              }
              break;

            // Address Fields
            case 'ADDRESS':
              var Pat1 = /^[-a-zA-Z0-9 ,\.\x27]*$/;
              apos = String.fromCharCode(39)
              if (!Pat1.test(obj_value)) {
                return SetFocus(
                    'Please enter a valid address.', obj)
              }
              break;

            // Name Fields
            case 'NAME':
              var Pat1 = /^[-a-zA-Z ,\.\x27]*$/;
              apos = String.fromCharCode(39)
              if (!Pat1.test(obj_value)) {
                if (valset[3] == 'olbp')
                  return SetFocus(
                      'The details you have entered have not been recognised - please re-enter',
                      obj);
                else
                  return SetFocus(
                      'Please enter a valid name', obj);
              }
              break;

            case 'REVIEWNAME':
              var Pat1 = /^[-a-zA-Z&@ \"\',\.\x27]*$/;
              apos = String.fromCharCode(39)
              if (!Pat1.test(obj_value)) {
                return SetFocus('Please enter a valid name.',
                    obj)
              }
              break;

            // UK Postcode XX1 1XX
            case 'POSTCODE':
              obj_value = obj_value.toUpperCase();
              obj.value = obj_value;
              Pat1 = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXYU0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
              if (!Pat1.test(obj_value)) {
                return SetFocus(
                    'Please enter a valid post code.', obj)
              }
              break;

            // Phone number
            case 'PHONE':
              obj.value = RemoveAllSpaces(obj.value)
              obj_value = obj.value
              Pat1 = /^[0-9 ]*$/;
              if ((!Pat1.test(obj_value)) && (obj_value != '')) {
                return SetFocus(
                    'Please enter valid phone details.',
                    obj)
              }
              break;

            // Credit/Debit Card number
            case 'CARDNO':
              var idIndex = (obj.name).indexOf("_cardNumber");
              var subString = (obj.name).substr(0, idIndex);
              var object = (subString) + "_type";
              FieldObjCardType = document
                  .getElementById("payment_type_0");
              var cardType = '';
              if (FieldObjCardType) {
                var cardTypeValue = (FieldObjCardType.value)
                    .split("|");
                cardType = cardTypeValue[0];
              }
              if (cardType == "AMERICAN_EXPRESS") {
                Pat1 = /^[0-9]{15}$/;
              } else {
                Pat1 = /^[0-9]{16,20}$/;
              }
              obj.value = RemoveAllSpaces(obj.value)
              obj_value = obj.value
              if (!Pat1.test(obj_value)) {
                return SetFocus(
                    'Please enter a valid card number.',
                    obj)
              }
              break;
            // Check to see if two values of 2 fields match
            case 'MATCH':
              obj_1 = getElem(obj_params[0])
              obj_2 = getElem(obj_params[1])
              if (obj_1.value != obj_2.value)
                return SetFocus(obj_name, obj_1)
              break;

            case 'EITHER_OR':
              obj_1 = document.getElementById(obj_params[0]);
              obj_2 = document.getElementById(obj_params[1]);
              selectedCardType = getSelectedCardType();
              var alertMsg = '';
              // //
              // if(document.getElementById(selectedCardType+'_issueNo')
              // &&
              // document.getElementById(selectedCardType+'_issueNo').value
              // == 'true')
              // {
              // alertMsg ="Please enter an issue number for your
              // card. If you don't have an issue number, please
              // enter the start date";
              // }
              // else
              // {
              alertMsg = obj_name;
              // }
              if ((obj_1.value == "MM" || obj_1.value == "")
                  && obj_2.value == "") {
                return SetFocus(alertMsg, obj_1)
              }

              /*
               * var idIndex = (obj.name).indexOf("_cardNumber");
               * var subString = (obj.name).substr(0,idIndex); var
               * object = (subString)+ "_type";
               * FieldObjCardType=document.getElementById("payment_type_0");
               * var cardType=''; if(FieldObjCardType) { var
               * cardTypeValue
               * =(FieldObjCardType.value).split("|"); cardType =
               * cardTypeValue[0]; } if(cardType=="SOLO" ||
               * cardType=="SWITCH") { return SetFocus("Please
               * enter an issue number for your card. If you don't
               * have an issue number, please enter the start
               * date",obj) }
               */
              break;

            case 'HOUSE_EITHER_OR':

 			    obj_1 = document.getElementById(obj_params[0]);
                obj_2 = document.getElementById(obj_params[1]);
                var alertMsg = obj_name;
                if (obj_1.value == "" && obj_2.value == "") {
                   return SetFocus(alertMsg, obj_1)

              }
             break;



            case 'AND':
              obj_1 = getElem(obj_params[0])
              obj_2 = getElem(obj_params[1])
              if (obj_1.value == '' || obj_2.value == '')
                return SetFocus(obj_name, obj_1)
              break;

            case 'RADIO_SELECTED':
              num = obj_params.length;
              selected = false;
              for (z = 0; z < num; z++) {
                if (getElem(obj_params[z]) != null
                    && getElem(obj_params[z]).checked) {
                  selected = true;
                }
              }
              if (selected == false)
                return SetFocus(obj_name,
                    getElem(obj_params[num - 1]))
              break;

            case 'DATE':
              day = getElemName(obj_params[0])[0].value
              month = getElemName(obj_params[1])[0].value
              year = getElemName(obj_params[2])[0].value
              datein = day + '/' + month + '/' + year
              var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;
              var matchArray = datein.match(datePat);
              if (matchArray == null) {
                return SetFocus(obj_name, obj)
              } else {
                if ((month == 4 || month == 6 || month == 9 || month == 11)
                    && day == 31) {
                  return SetFocus(obj_name, obj)
                }
                if (month == 2) {
                  var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
                  if (day > 29 || (day == 29 && !isleap)) {
                    return SetFocus(obj_name, obj)
                  }
                }
              }
              break;

            // Credit/Debit Card Date CARDDATE|START or EXPIRY|Year
            // Field Name|Reference Month|Reference Year
            case 'CARDDATE':
              cardmonth = obj.options[obj.options.selectedIndex].value
              obj_year = getElem(obj_params[1])
              cardyear = obj_year.options[obj_year.options.selectedIndex].value

              var Calendar = new Date();
              todaysmonth = Calendar.getMonth() + 1;
              var calib = (ns4 || ns6) ? 1900 : 0;
              todaysyear = 1 * year2digit();

              if (obj_mandatory) {
                if (cardmonth == '') {
                  return SetFocus(
                      'Please enter a valid date', obj)
                }
                if (cardyear == '') {
                  return SetFocus(
                      'Please enter a valid date', obj)
                }
              } else if ((cardmonth == '' && cardyear != '')
                  || (cardmonth != '' && cardyear == '')) {
                return SetFocus('Please enter a valid date',
                    obj)
              }
              cardmonth = 1 * cardmonth;
              cardyear = 1 * cardyear;
              // If Start Date
              if (obj_params[0] == 'START') {
                if (((cardmonth > todaysmonth) && (cardyear == todaysyear))
                    || (cardyear > todaysyear)) {
                  return SetFocus(
                      'Please enter a valid start date',
                      obj)
                }
              }
              // If Expiry Date

              if (obj_params[0] == 'EXPIRY') {
                if (((cardmonth < todaysmonth) && (cardyear == todaysyear))
                    || (cardyear < todaysyear)) {
                  return SetFocus(
                      'Please enter a valid expiry date',
                      obj)
                }
              }
              break;

            case 'LUHN':
              obj_1 = document
                  .getElementById('payment_0_cardNumberId');
              var cardnumber = document
                  .getElementById('payment_0_cardNumberId').value;
              var oddoreven = cardnumber.length & 1;
              var sum = 0;
              var addition = "";

              for ( var count = 0; count < cardnumber.length; count++) {
                var digit = parseInt(cardnumber
                    .substr(count, 1));
                if (!((count & 1) ^ oddoreven)) {
                  digit *= 2;
                  if (digit > 9) {
                    digit -= 9;
                    addition = addition + ' ' + digit;
                  } else {
                    addition = addition + ' ' + digit;
                  }
                  sum += digit;
                } else {
                  sum += digit;
                  addition = addition + ' ' + digit;
                }
              }

              if (sum % 10 != 0) {
                return SetFocus(
                    'Please enter a valid card number.',
                    obj_1)
              }
              break;

            case 'SECURITY':
              var cardType = '';
              var securityCodeLength = '';
              Pat1 = /^[0-9 ]*$/;
              var idIndex = (obj.name).indexOf("_securityCode");
              var subString = (obj.name).substr(0, idIndex);
              var object = (subString) + "_securityCode";
              var indexValue = (object).split("_");
              cardType = document.getElementById("payment_type_"
                  + indexValue[1]).value;
              cardTypeValue = cardType.split("|")[0];
              securityCodeLength = document
                  .getElementById(cardTypeValue
                      + "_securityCodeLength").value;
              if ((obj.value.length != securityCodeLength)
                  || (!Pat1.test(obj.value))) {
                return SetFocus(
                    'Please enter a valid security number (the last ' + securityCodeLength + ' digits in the signature strip on the reverse of your card)',
                    obj)
              }
              break;

            // Review >> Check for < and >
            // ALT defined as >>
            // 0-EmptyAlert|1-Y/N|2-inputtype|3-Minlength|4-MinlengthAlert|5-Maxlength|6-MaxlengthAlert
            case 'REVIEW':
              var Pat1 = /^[^<>]*$/;
              if (!Pat1.test(obj_value)) {
                return SetFocus(
                    'We cannot accept HTML or angle brackets in your review, please remove any HTML or \'<\' or \'>\' characters.',
                    obj)
              }
              if (valset[3] != '') { // check for the minlength
                          // attribute
                if (obj_value.length < valset[3]) {
                  if (valset[4] != '') {
                    return SetFocus(valset[4], obj);
                  } else {
                    return SetFocus(
                        'You have entered '
                            + obj_value.length
                            + ' characters and have exceeded the '
                            + valset[3]
                            + ' character limit',
                        obj);
                  }
                }
              }
              if (valset[5] != '') { // check for the maxlength
                          // attribute
                if (obj_value.length > valset[5]) {
                  if (valset[6] != '') {
                    return SetFocus(valset[6], obj);
                  } else {
                    return SetFocus(
                        'This field has a minimum '
                            + valset[5]
                            + ' character limit. You have entered only '
                            + obj_value.length
                            + ' characters', obj);
                  }
                }
              }
              break;

            case 'ALPHANUMHYPHEN':
              var Pat1 = /^[-a-zA-Z0-9]*$/;
              if (!Pat1.test(obj_value)) {
                return SetFocus(
                    'Please enter a valid Promotional Code. It accepts only Alphabets, Numerics and Hyphen(-)',
                    obj)
              }
              break;

            case 'DECIMAL':
              var Pat1 = /^[0-9.]*$/;// Check for Numeric and dot
              var Pat2 = /^[0-9]+(\.\d{1,2})?$/;// Check for 2
                                // optional
                                // decimals
              if (!Pat1.test(obj_value) || !Pat2.test(obj_value)) {
                return SetFocus(
                    'Please enter a valid amount and only upto 2 decimal places',
                    obj)
              }
              break;
            } // switch
          } // if - mandatory
        } // if - Validation Required
      } // if alt tag exists
    } // for
    // Change button style
    if (getElem('confirm')) {
      hideLogOff();
    }
    if (getElem('writerev')) {
      hidingTimer();
    }
  }

  formobj.submit()
}

function ClearField(obj) {
  if (obj.value != '') {
    obj.value = '';
    obj.select()
    obj.onfocus = null;
  }
}

// Set Focus to field and show message
function SetFocus(alertmess, obj1) {
  alert(alertmess)
  obj1.focus();
  return;
}

// Remove all spaces in value
function RemoveAllSpaces(objval) {
  RESpace = /\s/ig;
  objval = objval.replace(RESpace, '');
  return objval
}

// Trim spaces from start and end of string
function TrimSpaces(objval) {
  // Remove Space at start
  RESpace = /^\s*/i;
  objval = objval.replace(RESpace, '');
  // Remove Space at end
  RESpace = /\s*$/i;
  objval = objval.replace(RESpace, '');
  return objval;
}

// Strips the characters passed through the function.
function stripChars(s, bag) {

  if (s == undefined) {
    return;
  }
  var i;
  var returnString = "";
  // Search through string's characters one by one.
  // If character is not in bag, append to returnString.
  for (i = 0; i < s.length; i++) {
    // Check that current character isn't whitespace.
    var c = s.charAt(i);
    if (bag.indexOf(c) == -1)
      returnString += c;
  }
  return returnString;
}

/* Returns 2 digit year * */
function year2digit() {
  RightNow = new Date();
  return /..$/.exec(RightNow.getYear())
}
/*******************************************************************************
 * This file contains functions related Validation of payment panel
 */
/* Used to validate fields in payment methods */
function validatePaymentFields(inputObj) {
  thisObjType = inputObj.id.split("_")[2];
  // Card number (method: Card,CNP)
  if (thisObjType == 'cardNumberId' && inputObj.value != '') {
    inputObj.value = removeAllSpaces(inputObj.value);
  }

  // Name on card (method: Card,CNP)
  if (thisObjType == 'nameOnCardId' && inputObj.value != '') {
    inputObj.value = trimSpaces(inputObj.value);
  }

  if (thisObjType == 'postCodeId' && inputObj.value != '') {
    inputObj.value = trimSpaces(inputObj.value);
  }

  // Security code (method: Card,CNP)
  if (thisObjType == 'securityCodeId' && inputObj.value != '') {
    inputObj.value = removeAllSpaces(inputObj.value);
  }
}