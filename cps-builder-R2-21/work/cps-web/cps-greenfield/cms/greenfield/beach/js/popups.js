var bookingmode = false;
var popupflag = false;

function popup_country(country_code) {
  Popup('/th/beach/viewLocationInformation.do?'+country_code,600,300)
}

function popup_location (location_code,location_name,brochure_code,season_code) {
   Popup('/th/beach/viewContentInformation.do?' + location_code + '&' + location_name + '&' + season_code + '&' + brochure_code,600,300);
}

function popup_destination(dest_code) {
  Popup('/th/beach/viewLocationInformation.do?'+dest_code,600,300)
}

function popup_resort(resort_code) {
  Popup('/th/beach/viewLocationInformation.do?'+resort_code,600,300)
}

function popup_weather(dest_code) {
  Popup('/th/beach/viewWeather.do?'+dest_code,600,570)
}

function popup_brochure(BrochureCode, SeasonCode) {
if(productType == "CRUISE")
     Popup('/th/cruise/viewBrochureIntro.do?brochureCode='+BrochureCode+'&brochureSeasonCode='+SeasonCode,450,300,'scrollbars=1')
if(productType == "BEACH")
    Popup('/th/beach/viewBrochureIntro.do?brochureCode='+BrochureCode+'&brochureSeasonCode='+SeasonCode,450,300,'scrollbars=1')
}


function popup_flight_schedule(params) {
  Popup('/th/beach/viewFlightSchedule.do?'+params,600,350, 'scrollbars=1')
}

function editorialtopic(actionname,section,topic,bookmark) {
if (!bookmark || bookmark==null) bookmark=''
if (!topic || topic==null) topic=''
pageaction = po_url + '/po/' + actionname + '.do?'
pageaction += '&popup=' + popupflag
if (section != '') { pageaction += '&section=' + section }
if (topic != '') { pageaction += '&topic=' + topic }
if (bookmark != '') {	pageaction += '#' + bookmark }
document.location.href = pageaction
}

function editorial(actionname,section,topic,bookmark) {
if (!bookmark || bookmark==null) bookmark=''
if (!topic || topic==null) topic=''

var actionurl = clientDomainURL;

actionurl += '/po/' + actionname + '.do?'

if (bookingmode) {
  actionurl += 'popup=true'
  } else {
  actionurl += 'popup=false'
  }
if (section && section != '' && section != null) { actionurl += '&section=' + section }
if (topic && topic != '' && topic != null) { actionurl += '&topic=' + topic }
if (bookmark != '') {	actionurl += '#' + bookmark }
if (bookingmode) {
  Popup(actionurl,780,500,'scrollbars=1,left=25,top=25')
  } else {
  document.location.href = actionurl
  }
}

function editorialPrivacy() {

var actionname = "viewPrivacyPolicy.do?";
var product = "beach";
pageaction = po_url + "/th/";
pageaction +=  product + "/" + actionname;
pageaction += 'popup=false';
pageaction += '&brochureId=TH44';
document.location.href = pageaction
}

// Set Booking Mode
function BookingMode() {
 bookingmode = true
}

//opens a pop up/new browser window based on the flag for media items
function popupMediaItem(url,height,width,popupIndicator)
{
   if (popupIndicator=='N')
   {
      winpops=window.open(url,"Media_Type","fullscreen=no,toolbar=yes,status=yes,menubar=no,scrollbars=yes,resizable=no,directories=no,location=yes,width="+width+",height="+height)
   }else{
         winpops=window.open(url,"Media_Type","width="+width+",height="+height)
   }
}
