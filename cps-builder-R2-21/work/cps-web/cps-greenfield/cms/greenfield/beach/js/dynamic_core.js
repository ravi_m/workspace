// Dynamic Core
/* Modifications to this file need to be implemented in the same way
   in the sprocket_dynamic_core.js file. */

var PageLoad;
var dynamicFrameName = 'dynamicFrame';
var updateLocationActionCruise = '/th/cruise/updateLocation.do';
var updateLocationActionBeach = '/th/beach/updateLocation.do';
var updateLocationParams = '';

// Perform an action on an element
function ElementAction(id, action) {
  var element = getElem(id);
  switch (action) {
    case 'hide': element.style.visibility = 'hidden'; break;
    case 'visible': element.style.visibility = 'visible'; break;
    case 'display': element.style.display = 'inline'; break;
    case 'nodisplay': element.style.display = 'none'; break;
    case 'disable': element.disabled = true; break;
    case 'enable': element.disabled = false; break;
    case 'clear': element.value = ''; break;
    case 'showany': element.disabled = true; ClearDropDown(id); break;
    case 'focus': element.focus(); break;
    case 'selectany': element.options[0].selected = true; break;
    case 'selectanydisable': element.options[0].selected = true; element.disabled = true; break;
    case 'showdatemessage': element.disabled = true; ClearDropDown(id, 'Please select a valid date'); break;
    case 'shownoavailabilitymessage': element.disabled = true; ClearDropDown(id, 'No availability for selected date'); break;
  }
}

// Load Buffer with new Selection Input
function LoadDropDown(queryString, postUpdateJS) {
  var updateFrameID = dynamicFrameName;
  var updateJS = '';
  var ids = LoadDropDown.arguments;
  for (var i = 1; i < ids.length - 1; i++) {
    var id = ids[i + 1];
    if (id != '') {
      updateJS += 'UpdateDropDown("' + id + '", ' + i + ',"' + updateFrameID + '");';
      ClearDropDown(id, 'Loading...');
    }
  }
  var url = (window.productType == 'CRUISE') ? updateLocationActionCruise : updateLocationActionBeach;
  window[updateFrameID + 'LoadCompleteRun'] = updateJS + postUpdateJS;
  url += '?' + queryString + '&' + updateLocationParams;
  try {
    // try updates without affecting browser history
    window[updateFrameID].document.location.replace(url);
  } catch (e) {
    // fall-back position - required under some circumstances in Internet Explorer
    window[updateFrameID].location = url;
  }
}

// Add an option to a <select> element
function AddOption(select, text, value, defaultSelected, selected) {
  var option = (select.ownerDocument || select.document).createElement('option');
  option.text = text;
  option.value = value;
  option.defaultSelected = !!defaultSelected;
  option.selected = !!selected;
  // Safari doesn't handle add(option)
  // select.options.add(option);
  select.options[select.length] = option;
  if (!!selected && select.type == 'select-one') { select.selectedIndex = select.length - 1 }
}

// Clear all options in Selection
function ClearDropDown(id, text) {
  var select = getElem(id);
  select.options.length = 0;
  if (text) { select.disabled = true }
  AddOption(select, text || select.getAttribute('alt'), 'Any');
}

// Load Buffer with new Layer
function LoadDIV(queryString, id, postUpdateJS, frameName, url) {
  frameName = frameName || dynamicFrameName;
  var frame = window[frameName];
  window[frameName + 'LoadCompleteRun'] = 'UpdateDIV("' + id + '");' + postUpdateJS;
  frame.document.location.replace(url + '?' + queryString);
}

// Update Selection Input element from Buffer or Copy if source element specified
function UpdateDropDown(id, number, frameName) {
  var element = getElem(id);
  var frame = window[frameName || dynamicFrameName];
  CopyDropDown(frame.document.getElementById('DynamicSelect_' + number), element)
  // Remove top option if only 1 result
  SingleResult = (element.options.length == 1)
  ElementAction(id, 'enable')
  ShowSelection(id)
  if (SingleResult) { DynamicUpdate(id, 'change') }
}

// Copy Drop Down
function CopyDropDown(source, target) {
  target.options.length = 0;
  for (var i = 0, option; i < source.length; i++) {
    option = source.options[i];
    AddOption(target, option.text, option.value, option.defaultSelected, option.selected);
  }
}

// Update DIV element from Buffer
function UpdateDIV(id) {
  getElem(id).innerHTML = window.frames[dynamicFrameName].document.getElementById('DynamicDIV').innerHTML;
  if (getElem('priceblock')) {
    /* This section is used for printing the updated price panel.*/
    getElem('pricepanelprint').innerHTML = getElem('divPricePanel').innerHTML;
    getElem('flightsblock').innerHTML = getElem('flights').innerHTML;
    getElem('accommodationblock').innerHTML = getElem('accommodation').innerHTML;
  }
}

function DynamicLoadComplete(frame) {
  try { eval(window[frame.name + 'LoadCompleteRun']); } catch (er) { }
}

// Is selection box ANY or null
function isany(id) { return /^(any)?$/i.test(String(getElem(id).value)); }