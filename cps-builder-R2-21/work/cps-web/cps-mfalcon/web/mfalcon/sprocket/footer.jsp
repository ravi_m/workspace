 <%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div id="footer">
				<div id="call-us" class="hide">
					<div class="content-width">
						<i class="caret call white b"></i><h2 class="d-blue">BOOK NOW! <span>Call us on: <a href="tel:0203 636 1931">0203 636 1931</a></span></h2>
					</div>
				</div>
				<div id="search" class="b">
					<div class="content-width">
						<a href="#page" id="backtotop"><span>To top </span><i class="caret back-to-top white"></i></a>
					</div>
				</div>
				<div id="group" class="b falcon">
					<div class="content-width">
						<div class="copy">
							<span id="world-of-tui"><img alt="World of TUI" src="/cms-cps/mfalcon/images/logo/wtui.png"></span>
							<p>We're part of TUI Travel PLC, one of the world's leading travel groups - and we're in great financial shape. Precious time. Guaranteed.</p>

						</div>
					<div class="logos">
				         <!--  <span id="world-of-tui" title="World of TUI"></span>  -->
				          <a href="http://www.aviationreg.ie/" id="c-ar" title="Commission for Aviation Regulation"><span>T.O. 021</span></a>
			        </div>

					</div>
				</div>
				<%
				String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.falcon.mobile" , "");
				pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);

				%>
				<div id="terms">
					<div class="content-width">
						<p>
				<a href="http://www.falconholidays.ie/about-us/">About Falcon</a>
				<a href="http://www.falconholidays.ie/our-policies/accessibility/">Accessibility</a>
				<a href="http://www.falconholidays.ie/our-policies/statement-on-cookies/index.html">Statement on cookies</a>
				<a href="http://www.falconholidays.ie/our-policies/terms-of-use/">Terms &amp; conditions</a>
				<a href="http://www.falconholidays.ie/our-policies/privacy-policy/">Privacy Policy</a>
				<c:choose>
					<c:when test="${applyCreditCardSurcharge eq 'true'}">
				<a href="http://www.falconholidays.ie/information/credit-card-payments-and-charges/">Credit card fees</a>
					</c:when>
					<c:otherwise>
						<a href="http://www.falconholidays.ie/information/credit-card-payments-and-charges/">Ways to Pay</a>
					</c:otherwise>
					</c:choose>
				<a class="desktopLinkHide" href="http://www.falconholidays.ie">Desktop site</a>
				<a href="http://tuijobsuk.co.uk/">Travel Jobs</a>
				<a href="http://blog.falconholidays.ie/">Falcon Blog</a>
				<a target="_blank" href="http://www.tuigroup.com/en-en"><jsp:useBean id='CurrentDate12' class='java.util.Date'/>
         	<fmt:formatDate var='currentYear' value='${CurrentDate12}' pattern='yyyy'/>
			&copy; <c:out value="${currentYear}" />  TUI Group</a>
						</p>
					</div>
				</div>
				<%= staySafeAbroad %>
			</div>
			<div id="disclaimer">
				<div class="content-width disclaim two-columns">
				<p>Falcon Holidays and Thomson Holidays are licenced by the Commission for Aviation Regulation under Licence T.O. 021 and have an arranged an approved bond, therefore your money is secure with us.
		Falcon Holidays and Thomson Holidays are part of the TUI Travel PLC Group of Companies.</p>

				</div>
			</div>
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,communicateByEmail,chkThirdPartyMarketingAllowed,communicateByPost,communicateByPhone,tuiAnalyticsJson</c:set>
			<script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
				 tui.analytics.page.pageUid = "mobilePaymentDetailsPage";
				 <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
					 <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
					 tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
				    </c:if>
				    <c:if test= "${analyticsDataEntry.key == 'Party'}">
                        tui.analytics.page.Party= "${analyticsDataEntry.value}";
                        </c:if>
			    </c:forEach>
				</script>
				<script>
			
				window.dataLayer = window.dataLayer || [];
				window.dataLayer.push(${bookingInfo.bookingComponent.nonPaymentData['tuiAnalyticsJson']});

			
			</script>