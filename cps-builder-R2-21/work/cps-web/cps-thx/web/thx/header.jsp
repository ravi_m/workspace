 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%
 String headerlink = com.tui.uk.config.ConfReader.getConfEntry("thx.headerlink" , "");
 pageContext.setAttribute("headerlink", headerlink, PageContext.REQUEST_SCOPE);

 String tuiHeaderSwitch = com.tui.uk.config.ConfReader.getConfEntry("thx.tuiHeaderSwitch" , "");
pageContext.setAttribute("tuiHeaderSwitch", tuiHeaderSwitch, PageContext.REQUEST_SCOPE);
%>        

<c:set var="TUIHeaderSwitch" value="${tuiHeaderSwitch}"/>

<c:choose>

   <c:when test="${TUIHeaderSwitch eq 'ON'}">
      <c:set value="TUI" var="tuiFlagLink" />
	  <c:set value="true" var="tuiLogo" />
   </c:when>
   <c:otherwise>
      <c:set value="Thomson" var="tuiFlagLink" />
	  <c:set value="false" var="tuiLogo" />
   </c:otherwise>
</c:choose>


<c:if test="${headerlink== 'true'}">
<c:if test="${not empty bookingComponent.clientURLLinks.homePageURL}">
 <div class="header">
 
                     <div id="navigation-bar">
               <a target="_blank" id="logo" href="<c:out value='${bookingComponent.clientURLLinks.homePageURL}'/>">
               <c:if test="${tuiLogo== 'false'}">
               <img
				class="print-only removeBorder"
				src="http://www.thomson.co.uk/images/header/logo-thomson.png"
				alt="Thomson" style="width: 117px; margin-top: 20px;">
				</c:if>
				<c:if test="${tuiLogo== 'true'}">
				<div class="tuiLogo-header">
						<a href=""></a>
				</div>
				</c:if>
			</a>
                    </div>
                </div>
</c:if>	

 <c:if test="${empty bookingComponent.clientURLLinks.homePageURL}">
 <div class="header">
 
                     <div id="navigation-bar">
               <div target="_blank" id="logo" >
               <c:if test="${tuiLogo== 'false'}">
                <img
				class="print-only removeBorder"
				src="http://www.thomson.co.uk/images/header/logo-thomson.png"
				alt="Thomson" style="width: 117px; margin-top: 20px;">
				</c:if>
				<c:if test="${tuiLogo== 'true'}">
				<div class="tuiLogo-header">
						<a href=""></a>
				</div>
				</c:if>

			</div>
                    </div>
                </div>
  
</c:if>	
</c:if>	
		
<div id="header_main_menu">

</div>
<%@include file="breadCrumb.jspf"%>