 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<link rel="stylesheet" type="text/css" href="/cms-cps/thx/css/tech_difficult.css"/>
<%
    String sessionTimeoutHome = com.tui.uk.config.ConfReader.getConfEntry("thx.home" , "");
    pageContext.setAttribute("sessionTimeoutHome", sessionTimeoutHome, PageContext.REQUEST_SCOPE);
    %>
<html>
<head>
<title></title>
<%@include file="common.jspf"%>
</head>
<body>
	<div class="thxwrapper">
	<%@include file="errorHeader.jsp"%>
		
		<div class="error_left">
	<div class="contentSection"><h2>We're really sorry, <div class="clearboth"></div>Your session has expired due to inactivity.</h2></div>
	<div class="clearboth sessiontimedout"></div>
	<div class="contentSearch">
	<a href="<c:out value='${sessionTimeoutHome}'/>">Click here to begin another search</a>
	</div>
	</div>
	<div class="error_right"><img src="/cms-cps/thx/images/error_500_page.png"></div>
		<br>
		<br>
		<!--<div class="Error404_SearchBox">
               <input class="Error404_SearchTextbox" placeholder="Search on website"/>
            </div>
            <div class="Error404_SearchButtonImage">
               <img class="Error500Img" src="Images/TH-bookFlow-errors-404-Search.png">
            </div> -->


		<%@include file="footer.jsp"%>
		</div> 
</body>
</html>