<div id="details">
<div id="personal_details">
<h2 class="underline">PERSONAL DETAILS</h2>
<p class="underlined">We only require one set of details to act as the main contact.</p>
<div class="formrow pers-details">
                            <ul>
                               <li class="personal-details">
                                    <label for="title">Title</label>
                                    <c:set var="titleKey" value="title"/>
		<input type="alpha" name="personaldetails_0_title" id="passenger_0_title"
			class="textfield xsmall nonEditable" value="<c:out value='${bookingComponent.nonPaymentData[titleKey]}'/>"
			gfv_required="required" onfocus="this.blur()" readonly />
		<c:set var="passengerCount" value="0" scope="request"/>
                                </li>
                                <li>
                                    <label for="firstname">First name </label>
                                    <c:set var="passengerCount" value="0" scope="request"/>
		                            <c:set var="foreNameKey" value="foreName"/>
                                    <input type="text" class="textfield xxxlarge mandatory nonEditable" name="personaldetails_${passengerCount}_foreName" id="passenger_0_foreName" value="<c:out value='${bookingComponent.nonPaymentData[foreNameKey]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
                                </li>
                                <c:set var="initialKey" value="initial"/>
                                <c:if test="${not empty bookingComponent.nonPaymentData[initialKey]}">  
                                <li>
                                    <label for="initial">Initial</label>
                                    
                                    <input type="text" class="textfield xxxlarge nonEditable" name="personaldetails_${passengerCount}_middleInitial" id="passenger_0_initial" value="<c:out value='${bookingComponent.nonPaymentData[initialKey]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
                                </li>
                                </c:if>	
                                <li>
                                    <label for="surname">Surname </label>
                                    <c:set var="surNameKey" value="surName"/>
                                    <input type="text" class="textfield xxxlarge mandatory nonEditable " name="personaldetails_${passengerCount}_surName" id="passenger_0_surName" value="<c:out value='${bookingComponent.nonPaymentData[surNameKey]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
                                </li>

								<li>
                                    <label for="housename">House Name/No.</label>
                                    <c:set var="houseNameKey" value="houseName"/>
                                    <input type="text" class="textfield xxxlarge mandatory nonEditable" name="houseName" id="houseName" value="<c:out value='${bookingComponent.nonPaymentData[houseNameKey]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
                                </li>         
								
                                <li>
                                    <label for="address">Address </label>
                                    <c:set var="addressLine1Key" value="addressLine1"/>
                                    <input type="text" class="textfield xxxlarge mandatory nonEditable" name="addressLine1" id="addressLine1" value="<c:out value='${bookingComponent.nonPaymentData[addressLine1Key]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
								</li>
								<c:set var="addressLine2Key" value="addressLine2"/>
								<c:if test="${not empty bookingComponent.nonPaymentData[addressLine2Key]}">   
								<li>
									<label for="address"></label>
									
									<input type="text" class="textfield xxxlarge mandatory nonEditable" name="addressLine2" id="addressLine2" value="<c:out value='${bookingComponent.nonPaymentData[addressLine2Key]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
                                </li>
                                </c:if>	

                                <li>
                                    <label for="town">Town / City </label>
                                    <c:set var="cityKey" value="city"/>
                                    <input type="text" class="textfield xxxlarge mandatory nonEditable" name="city" id="city" value="<c:out value='${bookingComponent.nonPaymentData[cityKey]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
                                </li>
                                <li>
                                    <label for="county">Country</label>
                                    <c:set var="countyKey" value="county"/>
                                    <input type="text"  class="textfield xxxlarge mandatory nonEditable" name="country" id="county" value="<c:out value='${bookingComponent.nonPaymentData[countyKey]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
                                </li>
                                <li>
                                    <label for="postcode">Postcode </label>
                                    <c:set var="postCodeKey" value="postCode"/>
                                    <input type="text" class="textfield xxxlarge mandatory nonEditable" name="postCode" id="postCode" value="<c:out value='${bookingComponent.nonPaymentData[postCodeKey]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
                                </li>

                                <li>
                                    <label for="daytimen">Daytime phone n<sup>er</sup> </label>
                                    <c:set var="dayTimePhoneKey" value="dayTimePhone"/>
                                    <input type="text" class="textfield xxxlarge mandatory nonEditable" name="dayTimePhone" id="dayTimePhone" value="<c:out value='${bookingComponent.nonPaymentData[dayTimePhoneKey]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
                                </li>
                                <li>
                                    <label for="eveningn">Evening phone n<sup>er</sup> </label>
                                    <c:set var="eveningPhoneKey" value="eveningPhone"/>
                                    <input type="text" class="textfield xxxlarge mandatory nonEditable" name="eveningPhone" id="eveningPhone" value="<c:out value='${bookingComponent.nonPaymentData[eveningPhoneKey]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
                                </li>
                                <li>
                                    <label for="mobilen">Mobile phone n<sup>er</sup> </label>
                                    <c:set var="mobilePhoneKey" value="mobilePhone"/>
                                    <input type="text" class="textfield xxxlarge mandatory nonEditable" name="mobilePhone" id="mobilePhone" value="<c:out value='${bookingComponent.nonPaymentData[mobilePhoneKey]}'/>"
			                               gfv_required="required" onfocus="this.blur()" readonly>
                                </li>

                                <li>
                                    <label for="email">Email Address </label>
                                    <c:set var="emailAddressKey" value="emailAddress"/>
                                    <input type="text" class="textfield xxxlarge mandatory nonEditable" name="emailAddress" id="emailAddress" value="<c:out value='${bookingComponent.nonPaymentData[emailAddressKey]}'/>" gfv_required="required" onfocus="this.blur()" readonly>
                                </li>
                               <!--  <li>
                                    <label for="remail">Re-enter email Address </label>
                                    <input type="text" class="textfield mandatory nonEditable" data-validation-engine="validate[required, equals[email]]" name="remail" id="remail">
                                </li> -->
                            </ul>
                        </div>
</div>