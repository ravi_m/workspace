<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Payments</title>
<link type="image/x-icon" href="/cms-cps/thx/images/favicon.ico" rel="icon"/>
<link type="image/x-icon" href="/cms-cps/thx/images/favicon.ico" rel="shortcut icon"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<!--[if lt IE 9]>
  <script type="text/javascript" src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Add common files required for constructing fcx payment page  -->
<%@include file="common.jspf"%>
<%@include file="javascript.jspf"%>
<%@include file="configSettings.jspf"%>
<%@include file="tag.jsp"%>

<%
	String applyCreditCardSurcharge = com.tui.uk.config.ConfReader.getConfEntry("THX.applyCreditCardSurcharge" ,null);
	pageContext.setAttribute("applyCreditCardSurcharge", applyCreditCardSurcharge, PageContext.REQUEST_SCOPE);
%>
<script>
applyCreditCardSurcharge = "<%= applyCreditCardSurcharge %>";
</script>
<script type="">
   $(document).ready(function(){
     $("#paymentdetails").validationEngine();  
   });
</script>

<script>
$(document).ready(function () {
"use strict";
$(".tabHeadings").on('click', function () {
var $parent = $(this).parent();
if (!$parent.hasClass("active")) {
$(".footerTabHeader").removeClass("active");
$parent.addClass("active");
}
return false;
});
});
</script>

<script type="text/javascript">
			$(function(){ 
				$('a.clickTipMaster').aToolTip({
		    		clickIt: true,
		    		tipContent: 'We have introduced MasterCard SecureCode - a security programme created by MasterCard to give you peace of mind when shopping online. All you need to do is register once whilst paying on our website. You will be asked to set up a password that you can then use for any future payments you make with your card on participating websites.'
				});	

					$('a.clickTipVisa').aToolTip({
		    		clickIt: true,
		    		tipContent: 'We have introduced Verified by Visa - a security programme created by Visa to give you peace of mind when shopping online. All you need to do is register once whilst paying on our website. You will be asked to set up a password that you can then use for any future payments you make with your card on participating websites.'
				});	
					
						$('a.clickTipAmEx').aToolTip({
		    		clickIt: true,
		    		tipContent: 'We have introduced American Express SafeKey - a security programme created by American Express to give you peace of mind when shopping online. All you need to do is register once whilst paying on our website. You will be asked to set up a password that you can then use for any future payments you make with your card on participating websites.'
				});	
						
			}); 
		</script>

</head>

<body>
<div class="thxwrapper">
	<!-- Header  -->
	<%@include file="header.jsp"%>

	<!-- Payment Text -->
	<div id="payment">
		<h1>Payment</h1>
	</div>

	<!-- Shopping Basket -->
	<%@include file="shoppingBasket.jsp"%>

	<form id="paymentForm" name="paymentdetails" method="post"
		action="/cps/processPayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />">
		 <%@include file="topError.jspf" %>		
		
		<!-- Personal details -->
		<%@include file="personalDetails.jsp"%>

		<!-- payment Details -->
		<%@include file="paymentDetails.jsp"%>

		<!-- payment amount -->
		<%@include file="paymentAmount.jsp"%>

		<!-- Terms and Conditions -->
		<%@include file="termsAndConditions.jsp"%>
		
		<!-- Back to search results -->
		<%@include file="backToSearchResult.jspf"%>

		<!-- Pay Button -->
		<c:if test="${bookingInfo.newHoliday}">
			<%@include file="bookHoliday.jsp"%>
		</c:if>
		
	
	</form>
</div>
	<!-- Footer -->
		<%@include file="footer.jsp"%>
	
	<script type="text/javascript" src="/cms-cps/thx/js/vs.js"></script>
      <script type="text/javascript">
       window.onload=
        function()
        {
    	   amountUpdationWithCardCharge.updateCardChargeForTHX()
        }
    </script>

</body>

</html>