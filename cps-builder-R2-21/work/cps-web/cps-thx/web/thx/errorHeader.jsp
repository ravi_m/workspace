 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%
 String headerlink = com.tui.uk.config.ConfReader.getConfEntry("thx.headerlink" , "");
 pageContext.setAttribute("headerlink", headerlink, PageContext.REQUEST_SCOPE);
%>        
<c:if test="${headerlink== 'true'}">
<c:if test="${not empty bookingComponent.clientURLLinks.homePageURL}">
 <div class="header">
 
                     <div id="navigation-bar">
               <a target="_blank" id="logo" href="<c:out value='${bookingComponent.clientURLLinks.homePageURL}'/>"> <img
				class="print-only removeBorder"
				src="http://www.thomson.co.uk/images/header/logo-thomson.png"
				alt="Thomson" style="width: 117px; margin-top: 20px;">
			</a>
                    </div>
                </div>
</c:if>	

 <c:if test="${empty bookingComponent.clientURLLinks.homePageURL}">
 <div class="header">
 
                     <div id="navigation-bar">
               <div target="_blank" id="logo" > <img
				class="print-only removeBorder"
				src="http://www.thomson.co.uk/images/header/logo-thomson.png"
				alt="Thomson" style="width: 117px; margin-top: 20px;">
			</div>
                    </div>
                </div>
  
</c:if>	
</c:if>	
		
<div id="header_main_menu">

</div>
