<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:formatNumber value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}" var="totalCostingLine" type="number" pattern="####" maxFractionDigits="2" minFractionDigits="2"/>
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page"/>
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:if test="${not empty bookingComponent.breadCrumbTrail}">
<c:set var="optionsUrl" value="${bookingComponent.breadCrumbTrail['HOTEL']}"/>
<c:set var="searchUrl" value="${bookingComponent.breadCrumbTrail['HUBSUMMARY']}"/>
<c:set var="passengersUrl" value="${bookingComponent.breadCrumbTrail['PASSENGERS']}"/>
</c:if>

<c:choose>
   <c:when test="${bookingComponent.nonPaymentData['TUIHeaderSwitch'] eq 'ON'}">
      <c:set value="true" var="tuiLogo" />
   </c:when>
   <c:otherwise>
      <c:set value="false" var="tuiLogo" />  
   </c:otherwise>
</c:choose>

<div id="book-flow-header">
				<div class="content-width">
					<div class="logo<c:if test="${tuiLogo}">TUI</c:if> thomson firstchoiceX falconX"><a href="${bookingComponent.clientURLLinks.homePageURL}"></a></div>
					<c:if test="${!tuiLogo}"><img alt="World Of TUI" class="nomobile  nominitablet " id="header-wtui" src="/cms-cps/mcruise/images/WOT_Endorsement_V2_3C.png"></c:if>
					<div class="summary-panel-trigger b abs bg-tui-sand black">
						<i class="caret fly-out"></i>&pound;<c:out value="${totalcost[0]}."/><span class="pennys"><c:out value="${totalcost[1]}"/></span>
						
					</div>
				</div>
			</div>
			<div id="book-flow-progress">
				<div class="content-width">
					<div class="scroll uppercase" id="breadcrumb" data-scroll-dw="true" data-scroll-options='{"scrollX": true, "scrollY": false, "keyBindings": true, "mouseWheel": true}'>
					<!--	<ul class="c">
							<li class="back"><a href="<c:out value='${optionsUrl}'/>"><span class="rel b">1</span> ITINERARY & SHIP</a></li>
							<li class="back"><a href="<c:out value='${searchUrl}'/>"><span class="rel b">2</span> CRUISE OPTIONS</a></li>
							<li class="back"><a href="<c:out value='${passengersUrl}'/>"><span class="rel b">3</span> FLIGHTS</a></li>
							<li class="back"><a href="<c:out value='${optionsUrl}'/>"><span class="rel b">4</span> EXTRAS</a></li>
							<li class="back"><a href="<c:out value='${searchUrl}'/>"><span class="rel b">5</span> BOOK</a></li>
							
							<li class="active"><span class="rel b">6</span> Payment</li>
						</ul>-->
						
						<ul id="indicators-sprite-${numOfSteps}" class="c breadcrumbs-spacing">
<c:forEach var="indicator" items="${bookingComponent.breadCrumbTrail}"	varStatus="counter">
		<c:choose>
		
				<c:when test="${!counter.last}">
				<c:set var="indicatorHref" value="${indicator.value}" />

				</c:when>
				<c:otherwise>
				<c:set var="indicatorHref" value="#" />

				</c:otherwise>
		</c:choose>
		  
		  <li class="indicator back  uc  sprite-${numOfSteps}${counter.last ? ' last active' : counter.first ? ' first completed' : ' completed'}"><c:if test="${!counter.last}"><a href="<c:out value="${indicatorHref}"/>" class="ensLinkTrack" data-componentId="bookflowStepIndicator_comp"></c:if><span class="rel b">${counter.index+1}</span>${indicator.key}<c:if test="${!counter.last}"></a></c:if>
		</li>
		
		  	
		</c:forEach>  
		
</ul>
					</div>
				</div>
			</div>