/*
 * The JavaScript class TuiDialog encapsulates the functionality required for a dialog
 */

var tuiOverlay = new TuiOverlay();

function TuiOverlay()
{
  var TYPE_BLINKY = 'blinky';
  var TYPE_STICKY = 'sticky';

  /**
   * Initialisation function for blinky's on the page.
   */
  TuiOverlay.prototype.init = function()
  {
    initOverlay( TYPE_BLINKY );
  }

  var initOverlay = function( overlayType )
  {
    var id = "." + overlayType;
    var owner = id + "Owner";
    var allOverlays = $$( owner );

    if( allOverlays )
    {
      for( var i = 0, len = allOverlays.length; i < len; i++ )
      {
        var id = allOverlays[i].id;

        attachEvents( id );
      }
    }
  }

  var attachEvents = function( element )
  {
    var element = $( element );

    if( element )
    {
      var overlayId = element.id + "Overlay";

      element.onmouseover = function(event)
      {
        show( element.id, overlayId );
      }

      element.onmouseout = function( event )
      {
        hide( overlayId );
      }

      element.onclick = function(event)
      {
        TuiUtil.stopEventBubbling(event);

        return false;
      }

      element.setAttribute("title", "");
    }
  }

  // schedule a blinky to appear after a small delay
  var show = function( owner, blinky )
  {
    // make sure our arguments are nodes, not just IDs
    var owner = $( owner );
    var blinky = $( blinky );

    // if we can't find the blinky or its owner for some reason,
    // eg. during page load, then we can't do anything
    if( (owner == null) || (blinky == null) ){ return; }

    // ensure that our global array for remembering which parent a
    // blinky is attached to is created
    if( typeof( window.blinkyParentLookup ) == "undefined" )
    {
      window.blinkyParentLookup = {};
    }

    // make a note that the blinky will be attached to the particular parent
    window.blinkyParentLookup[blinky.id] = owner;

    // tell the blinky to really appear in half a second
    //owner.blinkyHoverTimerID = setTimeout( "dialogs.blinkyReallyShow('" + blinky.id + "')", 500 );
    reallyShow( blinky.id );
  }

  var reallyShow = function( blinky )
  {
    // make sure our arguments are nodes, not just IDs
    var blinky = $( blinky );

    // don't do anything if the blinky doesn't exist yet, eg. during page load
    if( blinky == null ){ return; }

    // find out which element this blinky was marked to appear against
    var blinkyOwner = window.blinkyParentLookup[blinky.id];

    // find the best place on the page for the blinky, and position it accordingly
    var position = calculateBestPopupPosition( blinkyOwner, blinky );
    positionElementFromPageOrigin( blinky, position.x, position.y );

    // close any other overlays that are open
    globalOverlayClose();

    // show the blinky
    blinky.style.display = "block";

    // position and show the shim, if necessary
    shimShowBehind( blinky, { top: 0, left: 3, bottom: 2, right: 2 } );

    // register a global close handler that can be called to close this overlay
    // in case we need to show another overlay
    globalOverlayRegisterCloseHandler( blinky.id, function(){ blinkyHide( blinky.id ); } );
  }

  var hide  = function( blinky )
  {
    // make sure our arguments are nodes, not just IDs
    var blinky = $( blinky );

    // only proceed if we have an actual blinky to work with
    if(blinky != null)
    {
      if( typeof( window.blinkyParentLookup) != "undefined" )
      {
        // find out which element this blinky was marked to appear against
        var blinkyOwner = window.blinkyParentLookup[blinky.id];

        if( typeof( blinkyOwner ) != "undefined" )
        {
          // if there was a timer on the parent to display the blinky, then clear it
          if( typeof( blinkyOwner.blinkyHoverTimerID ) != "undefined" )
          {
            clearTimeout( blinkyOwner.blinkyHoverTimerID );
          }
        }
      }

      blinky.style.display = "none";

      // hide the shim, if it exists
      shimHide( blinky );

      // remove any close handler we had registered for this overlay
      globalOverlayClearCloseHandler( blinky.id );
    }
  }

  // position an element at an offset from the page origin rather than its relative parent
  var positionElementFromPageOrigin = function( element, x, y )
  {
    // make sure our arguments are nodes, not just IDs
    var element = $( element );

    // find out where the relative parent starts
    var offsetParent = Position.offsetParent( element );
    var offsetParentPosition = Position.cumulativeOffset( offsetParent );

    // convert the desired coordinates into an offset against the
    // relative parent
    var offsetX = x - offsetParentPosition[0];
    var offsetY = y - offsetParentPosition[1];

    element.style.left = offsetX + "px";
    element.style.top  = offsetY + "px";
  }

  // position an element in the centre of the browser window
  var positionElementInViewportCentre = function( element )
  {
    var element = $( element );

    // get the viewport centre position and offset it by half the width
    // and height of the element
    var viewportCentre = getBrowserViewportCentre();
    var x = viewportCentre.x - Math.floor( Element.getWidth( element ) / 2 );
    var y = viewportCentre.y - Math.floor( Element.getHeight( element ) / 2 );

    positionElementFromPageOrigin( element, x, y );
  }

  // get the dimensions of the browser's *visible* area,
  // that is, not including scrollbars
  var getBrowserViewportDimensions = function()
  {
    var dimensions = {};

    if( browser.isSafari )
    {
      dimensions.width = window.innerWidth;
      dimensions.height = window.innerHeight;
    }
    else if( (typeof( document.documentElement ) != "undefined") && (typeof( document.documentElement.clientWidth ) != "undefined" ) )
    {
      dimensions.width = document.documentElement.clientWidth;
      dimensions.height = document.documentElement.clientHeight;
    }
    else if( typeof( window.innerWidth ) != "undefined" )
    {
      //Non-IE
      dimensions.width = window.innerWidth;
      dimensions.height = window.innerHeight;
    }
    else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) )
    {
      //IE 4 compatible
      dimensions.width = document.body.clientWidth;
      dimensions.height = document.body.clientHeight;
    }

    return dimensions;
  }

  var getBrowserScrollingOffsets = function()
  {
    var x,y;

    if (self.pageYOffset) // all except Explorer
    {
      x = self.pageXOffset;
      y = self.pageYOffset;
    }
    else if (document.documentElement && document.documentElement.scrollTop)
      // Explorer 6 Strict
    {
      x = document.documentElement.scrollLeft;
      y = document.documentElement.scrollTop;
    }
    else if (document.body) // all other Explorers
    {
      x = document.body.scrollLeft;
      y = document.body.scrollTop;
    }

    return { x: x, y: y };
  }

  var setBrowserScrollingOffsets = function( x, y )
  {
    if( document.documentElement && document.documentElement.scrollTop )
    {
      // Explorer 6 Strict
      document.documentElement.scrollLeft = x;
      document.documentElement.scrollTop = y;
    }
    else if( document.body )
    {
      document.body.scrollLeft = x;
      document.body.scrollTop = y;
    }
  }

  // get the coordinates of the centre of the browser viewport,
  // measured from the top-left corner of the pages
  var getBrowserViewportCentre = function ()
  {
    var scrollOffsets = getBrowserScrollingOffsets();
    var viewportSize = getBrowserViewportDimensions();

    var centre = {};
    centre.x = scrollOffsets.x + Math.floor( viewportSize.width / 2 );
    centre.y = scrollOffsets.y + Math.floor( viewportSize.height / 2 );

    return centre;
  }

  ////////////////////////////////////////////////////////////////////////
  //  GLOBAL OVERLAY FUNCTIONS
  //
  //  There can only be one overlay (ie. sticky, blinky or search overlay)
  //  open at once. So whenever we open an overlay we register a function
  //  to close it in a global variable. Then when we open a new overlay we
  //  call the registered function to close the old one.
  ////////////////////////////////////////////////////////////////////////

  // register the close handler function for an overlay
  var globalOverlayRegisterCloseHandler = function( element, handler )
  {
    // make sure any existing overlays are closed before we overwrite
    // the close handler
    globalOverlayClose();

    // store the handler and the element that it relates to
    window.globalOverlayElement = element;
    window.globalOverlayCloseHandler = handler;
  }

  // clear the registered close handler if it relates to the given element
  // If no element argument is supplied then the handler will be cleared
  // without checking which element it was registered for
  var globalOverlayClearCloseHandler = function( element )
  {
    if( !element || (window.globalOverlayElement == element) )
    {
      window.globalOverlayElement = null;
      window.globalOverlayCloseHandler = null;
    }
  }

  // close the current global overlay, if there is one
  var globalOverlayClose = function()
  {
    // call the registered close handler, if there is one
    if( window.globalOverlayCloseHandler )
    {
      window.globalOverlayCloseHandler();
    }
    // clear the close handler because the overlay has been shut now
    globalOverlayClearCloseHandler();
  }

  // move the shim, if it exists, to be behind a particular element, with optionally specified insets from the element's edges
  var shimShowBehind = function( element, insets )
  {
    var element = $( element );

    // if there were no insets provided, then just set them all to zero
    if( typeof( insets ) == "undefined" )
    {
      insets = { top: 0, left: 0, bottom: 0, right: 0 };
    }
    // the shim is only necessary for older versions of IE
    if( browser.isIE && (browser.versionMajor < 7) )
    {
      // if this element doesn't already have a shim, then make a new one
      if( typeof( element.shim ) == "undefined" )
      {
        element.shim = document.createElement( "iframe" );
        element.shim.style.position = "absolute";
        element.shim.style.display = "none";
        element.shim.style.zIndex = "201";
        element.shim.style.filter = "alpha(opacity=0);";
        element.shim.frameBorder = "0";
        // The following src value fixes a problem with the page being loaded using
        // https.
        // For further information about this fix please go to the site below:
        // http://www.zachleat.com/web/2007/04/24/adventures-in-i-frame-shims-or-how-i-learned-to-love-the-bomb/
        element.shim.src = "javascript:false;";
        element.shim.scrolling = "no";
        element.shim.id = element.id + "Shim";
        document.body.appendChild( element.shim );
      }

      var elementPosition = Position.cumulativeOffset( element );
      var elementWidth = Element.getWidth( element );
      var elementHeight = Element.getHeight( element );

      // set the size and position to be the same as the element, but inset by the given values
      element.shim.style.height = elementHeight - (insets.top + insets.bottom);
      element.shim.style.width = elementWidth - (insets.left + insets.right);
      element.shim.style.top = elementPosition[1] + insets.top;
      element.shim.style.left = elementPosition[0] + insets.left;
      element.shim.style.display = "block";
    }
  }

  var shimHide = function( element )
  {
    // make sure our arguments are nodes, not just IDs
    var element = $( element );
    if( (element != null) && (typeof( element.shim ) != "undefined") )
    {
      element.shim.style.display = "none";
    }
  }

  // calculate the position of a popup (ie. blinky/sticky) next to its owner
  // returns an (x,y) pixel offset from the page origin
  var calculateBestPopupPosition = function( owner, popup )
  {
    // make sure our arguments are nodes, not just IDs
    var owner = $( owner );
    var popup = $( popup );

    // get some useful measurements of the popup owner
    var ownerWidth = Element.getWidth( owner );
    var ownerHeight = Element.getHeight( owner );
    var ownerPosition = Position.cumulativeOffset( owner );
    var ownerLeft = ownerPosition[0];
    var ownerTop = ownerPosition[1];
    var ownerRight = ownerLeft + ownerWidth;
    var ownerBottom = ownerTop + ownerHeight;

    // middle of the popup owner
    var ownerCentreX = ownerLeft + Math.round( ownerWidth / 2 );
    var ownerCentreY = ownerTop + Math.round( ownerHeight / 2 );

    // how big is our popup?
    var popupWidth = Element.getWidth( popup );
    var popupHeight = Element.getHeight( popup );

    // find the position and size of the current browser viewport
    var viewportDimensions = getBrowserViewportDimensions();
    var scrollingOffsets = getBrowserScrollingOffsets();

    // use this information to find the edges of the viewport
    var viewportLeft = scrollingOffsets.x;
    var viewportTop = scrollingOffsets.y;
    var viewportRight = viewportLeft + viewportDimensions.width;
    var viewportBottom = viewportTop + viewportDimensions.height;

    // work out how much space we have in the viewport around our popup owner
    var spaceLeft = ownerLeft - viewportLeft;
    var spaceRight = viewportRight - ownerRight;
    var spaceTop = ownerTop - viewportTop;
    var spaceBottom = viewportBottom - ownerBottom;

    var x, y; // the position of the popup

    // will it fit to the right, top-aligned with its owner?
    if( (popupWidth < spaceRight) && (popupHeight < (spaceBottom + ownerHeight)) )
    {
      x = ownerRight;
      y = ownerTop;
    }
    else if( popupWidth < spaceRight )
    {
      x = ownerRight;
      y = ownerCentreY - (popupHeight - spaceBottom);
      y = (y <= 0 ? 0 : y);
    }
    else if( (popupHeight < spaceTop) && (popupHeight > spaceBottom) )
    {
      x = ownerCentreX - Math.round( popupWidth / 2 );
      // if the popup goes over the right hand edge of the viewport, then move it left so that it doesn't
      if( x + popupWidth > viewportRight ){ x = viewportRight - popupWidth; }
      x = (x < 0 ? 0 : x);
      y = ownerTop - popupHeight;
    }
    else
    {
      x = ownerCentreX - Math.round( popupWidth / 2 );
      // if the popup goes over the right hand edge of the viewport, then move it left so that it doesn't
      if( x + popupWidth > viewportRight ){ x = viewportRight - popupWidth; }
      x = (x < 0 ? 0 : x);
      y = ownerBottom;
    }

    return { x: x, y: y };
  }
}

TuiUtil.loadStylesheet("/cms-cps/tracs/css/js.css", "screen");

TuiUtil.addLoadEvent(tuiOverlay.init);