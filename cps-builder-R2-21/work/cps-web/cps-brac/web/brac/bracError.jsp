<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>BRAC Web Application</title>
<link rel="stylesheet" href="/cms-cps/brac/css/brac_main.css" type="text/css" />
  <link rel="stylesheet" href="/cms-cps/brac/css/brac_default.css" type="text/css" />
</head>
<body oncontextmenu="return false;">



<div style="position: absolute; visibility: hidden; z-index: 1000;" id="overDiv"></div>
<div id="container">

    <div id="header">
     <%--*******Include header.jsp************--%>
        <jsp:include page="header.jsp"/>
   </div>
   <div id="content">
       <div id="body_maincontainer">
           <div class="passhead3">
              <img width="25" height="25" alt="" src="/cms-cps/brac/images/warning.gif"/>
              <h6>
                   Error
              </h6>
           </div>
           <div class="passdetails">
               <div id="globalWarnings" class="warning">
                  <p style="clear: both ! important; width: 100%;">
                       An unexpected error has occured. Please contact IT Helpdesk for further support.
                  </p>
                  <p style="clear: both ! important; width: 100%;">
                      Please try your request again or contact technical support.
                  </p>
               </div><%--End of globalWarnings --%>
               <form name="SearchByRefForm" method="post" action="<c:out value='${bookingComponent.clientDomainURL}'/>/brac/">
                     <input type="submit" class="btn" value="Continue" name="back"/>
               </form>
           </div><%--End of passdetails --%>
       </div><%--End of body_maincontainer --%>
   </div><%--End of content --%>


<div class="clearb"/>
</div><%--End of container --%>

</body>
</html>