<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="maxTransactions" value="${bookingComponent.noOfTransactions}"/>
 <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
       <c:if test="${costingLine.itemDescription == 'TOTAL_HOLIDAY_COST'}">
           <c:set var="holidayCost" value="${costingLine.amount.amount}" scope="request"/>
       </c:if>
       <c:if test = "${costingLine.itemDescription == 'Outstanding Balance' }">
           <c:set var = "outstandingBalance" value="${costingLine.amount.amount}" scope="request"/>
       </c:if>
        <c:if test = "${costingLine.itemDescription == 'Total Charges'}">
          <c:set var = "totalCharges" value ="${costingLine.amount.amount}" scope= "request"/>
      </c:if>
   </c:forEach>

<script type="text/javascript">
   var maxTransactions = <c:out value="${maxTransactions}" />;
   var fullBalanceAmount = "<c:out value="${outstandingBalance}" />";
   var depositAmount = "<c:out value="${depositAmount}" />";
   var chargesAmount = "<c:out value="${totalCharges}" />";
   var balanceDue = "<c:out value="${balanceDue}" />";
   var newBalanceDue = "<c:out value="${newBalance}" />";
   var additionalPaymentOptionBalanceDue = "<c:out value="${additionalPaymentOptionBalanceDue}" />";
   var depositOptionBalanceDue = "<c:out value="${depositOptionBalanceDue}" />";
   var refundPaymentOptionBalanceDue = "<c:out value="${refundPaymentOptionBalanceDue}" />";
   var compensationChequeAmount ="<c:out value = "${compensationAmount}"/>"
   var creditCards = new Array();
   var lv_payment_creditCardsONLCharge = new Array();
   var lv_payment_creditCardsOFLCharge = new Array();
   var creditCardsPP = new Array();
   var debitCards = new Array();
   var lv_Amount_Types = new Array();
   var lv_Charge_Types = new Array();

   var lv_No_Refund = false;

</script>
 <script type="text/javascript">
      var numberOfSeniorPassengers =0// "<c:out value='${TravelOptionsFormBean.passengersOver65}'/>";
      var NumberOfAdults =2;//"<c:out value='${fhBookingContext.searchCriteria.partyComposition.partySize}'/>";
      var lessSeniorPassengersMessage="Please select the appropriate number of senior citizens as chosen in previous page";
      var moreSeniorPassengersMessage="Please select the appropriate number of senior citizens, navigate to previous page if required.";
      var surnameValidationError="We're sorry but we cannot book you onto your chosen flight.  Would you like to select another flight and try again.";
      var supplierSystem="<c:out value='${bookingComponent.flightSummary.flightSupplierSystem}'/>";
      var supplierAmadeus="Amadeus";
   </script>

<div class="rightPanelMain">
   <%-- Body section --%>
  <form name="paymentdetails" action="/cps/processPayment?token=<c:out value='${param.token}'/>&amp;tomcat=<c:out value='${param.tomcat}'/>" onsubmit="return  CheckSeniorPassengerAndValidate();win_showWaitWindow('Processing...');" method="post" >
  <input type="hidden" name="tomcatInstance" id="tomcatInstance" value="<c:out value='${param.tomcat}'/>"/>
  <input type="hidden" name="token" id="token" value="<c:out value='${param.token}'/>"/>


 <%-- ************************CSS FileBriefdata *************************************--%>
 <c:if test="${bookingComponent.searchType == 'CSS Mode'}">
    <div id="content">
         <div id="body_maincontainer">
             <div class="css_file_brief_details">
               <p>File Brief ID:<c:out value="${bookingComponent.bookingData.fileBriefID}"/>
               <p>Status:<c:out value="${bookingComponent.bookingData.fileBriefStatus}" escapeXml="false"/></p>
      </div>
           </div>
      </div>
        </c:if>


<%-- ************************CSS FileBriefdata *************************************--%>

  <h3>Payment</h3>
<%-- ************************Warning section*************************************--%>
  <c:if test="${not empty bookingComponent.errorMessage && empty responseMsg}">
       <div class="paymentPanel">
           <h4>Warning Message</h4>
           <p style="font: bold 20px; font-weight: bold; font-size: 1.1em; color: red;">
               <c:out value="${bookingComponent.errorMessage}"/>
           </p>
       </div>
   </c:if>
<%-- ************************End Warning section*************************************--%>
<%-- ************************Important Information section*************************************--%>
   <c:if test="${not empty bookingComponent.importantInformation.errata}">
       <div class="paymentPanel">
           <h4>Important Information</h4>
           <p>
               <c:forEach var="accomerrata" items="${bookingComponent.importantInformation.errata}">
                   <c:if test="${accomerrata != 'No errata' &&  not empty accomerrata}">
                       <c:out value="${accomerrata}" />
                   </c:if>
               </c:forEach>
           </p>
           <div id="Information" class="paymentPanelTandC">
               <span><input id="impInfoAccept" name="impInfo" type="checkbox" value="Accept" /></span>
            <label for="impInfoAccept">I have read and accept these changes.</label>
               <div class="clearb"></div>
           </div>
       </div>
   </c:if>
<%-- ************************End Important Information section*************************************--%>


<%-- ************************Payment Amount section*************************************--%>
  <jsp:include page="bracPaymentAmount.jsp"/>
<%-- ************************End of Payment Amount section*************************************--%>

<%--************************Payment details panel*****************************--%>
 <input type="hidden" value="0" name="totalAmountDue" id="totalAmountDue" />
       <jsp:include page="bracPaymentDetails.jsp"/>

<%--************************End of Payment details panel*****************************--%>

<%--************************Terms and conditions*****************************--%>
<div class="paymentPanel">
   <h4>Terms and Conditions</h4>
   <div class="paymentPanelTandC">
               <p><a href='javascript:Popup("<c:out value="${bookingComponent.clientDomainURL}"/>/brac/page/payment/termsandconditions.page",690,510,"scrollbars");'>View Terms and Conditions</a></p>
       <span><input id="TandCAccept" name="TandCAccept" type="checkbox" value="Accept" alt="You must read and accept our Terms and Conditions before confirming your booking.|Y|CHECK" /></span>
      <label for="TandCAccept">I have read and accept the Terms and Conditions.</label>
       <div class="clearb"></div>
   </div>
</div>
<%--************************End Terms and conditions*****************************--%>

<%--************************Print confirmation section*****************************--%>
  <c:if test="${bookingComponent.callType eq 'Retail'}">
<div class="paymentPanel">
   <div class="paymentPanelTandC">
       <span><input id="printLocally" name="printLocally" type="checkbox" onclick="javascript:setFlag('printLocal',this)"/></span>
      <label for="printLocally">Print confirmation locally.</label>
       <div class="clearb"></div>


<c:choose>
<c:when test="${bookingComponent.callType eq 'Retail' && (bookingComponent.nonPaymentData['atol_flag']) && (bookingComponent.nonPaymentData['atol_protected'])&&(bookingComponent.nonPaymentData['atol_protected'] !=null)}">


       <span><input id="printAtolLocally" name="printAtolLocally" type="checkbox" onclick="javascript:setAtolPrintFlag('printAtolLocal',this)" disabled="true"/></span>
       <label for="printAtolLocally">Print ATOL certificate locally.</label>
       <div class="clearb"></div>


</c:when>
 <c:otherwise>

 </c:otherwise>
 </c:choose>
 </div>
</div>
</c:if>

<%--************************End Print confirmation section*****************************--%>

<%--************************Buttons section*****************************--%>
<br clear="all"/>
<div class="paymentPanelButtonContainer buttonRowDiv">
    <button class="naviButton rightButton" type="button" onclick="return CheckSeniorPassengerAndValidate() && win_showWaitWindow('Processing...')"  name="Next" title="Next">Confirm</button>
    <button class="naviButton rightButton" type="cancel" name="cancelButton" title="Cancel Details" onclick="general_returnToCheckpoint('<c:out value='${bookingComponent.clientDomainURL}'/>');return false;">Check Point</button>
    <c:if test="${not empty bookingComponent.importantInformation.description['ProductionMode']&& bookingComponent.importantInformation.description['ProductionMode'] ne true}">
        <a href='javascript:Popup("<c:out value='${bookingComponent.clientDomainURL}'/>/brac/page/mail/testexceptionemail.page",440,400,"scrollbars");'> Test Exception Emails </a>
        <div class="buttonRowDiv">
            <button class="naviButton rightButton" type="button" onclick="general_Testprint('<c:out value='${bookingComponent.clientDomainURL}'/>');">Test Print</button>
        </div>
    </c:if>
</div>
</div>



