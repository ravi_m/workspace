<%@include file="/common/commonTagLibs.jspf"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.tui.uk.payment.domain.PaymentTransaction,
                 java.util.List,
                 com.tui.uk.payment.domain.DataCashPaymentTransaction"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var='clientapp' value='${bookingComponent.clientApplication.clientApplicationName}'/>
<%
	String clientApp = (String)pageContext.getAttribute("clientapp");
    String manualSegments = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".manualSegments" , "");
    pageContext.setAttribute("manualSegments", manualSegments, PageContext.REQUEST_SCOPE);
%>
<script>
var manualSegments = '<c:out value="${manualSegments}"/>' ;
var configurableCardCharge = <%= com.tui.uk.config.ConfReader.getConfEntry("cardChargePercent", "")%>;
</script>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>BRAC Web Application</title>
  <meta http-equiv="Content-Type" content="text/html;" />
  <style>
    .transactionRef
   {
       font-weight:bold;
       margin:5px 0pt 0pt 5px;
    }
  </style>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
  <link rel="stylesheet" href="/cms-cps/brac/css/brac_main.css" type="text/css" />
  <link rel="stylesheet" href="/cms-cps/brac/css/brac_default.css" type="text/css" />
  <link rel="stylesheet" href="/cms-cps/brac/css/windows.css" type="text/css" />
  <link rel="stylesheet" href="/cms-cps/brac/css/integrated_payments.css" type="text/css"/>

  <script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
  <script type="text/javascript" src="/cms-cps/brac/js/windows.js"></script>
  <script type="text/javascript" src="/cms-cps/common/js/paymentUpdation.js"></script>
  <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
  <script type="text/javascript" src="/cms-cps/common/js/paymentPanelContent.js"></script>
  <script type="text/javascript" src="/cms-cps/common/js/paymentPanelValidation.js"></script>
  <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
  <script type="text/javascript" src="/cms-cps/common/js/pinpadProcessor.js"></script>
  <script type="text/javascript" src="/cms-cps/brac/js/brac_Payment.js"></script>

  <%-- This condition must be removed once manual segments go live. --%>
  <c:choose>
   <c:when test="${manualSegments}">
       <script type="text/javascript" src="/cms-cps/brac/js/manualSegments/formvalidation.js"></script>
       <script type="text/javascript" src="/cms-cps/brac/js/manualSegments/paymentdetails.js"></script>
   </c:when>
   <c:otherwise>
       <script type="text/javascript" src="/cms-cps/brac/js/formvalidation.js"></script>
       <script type="text/javascript" src="/cms-cps/brac/js/paymentdetails.js"></script>
   </c:otherwise>
  </c:choose>
</head>
<body  oncontextmenu="return false;">
   <%-- Pinpad DLL initialization starts here --%>
    <object name="moATS" classid="clsid:FB6D3933-5128-4B13-AD9D-06B6B18C29F1" declare="declare" style="display:none;"></object>
    <script type="text/vbscript">
        On Error Resume Next
        Err.Clear
        If Not moATS.Initialise("C:\Program Files\TTG\Rapid\Database\TUICNP.DAT") Then
            MsgBox("Failed to Initialise:" & moATS.LastError)
        Else
            If moATS.Connect() = false then
                MsgBox("Failed to Connect:" & moATS.LastError)
            End If
        End If
        On Error Resume Next
        Err.Clear

        Sub moATS_StatusChanged(message)
           startGetMsg()
        End Sub

        Sub moATS_Complete(isProcessCompletedDLL)
            isProcessCompleted = isProcessCompletedDLL
        End Sub
    </script>
   <%-- Pinpad DLL initialization ends here --%>
<%--Since pan and security code are in form of character array we are converting them to String--%>
<%
if (request.getAttribute("paymentTransactions") != null)
{

 for(PaymentTransaction paymentTransaction : (List<PaymentTransaction>)request.getAttribute("paymentTransactions"))
 {
 if (paymentTransaction instanceof DataCashPaymentTransaction)
 {
  pageContext.setAttribute("paymentTransaction",paymentTransaction);
 %>
  <c:forEach var="pan" items="${paymentTransaction.card.pan}">
    <c:set var="pandata" value="${pandata}${pan}" scope="request"/>
  </c:forEach>
  <c:forEach var="securitycode" items="${paymentTransaction.card.cv2}">
    <c:set var="securitycodedata" value="${securitycodedata}${securitycode}" scope="request"/>
  </c:forEach>
   <c:set var="responseMsg" value="${transactionResponseMsg}" scope="request"/>
 <% }}}%>

   <%-- Page section start --%>
   <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000"></div>
   <div id="container">

      <div id="header">
     <%--*******Include header.jsp************--%>
     <jsp:include page="header.jsp"/>

     </div>

      <div id="left_panel">
     <%--*******Include leftPanel.This condition should be removed once manual segments will go live.************--%>
     <c:choose>
       <c:when test="${manualSegments}">
         <jsp:include page="manualSegments/sidePanel.jsp"/>
       </c:when>
       <c:otherwise>
         <jsp:include page="sidePanel.jsp"/>
       </c:otherwise>
     </c:choose>
     </div>

      <div id="content">
         <div id="body_maincontainer"></div>
      </div>

      <div id="right_panel">
         <div id="rightPanel_MainContainer">
       <%--*******Include rightPanel" ************--%>
       <c:choose>
       <c:when test="${manualSegments}">
         <jsp:include page="manualSegments/payment.jsp"/>
       </c:when>
       <c:otherwise>
         <jsp:include page="payment.jsp"/>
       </c:otherwise>
       </c:choose>
       </div>
      </div>

      <div id="footer"></div>
      <div class="clearb"></div>

    </div>

    <div id="waitMessage" class="pleaseWaitPopup hide" align="center">
      <br />
      <br />
      <br />
      <img id="win_loadingImage" src="/cms-cps/brac/images/loading.gif" alt="Please Wait..."/>
         <br />Please wait...
    </div>
</body>
</html>
