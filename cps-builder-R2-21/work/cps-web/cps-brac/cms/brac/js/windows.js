/**
  */

var win_gPopupMask = null;
var win_gPopupContainer = null;
var win_gPopupFrame = null;
var win_gPopupTitleBar = null;
var win_gPopupIsShown = false;
var win_gLoadingImage = new Image();

var win_gTabIndexes = new Array();
var win_cancelDelayedPopup = true;

// Pre-defined list of tags we want to disable/enable tabbing into
var win_gTabbableTags = new Array("A","BUTTON","TEXTAREA","INPUT","IFRAME");

// If using Mozilla or Firefox, use Tab-key trap.
if (!document.all) {
	document.onkeypress = win_keyDownHandler;
}

/**
  * Pop up a modal window with specified content url and default attributes.
  *
  * @argument url - url to display
  */
function win_showStdPopupWindow(url, title) {
    win_showPopupWindow(url, 700, 500, title, false);
}

/**
 * Pop up a modal window with specified attributes and content url.
 *
 * @argument url - url to display
 * @argument width - width in pixels
 * @argument height - height in pixels
 * @argument title - title of popup window
 * @argument showCloseBox - whether to show the close box
 */
function win_showPopupWindow(url, width, height, title, showCloseBox) {
   win_hidePopupWindow();
	win_showEmptyPopupWindow("Page", width, height, title, showCloseBox);
    ajax_loadContent(url, win_gPopupFrame.id);
}

/**
 * Show a standard window with error/warning/info messages
 *
 * @param messageContentId  id of the element containing message content
 */
function win_showMessageWindow(messageContentId)
{
    var messageContent = document.getElementById(messageContentId);
    var w = 300;//messageContent.innerWidth + 50;
    var h = 150;//messageContent.innerHeight + 50;
    win_showEmptyPopupWindow("Message", w, h, "Messages", true);

    // show messages
    win_setContent(messageContent);
    messageContent.style.display = "block";
}


/**
 * Show a standard window with error/warning/info messages
 *
 * @param messageContentId  id of the element containing message content
 * @param showCloseBox  optional parameter indicating whether a close
 *      button should appear on the title bar; defaults to false
 */
function win_showContentWindow(messageContentId, w, h, title, showCloseBox)
{
    win_showEmptyPopupWindow("Content", w, h, title, showCloseBox);

    // show messages
    var messageContent = document.getElementById(messageContentId);
    win_setContent(messageContent);
    messageContent.style.display = "block";
}

/**
 * Show a standard window with a wait message
 * @param title optional title. If not specified, default generic title is used.
 */
function win_implementWaitWindow(title)
{
   if(win_cancelDelayedPopup)
   {
      return true;
   }

   if(!title)
   {
      title = "Working - please wait...";
   }

   win_showEmptyPopupWindow("Wait", 300, 150, title, false);

   // show messages
   var messageContent = document.getElementById("waitMessage");
   win_setContent(messageContent);
   messageContent.style.display = "block";
   win_gPopupIsShown = true;

   return true;
}

function win_setContent(content)
{
   while(win_gPopupFrame.firstChild)
   {
      win_gPopupFrame.removeChild(win_gPopupFrame.firstChild);
   }
   win_gPopupFrame.appendChild(content);
}

/**
 * Shows the wait window after a delay. Use this when showing the wait window
 * on submission of a form and the wait window contains animation, because
 * IE stops animation when a form is submitted.
 */
function win_showWaitWindow(title)
{
   var param = "";
   if (title)
   {
      param = "'" + title + "'";
   }

   win_cancelDelayedPopup = false;
   setTimeout("win_implementWaitWindow(" + param + ");", 200);
   return true;
}


/**
 * First part of pop up window show logic.
 *
 * @argument url - url to display
 * @argument width - int in pixels
 * @argument height - int in pixels
 * @argument title - title of popup window
 * @argument showCloseBox - if true, shows the close box
 */
function win_showEmptyPopupWindow(windowId, width, height, title, showCloseBox) {
	win_preparePopup(windowId, title, showCloseBox ? "win_hidePopupWindow" : null);

	win_gPopupIsShown = true;
	win_disableTabIndexes();
	win_gPopupMask.style.display = "block";
	win_gPopupContainer.style.display = "block";

	// calculate where to place the window on screen
	win_centerPopupWindow(width, height);

	var titleBarHeight = parseInt(win_gPopupTitleBar.offsetHeight, 10);
	win_gPopupContainer.style.width = width + "px";
	win_gPopupContainer.style.height = (height + titleBarHeight) + "px";

	win_setMaskSize();

	// need to set the width of the iframe to the title bar width because of the dropshadow
	// some oddness was occuring and causing the frame to poke outside the border in IE6
    // N.B. iframe no longer used, but code left in for now...
   win_gPopupFrame.style.width = parseInt(win_gPopupTitleBar.offsetWidth, 10) + "px";
   win_gPopupFrame.style.height = (height) + "px";

   win_setSelectBoxes("hidden");
}

/**
 * Prepares the mask and pop up window markup in hidden <div>s.
 */
function win_preparePopup(windowId, title, onClose) {
    win_prepareMask();
    win_preparePopupWindow(windowId, title, onClose);
}

/**
 * Creates components with the following names:
 *   win_<windowId>_Container:   the popup container window
 *   win_<windowId>_Inner:       the inner area of the container
 *   win_<windowId>_TitleBar:    the title bar
 *   win_<windowId>_Title:       the title area of the title bar
 *   win_<windowId>_Controls:    the controls area of the title bar
 *   win_<windowId>_Frame:       the content frame
 *   win_<windowId>_Close:       the close button (if present)
 *
 * @param windowId  name of the window
 * @param title     title to display
 * @param onClose   if specified, a close control is added which calls the identified function.
 */
function win_preparePopupWindow(windowId, title, onClose) {
    var containerId = "win_" + windowId + "_Container";
    var frameId = "win_" + windowId + "_Frame";
    var innerId = "win_" + windowId + "_Inner";
    var titleBarId = "win_" + windowId + "_TitleBar";
    var titleId = "win_" + windowId + "_Title";
    var controlsId = "win_" + windowId + "_Controls";
    var closeButtonId = "win_" + windowId + "_Close";
    var closeSymbolCode = "";
    if (onClose != null)
    {
        closeSymbolCode = "<button class='win_popupClose' onclick='" + onClose + "();'>X</button>";
    }
    // create the container if it doesn't already exist
    if(document.getElementById(containerId) == null) {
        var popupContainer = document.createElement('div');
        popupContainer.id = containerId;
        popupContainer.className = "win_popupContainer";
        popupContainer.innerHTML =
            '<div id="' + innerId + '" class="win_popupInner">' +
                '<div id="' + titleBarId + '" class="win_popupTitleBar">' +
                    '<div id="' + titleId + '" class="win_popupTitle"></div>' +
                    '<div id="' + controlsId + '" class="win_popupControls">' + closeSymbolCode + '</div>'
                    + '</div>' +
                 '<div id="' + frameId + '" class="win_popupFrame"></div>' +
            '</div>';
        var theBody = document.getElementsByTagName('BODY')[0];
        theBody.appendChild(popupContainer);
    }

    document.getElementById(titleId).innerHTML = title;

    win_gPopupContainer = document.getElementById(containerId);
    win_gPopupFrame = document.getElementById(frameId);
    win_gPopupTitleBar = document.getElementById(titleBarId);
}

/**
 * Prepares the mask.
 */
function win_prepareMask() {
    var popupMaskId = "win_popupMask";

    if(document.getElementById(popupMaskId) == null) {
        var popupMask = document.createElement('div');
        popupMask.id = popupMaskId;
        popupMask.className = "win_popupMask";
        var theBody = document.getElementsByTagName('BODY')[0];
        theBody.appendChild(popupMask);
    }

    win_gPopupMask = document.getElementById(popupMaskId);
}

/**
 * Moves the popup container to the centre of the browser window.
 */
function win_centerPopupWindow(width, height) {
	if (win_gPopupIsShown) {
		if (width == null || isNaN(width)) {
			width = win_gPopupContainer.offsetWidth;
		}
		if (height == null) {
			height = win_gPopupContainer.offsetHeight;
		}

		var theBody = document.getElementsByTagName("BODY")[0];
		theBody.style.overflow = "hidden";
		var scTop = parseInt(win_getVerticalScroll(), 10);
		var scLeft = parseInt(theBody.scrollLeft, 10);

		win_setMaskSize();

		var titleBarHeight = parseInt(win_gPopupTitleBar.offsetHeight, 10);
		var fullHeight = win_getViewportHeight();
		var fullWidth = win_getViewportWidth();
		win_gPopupContainer.style.top = (scTop + ((fullHeight - (height + titleBarHeight)) / 2)) + "px";
		win_gPopupContainer.style.left =  (scLeft + ((fullWidth - width) / 2)) + "px";
	}
}

// Re-centre the popup window when the browser scrolls
win_addEvent(window, "resize", win_centerPopupWindow);
win_addEvent(window, "scroll", win_centerPopupWindow);
window.onscroll = win_centerPopupWindow;


/**
 * Sets the size of the popup mask.
 *
 */
function win_setMaskSize() {
	var theBody = document.getElementsByTagName("BODY")[0];

	var fullHeight = win_getViewportHeight();
	var fullWidth = win_getViewportWidth();

	// Determine what's bigger, scrollHeight or fullHeight / width
    var popHeight;
	if (fullHeight > theBody.scrollHeight) {
        popHeight = fullHeight;
    } else {
		popHeight = theBody.scrollHeight;
	}

    var popWidth;
    if (fullWidth > theBody.scrollWidth) {
		popWidth = fullWidth;
	} else {
		popWidth = theBody.scrollWidth;
	}

	win_gPopupMask.style.height = popHeight + "px";
	win_gPopupMask.style.width = popWidth + "px";
}

/**
 */
function win_hidePopupWindow() {
   win_cancelDelayedPopup = true;

   if(win_gPopupIsShown)
   {
   	win_gPopupIsShown = false;

      var theBody = document.getElementsByTagName("BODY")[0];
      theBody.style.overflow = "";

      win_restoreTabIndexes();

      win_gPopupMask.style.display = "none";
      win_gPopupContainer.style.display = "none";

      // display all select boxes
      win_setSelectBoxes("visible");
   }
}

/**
 * Tab key trap. iff popup is shown and key was [TAB], suppress it.
 * @argument e - event - keyboard event that caused this function to be called.
 */
function win_keyDownHandler(e) {
    return !(win_gPopupIsShown && e.keyCode == 9);
}

/**
 * For IE.  Go through predefined tags and disable tabbing into them.
 */
function win_disableTabIndexes() {
	if (document.all) {
		var i = 0;
		for (var j = 0; j < win_gTabbableTags.length; j++) {
			var tagElements = document.getElementsByTagName(win_gTabbableTags[j]);
			for (var k = 0 ; k < tagElements.length; k++) {
				win_gTabIndexes[i] = tagElements[k].tabIndex;
				tagElements[k].tabIndex="-1";
				i++;
			}
		}
	}
}

/**
 * For IE. Restore tab-indexes.
 */
function win_restoreTabIndexes() {
	if (document.all) {
		var i = 0;
		for (var j = 0; j < win_gTabbableTags.length; j++) {
			var tagElements = document.getElementsByTagName(win_gTabbableTags[j]);
			for (var k = 0 ; k < tagElements.length; k++) {
				tagElements[k].tabIndex = win_gTabIndexes[i];
				tagElements[k].tabEnabled = true;
				i++;
			}
		}
	}
}


/**
* Hides all drop down form select boxes on the screen so they do not appear above the mask layer.
* IE has a problem with wanted select form tags to always be the topmost z-index or layer
*
* Thanks for the code Scott!
*/
function win_setSelectBoxes(visibility) {
    var brsVersion = parseInt(window.navigator.appVersion.charAt(0), 10);
    if (brsVersion <= 6 && window.navigator.userAgent.indexOf("MSIE") > -1) {
        for(var i = 0; i < document.forms.length; i++) {
            for(var e = 0; e < document.forms[i].length; e++){
                if(document.forms[i].elements[e].tagName == "SELECT") {
                    document.forms[i].elements[e].style.visibility = visibility;
                }
            }
        }
    }
}

/**
 * Event handler attachment
 * TH: Switched first true to false per http://www.onlinetools.org/articles/unobtrusivejavascript/chapter4.html
 *
 * @argument obj - the object to attach event to
 * @argument evType - name of the event - DONT ADD "on", pass only "mouseover", etc
 * @argument fn - function to call
 */
function win_addEvent(obj, evType, fn){
 	if (obj.addEventListener){
		obj.addEventListener(evType, fn, false);
		return true;
	} else if (obj.attachEvent){
		return obj.attachEvent("on"+evType, fn);
	} else {
		return false;
	}
}

/**
 * Event handler detachment
 *
 * @argument obj - the object to attach event to
 * @argument evType - name of the event - DONT ADD "on", pass only "mouseover", etc
 * @argument fn - function to call
 */
function win_removeEvent(obj, evType, fn, useCapture){
	if (obj.removeEventListener){
		obj.removeEventListener(evType, fn, useCapture);
		return true;
	} else if (obj.detachEvent){
		return obj.detachEvent("on"+evType, fn);
	} else {
		alert("Handler could not be removed");
        return false;
    }
}

/**
 * Gets the viewport height
 */
function win_getViewportHeight() {
	if (window.innerHeight != window.undefined)
		return window.innerHeight;
	else if (document.compatMode == 'CSS1Compat')
		return document.documentElement.clientHeight;
	else if (document.body)
		return document.body.clientHeight;
	else
		return window.undefined;
}

/**
 * Gets the viewport width
 */
function win_getViewportWidth() {
	if (window.innerWidth != window.undefined)
		return window.innerWidth;
	else if (document.compatMode == 'CSS1Compat')
		return document.documentElement.clientWidth;
	else if (document.body)
		return document.body.clientWidth;
	else
		return window.undefined;
}

/**
 * Gets the vertical scroll offset
 */
function win_getVerticalScroll() {
	if (self.pageYOffset)
		return self.pageYOffset; // all except Explorer
	else if (document.documentElement && document.documentElement.scrollTop)
		return document.documentElement.scrollTop; // Explorer 6 Strict
	else if (document.body)
		return document.body.scrollTop; // all other Explorers
    else return 0;
}

/**
 * Gets the horizontal scroll offset
 */
function win_getHorizontalScroll() {
	if (self.pageXOffset)
		return self.pageXOffset; // all except Explorer
	else if (document.documentElement && document.documentElement.scrollLeft)
		return document.documentElement.scrollLeft; // Explorer 6 Strict
	else if (document.body)
		return document.body.scrollLeft; // all other Explorers
    else return 0;
}


