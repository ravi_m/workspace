var maxAlert='Discount offered is too high, please re-enter.'
var alertValuetd="Please enter a valid today\'s saving value. It accepts only Numerics upto 5 digits.(e.g.:xxx.xx)"
alertValue="Please enter a valid PB/PM. It accepts only Numerics upto 5 digits.(e.g.:xxx.xx)";
var displayPropertiesFlag = false;
var cssCompensationTotal=0;
var ns4 = (document.layers);
var ie4 = (document.all && !document.getElementById);
var ie5 = (document.all && document.getElementById);
var ie6 = (navigator.userAgent.indexOf("MSIE 6.0")!=-1);
var ns6 = (!document.all && document.getElementById);
var mac = (navigator.userAgent.indexOf("Mac") != -1);
var moz = (navigator.userAgent.indexOf("Netscape") == -1);
var popupWin = '';
var isFlyDrive=false;
var isAdditional=false;
var valid =true;


// Validate form for submission
var isCheck=true;
function ValidateForm(formobj) {
 valid = confirm("Are you sure you want to process the amendments and payments on this booking?");
  if(!radioAlert)
  {
  alert("Transaction Amount exceeds selected transaction amount");
  return false;
  }
  if (!valid)
   return false;
  if(!validateReaccreditationfileds())
   {
      return false;
   }
 // Setting the isAdditional variable if the selected deposit type is additional.
 objBalanceType = document.getElementsByName("depositType");
         for (var j = 0; j < objBalanceType.length; j++)
         {
  if (objBalanceType[j].checked && (objBalanceType[j].value == "Additional" || objBalanceType[j].value =="ADDITIONAL"))
               {
        isAdditional=true;
               }
        }
  if (validateInsTypeAndCustTypeFields()){
  for (i=0;i<formobj.elements.length;i++) {
   obj = formobj.elements[i]
   if(obj.name == 'importantinfo')
   {
     if ((obj.type == 'checkbox') && (!obj.checked) )
      {
          return SetFocus('Please read and accept the important additional information before continuing with your booking',obj)
      }
   }
   if(obj.name == 'importantInformationChecked')
   {
     if ((obj.type == 'checkbox') && (!obj.checked) )
      {
          return SetFocus('Please read and accept the important additional information before continuing with your booking',obj)
      }
   }
   if(obj.name == 'agree')
   {
     if ((obj.type == 'checkbox') && (!obj.checked) )
      {
          return SetFocus('Please read and accept our Privacy Policy before confirming your booking',obj)
      }
   }
  if(obj.name == 'baggagePolicy' && document.getElementById('bagg').style.display == 'block' )
   {
     if ((obj.type == 'checkbox') && (!obj.checked) )
      {
          return SetFocus('Please read and accept our baggage Policy before continuing with your booking',obj)
      }
   }
   if(TrimSpaces(obj.value) == 'select') {
      return SetFocus('Please select the nature of your e-mail.',obj)
   }

   // Identify Validation required - check for disabled
   if (obj.getAttribute("alt")) {

   disabledlayer = false
   obj1 = obj

   // Find if obj is on a DIV
   while (obj1.tagName != 'BODY') {
   if (obj1.tagName == 'DIV' ||obj1.tagName == 'DL') {
    if (obj1.style.display == 'none') {
     disabledlayer = true
     break;
    }
   }
    obj1 = obj1.parentNode
   }

   if ((obj.getAttribute("alt") != '') && (!disabledlayer)) {
   valset = obj.getAttribute("alt").split("|")

   // Trim input
   obj.value = TrimSpaces(obj.value)

   obj_value = obj.value
   obj_name = valset[0];
   if (valset[1] == 'Y') {
    obj_mandatory = true
   } else {
    obj_mandatory = false
   }
   obj_type = valset[2]
   obj_params = valset.slice(3)
   //alert(obj_mandatory)
   // Mandatory

   if (obj_mandatory) {
    if ((obj_value == '') && ((obj.type == 'text') || (obj.type == 'textarea') || (obj.type == 'password'))) return SetFocus(obj_name, obj)
    if ((!obj.checked) && (obj.type == 'checkbox')) return SetFocus(obj_name,obj)

         if (obj.name == 'partPaymentType' && (!obj.checked))
   {
      if (isCheck)
      {
         isCheck=false;
         return SetFocus(obj_name, obj)
      }
   }

   // Check selection has ben made on selection boxes
    if (obj.type.substr(0,6) == 'select') {
    if (obj.options.selectedIndex==0) {
      return SetFocus(obj_name, obj)
      }
    }
   }

   // Validation Type
   if (obj_type) {
    switch (obj_type.toUpperCase()) {
     // Alphanumeric 0-9 a-z A-Z Space Hyphen / \
    case 'ALPHA':
            var Pat1 = /^[a-zA-Z -]*$/;
            if (!Pat1.test(obj_value)) { return SetFocus('Please enter valid details' ,obj) }
            break;

    case 'ALPHANUMERIC':
            var Pat1 = /^[-a-zA-Z0-9 \\\/.,]*$/;
            if (!Pat1.test(obj_value)) { return SetFocus('Please enter valid details',obj) }
            break;

   case 'ALPHANUMERICONLY':
            var Pat1 = /^[a-zA-Z0-9]*$/;
            if (!Pat1.test(obj_value)) { return SetFocus('Please enter valid details',obj) }
            break;


    // Alphanumeric with Extended characters
    case 'ANEXTENDED':
            var Pat1 = /^[-a-zA-Z0-9 \\\/.,\x27!;@:"?\$%\^&\*()]*$/;
            if (!Pat1.test(obj_value)) { return SetFocus('Please enter valid details',obj) }
            break;

    // Alphanumeric with Extended characters for Password
    case 'PASSWORD':
            var Pat1 = /^[-a-zA-Z0-9 \\\/.,\x27!;@:"#?\$%\^&\*()]*$/;
            if (!Pat1.test(obj_value)) { return SetFocus('Please enter valid details',obj) }
            break;

    // Insurer Name
    case 'INSURERDETAILS':
            var Pat1 = /^[^&]*$/;
            if (!Pat1.test(obj_value)) { return SetFocus('Please enter valid details.\n(Invalid characters are &)',obj) }
            break;

     // Numeric 0-9 NUMERIC|Min|Max
    case 'NUMERIC':
            var Pat1 = /^[0-9]*$/;
            if (!Pat1.test(obj_value)) {
               if(valset[3]=='olbp') return SetFocus('The details you have entered have not been recognised - please re-enter',obj);
               else return SetFocus('Please enter a valid number',obj);
               }
            // Perform Range Checking
            if (obj_params != '') {
             obj_value = parseFloat(obj_value)
             // Minimum
             if (obj_params[0]) {
                if (obj_value < obj_params[0]) { return SetFocus('Please enter a valid number',obj) }
             }
             // Maximum
             if (obj_params[1]) {
                if (obj_value > obj_params[1]) { return SetFocus('Please enter a valid number',obj) }
             }
            }
            break;

    // Email Address xxx@xxx.xxx
    case 'EMAIL':
          obj_value = obj_value.toLowerCase();
            obj.value = obj_value;
            Pat1 = /^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;
            if(obj_value != '')
            {
               if (!Pat1.test(obj_value)) {
                     return SetFocus( 'Please enter a valid email address \n\ne.g. name@place.com', obj)
               }
            }
            break;

    // Address Fields
    case 'ADDRESS':
            var Pat1 = /^[-a-zA-Z0-9 ,\.\x27]*$/;
            apos = String.fromCharCode(39)
            if (!Pat1.test(obj_value)) { return SetFocus('Please enter a valid address.', obj) }
            break;

    // Name Fields
    case 'NAME':
            var Pat1 = /^[-a-zA-Z\u00C0-\u00DD\u00E0-\u00FF \.\'\x27]*$/;
            apos = String.fromCharCode(39)
            if (!Pat1.test(obj_value)) {
               if(valset[3]=='olbp') return SetFocus('The details you have entered have not been recognised - please re-enter',obj);
               else
               {
                 return SetFocus('Please enter a valid name',obj);
               }
               }
             if (!Pat1.test(obj_value)||(stripChars(obj_value,"-.' ").length == 0 && valset[1] == 'Y')) {
                   return SetFocus('Please enter valid details' ,obj) }
            break;

    case 'REVIEWNAME':
            var Pat1 = /^[-a-zA-Z&@ \"\',\.\x27]*$/;
            apos = String.fromCharCode(39)
            if (!Pat1.test(obj_value)) { return SetFocus('Please enter a valid name.' , obj) }
            break;

    // UK Postcode XX1 1XX
    case 'POSTCODE':
               obj_value = obj_value.toUpperCase();
          obj.value = obj_value;
               Pat1 = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXYU0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
               if (!Pat1.test(obj_value)) {
                  return SetFocus('We cannot validate the post code you have entered. Please check and re-enter. ',obj)
           }
            break;

    // Phone number
    case 'PHONE':
          obj.value = RemoveAllSpaces(obj.value)
            obj_value = obj.value
            var val = /(^-[0-9]+)|(^[0-9]*-$)|(^-[0-9]*-$)/;
            if(val.test(obj_value)){
               return SetFocus('Please enter a valid telephone number.', obj)
            }
            else {
               obj_value=stripChars(obj_value,"()- ");
               Pat1 = /^[0-9 ]*$/;
               if ((!Pat1.test(obj_value)) && (obj_value != '')) {
                  return SetFocus('Please enter a valid telephone number.', obj)
               }
            }
            obj.value = obj_value;
            break;

    // Credit/Debit Card number
    case 'CARDNO':

      var idIndex = (obj.name).indexOf("_cardNumber");
      var subString = (obj.name).substr(0,idIndex);
      var object = (subString)+ "_type";
      var temp=(obj.id).split("_");
      if($('payment_'+temp[1]+'_cardNumberId') && (maskedCardNumber != $('payment_'+temp[1]+'_cardNumberId').value || temp[1]!=0))
      {
   var FieldObjCardType=document.getElementById("payment_type_"+temp[1]).value;
      //alert(cardTypeValue[0])
         var cardType='';
         if(FieldObjCardType)
         {
         var cardTypeValue =(FieldObjCardType).split("|");
         cardType = cardTypeValue[0];
         }
        if(cardType=="AMERICAN_EXPRESS")
        {
            Pat1 = /^[0-9]{15}$/;
        }
        else
       {
            Pat1 = /^[0-9]{16,20}$/;
       }
       obj.value = RemoveAllSpaces(obj.value)
            obj_value = obj.value
          if (!Pat1.test(obj_value)) {

                  return SetFocus('Please enter a valid card number.',obj)
           }
     }
            break;

    // Check to see if two values of 2 fields match
    case 'MATCH':
           obj_1 = document.getElementById(obj_params[0])
           obj_2 = document.getElementById(obj_params[1])
          if (obj_1.value != obj_2.value) return SetFocus(obj_name,obj_1)
            break;

    case 'EITHER_OR':
         obj_1 = document.getElementById(obj_params[0])
           obj_2 = document.getElementById(obj_params[1])
           if( obj_1.value == '' && obj_2.value == '' ) return SetFocus(obj_name, obj_1)
           break;
    case 'AND_OR':
           obj_1 = document.getElementById(obj_params[0])
           obj_2 = document.getElementById(obj_params[1])
           if( obj_1.value == '' ) return SetFocus(obj_name, obj_1)
           break;
     case 'AND':
         obj_1 = document.getElementById(obj_params[0])
           obj_2 = document.getElementById(obj_params[1])
           if( obj_1.value == '' || obj_2.value == '' ) return SetFocus(obj_name, obj_1)
           break;

      case 'RADIO_SELECTED':
            num = obj_params.length;
            selected = false;
            for (z=0;z<num;z++)
            {
               if( document.getElementById(obj_params[z]).checked )
               {
                  selected = true;
               }
            }
            if( selected == false ) return SetFocus(obj_name, document.getElementById(obj_params[num-1]))
          break;

    case 'DATE':
           day = getElemName(obj_params[0])[0].value
           month = getElemName(obj_params[1])[0].value
            year = getElemName(obj_params[2])[0].value
            datein = day + '/' + month +'/' + year
            var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;
            var matchArray = datein.match(datePat);
           if (matchArray == null) {
               return SetFocus(obj_name,obj)
           } else {
            if ((month==4 || month==6 || month==9 || month==11) && day==31) {
                  return SetFocus(obj_name,obj)
            }
            if (month == 2) {
                var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
                if (day>29 || (day==29 && !isleap)) {
                        return SetFocus(obj_name,obj)
                }
            }
           }
           break;

     // Credit/Debit Card Date CARDDATE|START or EXPIRY|Year Field Name|Reference Month|Reference Year
    case 'CARDDATE':
              cardmonth = obj.options[obj.options.selectedIndex].value
           obj_year = document.getElementById(obj_params[1])
            cardyear = obj_year.options[obj_year.options.selectedIndex].value
         var Calendar=new Date();
            todaysmonth =Calendar.getMonth()+1;
            var calib=(ns4 || ns6)?1900:0;
         todaysyear = Calendar.getYear()+calib;
         if( obj_mandatory )
            {
              if (cardmonth == '') { return SetFocus('Please enter a valid date',obj) }
              if (cardyear == '') { return SetFocus('Please enter a valid date',obj) }
            }
            else if( (cardmonth == '' && cardyear != '') || (cardmonth != '' && cardyear == '') )
            {
              return SetFocus('Please enter a valid date',obj)
            }

            cardmonth = parseInt(cardmonth,10)
            cardyear = parseInt(cardyear,10)
            // If Start Date
             if (obj_params[0] == 'START') {
         if (((cardmonth>todaysmonth) && (cardyear == todaysyear)) || (cardyear>todaysyear)) { return SetFocus('Please enter a valid start date',obj) }
            }
            // If Expiry Date
            if (obj_params[0] == 'EXPIRY')
            {
               todaysyear=todaysyear+'';
               todaysyear=todaysyear.substring(2);
               todaysyear=1*todaysyear;
               if (((cardmonth<todaysmonth) && (cardyear == todaysyear)) || (cardyear<todaysyear))
               {
                       return SetFocus('You have entered an invalid Expiry Date. Please check and try again.',obj)
               }
            }
          break;

   case 'LUHN':
	    if($('payment_'+temp[1]+'_cardNumberId') && maskedCardNumber != $('payment_'+temp[1]+'_cardNumberId').value)
	      {
          obj_1 = document.getElementById(obj_params[0])
         var cardnumber = document.getElementById(obj_params[0]).value
          var oddoreven = cardnumber.length & 1;
         var sum = 0;
         var addition = "";

         for (var count = 0; count < cardnumber.length; count++)
         {
            var digit = parseInt(cardnumber.substr(count,1));
            if (!((count & 1) ^ oddoreven))
            {
               digit *= 2;
               if (digit > 9)
               {
                  digit -= 9;
                  addition = addition + ' ' + digit;
               }
               else
               {
                  addition = addition + ' ' + digit;
               }
               sum += digit;
            }
            else
            {
               sum += digit;
               addition = addition + ' ' + digit;
            }
         }
         if (sum % 10 != 0) { return SetFocus('Please enter a valid card number.',obj_1) }
	      }
             break;

   case 'SECURITY':
      var cardType='';
      var securityCodeLength='';
      var id =obj.id;
      var index = id.split("_")[1];
      if($('payment_'+index+'_securityCodeId') && maskedCvv != $('payment_'+index+'_securityCodeId').value || index!=0)
      {
      var paymentMethObj = document.getElementById("payment_type_"+index);
      var temp = processSelctedPaymentType(paymentMethObj,index);
      if(temp)
      {
           securityCodeLength=temp[2];
      }
      else
      {
          securityCodeLength=3;
      }

        Pat1 = /^[0-9 ]*$/;
        if( (obj.value.length != securityCodeLength)  || (!Pat1.test(obj.value)) )
        {
           return SetFocus('Please enter a valid security number (the last '+securityCodeLength+' digits in the signature strip on the reverse of your card)',obj)
        }
     }
        break;

    // Review >> Check for < and >
    //ALT defined as >> 0-EmptyAlert|1-Y/N|2-inputtype|3-Minlength|4-MinlengthAlert|5-Maxlength|6-MaxlengthAlert
    case 'REVIEW':
            var Pat1 = /^[^<>]*$/;
            if (!Pat1.test(obj_value)) { return SetFocus('We cannot accept HTML or angle brackets in your review, please remove any HTML or \'<\' or \'>\' characters.',obj) }
            if(valset[3]!='') { //check for the minlength attribute
               if (obj_value.length<valset[3])
               {
                  if(valset[4]!='')
                     { return SetFocus(valset[4],obj);}
                  else
                     { return SetFocus('You have entered '+obj_value.length+' characters and have exceeded the '+valset[3]+' character limit',obj); }
               }
            }
            if(valset[5]!='') { //check for the maxlength attribute
               if (obj_value.length>valset[5])
               {
                  if(valset[6]!='')
                     { return SetFocus(valset[6],obj);}
                  else
                     { return SetFocus('This field has a minimum '+valset[5]+' character limit. You have entered only '+obj_value.length+' characters',obj); }
               }
            }
         break;

   case 'ALPHANUMHYPHEN':
         var Pat1 = /^[-a-zA-Z0-9]*$/;
         if (!Pat1.test(obj_value)) { return SetFocus('Please enter a valid Promotional Code. It accepts only Alphabets, Numerics and Hyphen(-)',obj) }
     break;
   case 'AMOUNT_GBP':
         var Pat1 = /^[\u00A3]?[0-9]*[\.\{0,1}]?[0-9]*$/;
         if(obj.id.indexOf("voucherAmount")!=0)
         {
           if (!Pat1.test(obj_value)|| obj_value.replace(String.fromCharCode(163),"").length <1) { return SetFocus('Please enter a valid Amount ',obj) }
         }
         break;

   case 'DECIMAL':
         var Pat1 = /^[0-9.]*$/;//Check for Numeric and dot
         var Pat2 = /^[0-9]+(\.\d{1,2})?$/;//Check for 2 optional decimals
         if (!Pat1.test(obj_value) ||!Pat2.test(obj_value) ) { return SetFocus('Please enter a valid amount and only upto 2 decimal places',obj) }
      break;
      } // switch
      } // if - mandatory
   } // if - Validation Required
   } // if alt tag exists
   if(obj.name.indexOf("issueNumber")>=0)
   {
     if(!debitCardValidationNew(obj.name))
     {
         return false;
     }

   }

  } // for
  // Change button style
   //alert(document.getElementById('paymentTrans'));

  if (document.getElementById('paymentTrans'))
     {
        var totalAmountPaid = parseFloat((document.getElementById("totamtpaid").value).replace(currencySymbol, ""));
        var amountStillDue = parseFloat((document.getElementById("amtstilltxt").value).replace(currencySymbol, ""));
        var noOfTransactions = document.getElementById('paymentTrans').value
           //alert(noOfTransactions);
        if(noOfTransactions==1)
        {
            var payType = document.getElementById('payment_type_'+0).value.split("|");

            if (payType[1]!=null && trimSpaces(payType[1]) == "NoPayment" && payType[0].indexOf('RR') == -1)
            {
                   totamtdue=0;
            }
      }

    if(searchType =="CSS Mode" && isRefund)
    {
         if(Math.abs(compensationChequeAmount)== 0 && getCSSChequePayment(noOfTransactions))
           {
           alert("Compensation cheque is not valid for normal refund. Please select other refund type");
           return false;
           }
           if( Math.abs(parseFloat(cssCompensationTotal).toFixed(2)) > Math.abs(compensationChequeAmount)&& getCSSChequePayment(noOfTransactions))
           {
            alert("The Maximum Compensation that can be awarded is <=" + Math.abs(compensationChequeAmount));
            return false;
            }
          if( Math.abs(parseFloat(totalAmountPaid)) != Math.abs(parseFloat(additionaltotamount).toFixed(2)))
              {
               alert('Amount Paid should match total amount.');
               return false;
               }
     }
   else if(isAdditional)
            {
        var Total = parseFloat(additionaltotamount)+parseFloat(collectCharges(false))+ parseFloat(collectCharges(true));
         if(totalAmountPaid < additionalPaymentOptionBalanceDue || totalAmountPaid >Total.toFixed(2))
               {
              alert('Amount Paid should match total amount.');
              return false;
               }
          }
    else if (parseFloat(totamtdue).toFixed(2) != totalAmountPaid && amountStillDue != 0)
        {
           alert('Amount Paid should match total amount.');
           return false;
        }

   }
  if($("payment_0_cardNumberId") && ($("payment_0_cardNumberId").value == maskedCardNumber))
  {
     if($("payment_0_authCodeId").value == "" && responseMsg == "REFERRED")
     {
       return SetFocus('Please enter auth code', $("payment_0_authCodeId"));
     }
  }
 if(handlePayment())
{

    for (i=0;i<8; i++)
    {
       if ( document.getElementById("paymentType"+i))
       {
         document.getElementById("paymentType"+i).disabled=false;
       }
    }
    if(valid)
    {
      updateEssentialFieldsForBRAC();
    }
  if (document.getElementById("isPremiumCalculated") == null || document.getElementById("isPremiumCalculated").value == "true" || document.getElementById("isPremiumCalculated").value == "")
  {
  formobj.submit()
  }
  else if (document.getElementById("isPremiumCalculated") != null && document.getElementById("isPremiumCalculated").value == "invalid")
  {
     alert("Please select valid insurance type and click Calculate Premium before continuing");
  }
  else if (document.getElementById("isPremiumCalculated") != null && document.getElementById("isPremiumCalculated").value == "false")
  {
    alert("Please Calculate Premium before continuing");
  }
}
else
return false;
}
 return true;
}

function debitCardValidationNew(issueNoname)
{
   var temp=issueNoname.split("_");
   tranIndex=temp[1];

   var issueNoObj=document.getElementsByName(issueNoname)[0];
//   var startMonthObj=document.getElementsByName("payment_"+tranIndex+"_startMonth")[0];
 //  var startYearObj=document.getElementsByName("payment_"+tranIndex+"_startYear")[0];
  // TrimSpaces(issueNoObj.value);
   var paymentType=document.getElementById("payment_type_"+tranIndex).value;
   if(paymentType.indexOf("SOLO")>=0 || paymentType.indexOf("SWITCH")>=0&&(issueNoObj))
   {
       if(issueNoObj.value!='')
      {
            var Pat1 = /^[0-9]*$/;
            if (!Pat1.test(issueNoObj.value))
          {
               SetFocus("Please enter valid issue number",issueNoObj);
               return false;
            }
       }
     /* else if((startMonthObj.selectedIndex>1 && startYearObj.selectedIndex>1))
      {
          cardmonth = startMonthObj.options[startMonthObj.options.selectedIndex].value
          cardyear = startYearObj.options[startYearObj.options.selectedIndex].value
        cardmonth = parseInt(cardmonth,10)
          cardyear = parseInt(cardyear,10)
          var Calendar=new Date();
          todaysmonth =Calendar.getMonth()+1;
          var calib=(ns4 || ns6)?1900:0;
          todaysyear = Calendar.getYear()+calib;

          if( (cardmonth == '' && cardyear != '') || (cardmonth != '' && cardyear == '') )
          {
             return SetFocus('Please enter a valid date',startMonthObj)
             return false;
          }
          // If Start Date
          else if (((cardmonth>todaysmonth) && (cardyear == todaysyear)) || (cardyear>todaysyear))
        {
            return SetFocus('Please enter a valid start date',startMonthObj)
            return false;
          }
      }*/
    }
   return true;
}

function CardType() {
   UpdateCreditCardCharges();
   var cardType = document.getElementById('PaymentCardType').value;
   var cardCharge =document.getElementById('cardCharge').value;
   var cardLevyPercentage = document.getElementById('cardLevyPercentage').value;

   if(cardType != '')
   {
      var securityCodeLength=document.getElementById(cardType+"_securitycodelength").value;
   }
   if(cardType =='')
   {
      securityCodeLength=3;
   }

   var helptext="(last "+securityCodeLength+" digits in the signature strip on the reverse of your card)";
    document.getElementById("securityCodeHelp").innerHTML=helptext;

   if ((TrimSpaces(cardType) == 'SWITCH') || (TrimSpaces(cardType) == 'SOLO')) {
      document.getElementById('debitcards').style.display = "";
   } else {
      document.getElementById('debitcards').style.display = "none";
   }

   url = "/thomson/page/shop/byo/booking/updatecardcharges.page?"+$('cardCharge').name+"="+$('cardCharge').value+"&"+$('cardLevyPercentage').name+"="+$('cardLevyPercentage').value;
   if($('payDepositOnly') != null)
   {
       url = "/thomson/page/shop/byo/booking/updatecardcharges.page?"+$('cardCharge').name+"="+$('cardCharge').value+"&"+$('cardLevyPercentage').name+"="+$('cardLevyPercentage').value+"&"+$('payDepositOnly').name+"="+$('payDepositOnly').value;
   }else
   {
         url = "/thomson/page/shop/byo/booking/updatecardcharges.page?"+$('cardCharge').name+"="+$('cardCharge').value+"&"+$('cardLevyPercentage').name+"="+$('cardLevyPercentage').value+"&";
   }

   dynamicFrame.document.location.replace(url);

}

function ClearField(obj) {
if (obj.value != '') {
 obj.value = '';
 obj.select()
 obj.onfocus = null;
 }
}



//check whether atleast one  transaction is accepted
function handlePayment()
{
    var panelType=document.getElementById("panelType").value;

   if (document.getElementById('paymentTrans'))
   {
      var paymentFlag = false;
      var totalPayments = Number(document.getElementById('paymentTrans').value);
      for (var i = 0; i < totalPayments; i++)
      {
           if (document.getElementById('cardref'+i))
         {
           minLength = 4;
           var amexCheck = document.getElementById('payment_type_'+i).value.indexOf('AMERICAN_EXPRESS');
           if(amexCheck !=-1)
           {
              if (document.getElementById('cardref'+i) && document.getElementById('cardref'+i).value.length != 2)
             {
               SetFocus('Card reference no. should be of only 2 characters length.', document.getElementById('cardref'+i));
               return false;
             }
           }
           else
           {
             if (document.getElementById('cardref'+i) && document.getElementById('cardref'+i).value.length < 4)
             {
               SetFocus('Card reference no. should be more than 3 characters length.', document.getElementById('cardref'+i));
               return false;
             }
           }
          }
      }
      for (var i = 0; i < totalPayments; i++)
      {
         if (document.getElementById('chkbox'+i) && document.getElementById('chkbox'+i).checked)
         {
            paymentFlag = true;
            return true;
         }
          if((panelType=='CNP' || panelType=='CPNA' || panelType=='CP')&&document.getElementById('payment_type_'+i)&&
                                             document.getElementById('payment_type_'+i).selectedIndex>0)
          {
            paymentFlag = true;
            return true;
         }
      }
     alert("You need to accept atleast one payment transaction");
     return false;
   }

   return true;
}

//This function is used to validate the OLBP payment page
function checkOlbpPaymentAndValidate()
{
   ValidateForm(document.postPaymentFormBean);
}

function handlePromotionCode()
{
   if (!checkPaymentTypes(document.getElementById("promoCode"), false))
   {
      return false;
   }

   setUpdateTrue();
   var val = document.getElementById("promoCode").value;
   var Pat1 = /^[-a-zA-Z]*$/;
   var Pat2 = /^[0-9]*$/;

   /*if (!Pat1.test(val) || val == null || val =="")
   {
      return SetFocus('Please enter a valid Promotional Code. It accepts only Alphabets, Numerics and Hyphen(-)',document.getElementById("promoCode"));
   }*/
   if (!Pat2.test(val) || val == null || val =="")
   {
      return SetFocus('Please enter a valid Promotional Code. It accepts only  8 digits Numerics Value.', document.getElementById("promoCode"),document.getElementById("promoCode").value="");
   }

   if (Pat2.test(val) && val.length >8)
   {
      return SetFocus('Please enter a valid Promotional Code. It accepts only 8 digits numeric value.',document.getElementById("promoCode"),document.getElementById("promoCode").value="");
   }


   if (document.getElementById("hotelPriceBeat") || document.getElementById("flightPriceBeat"))
   {
      if (document.getElementById("hotelPriceBeat"))
      {
         document.getElementById("hotelPriceBeat").disabled = true;
      }
      if (document.getElementById("flightPriceBeat"))
      {
         document.getElementById("flightPriceBeat").disabled = true;
      }
   }

   if(document.getElementById('payDepositOnly') && document.getElementById('cardLevyPercentage'))
   {
      dynamicFrame.document.location.replace("/thomson/page/shop/byo/booking/handlepromotioncode.page?validatePromo=true&promoCode="+val+"&"+$('cardLevyPercentage').name+"="+$('cardLevyPercentage').value+"&"+$('payDepositOnly').name+"="+$('payDepositOnly').value);
   }
   else if(!document.getElementById('payDepositOnly') && document.getElementById('cardLevyPercentage'))
   {
      dynamicFrame.document.location.replace("/thomson/page/shop/byo/booking/handlepromotioncode.page?validatePromo=true&promoCode="+val+"&"+$('cardLevyPercentage').name+"="+$('cardLevyPercentage').value+"&payDepositOnly=false");
   }
   else if(document.getElementById('payDepositOnly') && !document.getElementById('cardLevyPercentage'))
   {
      dynamicFrame.document.location.replace("/thomson/page/shop/byo/booking/handlepromotioncode.page?validatePromo=true&promoCode="+val+"&cardLevyPercentage=0.0&"+$('payDepositOnly').name+"="+$('payDepositOnly').value);
   }
   else
   {
      dynamicFrame.document.location.replace("/thomson/page/shop/byo/booking/handlepromotioncode.page?validatePromo=true&promoCode="+val+"&");
     //$('cardLevyPercentage').name+"="+$('cardLevyPercentage').value+"&"++$('payDepositOnly').name+"="+$('payDepositOnly').value
   }
}

//Strips the characters passed through the function.
function stripChars(s, bag)
{
  if(!s)
  {
    return;
  }
   var i;
   var returnString = "";
   // Search through string's characters one by one.
   // If character is not in bag, append to returnString.
   for (i = 0; i < s.length; i++)
   {
      // Check that current character isn't whitespace.
      var c = s.charAt(i);
      if (bag.indexOf(c) == -1) returnString += c;
   }
   return returnString;
}
function UpdateCreditCardCharges()
{

    FieldObjCardType=document.getElementById("PaymentCardType");
    var cardType=FieldObjCardType.options[FieldObjCardType.selectedIndex].value
    if(cardType != '')
    {
    var cardType_enabled=cardType+"_enabled";
    updationrequiredElem=document.getElementById(cardType_enabled);
   updationrequired ="false";
   if(updationrequiredElem !=null)
       updationrequired=updationrequiredElem.value

        if(updationrequired == "true")
        {
        var cardType_charges = cardType+"_charges";
        CreditCardChargeElem = document.getElementById(cardType_charges);
        CreditCardCharge = CreditCardChargeElem.value;
        var ChargePercent = CreditCardCharge/100;
        var TotalAmount = document.getElementById("totalAmount").value;
      if(document.getElementById("payDepositOnly") !=null)
         {
            DepositSelection =document.getElementById("payDepositOnly");
            var isDepositSelected = DepositSelection.value
            if(isDepositSelected == "true")
               {
                  TotalAmount = document.getElementById("depositValue").value;
               }

         }

        TotalCreditCharge = (TotalAmount)*(ChargePercent);
        TotalFixedCharge =TotalCreditCharge.toFixed(2);
        document.getElementById("cardLevyPercentage").value=ChargePercent;
      document.getElementById("cardCharge").value=TotalFixedCharge;
        document.getElementById("cardChargesText").innerHTML="<b>&pound;</b>"+TotalFixedCharge;
        }
        else
        {
        document.getElementById("cardCharge").value=0.0;
      document.getElementById("cardLevyPercentage").value=0.0;
        document.getElementById("cardChargesText").innerHTML='<b>&pound;</b>0.00';
        }
    }
    else
    {
      document.getElementById("cardLevyPercentage").value=0.0;
        document.getElementById("cardCharge").value=0.0;
        document.getElementById("cardChargesText").innerHTML="";
    }
}
function CheckSeniorPassengerAndValidate()
{

            var noOfTransactions = document.getElementById('paymentTrans').value;
            obj = document.getElementsByName('impInfo')[0];
            objth = document.getElementsByName('thomsonholidayinfo')[0];
            objHop =document.getElementsByName('importantInformationChecked')[0];
         objPromotxt = document.getElementById('promoMainContainer');
         objPromocode = document.getElementById('promoCode');
         objBalanceType = document.getElementsByName("depositType");

         for (var j = 0; j < objBalanceType.length; j++)
         {
            for (var i = 0; i < noOfTransactions; i++)
            {
         var amount = parseFloat(stripChars(document.getElementById("transamt"+i).value,currencySymbol));
             if (objBalanceType[j].checked && objBalanceType[j].value == "Full Balance"  && objBalanceType.length!=1 )
               {

                var payType = document.getElementById('payment_type_'+i).value.split("|");
                 if ( payType[1]!=null && trimSpaces(payType[1]) == "NoPayment" && payType[0]=='CA' )
                  {
                     alert("�You can't select Save option type with Fullbalance payment.")
                     return false;
                  }
               }

               else if (amount == 0)
               {
                  var payType = document.getElementById('payment_type_'+i).value.split("|");

                  if (payType[1]!=null && trimSpaces(payType[1]) != "NoPayment" || trimSpaces(payType[1]).indexOf("Refund") != -1)
                  {
                     alert("The selected  payment type/types is not accepted  with zero amount.");
                     return false;
                  }
               }
            }
         }
         for (var i = 0; i < noOfTransactions; i++)
         {
            var selectedTypeValue = $("payment_type_"+i).value;
            var selectedTypeArray = selectedTypeValue.split("|");
            var selectedType = selectedTypeArray[1];
            var panelType=$("panelType").value;
            if (trimSpaces(selectedType) == "Voucher" && panelType == "CNP")
            {
               var voucherTotal = collectTotalVoucherAmount(i);
               if(1*$("total_amount_"+i).value > 1*voucherTotal)
               {
                  alert("The Voucher Value(s) entered must be equal to the Transaction Amount. Please adjust the appropriate value before proceeding.")
                  $("chkbox"+i).checked=false;
                  return;
               }
            }
         }

         for (var i = 0; i < noOfTransactions; i++)
         {
            var payType = document.getElementById('payment_type_'+i).value.split("|");
                 //alert(payType[1]);
           if(noOfTransactions == 1 && payType[1]!=null && trimSpaces(payType[1]) == "CardRefund" &&  !checkCardRefundRadio(0))
             {
                alert("Please check the transaction radio before proceeding.")
                return false;
             }
           if(noOfTransactions > 1)
            {
      if( searchType != "CSS Mode" && payType[1]!=null && trimSpaces(payType[1]) == "NoPayment" && (payType[0]== "RQ" || payType[0]=="RR"))
             {

              alert("Refund Cheque cannot be combined with Multiple Transactions");
              return false;
             }
           }
         if(isRefund &&  searchType == "CSS Mode" &&  payType[1]!=null && trimSpaces(payType[1]) == "Cheque" && payType[0]=="CQ")
             {
                var transamt_carry= document.getElementById("transamt_carry"+i).value;
                //This variable is set to zero before the cssCompensationTotal is calculated to avoid the unwanted alert
                //message which sometimes comes even when the correct cssCompensationAmount is entered.
                cssCompensationTotal=0;
                cssCompensationTotal=parseFloat(cssCompensationTotal)+parseFloat(transamt_carry);
             }
         }

         if (objPromocode && objPromotxt)
         {
            if ((objPromocode.value!='')&& (objPromotxt.innerHTML==''))
            {
               alert("Please validate your promotional code before you click on pay.");
               objPromocode.focus();
               return false;
            }
         }
           if (objth &&((objth.type == 'checkbox') && (!objth.checked)))
            {
             alert("Please acknowledge that your holiday does not include ThomsonPlus benefits.");
             if(!(ns4||ns6))
              {

             window.Information.focus();
              }
             else
             {
              var obj = document.getElementById("Information");
              var inp = document.createElement("input");
              obj.appendChild(inp);
              inp.focus();
              inp.setAttribute("style","margin-left:-400px;")
              }
            return false;
          }

            if (obj &&((obj.type == 'checkbox') && (!obj.checked)))
            {
             alert("Please read and accept the important additional information before continuing with your booking");
             if(!(ns4||ns6))
              {
            var obj = document.getElementById("impInfoAccept");
             obj.focus();
              window.Information.focus();
              }
             else
             {
             var obj = document.getElementById("impInfoAccept");
             var inp = document.createElement("input");
              obj.appendChild(inp);
             obj.focus();
              inp.setAttribute("style","margin-left:-400px;")
              }
            return false;
          }

          if (objHop &&((objHop.type == 'checkbox') && (!objHop.checked)))
            {
             alert("Please read and accept the important additional information before continuing with your booking");
             if(!(ns4||ns6))
              {

             window.Information.focus();
              }
             else
             {
              var objHop = document.getElementById("Information");
              var inp = document.createElement("input");
              objHop.appendChild(inp);
              inp.focus();
              inp.setAttribute("style","margin-left:-400px;")
              }
            return false;
          }

   var count =0;
   /*for (i=0; i<totalPassenCount ; i++)
   {
      if (supplierSystem != null && supplierSystem != "" && supplierSystem == supplierAmadeus)
      {
         if(document.getElementById('passengerDetailsFormBeans['+i+'].lastName').value.length == 1)
         {
            return SetFocus(surnameValidationError,document.getElementById('passengerDetailsFormBeans['+i+'].lastName'));
         }
      }
   }
   for(i=0;i<=NumberOfAdults;i++)
   {
      if(document.getElementsByName('passengerDetailsFormBeans['+i+'].over65')[0]!=null && document.getElementsByName('passengerDetailsFormBeans['+i+'].over65')[0].checked == true)
      {
         count=count+1;
      }
   }*/
   if(isHopla)
   {
      var noOfPayments=0;
      if(document.getElementById("paymentTrans"))
      {
        noOfPayments =document.getElementById("paymentTrans").value;
      }
      else
      {
        noOfPayments=0;
      }

      if(noOfPayments>=0)
      {
         var hoplaDropDown=document.getElementById("payment_type_"+(noOfPayments));
         if(hoplaDropDown&&hoplaDropDown.selectedIndex<1)
         {
           SetFocus("Your card number is required to complete your booking,please enter your card number",hoplaDropDown);
           return false;
         }
      }
   }
   if(count != numberOfSeniorPassengers)
   {
      if(count < numberOfSeniorPassengers)
      {
         alert(lessSeniorPassengersMessage);
                  return false;
      }
      else
      {
         alert(moreSeniorPassengersMessage);
         return false;
      }
   }
   else
   {
      //validation for payment types
      //validatePaymentTypes();
   valid =  ValidateForm(document.paymentdetails);
   }
   return valid;
}


function changeDiscount(obj)
{
  //dynamicFrame.document.location.replace("/thomson/page/shop/byo/booking/updatediscount.page?agentId=true&offlinePrice="+obj.checked);
}

function checkDateOfBirthValidation()
{
   dayArray = getElemName('day');
   monthArray = getElemName('month');
   yearArray = getElemName('year');
   for(var i=0;i<dayArray.length;i++)
   {
       datein = dayArray[i].value+ '/' + monthArray[i].value +'/' + yearArray[i].value;
       var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;
       var matchArray = datein.match(datePat);
       if (matchArray == null)
       {
            return SetFocus("Please enter the date of birth of each child",dayArray[i])
       }
       else {
          if ((monthArray[i].value==4 || monthArray[i].value==6 || monthArray[i].value==9 || monthArray[i].value==11) && dayArray[i].value==31) {
              return SetFocus("Enter a Valid Date",dayArray[i])
          }
          if (monthArray[i].value == 2)
          {
                var isleap = (yearArray[i].value % 4 == 0 && (yearArray[i].value % 100 != 0 || yearArray[i].value % 400 == 0));
                if (dayArray[i].value>29 || (dayArray[i].value==29 && !isleap))
                {
                    return SetFocus("Enter a Valid Date",dayArray[i])
                }
          }
       }
    }
}

// start price beat
function handlePriceBeat()
{
   var priceBeatObj=document.getElementById("priceBeat");
   var maxAllowedPriceBeatObj=document.getElementById("maxPriceBeatAmount");


   // format of price beat value, it should be 5 digits + 2 decimal points as per FS
   var Pat1 = /^\d{0,5}(\.\d{1,2})?$/;
   if (! priceBeatObj.value.match(Pat1))
   {
            return SetFocus(alertValue, priceBeatObj);
      return false;
   }

   if(parseFloat(priceBeatObj.value)>parseFloat(maxAllowedPriceBeatObj.value))
   {
      clearAndFocus(maxAlert,priceBeatObj);
      return false
   }
   else
   {
     document.getElementById("todaySaving").disabled = true;
     return true;
   }

   /*var priceBeatValue = '';
   var discreation;
   var discreationSecond;
   // Changed the format of price beat value, it should be 5 digits + 2 decimal points as per FS
   var Pat1 = /^\d{0,5}(\.\d{1,2})?$/;

   if (document.paymentdetails.hotelTodaySaving != null)
   {
      discreation = document.paymentdetails.hotelTodaySaving;
      //url = url + $('hotelTodaySaving').name + "=" + "&"
   }
   if (document.paymentdetails.flightTodaySaving != null)
   {
      discreationSecond = document.paymentdetails.flightTodaySaving;
      //url = url + $('flightTodaySaving').name + "=" + "&"
   }
   if(document.getElementById("hotelPriceBeat") !=null)
   {
      var vPriceBeat = document.getElementById('hotelPriceBeat');
      priceBeatValue = priceBeatValue + vPriceBeat.value;
      if (priceBeatValue != '')
      {
         if (! priceBeatValue.match(Pat1))
         {
            return SetFocus(alertValue, vPriceBeat);
         }
         if (parseFloat(vPriceBeat.value) > parseFloat(document.paymentdetails.maxHotelPriceBeatAmount.value))
         {
            return SetFocus(maxAlert, vPriceBeat);
         }
      }
      else
      {
      }
   }

   */
   return true;
         }


function handleTodaySaving()
         {
   var todaySavingObj=document.getElementById("todaySaving");
   var maxAllowedTodaySavingPriceBeatObj=document.getElementById("todaySavingPriceBeatMax");

      // format of today Saving, it should be 5 digits + 2 decimal points as per FS
   var Pat1 = /^\d{0,3}(\.\d{1,2})?$/;
   if (! todaySavingObj.value.match(Pat1))
      {
            return SetFocus(alertValue, todaySavingObj);
            return false;
      }

   if(parseFloat(todaySavingObj.value)>parseFloat(maxAllowedTodaySavingPriceBeatObj.value))
   {
      clearAndFocus(maxAlert,todaySavingObj);
      return false;
   }
   else
   {
     document.getElementById("priceBeat").disabled = true;
      return true;
      }
   /*
   if (!checkPaymentTypes(document.getElementById("hotelTodaySaving"), document.getElementById("flightTodaySaving")))
   {
      return false;
   }

   setUpdateTrue();
   var todaySavingValue = '';
   var url = '/thomson/page/shop/byo/booking/updatepaymentdiscounts.page?';
   var priceBeat;
   var priceBeatSecond;
   var Pat1 = /^\d{0,3}(\.\d{1,2})?$/;
   if (document.paymentdetails.hotelPriceBeat != null)
   {
      priceBeat = document.paymentdetails.hotelPriceBeat;
      url = url + $('hotelPriceBeat').name + "=" + "&"
   }
   if (document.paymentdetails.flightPriceBeat != null)
   {
      priceBeatSecond = document.paymentdetails.flightPriceBeat;
      url = url + $('flightPriceBeat').name + "=" + "&"
   }
   if(document.getElementById("hotelTodaySaving") !=null)
   {
      var vTodaySaving = document.getElementById('hotelTodaySaving');
      todaySavingValue = todaySavingValue + vTodaySaving.value;
      if (todaySavingValue != '')
      {
         if (! todaySavingValue.match(Pat1))
         {
            return SetFocus(alertValuetd, vTodaySaving);
         }
         if (parseFloat(vTodaySaving.value) > parseFloat(document.paymentdetails.maxHotelDiscreationAmount.value))
         {
            return SetFocus(maxAlerttd, vTodaySaving);
         }
         url = url + $('hotelTodaySaving').name + "=" + $('hotelTodaySaving').value + "&";
      }
      else
      {
         url = url + $('hotelTodaySaving').name + "=" + "&";
      }
   }
   if(document.getElementById("flightTodaySaving") !=null)
   {
      var vTodaySaving = document.getElementById('flightTodaySaving');
      if (vTodaySaving.value != '')
      {
         if (! vTodaySaving.value.match(Pat1))
         {
            return SetFocus(alertValuetd, vTodaySaving);
         }
         if (parseFloat(vTodaySaving.value) > parseFloat(document.paymentdetails.maxFlightDiscreationAmount.value))
         {
            return SetFocus(maxAlerttd, vTodaySaving);
         }
         todaySavingValue = todaySavingValue + vTodaySaving.value;
         url = url + $('flightTodaySaving').name + "=" + $('flightTodaySaving').value + "&";
      }
      else
      {
         url = url + $('flightTodaySaving').name + "=" + "&";
      }
   }
   if(todaySavingValue != '')
   {
      if (priceBeat != null)
      {
         priceBeat.value='';
         priceBeat.disabled = true;
      }
      if (priceBeatSecond != null)
      {
         priceBeatSecond.value='';
         priceBeatSecond.disabled = true;
      }
   }
   else
   {
      if (priceBeat != null)
      {
         if ((document.getElementById('promoMainContainer') != null
            && TrimSpaces(document.getElementById('promoMainContainer').innerHTML) == '')
            || document.getElementById('promoMainContainer') == null)
         {
         priceBeat.disabled = false;
      }
      }
      if (priceBeatSecond != null)
      {
         priceBeatSecond.disabled = false;
      }
   }
   dynamicFrame.document.location.replace(url);
   */
   return true;
}

function handlePromotionalCodePdp()
{
   var vPromotionCodePdp = document.paymentdetails.promoCode.value;
   if(vPromotionCodePdp != '')
   {
      //document.paymentdetails.priceBeatPdp.disabled = true;
      document.paymentdetails.todaySavingPdp.disabled = true;
   }
   else
   {
      //document.paymentdetails.priceBeatPdp.disabled = false;
      document.paymentdetails.todaySavingPdp.disabled = false;
   }
}
 //end of price beat
// for the email and print functionality
 function setFlag(desc,obj)
{
   if (desc=="printLocal")
   {
      document.getElementById("printLocally").value = obj.checked;

      if(document.getElementById("printAtolLocally").disabled)
      {
          document.getElementById("printAtolLocally").disabled = false;
      }
      else
      {

    	  if(document.getElementById("printAtolLocally").checked)
    	  {
    		  document.getElementById("printAtolLocally").checked=false;
    		  document.getElementById("printAtolLocally").value=false;
    		  document.getElementById("printAtolLocally").disabled =true;

    	  }
    	  else
    	  {
    		  document.getElementById("printAtolLocally").disabled =true;

    	  }




      }
   }
   else
   {

      document.getElementById("printLocally").checked = false;
   }
}

function setAtolPrintFlag(desc,obj)
{
   if (desc=="printAtolLocal")
   {
      document.getElementById("printAtolLocally").value = obj.checked;
   }
   else
   {

      document.getElementById("printAtolLocally").checked = false;
   }
}
function setUpdateTrue()
{
   if (document.getElementById("updateFlag"))
   {
      document.getElementById("updateFlag").value = 'true';
   }
}

function processDiscount(discountType)
{
   var finalValues = '';
   var url = '/thomson/page/shop/byo/booking/updatepaymentdiscounts.page?';
   var otherElementOne;
   var otherElementSecond;

   if (document.getElementById('hotel' + getOtherDiscount(discountType)) != null)
   {
      otherElementOne = document.getElementById('hotel' + getOtherDiscount(discountType));
      url = url + $('hotel' + getOtherDiscount(discountType)).name + "=" + "&"
   }
   if (document.getElementById('flight' + getOtherDiscount(discountType)) != null)
   {
      otherElementSecond = document.getElementById('flight' + getOtherDiscount(discountType));
      url = url + $('flight' + getOtherDiscount(discountType)).name + "=" + "&"
   }
   //url = url +

   return url;
}

function getOtherDiscount(discountName)
{
   if (discountName == 'TodaySaving')
   {
      return 'PriceBeat';
   }
   else if (discountName == 'PriceBeat')
   {
      return 'TodaySaving';
   }
}

function validateDiscount(discountName)
{
   var pattern = /^\d{0,5}$/;
   var url = '';
   if (document.getElementById('hotel' + discountName) != null)
   {
      var vTodaySaving = document.getElementById('hotel' + discountName);
      if (vTodaySaving.value != '')
      {
         if (! vTodaySaving.value.match(pattern))
         {
            return SetFocus(alertValuetd, vTodaySaving);
         }
         if (parseFloat(vTodaySaving.value) > parseFloat(document.paymentdetails.maxHotelDiscreationAmount.value))
         {
            return SetFocus(maxAlerttd, vTodaySaving);
         }
         url = url + $('hotelTodaySaving').name + "=" + $('hotelTodaySaving').value + "&";
      }
      else
      {
         url = url + $('hotelTodaySaving').name + "=" + "&";
      }
   }
   return url;
}
/***Added from extraoptions.js ****/
function validateInsTypeAndCustTypeFields()
{
    if (document.getElementById("numberOfPassengers") != null)
    {
      paxCount = document.getElementById("numberOfPassengers").value;

      for(i=0;i<paxCount;i++)
      {
        var insuranceTypeField=document.getElementById("insurancetype_" + i);
        var insurancePaxTypeField=document.getElementById("paxtype_" + i);

        // No validation should be done if we are in pags other than travel options page.
        if (insuranceTypeField == null || insurancePaxTypeField == null)
        {
            return true;
        }

       if(insuranceTypeField.value=='0')
       {
           alert("Please select Insurance Type");
           document.getElementById("insurancetype_" + i).focus();
           return false;
       }
       if(insurancePaxTypeField.value=='0')
       {
           alert("Please select Pax Type");
           document.getElementById("paxtype_" + i).focus();
           return false;
       }
      }
      return validateSameInsType(paxCount);
    }
    return true;
}

function validateSameInsType(noOfPeople)
  {
      var selectedCode;
      for(var i = 0; i < noOfPeople; i++)
      {
         temp = document.getElementById("insurancetype_"+i).value;
         value = temp.split("|");
         seleBoxVal = value[0];

         if(seleBoxVal!=1)
         {
            selectedCode = seleBoxVal;
            break;
         }
      }
     for(var i = 0; i < noOfPeople; i++)
     {
         temp = document.getElementById("insurancetype_"+i).value;
         value = temp.split("|");
         seleBoxVal = value[0];
         if (seleBoxVal != 1 && seleBoxVal != selectedCode)
         {
            alert("Please select same type of insurance.");
            return false;
         }
     }
     return true;
 }
/**************************Util**************************/
// Set Focus to field and show message
function SetFocus(alertmess,obj1) {
alert(alertmess)
obj1.focus();
return false;
}

// Remove all spaces in value
function RemoveAllSpaces(objval) {
   RESpace = /\s/ig;
  objval = objval.replace(RESpace, '');
  return objval
}
function validateReaccreditationfileds()
{
 if(isReaccreditationallowed)
 {
 var Pat1 = /^[0-9]*$/;//Check for Numeric and dot
 if(document.getElementById("consultantId").value != "" && (document.getElementById("authenticationUserID").value ==""||
   document.getElementById("authenticationPassword").value==""))
  {
   alert("Username and Password Required");
   return false
  }
  else if (!document.getElementById("consultantId").value.match(Pat1))
  {
  alert("special characters not allowed");
  return false;
  }
  else
   return true;
 }
 else
 return true;
}
// Trim spaces from start and end of string
function TrimSpaces(objval) {
  // Remove Space at start
  RESpace = /^\s*/i;
  objval = objval.replace(RESpace, '');
  // Remove Space at end
  RESpace = /\s*$/i;
  objval = objval.replace(RESpace, '');
  return objval;
}

function getCSSChequePayment(noOfTransactions)
{
 var csspaymentcheque = false;
 for (var i = 0; i < noOfTransactions; i++)
         {
            var payType = document.getElementById('payment_type_'+i).value.split("|");
            if(isRefund &&  searchType == "CSS Mode" &&  payType[1]!=null && trimSpaces(payType[1]) == "Cheque" && payType[0]=="CQ")
             {
              csspaymentcheque=true;
       }
        }
        return csspaymentcheque;
}