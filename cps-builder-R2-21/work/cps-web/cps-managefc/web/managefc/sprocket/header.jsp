<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" id="firstchoice" class="ie6 "> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" id="firstchoice" class="ie7 "> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" id="firstchoice" class="ie8 "> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" id="firstchoice" class="ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="" id="firstchoice"> <!--<![endif]-->
<head>
	<title></title>
	<!--[if IE 7 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /><![endif]-->
	<!--[if IE 8 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /><![endif]-->
	<!--[if IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" /><![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><!--<![endif]-->
	<meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<meta name="viewport" content="width=device-width,  itial-scale=1">


	<link rel="icon" type="image/png" href="/cms-cps/managefc/images/favicon.png" />

		<!--[if lte IE 9]>
			<link rel="shortcut icon" href="/cms-cps/managefc/images/favicon.ico" type="image/x-icon" />
			<link rel="icon" href="/cms-cps/managefc/images/favicon.ico" />
			<![endif]-->

			<!-- HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->

			<!-- javascript -->

			<script>
			var ensLinkTrack = function(){};
			</script>
		</head>
		<body>
			<div id="wrapper">
				<div id="header">
					<!-- <div class="beta">
						<p>Welcome to our brand new website.</p>
						<p class="tell-us">
							<a href="http://www.firstchoice.co.uk/beta/contact-us/send-us-an-email/report-a-problem/" target="_blank" class="ensLinkTrack" >Provide feedback on our website</a>
						</p>
						<a href="#" class="button">Switch to main site</a>
					</div> -->
					<div class="message-window" style="display:none" id="tellUsWhatYouThink">
						<a href="#" class="close ensLinkTrack" > <span class="text">Close</span></a>
						<div class="message-content small-pop beta-pop">
							<h3>Before you go...</h3>
							<p>We'd love to know what you made of the new site:</p>
							<ul>
								<li><a target="_blank" href="http://www.firstchoice.co.uk/" class="ensLinkTrack">Tell us what you think</a></li>
								<li><a target="_blank" href="http://www.firstchoice.co.uk/?customer=switch" class="ensLinkTrack">Switch to our main site</a></li>
							</ul>
						</div>
					</div>
					<div id="utils">
						<ul>
							 <li><p class="contact-us fl"><a style="color : #BBEAE2" target="_blank" title="Ask a Question" href="http://www.firstchoice.co.uk/holiday/faqCategories">Ask a Question</a></p></li>
							<li><a target="_blank" class="ensLinkTrack" title="Travel Alerts" href="http://www.thomson.co.uk/editorial/alerts/thomsonfly-travel-alert.html">Travel Alerts</a></li>
							<li><a target="_blank" class="ensLinkTrack" title="Statement on Cookies" href="http://www.firstchoice.co.uk/our-policies/statement-on-cookies/">Statement on Cookies</a></li>
							<li><a target="_blank" class="ensLinkTrack" title="API login" href="https://eapi.thomson.co.uk/eborders/login.jsp">API login</a></li>
							<li><a  class="ensLinkTrack" title="Manage My Booking" href="${bookingComponent.breadCrumbTrail['HOME']}">Manage My Booking</a></li>
						</ul>
					</div>

					<div id="logo">
						<a target="_blank" class="ensLinkTrack" href="http://www.firstchoice.co.uk">First Choice</a>

					</div>
					<div id="nav">
						<ul>
							<li id="generalTab" class="active"><a target="_blank" class="ensLinkTrack" href="http://www.firstchoice.co.uk/holiday/destinations">Destinations</a></li>
							<li><a target="_blank" class="ensLinkTrack" href="http://www.firstchoice.co.uk/sun-holidays/">Collections</a></li>
							<li><a target="_blank" class="ensLinkTrack" href="http://www.firstchoice.co.uk/last-minute-deals/">Deals</a></li>
							<li><a target="_blank" class="ensLinkTrack" href="http://www.firstchoice.co.uk/holiday-extras/">Extras</a></li>
						</ul>
					</div>
				</div>
			</div>
		</body>
	</html>