<%@ page import="com.tui.uk.config.ConfReader;"%>
<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
<link href="/cms-cps/falconfo/css/main.css" rel="stylesheet"	type="text/css" />

<c:choose>
    <c:when test="${not empty sessionScope=='true'}">
    <title>Falcon Flight Only | Error</title>
   </c:when>
   <c:otherwise>
   <title>Falcon Flight Only | Session Timed Out</title>
   </c:otherwise>
</c:choose>
<body>
	<div id="wrapper">
		<jsp:include page="sprocket/header.jsp" />
	<div id="BodyWide">
	<div class="clear"></div>
	   <div class="bodyPadder">

	   <c:choose>
        <%--Tech Difficulties Error Message --%>
        <c:when test="${not empty sessionScope=='true'}">
          <div class="span error-page">
            <div class="span error-page">
                <h1>We're really sorry, we're having some technical problems.</h1>
            </div>
            	<%
			 String getHomePageURL =(String)ConfReader.getConfEntry("hybrisfalconfo.homepage.url","");
				  pageContext.setAttribute("getHomePageURL",getHomePageURL);
			%>
            <div class="span clearfix error-page">
                <div class="fl message">
                <c:choose>
					 <c:when test="${not empty bookingComponent.clientURLLinks.homePageURL}">
					 <c:set var='homePageURL' value='${bookingComponent.clientURLLinks.homePageURL}' />
				      </c:when>
				     <c:otherwise>
				  	  <c:set var='homePageURL' value='${getHomePageURL}'/>
				     </c:otherwise>
				</c:choose>
                   <p>You can go back to viewing  <a class="history-reload" href="${homePageURL}">flights matching your search</a> </p>
                    <p>or give us a call on 1850 94 61 64</p>
                    <a class="logo" href="${homePageURL}"><div></div></a>
                </div>
                <img height="276" width="460" alt="Technical Difficulties" src="/cms-cps/falconfo/images/error-tech-diff-old.png" class="fr error-image">
            </div>
       </c:when>
       <%--End Tech Difficulties Error Message --%>

	    <%--Session Time out Error Message --%>
       <c:otherwise>
		      <div class="span error-page">
			<div class="span error-page">
			<h1>We are really sorry, your page has timed out</h1>
			</div>
			<div class="span clearfix error-page">
				<div class="fl message">
				   <p>You can go back to viewing  <a class="history-reload" href="${homePageURL}">flights matching your search</a> </p>
					<p>or give us a call on 1850 94 61 64</p>
					<a class="logo" href="${homePageURL}"><div></div></a>
				</div>
				<img height="276" width="460" alt="Technical Difficulties" src="/cms-cps/falconfo/images/error-tech-diff-old.png" class="fr error-image">
			</div>
			 </c:otherwise>
       <%--End of Session Time out Error Message --%>
       </c:choose>
	  </div>

	<div class="clear"></div>
	</div>
	</div>
	</div>
	<jsp:include page="sprocket/footer.jsp" />
	<script>
	  var tui = {};
	  tui.analytics = {};
	  tui.analytics.page = {};
	  tui.analytics.page.pageUid = "technicaldifficultiespage";
	 </script>
</body>
</html>