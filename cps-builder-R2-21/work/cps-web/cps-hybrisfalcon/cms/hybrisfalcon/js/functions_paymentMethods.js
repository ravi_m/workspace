/**
 ** This method handles the Deposit options display
**/
function selectedpaymentMode(id) {
	var getmode = id;
	var getfullDeposit = document.getElementById("full-amt");
	var gethalfDeposit = document.getElementById("half-amt");
	var getlowDeposit = document.getElementById("quarter-amt");
	var depositClass = document.getElementsByClassName("payment-details");
	if (getmode == "full-amt") {
		getfullDeposit.className += " highlighted-div";
		var fullCheck = document.getElementById("full");
		fullCheck.checked = "checked";
		if(gethalfDeposit != null){
			gethalfDeposit.className = "payment-details padding10pxnoBottom";
		}
		if(getlowDeposit != null){
			getlowDeposit.className = "payment-details padding10pxnoBottom";
		}
	} else if (getmode == "half-amt") {
		gethalfDeposit.className += " highlighted-div";
		var halfCheck = document.getElementById("half");
		halfCheck.checked = "checked";
		getfullDeposit.className = "payment-details padding10pxnoBottom";
		if(getlowDeposit != null){
			getlowDeposit.className = "payment-details padding10pxnoBottom";
		}
	} else {
		getlowDeposit.className += " highlighted-div";
		var lowdepositCheck = document.getElementById("initial");
		lowdepositCheck.checked = "checked";
		getfullDeposit.className = "payment-details padding10pxnoBottom";
		if(gethalfDeposit != null){
			gethalfDeposit.className = "payment-details padding10pxnoBottom";
		}
	}
}

/**
 ** This method handles
 *		1. Displaying of payment method
 *		2. Displaying the final Amount
 *		3. Card Type drop down toggling based on the condition
**/
function displayCreditCharges(id) {
	var getId = id;
	var getDiv = document.getElementById(id);
	var divClass = document.getElementsByClassName("paycard-details");
	var debitSection = document.getElementById("debitcardType");
	var creditSection = document.getElementById("creditcardType");
	//var giftSection = document.getElementById("giftcardType");
	var creditCharges= document.getElementById("creditCardCharges");
	var debitCharges = document.getElementById("debitCardCharges");
	//var giftCharges = document.getElementById("giftCardCharges");

	//Reset to the default value
	document.getElementById('payment_type_credit').value = "PleaseSelect";
	document.getElementById('payment_type_debit').value = "PleaseSelect";

	if (getId == 'creditcardType') {
		var check = document.getElementById("credit");
		check.checked = "checked";
		if (check.checked) {
			creditCharges.style.display = "block";
			debitCharges.style.display = "none";
			//giftCharges.style.display = "none";
		} else {
			creditCharges.style.display = "none";
			debitCharges.style.display = "none";
			//giftCharges.style.display = "none";
		}
		creditSection.className += " highlighted-div";
		debitSection.className = "paycard-details";
	//	giftSection.className = "paycard-details";

	} else if (getId == 'debitcardType') {
		var check = document.getElementById("debit");
		check.checked = "checked";
		debitCharges.style.display = "block";
		creditCharges.style.display = "none";
		//giftCharges.style.display = "none";
		debitSection.className += " highlighted-div";
		creditSection.className = "paycard-details";
		//giftSection.className = "paycard-details";

	} /*else if (getId == 'giftcardType') {
		document.getElementById('payment_0_type').value = "MAESTRO|Dcard";
		var check = document.getElementById("gift");
		check.checked = "checked";
		debitCharges.style.display = "none";
		giftCharges.style.display = "block";
		creditCharges.style.display = "none";
		giftSection.className += " highlighted-div";
		creditSection.className = "paycard-details";
		debitSection.className = "paycard-details";
	}*/

	if (document.getElementById('debitPaymentTypeCode').style.display == 'block'
			|| document.getElementById('creditPaymentTypeCode').style.display == 'block'
			/*|| document.getElementById('giftPaymentTypeCode').style.display == 'block'*/) {
		if (getId == 'creditcardType') {
			document.getElementById('debitPaymentTypeCode').style.display = 'none';
			document.getElementById('creditPaymentTypeCode').style.display = 'block';
			//document.getElementById('giftPaymentTypeCode').style.display = 'none';
		} else if (getId == 'debitcardType') {
			document.getElementById('debitPaymentTypeCode').style.display = 'block';
			document.getElementById('creditPaymentTypeCode').style.display = 'none';
			//document.getElementById('giftPaymentTypeCode').style.display = 'none';
		} /*else {
			document.getElementById('debitPaymentTypeCode').style.display = 'none';
			document.getElementById('creditPaymentTypeCode').style.display = 'none';
			document.getElementById('giftPaymentTypeCode').style.display = 'block';
		}*/
	}
}

/*
This method handles
	1. Displaying of important information
	2. This method toggle depends upon the user click the header
*/
	var get_mod_value=1;
	function displayImportantInfo(id){
		var getId = document.getElementById(id);
		var mod_value = get_mod_value%2;
		get_mod_value++;
		if(mod_value==1){
			getId.className = "item-content";
			important_infosection.className = "item open";
		}
		if(mod_value==0){
			getId.className = "disNone";
			important_infosection.className = "item";
		}
	}
/* displayImportantInfo function- end */

/*
This method handles
	1. Displaying of tootltip information - total price
	2. This method using while mouseover and mouse out showing total price
*/
		function totalprice_displayTooltipInfo_mouseover(id){
			var displayTooltip = document.getElementById(id);
			//displayTooltip.style= "display: block; position: relative; left: 3px;";
			//displayTooltip.className="tooltip_visible";
			displayTooltip.style.display = "block";
		}
		/*displayTooltipInfo_mouseover - end */

		function totalprice_displayTooltipInfo_mouseout(id){
			var displayTooltip = document.getElementById(id);
			displayTooltip.style.display = "none";
		}
		/*displayTooltipInfo_mouseout - end */

/*Method end*/

/*
This method handles
	1. Displaying of tootltip information
	2. This method using while mouseover and mouse out showing card and security code information
*/
	function displayTooltipInfo_mouseover(id){
		var displayTooltip = document.getElementById(id);
		//displayTooltip.style= "display: block; position: relative; left: 3px;";
		displayTooltip.style.display = "block";
	}
	/*displayTooltipInfo_mouseover - end */

	function displayTooltipInfo_mouseout(id){
		var displayTooltip = document.getElementById(id);
		displayTooltip.style.display = "none";
	}
	/*displayTooltipInfo_mouseout - end */

/*Method end*/

	/*
	This method handles
		1. Hidden of alert message content
	*/
		function closeAlertMsg(id){
			document.getElementById(id).style.display="none";
		}
		/*closeAlertMsg method - end*/