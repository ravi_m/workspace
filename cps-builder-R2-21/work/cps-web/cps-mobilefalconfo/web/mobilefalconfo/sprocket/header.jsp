<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:formatNumber value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}" var="totalCostingLine" type="number" pattern="####" maxFractionDigits="2" minFractionDigits="2"/>
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page"/>
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:if test="${not empty bookingComponent.breadCrumbTrail}">
<c:set var="optionsUrl" value="${bookingComponent.breadCrumbTrail['OPTIONS']}"/>
<c:set var="searchUrl" value="${bookingComponent.breadCrumbTrail['SEARCH_RESULT']}"/>
<c:set var="passengersUrl" value="${bookingComponent.breadCrumbTrail['PASSENGERS']}"/>
<c:set var="extrasUrl" value="${bookingComponent.breadCrumbTrail['EXTRAS']}"/>
</c:if>

<c:choose>
   <c:when test="${bookingComponent.nonPaymentData['TUIHeaderSwitch'] eq 'ON'}">
      <c:set value="true" var="tuiLogo" />
   </c:when>
   <c:otherwise>
      <c:set value="false" var="tuiLogo" />  
   </c:otherwise>
</c:choose>

<div id="book-flow-header" class="flight-only">
				<div class="content-width">
					<div class="logo<c:if test="${tuiLogo}">TUI</c:if> thomson firstchoiceX falconX"><a href="${bookingComponent.clientURLLinks.homePageURL}"></a></div>
					<c:if test="${!tuiLogo}">
						<img width="58"  height="77" class="nomobile  nominitablet " src="/cms-cps/mobilefalconfo/images/WOT_Endorsement_V2_3C.png" id="header-wtui" alt="Welcome to the world of TUI">
					</c:if>	
					<div class="summary-panel-trigger b abs bg-tui-sand black">
						<i class="caret fly-out"></i>&euro;<c:out value="${totalcost[0]}."/><span class="pennys"><c:out value="${totalcost[1]}"/></span>

					</div>
				</div>
			</div>
			<div id="book-flow-progress">
				<div class="content-width">
					<div class="scroll uppercase" id="breadcrumb" data-scroll-dw="true" data-scroll-options='{"scrollX": true, "scrollY": false, "keyBindings": true, "mouseWheel": true}'>
						<ul class="c">
							<li class="back"><a href="<c:out value='${searchUrl}'/>"><span class="rel b">1</span> Flights</a></li>
							<li class="back"><a href="<c:out value='${optionsUrl}'/>"><span class="rel b">2</span> Options</a></li>
							<li class="back"><a href="<c:out value='${extrasUrl}'/>"><span class="rel b">3</span> Extras</a></li>
							<li class="back"><a href="<c:out value='${passengersUrl}'/>"><span class="rel b">4</span> Passengers</a></li>
							<li class="active"><span class="rel b">5</span> Payment</li>
						</ul>
					</div>
				</div>
			</div>