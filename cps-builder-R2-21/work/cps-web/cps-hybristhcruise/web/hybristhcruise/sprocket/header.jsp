<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<c:choose>
   <c:when test="${bookingComponent.nonPaymentData['TUIHeaderSwitch'] eq 'ON'}">
      <c:set value="true" var="tuiLogo" />
   </c:when>
   <c:otherwise>
      <c:set value="false" var="tuiLogo" />  
   </c:otherwise>
</c:choose>

	<html lang="en" class="" id="firstchoice">
	<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<meta name="viewport" content="width=device-width,  itial-scale=1">
	<script>
			var ensLinkTrack = function(){};
			</script>
			</head>
			<body>
				<div id="header">
					<!--<div class="beta">
						 <p>Welcome to our brand new website.</p>
						<p class="tell-us">
							<a href="http://www.thomson.co.uk/beta/send-us-an-email/provide-feedback.html" target="_blank" class="ensLinkTrack">Tell us what you think</a> or <a href="http://www.thomson.co.uk/beta/send-us-an-email/provide-feedback.html" target="_blank" class="ensLinkTrack" >Report a problem</a>
						</p>
						<a href="#" class="button">Switch to main site</a>
					</div> -->
					<div class="message-window" style="display:none" id="tellUsWhatYouThink">
						<a href="#" class="close ensLinkTrack"> <span class="text">Close</span></a>
						<div class="message-content small-pop beta-pop">
							<h3>Before you go...</h3>
							<p>We'd love to know what you made of the new site:</p>
							<ul>
								<li><a target="_blank" href="http://www.thomson.co.uk/" class="ensLinkTrack">Tell us what you think</a></li>
								<li><a target="_blank" href="http://www.thomson.co.uk/?customer=switch" class="ensLinkTrack">Switch to our main site</a></li>
							</ul>
						</div>
					</div>
					<c:if test="${!tuiLogo}">
					<div id="utils">
						<ul>
							<!-- <li><a target="_blank" href="http://www.thomson.co.uk/destinations/faqCategories">Ask a Question</a></li> -->
							<li><img width="58" height="77" src="/cms-cps/hybrisfalcon/images/WOT_Endorsement_V2_3C.png" id="header-wtui" alt="Welcome to the world of TUI"></li>
						</ul>
					</div>
					</c:if>
					<div id="logo<c:if test="${tuiLogo}">TUI</c:if>">
						<a target="_blank" class="ensLinkTrack" href="${bookingComponent.clientURLLinks.homePageURL}">Thomson</a>

					</div>
					
				</div>
		
		</body>
	</html>