		<%@ page import="com.tui.uk.config.ConfReader"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

		
		
	<%
				String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.desktop.thcruise" , "");
				pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);

				%>
			<script>
			var ensLinkTrack = function(){};
			function displayHolidayTypes(ele){
				var holiday_type = new Array();
				holiday_type[0]="holidaytypes";
				holiday_type[1]="popDestinations";
				holiday_type[2]="longhaul";
				holiday_type[3]="shorthaul";
				holiday_type[4]="flightsTo";
				holiday_type[5]="cruseDestination";
				var get_holiday_type_length=holiday_type.length;
				for(var i=0;i<get_holiday_type_length;i++){
					document.getElementById(holiday_type[i]).style.display="none";
					document.getElementById(holiday_type[i]+"_li").className="";
				}
				var split_type=ele.id.split("_");
				document.getElementById(split_type[0]).style.display="block";
				ele.className="active";
			}
			</script>
				<div id="inner-footer">
					<ul id="footer-utils">
						<!--<li>
							<h3>Holiday extras</h3>
							<ul>
								<li><a href="http://www.thomsonexcursions.co.uk/" target="_blank" class="ensLinkTrack">Excursions</a></li>
								<li><a href="http://www.carhiremarket.com/thomson/" target="_blank" class="ensLinkTrack">Car hire</a></li>
								<li><a href="http://www.thomson.co.uk/editorial/extras/foreign-exchange.html" target="_blank" class="ensLinkTrack">Foreign Exchange</a></li>
								<li><a href="http://www.holidayextras.co.uk/thomson/parking.html" target="_blank" class="ensLinkTrack">Airport parking</a></li>
								<li><a href="http://www.thomson.co.uk/editorial/extras/travel-money-card.html" target="_blank" class="ensLinkTrack">Money Card</a></li>
								<li><a href="http://www.thomsonins.co.uk/travel/default.aspx" target="_blank"class="ensLinkTrack">Travel insurance</a></li>
							</ul>
						</li>-->

						<li id="safe-hands">
							<h3>You're in safe hands</h3>
							<!-- <p>We're part of TUI Group - one of the world's leading travel companies. And all of our holidays are designed to help you Discover Your Smile.</p> -->
							<p>Just so you know, Thomson is now called TUI and we're part of TUI Group - the world's leading travel company. All of our holidays are designed to help you 'Discover your smile'.</p>
							<p class="authority">
								<!-- <img width="93" height="19" alt="World of TUI" src="/cms-cps/thomson/images/WOT-logo.png" class="wot-logo" /> -->
								<a class="abta ensLinkTrack" href="http://www.abta.com/find-a-holiday/member-search/5736" target="_blank">ABTA</a>
								<a class="atol ensLinkTrack" href="http://www.caa.co.uk/default.aspx?catid=27" target="_blank">ATOL</a>
							</p>
						</li>
						<li class="staySafeAbroad">
							<%=staySafeAbroad%>
						</li>
						<li>
							<h3>Find a local store</h3>
							<p><a href="http://www.thomson.co.uk/shopfinder/shop-finder.html" target="_blank" class="ensLinkTrack">Shop Finder</a></p>
							<p><a href="https://www.tui.co.uk/destinations/info/tui-credit-card" target="_blank" class="ensLinkTrack">TUI Credit Card</a></p>
							
							<%-- <p><a href="http://www.thomson.co.uk/destinations/info/thomson-credit-card" target="_blank" class="ensLinkTrack"><c:out value="${textCapValue}"/> Credit Card</a></p>
 --%>
                      
						</li>

						<li id="questions">
							<h3>Search for anything</h3>
							<form action="https://www.thomson.co.uk/gsa/gsa.html" method="get">
								<div class="formrow">
									<textarea  name="q" class="textfield" placeholder="e.g. Where do I print my e-tickets?"></textarea>
									<!-- <input type="hidden" name="site" value="firstchoice_collection" />
									<input type="hidden" name="client" value="fc-hugo-main" />
									<input type="hidden" name="proxystylesheet" value="fc-hugo-main" />
									<input type="hidden" name="output" value="xml_no_dtd" />
									<input type="hidden" name="submit" value="Go" /> -->

									    <input type="hidden" value="default_collection" name="site">
							            <input type="hidden" value="production_frontend" name="client">
							            <input type="hidden" value="production_frontend" name="proxystylesheet">
							            <input type="hidden" value="xml_no_dtd" name="output">
							            <input type="hidden" value="Go" name="submit">


								</div>
								<div class="floater">
									<button class="button fr mt4 small">Search</button>
                                     <p class="help fl"><a target="_blank" href="http://www.thomson.co.uk/destinations/faqCategories" class="ensLinkTrack">Ask a question</a></p>
									<p class="contact-us fl"><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/contact-us.html" class="ensLinkTrack">Contact us</a></p>
								</div>
							</form>
						</li>
					</ul>
					<!-- <script type="text/javascript">
					dojoConfig.addModuleName("tui/widget/Tabs");
					</script> -->

					<div id="footer-seo">
						<div class="tabs-container">
							<ul class="tabs">
								<li onclick="displayHolidayTypes(this);"  id="holidaytypes_li" class="active"><a enslinktrackattached="true"  class="ensLinkTrack" >Holiday Types</a></li>
								<li onclick="displayHolidayTypes(this);"  id="popDestinations_li"><a enslinktrackattached="true"  class="ensLinkTrack" >Popular Destinations</a></li>
								<li onclick="displayHolidayTypes(this);"  id="longhaul_li"><a enslinktrackattached="true" class="ensLinkTrack" >Mid/Long haul</a></li>
								<li onclick="displayHolidayTypes(this);"  id="shorthaul_li"><a enslinktrackattached="true"  class="ensLinkTrack" >Short haul</a></li>
								<li onclick="displayHolidayTypes(this);"  id="flightsTo_li"><a enslinktrackattached="true"  class="ensLinkTrack" >Flights To</a></li>
								<li onclick="displayHolidayTypes(this);"  id="cruseDestination_li"><a enslinktrackattached="true"  class="ensLinkTrack" >Cruise</a></li>
							</ul>
							<div id="holidaytypes" class="menu" style="display:block">
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/deals" class="ensLinkTrack" >Cheap Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/holidays/luxury" class="ensLinkTrack" >Luxury Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/deals/summer-2017-deals" class="ensLinkTrack" >Sunshine Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/holidays/family" class="ensLinkTrack" >Family Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/holidays/villas" class="ensLinkTrack" >Villa Holidays</a></li>


								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/deals/winter-sun-holidays" class="ensLinkTrack" >Winter Sun Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/holidays/all-inclusive" class="ensLinkTrack" >All inclusive holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/holidays/" class="ensLinkTrack" >Package Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/holidays/beach" class="ensLinkTrack" >Beach holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/deals/short-breaks" class="ensLinkTrack" >Short Breaks</a></li>

								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/holidays/last-minute" class="ensLinkTrack" >Late holiday deals</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/holidays/city-breaks" class="ensLinkTrack" >City Breaks <script> document.write((new Date()).getFullYear()) </script></a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/deals/summer-2017-deals" class="ensLinkTrack" >Summer holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/holidays/last-minute" class="ensLinkTrack" >Last minute Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/holidays/multi-centre" class="ensLinkTrack" >Multi-Centre Holidays</a></li>


								</ul>
							</div>
							<div id="popDestinations" class="menu" style="display:none">
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/florida/miami/holidays-miami.html" class="ensLinkTrack" >Miami Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/costa-blanca/benidorm/holidays-benidorm.html" class="ensLinkTrack" >Benidorm holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/canary-islands/holidays-canary-islands.html" class="ensLinkTrack" >Canary Islands holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/lanzarote/holidays-lanzarote.html" class="ensLinkTrack" >Lanzarote holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/american-cities/las-vegas/holidays-las-vegas.html" class="ensLinkTrack" >Las Vegas Holidays</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/caribbean/mexico/holidays-mexico.html" class="ensLinkTrack" >Mexico Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/cyprus/holidays-cyprus.html" class="ensLinkTrack" >Cyprus holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/menorca/holidays-menorca.html" class="ensLinkTrack" >Menorca holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/majorca/holidays-majorca.html" class="ensLinkTrack" >Majorca holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/american-cities/new-york/holidays-new-york.html" class="ensLinkTrack" >New York Holidays</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/greece/crete/holidays-crete.html" class="ensLinkTrack" >Crete holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/ibiza/holidays-ibiza.html" class="ensLinkTrack" >Ibiza holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/gran-canaria/holidays-gran-canaria.html" class="ensLinkTrack" >Gran Canaria holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/portugal/algarve/holidays-algarve.html" class="ensLinkTrack" >Algarve holidays</a></li>
								</ul>
							</div>
							<div id="longhaul" class="menu" style="display:none">
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/caribbean/aruba-island/holidays-aruba-island.html" class="ensLinkTrack" >Aruba holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/asia/thailand/thailand/bangkok/holidays-bangkok.html" class="ensLinkTrack" >Bangkok Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/caribbean/barbados/holidays-barbados.html" class="ensLinkTrack" >Barbados holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/asia/vietnam/holidays-vietnam.html" class="ensLinkTrack" >Vietnam Holidays</a></li>

									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/africa/cape-verde/holidays-cape-verde.html" class="ensLinkTrack" >Cape Verde holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/caribbean/holidays-caribbean.html" class="ensLinkTrack" >Caribbean Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/the-americas/costa-rica/holidays-costa-rica.html" class="ensLinkTrack" >Costa Rica Holidays</a></li>



								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/caribbean/cuba/holidays-cuba.html" class="ensLinkTrack" >Cuba Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/middle-east/united-arab-emirates/dubai-and-emirates/holidays-dubai-and-emirates.html" class="ensLinkTrack" >Dubai Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/africa/egypt/holidays-egypt.html" class="ensLinkTrack" >Egypt holidays</a></li>

									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/florida/holidays-florida.html" class="ensLinkTrack" >Florida holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/asia/hong-kong/holidays-hong-kong.html" class="ensLinkTrack" >Hong Kong Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/asia/malaysia/malaysia/kuala-lumpur/holidays-kuala-lumpur.html" class="ensLinkTrack" >Kuala Lumpur Holidays</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/caribbean/jamaica/holidays-jamaica.html" class="ensLinkTrack" >Jamaica Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/indian-ocean/mauritius/holidays-mauritius.html" class="ensLinkTrack" >Mauritius holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/asia/singapore/holidays-singapore.html" class="ensLinkTrack" >Singapore Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/indian-ocean/sri-lanka/holidays-sri-lanka.html" class="ensLinkTrack" >Sri Lanka Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/asia/thailand/holidays-thailand.html" class="ensLinkTrack" >Thailand holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/tenerife/holidays-tenerife.html" class="ensLinkTrack" >Tenerife holidays</a></li>
								</ul>
							</div>
							<div id="shorthaul" class="menu" style="display:none">
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/costa-blanca/benidorm/holidays-benidorm.html" class="ensLinkTrack" >Benidorm Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/portugal/holidays-portugal.html" class="ensLinkTrack" >Portugal holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/holidays-spain.html" class="ensLinkTrack" >Spain holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/turkey/holidays-turkey.html" class="ensLinkTrack" >Turkey holidays</a></li>

								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/greece/zante/holidays-zante.html" class="ensLinkTrack" >Zante holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/africa/morocco/holidays-morocco.html" class="ensLinkTrack" >Morocco holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/croatia/holidays-croatia.html" class="ensLinkTrack" >Croatia holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/greece/holidays-greece.html" class="ensLinkTrack" >Greece holidays</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/malta/holidays-malta.html" class="ensLinkTrack" >Malta holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/bulgaria/holidays-bulgaria.html" class="ensLinkTrack" >Bulgaria holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/italy/holidays-italy.html" class="ensLinkTrack" >Italy holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/finland/lapland/holidays-lapland.html" class="ensLinkTrack" >Lapland holidays</a></li>
								</ul>
							</div>
							<div id="flightsTo" class="menu" style="display:none">
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/deals" class="ensLinkTrack" >Cheap Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/spain/canary-islands/tenerife-flights" class="ensLinkTrack" >Tenerife Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/jamaica/montego-bay-airport" class="ensLinkTrack" >Montego Bay Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/mauritius-flights" class="ensLinkTrack" >Mauritius Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/thailand-flights" class="ensLinkTrack" >Thailand Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/spain/valencia/alicante-airport" class="ensLinkTrack" >Alicante Flights</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/spain/canary-islands/lanzarote-flights" class="ensLinkTrack" >Lanzarote Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/spain/balearic-islands/ibiza-flights" class="ensLinkTrack" >Ibiza Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/united-states-of-america/florida-flights" class="ensLinkTrack" >Florida Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/united-states-of-america/florida/orlando-flights" class="ensLinkTrack" >Orlando Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/spain-flights" class="ensLinkTrack" >Spain Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/cyprus-flights" class="ensLinkTrack" >Cyprus Flights</a></li>

								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/malta-flights" class="ensLinkTrack" >Malta Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/portugal-flights" class="ensLinkTrack" >Portugal Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/turkey-flights" class="ensLinkTrack" >Turkey Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/india/goa-flights" class="ensLinkTrack" >Goa Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/mexico/mexico-caribbean-coast/cancun-airport" class="ensLinkTrack" >Cancun Flights</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flight/barbados-flights" class="ensLinkTrack" >Barbados Flights</a></li>

								</ul>
							</div>
							<div id="cruseDestination" class="menu" style="display:none">
			                 <ul>
				                 <li><a href="http://www.thomson.co.uk/cruise/central-america/puerto-limon-port/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Puerto Limon</a></li>
				                 <li><a href="http://www.thomson.co.uk/cruise/caribbean-cruises/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Caribbean Cruises</a></li>
								 <li><a href="http://www.thomson.co.uk/cruise/fjords-iceland-and-arctic-cruises/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Fjords Cruise</a></li>
				                 <li><a href="http://www.thomson.co.uk/cruise/western-mediterranean-cruises/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Italian Cruises</a></li>
				                 <li><a href="http://www.thomson.co.uk/cruise/cruise-and-stay/croatia/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Croatia</a></li>
				                 <li><a href="http://www.thomson.co.uk/cruise/western-mediterranean-cruises/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Mediterranean Cruises</a></li>
			                </ul>
			               <ul>
								<li><a href="http://www.thomson.co.uk/cruise/canary-islands-and-atlantic-cruises/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Canary Islands and Atlantic</a></li>
								<li><a href="http://www.thomson.co.uk/cruise/central-america-cruises/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Central America</a></li>
								<li><a href="http://www.thomson.co.uk/cruise/cruise-from-the-uk/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cruises from the UK</a></li>
								<li><a href="http://www.thomson.co.uk/cruise/eastern-mediterranean/dubrovnik-port/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Dubrovnik</a></li>
								<li><a href="http://www.thomson.co.uk/cruise/western-mediterranean/barcelona-port/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Barcelona</a></li>
								<li><a href="http://www.thomson.co.uk/cruise/northern-europe-and-uk/southampton-port/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cruises from Southampton</a></li>
							</ul>
							<ul>
								<li><a href="http://www.thomson.co.uk/destinations/info/all-inclusive-cruises" class="ensLinkTrack" data-componentId="WF_COM_200-3">All Inclusive</a></li>
								<li><a href="http://www.thomson.co.uk/destinations/info/family-cruises" class="ensLinkTrack" data-componentId="WF_COM_200-3">Family Cruises</a></li>
								<li><a href="http://www.thomson.co.uk/destinations/info/last-minute-cruises" class="ensLinkTrack" data-componentId="WF_COM_200-3">Last Minute Cruises</a></li>
								<li><a href="http://www.thomson.co.uk/cruise/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cruises</a></li>
								<li><a href="http://www.thomson.co.uk/destinations/info/summer-2017-cruises" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cruise Deals</a></li>

								<li><a href="http://www.thomson.co.uk/cruise/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cruise Holidays</a></li>

							</ul>
						</div>
						</div>
					</div>
				</div>

			<div id="footer">
				<ul >
					<li><a target="_blank" href="http://communicationcentre.thomson.co.uk/" class="ensLinkTrack">Communications Centre</a></li>
					<li><a target="_blank" href="http://tuijobsuk.co.uk/" class="ensLinkTrack">Travel Jobs</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/partners/thomson-affiliate-programme.html" class="ensLinkTrack">Affiliates</a></li>
					<li><a target="_blank" href="https://www.tui.co.uk/destinations/info/my-tui-app" class="ensLinkTrack">My TUI</a></li>
					<li><a target="_blank" href="https://blog.tui.co.uk/" class="ensLinkTrack">TUI Blog</a></li>
					<%-- <li><a target="_blank" href="http://www.thomson.co.uk/blog/" class="ensLinkTrack"><c:out value="${textCapValue}"/> Blog</a></li> --%>
				</ul>
				<ul>
					<li>&copy; <script> document.write((new Date()).getFullYear()) </script> <a target="_blank" href="http://www.tuigroup.com/en-en" class="ensLinkTrack">TUI Group</a></li>
					<%-- <li><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/about-thomson.html" class="ensLinkTrack">About <c:out value="${textCapValue}"/></a></li> --%>
					<li><a target="_blank" href="https://www.tui.co.uk/destinations/info/my-tui-app" class="ensLinkTrack">About TUI</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html" class="ensLinkTrack">Terms &amp; Conditions</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/gsa/gsa.html?q=customer+welfare&site=default_collection&client=production_frontend_new&proxystylesheet=production_frontend_new&;output=xml_no_dtd&;submit=Go" class="ensLinkTrack">Customer Welfare</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html" class="ensLinkTrack">Privacy Policy</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/statement-on-cookies.html" class="ensLinkTrack">Statement on Cookies</a></li>
					<li>
					<c:choose>
						<c:when test="${applyCreditCardSurcharge eq 'true'}">
					<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html" class="ensLinkTrack">Credit Card Fees</a>
					</c:when>
					<c:otherwise>
					<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html" class="ensLinkTrack">Ways to Pay</a>
					</c:otherwise>
					</c:choose>
					</li>
				</ul>
				<p><span>Many of the flights and flight-inclusive holidays on this website are financially protected by the ATOL scheme. But ATOL protection does not apply to all holiday and travel services listed on this website. Please ask us to confirm what protection may apply to your booking. If you do not receive an ATOL Certificate then the booking will not be ATOL protected. If you do receive an ATOL Certificate but all the parts of your trip are not listed on it, those parts will not be ATOL protected. Please see our booking conditions for information or for more information about financial protection and the ATOL Certificate go to:<span class="apple-converted-space">&nbsp;</span></span><span style="font-family: TUIType, sans-serif;"><a target="_blank" href="http://www.atol.org.uk/ATOLCertificate"><span style="font-size: 8.5pt; color: rgb(102, 102, 102);">www.atol.org.uk/ATOLCertificate</span></a></span>&nbsp;</p>

			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,chkTuiMarketingAllowed,chkThirdPartyMarketingAllowed,communicateByPost,communicateByPhone,communicateByEmail</c:set>
			<script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
				 tui.analytics.page.pageUid = "paymentDetailsPage";
				 <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
					 <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
					 tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
				    </c:if>

					    <c:if test= "${analyticsDataEntry.key == 'Party'}">
						tui.analytics.page.Party= "${analyticsDataEntry.value}";
						</c:if>

			    </c:forEach>
				</script>
			</div>
