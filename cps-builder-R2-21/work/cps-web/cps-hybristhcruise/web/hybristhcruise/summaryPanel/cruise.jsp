<div class="accordOff">
	<h3 class="summaryAccordSwitch"><span class="icon cruise"></span><span class="title">CRUISE</span></h3>
	<div class="noAccord pb10px">
	
		<div class="">
			<div class="copy">
			<div class="cruise-name">
         <c:set var="cruiseResort" value="${bookingComponent.cruiseSummary.ship.resort}" />
          <c:set var="cruiseResortArray" value="${fn:split(cruiseResort,'|')}" />
         
                           <!-- <c:out value="${bookingComponent.cruiseSummary.ship.resort}" />-->
                           
                           <c:out value="${cruiseResortArray[0]}" />
                           <c:if test="${not empty cruiseResortArray[1]}"><br />
                              <span class="followed">followed by </span><br />
                              <c:out value="${cruiseResortArray[1]}" />
                           </c:if>
             </div>
                
				<div class="ship-name">
			      
                            <c:if test="${not empty bookingComponent.cruiseSummary.ship.destination}">
                               <c:out value="${bookingComponent.cruiseSummary.ship.destination}" />
				            </c:if>
				</div>
				<div class="cruise-duration">
                       <c:out value="${bookingComponent.cruiseSummary.duration}" />
                        <c:choose>
							<c:when test="${bookingComponent.cruiseSummary.duration == 1}">
								night cruise
							</c:when>
							<c:otherwise>
								 night cruise
							</c:otherwise>
						</c:choose>
                </div>
		</div>
			
				
			
	<ul class="item-list">
	<c:if test="${not empty bookingComponent.pricingDetails}">
			<c:set var="cruiseExtras" value="${bookingComponent.pricingDetails['CRUISE_EXTRAS_PRICE_COMPONENT']}" />
		<c:if test="${not empty cruiseExtras}">
	     <c:forEach var="cruiseExtra" items="${cruiseExtras}">
	<li>
	
		<span class="fl">
					
			<c:out value="${cruiseExtra.itemDescription}" escapeXml='false'/>
				<c:if test="${cruiseExtra.quantity ne null && cruiseExtra.quantity > 0}"> x <c:out value="${cruiseExtra.quantity}" />
				</c:if>
		 </span>
		 
		<span class="fr">
			<c:choose>
				<c:when test="${cruiseExtra.additionalPriceInfo eq 'included'}">
					<c:out value="Included" />
				</c:when>
				<c:otherwise>
					<c:if test="${cruiseExtra.amount.amount >= 0 && cruiseExtra.quantity ne null && cruiseExtra.quantity > 0}">
										&pound;<fmt:formatNumber value="${cruiseExtra.amount.amount}"
												type="number" maxFractionDigits="2" minFractionDigits="2"
												pattern="#####.##" />
										</c:if>
										
					<c:if test="${cruiseExtra.amount.amount < 0}">
										-&pound;<fmt:formatNumber value="${cruiseExtra.amount.amount}"
												type="number" maxFractionDigits="2" minFractionDigits="2"
												pattern="#####.##" />
										</c:if>
				</c:otherwise>
			</c:choose>
		 </span>
		 
		<span class="fl">
		 <c:out value="${cruiseExtra.itemInfo}"/>
		 </span>	
		<div class="clear"></div>
		
	</li>
	
            </c:forEach>	
	    </c:if>
	</c:if>		
				<li>
					<span class="fl">
				
					<c:out value="${bookingComponent.cruiseSummary.ship.boardBasis}" />
					</span>
					
					
					<div class="clear"></div>
				</li>
			</ul>
			 
			
		</div>
    
	</div>
</div>

<c:if test="${empty bookingComponent.flightSummary}">
	<div class="accordOff">
		<c:if test="${(not empty bookingComponent.cruiseSummary.startPort) || (not empty bookingComponent.cruiseSummary.endPort)}">
			<h3 class="summaryAccordSwitch"><span class="icon cruise"></span><span class="title">SAILINGS</span></h3>
		</c:if>
		<div class="noAccord pb0px" style="background:#e9f2f9 url("/cms-cps/hybristhcruise/images/search-results-eliptical-shadow-top.png") no-repeat scroll center top / 100% 0px;">
			<div class="">
				<c:if test="${not empty bookingComponent.cruiseSummary.startPort}">
					<ul class="flightInfo">
						<li class="place inboundStartPort"><span class="from">SAILING FROM <c:out value="${bookingComponent.cruiseSummary.startPort}"/></span></li>
						<li class="itinerary inboundStartPort">
						   <span class="itinerary-dates"><fmt:formatDate value="${bookingComponent.cruiseSummary.startDate}" pattern="E dd MMM yyyy" /></span>
						   <span class="itinerary-departure-time"><c:out value="${bookingComponent.cruiseSummary.startTime}" /></span>
						</li>
					</ul>
				</c:if>
				<c:if test="${not empty bookingComponent.cruiseSummary.endPort}">
					<c:if test="${not empty bookingComponent.cruiseSummary.startPort}">
						<div style="padding-top:5px;" class="clear"></div>
						<div style="padding-bottom:3px;" class="summary-sub-border item-list"></div>
						<div style="padding-top:5px;" class="clear"></div>
					</c:if>
					<ul class="flightInfo">
						<li class="place outboundEndPort"><span class="from">SAILING BACK TO <c:out value="${bookingComponent.cruiseSummary.endPort}"/></span></li>
						<li class="itinerary outboundEndPort">
						   <span class="itinerary-dates"><fmt:formatDate value="${bookingComponent.cruiseSummary.endDate}" pattern="E dd MMM yyyy" /></span>
						   <span class="itinerary-departure-time"><c:out value="${bookingComponent.cruiseSummary.endTime}" /></span>
						</li>
					</ul>
				</c:if>
			</div>
		</div>
	</div>
</c:if> 
