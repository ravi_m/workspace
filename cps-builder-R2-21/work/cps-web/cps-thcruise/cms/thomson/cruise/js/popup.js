function Popup(popURL,popW,popH,attr){
   if (!popH) { popH = 350 }
   if (!popW) { popW = 600 }
   var winLeft = (screen.width-popW)/2;
   var winTop = (screen.height-popH-30)/2;
   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
   popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
   popupWin.close()

   if(popURL.indexOf("www.360travelguide.net")> -1)
   {
      popupWin=window.open(popURL,"popupWindowVideoTour",winProp);
   }
   else
   {
      popupWin=window.open(popURL,"popupWindow",winProp);
   }

   popupWin.window.focus()
}

function popUpBookingConditions(accInv, flightInv,deptDate,clientDomainURL)
{
   var d = new Date();
   var currentDate = d.getDate();
   var currentMonth = d.getMonth();
   currentMonth++;
   currentDate = currentDate+"";
   currentMonth = currentMonth+"";
   if (currentDate.length == 1)
   {
      currentDate = "0"+currentDate;
   }

   if (currentMonth.length == 1)
   {
      currentMonth = "0"+currentMonth;
   }
   if (accInv=="ThomsonSun")
   {
      accInv="TRACS"
   }
   if (flightInv=="Amadeus")
   {
      flightInv = flightInv.toUpperCase();
   }
   if (flightInv=="TRACSA")
   {
      flightInv = "TRACS";
   }
   if (flightInv=="NAV")
   {
      flightInv="NAVITARE";
   }
   var formattedDate = d.getFullYear()+"-"+currentMonth +"-"+currentDate;
   var url=clientDomainURL+"/webcruise/page/common/tandc/tandc.page?"+"accommInvSys="+accInv+"&"+"flightInvSys="+flightInv+"&"+"date="+formattedDate+"&"+"lang="+"en"+"&"+"deptDate="+deptDate
   window.open(url, "", "width=750,height=500,scrollbars=yes");
}

function showHiddenInformation(elemId){
   div = document.getElementById(elemId);
   if(div.style.display=='none')
   {
      div.style.display = "block";
   }
   else
   {
      div.style.display = "none";
   }
}