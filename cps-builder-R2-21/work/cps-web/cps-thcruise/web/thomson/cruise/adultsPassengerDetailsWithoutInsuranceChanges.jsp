<%-- To Be Deleted Once Cruise with insurance changes goes live. --%>
<%@include file="/common/commonTagLibs.jspf"%>
<li>
   <p class="paxNumber"><c:out value="${passengerRoom.label}"/></p>
   <c:set var="titlekey" value="personaldetails_${passengerCount}_title"/>
   <div class="title">
      <select name="<c:out value='${titlekey}' />" alt="Please select the title of passenger ${passengerCount+1}.|Y|TITLE">
	   <option value="" selected></option>
         <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mr'}">selected</c:if>>Mr</option>
		 <option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mrs'}">selected</c:if>>Mrs</option>
	     <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
		 <option value="Ms" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Ms'}">selected</c:if>>Ms</option>
		 <option value="Dr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Dr'}">selected</c:if>>Dr</option>
		 <option value="Rev" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Rev'}">selected</c:if>>Rev</option>
		 <option value="Prof" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Prof'}">selected</c:if>>Prof</option>
      </select>
   </div>

   <c:set var="foreNameKey" value="personaldetails_${passengerCount}_foreName"/>
   <div class="firstName">
      <input type="text" maxlength="15" value="<c:out value="${bookingComponent.nonPaymentData[foreNameKey]}"/>" id="<c:out value="${foreNameKey}"/>" name="<c:out value="${foreNameKey}"/>" alt="Please enter the names of all passengers as shown on your passports.|Y|NAME"/>
   </div>

   <c:set var="middleNameKey" value="personaldetails_${passengerCount}_middleInitial"/>
   <div class="middleInitial">
      <input type="text" maxlength="1" value="<c:out value="${bookingComponent.nonPaymentData[middleNameKey]}"/>" name="<c:out value="${middleNameKey}"/>" alt="Initial|N|ALPHA"/>
   </div>

   <c:set var="lastNameKey" value="personaldetails_${passengerCount}_surName"/>
   <div class="surname">
      <input type="text" onclick="javascript:unCheck();" maxlength="15" class="medium" value="<c:out value="${bookingComponent.nonPaymentData[lastNameKey]}"/>" id="lastname_<c:out value='${passengerCount}'/>"  name="<c:out value="${lastNameKey}"/>" alt="Please enter the names of all passengers as shown on your passports.|Y|NAME"/>
   </div>
   <input type="hidden" name="personaldetails_${passengerCount}_ageClassificationCode" value="${passengerRoom.ageClassification}"/>
   <%@include file="seniorAgeCheckbox.jspf" %>
</li><%-- END Adult or senior passenger details --%>

<%-- The following is for the lead passenger only --%>
<%@include file="roomConfigExtraInformationWithoutIns.jspf" %>