<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}"
	scope="request" />
<c:set var="errorPage" value="false" scope="request" />
<c:set var="multiCentre" value="${bookingComponent.multiCentre}" />
<c:set var="alertMessage"
	value="${bookingComponent.nonPaymentData['alertMessage']}" />
<c:set var='clientapp'
	value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName}' />
<c:set var="hccsmap" value="${bookingComponent.hccsMap}" />

<!-- currency -->

<c:choose>
	<c:when test="${(clientapp == 'ANCFALCON' || clientapp == 'ANCFJFO')}">
		<c:set var='currency' scope="request" value='&#128;' />
	</c:when>
	<c:otherwise>
		<c:set var='currency' scope="request" value='&#163;' />
	</c:otherwise>
</c:choose>


<%
	String clientApp = (String) pageContext.getAttribute("clientapp");
	clientApp = (clientApp != null) ? clientApp.trim() : clientApp;
	String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp + ".3DSecure", "");
	pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
	String cvvEnabled = com.tui.uk.config.ConfReader.getConfEntry(clientApp + ".CV2AVS.Enabled", "false");
	pageContext.setAttribute("cvvEnabled", cvvEnabled, PageContext.REQUEST_SCOPE);
	String isExcursion = com.tui.uk.config.ConfReader
			.getConfEntry(clientApp + ".summaryPanelWithExcursionTickets", "false");
	pageContext.setAttribute("isExcursion", isExcursion, PageContext.REQUEST_SCOPE);
	//added for payment page consolidation
	String title = com.tui.uk.config.ConfReader.getConfEntry(clientApp + ".title", "");
	String headerText = com.tui.uk.config.ConfReader.getConfEntry(clientApp + ".headerText", "");
	String holidaySummaryAccordian = com.tui.uk.config.ConfReader.getConfEntry("FAE.holidaySummaryAccordian",
			"FLIGHT EXTRAS SUMMARY");
	String paymentAccordian = com.tui.uk.config.ConfReader.getConfEntry("FAE.paymentAccordian", "payment");
%>



<!DOCTYPE HTML>
<html lang="en-US" class="" id="falcon">
<head>
<meta charset="UTF-8">
<title><%=title%></title>
<meta name="viewport"
	content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">

<script>
var hccIframe = "${hccsmap.hccSwitch == 'true'}" /* this variable referring to functions_depositSection_directDebit.js */
</script>

<c:if
	test="${hccsmap.hccSwitch == 'true' && hccsmap.paymentFailure eq 'true'}">
	<form id="redirectorForm"
		action="${bookingComponent.paymentFailureURL}" method="post"></form>
	<script>
		(function(){
			document.getElementById('redirectorForm').submit();
		}());
	</script>
</c:if>

<fmt:formatDate var="paySummaryCurrentDateFormatted" value="${paySummaryCurrentDate}" pattern="EEE dd MMM yyyy"/>
<fmt:parseDate value="${paySummaryCurrentDateFormatted}" type="date" pattern="EEE dd MMM yyyy" var="paySummaryCurrentDateValue"/>
<c:set var="paySummaryLeftToPay" value="${bookingComponent.payableAmount.amount-bookingInfo.calculatedDiscount.amount}"/>
<c:set var="paySummaryLeftToPaySplit" value="${fn:split(paySummaryLeftToPay, '.')}" />
<c:set var="paySummaryLeftToPay_Dec" value="${paySummaryLeftToPaySplit[1]}"/>
<c:if test="${empty paySummaryLeftToPay_Dec}">
<c:set var="paySummaryLeftToPay_Dec" value="00" />
</c:if>



<link rel="icon" type="image/png"
	href="/cms-cps/amendandcancel/images/favicon_${fn:toLowerCase(clientapp)}.png" />


<link rel="stylesheet"
	href="/cms-cps/amendandcancel/css/fae_bf_${fn:toLowerCase(clientapp)}.css" />
<link rel="stylesheet"
	href="/cms-cps/amendandcancel/css/fae_icons_${fn:toLowerCase(clientapp)}.css" />
<link rel="stylesheet"
	href="/cms-cps/amendandcancel/css/fae_base_${fn:toLowerCase(clientapp)}.css" />
<link rel="stylesheet"
	href="/cms-cps/amendandcancel/css/fae_mobile_${fn:toLowerCase(clientapp)}.css" />
<link rel="stylesheet"
	href="/cms-cps/amendandcancel/css/fae_acss_${fn:toLowerCase(clientapp)}.css" />
<link rel="stylesheet" href="/cms-cps/amendandcancel/css/promostrip.css" />

<!-- <script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script> -->
<%@include file="fae_javascript.jspf"%>
<script src="/cms-cps/amendandcancel/js/iscroll-lite.js"
	type="text/javascript"></script>

<script type="text/javascript">
		var breadcrumbScroll = null;
		
		var lockYourPriceSummarySelected = '${bookingComponent.lockYourPriceSummary.selected}';
		var lockYourPriceSummaryPpPrice = '${bookingComponent.lockYourPriceSummary.ppPrice}';
		var lockYourPriceSummaryTotalPrice = '${bookingComponent.lockYourPriceSummary.totalPrice}';
		var cancellationPeriodInHours = '${bookingComponent.lockYourPriceSummary.cancellationPeriodInHours}';
		var isLockYourPriceMMBFlow = '${bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow}';
		var seoSection ;
		jQuery(document).ready(function() {
			enableSelectBoxes();
			enableCheckBoxes();
			summaryPanelModal();
			intiateIscroll(jQuery(".scroll"));
			changeAddr();
			radioGrpInit();
			initTooltip();

			var allLinksPanels = jQuery('.accordion .content').hide();
			jQuery('.trigger').click(function() {
				seoSection =  jQuery(".accordion > .item").toggleClass("item item open");
				if(seoSection.hasClass('item open')) {
				   allLinksPanels.slideDown("fast");
				}else {
					allLinksPanels.slideUp("fast");
				}
			});
			var accordionPanels = jQuery(".component .accordion-content").hide();
			jQuery(".accordion-trigger").click(function(e) {
				var accordionSection = jQuery(this).parent().find(".accordion-content");
				var iconCollapseExpand = jQuery(this).find(".icon-collapse-expand");
				if(jQuery(this).hasClass("open")){
					accordionSection.slideUp("fast");
					if(jQuery(this)[0].id== "summary-panel"){



						jQuery(this)[0].removeAttribute('analytics-text','HSC');
						jQuery(this)[0].removeAttribute('analytics-id','PAYUS');
					}else if(jQuery(this)[0].id == "passenger-details"){
						jQuery(this)[0].removeAttribute('analytics-text','PDC');
						jQuery(this)[0].removeAttribute('analytics-id','PAYUS');

					}

				} else {
					accordionSection.slideDown("fast");
					if(jQuery(this)[0].id == "summary-panel"){


							jQuery(this)[0].setAttribute('analytics-text','HSC');
						jQuery(this)[0].setAttribute('analytics-id','PAYUS');
					}else if(jQuery(this)[0].id == "passenger-details"){
						jQuery(this)[0].setAttribute('analytics-text','PDC');
						jQuery(this)[0].setAttribute('analytics-id','PAYUS');

					}
				}
				jQuery(this).toggleClass("open");
				jQuery(iconCollapseExpand).toggleClass("icon-down");
				jQuery(iconCollapseExpand).toggleClass("icon-up");

			});
				jQuery(".gift-card-accordion").parent().find(".gift-accordion-text").slideUp("fast");
				jQuery(".gift-card-accordion").click(function(e) {
				var accordionSection = jQuery(this).parent().find(".gift-accordion-text");
				var iconCollapseExpand = jQuery(this).find(".gift-collapse-expand");
				if(jQuery(this).hasClass("open")){
					accordionSection.slideUp("fast");


				} else {
					accordionSection.slideDown("fast");

				}
				jQuery(this).toggleClass("open");
				jQuery(iconCollapseExpand).toggleClass("icon-down");
				jQuery(iconCollapseExpand).toggleClass("icon-up");

		});
		});
		window.addEventListener("resize", function() {
			var scrollDiv = jQuery('#breadcrumb');
			setTimeout(function() {
				scrollintoView(scrollDiv)
			}, 200);
		}, false);

		(function() {
			var userAgent = window.navigator.userAgent;
			var msie = userAgent.indexOf("MSIE ");

			if (msie > 0){
				jQuery("html").addClass("ie");
			}
  })();
    </script>
    
<script src="/cms-cps/amendandcancel/js/faeConfig.js" type="text/javascript"></script>
<script type="text/javascript">
		var ensLinkTrack = function(){};
</script>

<script type="text/javascript" language="javascript">
	function newPopup(url) {
		var win = window.open(url,"","width=700,height=600,scrollbars=yes,resizable=yes");
	    if (win && win.focus) win.focus();
	}

	var session_timeout= ${bookingInfo.bookingComponent.sessionTimeOut};
	var IDLE_TIMEOUT = (${bookingInfo.bookingComponent.sessionTimeOut})-(${bookingInfo.bookingComponent.sessionTimeAlert});
	var _idleSecondsCounter = 0;
	var IDLE_TIMEOUT_millisecond = IDLE_TIMEOUT *1000;
	var url="${bookingInfo.bookingComponent.breadCrumbTrail['SEARCH_RESULT']}";
	var sessionTimeOutFlag =  false;
	var prePaymentUrl = '${bookingComponent.prePaymentUrl}';

	window.addEventListener('pageshow', function(event) {
		console.log(event.persisted);
		if(event.persisted) {
			location.reload();
		}
		  if(event.currentTarget.performance.navigation.type == 2)
			{
				 location.reload();
				 }
	});

	function noback(){
		window.history.forward();
		try{
			$('expiryDateMM').selectedIndex = 0;
			$('expiryDateYY').selectedIndex = 0;
		}catch(ex){console.log(ex);}
		document.getElementById("tourOperatorTermsAccepted").checked=false;
	}


	//New code added for session time out pop-up
	var initialTime = Date.now();
    var currentTime = initialTime;
    var hidden = "hidden";
    //var myVar = setInterval(CheckIdleTime, 1000);
	window.setInterval(CheckIdleTime, 1000);
	window.alertMsg();
	function alertMsg(){

		var myvar = setTimeout(function(){
		document.getElementById("sessionTime").style.display = "block";
		jQuery("div.tooltip").css("display","none");
		jQuery('html').addClass("of-h");
		var count = Math.round((session_timeout - _idleSecondsCounter)/60);
		document.getElementById("sessiontimeDisplay").innerHTML = count +" mins";
		},IDLE_TIMEOUT_millisecond);

	}
	function CheckIdleTime() {
            _idleSecondsCounter++;
            if (  _idleSecondsCounter >= session_timeout) {
                    if (sessionTimeOutFlag == false){
                   	document.getElementById("sessionTime").style.display = "none";
				document.getElementById("sessionTimeOut").style.display = "block";
				jQuery('html').addClass("of-h");
				//clearInterval(myVar);
			}else{
				document.getElementById("sessionTimeout").style.display = "none";
				jQuery('html').removeClass("of-h");
                    }
                }

            }

            adjustTimer = function() {
               currentTime = Date.now();
               //timeDiff.innerHTML = parseInt((currentTime - initialTime)/1000);
               _idleSecondsCounter = parseInt((currentTime - initialTime)/1000);
    	}
	   //navigate to technical difficulty page

            // Standards:
            if (hidden in document)
                document.addEventListener("visibilitychange", onchange);
            else if ((hidden = "mozHidden") in document)
                document.addEventListener("mozvisibilitychange", onchange);
            else if ((hidden = "webkitHidden") in document)
                document.addEventListener("webkitvisibilitychange", onchange);
            else if ((hidden = "msHidden") in document)
                document.addEventListener("msvisibilitychange", onchange);

            // IE 9 and lower:
            else if ("onfocusin" in document)
                document.onfocusin = document.onfocusout = onchange;
            // All others:
            else
                window.onpageshow = window.onpagehide
                = window.onfocus = window.onblur = onchange;

            function onchange (evt) {
                var v = "visible", h = "hidden",
                    evtMap = {
                    focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h
                    };

                evt = evt || window.event;
                if (evt.type in evtMap) {
                    //document.body.className = evtMap[evt.type];
                    if(evtMap[evt.type] == 'visible') {
                        adjustTimer();
		}
	}
                else {
                    document.body.className = this[hidden] ? "hidden" : "visible";
                    if(!this[hidden]) {
                        adjustTimer();
                    }
                }
            }

            // set the initial state (but only if browser supports the Page Visibility API)
            if( document[hidden] !== undefined )
                onchange({type: document[hidden] ? "blur" : "focus"});

	function closesessionTime(){
		document.getElementById("sessionTime").style.display = "none";
		jQuery('html').removeClass("of-h");
	}
	function reloadPage(){
		window.sessionTimeOutFlag= true;
		document.getElementById("sessionTimeout").style.display = "none";
		jQuery('html').removeClass("of-h");

		window.noback();
		window.location.replace(window.url);
		return(false);

	}

	function homepage(){
		url="${bookingInfo.bookingComponent.clientURLLinks.homePageURL}";
		window.noback();
		window.location.replace(url);
	}
	function activestate(){
		document.getElementById("sessionTime").style.display = "none";
		jQuery('html').removeClass("of-h");
		window.ajaxForCounterReset();
		window.alertMsg();
		window._idleSecondsCounter = 0;
	}
	function ajaxForCounterReset() {
		var token = "<c:out value='${param.token}' />";
		var url = "/cps/SessionTimeOutServlet?tomcat=<c:out value='${param.tomcat}'/>";
		var request = new Ajax.Request(url, {
			method : "POST"
		});
		window._idleSecondsCounter = 0;
	}

	function makePayment() {
		var paymentForm = $('CheckoutPaymentDetailsForm');
		var element = paymentForm.serialize();
		if(!window.leadTitle){
			window.leadTitle = "";
		}
		elementstring = element
				+ "&token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />";
		var url = "/cps/processMobilePayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />";
		elementstring = elementstring.replace("&title=&", "&title=" + leadTitle + "&");
		elementstring = elementstring.replace("payment_0_type=&", "");
		elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
		elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
		//if(isvalid && cardValid){
			//document.getElementById("modal").style.display = "visible";
			var request = new Ajax.Request(url, {
				method : "POST",
				parameters : elementstring,
				onSuccess : showThreeDOverlay
			});
		//}
	}

	function showThreeDOverlay(http_request) {
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			result = http_request.responseText;
			if (result.indexOf("3dspage") == -1) {
				if (result.indexOf("ERROR_MESSAGE") == -1) {
					document.postPaymentForm.action = result;
					//document.CheckoutPaymentDetailsForm.reset();
					var csrfTokenVal = "<c:out value="${bookingComponent.nonPaymentData['csrfToken']}"/>";
                    if(csrfTokenVal != '')
                    {
                     var csrfParameter = document.createElement("input");
                     csrfParameter.type = "hidden";
                     csrfParameter.name = "CSRFToken";
                     csrfParameter.value = csrfTokenVal;
                     document.postPaymentForm.appendChild(csrfParameter);
                    }

                    var loadingScreens = jQuery("#loader-screens")[0];
						var loadingScreensSticky = jQuery("html")[0];
        			loadingScreens.className += " show in";
			loadingScreensSticky.className += " modal-open";

			setTimeout(function(){ document.postPaymentForm.submit(); }, 500);

				} else {
					var errorMsg = result.split(':');
						jQuery('.alert').html('<i class="caret warning"></i>'+errorMsg[1]);
						document.getElementById("commonError").style.display = "block";
						jQuery('#commonError').removeClass('hide');
						jQuery(".ctnbutton").removeClass("disable");
						//document.getElementById('commonError').innerHTML = "<p><strong>"
						//		+ errorMsg[1] + "</p></strong>";
						//document.getElementById('commonError').className = "commonErrorSummary info-section clear padding10px mb20px";
						clearCardEntryElements();
				}
			} else {
				window.location.href = 	result;

			}
		} else {
			document.getElementById('errorMsg').innerHTML = "<h2>Payment failed. Please try again.</h2>";
			jQuery(".ctnbutton").removeClass("disable");
		}
	}
	window.scrollTo(0, 0);
}

/**
 ** This method submits the bank form present in the overlay and fills the overlay in the iframe.
**/
function bankRedirect() {
	document.bankform.target = "ACSframe";
	document.bankform.submit();
}
function clearCardEntryElements(){
	jQuery("#cardType").val("");
	jQuery("#cardTypespan").html("Select type of credit card");
	jQuery("#cardNumber").val("");
	jQuery("#cardNumberDiv").removeClass("valid");
	jQuery("#cardName").val("");
	jQuery("#cardNameDiv").removeClass("valid");
	jQuery("#expiryDateMM").val("");
	jQuery("#monthspan").html("MM");
	jQuery("#expiryDateYY").val("");
	jQuery("#yearspan").html("YY");
	jQuery("#securityCode").val("");
	jQuery("#securityCodeDiv").removeClass("valid");
	jQuery("#creditPaymentTypeCode").hide();
	jQuery("#debitPaymentTypeCode").hide();
	jQuery("#issueNumber").val("");
    jQuery("#issue").hide();
	document.getElementById("card-img").className = "";
	document.getElementById("card-desc").innerHTML = "";


}
	</script>
</head>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag"%>
<version-tag:version />
</head>

<%
	String applyCreditCardSurcharge = com.tui.uk.config.ConfReader
			.getConfEntry(clientApp + ".applyCreditCardSurcharge", null);
	pageContext.setAttribute("applyCreditCardSurcharge", applyCreditCardSurcharge, PageContext.REQUEST_SCOPE);
%>
<script>
applyCreditCardSurcharge = "<%=applyCreditCardSurcharge%>";
</script>



<body onload="setDefaultDepositOption();">

	<%@include file="tealium.jspf"%>

	<form id="CheckoutPaymentDetailsForm" name="paymentdetails"
		method="post" action="javascript:makePayment();">
		<%@include file="flightsOnlyConfigSettings.jspf"%>

		<div class="structure">

			<div id="page" class="payment">

				<!-- Include for HEADER > Thomson -->
				<jsp:include
					page="sprocket/fae_header_${fn:toLowerCase(clientapp)}.jsp" />

				<div class="mboxDefault"></div>
				<script type="text/javascript">
				mboxCreate("${clientapp}_PromoStrip");
			</script>

				<div id="content" class="book-flow responsive">
					<div class="content-width">
						<div id="main">
							<%@include file="topError.jspf"%>

							<div class="component-wrap">
								<c:if test="${not empty alertMessage}">
									<div class="price-header-section">

										<div class="price-inc-dec-info">
											<c:choose>
												<c:when
													test="${bookingComponent.nonPaymentData['alertLevel'] == 'decPrice'}">

													<h2
														class="icon-v2 icon-TUI_ArrowDown dark-blue standard-layout">
														<span class="tui-font fl marg-left-20"><c:out
																value="${bookingComponent.nonPaymentData['messageHeader']}"
																escapeXml="false" /></span>
													</h2>
												</c:when>
												<c:otherwise>
													<h2
														class="icon-v2 icon-TUI_ArrowUp dark-blue standard-layout">
														<span class="tui-font fl marg-left-20"><c:out
																value="${bookingComponent.nonPaymentData['messageHeader']}"
																escapeXml="false" /></span>
													</h2>
												</c:otherwise>
											</c:choose>
											<p class="grey-med">
												<c:out
													value="${bookingComponent.nonPaymentData['alertMessage']}"
													escapeXml="false" />
											</p>
										</div>
									</div>
								</c:if>

								<div class="header-section">
									<h1
										class="current-state payment-heading standard-layout marg-top-20"><%=headerText%></h1>
									<div class="time-info">
										<h2 class="icon-v2 icon-info dark-blue standard-layout">
											<span class="tui-font fl marg-left-20"> PLEASE
												COMPLETE IN 30 MINUTES</span>
										</h2>

									</div>

									<span class="grey-med sub-time-text standard-layout">After
										30 minutes this page will time out and you will have to
										re-enter all your details.</span>

								</div>

								<div id="customer-form" class="full-width">
									<form name="paymentdetails" method="post"
										id="CheckoutPaymentDetailsForm"
										action="javascript:makePayment();" novalidate>

										<%@include file="faeHolidayDetails.jspf"%>
										<%@include file="faeDepositSection.jspf"%>
                                        <%@include file="faeHccCardDetails.jspf"%>
										
										<div class="ctaContent marg-bottom-10">

											<c:choose>
												<c:when test="${hccsmap.hccSwitch == 'true'}">
													<!-- Card Details -->
													<div class="paydetails">
												</c:when>
												<c:otherwise>
													<div class="paydetails fl">
												</c:otherwise>
											</c:choose>

											<!--Start - About to pay section-->
											<div class="paypalcreditbutton">
												<div class="disNone" id="creditCardCharges"
													onclick="displayCreditCharges(this.id)">
													<p class="size-18">
														<span class="final-amount-text">You're about to
															pay&nbsp;</span><span class="creditCardChargeFinalAmt currency"><c:choose>
																<c:when
																	test="${clientapp == 'TUIFALCON' || clientapp == 'TUIFALCONFO'}">&euro;</c:when>
																<c:otherwise>&pound;</c:otherwise>
															</c:choose></span><span class="creditCardChargeFinalAmt amount"
															id="creditCardChargeFinalAmt"><c:out
																value="${fullAmount}" /></span> by Credit Card
														<c:if
															test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}">
															<span class="lockPriceText">&nbsp;to Lock Your
																Price</span>
														</c:if>
													</p>
													<c:if test="${applyCreditCardSurcharge eq 'true'}">
														<p class="grey-med">
															Includes
															<fmt:formatNumber value="${cardCharge}" type="number"
																maxFractionDigits="2" minFractionDigits="0"
																pattern="#####.##" />
															% Credit Card Surcharge
														</p>
													</c:if>
												</div>
												<div id="debitCardCharges"
													onclick="displayCreditCharges(this.id)">
													<c:choose>
														<c:when test="${clientapp == 'ANCFALCON'}">
															<c:set value="&euro;" var="currency" scope="page" />
															<p class="size-18debitCard">
																<span class="final-amount-text">You're about to
																	pay&nbsp;</span> <span
																	class="debitCardChargeFinalAmt currency"
																	id="debitCardCurrency">${currency}</span> <span
																	class="creditCardChargeFinalAmt amount"
																	id="debitCardFinalAmt"><c:out value="${paySummaryLeftToPaySplit[0]}"/>.<c:out value="${paySummaryLeftToPay_Dec}"/></span> <%-- <span id="debitCardText"
																	class="hide">&nbsp; by Debit Card</span>
																<c:if
																	test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}">
																	<span class="lockPriceText">&nbsp;to Lock Your
																		Price</span>
																</c:if> --%>
																<br> <span class="spanMove">and check in for your flight</span>
															</p>
														</c:when>

														<c:otherwise>
															<c:set value="&pound;" var="currency" scope="page" />
															<p class="size-18debitCard">
																<span class="final-amount-text">You're about to
																	pay&nbsp;</span> <span
																	class="debitCardChargeFinalAmt currency"
																	id="debitCardCurrency">${currency}</span> <span
																	class="creditCardChargeFinalAmt amount"
																	id="debitCardFinalAmt">
																	<c:out value="${paySummaryLeftToPaySplit[0]}"/>.<c:out value="${paySummaryLeftToPay_Dec}"/></span>
																<%-- <span id="debitCardText"
																	class="hide"></span>
																<c:if
																	test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}">
																	<span class="lockPriceText">&nbsp;to Lock Your
																		Price</span>
																</c:if> --%>
																<br> <span class="spanMove">and check in for your flight</span>
															</p>
														</c:otherwise>
													</c:choose>
												</div>

												<div class="disNone" id="thomsonCardCharges"
													style="display: none">
													<p class="size-18">
														<span>You're about to pay&nbsp;</span><span
															class="creditCardChargeFinalAmt currency">${currency}</span>
														<span class="creditCardChargeFinalAmt amount"
															id="thomsonCardFinalAmt"></span><span class="hide">&nbsp;
															by TUI Credit Card</span>
														<c:if
															test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}">
															<span class="lockPriceText">&nbsp;to Lock Your
																Price</span>
														</c:if>
													</p>
												</div>
												<div class="disNone" id="giftCardCharges"
													onclick="displayCreditCharges(this.id)">
													<p class="size-18">
														<span class="final-amount-text">You're about to
															pay&nbsp;</span><span class="giftCardFinalAmt currency">${currency}</span>
														<span class="creditCardChargeFinalAmt amount"
															id="giftCardFinalAmt"><c:out value="${fullAmount}" /></span>
														<span class="hide">by Gift Card</span>
														<c:if
															test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}">
															<span class="lockPriceText">&nbsp;to Lock Your
																Price</span>
														</c:if>
													</p>
												</div>
												<div class="disNone" id="cardPaymentCharges"
													onclick="displayCardChargesPaypal(this.id)">
													<p class="size-18">
														<span class="final-amount-text">You're about to
															pay&nbsp;</span> <span
															class="cardPaymentChargeFinalAmt currency">${currency}</span>
														<span class="creditCardChargeFinalAmt amount"
															id="cardPaymentFinalAmt"></span> <span class="hide">&nbsp;
															by Credit Card</span>
														<c:if
															test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}">
															<span class="lockPriceText">&nbsp;to Lock Your
																Price</span>
														</c:if>
													</p>
													<p>No credit card fees.</p>
												</div>
												<div class="disNone" id="payPalCharges"
													onclick="displayCardChargesPaypal(this.id)">
													<p class="size-18">
														<span class="final-amount-text">You're about to
															pay&nbsp;</span><span class="payPalFinalAmt currency"
															id="paypalCurrency">${currency}</span> <span
															class="creditCardChargeFinalAmt amount"
															id="payPalFinalAmt"><c:out value="${paySummaryLeftToPaySplit[0]}"/>.<c:out value="${paySummaryLeftToPay_Dec}"/></span> <br> <span>and
															check in for your flight</span>
														<c:if
															test="${bookingInfo.bookingComponent.lockYourPriceSummary.selected eq 'true' && bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne 'true'}">
															<span class="lockPriceText">&nbsp;to Lock Your
																Price</span>
														</c:if>
													</p>
												</div>

											</div>
											<div class="displayCentre">
												<p class="size-18 payPalCreditDisplay hide">You're about
													to pay by PayPal Credit</p>
											</div>
											<!--End - About to pay section-->

										</div>
								</div>
	</form>
	</div>
	</div>
	<!-- Sidebar -->
	</div>
	</div>
	</div>

	<!-- Include for Footer > TUICOMMON -->
	<jsp:include page="sprocket/fae_footer.jsp" />

	<div class="page-mask"></div>
	</div>
	</div>
	<div id="video-container" class="hide"></div>
	<fmt:formatNumber
		value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}"
		var="totalCostingLine" type="number" pattern="####"
		maxFractionDigits="2" minFractionDigits="2" />
	<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page" />
	<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
	<div class="tooltip top center tp" id="tooltipTmpl">
		<p></p>
		<span class="card-back"></span> <span class="arrow"></span>
	</div>
	<!-- <script type="text/javascript" src="/cms-cps/tuicommon/js/vs.js"></script> -->

	</form>

	<div id="overlay" class="posFix"></div>
	<form novalidate name="postPaymentForm" method="post">
		<input type="hidden" name="emptyForm" class="disNone"></input>
	</form>
	<c:choose>
		<c:when
			test="${(clientapp == 'TUITH' || clientapp == 'TUIFC' || clientapp == 'TUIFALCON')}">
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,freeChildPlaceCount,ensightenUrl,bootstrapUrl,TUIHeaderSwitch,tuiAnalyticsJson,alertLevel,alertMessage,csrfToken,isResponsiveHubAndSpoke,messageHeader,bootstrapUrl_old,numberallowedincityfield,display_name,extra_detail_1,extra_detail_2,intro</c:set>
		</c:when>
		<c:when
			test="${(clientapp == 'THOMSONMOBILE' || clientapp == 'FIRSTCHOISEMOBILE')}">
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,freeChildPlaceCount,ensightenUrl,bootstrapUrl,TUIHeaderSwitch,tuiAnalyticsJson,csrfToken,display_name,extra_detail_1,extra_detail_2,intro</c:set>
		</c:when>
		<c:when
			test="${(clientapp == 'TUITHFO' || clientapp == 'TUIFALCONFO')}">
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,freeChildPlaceCount,ensightenUrl,bootstrapUrl,TUIHeaderSwitch,csrfToken,showATOLForExternalCarrier,paxCount,display_name,extra_detail_1,extra_detail_2,intro</c:set>
		</c:when>
		<c:when
			test="${(clientapp == 'TUITHCRUISE' || clientapp == 'TUITHRIVERCRUISE' || clientapp == 'TUICRUISEDEALS' || clientapp == 'TUIHMCRUISE')}">
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,bootstrapUrl_old,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,TUIHeaderSwitch,bootstrapUrl,chkThirdPartyMarketingAllowed,chkTuiMarketingAllowed,communicateByEmail,communicateByPhone,communicateByPost,communicateBySMS,csrfToken,ensightenUrl,online_discount,tuiAnalyticsJson,display_name,extra_detail_1,extra_detail_2,intro</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,freeChildPlaceCount,ensightenUrl,bootstrapUrl,TUIHeaderSwitch,tuiAnalyticsJson,display_name,extra_detail_1,extra_detail_2,intro</c:set>
		</c:otherwise>
	</c:choose>
	<script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
				 tui.analytics.page.pageUid = "responsivePaymentDetailsPage";
				 				
				<c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
					<c:set var="isAnalyticsParam" value="true"/>
					<c:forEach var="nonWebAnalytics" items="${nonWebAnalyticsData}">
						<c:if test="${nonWebAnalytics eq analyticsDataEntry.key}">
							<c:set var="isAnalyticsParam" value="false"/>
						</c:if>
					</c:forEach>
					<c:if test="${isAnalyticsParam eq 'true'}">
						<c:choose>
							<c:when test="${analyticsDataEntry.key == 'tuiAnalyticsJson' && (clientapp == 'TUITHFO' || clientapp == 'TUIFALCONFO')}"></c:when>
							<c:otherwise>
									tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
							</c:otherwise>
						</c:choose>
				    </c:if>
				</c:forEach>
				
				
				
			</script>

	<script>

			window.dataLayer = window.dataLayer || [];
			window.dataLayer.push(${bookingInfo.bookingComponent.nonPaymentData['tuiAnalyticsJson']});

			</script>




</body>
</html>
