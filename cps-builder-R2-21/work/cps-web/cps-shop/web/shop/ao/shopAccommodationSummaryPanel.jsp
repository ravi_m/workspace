<%--
 The Summary Panel JSP.
--%>
<div id="container_summarypanel_main_container">
   <div id="container_summarypanel_main">

      <%@include file="shopAccommodationPaymentPanel.jsp" %> <br clear="all"/>

      <c:if test="${bookingComponent.flightSummary != null&&bookingComponent.flightSummary.flightSelected=='false'}">
        <%@include file="shopAccommodationFlightPanel.jsp" %>
      </c:if>

      <%@include file="shopAccommodationPanel.jsp" %>
   </div>
</div>