<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
<%
    String creditCardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("WiSHAO.GBP.cardChargeDetails" , "");
    pageContext.setAttribute("creditCardChargeDetails", creditCardChargeDetails, PageContext.REQUEST_SCOPE);
%>
</c:if>
<c:set var="cardChargeDetails" value="${creditCardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<fmt:formatNumber value="${cardChargeArray[0]}" type="number" var="cardCharge" maxFractionDigits="1" minFractionDigits="0"/>

<div id="payment_amount">
   <h2>Payment Amount</h2>
   <p>
      <input type="hidden" id="totalHolidayCostH" name="totalHolidayCostH" value='<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>'/>
      Your total holiday cost is <strong>&pound;<span class="totalholidaycost" id="totalholidaycost"><fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/></span >*</strong>
      <br/>
      The deposit below is due today, but you can choose to pay the full balance if you wish. Please note, the deposit amount includes any insurance options you have selected.<br clear="all"/>Please select your preferred payment amount:
   </p>
   <div id="depositSection" class="container_depositcontainer depositSection">
      <c:set var='checkFlag' value='checked=checked'/>
      <c:set var="totalAmountDue" value="0.0"/>
      <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}">
         <c:if test="${depositComponent.depositType == 'LOWDEPOSIT' || depositComponent.depositType == 'lowdeposit'}">
            <div class="radiolabel">
               <p>
                  <input  name="lowdepositValue" id ="lowdepositValue" value="<c:out value='${depositComponent.depositAmount.amount}'/>" type="hidden"/>
                  <label>
                     <input type="hidden" id="lowDepositH" value="<fmt:formatNumber value="${depositComponent.depositAmount.amount}" type="number" maxFractionDigits="2"/>"/>
                     <strong>Low Deposit: &pound;<span id="lowdeposit" class="depositAmount LOWDEPOSIT"><fmt:formatNumber value="${depositComponent.depositAmount.amount}" type="number" maxFractionDigits="2"/></span >*</strong>
                     <br/>The outstanding balance of your deposit is <strong>&pound;<span id="lowoutstandingbalance"><fmt:formatNumber value="${depositComponent.outstandingBalance.amount}" type="number" maxFractionDigits="2"/></span>**</strong> and is due on <strong>
                     <span id="lowBalanceDueDate" >
                        <fmt:formatDate value="${depositComponent.depositDueDate}" pattern="dd/MM/yy"/></strong>
                     </span>
                  </label>
               </p>
               <div class="marginLeft500 marginTop0">
                  <input type="radio" class="deposit" name="depositType" id="lowDepositR" <c:out value="${checkFlag}"/> value="<c:out value='${depositComponent.depositType}'/>" onclick="DepositTypeChangeHandler.handle(this,this.value);"/>
               </div>
               <hr/>
            </div>
            <c:set var="totalAmountDue" value="${depositComponent.depositAmount.amount}"/>
            <c:set var='checkFlag' value=''/>
         </c:if>
         <c:if test="${depositComponent.depositType == 'DEPOSIT' || depositComponent.depositType == 'deposit'}">
            <div class="radiolabel">
               <p>
                  <input  name="paymentPage" id ="paymentPage" value="true" type="hidden"/>
                  <input  name="depositValue" id ="depositValue" value="<c:out value='${depositComponent.depositAmount.amount}'/>" type="hidden"/>
                  <label>
                     <input type="hidden" id="depositH" value="<fmt:formatNumber value="${depositComponent.depositAmount.amount}"  type="number" maxFractionDigits="2"/>"/>
                     <strong>Deposit: &pound;<span id="deposit" class="depositAmount DEPOSIT"><fmt:formatNumber value="${depositComponent.depositAmount.amount}" type="number" maxFractionDigits="2"/></span>*</strong><br/>
                     The outstanding balance of your deposit is
                     <strong>&pound;<span id="outstandingbalance"><fmt:formatNumber value="${depositComponent.outstandingBalance.amount}" type="number" maxFractionDigits="2"/></span>**</strong> and is due on <strong>
                     <span id="balanceDueDate">
                        <fmt:formatDate value="${depositComponent.depositDueDate}" pattern="dd/MM/yy"/>
                     </span></strong>
                  </label>
               </p>
               <div class="marginLeft500">
                  <input  type="radio" class="deposit" name="depositType" id="depositR" <c:out value='${checkFlag}'/> value="<c:out value='${depositComponent.depositType}'/>" onclick="DepositTypeChangeHandler.handle(this,this.value);"/>
               </div>
               <hr/>
               <c:if test="${totalAmountDue == '0.0'}">
                  <c:set var="totalAmountDue" value="${depositComponent.depositAmount.amount}"/>
               </c:if>
            </div>
         </c:if>
      </c:forEach>
      <div class="radiolabel">
         <p class="radio">
            <input type="hidden" id="fullCostH" value="<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2"/>"/>
            <strong>Full Balance: &pound;<span id="fullCost" class="depositAmount FULLCOST"><fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2"/></span>*
            </strong>
         </p>
         <div id="fullCostDiv"><input type="radio" class="deposit" name="depositType" id="fullDeposit" value="FULLCOST" onclick="DepositTypeChangeHandler.handle(this,this.value);"/></div>
      </div>
   </div>
   <p>* If you pay by credit card the charges will be updated in the price panel once you have selected your method of payment.</p>
   <p>** There are no additional charges when paying by Maestro ,MasterCard Debit or Visa/Delta debit cards.A fee of <c:out value='${cardCharge}'/>% applies to credit card payments,which is capped at �95.00 per transaction when using American Express,MasterCard Credit or Visa Credit Cards</p>
     </p>
</div>