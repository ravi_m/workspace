<div class="fh4_payment_head_tac">
	&nbsp;Terms and Conditions
</div>
<br clear="all"/>
<%-------------------------------------------------------------------------------------------------------%>
<div class="container_termsandcontainer">
	<p>
		Please read our
		<a href="javascript:Popup('<c:out value='${url}'/>/thomson/page/shop/common/insurance.page?insuranceName=privacypolicy',600,500,'scrollbars=yes');" >Privacy Policy </a>
		and <a href="javascript:showHiddenInformation('dataprotectionnoticebox')" >Data Protection Notice</a>
		and confirm you agree to our use of your information provided above (which may in special situations include sensitive personal data) by ticking the box below.
	</p>
</div>
<%------------------------------------------------------------------------------------------------------%>

<br clear="all"/>
<div class="container_termsandcontainer" id="dataprotectionnoticebox" style="display:none;">
	<p><strong>Data Protection Notice:</strong><br/> We may from time to time contact you by post with further information on the latest offers, brochures, products or services which we believe may be of interest to you, from Thomson Holidays (a division of TUI UK Retail Limited), other holiday divisions within and group companies of TUI UK Retail Limited.</p>
	<span class="termsandcontainer_checkbox1">
		<input  id="contactByEmail" name="contactByEmail" type="checkbox" value="true" /><c:out value="${tuiContactText1}" escapeXml="false"/>
	</span>
	<span class="termsandcontainer_text2">
		If you would not like to receive <b>e-communications</b> including information on discounts from <a href="https://www.thomson.co.uk" target="_blank">thomson.co.uk</a>, please <u>tick</u> this box.
	</span>
	<span class="termsandcontainer_checkbox1">
		<input  id="contactByPost" name="contactByPost" type="checkbox" value="true" />
	</span>
	<span class="termsandcontainer_text2">
		Our business partners and carefully selected companies outside our holiday group would like to send you information about their products and services <strong>by post</strong>. If you <a>would not</a> like to hear from them, please tick this box.
	</span>
</div>

<%---------------------------------------------------------------------------------------------------------%>
<div class="container_termsandcontainer2">
  <c:choose>
    <c:when test="${bookingComponent.flightSummary !=null}">
      <c:set var="icoPayment_ForeignOfficeType" value="FUPayment_ForeignOffice"/>
      <c:set var="icoPayment_ForeignOfficeType" value="FUPayment_PassportOffice"/>
    </c:when>
    <c:otherwise>
      <c:set var="icoPayment_ForeignOfficeType" value="aoPayment_ForeignOffice"/>
      <c:set var="icoPayment_ForeignOfficeType" value="AOPayment_PassportOffice"/>
    </c:otherwise>
  </c:choose>

  <p>
	 Please note that all members of your party require valid passports and any applicable visas before travelling.
	 Visit the Foreign Office website at
	 <a href="http://www.fco.gov.uk/travel?ico=<c:out value="icoPayment_ForeignOfficeType" escapeXml="false"/>" target="_blank">http://www.fco.gov.uk/travel</a>

	 to see visa and travel advice, or the Passport Office website at
	 <a href="https://www.passport.gov.uk?ico=<c:out value="icoPayment_PassportOfficeType" escapeXml="false"/>" target="_blank">https://www.passport.gov.uk</a>
	 for passport information.
  </p>

  <%-- Start of to view text--%>
  <p>
	 To view the notice summarising the liability rules applied by Community air carriers as required by Community legislation and the Montreal Convention, please view the
	 <a href="javascript:Popup('<c:out value='${url}'/>/thomson/page/shop/common/insurance.page?insuranceName=airpassengernotice',600,500,'scrollbars=yes');" >Air Passenger Notice</a>
  </p>
	<%-- End of to view text --%>
</div>
<%--------------------------------------------------------------------------------------------------------%>

<%------------------------------------------------------------------------------------------------------%>
<div class="container_termsandcontainer">
	<span class="termsandcontainer_checkbox1">
		<input name="bookingConditionsChecked" type="checkbox" value="true"  alt="You must read and accept our Terms and Conditions before confirming your booking|Y|CHECK"/>
	</span>
	<span class="termsandcontainer_text_accept">
		I confirm that :<br />
		(a) as the lead name/passenger, I am at least 18 years old.<br />
		(b) I have contacted the relevant travel supplier(s) regarding any special needs before booking and <br />
		(c) I have read and accept all of the following:
	</span>
	<br clear="all"/>

	<c:set var="departureDate">
		<fmt:formatDate value="${bookingComponent.accommodationSummary.startDate}" pattern="yyyy-MM-dd"/>
	</c:set>

	<ul class="termsandcontainer_text2_points">
		<li><a href="javascript:void(0)" onclick="popUpBookingConditions('<c:out value="${accommodationInventorySystem}"/>','<c:out value="${flightInventorySystem}"/>','<c:out value="${departureDate}"/>','<c:out value="${bookingComponent.clientDomainURL}"/>')">Booking Conditions</a></li>
		<li><a href="javascript:Popup('<c:out value='${url}'/>/thomson/page/shop/common/insurance.page?insuranceName=privacypolicy',600,500,'scrollbars=yes');" >Privacy Policy</a>&nbsp;and use of my information</li>
	</ul>
</div>
<%---------------------- -----------------------------------------%>

<%---------------------- -----------------------------------------%>
<div class="container_termsandcontainer_print">
	<p>
		<input id="printLocally" name="printLocally" type="radio" onclick="javascript:setFlag('printLocal','true')"  value="<c:out value='${personalDetailsAndPaymentFormBean.printLocally}'/>" alt="Print locally" checked />Print in Shop
	</p>
	<p>
		<input id="printCentrally" name="printCentrally" type="radio" onclick="javascript:setFlag('printCentral','true')"  value="<c:out value='${personalDetailsAndPaymentFormBean.printCentrally}'/>" alt="Print centrally" />Print centrally
	</p>
</div>
<%---------------------- -----------------------------------------%>