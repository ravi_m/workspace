<div class="container_summarypanel_head hide_in_print">Accommodation</div>

<div class="container_accommodation hide_in_print">

  <span class="accommodation_title1"><c:out value="${bookingComponent.accommodationSummary.accommodation.hotel}"/></span>

  <%--------------------------------Rating -----------------------------------------------------------%>
  <span class="accomodation_value alignaccomrating">
	 <c:set var="rating" value="${bookingComponent.accommodationSummary.accommodation.rating}"/>
    <c:if test="${not empty rating}">
	   <img src="/cms-cps/shop/ao/images/<c:out value='${rating}'/>.gif"/>
    </c:if>
  </span>
  <%--------------------------------End Rating -----------------------------------------------------------%>

  <%--------------------------------Destination, resort,Check-in and Check-out---------------------------%>
  <span class="accommodation_title2">Destination&#58;</span>
  <span class="accommodation_value"><c:out value="${bookingComponent.accommodationSummary.accommodation.destination}"/></span>

  <span class="accommodation_title2">Resort&#58;</span>
  <span class="accommodation_value"><c:out value="${bookingComponent.accommodationSummary.accommodation.resort}"/></span>

  <span class="accommodation_title2">Check-in : </span>
  <span class="accommodation_value"><fmt:formatDate value="${bookingComponent.accommodationSummary.startDate}" pattern="dd/MM/yyyy"/></span>

  <span class="accommodation_title2">Check-out:</span>
  <span class="accommodation_value"><fmt:formatDate value="${bookingComponent.accommodationSummary.endDate}" pattern="dd/MM/yyyy"/></span>
  <%--------------------------------Destination, resort,Check-in and Check-out -----------------------------------------------------------%>

  <%--------------------------------Duration---------------------------%>
  <span class="accommodation_title2">Duration&#58; </span>
  <c:choose>
    <c:when test="${bookingComponent.accommodationSummary.duration =='1'}">
		<span class="accommodation_value"><c:out value="${bookingComponent.accommodationSummary.duration}"/> night</span>
		<br clear="all"/>
	 </c:when>
	 <c:otherwise>
		<span class="accommodation_value"><c:out value="${bookingComponent.accommodationSummary.duration}"/> nights</span><br clear="all"/>
	 </c:otherwise>
  </c:choose>
<%--------------------------------Duration---------------------------%>

<%--------------------------------Board Basis and Rooms selected---------------------------%>
  <span class="accommodation_title2">Board Basis&#58;</span>
  <span class="accommodation_value" id='bb'><c:out value="${bookingComponent.accommodationSummary.accommodation.boardBasis}"/></span>

  <span class="accommodation_title2">Rooms selected&#58;</span>
  <span class="accommodation_value" id='roomsSelected'><c:out value="${bookingComponent.accommodationSummary.accommodation.roomDescription}"/></span>
<%--------------------------------Board Basis and Rooms selected---------------------------%>

</div> <%------- END container_accommodation hide_in_print --%>

<%--------------------------------The View accommodation details Link ---------------------------%>
<div class="container_accommodation hide_in_print">
  <span class="accommodation_viewaccommodation">
	 <a href="javascript:Popup('<c:out value="${bookingComponent.clientDomainURL}${bookingComponent.accommodationSummary.accommodationDetailsUri}"/>',755,584,'scrollbars=yes');">View accommodation details</a>
  </span>
</div>
<%--------------------------------The View accommodation details Link---------------------------%>

