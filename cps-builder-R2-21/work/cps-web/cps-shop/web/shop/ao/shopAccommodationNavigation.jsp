<div class="container_backpaybutton">
   <span class="backbutton">
	   <a href="<c:out value='${url}'/>/thomson/page/shop/ao/booking/prepayment.page">
			<img src="/cms-cps/shop/ao/images/img_dp_button_back.gif" alt="Back" width="55" height="24" hspace="0" vspace="0" border="0" title="Back" />
		</a>
	</span>
	<c:if test="${not empty bookingComponent.paymentType and bookingInfo.newHoliday}">
		<span class="paybutton" id="confirmButton1">
			<a href="javascript:void(0);" onclick="return FormHandler.handleSubmission(document.forms[0])"><img src="/cms-cps/shop/common/images/btn_confirm_booking.gif" border="0" id="confirmbooking"/></a>
		</span>
	</c:if>
</div>
