<%@include file="/common/commonTagLibs.jspf"%>
<div class="width_add <c:if test ='${empty bookingComponent.depositComponents && empty bookingComponent.termsAndCondition.relativeTAndCUrl}'>heightAdjust</c:if>">
    <span class="backlink">
        <c:set var="url" value='${bookingComponent.clientDomainURL}'/>
            <a href="<c:out value='${url}'/>/thomson/page/shop/byo/booking/prepayment.page">
                <img src="/cms-cps/shop/common/images/img_dp_button_back.gif" border="0" id="back"/>Back
            </a>
    </span>
    <c:if test="${not empty bookingComponent.paymentType and bookingInfo.newHoliday}">
        <span id="confirmButton">
            <a href="javascript:void(0);" onclick="return FormHandler.handleSubmission(document.forms[0])"><img src="/cms-cps/shop/common/images/btn_confirm_booking.gif" border="0" id="confirmbooking"/></a>
        </span>
    </c:if>
</div>
<br clear="all" />