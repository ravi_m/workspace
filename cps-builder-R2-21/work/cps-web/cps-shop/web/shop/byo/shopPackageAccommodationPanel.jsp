<%@include file="/common/commonTagLibs.jspf"%>
<h3>Accommodation</h3>
<div <c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'false'}">class="inactive" </c:if> >

	<h4><c:out value="${bookingComponent.accommodationSummary.accommodation.hotel}"/>

	<c:set var="isPlus" value="false"/>
	<c:if test="${bookingComponent.accommodationSummary.accommodation.rating == null}">
				<c:set var="isPlus" value="true"/>
	</c:if>


	<c:set var="trating" value="${bookingComponent.accommodationSummary.accommodation.rating}"/>
	<c:set var="starrating" value="${bookingComponent.accommodationSummary.accommodation.rating}"/>
	<c:choose>
		<c:when test="${bookingComponent.companyCode == 'TH'}">
			<c:choose>
				<c:when test="${isPlus}">
					<c:choose>
						<c:when test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
							<img src="/cms-cps/shop/byo/images/<c:out value='${starrating}'/>.gif" border="0"/>
						</c:when>
						<c:otherwise>
							<img src="/cms-cps/shop/byo/images/<c:out value='${trating}'/>_plus.gif" border="0"/>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<img src="/cms-cps/shop/byo/images/<c:out value='${trating}'/>.gif" border="0"/>
				</c:otherwise>
			</c:choose>
		</c:when>

		<c:when test="${bookingComponent.companyCode == 'FC'}">
			<c:choose>
				<c:when test="${isPlus}">
					<img src="/cms-cps/shop/byo/images/<c:out value='${trating}'/>_sun_small_plus.gif" border="0"/>
				</c:when>
				<c:when test="${!isPlus and starrating !='fcrating0'}">
					<img src="/cms-cps/shop/byo/images/<c:out value='${starrating}'/>_sun_small.gif" border="0"/>
				</c:when>
			</c:choose>
		</c:when>

		<c:otherwise>
			<c:choose>
				<c:when test="${isPlus}">
					<c:choose>
						<c:when test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
							<img src="/cms-cps/shop/byo/images/<c:out value='${starrating}'/>.gif" border="0"/>
						</c:when>
						<c:otherwise>
							<img src="/cms-cps/shop/byo/images/<c:out value='${trating}'/>_plus.gif" border="0"/>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<c:if test="${!isPlus and trating !='fnrating0'}">
						<img src="/cms-cps/shop/byo/images/<c:out value='${trating}'/>.gif" border="0"/>
					</c:if>
				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>

	</h4>

	<dl class="accom_details">

		<dt>Country&#58;</dt>
		<dd><c:out value="${bookingComponent.accommodationSummary.accommodation.country}"/></dd>

		<dt>Destination&#58;</dt>
		<dd><c:out value="${bookingComponent.accommodationSummary.accommodation.destination}"/></dd>

		<dt>Resort&#58;</dt>
		<dd><c:out value="${bookingComponent.accommodationSummary.accommodation.resort}"/></dd>

		<dt>Duration&#58;</dt>
		<c:set var="durationUnit" value=" nights"/>
		<c:if test="${bookingComponent.accommodationSummary.duration =='1'}">
				<c:set var="durationUnit" value=" night"/>
		</c:if>
   	<dd><c:out value="${bookingComponent.accommodationSummary.duration}${durationUnit}"/></dd>

		<dt>Board Basis&#58;</dt>
		<dd><c:out value="${bookingComponent.accommodationSummary.accommodation.boardBasis}"/></dd>

	</dl>


	<br clear="all" />
	<a href="javascript:Popup('<c:out value="${bookingComponent.clientDomainURL}/thomson/page/shop/byo/details/detailspopup.page?packageNumber=${bookingComponent.accommodationSummary.accommodationDetailsUri}"/>',755,584,'scrollbars=yes');">
		<c:out value="${bookingComponent.accommodationSummary.caption}"/>
	</a>
</div>
