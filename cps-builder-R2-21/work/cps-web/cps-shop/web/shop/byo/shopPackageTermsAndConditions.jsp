<%--

--%>
<div id="t_and_c" class="bordertc">
  <h2>Terms and Conditions</h2>
  
  <%-------------------------------------------------------------------------------------------------------%>  
  <div class="highlight">
    <p>
      Please read our <a href="javascript:void(0);" onclick="Popup('<c:out value="${bookingComponent.clientDomainURL}"/>/thomson/page/shop/common/insurance.page?insuranceName=privacypolicy',600,500,'scrollbars=yes,resizable=yes');" >Privacy Policy</a>
      and <a href="javascript:void(0);" onclick="javascript:showHiddenInformation('dataprotectionnoticediv')" >Data Protection Notice</a>
      and confirm you agree to our use of your information provided above (which may in special situations include sensitive personal data) by ticking the box below.
    </p>
  </div>
  <%-------------------------------------------------------------------------------------------------------%>  
  
  <%-------------------------------------------------------------------------------------------------------%>  
  <div id="dataprotectionnoticediv" class="highlight highlight_protectionnotice" style="display:none;">
    <p id="dataprotectionnoticebox">
      <strong>Data Protection Notice:</strong><br/>We may from time to time contact you by post with further information on the latest offers, brochures, products or services which we believe may be of interest to you, from Thomson Holidays (a division of TUI UK Retail Limited), other holiday divisions within and group companies of TUI UK Retail Limited.</p>
      <p><input  id="contactByEmail" name="contactByEmail" type="checkbox" value="true" />If you would not like to receive <b>e-communications</b> including information on discounts from <a href="https://www.thomson.co.uk" target="_blank">thomson.co.uk</a>, please <u>tick</u> this box.</p>
      <p><input id="contactByPost" name="contactByPost" type="checkbox" value="true" /><label for="contactByPost">Our business partners and carefully selected companies outside our holiday group would like to send you information about their products and services <strong>by post</strong>. If you <strong>would not</strong> like to hear from them, please tick this box.</label>
    </p>
  </div><br/>
  <%-------------------------------------------------------------------------------------------------------%>
  
  <%-------------------------------------------------------------------------------------------------------%>  
  <div class="highlight">
    <p>Please note that all members of your party require valid passports and any applicable visas before travelling. Visit the Foreign Office website at <a href="http://www.fco.gov.uk/travel" target="_blank">http://www.fco.gov.uk/travel</a> to see visa and travel advice, or the Passport Office website at <a href="https://www.passport.gov.uk" target="_blank">https://www.passport.gov.uk</a> for passport information.</p>
    <br/>
    <p>To view the notice summarising the liability rules applied by Community aircarriers as required by Community legislation and the Montreal Convention, please view the <a href="javascript:void(0);" onclick="Popup('<c:out value="${bookingComponent.clientDomainURL}"/>/thomson/page/shop/common/insurance.page?insuranceName=airpassengernotice',600,500,'scrollbars=yes,resizable');">Air Passenger Notice.</a></p>
  </div>
  <%-------------------------------------------------------------------------------------------------------%>
  
  <%-------------------------------------------------------------------------------------------------------%>  
  <div class="highlight termscheck_height">
    <input name="termsAndConditionsCheckbox" type="checkbox" alt="You must read and accept our Terms and Conditions before confirming your booking.|Y|CHECK" />
    <div class="width_add">
      <ul class="termsAndConditions">
        <li>
          I confirm that :<br />
          (a) as the lead name/passenger, I am at least 18 years old.<br />
          (b) I have contacted the relevant travel supplier(s) regarding any special needs before booking and <br />
          (c) I have read and accept all of the following:
        </li>
      </ul>
      <ul class="bookingconds">
        <li><a href="javascript:void(0)" onclick="popUpBookingConditions()">Booking Conditions</a></li>
        <li><a href="javascript:void(0);" onclick="Popup('<c:out value="${bookingComponent.clientDomainURL}"/>/thomson/page/shop/common/insurance.page?insuranceName=privacypolicy',600,500,'scrollbars=yes,resizable=yes');" >Privacy Policy</a> and use of my information</li>
      </ul>
    </div>
  </div>
  <%-------------------------------------------------------------------------------------------------------%>
  
  <br clear="all" />
  <%-------------------------------------------------------------------------------------------------------%>  
  <div class="highlight width_add">
    <p><input id="printLocally" class="marginTopMinusTwoPixel" name="printLocally" type="radio" onclick="javascript:setFlag('printLocal','true')"  value="<c:out value='${personalDetailsAndPaymentFormBean.printLocally}'/>" alt="Print locally" checked />Print in Shop</p><br/>
    <p><input id="printCentrally" name="printCentrally" class="marginTopMinusTwoPixel" type="radio" onclick="javascript:setFlag('printCentral','true')"  value="<c:out value='${personalDetailsAndPaymentFormBean.printCentrally}'/>" alt="Print centrally" />Print centrally</p>
  </div>
  <%-------------------------------------------------------------------------------------------------------%>
  <br clear="all" />
  
</div><%--END t_and_c --%>
<br/>
<br clear="all" />