<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="discount_pd" class="borderpaydt">
    <h2>Payment Details</h2>
    <div class="highlight paddingbtm5" id="paymenttypeid" >
       
        <c:set var="grayText" value=""/>
        <c:if test="${bookingComponent.datacashEnabled == false}">
            <c:set var="grayText" value="gray_text"/>
        </c:if>
        <%--Payment type header with radio buttons --%>
        <div id="payment_type_header" >
            <div id="c1" class="cust_present">
                <input type="radio" id="cp" name="customer_Type" value="CP" onclick="CustomerTypeChangeHandler.handle(this.value)"/>
                <label  class="payment_type_header_label"><span class="<c:out value='${grayText}'/>"> Customer present</span></label>
            </div>
            <div id="c2" class="cust_not_present">
                <input type="radio" id="cnp" name="customer_Type" value="CNP" onclick="CustomerTypeChangeHandler.handle(this.value)" />
                <label class="payment_type_header_label"><span class="<c:out value='${grayText}'/>">Customer not present (CNP)</span></label>
            </div>
            <div id="c3" class="chip_pin_not_avail">
                <input type="radio" id="cpna" name="customer_Type" value="CPNA" onclick="CustomerTypeChangeHandler.handle(this.value)"/>
                <label class="payment_type_header_label"><span class="<c:out value='${grayText}'/>"> Chip-and-pin not available</span></label>
            </div>
        </div>
        <%--End of Payment type header with radio buttons --%>
        <div id="payment_trans_txt" class="payment_trans_txt">How many payments would you like to make?</div>
        <div id="noOfPaymentsBlock" class="movepaymentrans">
            <select class="num_paymenttypes" id="noOfPayments" name="payment_totalTrans" onchange="NoOfPaymentsChangeHandler.handle(this.value);">
                <c:forEach begin="1" end="${bookingComponent.noOfTransactions}" varStatus="status">
                    <c:out value="<option value='${status.count}'>${status.count}</option>" escapeXml="false"/> 
                </c:forEach>
            </select>
            <input type="hidden" id="preTrans" name="preTrans" value="0"/>
        </div>
        <div id="paymentSelections" class="payment_type_blk"></div>
        <div id="hoplaDisplay"></div>

<div id="paymentAccumlation">

           <div id='totamthide' class='summary_cont'>
	           <div class='summary_val'>Amount Paid</div>
	           <div class='summary_val_txt'>
		           <input  type='hidden' id='totamtpaid_carry'  name='payment_totamtpaid' value=''/>
		           <input class='medium paidAmount' disabled = 'disabled' type='text' id='amountPaid'  value=''/>
	           </div>
           </div>
           <div  class='summary_cont'>
	           <div class='summary_val'>Amount still due</div>
	           <div class='summary_val_txt'><input class='medium amountStillDue' disabled = 'disabled' type='text' id='amountStillDue' name='payment_amtstilltxt' value=''/></div>
           </div>
	           <div class='summary_cont'><div class='summary_val'>Total Amount due</div>
	           <div class='summary_val_txt'><input class='medium calculatedPayableAmount' type='text' disabled = 'disabled' id='totalAmountDue' name='payment_totamtduetxt' value='0'/></div>
           </div>
</div>
</div>
</div>

<%-----------------------------------------------------------------------------------------------------%>
<script type="text/javascript">
        var newHoliday = "<c:out value='${bookingInfo.newHoliday}'/>";
        var totAmtDue = $("#totalAmountDue").val();

        if (parseFloat(totAmtDue) == 0)
        {
            totAmtDue = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
        }

        amtRec = totAmtDue;
        $("#totalAmountDue").val(totAmtDue) ;

        window.onunload=function()
        {
            transactionStopped = true;
            moATS.Reset;
            moATS.Close;
        }
</script>

    <input  name="updateFlag" id="updateFlag" value="false" type="hidden"/>
    <input type="hidden" name="total_transamt" id="total_transamt" value='' />
    <input type="hidden" name="total_cardChargeAmt" id="total_cardChargeAmt" value='' />
    <input type='hidden' id='payment_pinpadTransactionCount' name='payment_pinpadTransactionCount'/>
    <input type='hidden' id='payment_pinpadTransactionAmount' name='payment_pinpadTransactionAmount'/>
     <input type="hidden" name="datacashEnabled" id="datacashEnabled" value="<c:out value='${bookingComponent.datacashEnabled}'/>"/>
     <div id="paymentFields_required"><input type='hidden'  id='panelType' value='CP'/></div>
     
    <script type="text/javascript">
    var tottransamt="Total amount charged in this transaction:";
</script>