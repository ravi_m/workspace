<%--
FlightPanel jsp with following Sections

+OutBound Flight Details
+InBound Flight Details

--%>
<%@include file="/common/commonTagLibs.jspf"%>
<br clear="all" />
<c:if test="${not empty bookingComponent.flightSummary}">
<div id="flightandhotelsection">
  <h3>Flights</h3>
  	<%--------------------------OutBound Flight Details-------------------------------------------%>
    <div <c:if test="${bookingComponent.flightSummary.flightSelected == 'false'}"> class="inactive"  </c:if>>
			<h4>Leaving&#58;</h4>
			<c:forEach var="outboundFlight" items="${bookingComponent.flightSummary.outboundFlight}">
				<c:if test="${not empty outboundFlight}">
					<ul class="flight_info">
						<li>Depart&#58;
							<ul>
								<li><c:out value="${outboundFlight.departureAirportName}"/></li>
								<li class="date"><fmt:formatDate value="${outboundFlight.departureDateTime}" pattern="dd-MMM-yy"/></li>
								<li class="time"><fmt:formatDate value="${outboundFlight.departureDateTime}" type="TIME" pattern="HH:mm"/></li>
							</ul>
						</li>
						<li>Arrive&#58;
							<ul>
								<li><c:out value="${outboundFlight.arrivalAirportName}"/></li>
								<li class="date"><fmt:formatDate value="${outboundFlight.arrivalDateTime}" pattern="dd-MMM-yy"/></li>
								<li class="time"><fmt:formatDate value="${outboundFlight.arrivalDateTime}" type="TIME" pattern="HH:mm"/></li>
							</ul>
						</li>
						<li>Carrier&#58;
							<ul>
								<li>
									<c:out value="${outboundFlight.carrier}" escapeXml="false"/><br/>
									<c:out value="${outboundFlight.operatingAirlineCode}${outboundFlight.flightNumber}"/>
									<br />
								</li>
								<li>
									<c:if test="${(bookingComponent.nonPaymentData['isOutboundLegDreamliner'])== 'true'}">
                 						<div class="wishByo_dreamliner"><img src="/cms-cps/shop/byo/images/Flight.png"></img></div>
	 				    			</c:if>
								</li>
							</ul>
						</li>
						<c:set var="marketingAirlineCode" value="${outboundFlight.marketingAirlineCode}"/>
						<c:set var="operatingAirlineCode" value="${outboundFlight.operatingAirlineCode}"/>
						<c:if test="${marketingAirlineCode != null && operatingAirlineCode != null && operatingAirlineCode != marketingAirlineCode}">
							<li> Operated
								<ul>
									<li><c:out value="${outboundFlight.operatingAirlineShortName}" escapeXml="false"/></li>
								</ul>
								<br/>by&#58;
							</li>
						</c:if>
					</ul>
				</c:if>
			</c:forEach>
			<%------------------------End OutBound Flight Details-----------------------------------------------------------%>

			<%------------------------InBound Flight Details----------------------------------------------------%>
			<h4>Returning&#58;</h4>
			<c:forEach var="inboundFlight" items="${bookingComponent.flightSummary.inboundFlight}">
				<ul class="flight_info">
					<li>Depart&#58;
						<ul>
							<li><c:out value="${inboundFlight.departureAirportName}"/></li>
							<li class="date"><fmt:formatDate value="${inboundFlight.departureDateTime}" pattern="dd-MMM-yy"/></li>
							<li class="time"><fmt:formatDate value="${inboundFlight.departureDateTime}" type="TIME" pattern="HH:mm"/></li>
						</ul>
					</li>
					<li>Arrive&#58;
						<ul>
							<li><c:out value="${inboundFlight.arrivalAirportName}" /></li>
							<li class="date"><fmt:formatDate value="${inboundFlight.arrivalDateTime}" pattern="dd-MMM-yy"/></li>
							<li class="time"><fmt:formatDate value="${inboundFlight.arrivalDateTime}" type="TIME" pattern="HH:mm"/></li>
						</ul>
					</li>
					<li>Carrier&#58;
						<ul>
							<li>
								<c:out value="${inboundFlight.carrier}" escapeXml="false"/><br/>
								<c:out value="${inboundFlight.operatingAirlineCode}${inboundFlight.flightNumber}"/>
								<br />
							</li>
							<li>
								<c:if test="${(bookingComponent.nonPaymentData['isInboundLegDreamliner'])== 'true'}">
                 					<div class="wishByo_dreamliner"><img src="/cms-cps/shop/byo/images/Flight.png"></img></div>
	 				    		</c:if>
							</li>
						</ul>
					</li>
					<c:set var="marketingAirlineCode" value="${inboundFlight.marketingAirlineCode }"/>
					<c:set var="operatingAirlineCode" value="${inboundFlight.operatingAirlineCode}"/>
					<c:if test="${marketingAirlineCode != null && operatingAirlineCode != null && operatingAirlineCode != marketingAirlineCode}">
						<li>Operated
							<ul>
								<li><c:out value="${inboundFlight.operatingAirlineShortName}" escapeXml="false"/></li>
							</ul>
							<br />by&#58;
						</li>
					</c:if>
				</ul>
			</c:forEach>
			<%-------------------------End InBound Flight Details---------------------------------------------------------%>

	 </div>
  </div><%--END flightandhotelsection --%>
</c:if>