<%@include file="/common/commonTagLibs.jspf"%>
<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
<%
    String creditCardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("WiSHBYO.GBP.cardChargeDetails" , "");
    pageContext.setAttribute("creditCardChargeDetails", creditCardChargeDetails, PageContext.REQUEST_SCOPE);
%>
</c:if>
<c:set var="cardChargeDetails" value="${creditCardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<fmt:formatNumber value="${cardChargeArray[0]}" type="number" var="cardCharge" maxFractionDigits="1" minFractionDigits="0"/>
<%--
Deposit Options JSP, consists of following sections
+TotalHolidayCost Details
+Deposit Details
+Full Deposit Details
+Card charge related text in Deposit Section
--%>

<div id="payment_amount" class="borderone depositSection">
  <h2>Payment Amount</h2>
  <%----------------------------TotalHolidayCost Details----------------------------------------------------%>
  <p>
	 Your total holiday cost is <strong>&pound;<span id="totalholidaycost" class="totalholidaycost"><fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/></span ></strong>
	 <br/>
	 The deposit below is due today, but you can choose to pay the full balance if you wish. Please note, the deposit amount includes any insurance options you have selected.<br clear="all"/>Please select your preferred payment amount:
  </p>
  <%---------------------------End TotalHolidayCost Details---------------------------------------------------%>
  <br clear="all"/>

  <%------------------------------Deposit Details------------------------------------------------------%>
  <div class="highlight">
	 <c:set var='checkFlag' value='checked=checked'/>
	 <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}">
		<c:set var="depositDescription" value="Deposit"/>

		<c:if test="${depositComponent.depositType == 'LOWDEPOSIT' || depositComponent.depositType == 'lowdeposit'}">
		      <c:set var="depositDescription" value="Low Deposit"/>
		</c:if>

		<div class="radiolabel">
		  <%------------------------------------------------------------%>
		  <p class="radio">
		    <input  name="paymentPage"  id ="paymentPage" value="true" type="hidden"/>
	       <label>
				<strong><c:out value="${depositDescription}"/>: &pound;<span class="depositAmount <c:out value="${depositComponent.depositType}"/>"><fmt:formatNumber value="${depositComponent.depositAmount.amount}" type="number" maxFractionDigits="2"/></span>*</strong>
				<br/>The outstanding balance of your deposit is <strong>&pound;<span class="outstandingbalance" id="outstandingbalance"><fmt:formatNumber value="${depositComponent.outstandingBalance.amount}" type="number" maxFractionDigits="2"/></span>**</strong> and is due on
				<strong><span id="balanceDueDate"><fmt:formatDate value="${depositComponent.depositDueDate}" pattern="dd/MM/yy"/></span></strong>
			 </label>
		  </p>
		 <%------------------------------------------------------------%>

		 <%------------------------------------------------------------%>
		  <div style="margin-left:580px;">
			 <input  type="radio" class="deposit" name="depositType" id="depositR" <c:out value='${checkFlag}'/> value='<c:out value='${depositComponent.depositType}'/>' onclick="DepositTypeChangeHandler.handle(this,this.value);"/>
		  </div>
		  <hr style="width:690px; size:1px; color:#ADD8E6;"/>
		 <%------------------------------------------------------------%>
		</div><%--END radiolabel --%>
            <c:set var='checkFlag' value=''/>
	 </c:forEach>
  </div>	<%--END highlight --%>
  <%-------------------------------------------------------------------------------------------------------%>

  <%---------------------------Full Deposit Details-----------------------------------------------------%>
  <div class="highlight radiolabel">
	 <p class="radio">
		<label>
			<strong>Full Balance: &pound;<span id="fullCost" class="depositAmount FULLCOST"><fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2"/></span>*</strong>
		</label>
	 </p>
	 <div style="margin-left:580px;margin-top:0px;">
		<input type="radio" class="deposit" name="depositType" id="fullDeposit" value="FULLCOST" onclick="DepositTypeChangeHandler.handle(this,this.value);"/>
	 </div>
  </div>
  <%--------------------------End Full Deposit Details------------------------------------------------------%>

  <%--------------------------Card charge related text in Deposit Section-----------------------------------------------------%>
  <p>* If you pay by credit card the charges will be updated in the price panel once you have selected your method of payment.</p>
  <p>** There are no additional charges when paying by Maestro ,MasterCard Debit or Visa/Delta debit cards.A fee of <c:out value='${cardCharge}'/>% applies to credit card payments,which is capped at �95.00 per transaction when using American Express,MasterCard Credit or Visa Credit Cards</p>

  <%--------------------------End Card charge related text in Deposit Section----------------------------------%>

</div><%--END payment_amount --%>
<br clear="all"/>

<%---------------------------------------------Following should be removed  -------------------------------------%>
<input  name="depositValue"  id ="depositValue" value="<c:out value='${depositComponent.depositAmount.amount}'/>" type="hidden"/>
<input type="hidden" name="lowdepositValue" id ="lowdepositValue" value="<c:out value='${depositComponent.depositAmount.amount}'/>"/>
<input type="hidden" id="depositH" value="<fmt:formatNumber value="${depositComponent.depositAmount.amount}"  type="number" maxFractionDigits="2"/>"/>
<input type="hidden" id="lowDepositH" value="<fmt:formatNumber value="${depositComponent.depositAmount.amount}" type="number" maxFractionDigits="2"/>"/>
<input type="hidden" id="totalHolidayCostH" name="totalHolidayCostH" value='<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>'/>
<input type="hidden" id="fullCostH" value="<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2"/>"/>

