<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />

<script language="JavaScript" type="text/javascript">
    var transamt="Transaction Amount";
    var transamtalert="Please enter a valid Transaction Amount";
    var voucheralert="Please enter a valid Voucher Code";
    var tottransamt="Total amount charged in this transaction:";
    var cashaccept="Cash Accepted";
    var cheqaccept="Cheque Accepted";
    var vouchercode="Voucher Code";
    var vouchervalue="Voucher Value";
    var addanothervoc="Add another voucher";
    var voucheraccept="Voucher Accepted";
    var amountpaid="Amount Paid";
    var amountstilldue="Amount still due";
    var totalamountdue="Total Amount due";
    var cardaccept="Card Payment Accepted";
    var poundsymbol="�";
    var totalvouchervalue="Total Voucher Value";
    var vocamtalert="Please Enter Valid Voucher Amount"
    var exceedalert="Amount entered exceeds total amount due. Please re-enter";
    var cardcode = "Auth code";
    var isHopla = false;
    var amtRec= "";

	var paymenttypeoptions = new Array();
	var paymenttypeoptionsHopla = new Array();
	var payment_CP = new Array();
	var payment_CNP = new Array();
	var payment_CPNA = new Array();
	var payment_PPG = new Array();
    var payment_startYear = new Array();
	var payment_expiryYear = new Array();

    var requiredFields_cp='';
    var requiredFields_cnp=''
    var requiredFields_cpna='';
    var requiredFields_ppg='';
    var requiredFields_startYear ='';
	var requiredFields_expiryYear ='';

    var callType='<c:out value="${bookingComponent.callType}"/>' ;
</script>
<c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_PEG'}">
 <script>
 isHopla = true;
 </script>
</c:if>
<c:forEach var="depositComponent" items="${bookingComponent.depositComponents}">
   	    <c:if test="${depositComponent.depositType == 'olbpFullBalance'}">
	      	  <c:set value="${depositComponent.depositAmount.amount}" var="totalAmountDue" />
	      </c:if>
</c:forEach>

<c:set var="iterCount"  value="${bookingComponent.noOfTransactions}"/>
<div id="paymentFields_required"><input type='hidden'  id='panelType' value='CP'/></div>
	<script>
		<c:forEach var="paymentType" items="${bookingComponent.paymentType['CP']}">
			payment_CP.push("<option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/> '><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>");
				requiredFields_cp+="<c:out value="<input type='hidden' value='${bookingComponent.cardChargesMap[paymentType.paymentCode]}'  name='payment_charge_${paymentType.paymentCode}' id='${paymentType.paymentCode}_charges' />" escapeXml="false"/>"+
				"<c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled'  name='payment_code_${paymentType.paymentCode}' />" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo'  />" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>";
		</c:forEach>
	</script>

	<script>
		<c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
			payment_CNP.push("<option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>");
				requiredFields_cnp+="<c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled' name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>";
		</c:forEach>
	</script>

	<script>
		<c:forEach var="paymentType" items="${bookingComponent.paymentType['CPNA']}">
			payment_CPNA.push("<option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/> '><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>");
				requiredFields_cpna+="<c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled'  name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>";
		</c:forEach>
	</script>

	<%--For HOPLA --%>
	<script>
		<c:forEach var="paymentType" items="${bookingComponent.paymentType['PPG']}">
			payment_PPG.push("<option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/> '><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>");
				requiredFields_ppg+="<c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled'  name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>"+
				" <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>";
		</c:forEach>
	</script>

	<script>
	requiredFields_startYear += "<option value=''>YY</option>";
	<c:forEach var="startYear" items="${bookingInfo.startYearList}">
		requiredFields_startYear += "<option value='<c:out value='${startYear}'/>'><c:out value='${startYear}'/></option>"
	</c:forEach>
	payment_startYear.push(requiredFields_startYear);
	</script>

	<script>
		requiredFields_expiryYear += "<option value=''>YY</option>";
		<c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
			requiredFields_expiryYear += "<option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>"
		</c:forEach>
		payment_expiryYear.push(requiredFields_expiryYear);
	</script>

<div id="discount" class="borderpaydt">
      <h2>Payment Details</h2>
      <div class="highlight paddingbtm5" id="paymenttypeid" >
   <input type="hidden" name="datacashEnabled" id="datacashEnabled" value="<c:out value='${bookingComponent.datacashEnabled}'/>"/>
   <c:choose>
		<c:when test="${bookingComponent.datacashEnabled == false}">
         <%--Payment type header with radio buttons --%>
              <div id="payment_type_header" >
                <div id="c1" class="cust_present">
                   <input type="radio" id="cp" name="customer_Type" value="payment_CP" onclick="javascript:setCustomerType(this.value)"/>
                   <label  class="payment_type_header_label" ><span class="gray_text"> Customer present</span></label>
                </div>
                 <div id="c2" class="cust_not_present">
                    <input type="radio" id="cnp" name="customer_Type" value="payment_CNP" onclick="javascript:setCustomerType(this.value)" />
                    <label class="payment_type_header_label"><span class="gray_text">Customer not present (CNP)</span></label>
                 </div>
                  <div id="c3" class="chip_pin_not_avail">
                     <input type="radio" id="cpna" name="customer_Type" value="payment_CPNA" onclick="javascript:setCustomerType(this.value)"/>
                     <label class="payment_type_header_label"><span class="gray_text">Chip-and-pin not available</span></label>
                  </div>
              </div>
		</c:when>
		<c:otherwise>
              <div id="payment_type_header" >
                <div id="c1" class="cust_present">
                   <input type="radio" id="cp" name="customer_Type" value="payment_CP" onclick="javascript:setCustomerType(this.value)"/>
                   <label  class="payment_type_header_label" >Customer present</label>
                </div>
                 <div id="c2" class="cust_not_present">
                    <input type="radio" id="cnp" name="customer_Type" value="payment_CNP" onclick="javascript:setCustomerType(this.value)" />
                    <label class="payment_type_header_label">Customer not present (CNP)</label>
                 </div>
                  <div id="c3" class="chip_pin_not_avail">
                     <input type="radio" id="cpna" name="customer_Type" value="payment_CPNA" onclick="javascript:setCustomerType(this.value)"/>
                     <label class="payment_type_header_label">Chip-and-pin not available</label>
                  </div>
              </div>
		</c:otherwise>
	</c:choose>
        <%--End of Payment type header with radio buttons --%>

            <div id="payment_trans_txt" class="payment_trans_txt">How many payments would you like to make?</div>
            <div id="movepaymentrans" class="movepaymentrans">
                <select class="num_paymenttypes" id="paymentTrans" name="payment_totalTrans" onchange="javascript:noOfPaymentsHandler(this.value);">
                <c:forEach begin="1" end="${iterCount}" varStatus="status">
                    <c:out value="<option value=${status.count}>${status.count}</option>" escapeXml="false"/>
                </c:forEach>
                </select>
                <input type="hidden" id="preTrans" name="preTrans" value="0"/>
             </div>

             <div id="paymentSelections" class="payment_type_blk"></div>
       <div id="hoplaDisplay"></div>
      <div id="paymentAccumlation"></div>
    </div>
</div>

<script language="JavaScript" type="text/javascript">
   if (parseFloat(document.getElementById("totalAmountDue").value) == 0)
   {
      document.getElementById("totalAmountDue").value = <c:out value="${totalAmountDue}"/>;
   }
   amtRec = document.getElementById("totalAmountDue").value;
   window.onload=
   function()
   {
     <c:choose>
         <c:when test="${bookingInfo.calculatedPayableAmount.amount==0.0}">
               onLoadZeroPaymentHandler();
         </c:when>
         <c:otherwise>
               onLoadPaymentHandler();
          </c:otherwise>
       </c:choose>
        initializeDepositSelection();
  		<c:if test="${bookingComponent.errorMessage != null}">
         disableOtherDiscounts();
	   </c:if>
	   checkBookedStatus();
  }
  window.onunload=
  function()
  {
	  transactionStopped = true;
	  moATS.Reset;
	  moATS.Close;
  }
</script>
<script>
function checkBookedStatus()
{
	checkBookingButton();
}
</script>

<%--maximum card charge and applicable charge cap --%>
<input  name="cardDetailsFormBean.cardCharge" id="cardCharge" value="0.0" type="hidden"/>
<input  name="cardDetailsFormBean.cardLevyPercentage" id="cardLevyPercentage" value="0.0" type="hidden"/>

<input  name="updateFlag" id="updateFlag" value="false" type="hidden"/>

<input  type='hidden' id='totCalculatedAmount'  value='<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>'/>
<input  type='hidden' id='totamtpaidWithoutCardCharge'  name='payment_amtwithdisc' value='<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>'/>
<input  type='hidden' id='payment_pinpadTransactionCount' name='payment_pinpadTransactionCount'/>
<%-- Added for obtaining card type and processing it starts here --%>
<script language="vbscript">
	Dim trans_Index
	Dim amountWithCardCharge
	Sub moATS_CardInserted(CardType)
	  document.getElementById("cardType"&trans_Index).value = CardType
	  transAmt = RmChr(document.getElementById("transamt"&trans_Index).value, "�")
	  moATS.AcceptCard(amountWithCardCharge)
	End Sub

	Public Function setIndex(byVal index)
		trans_Index = index
	End Function

	Public Function acceptCard(byVal calculatedAmount)
		amountWithCardCharge = calculatedAmount
	End Function

	Private Function RmChr(byVal string, byVal remove)
		Dim i, j, tmp, strOutput
		strOutput = ""
		for j = 1 to len(string)
			tmp = Mid(string, j, 1)
			for i = 1 to len(remove)
				tmp = replace( tmp, Mid(remove, i, 1), "")
				if len(tmp) = 0 then exit for
			next
			strOutput = strOutput & tmp
		next
		RmChr = strOutput
	End Function
</script>