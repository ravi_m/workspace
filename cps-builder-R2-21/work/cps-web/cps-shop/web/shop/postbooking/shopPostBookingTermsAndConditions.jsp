<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />

<div id="t_and_c" class="bordertc">
   <h2>Terms and Conditions</h2>
   <%-- On any error below class="highlight" becomes class="highlight errormessage"--%>
   <%-- Start of the key formation --%>
   <c:set var="accommodationInventorySystem" value="" scope="page"/>
   <c:set var="flightInventorySystem" value="" scope="page"/>
   <c:set var="callType" value="" scope="page"/>
    <c:set var="ptBookingDate" value="" scope="page"/>
   <c:set var="callType" value="${bookingComponent.callType}"/>
   <c:set var="searchtype" value="${bookingComponent.searchType}" scope="request"/>
   <c:if test="${bookingComponent.accommodationSummary.accommodationSelected == true}">
      <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem != null}">
         <c:set var="accommodationInventorySystem" value="${bookingComponent.accommodationSummary.accommodationInventorySystem}" scope="page"/>
      </c:if>
   </c:if>
   <c:if test="${(bookingComponent.flightSummary.flightSelected == true)}">
      <c:if test="${bookingComponent.flightSummary.flightSupplierSystem != null}">
         <c:set var="flightInventorySystem" value="${bookingComponent.flightSummary.flightSupplierSystem}" scope="page"/>
      </c:if>
   </c:if>
   <c:set var="key" value="${accommodationInventorySystem}${flightInventorySystem}" scope="page"/>
   <%-- End of key formation--%>


   <div class="highlight">
      <p>Please read our
      <a href="javascript:void(0);" onclick="Popup('<c:out value="${bookingComponent.clientDomainURL}"/>/thomson/page/shop/common/insurance.page?insuranceName=privacypolicy',600,500,'scrollbars=yes,resizable=yes');" >Privacy Policy</a>
      and
      <a href="javascript:void(0);" onclick="javascript:showHiddenInformation('dataprotectionnoticediv')" >Data Protection Notice</a>
      and confirm you agree to our use of your information provided above (which may in special situations include sensitive personal data) by ticking the box below. </p>
   </div>

   <div id="dataprotectionnoticediv" class="highlight highlight_protectionnotice" style="display:none;">
     <p id="dataprotectionnoticebox" ><strong>Data Protection Notice:</strong><br/>We may from time to time contact you by post with further information on the latest offers, brochures, products or services which we believe may be of interest to you, from Thomson Holidays (a division of TUI UK Ltd), other holiday divisions within and group companies of TUI UK Ltd.</p>
    <p><input  id="contactByEmail" name="contactByEmail" type="checkbox" value="true" />If you would not like to receive <b>e-communications</b> including information on discounts from <a href="https://www.thomson.co.uk" target="_blank">thomson.co.uk</a>, please <u>tick</u> this box.</p>
      <p><input  id="contactByPost" name="contactByPost" type="checkbox" value="true" /><label for="contactByPost">Our business partners and carefully selected companies outside our holiday group would like to send you information about their products and services <strong>by post</strong>. If you <strong>would not</strong> like to hear from them, please tick this box.</label></p>

   </div>

   <div class="highlight">
      <p>Please note that all members of your party require valid passports and any applicable visas before travelling. Visit the Foreign Office websiteat <a href="https://www.fco.gov.uk/travel" target="_blank">https://www.fco.gov.uk/travel</a> to see visa and travel advice, or the Passport Office website at <a href="https://www.passport.gov.uk" target="_blank">https://www.passport.gov.uk</a> for passport information.</p>
   </div>
   <c:if test="${tcClassificationKey != 'three'}">
      <div class="highlight">
      <p>To view the notice summarising the liability rules applied by Community aircarriers as required by Community legislation and the Montreal Convention, please view the <a href="javascript:void(0);" onclick="Popup('<c:out value="${bookingComponent.clientDomainURL}"/>/thomson/page/shop/common/insurance.page?insuranceName=airpassengernotice',600,500,'scrollbars=yes,resizable');">Air Passenger Notice.</a></p>
   </div>
   </c:if>
  	<c:choose>
		<c:when
			test="${bookingComponent.packageType == 'DP' && !bookingComponent.flightSummary.flightSelected}">
			<div class="highlight termscheck_height">
			    <input name=""	type="checkbox" value=""
				  alt="You must read and accept our Terms and Conditions before confirming your booking.|Y|CHECK" />
				<div class="width_add">
				   <label for="book_cond"> The customer has read and accepts Thomson's
						<ul class="bookingconds">
							<li><a
								href="https://www.thomsonbeach.co.uk/th/beach/viewTermsAndConditions.do?brochureId=TH01&seasonId=S2007"
								target="_blank">Booking Conditions</a></li>
							<li><a href="javascript:void(0);"
								onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',755,584,'scrollbars=yes');">
								Privacy Policy</a> and use of my information</li>
						</ul>
				 	</label>
				</div>
			</div>
		</c:when>
		<c:when
			test="${bookingComponent.packageType == 'DP' && !bookingComponent.accommodationSummary.accommodationSelected}">
			<div class="highlight termscheck_height">
			    <input name=""	type="checkbox" value=""
				  alt="You must read and accept our Terms and Conditions before confirming your booking.|Y|CHECK" />
				<div class="width_add">
					<label for="book_cond"> The customer has read and accepts Thomson's
					<ul class="bookingconds">
						<li><a
							href="https://destinations.thomson.co.uk/devolved/terms/terms-and-conditions-index.htm"
							target="_blank">Booking Conditions</a></li>
						<li><a href="javascript:void(0);"
							onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',755,584,'scrollbars=yes');">
							Privacy Policy</a> and use of my information</li>
					</ul>
					</label>
				</div>
			</div>
		</c:when>
		<c:when
			test="${bookingComponent.packageType == 'DP' && bookingComponent.flightSummary.flightSelected && fhBookingContext.accommodationSummary.accommodationSelected}">
			<div class="highlight termscheck_height">
			    <input name=""
				 type="checkbox" value=""
				 alt="You must read and accept our Terms and Conditions before confirming your booking.|Y|CHECK" />
				<div class="width_add">
					<label for="book_cond"> The customer has read and accepts Thomson's
					<ul class="bookingconds">
						<li><a
							href="https://destinations.thomson.co.uk/devolved/terms/terms-and-conditions-index.htm"
							target="_blank">Booking Conditions</a></li>
						<li><a href="javascript:void(0);"
							onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',755,584,'scrollbars=yes');">
							Privacy Policy</a> and use of my information</li>
					</ul>
					</label>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="highlight termscheck_height">
			    <input name=""
				 type="checkbox" value=""
				 alt="You must read and accept our Terms and Conditions before confirming your booking.|Y|CHECK" />
				<div class="width_add">
					<label for="book_cond"> The customer has read and accepts Thomson's
					<ul class="bookingconds">
						<li><a
							href="https://destinations.thomson.co.uk/devolved/terms/terms-and-conditions-index.htm"
							target="_blank">Booking Conditions</a></li>
						<li><a href="javascript:void(0);"
							onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',755,584,'scrollbars=yes');">
							Privacy Policy</a> and use of my information</li>
					</ul>
					</label>
				</div>
			</div>
		</c:otherwise>
	</c:choose>

	<c:if test="${bookingComponent.packageType == 'DP'}">
		<div class="highlight width_add"><input name="" type="checkbox"
			value=""
			alt="You must read and accept our Terms and conditions  before confirming your booking|Y|CHECK" />
		    I understand that this selection of components is not a package and therefore falls outside the Package Travel, Package Holidays and Package Tours Regulations 1992.
		    <br	clear="all" />
		</div>
	</c:if>


   <div class="highlight width_add">
      <p><input id="printLocally" class="marginTopMinusTwoPixel" name="printLocally" type="radio" onclick="javascript:setFlag('printLocal','true')"  value="<c:out value='${personalDetailsAndPaymentFormBean.printLocally}'/>" alt="Print locally" checked />Print in Shop</p>
      <br clear="all" />

   </div>
</div><br>


<div class="alignButtonTop">
   <span class="backlink">
      <c:set var="url" value='${bookingComponent.clientDomainURL}'/>
      <c:choose>
         <c:when test="${bookingComponent.nonPaymentData['multipleResults'] eq 'true'}">
           <a class='backlink' title='Back' href="<c:out value='${url}'/>/thomson/page/shop/postbooking/book/shopbookingssearchresults.page?back='true'">
             <img src="/cms-cps/shop/postbooking/images/icons/img_dp_button_back.gif"	border="0" /></a>
         </c:when>
         <c:otherwise>
           <a href="<c:out value='${url}'/>/thomson/page/shop/postbooking/search/shopbookingssearchpanel.page" class="backlink" title="Back">
             <img src="/cms-cps/shop/postbooking/images/icons/img_dp_button_back.gif"	border="0" /></a>
         </c:otherwise>
      </c:choose>
   </span>
   <input type="hidden" name="multipleResults" value='<c:out value="${bookingComponent.nonPaymentData['multipleResults']}"/>'>
       <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}" varStatus="vardepost">
         <c:if test="${depositComponent.depositType == 'alreadyPaid'}">
            <c:set value="${depositComponent.outstandingBalance.amount}" var="amountToBePaid"/>
         </c:if>
       </c:forEach>
<c:if test="${not empty bookingComponent.paymentType}">
   <span>
	  <c:if
		test="${amountToBePaid > 0 && bookingComponent.nonPaymentData['postPaid'] eq 'false'}">
		<a href="javascript:void(0);" style="cursor:hand" onclick="checkOlbpPaymentAndValidate();">
          <img
			src="/cms-cps/shop/postbooking/images/buttons/btn_confirm_payment.gif"
			alt="Confirm payment" title="Confirm payment" border="0"
			id="confirmpayment" />
           </a>
   	  </c:if>
   </span>
</c:if>
</div>
<br clear="all" />