<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="url" value='${bookingComponent.clientDomainURL}'/>

<ul id="p_thead_menu">
    <li class="firstChild"><a href="<c:out value='${url}'/>/thomson/page/shop/byo/home/home.page" target="_top">Home</a></li>

    <li><a href="<c:out value='${url}'/>/thomson/page/shop/byo/home/lateshome.page" target="_top">Late Offers</a></li>
    <%-- <li><a href="javascript:void(0)">Manage Booking</a></li> --%>
    <li><a href="http://tui-retail-braclive/">Manage Booking</a></li>
    <li id="hideBalancePayment"><a href="<c:out value='${url}'/>/thomson/page/shop/postbooking/search/shopbookingssearchpanel.page" target="_top">Balance Payment</a></li>
    <li><a onclick="LaunchExe();" href="file:///C:/Program Files/TTG/Rapid/Vision.exe" target="_blank">Rapid</a></li>
    <li><a href="http://click" target="_blank">InsideClick Retail</a></li>
    <li><a href="http://click/aw/Retail/On_the_web/~bgwn/On_the_web/" target="_blank">On the Web</a></li>


</ul>

<a href="<c:out value='${url}'/>/thomson/page/shop/byo/home/home.page">
<img src="/cms-cps/shop/common/images/tuithomsonlogo.gif" alt="Thomson.co.uk" name="header_thomsonlogo" width="271" height="50" /></a>
<script language="JavaScript">
  function LaunchExe()
  {
  		if(window.XMLHttpRequest)
  		{
    	if (window.ActiveXObject)
    	 {
     	   var WshShell = new ActiveXObject("WScript.Shell");
   		   var dirfile="C:\\Program Files\\TTG\\Rapid\\Vision.exe";
    	   var oExec = WshShell.Exec(dirfile);
		 }
		else
		{
		   window.open('file:///C:/Program Files/TTG/Rapid/Vision.exe');
		}
		}
		else
		{
		   window.open('file:///C:/Program Files/TTG/Rapid/Vision.exe');
		}
   }
</script>
<script type="text/javascript" language="javascript">
	//js variable to be used in the intellitracker pqry string.
	var previousPageVar ="<c:out value='${bookingComponent.intelliTrackSnippet.previousPage}'/>";
</script>
<div style="display:none">
	<c:out value="${bookingComponent.intelliTrackSnippet.intelliTrackerString}" escapeXml="false"/>
</div>