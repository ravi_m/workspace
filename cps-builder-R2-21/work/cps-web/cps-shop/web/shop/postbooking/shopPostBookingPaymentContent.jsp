<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="searchtype" value="${bookingComponent.searchType}" scope="request" ></c:set>
<c:set var="byo" value="BYO" scope="request" ></c:set>
<c:set var="lates" value="LATES"  scope="request" ></c:set>
<c:set var="freekids" value="FKS"  scope="request" ></c:set>

<div id="breadcrumb">
   <c:set var="url" value='${bookingComponent.clientDomainURL}'/>
   <a href="<c:out value='${url}'/>/thomson/page/shop/postbooking/search/shopbookingssearchpanel.page">Search &raquo;</a>
   <ul>
      <c:if test="${bookingComponent.nonPaymentData['multipleResults'] eq 'true'}">
         <li><a href="<c:out value='${url}'/>/thomson/page/shop/postbooking/book/shopbookingssearchresults.page?back='true'">Results</a>&nbsp;&raquo;</li>
      </c:if>
      <li class="current">Payment&nbsp;&raquo;</li>
      <li>Confirmation</li>
   </ul>
</div>

<div class="contenthead filterpage"><%-- add "filterpage" class if this page is a filtered results page --%>
   <h1>Payment Details</h1>
</div>

<c:set scope = "request" var="callscript" value="javascript:CheckSeniorPassengerAndValidate()"/>
<c:set scope = "request" var="callscriptPromotionCode" value="javascript:handlePromotionCode()"/>
<c:set scope = "request" var="callscriptPriceBeat" value="javascript:handlePriceBeat()"/>
<c:set scope = "request" var="callscriptTodaySaving" value="javascript:handleTodaySaving()"/>
<c:set scope = "request" var="callscriptPromotionalCode" value="javascript:handlePromotionalCodePdp()"/>

<c:if test="${not empty bookingComponent.errorMessage}">
    <div id="pricediff1" class="warning_box">
         <p><c:out value="${bookingComponent.errorMessage}"/></p>
    </div>
</c:if>
<%-- This id is added to display the error page in the case if the promotion code is not valid --%>
<div id="promoError" class="warning_box" style="display:none;">
</div>
   <input type="hidden" value="0" name="totalAmountDue" id="totalAmountDue" />
   <input type="hidden" value="<c:out value="${bookingComponent.clientApplication}" />" name="clientApplication" id="clientApplication" />
   <div id="Information"  name="Information" style="width:250px;"></div>
   <script>
      var numberOfSeniorPassengers =0// "<c:out value='${TravelOptionsFormBean.passengersOver65}'/>";
      var NumberOfAdults =2;//"<c:out value='${fhBookingContext.searchCriteria.partyComposition.partySize}'/>";
      var lessSeniorPassengersMessage="Please select the appropriate number of senior citizens as chosen in previous page";
      var moreSeniorPassengersMessage="Please select the appropriate number of senior citizens, navigate to previous page if required.";
      var surnameValidationError="We're sorry but we cannot book you onto your chosen flight.  Would you like to select another flight and try again.";
      var supplierSystem="<c:out value='${bookingComponent.flightSummary.flightSupplierSystem}'/>";
      var supplierAmadeus="Amadeus";
   </script>
   <jsp:include page="shopPostBookingPersonalDetails.jsp" /><br/>
   <c:if test="${not empty bookingComponent.depositComponents}">
   	  <jsp:include page="shopPostBookingDepositOptions.jsp" /><br/>
   </c:if>
   <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}">
      <c:if test="${depositComponent.depositType == 'alreadyPaid'}">
	    <c:set value="${depositComponent.outstandingBalance.amount}" var="outstandingBalance" />
      </c:if>
   </c:forEach>
   <c:if test="${outstandingBalance != 0 && not empty bookingComponent.paymentType}">
   <jsp:include page="shopPostBookingPaymentDetails.jsp" /><br/>
   </c:if>
   <jsp:include page="shopPostBookingTermsAndConditions.jsp" />