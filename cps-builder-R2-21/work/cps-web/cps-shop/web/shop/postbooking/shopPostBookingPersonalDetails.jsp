<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />

<div id="pers_details" class="borderpd">
<h2>Personal Details</h2>
  <div>
	<p>Passenger names shown on your passports.</p>
	<%--   enter passenger names starts--%>
	<table cellspacing="0" cellpadding="0" class="highlight" border="0">
		<tr>
			<th width="85">Title</th>
			<th width="140">Forename</th>
			<th width="80">Middle Initial</th>
			<th width="133">Surname</th>
		</tr>
		<c:set var="passengerCount" value="0" />
	    <c:forEach var="passenger" items="${bookingComponent.passengerRoomSummary}" varStatus="status">
	    	<c:forEach var="passengerRoom" items="${passenger.value}" varStatus="passen">
				<tr>
		           <c:set var="numberOfPassengers" value="${passen.index}"/>
		           <td width="85">
		  		      <c:set var="titlekey" value="personaldetails_${passengerCount}_title"/>
					  <c:out value="${bookingComponent.nonPaymentData[titlekey]}" />
				   </td>
		           <td width="140">
		              <c:set var="foreNameKey" value="personaldetails_${passengerCount}_foreName"/>
		              <c:out value="${bookingComponent.nonPaymentData[foreNameKey]}"/>
		           </td>
		           <td width="80">
		              <c:set var="middleNameKey" value="personaldetails_${passengerCount}_middleInitial"/>
					  <c:out value="${bookingComponent.nonPaymentData[middleNameKey]}"/>
		           </td>
		           <td width="133">
		              <c:set var="lastNameKey" value="personaldetails_${passengerCount}_surName"/>
					  <c:out value="${bookingComponent.nonPaymentData[lastNameKey]}"/>
		           </td>
	          	</tr>
			</c:forEach>
	    </c:forEach>
	</table>
</div>
</div>