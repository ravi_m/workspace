<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="url" value='${bookingComponent.clientDomainURL}'/>
<c:choose>
    <c:when test="${bookingComponent.shopDetails.retailBrand == 'FIRS'}">
    <ul id="p_thead_menufc">
            <li class="firstChild"><a href="http://tui-retail-live/thomson/page/shop/byo/home/home.page" target="_top">Home</a></li>
            <li><a href="http://tui-retail-live/thomson/page/shop/byo/home/lateshome.page" target="_top">Late Offers</a></li>
            <li><a href="http://tui-retail-braclive/">Manage Booking</a></li>
            <%--<li id="hideBalancePayment"><a href="<c:out value='${url}'/>/thomson/page/shop/postbooking/search/shopbookingssearchpanel.page" target="_top">Balance Payment</a></li>--%>
            <li><a onclick="LaunchExe();" href="file:///C:/Program Files/TTG/Rapid/Vision.exe" target="_blank">Rapid</a></li>
            <li><a href="http://click" target="_blank">InsideClick Retail</a></li>
            <li><a href="http://click/aw/Retail/On_the_web/~bgwn/On_the_web/" target="_blank">On the Web</a></li>
            <li><a target="_blank" href="http://www.specialistholidays.com/agents">Specialist Holidays Group</a></li>
        </ul>
    <div class="homefcLogo">
        <a href='http://tui-retail-live/thomson/page/shop/byo/home/home.page'>
            <img src="/cms-cps/shop/byo/images/logos/first_choice_logo.gif" alt="First Choice" /></div>
        </a>
        <img src="/cms-cps/shop/byo/images/backgrounds/FC_icon.jpg" alt="First Choice" class="homefcFlower" />
        </div>
    </c:when>
    <c:otherwise>
<ul id="p_thead_menu">
    <li class="firstChild"><a href="http://tui-retail-live/thomson/page/shop/byo/home/home.page" target="_top">Home</a></li>
    <li><a href="http://tui-retail-live/thomson/page/shop/byo/home/lateshome.page" target="_top">Late Offers</a></li>
    <li><a href="http://tui-retail-braclive/">Manage Booking</a></li>
    <%--<li id="hideBalancePayment"><a href="<c:out value='${url}'/>/thomson/page/shop/postbooking/search/shopbookingssearchpanel.page" target="_top">Balance Payment</a></li>--%>
    <li><a onclick="LaunchExe();" href="file:///C:/Program Files/TTG/Rapid/Vision.exe" target="_blank">Rapid</a></li>
    <li><a href="http://click" target="_blank">InsideClick Retail</a></li>
    <li><a href="http://click/aw/Retail/~bfzm/Frameset/?cfr=/aw/~bfzk/Retail/" target="_blank">On the Web</a></li>
    <li><a target="_blank" href="http://www.specialistholidays.com/agents">Specialist Holidays Group</a></li>
</ul>
<a href='http://tui-retail-live/thomson/page/shop/byo/home/home.page'>
     <img src="/cms-cps/shop/common/images/header.gif" alt="Thomson.co.uk" name="header_thomsonlogo" style="margin-left: 0px;" border="0" height="100" width="800">
</a>
    </c:otherwise>
</c:choose>
<script type="text/javascript">

   function LaunchExe()
  {
        if(window.XMLHttpRequest)
        {
        if (window.ActiveXObject)
         {
           var WshShell = new ActiveXObject("WScript.Shell");
           var dirfile="C:\\Program Files\\TTG\\Rapid\\Vision.exe";
           var oExec = WshShell.Exec(dirfile);
         }
        else
        {
           window.open('file:///C:/Program Files/TTG/Rapid/Vision.exe');
        }
        }
        else
        {
           window.open('file:///C:/Program Files/TTG/Rapid/Vision.exe');
        }
   }


</script>

<script type="text/javascript">
    //js variable to be used in the intellitracker pqry string.
    <c:if test="${bookingComponent != null}">
        var previousPageVar ="<c:out value='${bookingComponent.intelliTrackSnippet.previousPage}'/>";
    </c:if>
     </script>
<div style="display:none">
    <c:if test="${bookingComponent != null}">
        <c:out value="${bookingComponent.intelliTrackSnippet.intelliTrackerString}" escapeXml="false"/>
    </c:if>
</div>
