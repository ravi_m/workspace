<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="url" value='${bookingComponent.clientDomainURL}'/>

<input type="hidden" id="homepagename" name="homepagename" value="byohomepage"/>
<div id="p_logoff_panel">
        <div id="p_logoff_btn">
   	  		<a href="javascript:logoffDialog();">Logoff</a>
	    </div>
		<div id="p_new_customer">
     		<a href="http://tui-retail-live/thomson/page/shop/login/newcustomer.page">New Customer</a>
     	</div>
		<div id="logoffCheck" style="display: none;">

			 <div class="message">
				<p><img class="warningIcon"
				src="/cms-cps/shop/common/images/exclamation-icon.gif" />
				Are you sure you want to log off?
				</p>

				<br clear="all"/>
				<img class="button" src="/cms-cps/shop/common/images/yes-btn.gif" alt="Yes"
	         title="Yes" onclick="javascript:logOff('http://tui-retail-live');" />
				<img class="button" src="/cms-cps/shop/common/images/no-btn.gif" alt="No"
				title="No" onclick="cancelLogin()" />
				<br clear="all"/>
			</div>
	      <img class="logoffArrow" src="/cms-cps/shop/common/images/plus_popup_arrow_yellow.gif"
	       alt="" title="" />
		 </div>
	</div>