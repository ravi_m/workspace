<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div id="t_and_c" class="bordertc">
	<h2>Terms and Conditions</h2>
	<c:set var="accommodationInventorySystem" value="CRUISE_ACC" scope="page"/>
	<c:set var="flightInventorySystem" value="CRUISE_FLI" scope="page"/>
	<c:set var="accomType" value="${bookingComponent.cruiseSummary.ship.accommodationType}" />
	<%-- On any error below class="highlight" becomes class="highlight errormessage"--%>
	<div class="highlight">
		<p>
			Please read our <a href="javascript:void(0);" onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',755,584,'scrollbars=yes');" >Privacy Policy</a> and
			<a href="javascript:void(0);" onclick=" javascript:showHiddenInformation('dataprotectionnoticediv')"> Data Protection Notice</a>
			and confirm you agree to our use of your information provided above (which may in special situations include sensitive personal data) by ticking the box below.
		</p>
	</div>
	<div id="dataprotectionnoticediv" class="highlight highlight_protectionnotice" style="display:none;">
		<p id="dataprotectionnoticebox">
			<strong>Data Protection Notice:</strong><br />
			We may from time to time contact you by post with further information on the latest offers, brochures, products or services which we believe may be of interest to you, from Thomson Holidays (a division of TUI UK Retail Limited), other holiday divisions within and group companies of TUI UK Retail Limited.
		</p>
		<p>
			<input id="contactByEmail" name="contactByEmail" type="checkbox" value="true" />If you would not like to receive information on the latest offers, discounts,products and services by <strong>e-communications</strong> from <a href="https://www.thomson.co.uk" target="_blank">Thomson.co.uk</a>, please tick this box.
		</p>
		<p>
			<input id="contactByPost" name="contactByPost" type="checkbox" value="true" />
			<label for="contactByPost">
				Our business partners and carefully selected companies outside our holiday group would like to send you information about their products and services <strong>by post</strong>. If you
				<strong>would not</strong>like to hear from them, please tick this box.
			</label>
		</p>
	</div>
	<div class="highlight">
		<p>Please note that all members of your party require valid passports and any applicable visas before travelling. Visit the Foreign Office website at <a href="http://www.fco.gov.uk/travel" target="_blank">http://www.fco.gov.uk/travel</a> to see visa and travel advice, or the Passport Office website at <a href="https://www.passport.gov.uk" target="_blank">https://www.passport.gov.uk</a> for passport information.</p>
		<p>For information summarising terms and provisions relating to the carriage of passengers and their luggage by sea 1974, please view the
			<a href="javascript:void(0);" onclick="Popup('https://www.thomson.co.uk/editorial/legal/Cruise/athens-convention.html',795,595,'location=0,status=0,left=100,top=50,toolbar=0,menubar=0,titlebar=0,resizable=1,scrollbars=1')">Athens Convention Notice.</a>
		</p>
	</div>
	<c:if test='${accomType == "CRUISE_AND_STAY" || accomType == "STAY_AND_CRUISE" || accomType == "FLY_CRUISE"}'>
		<div class="highlight">
			<p>To view the notice summarising the liability rules applied by Community air carriers as required by Community legislation and the Montreal Convention, please view the Air Passenger Notice</p>
		</div>
	</c:if>
	<c:forEach var="outboundFlight" items="${bookingComponent.flightSummary.outboundFlight}">
		<fmt:formatDate var="departureDate" value="${outboundFlight.departureDateTime}" pattern="yyyy-MM-dd"/>
	</c:forEach>
	<div id="termConditions" class="termscheck_height">
		<div class="width_add">
			<input name="book_cond" type="checkbox" value="" alt="You must read and accept our Terms and Conditions before confirming your booking.|Y|CHECK" />
			<p class="bookCondLabel">
				I confirm that :<br />
				(a) as the lead name/passenger, I am at least 18 years old.<br />
				(b) I have contacted the relevant travel supplier(s) regarding any special needs before booking and <br />
				(c) I have read and accept all of the following: <br/>
				<ul class="bookConditions">
					<li><a href="javascript:void(0)" onclick="popUpBookingConditions('<c:out value="${accommodationInventorySystem}"/>','<c:out value="${flightInventorySystem}"/>','<c:out value="${departureDate}"/>','<c:out value="${bookingComponent.clientDomainURL}"/>')">Booking Conditions</a></li>
					<li><a href="javascript:void(0);" onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',755,584,'scrollbars=yes');">Privacy Policy</a> and use of my information </li>
				</ul>
			</p>
		</div>
	</div><br clear="all"/>
	<div class="highlight width_add">
		<p><input id="printLocally" name="printLocally" type="radio" onclick="javascript:setFlag('printLocal','true')" checked="checked" alt="Print locally" /><label for="printLocally">Print in Shop</label></p>
		<p><input id="printCentrally" name="printCentrally" type="radio" onclick="javascript:setFlag('printCentral','true')" alt="Print centrally" /><label for="printCentrally">Print centrally</label></p>
		<br clear="all" />
	</div>
	<div class="width_add"></div>
</div>
<br/>
<div class="width_add">
	<span class="backlink">
		<a href="<c:out value='${bookingComponent.clientDomainURL}'/>/thomson/page/shop/cruise/booking/prepayment.page" title="Back" class="backlink">
			<img src="/cms-cps/shop/common/images/img_dp_button_back.gif" width="17" height="16" border="0" alt="Back"/>Back
		</a>
	</span>
	<c:if test="${not empty bookingComponent.paymentType and bookingInfo.newHoliday}">
		<span id="confirmButton"> <%-- The pay button will be enabled only if the param newHoliday is set to true. --%>
			<a href="javascript:void(0);" onclick="updateEssentialFields();CheckSeniorPassengerAndValidate();">
				<img src="/cms-cps/shop/common/images/btn_confirm_booking.gif" width="119" height="23" alt="Confirm booking" title="Confirm booking" border="0" id="confirmbooking" />
			</a>
		</span>
	</c:if>
</div>
<br clear="all" />
