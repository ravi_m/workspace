<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
 <c:set var="url" value='${bookingComponent.clientDomainURL}'/>
<div id="breadcrumb">
    <c:if test="${not empty bookingComponent.breadCrumbTrail}">
   <ul>
     <c:forEach var="breadCrumb" items="${bookingComponent.breadCrumbTrail}">
	 <li><a href="<c:out value='${url}'/><c:out value='${breadCrumb.value}'/>"><c:out value="${breadCrumb.key}" /></a>&raquo;</li>
	 </c:forEach>
	 <li class="current">Payment &raquo;</li>
     <li>Confirmation</li>
   </ul>
  </c:if>
</div>
<c:if test="${not empty bookingComponent.errorMessage && empty responseMsg}">
    <div id="pricediff1" class="warning_box">
            <p><c:out value="${bookingComponent.errorMessage}"/></p>
    </div>
</c:if>
<input type="hidden" value="0" name="totalAmountDue" id="totalAmountDue" />
<div id="Information" style="width:250px;"></div>
<script type="text/javascript">
    var supplierSystem="<c:out value='${bookingComponent.flightSummary.flightSupplierSystem}'/>";
    var supplierAmadeus="Amadeus";
</script>
<c:if test="${not empty bookingComponent.depositComponents}">
    <jsp:include page="cruiseDepositOptions.jsp" /><br/>
</c:if>
<c:if test="${not empty bookingComponent.paymentType}">
    <jsp:include page="cruisePaymentDetails.jsp" /><br/>
</c:if>
<c:choose>
    <c:when test ="${not empty bookingComponent.termsAndCondition.relativeTAndCUrl}">
        <jsp:include page="cruiseTermsandConditions.jsp" /><br/>
    </c:when>
    <c:otherwise>
        <div>
            <jsp:include page="cruiseNavigation.jsp" />
        </div>
    </c:otherwise>
</c:choose>
