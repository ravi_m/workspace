<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1;" />
        <version-tag:version/>
        <title>Build Your Own Holidays - Cruises - Payment </title>
        <%@ include file="/shop/cruise/resources.jspf" %>
        <c:set var="clientapp" value="${bookingComponent.clientApplication.clientApplicationName}"/>
        <%-- Terms and Conditions url--%>
        <c:catch>
            <c:set var="relativeTAndCUrl" value="${bookingComponent.termsAndCondition.relativeTAndCUrl}" />
        </c:catch>
        <script type="text/javascript">
            var tandCurl = "<c:out value='${relativeTAndCUrl}' escapeXml='false'/>";
        </script>
        <%-- End of Terms and Conditions url--%>
    </head>
    <body>
    <%-- TBP section starts here --%>
    <%
         String brandName = (String) request.getParameter("brand");
         String tbpValue = com.tui.uk.config.ConfReader.getConfEntry(brandName+".isTbpDisplay", "");
         pageContext.setAttribute("tbpValue", tbpValue, PageContext.REQUEST_SCOPE);
    %>
    <%-- TBP section ends here --%>
    <c:set var="customerType" value="${bookingComponent.nonPaymentData['customer_Type']}" scope="request"/>
    <c:set var="selectedDeposit" value="${bookingComponent.nonPaymentData['depositType']}" scope="request"/>
    <%--Since pan and security code are in form of character array we are converting them to String--%>
    <c:forEach var="paymentTransaction" items="${paymentTransactions}">
       <c:forEach var="pan" items="${paymentTransaction.card.pan}">
          <c:set var="pandata" value="${pandata}${pan}" />
       </c:forEach>
       <c:forEach var="securitycode" items="${paymentTransaction.card.cv2}">
          <c:set var="securitycodedata" value="${securitycodedata}${securitycode}" />
       </c:forEach>
       <c:set var="responseMsg" value="${transactionResponseMsg}" scope="request"/>
    </c:forEach>
    <%@ include file="/shop/common/scriptConfigSettings.jspf" %>
        <c:if test="${not empty bookingInfo.bookingComponent.paymentType}">
            <%-- Pinpad DLL initialization starts here --%>
            <object name="moATS" classid="clsid:FB6D3933-5128-4B13-AD9D-06B6B18C29F1" declare="declare" style="display:none;"></object>
            <script  type="text/vbscript">
                On Error Resume Next
                Err.Clear
                If Not moATS.Initialise("C:\Program Files\TTG\Rapid\Database\TUICNP.DAT") Then
                    MsgBox("Failed to Initialise:" & moATS.LastError)
                Else
                    If moATS.Connect() = false then
                        MsgBox("Failed to Connect:" & moATS.LastError)
                    End If
                End If
                On Error Resume Next
                Err.Clear

            Sub moATS_StatusChanged(message)
                startGetMsg()
            End Sub

            Sub moATS_Complete(isProcessCompletedDLL)
                isProcessCompleted = isProcessCompletedDLL
            End Sub
            </script>
            <%-- Pinpad DLL initialization ends here --%>
        </c:if>
        <c:choose>
            <c:when test="${bookingInfo.bookingComponent.shopDetails.retailBrand == 'FIRS'}">
                <div id="fcheader">
            </c:when>
            <c:otherwise>
                <div id="header">
            </c:otherwise>
        </c:choose>
            <jsp:include page="cruiseHeaderPartHtml.jsp" />
        </div>
        <div id="logoff">
            <jsp:include page="/shop/common/logoff.jsp" />
        </div>
        <%-- Body section --%>
        <form name="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}'/>&amp;brand=<c:out value='${param.brand}'/>&amp;tomcat=<c:out value='${param.tomcat}' />">
            <input type="hidden" name="tomcatInstance" id="tomcatInstance" value="<c:out value='${param.tomcat}' />"/>
            <input type="hidden" name="token" id="token" value="<c:out value='${param.token}' />"/>

            <div id="leftpanel">
                <jsp:include page="cruiseSummaryPanel.jsp"/>
                <c:if test='${bookingComponent.cruiseSummary.ship.accommodationType == "CRUISE_AND_STAY" || bookingComponent.cruiseSummary.ship.accommodationType == "STAY_AND_CRUISE" || bookingComponent.cruiseSummary.ship.accommodationType == "FLY_CRUISE"}'>
                     <div align="center">
                        <a href="javascript:Popup('<c:out value="${bookingComponent.clientDomainURL}/thomson/page/shop/common/atoz/atoztandc.page"/>',755,584,'scrollbars=yes');">A-Z Booking Information</a>
                     </div>
                </c:if>
            </div>

            <div id="bodysection">

               <div id="content">
                  <jsp:include page="cruisePaymentContent.jsp"/>
               </div>
               <c:if test='${bookingComponent.cruiseSummary.ship.accommodationType == "CRUISE"}'>
            </div>
                </c:if>
            </div>
        </form>
        <%-- End of body section --%>
        <%-- Footer --%>
        <div id="footer">
        </div>
        <%-- End of  Footer --%>
    </body>
</html>