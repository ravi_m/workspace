// Trim spaces from start and end of string
function TrimSpaces(objval) {
  // Remove Space at start
  RESpace = /^\s*/i;
  objval = objval.replace(RESpace, '');
  // Remove Space at end
  RESpace = /\s*$/i;
  objval = objval.replace(RESpace, '');
  return objval;
}

function logoffDialog()
{
   document.getElementById('logoffCheck').style.display='block';
}
function cancelLogin()
{
   document.getElementById('logoffCheck').style.display='none';
}

function logOff(url)
{
   window.parent.document.location.replace(url+'/thomson/page/shop/login/logoff.page');
}


document.onkeydown = function() {
      return alertkey(window.event);
};
function alertkey(e) {
  if( !e ) {
    if( window.event ) {
     //Internet Explorer
      e = window.event;
    } else {
      return;
    }
  }
  if( typeof( e.keyCode ) == 'number'  ) {
    //DOM
      e = e.keyCode;
  } else if( typeof( e.which ) == 'number' ) {
    //NS 4 compatible
      e = e.which;
  } else if( typeof( e.charCode ) == 'number'  ) {
    //also NS 6+, Mozilla 0.9+
      e = e.charCode;
  } else {
    //total failure, we have no way of obtaining the key code
    return;
  }
  if(e==13) {

      var loginErrorDisplayedObj = document.getElementById('loginErrorDisplayed');

      if (loginErrorDisplayedObj)
      {
         var loginErrorDisplayed = loginErrorDisplayedObj.value;

         if (loginErrorDisplayed == 'true')
         {
            element_to_process = document.getElementById('error_ok');
         }
         else
         {
            element_to_process = document.getElementById('continue');
         }
         document.getElementById('loginErrorDisplayed').value = 'false';
         element_to_process.click();
      }
  }
}