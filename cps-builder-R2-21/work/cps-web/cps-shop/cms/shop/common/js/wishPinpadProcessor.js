/**************************************************************************************************/
/**********************************PIN PAD*********************************************************/
/**************************************************************************************************/
/* Scripts for pinpad starts here */

/* Global variables used throughout the script starts here. */
var stitle;
var smessage = "";
var pinPadReference;
var userMsg = "";
var transactionNumber;
var changedUserMsg;
var txtAmount = 0;
var reversalStatus = false;
var reverseCompleted;
var acceptStatus = false;
var transNumber;
var refundTransNumber;
var transactionStopped = false;
var poundSymbol = "�";
var trans_Index;
var paymentStatus = false;
var timer;
var pinPadTransactionCount = 0;
var ispinpadRefund = false;
var cardChargesObtained = false;
var STAN_Number;
var transactionStatus = false;
var reverseAmountStatus = false;
var postpayTextOne="Card Charges of ";
var postpayTextTwo=" applied";
var reversalIndex;
var pinpad_calculatedAmount, pinpad_CardCharges;
var cardChargesCounter = 0;
var pinpadReverseCompleted = false;
var displayCardReverseCompleted = false;
var referrerStatus = true;

/* Global variables used throughout the script ends here. */

/**
 ** Function to start the pinpad payment transaction. This function checks whether the transaction
 ** amount is entered and if amount is entered then starts the transaction by passing the
 ** amount as a parameter to ATS API "TakeThisPayment". This function is also responsible for
 ** acquiring the messages sent by ATS via DLL.
 **
 ** @param index - The index of the processing transaction.
**/
function startTransaction(index)
{
   trans_Index = index;

   //This varibale needs to be reset for every transaction since the flag might refer to
   //the previous txn in case of multiple transactions
   isProcessCompleted = false;
   if ($("#transAmt_"+trans_Index).val().length != 1 && $("#transAmt_"+trans_Index).val() != PaymentDetailsContent.currencySymbol)
   {
    txtAmount = StringUtils.stripChars(document.getElementById('transAmt_'+index).value,PaymentDetailsContent.currencySymbol+''+','+' ');
   }
   else
   {
      alert("Please enter the transaction amount.");
      return false;
   }
   $("#transamt"+trans_Index).disabled = true;
   moATS.CP = true;
   if (moATS.Message == undefined)
      userMsg = "";
   else
      userMsg = moATS.Message;
   changedUserMsg = moATS.Message;
   if(txtAmount>=0)
   {
     moATS.TakeThisPayment(txtAmount);
   }
   transactionNumber = moATS.TRN;
   transactionStopped = false;
   $("#paymentType_"+trans_Index).disabled=true;
   insertPinpadDiv(index);
   startClaim();
}

/**
 ** Function to start the pinpad refund transaction. This function checks whether the transaction
 ** amount is entered and if amount is entered then starts the transaction by passing the
 ** amount as a parameter to ATS API "IssueThisRefund". This function is also responsible for
 ** acquiring the messages sent by ATS via DLL.
 **
 ** @param index - The index of the processing transaction.
**/
function startRefundTransaction(index)
{
   trans_Index = index;
   if (($("transamt"+trans_Index).value).length != 1 && $("transamt"+trans_Index).value != currencySymbol)
      txtAmount = stripChars($("transamt"+trans_Index).value,currencySymbol+''+','+' ');
   else
   {
      alert("Please enter transaction amount.");
      return false;
   }
   $("transamt"+trans_Index).disabled = true;
   moATS.CP = true;
   if (moATS.Message == undefined)
      userMsg = "";
   else
      userMsg = moATS.Message;
   changedUserMsg = moATS.Message;
   var refundAmount = parseFloat(Math.abs(txtAmount)).toFixed(2);
   if (refundAmount > 0)
   {
      moATS.IssueThisRefund(refundAmount);
      ispinpadRefund = true;
   }
   transactionNumber = moATS.TRN;
   transactionStopped = false;
   $("payment_type_"+trans_Index).disabled=true;
   startClaim();
}

/**
 ** Function used for initializing the pinpad transactions. This function resets all the
 ** transaction details entered.
**/
function startStep()
{
   if (transactionStopped && $("#pinPadCard"+trans_Index))
   {
	  document.getElementById("paymentType_"+trans_Index).disabled = false;
	  document.getElementById("paymentType_"+trans_Index).selectedIndex = 0;
      stitle = $("#header_title"+trans_Index);
      stitle.html("");
      smessage = $("#header_message"+trans_Index);
      smessage.html("");
      pinpadReference = $("#pinPadReference"+trans_Index);
      pinpadReference.html("");
      $("#cancelDiv"+trans_Index).html("");
      //$("cancelDiv"+trans_Index).style.display = "none";
      $("#pinPadCard"+trans_Index).attr("display","none");
      $("#pinPadCard"+trans_Index).remove();
      $("#buttonDiv"+trans_Index).html('');
   }
}

/**
 ** Function used for receiving the messages sent by ATS via DLL. This function is also
 ** responsible for calling the pinpad processing function.
**/
function startGetMsg()
{
   userMsg = moATS.Message;
   startClaim();
}

/**
 ** Function used for processing the pinpad transactions as a timely event call. The time
 ** configured for this is 1/2 second. Flags are used for processing the events (events
 ** are initializing the device, device acquisition, card insertion, payment, reversals & refunds)
 ** as they occur. Incase if any error occurs the same will be conveyed to user as an alert.
**/
function startClaim()
{
   if (moATS.STAN != "")
   {
      STAN_Number = moATS.STAN;
   }
   if (!transactionStopped && $("#pinPadCard"+trans_index))
   {
      //alert(userMsg+" && "+acceptStatus+" && "+reversalStatus+" && "+reverseCompleted+" && "+ispinpadRefund)
      stitle = $("#header_title"+trans_Index);
      if (isProcessCompleted && !acceptStatus && !reversalStatus && ispinpadRefund)
      {
		  refundConfirmationDisplay();
      }
      else if (isProcessCompleted && !acceptStatus && !reversalStatus && !ispinpadRefund)
      {
		  paymentStatusSetter();
      }
      else if (isProcessCompleted && !acceptStatus && reversalStatus && !reverseCompleted)
      {
		  paymentReversalPrompt();
      }
      else if (isProcessCompleted && !acceptStatus && reverseCompleted && reversalStatus && displayCardReverseCompleted)
      {
		  paymentReverseConfirmation();
      }
	  else if (userMsg.indexOf("Call Auth Centre") != -1 && !acceptStatus && !reversalStatus && !ispinpadRefund)
	  {
		  referCardToIssuer();
	  }
      else
      {
		  pinpadStarterDisplay();
      }
   }
}

/**
 ** Function used to display the initialization of pinpad dymanic text area and
 ** responsible for updating till the payment is actually made at the datacash end.
 ** This function also takes the responsibility to update the applicable card charges
 ** for displaying it to the user.
 **/
function pinpadStarterDisplay()
{
	acceptStatus = false;
	stitle.html( "Claim PIN Pad" );
	smessage = $("#header_message"+trans_Index);
	smessage.html(userMsg);
	pinpadReference = $("#pinPadReference"+trans_Index);
	pinpadReference.html(pinpadInitialDisplay(transactionNumber));
	makeCancelDivEmpty(trans_Index);
	var disableAttributeSetter = null;
	if (reverseCompleted && reversalStatus && displayCardReverseCompleted)
	{
		disableAttributeSetter = true;
		//$("#cancelDiv"+trans_Index).html(renderCancelButton(disableAttributeSetter, "claim"));
	}
	else
	{
		disableAttributeSetter = false;
		//$("#cancelDiv"+trans_Index).html(renderCancelButton(disableAttributeSetter, "claim"));

	}
	PaymentView.displayCancelButton(trans_Index,disableAttributeSetter);
	$("#paymentType_"+trans_Index).attr('disabled','disabled');
}

/**
 ** This function is responsible for displaying the refer the transaction
 ** to the card issuer. If the transaction is a referred transaction the
 ** call centre person will be prompted to call the bank and get the issue
 ** number and enter it in the displayed field.
 **/
function referCardToIssuer()
{
	// Remove the top cancel button
	$("#buttonDiv"+trans_Index).html('');
    var transactionDetMapValueList = PaymentInfo.transactionDetailsMap.getAllValuesList();
    var pinpadCardCharge =  1*transactionDetMapValueList[trans_Index].cardCharge
	stitle = "Refer transaction to card issuer";
	smessage = $("#header_message"+trans_Index);
	smessage.html(userMsg);
	var acc_CardChargesDisplay = '';
	if (referrerStatus)
	{
		acc_CardChargesDisplay += pinpadReferToIssuer(trans_Index);
		$("#pinPadReference"+trans_Index).html("");
		$("#pinPadReference"+trans_Index).html(acc_CardChargesDisplay);
		//makeCancelDivEmpty(trans_Index);
		$("#referToIssuerCancelDiv"+trans_Index).html(renderCancelDiv(null, "referToIssuer"));
		$("#cardCharge"+trans_Index).val(PaymentDetailsContent.currencySymbol + pinpadCardCharge);
		//$("#payment_"+trans_Index+"_chargeIdAmount").value = pinpad_CardCharges;
		//$("#total_amount_"+trans_Index).val(stripChars($("#transAmt"+trans_Index).val(),currencySymbol+''+','+' '));
		//$("#transamt_carry"+trans_Index).val(stripChars($("transAmt"+trans_Index).val(),currencySymbol+''+','+' '));
		//var cardCharges = stripChars($("#cardCharge"+trans_Index).val(),currencySymbol+''+','+' ');
		$("#payment_"+trans_Index+"_chargeIdAmount").val(pinpad_CardCharges);
		$("#acc_totalAmount"+trans_Index).val($("#totalTransAmount_"+trans_Index).html());
		//$("#cardType"+trans_Index).val(PaymentInfo.pinpadCardType);
		//$("#transamt_carry"+trans_Index).val(parseFloat(pinpad_calculatedAmount).toFixed(2));
		$("#cardIssuerAuthNo"+trans_Index).val("");
		referrerStatus = false;


	}
}

/**
 ** Function used to set the status flags that helps in progressing further to next steps
 ** of payment process. This function also sets the card type and the accumulatedCardCharge
 ** into PaymentInfo object.
 **/
function paymentStatusSetter()
{
	acceptStatus = true;
	paymentStatus = true;
	transactionStatus = true;
	$("#pinpadMsgDetails"+trans_Index).html("");
	//alert("cardType"+trans_Index+" :"+$("cardType"+trans_Index).value)
	$("#paymentType_"+trans_Index).attr('disabled','disabled');

	//The confirmation code.
	paymentConfirmation();
}

/**
 ** Function used to display the payment confirmation screen, once the payment process is successfully
 ** completed. This function also takes the responsibility to update the card charges (if applicable),
 ** total amount and the deposit section based on the transaction made.
 ** Not required to be called from startClaim() function as per event driven implementation.
 ** Its called from paymentStatusSetter function directly.
 **/
function paymentConfirmation()
{
	     //Remove the cancel button
	     $("#buttonDiv"+trans_Index).html("");
	     var transactionDetMapValueList = PaymentInfo.transactionDetailsMap.getAllValuesList();
	     var pinpadCardCharge =  1*transactionDetMapValueList[trans_Index].cardCharge
	     var transactionAmountWithCardCharge = 1*pinpadCardCharge+1*transactionDetMapValueList[trans_Index].transactionAmount
         $("#pinpadMsgDetails"+trans_Index).html(postpayTextOne+PaymentDetailsContent.currencySymbol+pinpadCardCharge+postpayTextTwo);
         stitle.html("Card payment successful");
         smessage = $("#header_message"+trans_Index);
         smessage.html(userMsg);
         var acc_CardChargesDisplay = pinpadPaymentSuccess(trans_Index);
         var xmlPrintContent = printContents(trans_Index);
		 //makeCancelDivEmpty(trans_Index);

		 //Commented the below line, since reversal is caveated for IP release.
         /*$("cancelDiv"+trans_Index).innerHTML = '<div class="colDiv floatLeft" style="margin-left: 180px;">'+
               '<input type="button" name="reversePayment'+trans_Index+'" value="Reverse Payment" onclick="javascript:setReversalStatus('+trans_Index+');"/></div>';*/
         reversalIndex = trans_Index;
         $("#pinPadReference"+trans_Index).html("");
         $("#pinPadReference"+trans_Index).html(acc_CardChargesDisplay + xmlPrintContent);
         $("#cardCharge"+trans_Index).val(PaymentDetailsContent.currencySymbol + pinpadCardCharge);
         $("#payment_"+trans_Index+"_chargeIdAmount").val(pinpadCardCharge);
         $("#payment_"+trans_Index+"_chargeIdAmount").val(pinpadCardCharge);
         $("#acc_totalAmount"+trans_Index).val(PaymentDetailsContent.currencySymbol+(1*(StringUtils.stripFirstChar($("#transAmt_"+trans_Index).val()))+1*pinpadCardCharge).toFixed(2));
         transNumber = moATS.DatacashRefCode;
         $("#transactionNo"+trans_Index).val(transNumber);
         $("#payment_"+trans_Index+"_referenceNumber").val(transNumber);
         $("#transamt_carry"+trans_Index).val(parseFloat(transactionAmountWithCardCharge).toFixed(2));
         $("#customerCopy"+trans_Index).val(moATS.CustPrintXML);
         $("#merchantCopy"+trans_Index).val(moATS.ShopPrintXML);
         if (paymentStatus)
         {
            ++pinPadTransactionCount;
            $("#payment_pinpadTransactionCount").val(pinPadTransactionCount);
            if (transactionStatus)
            {
               transactionStatus = false;
			   referrerStatus = true;
			   PaymentInfo.updateTransactionDetails(trans_Index,true)
			   PaymentInfo.updatePaymentInfo();
            }
         }
         xmlPrintContent = "";
         document.getElementById('cardType'+trans_Index).value = $("#pinpadCardType"+trans_Index).val();
         document.getElementById('payment_'+trans_Index+'_referenceNumber').value = $("#payment_"+trans_Index+"_referenceNumber").val();
         PaymentView.updatePaymentPage(PaymentInfo);
}

/**
 ** Function used to display the reversal prompt screen.
 ** This function also takes the responsibility to display the card charges (if applicable),
 ** total amount and the deposit section based on the transaction made.
 **/
function paymentReversalPrompt()
{
	stitle.innerHTML = "Payment reversal";
	smessage = $("header.message"+trans_Index);
	smessage.innerHTML = userMsg;
	var confirmAcceptReversal = pinpadPaymentReversal(trans_Index);
	makeCancelDivEmpty(trans_Index);
	$("cancelDiv"+trans_Index).innerHTML = renderCancelDiv(null, "paymentReversal");
	$("pinPadReference"+trans_Index).innerHTML = "";
	$("pinPadReference"+trans_Index).innerHTML = confirmAcceptReversal;
	transNumber = moATS.DatacashRefCode;
	$("cardCharge"+trans_Index).value = currencySymbol + pinpad_CardCharges;
	$("payment_"+trans_Index+"_chargeIdAmount").value = pinpad_CardCharges;
	var cardCharges = stripChars($("cardCharge"+trans_Index).value,currencySymbol+''+','+' ');
	$("totalAmount"+trans_Index).value = currencySymbol + parseFloat(1*txtAmount + 1*cardCharges).toFixed(2);
	$("transactionNo"+trans_Index).value = transNumber;
	$("cardType"+trans_Index).value = PaymentInfo.pinpadCardType;
	if ($("pinpadMsgDetails"+trans_Index) && $("pinpadCardDetails"+trans_Index))
	{
		if ($("pinpadMsgDetails"+trans_Index).innerHTML != '' || $("pinpadCardDetails"+trans_Index).innerHTML != '')
		{
			$("cardCharges"+trans_Index).style.display = "none";
			$("pinpadMsgDetails"+trans_Index).style.display = "none";
			$("pinpadCardDetails"+trans_Index).style.display = "none";
		}
	}
}

/**
 ** Function used to display the reversal confirmation screen.
 ** This function also takes the responsibility to display the reversed card charges (if applicable),
 ** total amount and the deposit section based on the reversal made.
 **/
function paymentReverseConfirmation()
{
	if (($("payment_type_"+trans_Index).value).indexOf("CardPinPad ") != -1)
	{
		stitle.innerHTML = "Card payment reversed";
		smessage = $("header.message"+trans_Index);
		smessage.innerHTML = userMsg;
		var rev_CardChargesDisplay = pinpadPaymentReverseConfirmation(trans_Index);
		makeCancelDivEmpty(trans_Index);
		$("cancelDiv"+trans_Index).innerHTML = renderCancelDiv(null, "paymentReversed");
		$("pinPadReference"+trans_Index).innerHTML = "";
		$("pinPadReference"+trans_Index).innerHTML = rev_CardChargesDisplay;
		transNumber = moATS.DatacashRefCode;
		$("cardCharge"+trans_Index).value = currencySymbol + parseFloat(0.00).toFixed(2);
		$("payment_"+trans_Index+"_chargeIdAmount").value = pinpad_CardCharges;
		var cardCharges = stripChars($("cardCharge"+trans_Index).value,currencySymbol+''+','+' ');
		$("totalAmount"+trans_Index).value = currencySymbol + parseFloat(1*cardCharges + 1*txtAmount).toFixed(2);
		$("transactionNo"+trans_Index).value = transNumber;
		if (reverseAmountStatus)
		{
			reversePaymentForPinpad(pinpad_CardCharges);
			reverseAmountStatus = false;
			$("cardCharges"+trans_Index).innerHTML = "";
			$("tottransamt"+trans_Index).innerHTML = currencySymbol + txtAmount;
			$("pinpadMsgDetails"+trans_Index).innerHTML = "";
			$("pinpadCardDetails"+trans_Index).innerHTML = "";
		}
	}
}

/**
 ** This function is responsible for displaying the refund confirmation screen.
 **/
function refundConfirmationDisplay()
{
	$("payment_type_"+trans_Index).disabled=false;
	stitle.innerHTML = "Confirmation of Refund";
	smessage = $("header.message"+trans_Index);
	smessage.innerHTML = userMsg;
	var refundDisplay = refundDisplay(trans_Index);
	makeCancelDivEmpty(trans_Index);
	$("pinPadReference"+trans_Index).innerHTML = "";
	$("pinPadReference"+trans_Index).innerHTML = refundDisplay;
	refundTransNumber = moATS.DatacashRefCode;
	$("transactionNo"+trans_Index).value = refundTransNumber;
}

/**
 ** This function is responsible for clearing the cancel div content and makes it's
 ** inner HTML empty.
 **/
function makeCancelDivEmpty(trans_Index)
{
	$("cancelDiv"+trans_Index).innerHTML = "";
	$("cancelDiv"+trans_Index).className = "";
}

/**
 ** Function to check whether the inserted card is valid or not. The card date received
 ** from the ATS via DLL is verified against the current date and intimates the user if
 ** the card is expired.
 **
 ** @param trans_Index - The index of transaction being processed currently.
 **/
function checkCardValidity(trans_Index)
{
   var cardValidationError = false;
   var cardExpiryDate, cardValidation;
   var currentDate = new Date();
   if ($("cardType"+trans_Index).value != "")
      cardExpiryDate = moATS.ExpiryDate;

   if (cardExpiryDate != undefined || cardExpiryDate != null)
   {
      var cardExpireMonth = cardExpiryDate.substring(0,2);
      var cardExpireYear = cardExpiryDate.substring(2,4);
      var currentMonth = currentDate.getMonth();
      var currentYear = currentDate.getFullYear();
      if (cardExpireYear.length == 2)
         var cardExpireFullYear = "20"+(cardExpireYear);
   }
   if (parseInt(cardExpireFullYear, 10) < parseInt(currentYear, 10))
   {
      if ((parseInt(cardExpireFullYear, 10) < parseInt(currentYear, 10)) || (parseInt(cardExpireFullYear, 10) == parseInt(currentYear, 10)))
      {
         if ((parseInt(cardExpireMonth, 10) <= parseInt(currentMonth, 10)))
         {
            alert("Card used is expired. Use another card having expiry year greater than " + currentMonth + "/" + currentYear);
            $("pinpadMsgDetails"+trans_Index).innerHTML = "";
            $("pinpadCardDetails"+trans_Index).innerHTML = "";
            transactionStopped = true;
            moATS.AbortTransaction;
            moATS.Reset;
            startStep();
            timer = clearTimeout(timer);
            cardValidationError = true;
            cardChargesObtained = true;
         }
      }
   }
   return cardValidationError;
}

/**
 ** Function for cancelling the transaction being processed, onclick of cancel
 ** button the processing transaction is cancelled and resets the device to ready
 ** state for processing the next trasaction.
**/
function doStopButtonCancel()
{
   acceptStatus = true;
   reversalStatus = false;
   ispinpadRefund = false;
   moATS.AbortTransaction;
   moATS.Reset;
   $("cardCharges"+trans_Index).style.display = "block";
   $("pinpadMsgDetails"+trans_Index).style.display = "block";
   $("pinpadCardDetails"+trans_Index).style.display = "block";
}

/**
 ** Function for stopping the transaction being processed, onclick of cancel
 ** button the processing transaction is stopped and resets the device to ready
 ** state for processing the next trasaction.
**/
function doStopButton()
{
   if ($("#pinpadMsgDetails"+trans_Index) && $("#pinpadCardDetails"+trans_Index))
   {
      if ($("#pinpadMsgDetails"+trans_Index).html() != '' || $("#pinpadCardDetails"+trans_Index).html() != '')
      {
         $("#cardCharges"+trans_Index).html("");
         $("#tottransamt"+trans_Index).html(PaymentDetailsContent.currencySymbol + txtAmount);
         $("#pinpadMsgDetails"+trans_Index).html("");
         $("#pinpadCardDetails"+trans_Index).html("");
      }
   }
   transactionStopped = true;
   moATS.AbortTransaction;
   moATS.Reset;
   PaymentInfo.resetTransactionDetails(trans_Index);
   PaymentInfo.updatePaymentInfo();
   PaymentView.updatePaymentPage(PaymentInfo);
   PaymentView.displayPinpadLI(trans_Index, true);
   startStep();
   return transactionStopped;
}

/**
 ** Function for clearing the transaction happened, onclick of clear button
 ** the processed transaction is reset and makes the device to ready
 ** state for processing the next trasaction.
**/
function doClearButton()
{
   if ($("pinpadMsgDetails"+trans_Index) && $("pinpadCardDetails"+trans_Index))
   {
      if ($("pinpadMsgDetails"+trans_Index).innerHTML != '' || $("pinpadCardDetails"+trans_Index).innerHTML != '')
      {
         $("cardCharges"+trans_Index).innerHTML = "";
         $("tottransamt"+trans_Index).innerHTML = PaymentDetailsContent.currencySymbol + txtAmount;
         $("pinpadMsgDetails"+trans_Index).innerHTML = "";
         $("pinpadCardDetails"+trans_Index).innerHTML = "";
      }
   }
   transactionStopped = true;
   reverseAmountStatus = false;
   reversalStatus = false;
   acceptStatus = false;
   reverseCompleted = false;
   ispinpadRefund = false;
   moATS.AbortTransaction;
   moATS.Reset;
   startStep();
}

/**
 ** Function responsible for setting the reversal status flags.
 **
 ** @param index - The index of transaction being processed currently.
 **/
function setReversalStatus(index)
{
   if (reversalIndex == index)
   {
      reversalStatus = true;
      acceptStatus = false;
      reverseCompleted = false;
      //return reversalStatus;
   }
   else
   {
      alert("Reversal not allowed for previous transactions.");
      return false;
   }
}

/**
 ** Function responsible for invoking the reversal API for the transaction
 ** completed. The last happened transaction's STAN number is passed in as
 ** a parameter and based on reversal status a boolean value is returned.
**/
function checkReversalStatus()
{
   moATS.Amount = txtAmount;
   reverseCompleted = moATS.DoReversal(STAN_Number);
   if (reverseCompleted)
   {
      reverseAmountStatus = true;
	  displayCardReverseCompleted = true;
   }
}

/**
 ** Function to obtain card charges for the pinpad cards.
 ** @param cardType - CardType obtained from pinpad DLL
 ** @param trans_Index - Transaction number
*/
function getCardChargeDetails(cardType, trans_Index)
{
   if (cardChargesCounter == 0)
   {
      ++cardChargesCounter;
      cardChargesObtained = true;
      var amtWithoutCC = stripChars($("#transamt"+trans_Index).value,PaymentDetailsContent.currencySymbol+''+','+' ');
      pinpad_calculatedAmount = parseFloat($("#payment_pinpadTransactionAmount").value).toFixed(2);
	  PaymentInfo.chargePercent = cardChargeMap.get(PaymentInfo.pinpadCardType);
      pinpad_CardCharges = parseFloat(1*amtWithoutCC * 1*PaymentInfo.chargePercent/100).toFixed(2);
      if (1*pinpad_CardCharges > 1*PaymentInfo.maxCardChargeAmount)
	   {
         pinpad_CardCharges = parseFloat(1*PaymentInfo.maxCardChargeAmount).toFixed(2);
	   }
      cardChargesObtained = false;
   }
}

/** This function is responsible for updating card charges in summary panel
 ** @params cardCharge -represents cardChargeAmt to be displayed for the transaction
*/
function updateCardChargesInSummaryPanelForPinpad(cardCharge)
{
    if($('cardChargeText'))
     {
         if(cardCharge > 0)
        {
           $('cardChargeAmount').innerHTML=PaymentDetailsContent.currencySymbol+parseFloat(cardCharge).toFixed(2);
           $('cardChargeText').style.display='block';
           $('cardChargeAmount').style.display='block';
        }
        else
        {
           $('cardChargeText').style.display='none';
           $('cardChargeAmount').style.display='none';
         }
    }
}

/**
 ** Function responsible for updating the amounts based on the reversals
 ** happened for a particular transaction.
 **
 ** @param cardCharges - Card charge amount for transaction happened.
**/
function reversePaymentForPinpad(cardCharges)
{
   if (PaymentInfo.currentIndex > 0)
      PaymentInfo.totalCardCharge = parseFloat(1*PaymentInfo.totalCardCharge - 1*cardCharges).toFixed(2);
   else
      PaymentInfo.totalCardCharge = parseFloat(1*cardCharges).toFixed(2);
   PaymentInfo.accumulatedCardCharge = PaymentInfo.totalCardCharge;
   calculateReversalAmount();
   updatePaymentInfoForMultipleTransactions();
   displaySelectedDepositOptionsWithCardCharge();
   updateCardChargesInSummaryPanelForPinpad(PaymentInfo.accumulatedCardCharge);
   displayTotalAmounts(bookingConstants.TOTAL_CLASS);
   showPayDetails();
}

/**
 ** Function responsible for calculating the amounts based on the reversals
 ** happened for a particular transaction.
**/
function calculateReversalAmount()
{
   var reversalAmount = 0;
   pinpadReverseCompleted = true;
   for (var i = 0; i < PaymentInfo.totalTransactions; i++)
   {
      if (PaymentInfo.currentIndex != i && PaymentInfo.currentIndex > 0)
      {
         reversalAmount += 1*stripChars($("transamt"+i).value,currencySymbol+''+','+' ');
         PaymentInfo.amtPaid = parseFloat(1*reversalAmount + 1*PaymentInfo.accumulatedCardCharge).toFixed(2);
		 if ($("clientApplication") && ($("clientApplication").value).indexOf("BRAC") != -1)
		 {
			 allTransTotal = PaymentInfo.amtPaid;
		 }
         PaymentInfo.amtDue = parseFloat((1*PaymentInfo.basePayableAmount + 1*PaymentInfo.accumulatedCardCharge)- 1*PaymentInfo.amtPaid).toFixed(2);
         //PaymentInfo.payableAmount = parseFloat((1*PaymentInfo.basePayableAmount + 1*PaymentInfo.amtPaid)- 1*PaymentInfo.amtDue).toFixed(2);
		 PaymentInfo.payableAmount = parseFloat(1*PaymentInfo.basePayableAmount + 1*PaymentInfo.accumulatedCardCharge).toFixed(2);
      }
      else if (PaymentInfo.currentIndex == 0)
      {
         intialUpdatePaymentInfo();
      }
   }
}

/**
 ** This function is responsible for checking the card issuer
 ** auth number field and validate the same and sends it to
 ** datacash for further processing.
 **/
function checkReferToIssuer()
{
	cardIssuerAuthNumber = $("#cardIssuerAuthNo"+trans_Index).val();
	if(cardIssuerAuthNumber=="" || cardIssuerAuthNumber==null)
	{
		alert("Card Issuer Auth Number cannot be empty.");
		return false;
	}
	moATS.UserInput=cardIssuerAuthNumber;
}

/**
 * This function inserts the pinpad div
 */
function insertPinpadDiv(index)
{
	$('#paymentSection'+index).append(pinPadCardSectionDisplay('PINPAD','CardPinPad',index));
}


function pinPadCardSectionDisplay(selVal,selValEnumValue,transVal)
{
   var pinpadPopup = '<div id="pinPadCard'+transVal+'"><div class="rowDiv headerTitle" id="header_title'+transVal+'">&nbsp;</div>'+
      '<div class="rowDiv statusMsg" id="header_message'+transVal+'"></div><div class="rowDiv">&nbsp;</div>'+
      '<div class="rowDiv" id="pinPadReference'+transVal+'">&nbsp;</div><div class="rowDiv">&nbsp;</div>'+
      '<div class="rowDiv"><input type="hidden" value='+selValEnumValue+' name="payment_'+transVal+'_paymentmethod" />'+
      '<input type="hidden" value='+selVal+' name="payment_'+transVal+'_paymenttypecode"/>'+
      '<input type="hidden" value="" name="payment_'+transVal+'_referenceNumber" id="payment_'+transVal+'_referenceNumber">'+
      '<input type="hidden" value="" name="payment_pinpadCardType'+transVal+'" id="pinpadCardType'+transVal+'"/></div></div>';
	return pinpadPopup;
}

/* Scripts for pinpad ends here */
