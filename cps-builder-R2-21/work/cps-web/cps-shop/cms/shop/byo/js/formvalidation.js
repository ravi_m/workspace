var ns4 = (document.layers);
var ns6 = (!document.all && document.getElementById);


// Validate form for submission
var isCheck=true;
function ValidateForm(formobj)
{
  for (i=0;i<formobj.elements.length;i++)
  {
   obj = formobj.elements[i]

   if(TrimSpaces(obj.value) == 'select') {
      return SetFocus('Please select the nature of your e-mail.',obj)
   }

   // Identify Validation required - check for disabled
   if (obj.getAttribute("alt")) {

   disabledlayer = false
   obj1 = obj

   // Find if obj is on a DIV
   while (obj1.tagName != 'BODY') {
   if (obj1.tagName == 'DIV' ||obj1.tagName == 'DL') {
    if (obj1.style.display == 'none') {
     disabledlayer = true
     break;
    }
   }
    obj1 = obj1.parentNode
   }

   if ((obj.getAttribute("alt") != '') && (!disabledlayer)) {
   valset = obj.getAttribute("alt").split("|")

   // Trim input
   obj.value = TrimSpaces(obj.value)

   obj_value = obj.value
   obj_name = valset[0];
   if (valset[1] == 'Y') {
    obj_mandatory = true
   } else {
    obj_mandatory = false
   }
   obj_type = valset[2]
   obj_params = valset.slice(3)
   //alert(obj_mandatory)
   // Mandatory

   if (obj_mandatory) {
    if ((obj_value == '') && ((obj.type == 'text') || (obj.type == 'textarea') || (obj.type == 'password'))) return SetFocus(obj_name, obj)
    if ((!obj.checked) && (obj.type == 'checkbox')) return SetFocus(obj_name,obj)

         if (obj.name == 'partPaymentType' && (!obj.checked))
   {
      if (isCheck)
      {
         isCheck=false;
         return SetFocus(obj_name, obj)
      }
   }

   // Check selection has ben made on selection boxes
    if (obj.type.substr(0,6) == 'select') {
    if (obj.options.selectedIndex==0) {
      return SetFocus(obj_name, obj)
      }
    }
   }

   // Validation Type
   if (obj_type) {
    switch (obj_type.toUpperCase()) {
     // Alphanumeric 0-9 a-z A-Z Space Hyphen / \
    case 'ALPHA':
            var Pat1 = /^[a-zA-Z -]*$/;
            if (!Pat1.test(obj_value)) { return SetFocus('Please enter valid details' ,obj) }
            break;

    case 'ALPHANUMERIC':
            var Pat1 = /^[-a-zA-Z0-9 \\\/.,]*$/;
            if (!Pat1.test(obj_value)) { return SetFocus('Please enter valid details',obj) }
            break;

   case 'ALPHANUMERICONLY':
            var Pat1 = /^[a-zA-Z0-9]*$/;
            if (!Pat1.test(obj_value)) { return SetFocus('Please enter valid details',obj) }
            break;

     // Numeric 0-9 NUMERIC|Min|Max
    case 'NUMERIC':
            var Pat1 = /^[0-9]*$/;
            if (!Pat1.test(obj_value)) {
               if(valset[3]=='olbp') return SetFocus('The details you have entered have not been recognised - please re-enter',obj);
               else return SetFocus('Please enter a valid number',obj);
               }
            // Perform Range Checking
            if (obj_params != '') {
             obj_value = parseFloat(obj_value)
             // Minimum
             if (obj_params[0]) {
                if (obj_value < obj_params[0]) { return SetFocus('Please enter a valid number',obj) }
             }
             // Maximum
             if (obj_params[1]) {
                if (obj_value > obj_params[1]) { return SetFocus('Please enter a valid number',obj) }
             }
            }
            break;

    // Email Address xxx@xxx.xxx
    case 'EMAIL':
          obj_value = obj_value.toLowerCase();
            obj.value = obj_value;
            Pat1 = /^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;
            if(obj_value != '')
            {
               if (!Pat1.test(obj_value)) {
                     return SetFocus( 'Please enter a valid email address \n\ne.g. name@place.com', obj)
               }
            }
            break;

    // Name Fields
    case 'NAME':
            var Pat1 = /^[-a-zA-Z\u00C0-\u00DD\u00E0-\u00FF \.\'\x27]*$/;
            apos = String.fromCharCode(39)
            if (!Pat1.test(obj_value)) {
               if(valset[3]=='olbp') return SetFocus('The details you have entered have not been recognised - please re-enter',obj);
               else
               {
                 return SetFocus('Please enter a valid name',obj);
               }
               }
             if (!Pat1.test(obj_value)||(stripChars(obj_value,"-.' ").length == 0 && valset[1] == 'Y')) {
                   return SetFocus('Please enter valid details' ,obj) }
            break;

    // Phone number
    case 'PHONE':
          obj.value = RemoveAllSpaces(obj.value)
            obj_value = obj.value
            var val = /(^-[0-9]+)|(^[0-9]*-$)|(^-[0-9]*-$)/;
            if(val.test(obj_value)){
               return SetFocus('Please enter a valid telephone number.', obj)
            }
            else {
               obj_value=stripChars(obj_value,"()- ");
               Pat1 = /^[0-9 ]*$/;
               if ((!Pat1.test(obj_value)) && (obj_value != '')) {
                  return SetFocus('Please enter a valid telephone number.', obj)
               }
            }
            obj.value = obj_value;
            break;

    // Credit/Debit Card number
    case 'CARDNO':
      var idIndex = (obj.name).indexOf("_cardNumber");
      var subString = (obj.name).substr(0,idIndex);
      var object = (subString)+ "_type";
        FieldObjCardType=document.getElementById(object);

      //alert(cardTypeValue[0])
         var cardType='';
         if(FieldObjCardType)
         {
             var cardTypeValue =(FieldObjCardType.value).split("|");
         cardType = cardTypeValue[0];
         }
        if(cardType=="AMERICAN_EXPRESS")
        {
            Pat1 = /^[0-9]{15}$/;
        }
        else
       {
            Pat1 = /^[0-9]{16,20}$/;
       }
       obj.value = RemoveAllSpaces(obj.value)
            obj_value = obj.value
          if (!Pat1.test(obj_value)) {
                  return SetFocus('Please enter a valid card number.',obj)
           }
            break;

    // Check to see if two values of 2 fields match
    case 'MATCH':
           obj_1 = document.getElementById(obj_params[0])
           obj_2 = document.getElementById(obj_params[1])
          if (obj_1.value != obj_2.value) return SetFocus(obj_name,obj_1)
            break;

    case 'EITHER_OR':
         obj_1 = document.getElementById(obj_params[0])
           obj_2 = document.getElementById(obj_params[1])
           if( obj_1.value == '' && obj_2.value == '' ) return SetFocus(obj_name, obj_1)
           break;
    case 'AND_OR':
           obj_1 = document.getElementById(obj_params[0])
           obj_2 = document.getElementById(obj_params[1])
           if( obj_1.value == '' ) return SetFocus(obj_name, obj_1)
           break;
     case 'AND':
         obj_1 = document.getElementById(obj_params[0])
           obj_2 = document.getElementById(obj_params[1])
           if( obj_1.value == '' || obj_2.value == '' ) return SetFocus(obj_name, obj_1)
           break;

      case 'RADIO_SELECTED':
            num = obj_params.length;
            selected = false;
            for (z=0;z<num;z++)
            {
               if( document.getElementById(obj_params[z]).checked )
               {
                  selected = true;
               }
            }
            if( selected == false ) return SetFocus(obj_name, document.getElementById(obj_params[num-1]))
          break;

    case 'DATE':
           day = getElemName(obj_params[0])[0].value
           month = getElemName(obj_params[1])[0].value
            year = getElemName(obj_params[2])[0].value
            datein = day + '/' + month +'/' + year
            var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;
            var matchArray = datein.match(datePat);
           if (matchArray == null) {
               return SetFocus(obj_name,obj)
           } else {
            if ((month==4 || month==6 || month==9 || month==11) && day==31) {
                  return SetFocus(obj_name,obj)
            }
            if (month == 2) {
                var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
                if (day>29 || (day==29 && !isleap)) {
                        return SetFocus(obj_name,obj)
                }
            }
           }
           break;
           // UK Postcode XX1 1XX
    case 'POSTCODE':
               obj_value = obj_value.toUpperCase();
          obj.value = obj_value;
               Pat1 = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
               if (!Pat1.test(obj_value)) {
                  return SetFocus('We cannot validate the post code you have entered. Please check and re-enter. ',obj)
           }
            break;

         // Credit/Debit Card Date CARDDATE|START or EXPIRY|Year Field Name|Reference Month|Reference Year
         case 'CARDDATE':
            cardmonth = obj.options[obj.options.selectedIndex].value
            obj_year = document.getElementById(obj_params[1])
            cardyear = obj_year.options[obj_year.options.selectedIndex].value
            var Calendar=new Date();
            todaysmonth =parseInt(Calendar.getMonth()+1,10);
            todaysyear = parseInt(Calendar.getFullYear(),10);
            if( obj_mandatory )
            {
               if (cardmonth == '') { return SetFocus('Please enter a valid date',obj) }
               if (cardyear == '') { return SetFocus('Please enter a valid date',obj) }
            }
            else if( (cardmonth == '' && cardyear != '') || (cardmonth != '' && cardyear == '') )
            {
               return SetFocus('Please enter a valid date',obj)
            }

            cardmonth = parseInt(cardmonth,10);
            cardyear = parseInt("20" + cardyear, 10);
            // If Start Date
            if (obj_params[0] == 'START') 
            {
               if (((cardmonth>todaysmonth) && (cardyear == todaysyear)) || (cardyear>todaysyear)) { return SetFocus('Please enter a valid start date',obj) }
            }
            // If Expiry Date
            if (obj_params[0] == 'EXPIRY')
            {
               if ((cardyear == todaysyear) || (cardyear<todaysyear))
               {
                  if ((cardmonth<todaysmonth))
                  {
                     return SetFocus('You have entered an invalid Expiry Date. Please check and try again.',obj)
                  }
               }
            }
         break;

   case 'LUHN':
          obj_1 = document.getElementById(obj_params[0])
         var cardnumber = document.getElementById(obj_params[0]).value
          var oddoreven = cardnumber.length & 1;
         var sum = 0;
         var addition = "";

         for (var count = 0; count < cardnumber.length; count++)
         {
            var digit = parseInt(cardnumber.substr(count,1));
            if (!((count & 1) ^ oddoreven))
            {
               digit *= 2;
               if (digit > 9)
               {
                  digit -= 9;
                  addition = addition + ' ' + digit;
               }
               else
               {
                  addition = addition + ' ' + digit;
               }
               sum += digit;
            }
            else
            {
               sum += digit;
               addition = addition + ' ' + digit;
            }
         }

         if (sum % 10 != 0) { return SetFocus('Please enter a valid card number.',obj_1) }
             break;

   case 'SECURITY':
       var cardType='';
       var securityCodeLength='';
       Pat1 = /^[0-9 ]*$/;
       var idIndex = (obj.name).indexOf("_securityCode");
       var subString = (obj.name).substr(0,idIndex);
       var object = (subString)+ "_securityCode";
       var indexValue = (object).split("_");
       cardType = document.getElementById("payment_type_"+indexValue[1]).value;
       cardTypeValue=cardType.split("|")[0];
       securityCodeLength=document.getElementById(cardTypeValue+"_securityCodeLength").value;
       if( (obj.value.length != securityCodeLength)  || (!Pat1.test(obj.value)))
       {
          return SetFocus('Please enter a valid security number (the last '+securityCodeLength+' digits in the signature strip on the reverse of your card)',obj)
       }
       break;

    // Review >> Check for < and >
    //ALT defined as >> 0-EmptyAlert|1-Y/N|2-inputtype|3-Minlength|4-MinlengthAlert|5-Maxlength|6-MaxlengthAlert
    case 'REVIEW':
            var Pat1 = /^[^<>]*$/;
            if (!Pat1.test(obj_value)) { return SetFocus('We cannot accept HTML or angle brackets in your review, please remove any HTML or \'<\' or \'>\' characters.',obj) }
            if(valset[3]!='') { //check for the minlength attribute
               if (obj_value.length<valset[3])
               {
                  if(valset[4]!='')
                     { return SetFocus(valset[4],obj);}
                  else
                     { return SetFocus('You have entered '+obj_value.length+' characters and have exceeded the '+valset[3]+' character limit',obj); }
               }
            }
            if(valset[5]!='') { //check for the maxlength attribute
               if (obj_value.length>valset[5])
               {
                  if(valset[6]!='')
                     { return SetFocus(valset[6],obj);}
                  else
                     { return SetFocus('This field has a minimum '+valset[5]+' character limit. You have entered only '+obj_value.length+' characters',obj); }
               }
            }
         break;
      } // switch
      } // if - mandatory
   } // if - Validation Required
   } // if alt tag exists
   if(obj.name.indexOf("issueNumber")>=0)
   {
     // alert("debitCardValidationNew");
      if(!debitCardValidationNew(obj.name))
     {
         return false;
     }

   }

  } // for
  // Change button style

  if (document.getElementById('paymentTrans'))
  {
        var totalAmountPaid = parseFloat((document.getElementById("totamtpaid").value).replace(poundsymbol, ""));
        var amountStillDue = parseFloat((document.getElementById("amtstilltxt").value).replace(poundsymbol, ""));
        if (parseFloat(totamtdue).toFixed(2) != totalAmountPaid && amountStillDue != 0)
        {
           alert('Amount Paid should match total amount.');
           return false;
        }
  }
 if(handlePayment())
{

    for (i=0;i<8; i++)
    {
       if ( document.getElementById("paymentType"+i))
       {
         document.getElementById("paymentType"+i).disabled=false;
       }
    }
  formobj.submit();
}
}


function debitCardValidationNew(issueNoname)
{
   var temp=issueNoname.split("_");
   tranIndex=temp[1];

   var issueNoObj=document.getElementsByName(issueNoname)[0];
   var startMonthObj=document.getElementsByName("payment_"+tranIndex+"_startMonth")[0];
   var startYearObj=document.getElementsByName("payment_"+tranIndex+"_startYear")[0];
  // TrimSpaces(issueNoObj.value);
   var paymentType=document.getElementById("payment_type_"+tranIndex).value;
   if(paymentType.indexOf("SOLO")>=0 || paymentType.indexOf("SWITCH")>=0&&(issueNoObj && startMonthObj && startYearObj))
   {
     if(issueNoObj.value=='')
    {
       SetFocus("Please enter the issue number of your card",issueNoObj);
         return false;
    }
     else //start else
     {
       if(issueNoObj.value!='')
      {
            var Pat1 = /^[0-9]*$/;
            if (!Pat1.test(issueNoObj.value))
          {
               SetFocus("Please enter valid issue number",issueNoObj);
               return false;
            }
       }
      else if((startMonthObj.selectedIndex>1 && startYearObj.selectedIndex>1))
      {
          cardmonth = startMonthObj.options[startMonthObj.options.selectedIndex].value
          cardyear = startYearObj.options[startYearObj.options.selectedIndex].value
        cardmonth = parseInt(cardmonth,10)
          cardyear = parseInt(cardyear,10)
          var Calendar=new Date();
          todaysmonth =Calendar.getMonth()+1;
          var calib=(ns4 || ns6)?1900:0;
          todaysyear = Calendar.getYear()+calib;

          if( (cardmonth == '' && cardyear != '') || (cardmonth != '' && cardyear == '') )
          {
             return SetFocus('Please enter a valid date',startMonthObj)
             return false;
          }
          // If Start Date
          else if (((cardmonth>todaysmonth) && (cardyear == todaysyear)) || (cardyear>todaysyear))
        {
            return SetFocus('Please enter a valid start date',startMonthObj)
            return false;
          }
      }
     }//end else
    }
   return true;
}

function ClearField(obj) {
if (obj.value != '') {
 obj.value = '';
 obj.select()
 obj.onfocus = null;
 }
}



//check whether atleast one  transaction is accepted
function handlePayment()
{
    var panelType=document.getElementById("panelType").value;

   if (document.getElementById('paymentTrans'))
   {
      var paymentFlag = false;
      var totalPayments = Number(document.getElementById('paymentTrans').value);
      for (var i = 0; i < totalPayments; i++)
      {
       if (document.getElementById('cardref'+i))
         {
           minLength = 4;
           var amexCheck = document.getElementById('payment_type_'+i).value.indexOf('AMERICAN_EXPRESS');
           if(amexCheck !=-1)
           {
              if (document.getElementById('cardref'+i) && document.getElementById('cardref'+i).value.length != 2)
             {
               SetFocus('Card reference no. should be of only 2 characters length.', document.getElementById('cardref'+i));
               return false;
             }
           }
           else
           {
             if (document.getElementById('cardref'+i) && document.getElementById('cardref'+i).value.length < 4)
             {
               SetFocus('Card reference no. should be more than 3 characters length.', document.getElementById('cardref'+i));
               return false;
             }
           }
          }
       }
      for (var i = 0; i < totalPayments; i++)
      {
         if (document.getElementById('chkbox'+i) && document.getElementById('chkbox'+i).checked)
         {
            paymentFlag = true;
            return true;
         }
          if((panelType=='CNP' || panelType=='CPNA' || panelType=='CP')&&document.getElementById('payment_type_'+i)&&
                                             document.getElementById('payment_type_'+i).selectedIndex>0)
          {
            paymentFlag = true;
            return true;
         }
      }
     alert("You need to accept atleast one payment transaction");
     return false;
   }

   return true;
}

//This function is used to validate the OLBP payment page
function checkOlbpPaymentAndValidate()
{
   ValidateForm(document.postPaymentFormBean);
}

//Strips the characters passed through the function.
function stripChars(s, bag)
{

  if(s == undefined)
  {
   return;
  }
   var i;
   var returnString = "";
   // Search through string's characters one by one.
   // If character is not in bag, append to returnString.
   for (i = 0; i < s.length; i++)
   {
      // Check that current character isn't whitespace.
      var c = s.charAt(i);
      if (bag.indexOf(c) == -1) returnString += c;
   }
   return returnString;
}

function CheckSeniorPassengerAndValidate()
{
      ValidateForm(document.paymentdetails);
}

// for the email and print functionality
 function setFlag(desc,val)
{
   if (desc=="printLocal")
   {
      document.getElementById("printLocally").value = val;
      document.getElementById("printCentrally").checked = false;
   }
   else
   {
      document.getElementById("printCentrally").value = val;
      document.getElementById("printLocally").checked = false;
   }
}

function setUpdateTrue()
{
   if (document.getElementById("updateFlag"))
   {
      document.getElementById("updateFlag").value = 'true';
   }
}
/**************************Util**************************/
// Set Focus to field and show message
function SetFocus(alertmess,obj1) {
alert(alertmess)
obj1.focus();
return;
}

// Remove all spaces in value
function RemoveAllSpaces(objval) {
   RESpace = /\s/ig;
  objval = objval.replace(RESpace, '');
  return objval
}

// Trim spaces from start and end of string
function TrimSpaces(objval) {
  // Remove Space at start
  RESpace = /^\s*/i;
  objval = objval.replace(RESpace, '');
  // Remove Space at end
  RESpace = /\s*$/i;
  objval = objval.replace(RESpace, '');
  return objval;
}