<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <%@include file="/common/commonTagLibs.jspf"%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ATCOMRES - Refund Page</title>
    <link rel="icon" href="https://pay.st2.thomsonprjuat.co.uk/cms-cps/atcomres/images/favicon.ico" type="image/x-icon" xmlns:myxsltextension="urn:XsltExtension">
    <link rel="shortcut icon" href="https://pay.st2.thomsonprjuat.co.uk/cms-cps/atcomres/images/favicon.ico" type="image/x-icon" xmlns:myxsltextension="urn:XsltExtension">
    <script src="/cms-cps/atcomres/js/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="/cms-cps/atcomres/js/refund.js" type="text/javascript"></script>
    <script src="/cms-cps/atcomres/js/popups.js" type="text/javascript"></script>
	<script src="/cms-cps/atcomres/js/placeholders.min.js" type="text/javascript"></script>
</head>

<body>
  <div class="pageContainer">
	<div class="rf">
		<div class="content-section">
			<!-- start lt- col -->
            <%@include file="refundSummeryPanel.jsp"%>
            <!-- end lt- col -->

        <!-- start rt- col -->
			<form  id="refundpayform" name ="SkySales" method="post" action="/cps/processPayment?b=15000&token=<c:out value='${param.token}' />&tomcat=<c:out value='${param.tomcat}' />">
				<div class="rt-col">
					<%@include file="refundTransactionPaymentDetails.jsp"%>
					<br>
					<div class="refundsection">
					<!--<h2>Refund</h2>
					<h3 class="sectionhead"> Cost to customer </h3>
					<p>Your total holiday cost is <span class="bld"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.transaction_total}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##"/></span></p>

					<p>Compensation <span class="bld"><c:out value="${bookingComponent.totalAmount.symbol}"/> <c:out value="${nonPaymentData.creditcard_charges}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##"/> (charges included)</span></p>

					<p>The Outstanding balance of your holiday is <span class="bld"><c:out value="${bookingComponent.totalAmount.symbol}"/><c:out value="${nonPaymentData.balance_amount}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##"/></span></p>

					<p>The full balance is due <span class="bld"><c:out value="${nonPaymentData.balance_due_date}" escapeXml="false"/>.</span></p>
					<p class="bld bluehiglight">Payment/Refund will include any outstanding deposits, charges, compensations and balance due.</p> -->
					<ul class="refundamt">
					<li class="bld">
						<label>Full Refund Amount: <c:out value="${bookingComponent.totalAmount.symbol}"/> <span id="fullRefundAmtId" class="amt">
						<fmt:formatNumber value="${bookingComponent.payableAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##"/></span></label>
						<input type="radio" id="fullRefundAmt"  name="refund" value="refundamount" checked>
					</li>
					<li class="bld partpayment">
						<label>Part Refund Amount: <span class="amt"></span></label>
						<input type="radio" id="partRefundAmt" name="refund" value="partrefundamount">
					</li>
					<li class="hide paypart" style="position:relative">
					<span class="currencySymbol">${bookingComponent.totalAmount.symbol}</span>
					<input id="partRefundAmtId" type="text" name="refund" value="partRefundAmtId" class="partamt">

					</li>
					</div>
					<!--Summary Panel Included-->
					<div class="summarypanel">
					<!--<ul class="paysummarylist">
						<li>
						<span class="detail bld">Refund Amount:</span>
						<span class="price bld"><c:out value="${bookingComponent.totalAmount.symbol}"/> <span class="amt"><c:out value="${nonPaymentData.holiday_cost}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##"/></span></span>
						</li>
					</ul>-->
					<h2 class="summaryhead">Summary</h2>
					<ul class="paysummarylist refundsummary">
					<li>
						<span class="detail">Maximum Amount Refundable:</span>
						<span class="price bld"><c:out value="${bookingComponent.totalAmount.symbol}"/> <span class="baseamt"><fmt:formatNumber value="${bookingComponent.payableAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##"/>
						</span></span>
					</li>
					<li>
						<span class="detail">Amount to be Refunded:</span>
						<span class="price bld"><c:out value="${bookingComponent.totalAmount.symbol}"/><span class="amt rfundamt"></span></span>
					</li>
					<li>
						<span class="detail">Outstanding Refundable Amount:</span>
						<span class="price bld"><c:out value="${bookingComponent.totalAmount.symbol}"/><span class="dueamt"></span></span>
					</li>
					</ul>
					</div>
			<!--END Of Summary Panel Included-->
			<!--Tems and Conditions Start-->
			<div class="term-conditions" >
					<div class="termscondition fl"  >
					<h4>Terms and Conditions</h4>
						<div class="content">
							<a href="javascript:void(0);" onclick="Popup('http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html',795,595,'location=0,status=0,left=100,top=50,toolbar=0,menubar=0,titlebar=0,resizable=1,scrollbars=1')">View Terms and Conditions</a>
							<p>
								<input type="checkbox" value="tandc" class="tandc">
								<label class="bld">I have read and accept the Terms and Conditions.</label>
							</p>
						</div>

					</div>
					<div class="buttonGroup buttonGroup-align fl">
					<span class="forwardButtonOuter"><span class="forwardButtonInner">
							<input type="submit"  id="refundpayment" value="Refund" class="primary" title="Refund">
							<input type="hidden"  id="refundPartAmount" value="" name="refundPartAmount"/>
							<input type="hidden"  id="payment_0_paymentmethod" value="CardRefund" name="payment_0_paymentmethod"/>
							<input type="hidden"  id="payment_0_transamt" value="${bookingComponent.payableAmount.amount}" name="payment_0_transamt"/>
							<input type="hidden"  id="stilDue_Amt" value="" name="payment_0_stillDueAmount"/>
						</span></span>
					</div>
					<div class="clear"></div>
			</div>
			<!--END OF Terms and Conditions-->
			<!--Logo and link -->
				<div class="logo-link">
					<div class="pageControls align">

					<a class="backPage" href="${bookingComponent.prePaymentUrl}">Back to booking</a>

					</div>
					<div class="footer units size1of4 lastUnitRefund">
						<!--<div class="hereToHelp">
						<ul>
							<li><a onclick="window.open('http://www.thomson.co.uk/shopfinder/shop-finder.html','name','height=745,width=820,toolbar=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no'); return false;" rel="nofollow" target="_blank" title="Shop finder" href="http://www.thomson.co.uk/shopfinder/shop-finder.html">Shop finder</a></li>
							<li><a rel="nofollow" title="Ask us a question" href="http://www.thomson.co.uk/editorial/faqs/flights/flight-faqs.html">Ask us a question</a></li>
							<li><a rel="nofollow" href="http://www.thomson.co.uk/editorial/legal/contact-us-flights.html">Contact us</a></li>
						</ul>
						<address>0871 231 4787</address>
						<p>Calls cost 10p per minute plus network extras</p>
						</div> -->
					<ul class="horizontal-logo  partner">
						<li><a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&amp;dn=www.thomson.co.uk&amp;lang=en" title="This Web site has chosen one or more VeriSign SSL Certificate or online payment solutions to improve the security of e-commerce and other confidential communication" class="verisignlogo" target="_blank"><span class="hide">Verisign Logo</span></a></li>
						<!--  <li><a href="http://www.abta.com/find-a-holiday/member-search/5736" title="Verify ABTA membership - ABTA Number V5126" class="abta" target="_blank"><span class="hide">The ABTA logo</span>ABTA</a></li>
						--> </ul>
					</div>
				</div>
			<!--END Of Logo and link -->
				</div>
           <!-- end rt- col -->

					<!-- start transactiondetails
						<div class="transactiondetails">

						</div>
					 end transactiondetails   -->
		    </form>
        </div>
    </div>
  </div>
</body>

</html>