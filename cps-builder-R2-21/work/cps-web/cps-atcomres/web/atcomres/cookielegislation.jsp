<%@include file="/common/commonTagLibs.jspf"%>
<%
    String tandcLink=com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.TermsofUse", "#");
	String privacyandpolicyLink=com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.Privacypolicy", "#");
	String stmtlink=com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.Statement", "#");
%>

<link rel="stylesheet" type="text/css" href="/cms-cps/atcomres/css/cookielegislation.css"/>
<script type="text/javascript">
function Popup(popURL,popW,popH,attr){
	   if (!popH) { popH = 450 }
	   if (!popW) { popW = 600 }
	   var winLeft = (screen.width-popW)/2;
	   var winTop = (screen.height-popH-30)/2;
	   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
	   popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
	   popupWin.close()


	      popupWin=window.open(popURL,"popupWindow",winProp);


	   popupWin.window.focus()
	}
function pnlReadOurTerms(){
var tomcatInstance= "<c:out value='${param.tomcat}' />";
	jQuery(document).ready(function(){
			jQuery.ajax({
			  type: "POST",
			   url: "<%=request.getContextPath()%>/CookieLegislationServlet?b=<c:out value='${param.b}' />&tomcat="+tomcatInstance,
				  success: function(response){
jQuery('.cookie_bg').slideUp('slow');
			  }
			});
		});



	}

</script>

<c:choose>
<c:when test="${requestScope.cookieswitch=='true'}">


<c:choose>
<c:when test="${requestScope.cookiepresent != 'true'}">

<div id="fhpiCookieLegislation">
<div class="cookie_bg" style="z-index:1000;">
		<div><strong>Please read our terms of use</strong></div>
		<div class="w98 fLeft txt90">
			<div class="txtSlideContent">
			By clicking OK or continuing to use our website, you consent to our use of cookies and our <a href="javascript:void(0);"  onclick="Popup('<%=tandcLink%>',465,450,'scrollbars=yes,resizable=yes');" class="lnkContent">Website Terms and Conditions</a>, <a href="javascript:void(0);"  onclick="Popup('<%=privacyandpolicyLink%>',465,450,'scrollbars=yes,resizable=yes');" class="lnkContent">Privacy Policy</a> and <a href="javascript:void(0);"  onclick="Popup('<%=stmtlink%>',465,450,'scrollbars=yes,resizable=yes');" class="lnkContent">Statement on Cookies</a>. Please leave our website if you don't agree.
			</div>

		<span class="ok_button" onclick="javascript:pnlReadOurTerms();"><img class="floatedRight" id="bookingcontinue"
	     src="/cms-cps/atcomres/images/fhpi_ok.gif" alt="OK" title="OK"/></span>
<br />
		</div>
</div>
</div>




</c:when>
<c:otherwise>

</c:otherwise>

 </c:choose>

</c:when>
<c:otherwise>

</c:otherwise>

</c:choose>

