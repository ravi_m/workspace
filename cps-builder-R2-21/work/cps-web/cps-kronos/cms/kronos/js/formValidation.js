

/*  Trim spaces from start and end of string   */
function trimSpaces(objval)
{
  // Remove Space at start
  RESpace = /^\s*/i;
  objval = objval.replace(RESpace, '');
  // Remove Space at end
  RESpace = /\s*$/i;
  objval = objval.replace(RESpace, '');
  return objval;
}
//Strips the characters passed through the function.
function stripChars(s, bag)
{
   var i;
   var returnString = "";
   // Search through string's characters one by one.
   // If character is not in bag, append to returnString.
   for (i = 0; i < s.length; i++)
   {
      // Check that current character isn't whitespace.
      var c = s.charAt(i);
      if (bag.indexOf(c) == -1) returnString += c;
   }
   return returnString;
}


/*This file contains functions related Validation of payment panel *
 */
/* Used to validate fields in payment methods */
var checkCardNumber=false;
var checkName=false;
var postCode=true;
var county=true;
var city=true;
var checkEmail=true;
var emailvalidationdata="";
var cardvalidationdata="";
var securitycodevalidationdata="";
var checkstartdate=true;
function validatePaymentFields(inputObj)
{

   thisObjType=inputObj.id.split("_")[2];

   //get unique method  identifier for this payment method
   var temp=(inputObj.id).split("_");
   var uniqueIdentifier=temp[0]+"_"+temp[1];
   var transVal=temp[1];
   var paymentType=document.getElementById("payment_type_0").value;

  if(thisObjType=='emailAddress'&&inputObj.value!='')
  {
     checkEmail=true;
       obj_value = inputObj.value.toLowerCase();
         Pat1 = /^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;
         if (!Pat1.test(obj_value))
         {
      checkEmail=false;
            return;
         }
     else
     {
      emailvalidationdata=	"";
         }
  }
  else if(thisObjType=='emailAddress' )
  {
     checkEmail=false;
    }

    // Card number (method: Card,CNP)
   if(thisObjType=='cardNumberId'&&inputObj.value!='')
   {
      checkCardNumber=true;
      if(paymentType.indexOf("AMERICAN_EXPRESS")>=0)
      {
         Pat1 = /^[0-9]{15}$/;
      }
      else
      {
            Pat1 = /^[0-9]{16,20}$/;
      }
      inputObj.value = removeAllSpaces(inputObj.value)
      obj_value = inputObj.value
      if (!Pat1.test(obj_value))
      {
            cardvalidationdata="Please enter a valid card number.";
      checkCardNumber=false;
      return;
      }
    else
    {
           cardvalidationdata="";
    }
   }
   else if(thisObjType=='cardNumberId')
        {
      checkCardNumber=false;
        }
    //Name on card (method: Card,CNP)
    if(thisObjType=='nameOnCardId'&&inputObj.value!='')
    {
      checkName=true;
            var Pat1 = /^[-a-zA-Z&@ \"\',\.\x27]*$/;
            if (!Pat1.test(inputObj.value))
      {
        checkName=false;
        return setFocus('Please enter a valid name.' , inputObj)
      }
    }

   //Security code (method: Card,CNP)
    if(thisObjType=='securityCodeId'&&inputObj.value!='')
    {
    checkCardSecurityCode=true;
        // method=document.getElementById("payment_"+transVal+"_paymenttypecode").value;
        // securityCodeLength=document.getElementById(method+"_securityCodeLength").value;
       if (paymentType!='')
       {
         var cardtype= paymentType.split("|")[0];
         var securityCodeLength=document.getElementById(cardtype+"_securityCodeLength").value
         Pat1 = /^[0-9 ]*$/;
         if(paymentType.indexOf("AMERICAN_EXPRESS")>=0 && inputObj.value.length != securityCodeLength)
         {
            return;
     }
         else if( (inputObj.value.length != securityCodeLength)  || (!Pat1.test(inputObj.value)))
         {
        return;
     }
       }
    } else  if(thisObjType=='securityCodeId' )
  {
       checkCardSecurityCode=false;
    }
  //Start date
   if(thisObjType=='startDateId'&&inputObj.value!='')
   {
       //ddmm
     var val=document.getElementById(inputObj.id).value;
     var datePattern=/^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[0-9]|2[0-9])$/;
     if(!val.match(datePattern))
     {
          return clearAndFocus('Please enter date in mmyy format only',inputObj)
     }
   }

    //Expiry date
   if(thisObjType=='expiryDateId'&&inputObj.value!='')
   {
      //ddmm
     var val=document.getElementById(inputObj.id).value;
     checkStartDate(expiryDateObj);
   }


}

var checkCardSecurityCode=false;
function validateSecurityCode(thisObj)
{
  if(cardChange)
  {
    if(thisObj.value.length<3 || thisObj.value.length>4)
    {
       checkCardSecurityCode=false;
      return;
  }
  else if(thisObj.value.length != securityCodeLength)
  {
     checkCardSecurityCode=false;
       return;
  }
   else
  {
     securitycodevalidationdata="";
     checkCardSecurityCode=true;
  }
     }
     else{
       checkCardSecurityCode=false;
     }
}

/* Remove all spaces in value */
function removeAllSpaces(objval)
{
   RESpace = /\s/ig;
  objval = objval.replace(RESpace, '');
  return objval
}

/* checks for expiry date */
 var expirychange=false ;

function updateExpiry(thisObj)
{
     var expiry = thisObj.value.split("|")[0];

     if(expiry.indexOf('MM') >=0)
      {
       expirychange=false;
      }
      else
    {
       expirychange=true;
      expirychange=checkExpiryDate();
      expiryyearchange=checkExpiryDate();
      }
 }

  var expiryyearchange=false ;
function updateExpiryYear(thisObj)
{
    var expiry = thisObj.value.split("|")[0];

     if(expiry.indexOf('YY') >=0)
      {
        expiryyearchange=false;
      }
      else
    {
       expiryyearchange=true;
      expiryyearchange=checkExpiryDate();
      expirychange=checkExpiryDate();
      }
 }

 /* check for Mobile validation */
     var dayTimePhone = true;
     var mobilePhone=true;
function validateMobile(objval)
{
      var mobVal = removeAllSpaces(objval);
      obj_value = mobVal;
    var val = /(^-[0-9]+)|(^[0-9]*-$)|(^-[0-9]*-$)/;
    validatemobile = true;
      if(val.test(obj_value))
      {
    return false;
      }
      else
      {
            obj_value=stripChars(obj_value,"()- ");
            Pat1 = /^[0-9 ]*$/;
            if ((!Pat1.test(obj_value)) && (obj_value != ''))
            {
                return false;
            }
      }

       return true;
}

 /* check for post code validation */
function validatePostCode()
{
      obj_value = document.getElementById("postCode").value.toUpperCase();
       var countryCode= document.getElementById("country").value;
        if(countryCode == 'GB')
    {
       Pat1 = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXYU0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
       if (!Pat1.test(obj_value))
       {
         postCode=false;
       }
    }
    else
     {
       Pat = /[^a-zA-Z0-9-,\.\\\\/ ]/;
       if (Pat.test(obj_value)&& trimSpaces(obj_value)!="")
       {
            postCode=false;
       }

     }
}
function validateleadPostCode()
{
      obj_value = document.getElementById("payment_0_postCode").value.toUpperCase();
       var countryCode= document.getElementById("country").value;
        if(countryCode == 'GB')
    {
       Pat1 = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXYU0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
       if (!Pat1.test(obj_value))
       {
         leadpostcode=false;
       }
    }
    else
     {
       Pat = /[^a-zA-Z0-9-,\.\\\\/ ]/;
       if (Pat.test(obj_value)&& trimSpaces(obj_value)!="")
       {
            leadpostcode=false;
       }

     }
}
function showpopInfo()
{
    document.getElementById("digit").style.display="block";
}

function hidepopInfo()
{
    document.getElementById("digit").style.display="none";
}
 var address1=true;
 var address2=true;
 var passleadaddress1=true;
 var passleadaddress2=true;
 var leadtown = true;
 var leadcounty = true;
 var leadpostcode = true;
 function validateAddress1(value)
 {
      var Pat1 = /^[-a-zA-Z0-9 ,\.\x27]*$/;
            apos = String.fromCharCode(39)
            if (!Pat1.test(value))
            {
              return false;
            }
           return validateMobile(value) ? false: true;
 }
function validateAddress2(value)
 {
        var Pat1 = /^[-a-zA-Z0-9 ,\.\x27]*$/;
            apos = String.fromCharCode(39)
            if (!Pat1.test(value))
            {
               return false;
            }
                  return validateMobile(value)? false: true;
 }

 function validationTestHasLetters( value ) {

  var hasLetter = false;

  // find a letter in the string and set boolean
    for( var i = 0; i < value.length; ++i ){
      if (/^[A-Za-z]*$/.test(value.charAt(i)))
         hasLetter = true;
   }

   return hasLetter;
}

// Validate town/city
function validationTestTownCity( value ) {
  return validationTestHasLetters( value) ? (/^[A-Za-z0-9 \/\-#.,;:]*$/.test( value )) : false;
}

// Validate country
function validationTestCountry( value ) {
  return validationTestHasLetters( value) ? (/^[A-Za-z0-9 \/\-#.,;:]*$/.test( value )) : false;
}


function validateNonMandatoryFields()
{
          city=true;
          if(document.getElementById("city") .value.length==0)
            {
            city=true;
        }
        else
        {
             city= validationTestTownCity(document.getElementById("city") .value);
        }
          county=true;
          if(document.getElementById("county") .value.length==0)
            {
            county=true;
        }
        else
        {
             county= validationTestTownCity(document.getElementById("county") .value);
        }

         postCode=true;
       if(document.getElementById("postCode") .value.length==0)
            {
            postCode=true;
        }
        else
        {
              validatePostCode();
        }

         if(document.getElementById("dayTimePhone") .value.length==0)
            {
              dayTimePhone = true;
            }
        else
        {
            dayTimePhone = validateMobile(document.getElementById("dayTimePhone") .value);
            }
         if(document.getElementById("mobilePhone") .value.length==0)
            {
              mobilePhone = true;
            }
        else
        {
            mobilePhone = validateMobile(document.getElementById("mobilePhone") .value);
            }
         address1=true;
        if(document.getElementById("addressLine1") .value.length==0)
            {
            address1=true;
        }
        else
        {
             address1= validateAddress1(document.getElementById("addressLine1") .value);
        }
		  passleadaddress1=true;
		   if(document.getElementById("payment_0_street_address1") .value.length==0)
            {
            passleadaddress1=false;
        }
        else
        {
             passleadaddress1= validateAddress1(document.getElementById("payment_0_street_address1") .value);
        }
		passleadaddress2=true;
		  if(document.getElementById("payment_0_street_address2") .value.length==0)
            {
            passleadaddress2=false;
        }
        else
        {
             passleadaddress2= validateAddress1(document.getElementById("payment_0_street_address2") .value);
        }

		leadtown = true;
		  if(document.getElementById("payment_0_street_address3") .value.length==0)
            {
            leadtown=false;
        }
        else
        {
             leadtown= validationTestTownCity(document.getElementById("payment_0_street_address3") .value);
        }
		leadcounty = true;
		 if(document.getElementById("payment_0_street_address4") .value.length==0)
            {
            leadcounty=false;
        }
        else
        {
             leadcounty= validationTestTownCity(document.getElementById("payment_0_street_address4") .value);
        }
		leadpostcode= true;
		if(document.getElementById("payment_0_postCode") .value.length==0)
         {
            leadpostcode=false;
        }
        else
        {
            validateleadPostCode();
        }
         address2=true;
        if(document.getElementById("addressLine2") .value.length==0)
            {
            address2=true;
        }
        else
        {
             address2= validateAddress2(document.getElementById("addressLine2") .value);
        }

}

function checkExpiryDate()
{
      var cardmonth = document.getElementById("payment_0_expiryMonthId").value;
     var cardyear = document.getElementById("payment_0_expiryYear") .value;
      if(cardmonth.indexOf('MM') ==0 && cardyear.indexOf('YY') ==0 )
      {
      return false;
      }
      else   if(cardmonth.indexOf('MM') ==0 || cardyear.indexOf('YY') ==0 )
    {
          return false;
    }
     var Calendar=new Date();
     todaysmonth =Calendar.getMonth()+1;
      todaysmonth	=parseInt(todaysmonth);
         todaysyear = Calendar.getYear();
         todaysyear=todaysyear+' ';
     todaysyear=todaysyear.substring(2);
         todaysyear=parseInt(todaysyear);

    if (((cardmonth<todaysmonth) && (cardyear == todaysyear)) || (cardyear<todaysyear))
            {
         return false;
            }
      return true;
  }

function autoCompleteLeadAddress()
{
	if(document.getElementById("autoCheckAddress").checked==true)
	{
	document.getElementById("payment_0_street_address1").value=document.getElementById("addressLine1").value;
    document.getElementById("payment_0_street_address2").value=document.getElementById("addressLine2").value;
	document.getElementById("payment_0_street_address3").value=document.getElementById("city").value;
	document.getElementById("payment_0_street_address4").value=document.getElementById("county").value;
	document.getElementById("payment_0_postCode").value=document.getElementById("postCode").value;
	}

}

/**This function is brand specific and changes the pay button based on the selected cards 3D scheme*/
function changePayButton()
{
	var cardType = document.getElementById('payment_type_0').value.split('|');
	var payButtonDescription = threeDCards.get(cardType[0]);
	var element =document.getElementById('changebutton');
	if (payButtonDescription == "mastercardgroup")
	{

		element.innerHTML = '<img class="floatedRight forewardButton" id="bookingcontinue" src="/cms-cps/kronos/images/proceed-to-payment.gif" alt="Proceed to payment" title="Proceed to payment"/>';
	}
	else if(payButtonDescription == "visagroup")
	{
		element.innerHTML = "<img class='floatedRight forewardButton' id='bookingcontinue' src='/cms-cps/kronos/images/proceed-to-payment.gif' alt='Proceed to payment' title='Proceed to payment'/>" ;
	}
	else
	{
		element.innerHTML = '<img class="floatedRight forewardButton" id="bookingcontinue" src="/cms-cps/kronos/images/blueContinueButton.png" alt="Book Extras now" title="Book Extras now"/>';
	}
}

function checkStartDate()
{
   var cardmonth = document.getElementById("payment_0_startMonth") .value;
   var cardyear = document.getElementById("payment_0_startYear") .value;
   if(cardmonth.indexOf('MM') ==0 && cardyear.indexOf('YY') ==0 )
   {
    checkstartdate= true;
    return;
   }
   else if(cardmonth.indexOf('MM') ==0 || cardyear.indexOf('YY') ==0 )
   {
       checkstartdate= false;
    return;
   }
   var Calendar=new Date();
   todaysmonth =Calendar.getMonth()+1;
   todaysmonth	=parseInt(todaysmonth);
   todaysyear = Calendar.getYear();
   todaysyear=todaysyear+' ';
   todaysyear=todaysyear.substring(2);
   todaysyear=parseInt(todaysyear);
   if (((cardmonth<=todaysmonth) && (cardyear == todaysyear)) || (cardyear<todaysyear))
   {
    checkstartdate =true;
   }
   else
   {
    checkstartdate= false;
   }

}