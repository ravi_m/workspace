var securityCodeLength;
var cardChange=false;

function setSecurityCode(thisObj)
{
	var cardType = thisObj.value.split("|")[0];
	if(cardType.indexOf('AMERICAN_EXPRESS') >=0)
    {
     securityCodeLength = 4;
	 cardChange=true;
    }
 	else
 	{
     securityCodeLength = 3;
	 cardChange=true;
 	}
    if(cardType.indexOf('Please Select') >=0)
 	{
   		cardChange=false;
 	}
}

 var showIssueNumber=false;
function setIssueField(thisObj)
{
	   var cardType = thisObj.value.split("|")[0];
	   if( cardType.indexOf('SWITCH') >=0 || cardType.indexOf('SOLO') >=0)
	   {
		  showIssueNumber = true;
		  document.getElementById("newcontactrowcardissue").style.display="block" ;
		  document.getElementById("newcontactcolcardissue").style.display="block" ;
	   }
	   else
	   {

		  document.getElementById("newcontactrowcardissue").style.display="none" ;
		  document.getElementById("newcontactcolcardissue").style.display="none" ;
	   }
	   document.getElementById("showlink").innerHTML="3 digit number on the back of your card" ;
}

var totCardCharge=0.0;
var issuekey= new Array();
var issuevalue= new Array();

/*
 * Gets the currency that is currently used for the page.
 */
function getCurrency()
{
  var currency = $("currencyText");

  if( currency )
  {
    currency = currency.value;
  }

  return currency;
}


  /*updating total cost */
function displayFieldsWithValuesForCard()
{
	$('total_trans_amt').innerHTML="<b>"+getCurrency()+" "+PaymentInfo.calculatedTotalAmount+"</b>";
	if(PaymentInfo.totalCardCharge > 0)
	{
		 	$('total_trans_amt').innerHTML+="&nbsp;&nbsp;<b>  (Includes "+getCurrency()+PaymentInfo.totalCardCharge +" booking fee)</b>";
	}

}

/* updating card charges*/

function updateCardChargeForKronos()
{
  if(PaymentInfo.totalAmount>0)
  {
	  handleCardSelection(0);
  }
}
