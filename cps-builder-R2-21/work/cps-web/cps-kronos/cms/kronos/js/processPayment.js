function GoToNextPage()
{
	  var placefocus=false;
	  var status=true;
	  var noneimageurl= '/cms-cps/kronos/images/none.png';
	  var errorimageurl='/cms-cps/kronos/images/errorSecond.png'
	  document.getElementById("newcontactrowcardcd").style.backgroundImage ="url("+noneimageurl+")";
	  document.getElementById("newcontactrowcardnum").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("newcontactrowcardexpiry").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("newcontactrowcardsecurity").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("newcontactrowemail").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("dayTimePhoneRownum").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("mobilePhoneRownum").style.backgroundImage ="url("+noneimageurl+")" ;
      document.getElementById("postCodeRownum").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("cityRownum").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("countyRownum").style.backgroundImage ="url("+noneimageurl+")" ;
      document.getElementById("address1Rownum").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("address2Rownum").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("address1leadpassRownum").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("address2leadpassRownum").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("cityleadRownum").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("countyleadRownum").style.backgroundImage ="url("+noneimageurl+")" ;
	  document.getElementById("postCodeleadRownum").style.backgroundImage ="url("+noneimageurl+")" ;


	  document.getElementById("bforeerrorcardnum").style.marginLeft="0px" ;
	  document.getElementById("bforeerrorcardexpiry").style.marginLeft="0px" ;
	  document.getElementById("bforeerrorcardsecurity").style.marginLeft="0px" ;
	  document.getElementById("bforeerrorcardtype").style.marginLeft="0px" ;
	  document.getElementById("bforeerroremail").style.marginLeft="0px" ;
	  document.getElementById("bforeerrormobile").style.marginLeft="0px" ;
	  document.getElementById("bforeerrordayTimePhone").style.marginLeft="0px" ;
	  document.getElementById("bforeerrorPostcode").style.marginLeft="0px" ;
	  document.getElementById("bforeerrorcounty").style.marginLeft="0px" ;
	  document.getElementById("bforeerrorcity").style.marginLeft="0px" ;
      document.getElementById("bforeerroraddress1").style.marginLeft="0px" ;
	  document.getElementById("bforeerroraddress2").style.marginLeft="0px" ;
	  document.getElementById("bforeerrorleadpassaddress1").style.marginLeft="0px" ;
	  document.getElementById("bforeerrorleadpassaddress2").style.marginLeft="0px" ;
	  document.getElementById("bforeerrorleadcity").style.marginLeft="0px" ;
	  document.getElementById("bforeleaderrorcounty").style.marginLeft="0px" ;
	   document.getElementById("bforeleaderrorPostcode").style.marginLeft="0px" ;


	  document.getElementById("newcontactrowcardcd").style.fontWeight="normal" ;
	  document.getElementById("newcontactrowemail").style.fontWeight="normal" ;
	  document.getElementById("newcontactrowcardnum").style.fontWeight="normal" ;
	  document.getElementById("newcontactrowcardexpiry").style.fontWeight="normal" ;
	  document.getElementById("mobilePhoneRownum").style.fontWeight="normal"  ;
	  document.getElementById("dayTimePhoneRownum").style.fontWeight="normal"  ;
	  document.getElementById("postCodeRownum").style.fontWeight="normal"  ;
	  document.getElementById("countyRownum").style.fontWeight="normal"  ;
	  document.getElementById("cityRownum").style.fontWeight="normal"  ;
	  document.getElementById("address1Rownum").style.fontWeight="normal"  ;
	  document.getElementById("address2Rownum").style.fontWeight="normal"  ;
	  document.getElementById("address1leadpassRownum").style.fontWeight="normal"  ;
	  document.getElementById("address2leadpassRownum").style.fontWeight="normal"  ;
	  document.getElementById("cityleadRownum").style.fontWeight="normal"  ;
	  document.getElementById("countyleadRownum").style.fontWeight="normal"  ;
	  document.getElementById("postCodeleadRownum").style.fontWeight="normal"  ;

	  validateNonMandatoryFields();
	  if(!address1)
	  {
		 document.getElementById("address1Rownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("address1Rownum").style.fontWeight="bold" ;
		 document.getElementById("bforeerroraddress1").style.marginLeft="75px" ;
		 document.getElementById("addressLine1").focus();
		 placefocus=true;
		 status=false;
		}
		  if(!address2)
	  {
		 document.getElementById("address2Rownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("address2Rownum").style.fontWeight="bold" ;
		 document.getElementById("bforeerroraddress2").style.marginLeft="75px" ;
		   if(!placefocus)
		 {
		 document.getElementById("addressLine2").focus();
		 placefocus=true;
		 status=false;
		}
	  }
	  if(!city)
	  {
		 document.getElementById("cityRownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("cityRownum").style.fontWeight="bold" ;
		 document.getElementById("bforeerrorcity").style.marginLeft="75px" ;
		   if(!placefocus)
		 {
		 document.getElementById("city").focus();
		 placefocus=true;
		 status=false;
		}
	  }
	  if(!county)
	  {
		 document.getElementById("countyRownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("countyRownum").style.fontWeight="bold" ;
	     document.getElementById("bforeerrorcounty").style.marginLeft="75px" ;
		 if(!placefocus)
		 {
			document.getElementById("county").focus();
    	 }
		 placefocus=true;
		 status=false;
	  }
	  if(!postCode)
	  {
		 document.getElementById("postCodeRownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("postCodeRownum").style.fontWeight="bold" ;
		 document.getElementById("bforeerrorPostcode").style.marginLeft="75px" ;
		 if(!placefocus)
		 {
		     document.getElementById("postCode").focus();
		 }
		 placefocus=true;
		 status=false;
	  }
	  if(!dayTimePhone)
	  {
		 document.getElementById("dayTimePhoneRownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("dayTimePhoneRownum").style.fontWeight="bold" ;
		 document.getElementById("bforeerrordayTimePhone").style.marginLeft="75px" ;
		 if(!placefocus)
		 {
			document.getElementById("dayTimePhone").focus();
		 }
		 placefocus=true;
		 status=false;
	  }
	  if(!mobilePhone)
	  {
		 document.getElementById("mobilePhoneRownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("mobilePhoneRownum").style.fontWeight="bold" ;
		 document.getElementById("bforeerrormobile").style.marginLeft="75px" ;
		 if(!placefocus)
		 {
		    document.getElementById("mobilePhone").focus();
		 }
	     placefocus=true;
		 status=false;
	  }
	  if(document.getElementById("contact_leadPassengerDetail_emailAddress") .value.length==0)
	  {
		 checkEmail=false;
	  }
	  if(!checkEmail)
	  {
    	 document.getElementById("newcontactrowemail").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("newcontactrowemail").style.fontWeight="bold" ;
		 document.getElementById("bforeerroremail").style.marginLeft="75px" ;
		 if(!placefocus)
		 {
		    document.getElementById("contact_leadPassengerDetail_emailAddress").focus();
		 }
		 placefocus=true;
		 status=false;
	  }
	  if(!cardChange)
	  {
		 document.getElementById("newcontactrowcardcd").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("newcontactrowcardcd").style.fontWeight="bold" ;
		 document.getElementById("bforeerrorcardtype").style.marginLeft="75px" ;
		 status=false;
	  }
	  if(!checkCardNumber)
	  {
	     document.getElementById("newcontactrowcardnum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("newcontactrowcardnum").style.fontWeight="bold" ;
		 document.getElementById("bforeerrorcardnum").style.marginLeft="75px" ;
		 if(!placefocus)
		 {
 		    document.getElementById("payment_0_cardNumberId").focus();
			placefocus=true;
		 }
     		status=false;
      }
	  if(!expirychange || !expiryyearchange)
	  {
		 document.getElementById("newcontactrowcardexpiry").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("newcontactrowcardexpiry").style.fontWeight="bold" ;
		 document.getElementById("bforeerrorcardexpiry").style.marginLeft="75px" ;
    	 status=false;
	  }
	  if(!checkCardSecurityCode)
	  {
	     document.getElementById("newcontactrowcardsecurity").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("newcontactrowcardsecurity").style.fontWeight="bold" ;
		 document.getElementById("bforeerrorcardsecurity").style.marginLeft="75px" ;
		 if(!placefocus)
		 {
			document.getElementById("payment_0_securityCode").focus();
		 }
         status=false;
      }

	  document.getElementById("showlink").innerHTML="3 digit number on the back of your card" ;

	  	  if(!passleadaddress1)
	  {
		 document.getElementById("address1leadpassRownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("address1leadpassRownum").style.fontWeight="bold" ;
		 document.getElementById("bforeerrorleadpassaddress1").style.marginLeft="75px" ;
		   if(!placefocus)
		 {
		 document.getElementById("payment_0_street_address1").focus();
		 placefocus=true;
		 status=false;
		}
	  }

	  if(!passleadaddress2)
	  {
		 document.getElementById("address2leadpassRownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("address2leadpassRownum").style.fontWeight="bold" ;
		 document.getElementById("bforeerrorleadpassaddress2").style.marginLeft="75px" ;
		   if(!placefocus)
		 {
		 document.getElementById("payment_0_street_address2").focus();
		 placefocus=true;
		 status=false;
		}
	  }

	   if(!leadtown)
	  {
		 document.getElementById("cityleadRownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("cityleadRownum").style.fontWeight="bold" ;
		 document.getElementById("bforeerrorleadcity").style.marginLeft="75px" ;
		   if(!placefocus)
		 {
		 document.getElementById("payment_0_street_address3").focus();
		 placefocus=true;
		 status=false;
		}
	  }
	     if(!leadcounty)
	  {
		 document.getElementById("countyleadRownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("countyleadRownum").style.fontWeight="bold" ;
		 document.getElementById("bforeleaderrorcounty").style.marginLeft="75px" ;
		   if(!placefocus)
		 {
		 document.getElementById("payment_0_street_address4").focus();
		 placefocus=true;
		 status=false;
		}
	  }

	   if(!leadpostcode)
	  {
		 document.getElementById("postCodeleadRownum").style.backgroundImage ="url("+errorimageurl+")" ;
		 document.getElementById("postCodeleadRownum").style.fontWeight="bold" ;
		 document.getElementById("bforeleaderrorPostcode").style.marginLeft="75px" ;
		   if(!placefocus)
		 {
		 document.getElementById("payment_0_postCode").focus();
		 placefocus=true;
		 status=false;
		}
	  }
 	  if(status == true)
  	  {
	 	 if(!document.getElementById("agreeTermAndCondition").checked)
		 {
	    	alert("Before we continue with your booking, Please confirm that you agree to our terms and conditions.");
		 }
		 else
		 {

		   document.getElementById("tourOperatorTermsAccepted").value = document.getElementById("agreeTermAndCondition").value;
   		   document.getElementById('emailAddress').value = document.getElementById('contact_leadPassengerDetail_emailAddress').value ;

		    document.getElementById("total_transamt").value=PaymentInfo.calculatedPayableAmount;
	        var tomcatInstance = document.getElementById("tomcatInstance").value;
		    var token = document.getElementById("token").value;
		    document.paymentdetails.method="post";
	        document.paymentdetails.action="/cps/processPayment?token="+token+"&b=14000"+"&tomcat="+tomcatInstance;
			 document.paymentdetails.submit();
	  	 }
      }
  	  if(status != true)
 	  {
 		 document.getElementById("errorlist").style.display="block" ;
 	  }
}

function toggleKronosOverlay()
{
  var overlayZIndex = 99;
  var zIndex = 100;
  var prevOverlay;
  var stickyOpened = false;
  jQuery("a.stickyOwner").click(function(e){
    var overlay = "#" + this.id + "Overlay";
	if (!stickyOpened)
	{
		prevOverlay = overlay;
	}
	if (prevOverlay != overlay)
	{
		jQuery(prevOverlay).hide();
		stickyOpened = false;
	}
	var pos = jQuery("#"+this.id).offset();
	var left = parseInt((1*pos.left-75),10);
	var top = parseInt((1*pos.top+50),10);
	jQuery(overlay).show();
	jQuery(overlay).css("left",left);
	jQuery(overlay).css("top",top);

	jQuery("#masterCardDetailsClose").html("&nbsp;");
	jQuery("#visaDetailsClose").html("&nbsp;");

	prevOverlay = overlay;
	stickyOpened = true;
	jQuery(overlay + ".genericOverlay").css("z-index",zIndex);
	zIndex++;

	if (jQuery(overlay).parent(".overlay") != null){
	  jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
	  overlayZIndex++;
	}
    return false;
  });

  jQuery("a.close").click(function(){
    var overlay = this.id.replace("Close","Overlay");
    jQuery("#" + overlay).hide();
    return false;
  });
}