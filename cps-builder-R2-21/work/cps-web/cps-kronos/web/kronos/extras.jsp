
<%@include file="/common/commonTagLibs.jspf"%>

<c:forEach var="extraFacilities"
	items="${bookingInfo.bookingComponent.extraFacilities}">
	<c:forEach var="subextraFacilities" items="${extraFacilities.value}">
		<div id="subextras">			&nbsp;
		</div>
		<div id="subextrascont"><b><c:out
			value='${subextraFacilities}' escapeXml='false' /></b>
		</div>
	</c:forEach>
</c:forEach>

