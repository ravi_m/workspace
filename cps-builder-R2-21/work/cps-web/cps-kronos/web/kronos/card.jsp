<%@include file="/common/commonTagLibs.jspf"%>

<div class="collapsibleHeadcard">
   <div id="contentcontactdetails"><b>Card details</b>
   </div>
</div>
<div id="cardcontentarea">
<c:if test="${bookingInfo.bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
<%
    String creditCardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("cardChargePercent", "");
    pageContext.setAttribute("creditCardChargeDetails", creditCardChargeDetails, PageContext.REQUEST_SCOPE);
%>
<c:set var="cardChargeDetails" value="${creditCardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<fmt:formatNumber value="${cardChargeArray[0]}" type="number" var="cardCharge" maxFractionDigits="1" minFractionDigits="0"/>
	<div class="newcontactrowcard">
				If you're paying with a Visa Credit or Mastercard credit card, a Booking Fee of
				<c:out value='${cardCharge}'/>% will be
				charged up to a maximum of � 60.
	</div>
	<div class="newcontactrowcard">
		<b>All other card types do not charge a booking fee.</b>
	</div>
	   </c:if>

	<div id="newcontactrowcardcd">
		<div id="bforeerrorcardtype" >
			Card type*
		</div>
	</div>

	<div id="newcontactcolcard">
		<select class='select'  id='payment_type_0'
			class='payment_type' name='payment_0_type'
			onChange="updateCardChargeForKronos(),setSecurityCode(this),setIssueField(this),changePayButton()"
			style="width: 185px;">
			<option value="Please Select">Please Select</option>
			<c:forEach var="paymentType"
				items="${bookingInfo.bookingComponent.paymentType['CNP']}">
				<option	value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'>
					<c:out value="${paymentType.paymentDescription}" escapeXml='false' />
				</option>
				<script type="text/javascript">
					 issuekey.push("<c:out value='${paymentType.paymentCode}' escapeXml='false'/>") ;
					 issuevalue.push("<c:out value="${paymentType.cardType.isIssueNumberRequired}" escapeXml='false'/>");
		   	    </script>
			</c:forEach>
		</select>
	</div>

	<div class="newcontactrowcardtotal">
		Total Cost
	</div>
   <div class="newcontactcolcardtotal">
   		<label id="total_trans_amt"><b>
 		<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" var="TotalAmount" type="number" maxFractionDigits="2" minFractionDigits="2"/>
   		<c:out value="${bookingInfo.bookingComponent.totalAmount.symbol}" escapeXml='false' />
   		${TotalAmount}</b></label>
   </div>

   <div id="newcontactrowcardnum">
		<div id="bforeerrorcardnum" >Card number*
		</div>
   </div>

   <div id="newcontactcolcardnum" id="payment_cardnumber">
   		<input  class='select' type="text"
			id='payment_0_cardNumberId' name='payment_0_cardNumber'
			style="width: 185px;" onblur='validatePaymentFields(this) '  autocomplete="off"/>
   </div>

   <div id="newcontactrowcardexpiry">
		<div id="bforeerrorcardexpiry" >
			Expiry date*
		</div>
   </div>
   <div id="newcontactcolcardexpiry">
   		<select class='select'  id='payment_0_expiryMonthId'
			name='payment_0_expiryMonth' style="width: 92px;" onChange="updateExpiry(this)">
			<option value="MM">MM</option>
			<option value="01">01</option>
			<option value="02">02</option>
			<option value="03">03</option>
			<option value="04">04</option>
			<option value="05">05</option>
			<option value="06">06</option>
			<option value="07">07</option>
			<option value="08">08</option>
			<option value="09">09</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
		</select>
		<select class='select'id='payment_0_expiryYear' name='payment_0_expiryYear'
			style="width: 92px;" onChange="updateExpiryYear(this)">
			<option value="YY">YY</option>
		  <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
			<option value='<c:out value='${expiryYear}'/>'><c:out
				value='${expiryYear}' /></option>
		  </c:forEach>
		</select>
   </div>

   <div id="newcontactrowcardissue" style="display:none">Issue number
   </div>
   <div id="newcontactcolcardissue" id="payment_0_issueNumber" style="display:none">
	    <input  class='select'  type='text' size='4'  maxlength="4"   name='payment_0_issueNumber'  value=""  onblur='validatePaymentFields(this) ' />   Please enter the issue number
			if available
   </div>

   <div id="newcontactrowcardsecurity">
	   <div id="bforeerrorcardsecurity" >
			Card security code*
		</div>
   </div>

   <div id="newcontactcolcardsecurity" >
   		<input type="text"  class='select'  id="payment_0_securityCode"	 autocomplete="off"
   			name ="payment_0_securityCode"  size ="4"  maxlength="4"  onblur='validateSecurityCode(this) '/>   <span id="showlink"
    		onmouseover="showpopInfo()";  onmouseout="hidepopInfo();">
	 		3 digit number on the back of your card </span><img src="/cms-cps/kronos/images/info-icon.gif"
   			onmouseover="showpopInfo()";  onmouseout="hidepopInfo();"  />
        <div id="digit">
		    <img src="/cms-cps/kronos/images/pop.gif"
                      style="position:absolute;margin-left:228px;margin-bottom:-74px;z-index:11;clear:both;"/>
	               <a href="javascript:void(0)" onmouseout="hidepopInfo();">
	        <div style="cursor: pointer; height:14px;left:445px;position:absolute;top:5px;width:35px;z-index:13;"> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	         </div></a>
 	    </div>
   </div>
   <div id="threeDsecureimages">
      <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
	     <c:if test="${threeDLogos == 'mastercardgroup'}">
            <c:set var="masterCardLogoText" value="true"/>
			<a class="stickyOwner" id="masterCardDetails" title="MasterCard SecureCode" href="javascript:void(0);">
               <img src="/cms-cps/kronos/images/mastercard.gif"/>
            </a>
	     </c:if>
      </c:forEach>
	  <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
         <c:if test="${threeDLogos == 'visagroup'}">
            <c:set var="visaLogoText" value="true"/>
	        <a id="visaDetails" class="stickyOwner" title="Verified by VISA" href="javascript:void(0);">
	           <img src="/cms-cps/kronos/images/visa.gif"/>
	        </a>
	     </c:if>
      </c:forEach>
   </div>
   <div id="learnMoreLogos">
      <c:if test="${masterCardLogoText}">

         <a id="masterCardDetails" href="javascript:void(0);" class="stickyOwner showlink learnMoreI" title="Learn more">Learn more</a>
         <%@ include file="/common/mastercardLearnMoreSticky.jspf"%>

      </c:if>
      <c:if test="${visaLogoText}">
         <a id="visaDetails" href="javascript:void(0);" class="stickyOwner showlink learnMoreI" title="Learn more">Learn more</a>
         <%@ include file="/common/visaLearnMoreSticky.jspf"%>
      </c:if>
   </div>
   <c:choose>
      <c:when test="${masterCardLogoText || visaLogoText}">
         <div class="mandatory">
      </c:when>
      <c:otherwise>
         <div class="newcontactrowcard">
      </c:otherwise>
   </c:choose>
   *Mandatory field and must be completed</div>
</div>

	<div class="collapsiblecardlast" style="clear:both;">
	</div>
   	<input type="hidden" name="tomcatInstance" id="tomcatInstance"
		value="<c:out value='${param.tomcat}' />" />
	<input type="hidden" name="token" id="token"
		value="<c:out value='${param.token}' />" />
	<input type="hidden" name="payment_0_nameOnCard"
		id="payment_0_nameOnCard" value="test"/>