<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css" href="/cms-cps/kronos/css/flexi-box/core.css" title="Thomson" media="screen" />
<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="/cms-cps/kronos/css/flexi-box/core-ie6.css" title="Thomson" media="screen" />
<![endif]-->


<link rel="stylesheet" type="text/css" media="screen" href="/cms-cps/kronos/css/newstyle.css">
<link rel="stylesheet" type="text/css" media="screen" href="/cms-cps/kronos/css/newie6.css">
<link rel="stylesheet" type="text/css" media="screen" href="/cms-cps/kronos/css/secure.css" />


<title></title>
<script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>

<style>
.toyBFFooterCopyright {margin-top: 50px !important;width: 949px !important;}
</style>
<%@include file="tag.jsp"%>
</head>
<body>
<div id="cookie_threed">
<jsp:include page="/kronos/cookielegislation.jsp" />
</div>
	<div id="pageContainer">

    <DIV id=header>
<STYLE>
</STYLE>

<SCRIPT type="text/javascript">
         function fixImagePNG(elemID)
         {
           var img = document.getElementById(elemID);
           if(! img.complete)
           {img.onload = fix_png;}
           else
           {fix_png.apply(img);}
         }

         function fix_png( )
         {
            var img = this;
            var span = document.createElement( "span" );
            if(img.id) span.id = img.id;
            if(img.className) span.className = img.className;
            if(img.onclick) span.onclick = img.onclick;
            if(img.onmouseover) span.onmouseover = img.onmouseover;
            if(img.onmouseout) span.onmouseout = img.onmouseout;
            if(img.onmouseup) span.onmouseup = img.onmouseup;
            if(img.onmousedown) span.onmousedown = img.onmousedown;
            span.title = img.title ? img.title : img.alt;
            span.style.width = img.width + "px";
            span.style.height = img.height + "px";
            span.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'" + img.src + "\', sizingMethod='scale'";
            img.id = null;
            img.parentNode.insertBefore(span,img);
            img.parentNode.removeChild(img);
         }
      </SCRIPT>

<DIV id=toyHeaderSection>

<DIV id=toyHeaderPanel><A title="Thomson logo" href="http://www.thomson.co.uk/"
alt="Thomson logo"><IMG id=toyHeaderLogo
src="/cms-cps/kronos/images/logo-thomson.gif"></A>
<DIV class=clearb></DIV>
<DIV id=toyHeaderNavPanelTop></DIV>
<DIV id=toyHeaderNavPanelSides>
<DIV id=toyHeaderNavPanelSidesPadder>
<UL id=toyHeaderNavPanel>
  <LI id=toyNavHolidays><A
  href="http://www.thomson.co.uk/package-holidays.html">Package holidays</A>
  </LI>
  <LI class=selected id=toyNavFlights><A
  href="http://flights.thomson.co.uk/en/index.html">Flights</A> </LI>
  <LI id=toyNavHotels><A href="http://www.thomson.co.uk/hotels.html">Hotels</A>
  </LI>
  
  <LI id=toyNavCruises><A
  href="http://www.thomson.co.uk/cruise.html">Cruises</A> </LI>
  <LI id=toyNavVillas><A href="http://www.thomson.co.uk/villas.html">Villas</A>
  </LI>
  <LI id=toyNavDeals><A href="http://www.thomson.co.uk/deals.html">Deals</A>
  </LI>
  <LI id=toyNavExtras><A
  href="http://www.thomson.co.uk/holiday-extras.html">Extras</A> </LI>
  <LI id=toyNavDests><A
  href="http://www.thomson.co.uk/holiday-destinations.html">Destinations</A>
  </LI></UL>
<DIV class=clearb></DIV></DIV></DIV>
<DIV id=toyHeaderNavPanelBottom></DIV>
<DIV class=clearb></DIV></DIV></DIV></DIV><!-- page headline -->
<!-- end page header -->


<h1>Select Your Seat and Flight Extras</h1><!-- cotent container -->


<div id="contentContainer">
<div id="step1" class="done">1. Your Booking</div>
<div id="step2" class="done">
<div id="step2content">2. Select your seat<br>
and flight extras</div>
</div>
<div id="step3" class="active">3. Payment</div>
<div id="step4">4. Confirmation</div>
<div class="clear"></div>




<div id="contentSection">
    <div class="titleSection">
      <h2>Nearly done...</h2>
    </div>
    <div id="secureForm">
      <div class="panel" id="iframeInclude">
        <div class="top">
          <div class="right"></div>
        </div>
        <div class="body">
          <div class="right">
            <div class="content">
              <div class="section">

                <iframe name="ACSframe" class="secureframe" frameborder="0" style="overflow:auto;"  src="common/bank3dwaiting.jsp?b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}'/>">&nbsp;</iframe>
              </div>
            </div>
          </div>
        </div>
        <div class="bottom">
          <div class="right"></div>
        </div>
      </div>
    </div>
    <!--end of secure form-->
    <div id="secureDetails">
      <h3>To complete your booking please verify your card</h3>
      <p class="sectionContent"> We'd like you to book online with us knowing your details are 100% secure. That's why we've introduced Verified by Visa and MasterCard SecureCode - security programmes from your card provider designed to give you even more peace of mind when paying online. </p>
      <h3>First time you've seen this?</h3>
      <p class="sectionContent"> Registering for Verified by Visa or MasterCard SecureCode is simple. Complete your card details in the box opposite. You can then set up a personal password to use for any future purchases you make with your card on participating websites - you only need to register once. We don't store any of your details - they're provided to your card issuer securely. </p>
      <h3>Already registered?</h3>
      <p class="sectionContent"> If you've already used Verified by Visa or MasterCard SecureCode, simply enter your personal password in the box on the right to continue your payment. </p>
      <p class="callInfo">If you need any help, please call us on <span class="number">0871 231 4793</span> (Calls cost 10p per minute plus network extras).</p>
    </div>
    <!--end secure details-->
    <div class="pageControls"><a class="backPage" href="<%=request.getAttribute("postPaymentUrl")%>" title="Back to payments"><img class="floatedLeft backwardButton" src="/cms-cps/kronos/images/back-to-payment.gif" title="Back to fligh extras" alt="Back to fligh extras"></a></div>
  </div>
  <!--end of ContentSection-->

<div class="clear"></div>

</div>
<div id="contentBottom"></div>

<jsp:include page="/kronos/footer.jsp" /><!-- end page footer -->

</div><!-- end: page container -->
<div class="clear"></div>
</body></html>