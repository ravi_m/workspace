<%@include file="/common/commonTagLibs.jspf"%>

<div class="collapsibleHeadcard">
   <div id="contentcontactdetails"><b>Cardholder address</b>
   </div>
</div>

<div id="cardcontentarea">

	<div class="newcontactrowcard">
			To help ensure your card details remain secure, please confirm the address of the cardholder.
If this is same as the lead passenger <br/>address,  you just need to check the box.
	</div>
	 	<div id="address1Rownum">
	<div id="bforeerroraddress1">
	</div>
	</div>
	<div id="address1Rowcol">
		<input type="checkbox" onclick="javascript:autoCompleteLeadAddress()" id="autoCheckAddress" name="autoCheckAddress"/>
   <span class="checkBoxLabel">Same address as lead passenger</span>
	</div>


	<div id="address1leadpassRownum">
	<div id="bforeerrorleadpassaddress1">Address1*
	</div>
	</div>
	<div id="address1leadpassRowcol">
		<input type="text" required="true" style="width: 185px;" class="select" value="" requirederror="Your city" id="payment_0_street_address1" name="payment_0_street_address1"/>
	</div>

	<div id="address2leadpassRownum">
	   <div id="bforeerrorleadpassaddress2">
	   Address2*
    	</div>
	</div>
	<div id="address2leadpassRowcol">
		<input type="text" required="true" style="width: 185px;" class="select" value="" requirederror="Your city" id="payment_0_street_address2" name="payment_0_street_address2"/>
	</div>

	<div id="cityleadRownum">
		<div id="bforeerrorleadcity">
			Town*
		</div>
	</div>
	<div id="cityleadRowcol">
		<input type="text" required="true" style="width: 185px;" maxlength="25" class="select" value="" requirederror="Your city " id="payment_0_street_address3" name="payment_0_street_address3"/>
	</div>

	<div id="countyleadRownum">
		<div id="bforeleaderrorcounty">
			County*
		</div>
	</div>
	<div id="countyleadRowcol">
		<input type="text" required="true" style="width: 185px;" maxlength="25" class="select" value="" requirederror="Your city " id="payment_0_street_address4" name="payment_0_street_address4"/>
	</div>

	<div id="postCodeleadRownum">
		<div id="bforeleaderrorPostcode">
			Postcode*
		 </div>
	</div>
	<div id="postCodeleadRowcol">
		<input type="text" requirederror="Your postal code" style="width: 75px;" maxlength="8" class="select" id="payment_0_postCode" value="" name="payment_0_postCode"/>
	</div>
	 <div class="newcontactrowcard">*Mandatory field and must be completed
   </div>



</div>

	<div class="collapsiblecardlast" style="clear:both;">
	</div>
