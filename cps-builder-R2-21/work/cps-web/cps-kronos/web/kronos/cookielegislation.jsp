<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" type="text/css" href="/cms-cps/kronos/css/cookielegislation.css">
<%
 String tandcLink=com.tui.uk.config.ConfReader.getConfEntry("KRONOS.TermsofUse", "#");
String privacyandpolicyLink=com.tui.uk.config.ConfReader.getConfEntry("KRONOS.Privacypolicy", "#");
String stmtlink=com.tui.uk.config.ConfReader.getConfEntry("KRONOS.Statement", "#");
%>

<script type="text/javascript">

	function Popup(popURL,popW,popH,attr){
	   if (!popH) { popH = 450 }
	   if (!popW) { popW = 600 }
	   var winLeft = (screen.width-popW)/2;
	   var winTop = (screen.height-popH-30)/2;
	   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
	   popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
	   popupWin.close()
  	   popupWin=window.open(popURL,"popupWindow",winProp);
	   popupWin.window.focus()
	}
	function pnlReadOurTerms(){
var tomcatInstance= "<c:out value='${param.tomcat}' />";
	jQuery(document).ready(function(){
			jQuery.ajax({
			  type: "POST",
			 url: "<%=request.getContextPath()%>/CookieLegislationServlet?b=<c:out value='${param.b}' />&tomcat="+tomcatInstance,
				  success: function(response){
jQuery('.cookie_bg').slideUp('slow');
			  }
			});
		});

		

	}

</script>



<div id="<c:out value='kronos'/>CookieLegislation">
<c:choose>
<c:when test="${requestScope.cookieswitch=='true'}">

<c:choose>
<c:when test="${requestScope.cookiepresent != 'true'}">
<div class="cookie_bg" style="z-index:1000;">

		<div><strong>Please read our terms of use</strong></div>
		<div class="w98 fLeft txt90">
			<div class="txtSlideContent">
			By clicking OK or continuing to use our website, you consent to our use of cookies and our
			<a href="javascript:void(0);"  onclick="Popup('<%=tandcLink%>',465,450,'scrollbars=yes,resizable=yes');" class="lnkContent">Website Terms and Conditions</a>,
			<a href="javascript:void(0);"  onclick="Popup('<%=privacyandpolicyLink%>',465,450,'scrollbars=yes,resizable=yes');" class="lnkContent">Privacy Policy</a> and
			<a href="javascript:void(0);"  onclick="Popup('<%=stmtlink%>',465,450,'scrollbars=yes,resizable=yes');" class="lnkContent">Statement on Cookies</a>.
			Please leave our website if you don't agree.
			</div>
			</div>

	 <a href="javascript:pnlReadOurTerms()">
	     <span class="ok_button"><img  src="/cms-cps/kronos/images/fhpi_ok.gif" alt="OK" title="OK"/></span>
	  </a>


</div>
</c:when>
<c:otherwise>

</c:otherwise>

 </c:choose>

</c:when>
<c:otherwise>

</c:otherwise>

</c:choose>
</div>
<br clear="all" />

