<%@include file="/common/commonTagLibs.jspf"%>
<DIV id=footer>
<STYLE>

</STYLE>

<DIV id=toyBFftr>
<DIV id=toyBFftrLeft>
<DIV class=toyBFexploreLinks>
<UL>
  <LI class=first><A title="Package holidays"
  href="http://www.thomson.co.uk/package-holidays.html" rel=nofollow>Package
  holidays</A> </LI>
  <LI><A title=Flights href="http://flights.thomson.co.uk/en/index.html"
  rel=nofollow>Flights</A> </LI>
  <LI><A title=Hotels href="http://www.thomson.co.uk/hotels.html"
  rel=nofollow>Hotels</A> </LI>
  <LI><A title=Cruises href="http://www.thomson.co.uk/cruise.html"
  rel=nofollow>Cruises</A> </LI>

  <LI><A title=Villas href="http://www.thomson.co.uk/villas.html"
  rel=nofollow>Villas</A> </LI>
  <LI><A title=Deals href="http://www.thomson.co.uk/deals.html"
  rel=nofollow>Deals</A> </LI>
  <LI><A title="Holiday extras"
  href="http://www.thomson.co.uk/holiday-extras.html" rel=nofollow>Holiday
  extras</A> </LI>
  <LI class=lastOne><A title="Holiday destinations"
  href="http://www.thomson.co.uk/holiday-destinations.html" rel=nofollow>Holiday
  destinations</A> </LI></UL>
<UL>
  <LI class=first><A title="About Thomson"
  href="http://www.thomson.co.uk/editorial/legal/about-thomson.html"
  rel=nofollow>About Thomson</A> </LI>
  <LI><A title=Press
  href="http://www.thomson.co.uk/editorial/press-centre/news-releases.html"
  rel=nofollow>Press</A> </LI>
  <LI><A title="Travel Jobs"
  href="http://www.thomson.co.uk/jobs/travel-jobs.html" rel=nofollow>Travel
  Jobs</A> </LI>
  <LI><A title=Affiliates
  href="http://www.thomson.co.uk/partners/thomson-affiliate-programme.html"
  rel=nofollow>Affiliates</A> </LI>
  <LI><A title=Sitemap
  href="http://www.thomson.co.uk/destinations/sitemap/site-map.html">Sitemap</A>
  </LI>
  <LI><A title="Register for offers"
  href="http://www.thomson.co.uk/editorial/ecrm/winaholiday.html"
  rel=nofollow>Register for offers</A> </LI>
  <LI class=lastOne><A title=RSS
  href="http://www.thomson.co.uk/editorial/deals/rss.html" rel=nofollow><IMG
  title=RSS height=15 alt=RSS
  src="/cms-cps/kronos/images/icon_rss.gif" width=32></A>
</LI></UL></DIV>

<DIV class=toyBFFooterCopyright>
<UL>
<jsp:useBean id='CurrDate5' class='java.util.Date'/>
<fmt:formatDate var ="currentYear" type="date" value="${CurrDate5}" pattern="yyyy"/>
  <LI class="first lastOne">&copy; <c:out value="${currentYear}" /> TUI UK </LI>
  <LI><A title="Terms &amp; Conditions"
  href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html"
  rel=nofollow>Terms &amp; Conditions</A> </LI>
  <LI><A title="Privacy &amp; Cookies Policy"
  href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html"
  rel=nofollow>Privacy &amp; Cookies Policy</A> </LI>
  <LI><A title="TUI Travel plc"
  href="http://www.tuitravelplc.com/">TUI Travel plc</A> </LI>
  <LI ><A title="Credit Card Fees"
  href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html"
  rel=nofollow>Credit Card Fees</A> </LI>
  <LI class=lastOne><A title="Statement on Cookies"
  href="http://www.thomson.co.uk/editorial/legal/statement-on-cookies-content.html"
  rel=nofollow>Statement on Cookies</A> </LI>

  <LI class="lastOne card">We accept: </LI>
  <LI class="lastOne cardtype"><IMG height=20 alt="Visa Logo"
  src="/cms-cps/kronos/images/logo_visa.gif" width=32><IMG
  height=20 alt="Delta Logo"
  src="/cms-cps/kronos/images/logo_delta.gif" width=32><IMG
  height=20 alt="MasterCard Logo"
  src="/cms-cps/kronos/images/icon_mastercard.gif"
  width=32><IMG height=20 alt="Maestro Logo"
  src="/cms-cps/kronos/images/icon_maestro.gif" width=33><IMG
  height=20 alt="Solo Logo"
  src="/cms-cps/kronos/images/icon_solo.gif" width=33><IMG
  height=20 alt="American Express Logo"
  src="/cms-cps/kronos/images/icon_americanexpress.gif"
  width=32> </LI></UL></DIV></DIV>
<DIV id=toyBFftrRight>
<DIV class=toyBFFooterHelp>
<UL>
  <LI><A title="Shop finder"
  onclick="window.open('http://www.thomson.co.uk/shopfinder/shop-finder.html','name','height=745,width=820,toolbar=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no'); return false;"
  href="http://www.thomson.co.uk/shopfinder/shop-finder.html"
  target=_new rel=nofollow>Shop finder</A>
  <LI><A title="Ask us a question"
  href="http://www.thomson.co.uk/editorial/faqs/faqs.html" rel=nofollow>Ask us a
  question</A>
  <LI><A href="http://www.thomson.co.uk/editorial/legal/contact-us.html"
  rel=nofollow>Contact us</A>
  <LI class=number>0871 231 4793 </LI></UL></DIV>
<DIV class=toyBFFooterPartners><A
href="http://seal.verisign.com/splash?form_file=fdf/splash.fdf&amp;dn=www.thomson.co.uk&amp;lang=en"><IMG
title="This Web site has chosen one or more VeriSign SSL Certificate or online payment solutions to improve the security of e-commerce and other confidential communication"
height=45 alt="The Verisign logo"
src="/cms-cps/kronos/images/logo_verisignNew.gif"
width=82></A><A onclick="openWindow(this); return false;"
href="http://www.travelmatch.co.uk/scripts/verify.hts?+V5126"><IMG class=abtaPos
title="Verify ABTA membership - ABTA Number V5126" alt="The ABTA logo"
src="/cms-cps/kronos/images/logo-abta_2009.gif"></A><A
onclick="openWindow(this); return false;"
href="http://www.caa.co.uk/applicationmodules/atol/atolverify.aspx?atolnumber=2524"><IMG
title="ATOL Protected - ATOL Number 2524. Click on the ATOL Logo if you want to know more"
alt="The ATOL logo"
src="/cms-cps/kronos/images/logo_atolNew.gif"></A>
<UL>
  <LI><A onclick="openWindow(this); return false;"
  href="http://www.travelmatch.co.uk/scripts/verify.hts?+V5126">ABTA</A></LI>
  <LI class=lastOne><A onclick="openWindow(this); return false;"
  href="http://www.caa.co.uk/applicationmodules/atol/atolverify.aspx?atolnumber=2524">ATOL</A></LI></UL></DIV></DIV></DIV></DIV></DIV><!-- end: page container -->
<DIV class=clear></DIV>
