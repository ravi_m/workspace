
<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
 	<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<version-tag:version/>

<link rel="stylesheet" type="text/css"
	href="/cms-cps/kronos/css/style.css">
<link rel="stylesheet" type="text/css"
	href="/cms-cps/kronos/css/ie6.css">
<!--[if IE 6]><style type="text/css"> div { behavior: url("/cms-cps/kronos/images/iepngfix.htc") }</style><![endif]-->
<!--[if gte IE 6]>
   <style>
      #contentContainer #step1,
      #contentContainer #step2,
      #contentContainer #step3,
      #contentContainer #step4 {height:45px;}
      #contentContainer #step1{width:202px;}
   </style>
<![endif]-->
<script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
<%@include file="tag.jsp"%>
</head>
<body>
<div id="cookie_payment">
	<jsp:include page="/kronos/cookielegislation.jsp" />
	</div>
	<jsp:include page="/kronos/header.jsp" />
<%-- end: page headline --%> <%-- cotent container --%>
<div id="contentContainer"><%-- order steps --%>
<div id="step1" class="done">1. Your Booking</div>
<div id="step2" class="done">
<div id="step2content">2. Select your seat<br />
and flight extras</div>
</div>
<div id="step3" class="active">3. Payment</div>
<div id="step4">4. Confirmation</div>
<div class="clear"></div>
<%-- end: order steps --%>
		<div id="sessionerror"><img src="/cms-cps/kronos/images/errorSecond1.png" />
		    <div id="sessionerrorcontent">

			<c:choose>
		    <c:when test="${not empty sessionScope=='true'}">
 			   <b>Sorry, we are currently experiencing technical difficulties. For further assistance please call us on 0871 231 4793. Alternatively, please try again later.	 </b>
		   </c:when>
		   <c:otherwise>
				<b>Sorry, your session has timed out. Please select your flight extras again or call 0871 231 4793
		and our team of experts will be pleased to assist you.	  <br/>Return to flight extras homepage	 </b>
		</c:otherwise>
   </c:choose>
		   </div>
		</div>


</div>
 <jsp:include page="/kronos/footer.jsp" />
 </form>
 </BODY></HTML>
