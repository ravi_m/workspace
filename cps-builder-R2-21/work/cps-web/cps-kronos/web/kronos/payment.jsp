 <%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<version-tag:version/>
<link rel="stylesheet" type="text/css"
	href="/cms-cps/kronos/css/style.css">
<link rel="stylesheet" type="text/css"
	href="/cms-cps/kronos/css/ie6.css">
<!--[if IE 6]><style type="text/css">
.genericOverlay .close{display:block;color:#FFFFFF;margin: -35px 0 0 265px;width:20px;text-decoration:none;}
.genericOverlay h3{width:200px;margin-top:0px;}
#learnMoreLogos{margin-left:571px;}
#threeDsecureimages{margin-left:570px;}
</style><![endif]-->
<style type="text/css"> div { behavior: url("/cms-cps/kronos/images/iepngfix.htc") }</style>
	<script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
<script type="text/javascript" src="/cms-cps/kronos/js/payment.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
<script type="text/javascript"
	src="/cms-cps/kronos/js/processPayment.js"></script>
<script type="text/javascript"
	src="/cms-cps/kronos/js/formValidation.js"></script>
	<script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
	<script type="text/javascript">

///Define a javascript object for using thru out the page
var  newHoliday  = "<c:out value='${bookingInfo.newHoliday}'/>";

 var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
 var token = "<c:out value='${param.token}' />" ;

var PaymentInfo = new Object();
PaymentInfo.totalAmount =${bookingInfo.bookingComponent.totalAmount.amount};
PaymentInfo.payableAmount = ${bookingInfo.bookingComponent.payableAmount.amount};
PaymentInfo.calculatedPayableAmount = ${bookingInfo.calculatedPayableAmount.amount};
PaymentInfo.calculatedTotalAmount = ${bookingInfo.calculatedTotalAmount.amount};
PaymentInfo.maxCardChargeAmount = null;

//Let us give default values
PaymentInfo.calculatedDiscount = 0.0;
PaymentInfo.chargePercent = 0.0;
PaymentInfo.selectedCardType = null;
var depositAmountsMap = new Map();
var cardChargeMap = new Map();
  cardChargeMap.put('PleaseSelect', 0);
     <c:forEach var="cardCharges" items="${bookingInfo.cardChargeMap}">
       cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');
     </c:forEach>

	   var threeDCards = new Map();
         <c:forEach var="threeDCards" items="${bookingInfo.payDescription}">
             threeDCards.put("<c:out value="${threeDCards.key}"/>","<c:out value="${threeDCards.value}"/>");
         </c:forEach>

   //Let us initialize

 if(PaymentInfo.maxCardChargeAmount=='')
 {
     PaymentInfo.maxCardChargeAmount = null  ;
 }
    //Payment constants used thru out the page
var payableConstants  = { FULL_COST                        : "fullCost",
		                  CARD_TYPE_ID                                : "payment_type_0",
		                  calculatePayableAmountClassName   :	 "calPayableAmount",
		                  TOTAL_CLASS                                 : "totalAmountClass",
                        DEFAULT_SECURITY_CODE_LEN    : 3,
                        DEFAULT_CARD_SELECTION          : ""
                        };
	window.onload= function(){ checkBookingButton("bookingcontinue");}
   jQuery(document).ready(function()
  {
      toggleKronosOverlay();
  });
  </script>


<title>Payment details</title>
<%@include file="tag.jsp"%>
</head>
<body>
<div id="cookie_payment">
<jsp:include page="/kronos/cookielegislation.jsp" />
</div>
	<jsp:include page="/kronos/header.jsp" />
<%-- end: page headline --%> <%-- cotent container --%>
<div id="contentContainer"><%-- order steps --%>
<div id="step1" class="done">1. Your Booking</div>
<div id="step2" class="done">
<div id="step2content">2. Select your seat<br />
and flight extras</div>
</div>
<div id="step3" class="active">3. Payment</div>
<div id="step4">4. Confirmation</div>
<div class="clear"></div>
<%-- end: order steps --%>
<h2>Payment</h2>
<form action="" name="paymentdetails"><input type="hidden"
	name="tomcatInstance" id="tomcatInstance" value="${param.tomcat}" /> <input
	type="hidden" name="token" id="token" value="${param.token}" /> <input
	type='hidden' id='panelType' value='CNP' /> <input type='hidden'
	id='totamtpaidWithoutCardCharge' name='payment_amtwithdisc'
	value="${bookingInfo.calculatedTotalAmount.amount}" />

	<div id="subHealine_payment">
<div id="subHealine_paymentextras">You are about to purchase the following flight extras, please check your details are correct, ensuring a valid email address has been provided and proceed with payment:	  </div>
<div id="extracontent"><jsp:include page="/kronos/extras.jsp" /></div>
<br />

<div id="errorlist"><c:if
	test="${not empty bookingInfo.bookingComponent.errorMessage}">

		<c:choose>
            <c:when test="${fn:containsIgnoreCase(bookingInfo.bookingComponent.errorMessage,
	                       'Cannot ful')}">
            		We have encountered a problem with your card.
					<br/>
					Please check the details and try again.
           </c:when>
           <c:otherwise>
        		<p>${bookingInfo.bookingComponent.errorMessage}</p>
           </c:otherwise>
         </c:choose>

	<script>
		document.getElementById("errorlist").style.display="block";
	 </script>
</c:if>

  <c:if test="${empty bookingInfo.bookingComponent.errorMessage}">
	<p>There has been an issue with your details, please check the form
	below:</p>
</c:if></div>
<jsp:include page="/kronos/contacts.jsp" /> <br />
<jsp:include page="/kronos/card.jsp" /> <br />
<jsp:include page="/kronos/leadpassenger.jsp" /> <br />
<jsp:include page="/kronos/t&c.jsp" /></div>
<%-- *******Essential fields ********************  --%>
      <input name="currencyText" id="currencyText" value='<c:out value="${bookingInfo.bookingComponent.totalAmount.symbol}"/>'
   type="hidden">
<input name="payment_0_chargeAmount" id="payment_0_chargeIdAmount" value="0"
	type="hidden">
	<input type="hidden" name="payment_totamtpaid" id="payment_totamtpaid"  value='<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>'/>
	<input type="hidden" name="payment_0_transamt"  id="payment_0_transamt"  value="0" />
	<input type="hidden" name="total_transamt" id="total_transamt" value="NA" />
	<input id="payment_0_paymenttypecode" value=""
	name="payment_0_paymenttypecode" type="hidden">

	<c:set var="backurl" value="${bookingInfo.bookingComponent.prePaymentUrl}"/>
	<a href="<c:out value='${backurl}'/>" >
	<img	class="floatedLeft backwardButton"
	src="/cms-cps/kronos/images/backToFlightExtrasButton.png" title="Back to fligh extras" alt="Back to fligh extras"/></a>

	<c:if test="${bookingInfo.newHoliday}">
	  <a href="javascript:GoToNextPage()">
	     <span id="changebutton"><img class="floatedRight forewardButton" id="bookingcontinue"
	     src="/cms-cps/kronos/images/blueContinueButton.png" alt="Book Extras now" title="Book Extras now"/></span>
	  </a>
	</c:if>

<div class="clear"></div>
<%-- end: page navigation buttons --%>
</div>
<div id="contentBottom"></div>
<%-- end: cotent container --%> <%-- footer --%>
 <jsp:include page="/kronos/footer.jsp" />
 <script type="text/javascript" src="/cms-cps/kronos/js/tracking.js"></script>

 </form>
 </BODY></HTML>