<DIV id=pageContainer>
<DIV id=header>
<STYLE>
</STYLE>

<SCRIPT type="text/javascript">
         function fixImagePNG(elemID)
         {
           var img = document.getElementById(elemID);
           if(! img.complete)
           {img.onload = fix_png;}
           else
           {fix_png.apply(img);}
         }

         function fix_png( )
         {
            var img = this;
            var span = document.createElement( "span" );
            if(img.id) span.id = img.id;
            if(img.className) span.className = img.className;
            if(img.onclick) span.onclick = img.onclick;
            if(img.onmouseover) span.onmouseover = img.onmouseover;
            if(img.onmouseout) span.onmouseout = img.onmouseout;
            if(img.onmouseup) span.onmouseup = img.onmouseup;
            if(img.onmousedown) span.onmousedown = img.onmousedown;
            span.title = img.title ? img.title : img.alt;
            span.style.width = img.width + "px";
            span.style.height = img.height + "px";
            span.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'" + img.src + "\', sizingMethod='scale'";
            img.id = null;
            img.parentNode.insertBefore(span,img);
            img.parentNode.removeChild(img);
         }
      </SCRIPT>

<DIV id=toyHeaderSection>
<DIV id=toyHeaderPanel><A title="Thomson logo" href="http://www.thomson.co.uk/"
alt="Thomson logo"><IMG id=toyHeaderLogo
src="/cms-cps/kronos/images/logo-thomson.gif"></A>
<DIV class=clearb></DIV>
<DIV id=toyHeaderNavPanelTop></DIV>
<DIV id=toyHeaderNavPanelSides>
<DIV id=toyHeaderNavPanelSidesPadder>
<UL id=toyHeaderNavPanel>
  <LI id=toyNavHolidays><A
  href="http://www.thomson.co.uk/package-holidays.html">Package holidays</A>
  </LI>
  <LI class=selected id=toyNavFlights><A
  href="http://flights.thomson.co.uk/en/index.html">Flights</A> </LI>
  <LI id=toyNavHotels><A href="http://www.thomson.co.uk/hotels.html">Hotels</A>
  </LI>
 
  <LI id=toyNavCruises><A
  href="http://www.thomson.co.uk/cruise.html">Cruises</A> </LI>
  <LI id=toyNavVillas><A href="http://www.thomson.co.uk/villas.html">Villas</A>
  </LI>
  <LI id=toyNavDeals><A href="http://www.thomson.co.uk/deals.html">Deals</A>
  </LI>
  <LI id=toyNavExtras><A
  href="http://www.thomson.co.uk/holiday-extras.html">Extras</A> </LI>
  <LI id=toyNavDests><A
  href="http://www.thomson.co.uk/holiday-destinations.html">Destinations</A>
  </LI></UL>
<DIV class=clearb></DIV></DIV></DIV>
<DIV id=toyHeaderNavPanelBottom></DIV>
<DIV class=clearb></DIV></DIV></DIV></DIV><!-- page headline -->
<H1>Select Your Seat and Flight Extras </H1><!-- end: page headline --><!-- cotent container -->
