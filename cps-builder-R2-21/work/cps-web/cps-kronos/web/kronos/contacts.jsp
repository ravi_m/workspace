
<%@include file="/common/commonTagLibs.jspf"%>

<div class="collapsibleHeadcontact">
	<div id="contentcontactdetails"><b>Contact details</b>
	</div>
</div>
<div id="cardcontentarea">
	<div id="contactdata">
		<div  id="titlehead"><b> ${bookingInfo.bookingComponent.nonPaymentData['personaldetails_0_title']}
 			${bookingInfo.bookingComponent.nonPaymentData['personaldetails_0_foreName']}
 			${bookingInfo.bookingComponent.nonPaymentData['personaldetails_0_surName']}
			</b>
		</div>
	</div>
	<div id="address1Rownum">
	<div id="bforeerroraddress1">Address1
	</div>
	</div>
	<div id="address1Rowcol">
		<input name="addressLine1" class='select'
		id="addressLine1" requirederror="Your city"
	 	value="<c:out value="${bookingInfo.bookingComponent.nonPaymentData['addressLine1']}"/>"
		class="large"  style="width: 185px;" required="true" type="text">
	</div>

	<div id="address2Rownum">
	   <div id="bforeerroraddress2">
	   Address2
    	</div>
	</div>
	<div id="address2Rowcol">
		<input name="addressLine2" class='select'
		id="addressLine2" requirederror="Your city"
	 	value="<c:out value="${bookingInfo.bookingComponent.nonPaymentData['addressLine2']}"/>"
		class="large"  style="width: 185px;" required="true"
		type="text" >
	</div>
	<div id="cityRownum">
		<div id="bforeerrorcity" >
			Town
		</div>
	</div>
	<div id="cityRowcol">
		<input class='select' name="city"
			id="city" requirederror="Your city "
			value="<c:out value="${bookingInfo.bookingComponent.nonPaymentData['city']}"/>"
			class="large" maxlength="25" style="width: 185px;" required="true"
			type="text">
	</div>
	<div id="countyRownum">
		<div id="bforeerrorcounty" >
			County
		</div>
	</div>
	<div id="cityRowcol">
		<input class='select' name="county"
			id="county" requirederror="Your city "
    		value="<c:out value="${bookingInfo.bookingComponent.nonPaymentData['county']}"/>"
			class="large" maxlength="25" style="width: 185px;" required="true"
			type="text">
	</div>

	<div id="postCodeRownum">
		<div id="bforeerrorPostcode" >
			Postcode
		 </div>
	</div>
	<div id="postCodeRowcol">
		<input class='select' name="postCode"
			value="<c:out value="${bookingInfo.bookingComponent.nonPaymentData['postCode']}"/>"
			id="postCode" class="small" maxlength="8"
			style="width: 75px;" requirederror="Your postal code" type="text">
	</div>

	<div class="newcontactrow">Country
	</div>
	<div class="newcontactcol">
		<select id='country' class='select'
			name='country' style="width: 185px;">
			<c:choose>
            	 <c:when test="${bookingInfo.bookingComponent.nonPaymentData['country'] != null && bookingComponent.nonPaymentData['country'] != '-1'}">
                	<c:set var="selectedCountry" value="${bookingInfo.bookingComponent.nonPaymentData['country']}" scope="page"/>
             	</c:when>
             	<c:otherwise>
                	<c:set var="selectedCountry" value="GB" scope="page"/>
             	</c:otherwise>
          	</c:choose>
			<c:forEach var="countryMap"
				items="${bookingInfo.bookingComponent.countryMap}">
				<option value="<c:out value='${countryMap.key}' escapeXml='false'/>"
				<c:if test="${countryMap.key == selectedCountry}">
				        selected
				</c:if>>
				<c:out value="${countryMap.value}" escapeXml='false' /></option>
			</c:forEach>
		</select>
	</div>


	<div id="dayTimePhoneRownum" >
		<div id="bforeerrordayTimePhone" >Contact telephone
		</div>
	</div>
	<div id="dayTimePhonecol" >
			<input class='select' name="dayTimePhone" id="dayTimePhone"
				value="<c:out value="${bookingInfo.bookingComponent.nonPaymentData['dayTimePhone']}"/>"
				required="true"
				id="leadPassengerDetail_phoneNumber"
	 			style="width: 185px;" type="text">
	</div>

	<div id="mobilePhoneRownum">
		<div id="bforeerrormobile">
			Mobile
		</div>
	</div>
	<div id="mobilePhoneRowcol">
		<input class='select' name="mobilePhone" id="mobilePhone"
			value="<c:out value="${bookingInfo.bookingComponent.nonPaymentData['mobilePhone']}"/>"
		    style="width: 185px;" type="text">
	</div>

	<div id="newcontactrowemail">
		<div id="bforeerroremail" >Email address*
		</div>
	</div>
	<div id="newcontactcolnewemail">
	<input id="emailAddress"
                type="hidden" name="emailAddress">

		<input class="select"
       		name="contact_leadPassengerDetail_emailAddress" id="contact_leadPassengerDetail_emailAddress"
	  		value="<c:out value="${bookingInfo.bookingComponent.nonPaymentData['contact_leadPassengerDetail_emailAddress']}"/>"
			id="contact_leadPassengerDetail_emailAddress"
			style="width: 185px;" type="text" onblur='validatePaymentFields(this)' />
	</div>

	<div class="newcontactrow">*Mandatory field and must be completed
	</div>
</div>
	<div class="collapsiblecardlast" style="clear:both;">
	</div>