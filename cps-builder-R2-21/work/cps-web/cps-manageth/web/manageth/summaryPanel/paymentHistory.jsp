<c:set var="paymentHistory" value="${bookingComponent.pricingDetails['PAYMENT_HISTORY_PRICE_COMPONENT']}" />
  <c:if test="${not empty paymentHistory}">
<div class="summaryAccord pay-history">
                            <h3 class="summaryAccordSwitch"><span class="icon history"></span><span class="title">PAYMENT HISTORY</span></h3>
                            <div class="summaryAccordCont pay-his-cont">
                                <div class="btm-dashed">
                                    <ul class="item-list">
                                    <c:forEach var="paymentoptions" items="${paymentHistory}">
                                        <li>
                                            <span class="history-head"><c:out value="${paymentoptions.itemDescription}" /></span>
                                         
                                             <span class="fr">&pound;
                                            <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" 
                                            value="${paymentoptions.amount.amount}" />
                                            </span>
                                            
                                            <div class="clear"></div>
                                        </li>

                                         </c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </div>
                         </c:if>
                   
                         
                         
                         
                         