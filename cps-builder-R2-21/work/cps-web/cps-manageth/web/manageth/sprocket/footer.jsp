<!-- <!doctype html> -->
<!--[if lt IE 7 ]> <html lang="en" id="firstchoice" class="ie6 "> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" id="firstchoice" class="ie7 "> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" id="firstchoice" class="ie8 "> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" id="firstchoice" class="ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="" id="firstchoice"> <!--<![endif]-->
<!-- <head>
	<title></title> -->
	<!--[if IE 7 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /><![endif]-->
	<!--[if IE 8 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /><![endif]-->
	<!--[if IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" /><![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><!--<![endif]-->
	<!-- <meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1"> -->


	<link rel="icon" type="image/png" href="/cms-cps/manageth/images/favicon.png" />

		<!--[if lte IE 9]>
			<link rel="shortcut icon" href="/cms-cps/manageth/images/favicon.ico" type="image/x-icon" />
			<link rel="icon" href="/cms-cps/manageth/images/favicon.ico" />
			<![endif]-->

			<!-- HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->

		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
			<!-- javascript -->
			<!-- <script type="text/javascript"  src="../js/TuiConfigProd.js"></script>
			<script type="text/javascript" src="../js/Bootstrap.js"></script>
			<link href="../css/main.css" rel="stylesheet" type="text/css" />
			<link href="../css/main_header.css" rel="stylesheet" type="text/css" />
			<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" /> -->
			<script>
			var ensLinkTrack = function(){};
			function displayHolidayTypes(ele){			
				var holiday_type = new Array();				
				holiday_type[0]="holidaytypes";
				holiday_type[1]="popDestinations";
				holiday_type[2]="longhaul";
				holiday_type[3]="shorthaul";
				holiday_type[4]="flightsTo";
				var get_holiday_type_length=holiday_type.length;				
				for(var i=0;i<get_holiday_type_length;i++){					
					document.getElementById(holiday_type[i]).style.display="none";
					document.getElementById(holiday_type[i]+"_li").className="";
				}					
				var split_type=ele.id.split("_");	
				document.getElementById(split_type[0]).style.display="block";	
				ele.className="active";
			}
			</script>
		<!-- </head>
		<body> -->
			<div id="wrapper">

				<div id="inner-footer">
					<ul id="footer-utils">
						<li>
							<h3>Holiday extras</h3>
							<ul>
								<li><a href="http://www.thomsonexcursions.co.uk/" target="_blank" class="ensLinkTrack">Excursions</a></li>
								<li><a href="http://www.carhiremarket.com/thomson/" target="_blank" class="ensLinkTrack">Car hire</a></li>
								<li><a href="http://www.thomson.co.uk/editorial/extras/foreign-exchange.html" target="_blank" class="ensLinkTrack">Foreign Exchange</a></li>
								<li><a href="http://www.holidayextras.co.uk/thomson/parking.html" target="_blank" class="ensLinkTrack">Airport parking</a></li>
								<li><a href="http://www.thomson.co.uk/editorial/extras/travel-money-card.html" target="_blank" class="ensLinkTrack">Money Card</a></li>
								<li><a href="http://www.thomsonins.co.uk/travel/default.aspx" target="_blank"class="ensLinkTrack">Travel insurance</a></li>
							</ul>
						</li>
						<li>
							<h3>Find a local store</h3>
							<p><a href="http://www.thomson.co.uk/shopfinder/shop-finder.html" target="_blank" class="ensLinkTrack">Shop Finder</a></p>
						</li>

						<li id="safe-hands">
							<h3>You're in safe hands</h3>
							<p>We're part of TUI Group - one of the world's leading travel companies. And all of our holidays are designed to help you Discover Your Smile.</p>
							<p class="authority">
								<!-- <img width="93" height="19" alt="World of TUI" src="/cms-cps/manageth/images/WOT-logo.png" class="wot-logo" /> -->
								<a class="abta ensLinkTrack" href="http://www.abta.com/find-a-holiday/member-search/5736" target="_blank">ABTA</a>
								<a class="atol ensLinkTrack" href="http://www.caa.co.uk/default.aspx?catid=27" target="_blank">ATOL</a>
							</p>
						</li>
						<li id="questions">
							<h3>Have a question?</h3>
							<form action="https://www.thomson.co.uk/gsa/gsa.html" method="get">
								<div class="formrow">
									<textarea  name="q" class="textfield" placeholder="e.g. Where do I print my e-tickets?"></textarea>
									<!-- <input type="hidden" name="site" value="firstchoice_collection" />
									<input type="hidden" name="client" value="fc-hugo-main" />
									<input type="hidden" name="proxystylesheet" value="fc-hugo-main" />
									<input type="hidden" name="output" value="xml_no_dtd" />
									<input type="hidden" name="submit" value="Go" /> -->
									
									    <input type="hidden" value="default_collection" name="site">
							            <input type="hidden" value="production_frontend" name="client">
							            <input type="hidden" value="production_frontend" name="proxystylesheet">
							            <input type="hidden" value="xml_no_dtd" name="output">
							            <input type="hidden" value="Go" name="submit">
            
            
								</div>
								<div class="floater">
									<button class="button fr mt4 small">Search</button>
									<!-- <p class="help fl"><a target="_blank" href="http://www.thomson.co.uk/destinations/faqCategories" class="ensLinkTrack">Ask a question</a></p> -->
									<p class="contact-us fl"><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/contact-us.html" class="ensLinkTrack">Contact us</a></p>
								</div>
							</form>
						</li>
					</ul>
					<!-- <script type="text/javascript">
					dojoConfig.addModuleName("tui/widget/Tabs");
					</script> -->

					<div id="footer-seo">
						<div class="tabs-container">
							<ul class="tabs">
								<li onclick="displayHolidayTypes(this);"  id="holidaytypes_li" class="active"><a enslinktrackattached="true"  class="ensLinkTrack" >Holiday Types</a><span class="arrow"></span></li>
								<li onclick="displayHolidayTypes(this);"  id="popDestinations_li"><a enslinktrackattached="true"  class="ensLinkTrack" >Popular Destinations</a><span class="arrow"></span></li>
								<li onclick="displayHolidayTypes(this);"  id="longhaul_li"><a enslinktrackattached="true" class="ensLinkTrack" >Mid/Long haul</a><span class="arrow"></span></li>
								<li onclick="displayHolidayTypes(this);"  id="shorthaul_li"><a enslinktrackattached="true"  class="ensLinkTrack" >Short haul</a><span class="arrow"></span></li>
								<li onclick="displayHolidayTypes(this);"  id="flightsTo_li"><a enslinktrackattached="true"  class="ensLinkTrack" >Flights To</a><span class="arrow"></span></li>
							</ul>
							<div id="holidaytypes" class="menu" style="display:block">
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/" class="ensLinkTrack" >Cheap Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/luxury-holidays.html" class="ensLinkTrack" >Luxury Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/editorial/features/sunshine-holidays.html" class="ensLinkTrack" >Sun Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/family-holidays.html" class="ensLinkTrack" >Family Holidays</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/deals/winter-2013-deals.html" class="ensLinkTrack" >Winter Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/all-inclusive-holidays.html" class="ensLinkTrack" >All inclusive holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/editorial/features/sunshine-holidays.html" class="ensLinkTrack" >Sunshine Holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/editorial/features/beach-holidays.html" class="ensLinkTrack" >Beach holidays</a></li>

								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/late-deals/late-deals.html" class="ensLinkTrack" >Late holiday deals</a></li>
									<li><a target="_blank" href="https://thomson.co.uk/holidays/" class="ensLinkTrack" >Holidays <script> document.write((new Date()).getFullYear()) </script></a></li>
									<li><a target="_blank" href="https://thomson.co.uk/destinations/deals/summer-2016-deals" class="ensLinkTrack" >Summer holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/late-deals/late-deals.html" class="ensLinkTrack" >Last minute Holidays</a></li>
								</ul>
							</div>
							<div id="popDestinations" class="menu" style="display:none">
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/tenerife/holidays-tenerife.html" class="ensLinkTrack" >Tenerife holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/costa-blanca/benidorm/holidays-benidorm.html" class="ensLinkTrack" >Benidorm holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/editorial/features/canary-islands-holidays.html" class="ensLinkTrack" >Canary Islands holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/lanzarote/holidays-lanzarote.html" class="ensLinkTrack" >Lanzarote holidays</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/africa/egypt/egypt-red-sea/sharm-el-sheikh/holidays-sharm-el-sheikh.html" class="ensLinkTrack" >Sharm el Sheikh holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/cyprus/holidays-cyprus.html" class="ensLinkTrack" >Cyprus holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/menorca/holidays-menorca.html" class="ensLinkTrack" >Menorca holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/majorca/holidays-majorca.html" class="ensLinkTrack" >Majorca holidays</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/greece/crete/holidays-crete.html" class="ensLinkTrack" >Crete holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/ibiza/holidays-ibiza.html" class="ensLinkTrack" >Ibiza holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/gran-canaria/holidays-gran-canaria.html" class="ensLinkTrack" >Gran Canaria holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/portugal/algarve/holidays-algarve.html" class="ensLinkTrack" >Algarve holidays</a></li>
								</ul>
							</div>
							<div id="longhaul" class="menu" style="display:none">
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/africa/egypt/holidays-egypt.html" class="ensLinkTrack" >Egypt holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/africa/cape-verde/holidays-cape-verde.html" class="ensLinkTrack" >Cape Verde holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/caribbean/mexico/holidays-mexico.html" class="ensLinkTrack" >Mexico holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/indian-ocean/india/goa/holidays-goa.html" class="ensLinkTrack" >Goa holidays</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/florida/holidays-florida.html" class="ensLinkTrack" >Caribbean holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/florida/holidays-florida.html" class="ensLinkTrack" >Florida holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/caribbean/jamaica/holidays-jamaica.html" class="ensLinkTrack" >Aruba holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/indian-ocean/mauritius/holidays-mauritius.html" class="ensLinkTrack" >Mauritius holidays</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/caribbean/dominican-republic/holidays-dominican-republic.html" class="ensLinkTrack" >Dominican Republic holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/caribbean/cuba/holidays-cuba.html" class="ensLinkTrack" >Cuba holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/africa/kenya/holidays-kenya.html" class="ensLinkTrack" >Kenya holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/caribbean/barbados/holidays-barbados.html" class="ensLinkTrack" >Barbados holidays</a></li>
								</ul>
							</div>
							<div id="shorthaul" class="menu" style="display:none">
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/africa/tunisia/holidays-tunisia.html" class="ensLinkTrack" >Tunisia holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/portugal/holidays-portugal.html" class="ensLinkTrack" >Portugal holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/spain/holidays-spain.html" class="ensLinkTrack" >Spain holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/turkey/holidays-turkey.html" class="ensLinkTrack" >Turkey holidays</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/cyprus/holidays-cyprus.html" class="ensLinkTrack" >Cyprus holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/africa/morocco/holidays-morocco.html" class="ensLinkTrack" >Morocco holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/croatia/holidays-croatia.html" class="ensLinkTrack" >Croatia holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/greece/holidays-greece.html" class="ensLinkTrack" >Greece holidays</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/malta/holidays-malta.html" class="ensLinkTrack" >Malta holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/bulgaria/holidays-bulgaria.html" class="ensLinkTrack" >Bulgaria holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/destinations/europe/italy/holidays-italy.html" class="ensLinkTrack" >Italy holidays</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/editorial/features/canary-islands-holidays.html" class="ensLinkTrack" >Canary Islands holidays</a></li>
								</ul>
							</div>
							<div id="flightsTo" class="menu" style="display:none">
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/spain/tenerife/tenerife-airport/tenerife-flights.html" class="ensLinkTrack" >Tenerife</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/cyprus/larnaca-region/larnaca-airport/larnaca-flights.html" class="ensLinkTrack" >Cyprus</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/spain/ibiza/ibiza-airport/ibiza-flights.html" class="ensLinkTrack" >Ibiza</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/turkey/turkey-bodrum/bodrum-airport/bodrum-flights.html" class="ensLinkTrack" >Bodrum</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/spain/menorca/menorca-flights.html" class="ensLinkTrack" >Menorca</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/the-americas/united-states-of-america/florida/orlando-sanford-airport/orlando-sanford-flights.html" class="ensLinkTrack" >Florida</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/spain/costa-blanca/alicante-airport/alicante-flights.html" class="ensLinkTrack" >Alicante</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/portugal/algarve/faro-airport/faro-flights.html" class="ensLinkTrack" >Faro</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/africa/morocco/morocco-marrakech/marrakech-airport/marrakech-flights.html" class="ensLinkTrack" >Marrakech</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/greece/kefalonia/kefalonia-airport/kefalonia-flights.html" class="ensLinkTrack" >Kefalonia</a></li>
								</ul>
								<ul>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/spain/costa-del-sol/malaga-airport/malaga-flights.html" class="ensLinkTrack" >Malaga</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/spain/lanzarote/lanzarote-flights.html" class="ensLinkTrack" >Lanzarote</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/spain/majorca/palma-airport/palma-flights.html" class="ensLinkTrack" >Majorca</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/europe/greece/corfu/corfu-airport/corfu-flights.html" class="ensLinkTrack" >Corfu</a></li>
									<li><a target="_blank" href="http://www.thomson.co.uk/flights/destinations/africa/egypt/egypt-red-sea/sharm-el-sheikh-airport/sharm-el-sheikh-flights.html" class="ensLinkTrack" >Sharm El Sheikh</a></li>
								</ul>
							</div>
						</div>
					</div></div>
				</div>
			<div id="footer">
				<ul >
					<li><a target="_blank" href="http://communicationcentre.thomson.co.uk/" class="ensLinkTrack">Communications Centre</a></li>
					<li><a target="_blank" href="http://www.tuitraveljobs.co.uk/" class="ensLinkTrack">Travel Jobs</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/partners/thomson-affiliate-programme.html" class="ensLinkTrack">Affiliates</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/mythomson/" class="ensLinkTrack">My Thomson</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/blog/" class="ensLinkTrack">Thomson Blog</a></li>
				</ul>
				<ul>
					<li>&copy; <script> document.write((new Date()).getFullYear()) </script> <a target="_blank" href="http://www.tuitravelplc.com/" class="ensLinkTrack">TUI Travel plc</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/about-thomson.html" class="ensLinkTrack">About Thomson</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html" class="ensLinkTrack">Terms &amp; Conditions</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/" class="ensLinkTrack">Accessibility</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html" class="ensLinkTrack">Privacy Policy</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/statement-on-cookies.html" class="ensLinkTrack">Statement on Cookies</a></li>
					<li><a target="_blank" href="http://www.thomson.co.uk/destinations/sitemap/site-map.html" class="ensLinkTrack">Site Map</a></li>
				</ul>
				<p style="text-align: left;"><span style="font-size: 8.5pt; font-family: TUIType, sans-serif; color: rgb(140, 140, 140);">Many of the flights and flight-inclusive holidays on this website are financially protected by the ATOL scheme. But ATOL protection does not apply to all holiday and travel services listed on this website. Please ask us to confirm what protection may apply to your booking. If you do not receive an ATOL Certificate then the booking will not be ATOL protected. If you do receive an ATOL Certificate but all the parts of your trip are not listed on it, those parts will not be ATOL protected. Please see our booking conditions for information or for more information about financial protection and the ATOL Certificate go to:<span class="apple-converted-space">&nbsp;</span></span><span style="font-family: TUIType, sans-serif;"><a target="_blank" href="http://www.atol.org.uk/ATOLCertificate"><span style="font-size: 8.5pt; color: rgb(102, 102, 102);">www.atol.org.uk/ATOLCertificate</span></a></span>&nbsp;</p>

<%-- 			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip</c:set>
 --%>			
 
 <!-- <script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
				 tui.analytics.page.pageUid = "paymentPage";
				 <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
					 <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
					 tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
				    </c:if>
			    </c:forEach>
				</script> -->
			</div>
		<!-- </body>
		</html> -->