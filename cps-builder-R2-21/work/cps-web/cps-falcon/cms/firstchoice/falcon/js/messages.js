/* Validation error messages to be displayed on payment page */
var errorMsg = new Object();

// Promotion Code
errorMsg.promoCodeBlank       = "Please redeem your promotional code before continuing with this booking.";

// Card Type
errorMsg.cardTypeBlank        = "Please select your card type from the list.";

// Card Number
errorMsg.cardNumberNonNumeric = "Please enter a valid card number.";
errorMsg.cardNumberMOD        = "It appears that your Card Type and Card Number don't match. Could you check your credit card details and try again.";
errorMsg.cardNumberBlank      = "Before continuing, we need your card number.";

// Valid From
errorMsg.validFromBlank       = "Please enter a valid from date.";
errorMsg.validFromInvalid     = "You have entered an incorrect 'valid from' date.";

// Expires
errorMsg.expiryBlank          = "Please enter a valid 'Expires End' date.";
errorMsg.expiryInvalid        = "You have entered an incorrect expiry date.";

// Name On Card
errorMsg.nameOnCardBlank      = "Please enter the name on the card.";
errorMsg.nameOnCardNoChars    = "Please enter a valid name on the card.";

// Issue Number
errorMsg.issueNumberBlank     = "Please enter the issue number found on your card.";

// Terms and Conditions
errorMsg.termsConditions      = "Before we continue with your booking, please confirm that you agree to our \nterms and conditions. ";

//Address Line 1
errorMsg.addressLine1         = "Please enter your address.";

//Address Line 1
errorMsg.addressLine2         = "Please enter your address.";

//Town/City
errorMsg.city                 = "Please enter your town or city.";

//County
errorMsg.county               = "Please enter your county.";

//Postcode
errorMsg.postCode             = "Please enter your postcode.";

//Country
errorMsg.country             = "Please enter your country.";
