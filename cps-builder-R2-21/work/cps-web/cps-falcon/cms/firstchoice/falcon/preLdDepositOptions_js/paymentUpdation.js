var indexForCCResponse;
var totCardCharge=0;
/*
 * Base Ajax update function used for all Ajax calls.
 *
 * @param servlet
 * @param params
 * @successCallBackFunction
 */
function performAjaxUpdate(servlet, params, successCallBackFunction)
{
  var url = "/cps/" + servlet + "?";

  if( params )
  {
    url = url + params + "&";
  }

  url = url + "token=" + token;
  url = uncache( url );
  url = url + tomcatInstance;

  var request = new Ajax.Request( url, {method:"GET", onSuccess:successCallBackFunction} );
}
/** Prevents browsers from caching requests
*/
function uncache(url)
{
   var d = new Date();
   var time = d.getTime();
   var newUrl;
   if (url.indexOf("?") != -1)
      newUrl = url + "&time="+time;
   else
      newUrl = url + "?time="+time;

   return newUrl;
}


/****************************PROMOTIONAL CODE AJAX CALL*****************************/
/*
 * This function calls promCodeServlet to calculate
 * Promotional code discounts and gets calculated amount
 * and discount amount.
*/
function updatePromotionalCode(promoCode, balanceType)
{
   var params = "promotionalCode=" + promoCode + "&balanceType=" + balanceType;

   performAjaxUpdate( "promCodeServlet", params, responseUpdatePromotionalCodeDiscount );
}

function responseUpdatePromotionalCodeDiscount(request)
{
  if ((request.readyState == 4) && (request.status == 200))
  {
    var temp = request.responseText;

    temp = temp.split("|");

    var promCodeDiscount   = temp[0];
    var totalAmount        = temp[1];
    var isPromoCodeApplied = $("isPromoCodeApplied");

    // if(promCodeDiscount != 'Promotional code discount not available.')
    if( parseFloat(promCodeDiscount) )
    {
       ///Hidden fields
       isPromoCodeApplied.value = "true";
       $('promoDiscount').value = promCodeDiscount;
       displayPromoCodeSuccessMessage(promCodeDiscount);
       updateAmountDetails(totalAmount,promCodeDiscount);
       displayPromoCodeInSummaryPanel(promCodeDiscount,"block");
    }
    else
    {
      //Hidden field
      isPromoCodeApplied.value = "false";
      ///Dispay error message
      displayPromoCodeErrorMessage(promCodeDiscount);
      displayPromoCodeInSummaryPanel(promCodeDiscount,"none");

      if( appConfig.disableRedeemBtn )
      {
        enableRedeemButton();
      }
      else
      {
        refreshPromoCodeField();
      }
    }
  }
  else
  {
    // Because the AJAX returned an error in the response we need to re-enable the redeem button.
    enableRedeemButton();
  }
}

/* Update total amount payable */
function updateAmountDetails(totalAmount,promCodeDiscount)
{
 /* var amtToUpdate = 1*$('totalCostingAmount').innerHTML-1*promCodeDiscount;
  amtToUpdate = parseFloat(amtToUpdate).toFixed(2);
  $('CheckoutPaymentTotalAmount').innerHTML = amtToUpdate;
  $('totalCostingAmount').innerHTML = amtToUpdate;
  if($('FullAmountPayable'))
  {
    $('FullAmountPayable').innerHTML = amtToUpdate;
  }
  //Call Throbber
  priceThrob();
 
  if(document.getElementById('payment_type_0').selectedIndex>0 )
  {
   // setTimeout("updateAllCardCharges(0)",500);
     updateAllCardCharges(0);
  }*/
  //Call Throbber
  priceThrob();
  
  PaymentInfo.calculatedDiscount = promCodeDiscount;
  
  var totalAmountMinusDiscount = parseFloat(1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount).toFixed(2);
  depositAmountsMap.put(payableConstants.FULL_COST ,totalAmountMinusDiscount);
  updatePaymentInfo();
  
  displayTheAmounts(payableConstants.TOTAL_AMOUNT_CLASS , totalAmountMinusDiscount);
  displayTheAmounts(payableConstants.PAYABLE_AMT_CLASS , PaymentInfo.calculatedPayableAmount);
  
  /**
   * After applying promotional code refresh the card charges in card type drop down(if applicable).
   */
  if( appConfig.updateCardCharges )
  {
    updateChargesInDropDown();
  }
  
}

function displayPromoCodeInSummaryPanel(promCodeDiscount,isDisplay)
{
  promCodeDiscount = parseFloat(promCodeDiscount).toFixed(2);

  var promoCodeCaption = $( "promoCodeCaption" );

  if( promoCodeCaption )
  {
    Element.addClassName( promoCodeCaption, "promoCodeCaption");

    promoCodeCaption.style.display = isDisplay;

    if(isDisplay == "block")
    {
      $('promocodeDiscountAmt').innerHTML = promCodeDiscount;
    }
  }
  else if(isDisplay == "block")
  {
    var pricePanelItems = $("pricePanelList");
    if($('displayPromoLi')==undefined)
	{
      var promoLi         = document.createElement('li');
    
      promoLi.setAttribute("id","displayPromoLi");
 
      prmoText = document.createTextNode( getCurrency() + promCodeDiscount + " promo code discount" );

      promoLi.appendChild(prmoText);

      var captionLi = document.createElement('li');

      captionLi.setAttribute("id","captionli");
      captionliText = document.createTextNode("and all taxes and charges");
      captionLi.appendChild(captionliText);

      pricePanelItems.removeChild($("captionli"));
      pricePanelItems.appendChild(promoLi);
      pricePanelItems.appendChild(captionLi);
	}
  }
}
/*Displays error message in payment page when promo code discount is not available
*
*/
function displayPromoCodeErrorMessage(promCodeError)
{
  //For Hugo etc
  var promoCodeFailed = $("promoCodeFailed");

  if( promoCodeFailed )
  {
    // Add message to error div and display (hiding other divs)
    $('failedMsg').innerHTML = promCodeError;
    promoCodeFailed.removeClassName( "hidden" );
    promoCodeFailed.show();
  }
}

/* Displays promotional code success message when promo code is successfully applied
 * Hides Promotional code section below success message(promotion code text field and redeem button)
*
*/
function displayPromoCodeSuccessMessage(promCodeDiscount)
{
  var promoCodeSuccess = $("promoCodeSuccess");

  if( promoCodeSuccess )
  {
    var success = "Thanks, a discount of " + getCurrency() + promCodeDiscount + " has been applied to your booking";

    $('successMsg').innerHTML = success;

    // Lets now show the success message
    promoCodeSuccess.removeClassName( "hidden" );
    // TODO: Not sure if we need this as I believe the above line should handle the showing.
    promoCodeSuccess.show();

    // Lets hide the failure container and also the promotional code validation container.
    $( "promoCodeFailed" ).hide();
   // $( "PromotionalCodeValidationContainer").innerHTML = "";
    //$( "PromotionalCodeValidationContainer" ).hide();
    $('promoInputs').innerHTML = "";
    $('promoInputs').hide();
    
    if($( "promoCodeFlag" ))
    {
      $( "promoCodeFlag" ).hide();
    }
  }
}
/****************************END OF PROMOTIONAL CODE AJAX CALL*****************************/

/* Check for the booking button 
 * Show or hide it as applicable 
 * */
function checkBookingButton()
{
   performAjaxUpdate( "ConfirmBookingServlet", null, responseShowConfirmBooking );
}

function responseShowConfirmBooking(request)
{
  if((request.readyState == 4) && (request.status == 200))
  {
     var result = request.responseText;
     var checkoutPaymentDetailsBookNowButton = $("CheckoutPaymentDetailsBookNowButton");
 
  }
}
/**********************************************************************************************************/


/*********************AJAX helpers ******************/

///////////////////////////////////functions related to Updating Payment panel //////////////////////////////////////////////




///////////////////////////////////End of functions related to Updating Payment panel //////////////////////////////////////////////

/* Get the depositType selected
*
*/
function getBalanceType()
{
  var selectDepositValue = getDepositSelectedValue();

  if( !selectDepositValue )
  {
    selectDepositValue = appConfig.defaultDepositType;
  }

  return selectDepositValue;
}
/**********************End of AJAX Helpers ********/

/**********************End of AJAX  related functions *********/

/*********************************************************************/

/*
 * Gets the currency that is currently used for the page.
 */
function getCurrency()
{
  var currency = $("currencyText");

  if( currency )
  {
    currency = currency.value;
  }

  return currency;
}

/**************************************************************************/
