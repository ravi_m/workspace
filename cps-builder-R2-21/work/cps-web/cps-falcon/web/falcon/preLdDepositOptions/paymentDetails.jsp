 <%@include file="/common/commonTagLibs.jspf"%>
<div id="paymentFields_required">
     <input type='hidden'  id='panelType' value='CNP'/>
	 <c:set var="cardCharges" value="false"/>
     <c:forEach var="cardCharge" items="${bookingComponent.cardChargesMap}">
           <c:out value='<input type="hidden" id="${cardCharge.key}_charge" value="${cardCharge.value} "/>' escapeXml='false'/>
		   <c:if test='${cardCharge.value > 0}'>
		      <c:set var="cardCharges" value="true"/>
		   </c:if>
     </c:forEach>

     <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
        <c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled' name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>
        <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>
        <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' name='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>
        <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>
        <c:out value="<input type='hidden' value='${paymentType.cardType.minSecurityCodeLength}'  id='${paymentType.paymentCode}_minSecurityCodeLength' name='${paymentType.paymentCode}_minSecurityCodeLength' />" escapeXml="false"/>
     </c:forEach>
</div>


 <div class="subheaderBar">
             <div class="subheaderBarSidesShadow">
                  <img src="/cms-cps/firstchoice/falcon/images/icons/header-payment-details-euro.gif" alt=""/>
                    <h2>Payment Details</h2>
             </div>
           </div>

          <div class="firstColSidesShadow">
		  <c:if test="${cardCharges == 'true'}">
          <p>
          We charge a fee on all payments made using a MasterCard and Visa credit card. You will not be charged for payments made on Visa Debit/Electron or Maestro debit cards.
          </p>
	      </c:if>

     <%-- ----------------- DEPOSIT section-------------------------------------------- --%>
     <c:set var="depositAvailable" value="false"/>
<c:forEach var="depositComponent" items="${bookingComponent.depositComponents}" varStatus="vardepost">
<c:set var="depositAvailable" value="true"/>
    <c:if test="${depositComponent.depositType=='DEPOSIT' || depositComponent.depositType=='deposit' }">
       <c:set var="depositType" value="${depositComponent.depositType} " scope="page"/>
        <c:set var="deposit" scope="page" value="${depositComponent.depositAmount.amount}"/>
        <fmt:formatNumber var="outstandingBalanceDeposit" value="${depositComponent.outstandingBalance.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##"/>
        <fmt:formatDate value="${depositComponent.depositDueDate}" var="depositDueDate" pattern="EE dd MMM yyyy"/>
    </c:if>
</c:forEach>
<c:set var="fullAmount" value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}" />
<c:choose>
<c:when test="${not empty bookingComponent.depositComponents}">

 <%--INTERIM DEPOSIT text change ---%>
  <c:choose>
    <c:when test="${bookingComponent.nonPaymentData['displayInterimLowDeposit'] == false}">
          <c:set var="depositTypeText">a deposit only</c:set>
          <c:set var="depositTypeCaption">
               The final balance of <c:out value="${currency}" escapeXml="false"/><c:out value="${outstandingBalanceDeposit}"/> is due by the <c:out value="${depositDueDate}"/>
          </c:set>
     </c:when>
     <c:otherwise>
           <c:set var="depositTypeText">Low deposit only</c:set>
          <c:set var="depositTypeCaption">
                <c:if test="${Irlandnorth=='true'}">
                  A deposit top up of �100 per person (excluding infants) is due in 8 weeks.
               </c:if>
               <c:if test="${Irlandnorth=='false'}">
                  A deposit top up of <c:out value="${currency}" escapeXml="false"/>80 per person (excluding infants) is due in 8 weeks.
              </c:if>
             <br/>
               The remaining balance is then due by <c:out value="${depositDueDate}"/>
          </c:set>
     </c:otherwise>
  </c:choose>
<%-- END of INTERIM DEPOSIT text chage  ---%>

<fieldset class="checkoutPaymentDetailsWishToPay">
              <p>I wish to pay</p>
              <ul>
             <li>
           <label for="radioDeposit">
             <input type="hidden" id="depositH" value="<fmt:formatNumber value="${deposit}"  type="number" maxFractionDigits="2"/>"/>
              <input type="hidden" id="payDeposit" name="payDepositOnly" value="true"/>
              <input type="radio" class="depositRadioOption" checked="checked" id="radioDeposit" name="depositType" value="<c:out value='${depositType}'/>"  />
               <c:out value="${depositTypeText}"/> (<span class="boldFont"><c:out value="${currency}" escapeXml="false"/></span><span id="DepositAmountPayable"><fmt:formatNumber value="${deposit}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#######"/></span>)&nbsp;<c:if test="${cardCharges == 'true'}">Credit card charge is <c:out value="${currency}" escapeXml="false"/><span class="creditCardChargeAmt">0.00</span> (if applicable).</c:if>
             <br/>
              <c:out value="${depositTypeCaption}" escapeXml="false"/>

           <%-- DEPOSIT HIDDEN FIELD - FOR JAVASCRIPT CALCULATIONS --%>
           <input id="DeposityHiddenField" type="hidden" value="<c:out value='${deposit}'/>"/>
          </label>
        </li>
        <li>
           <label for="radioFullAmount">
            <input type="hidden" id="totalHolidayCostH" value="<fmt:formatNumber value="${fullAmount}"  type="number" maxFractionDigits="2"/>"/>

             <input type="radio" class="depositRadioOption" id="radioFullAmount" name="depositType" <c:if test="${bookingComponent.nonPaymentData['depositType']=='fullCost'}">checked="checked"</c:if> value="fullCost" />
               the full amount (<span class="boldFont"><c:out value="${currency}" escapeXml="false"/></span><span id="FullAmountPayable" class="totalAmountClass"><fmt:formatNumber value="${fullAmount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##"/></span>)&nbsp;<c:if test="${cardCharges == 'true'}">Credit card charge is <c:out value="${currency}" escapeXml="false"/><span class="creditCardChargeAmt">0.00</span> (if applicable).</c:if>
           </label>
        </li>
          </ul>
          <script type="text/javascript">
              <!--
                   transactionAmounts['<c:out value="${depositType}" />'] = {depositAmount: <c:out value="${deposit}"/>};
                   transactionAmounts['fullCost'] = {depositAmount: <c:out value="${fullAmount}"/>};
              // -->
             </script>

<%--End of Deposit amount section --%>
             </fieldset>
 </c:when>
<c:otherwise>
   <input type="hidden" id="payDeposit" name="payDepositOnly" value="false"/>
   <input type="hidden" id="payDeposit" name="selectedDepositType" value="fullCost"/>
  <input type="hidden" id="totalHolidayCostH" value="<fmt:formatNumber value="${fullAmount}"  type="number" maxFractionDigits="2"/>"/>
</c:otherwise>
</c:choose>

          <div class="clearer"></div>

          <fieldset id="CheckoutPaymentCardDetailsFields">
            <div id="FieldCardTypeValidationContainer" class="validationContainerInactive">
              <a id="FieldCardTypeAnchor" name="FieldCardTypeAnchor"></a>
              <span id="FieldCardTypeValidationMessage" class="validationMessage">Please choose the type of card you wish to use.</span>
              <label for="FieldCardType">Card type:</label>
                  <select  id="payment_type_0"   name="payment_0_type" <c:if test="${requestScope.disablePaymentButton != 'true'}">class="cardSelector"</c:if>>
                      <option value="PleaseSelect" selected="selected" id="pleaseSelect">Please Select</option>
                      <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
                          <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>
                    </c:forEach>
                 </select>
    <%-- ACCEPTED CREDIT CARD LOGOS --%>
    <span class="checkoutPaymentCardTypeIcons">
    <c:choose>
      <c:when test="${Irlandnorth=='true'}">
         <img id="imgVisaCredit" src="/cms-cps/firstchoice/falcon/images/checkout/visa_card.gif" alt="We accept Visa" title="We accept Visa">
         <img id="imgVisaDebit" src="/cms-cps/firstchoice/falcon/images/checkout/delta_card.gif" alt="We accept Delta" title="We accept Delta">
         <img id="imgVisaElectron" src="/cms-cps/firstchoice/falcon/images/checkout/visaElectron_card.gif" alt="We accept Visa Electron" title="We accept Visa Electron">
         <img id="imgMasterCard" src="/cms-cps/firstchoice/falcon/images/checkout/mastercard_card.gif" alt="We accept MasterCard" title="We accept MasterCard">
         <img id="imgMaestro" src="/cms-cps/firstchoice/falcon/images/checkout/maestro_card.gif" alt="We accept Maestro" title="We accept Maestro">
      </c:when>
       <c:otherwise>
         <img id="imgVisaCredit" src="/cms-cps/firstchoice/falcon/images/checkout/visa_card.gif" alt="We accept Visa" title="We accept Visa">
         <img id="imgMasterCard" src="/cms-cps/firstchoice/falcon/images/checkout/mastercard_card.gif" alt="We accept MasterCard" title="We accept MasterCard">
         <img id="imgLaser" src="/cms-cps/firstchoice/falcon/images/checkout/laser_card.gif" alt="We accept Laser" title="We accept Laser">
       </c:otherwise>
    </c:choose>
    </span>
    <%--end of ACCEPTED CREDIT CARD LOGOS --%>
     <div class="clearer"></div>
  </div>


            <%-- AMOUNT PAYABLE - FORMATTED AND DISPLAYED --%>
         <div class="checkoutPaymentTotal">
           Total amount payable <c:if test="${bookingInfo.newHoliday == 'true' }"><span class="CheckoutPaymentTotalAmountCurrency"><c:out value="${currency}" escapeXml="false"/></span><span id="CheckoutPaymentTotalAmount" class="calPayableAmount"><c:choose><c:when
           test="${depositAvailable == true}"><fmt:formatNumber
           value="${deposit}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##"/>
                  </c:when><c:otherwise><fmt:formatNumber
                  value="${fullAmount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##"/>
                  </c:otherwise>
               </c:choose>
            </span></c:if><c:if test="${bookingInfo.newHoliday == 'false' && (bookingInfo.calculatedPayableAmount.amount != null &&  bookingInfo.calculatedPayableAmount.amount != 0)}"><span class="CheckoutPaymentTotalAmountCurrency"><c:out
            value="${currency}" escapeXml="false"/></span><span class="CheckoutPaymentTotalAmountCurrency" ><c:out value="${bookingInfo.calculatedPayableAmount.amount}"/> </span></c:if><span id="AmtPayableCaption" style="margin-left:10px;display:none;font-size:14px"></span>
           </div>

            <%-- CREDIT CARD NUMBER --%>
           <div id="FieldCardNumberValidationContainer" class="validationContainerInactive">
            <a id="CardNumberAnchor" name="CardNumberAnchor"></a>
            <span id="CardNumberValidationMessage" class="validationMessage">Card number:</span>
            <label for="FieldCardNumber">Card number</label>
            <input type="text" id="FieldCardNumber" maxlength="20"
                   name="payment_0_cardNumber" value="" autocomplete="off"/>
           </div>

           <%-- VALID FROM --%>
            <c:set var="startMonthSelected" value=""/>
            <c:set var="startYearSelected" value=""/>
           <div id="FieldValidFromValidationContainer" class="validationContainerInactive">
            <a id="ValidFromAnchor" name="ValidFromAnchor"></a>
            <span id="ValidFromValidationMessage" class="validationMessage">
            Please provide the month on which the card is shown to be "Valid From".
            </span>
            <label for="FieldValidFromMonth">Valid from <span class="accessibility">month</span></label>

           <%-- VALID FROM MONTH --%>
            <select id="FieldValidFromMonth" name="payment_0_startMonth">
               <%-- DEFAULT IS A BLANK FIELD. --%>
               <c:choose>
                  <c:when test="${startMonthSelected == '' }">
                  <option value="" selected="selected"></option>
                  </c:when>
                  <c:otherwise>
                  <option value="" ></option>
                  </c:otherwise>
               </c:choose>
               <%-- DYNAMICALLY GENERATE MONTHS --%>
               <c:set var="validFromMonth" value="01"/>

               <c:forEach begin="1" end="12" varStatus="loopStatus">
                 <fmt:formatNumber var="validFromMonth" value="${loopStatus.count}" type="number" minIntegerDigits="2" pattern="##"/>
             <c:choose>
                <c:when test="${startMonthSelected ==  validFromMonth}">
                <option value="<c:out value="${validFromMonth}"/>" selected="selected">
                <c:out value="${validFromMonth}"/>
                </option>
            </c:when>
            <c:otherwise>
                <option value="<c:out value="${validFromMonth}"/>"><c:out value="${validFromMonth}"/></option>
            </c:otherwise>
              </c:choose>
              </c:forEach>
            </select>

        <%-- VALID FROM YEAR --%>
            <label class="accessibility" for="FieldValidFromYear">Please provide the month on which the card is shown to expire.</label>

            <c:set var="validFromYear" value="01"/>
             <c:set var="validFromYearDisp" value="2001"/>
            <select id="FieldValidFromYear" name="payment_0_startYear">
              <%-- DEFAULT IS A BLANK FIELD. --%>
               <c:choose>
                  <c:when test="${startYearSelected == '' }">
                  <option value="" selected="selected"></option>
                  </c:when>
                  <c:otherwise>
                  <option value="" ></option>
                  </c:otherwise>
                  </c:choose>
              <%-- IF FORM BEAN HAS VALUE FOR THIS, MAKE THIS SELECTED. OTHERWISE, MAKE CURRENT YEAR SELECTED --%>
              <c:forEach var="startYear" items="${bookingInfo.startYearList}">
                  <option value='<c:out value='${startYear}'/>'><c:out value='${startYear}'/></option>
              </c:forEach>
            </select>
            (optional)
           </div>

         <%-- EXPIRY DATE --%>
            <c:set var="expiryMonthSelected" value=""/>
            <c:set var="expiryYearSelected" value=""/>
           <div id="FieldExpiryDateValidationContainer" class="expiry-date validationContainerInactive">
            <a id="ExpiryDateAnchor" name="ExpiryDateAnchor"></a>
            <span id="ExpiryDateValidationMessage" class="validationMessage">Please provide the month on which the card is shown to expire.</span>
            <label for="FieldExpiryDateMonth">Expires<span class="accessibility">month</span></label>

            <%-- EXPIRY MONTH --%>
            <select id="FieldExpiryDateMonth" name="payment_0_expiryMonth">
               <%-- DEFAULT IS A BLANK FIELD. --%>
               <c:choose>
                  <c:when test="${expiryMonthSelected == '' }">
                  <option value="" selected="selected"></option>
                  </c:when>
                  <c:otherwise>
                  <option value="" ></option>
                  </c:otherwise>
               </c:choose>

               <c:set var="expiryMonth" value="01"/>
               <c:forEach begin="1" end="12" varStatus="loopStatus">
                 <fmt:formatNumber var="expiryMonth" value="${loopStatus.count}" type="number" minIntegerDigits="2" pattern="##"/>
               <c:choose>
             <c:when test="${expiryMonthSelected ==  expiryMonth}">
             <option value="<c:out value="${expiryMonth}"/>" selected="selected"><c:out value="${expiryMonth}"/></option>
         </c:when>
         <c:otherwise>

             <option value="<c:out value="${expiryMonth}"/>"><c:out value="${expiryMonth}" /></option>
         </c:otherwise>
           </c:choose>
              </c:forEach>
            </select>

            <%-- EXPIRY YEAR --%>
            <label class="accessibility" for="FieldExpiryDateYear">
            Expires year</label>

            <select id="FieldExpiryDateYear" name="payment_0_expiryYear">
               <%-- DEFAULT IS A BLANK FIELD. --%>
               <c:choose>
                  <c:when test="${expiryYearSelected == '' }">
                  <option value="" selected="selected"></option>
                  </c:when>
                  <c:otherwise>
                  <option value="" ></option>
                  </c:otherwise>
               </c:choose>
                <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
                     <option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>
                </c:forEach>

            </select>
           </div>

        <%-- NAME ON CARD --%>
           <div id="FieldCardNameValidationContainer" class="validationContainerInactive">
            <a id="FieldCardNameAnchor" name="FieldCardNameAnchor"></a>
            <span id="FieldCardNameValidationMessage" class="validationMessage">Please provide the cardholder's name as it appears on the card.</span>
            <label for="FieldCardName">Name on card</label>
            <input type="text" id="FieldCardName" maxlength="80" name="payment_0_nameOnCard" value="" autocomplete="off"/>
           </div>
<div id="IssueNoSecurityCodeContainer" class="issueNoSecurityCodeContainer" >
        <%-- ISSUE NUMBER --%>
           <div id="FieldIssueNumberValidationContainer" class="validationContainerInactive hidden">
            <a id="FieldIssueNumberAnchor" name="FieldIssueNumberAnchor"></a>
            <span id="FieldIssueNumberValidationMessage" class="validationMessage">&nbsp;</span>
            <label id="labelFieldIssueNumber" for="FieldIssueNumber">Issue number</label>
            <input type="text" id="FieldIssueNumber" maxlength="2" name="payment_0_issueNumber" value="" autocomplete="off"/>
           </div>

         <%-- SECURITY DIGITS --%>
           <div id="FieldSecurityCodeValidationContainer" class="validationContainerInactive">
            <a id="FieldSecurityCodeAnchor" name="FieldSecurityCodeAnchor"></a>
            <span id="FieldSecurityCodeValidationMessage" class="validationMessage">The security number must be three digits.</span>
            <label id="LabelFieldSecurityCode" for="FieldSecurityCode">Security code</label>
            <%-- common js name - id='payment_0_securityCodeId'  name='payment_0_securityCode'  --%>
            <input type="text" id="FieldSecurityCode" maxlength="4" name='payment_0_securityCode'  value="" autocomplete="off"/>
            <a id="securityLink" href="#" class="hasPopup" onclick="return false;" onmouseover="securityInfoBlinkyShow(this);" onfocus="securityInfoBlinkyShow(this);" onmouseout="securityInfoBlinkyHide(); return false;" onblur="securityInfoBlinkyHide(); return false;">
                <span id="numSecurityDigits">The last 3 digits on the reverse of your card</span>
            </a>
          </div>
 </div><%-- End of IssueNoSecurityCodeContainer(Peekaboo)--%>
       </fieldset>
      <%----------------------------------------------------------------------------------------------------%>
      <c:if test="${is3DSecure == true}">
         <%------3D security images ------------%>
         <%@include file="/falcon/threeDSecureImages.jspf" %>
         <%--------End 3D security images ------%>
      </c:if>
 <%-- PAYMENT POLICY LINKS --%>
 <c:choose>
 <c:when test="${mastercardStickyLink == 'true' || visaStickyLink == 'true'}">
 <div class="threeDCheckoutPaymentPolicyLinks" >
   <div>
            <a rel="external" class="arrow-link" href="JavaScript:void(openPolicyWindow('<c:out value='${clientUrl}'/>/fcfalcon/com/tui/uk/view/common/travelinfo/privacyPolicy.jsp'))"  title="The following link opens in a new window"><img width="10" height="10" alt="The following link opens in a new window" src="/cms-cps/firstchoice/falcon/images/icons/link_new_window.gif"/> Our Privacy Policy</a>
   </div>
   <div>
           <a rel="external" class="arrow-link" href="JavaScript:void(openPolicyWindow('<c:out value='${clientUrl}'/>/fcfalcon/com/tui/uk/view/common/travelinfo/securityPolicy.jsp'))"  title="The following link opens in a new window"><img width="10" height="10" alt="The following link opens in a new window" src="/cms-cps/firstchoice/falcon/images/icons/link_new_window.gif"/> Our security policy</a>
   </div>
</div>
</c:when>
<c:otherwise>
<div class="checkoutPaymentPolicyLinks" >
   <div>
            <a rel="external" class="arrow-link" href="JavaScript:void(openPolicyWindow('<c:out value='${clientUrl}'/>/fcfalcon/com/tui/uk/view/common/travelinfo/privacyPolicy.jsp'))"  title="The following link opens in a new window"><img width="10" height="10" alt="The following link opens in a new window" src="/cms-cps/firstchoice/falcon/images/icons/link_new_window.gif"/> Our Privacy Policy</a>
   </div>
   <div>
           <a rel="external" class="arrow-link" href="JavaScript:void(openPolicyWindow('<c:out value='${clientUrl}'/>/fcfalcon/com/tui/uk/view/common/travelinfo/securityPolicy.jsp'))"  title="The following link opens in a new window"><img width="10" height="10" alt="The following link opens in a new window" src="/cms-cps/firstchoice/falcon/images/icons/link_new_window.gif"/> Our security policy</a>
   </div>
</div>
</c:otherwise>
</c:choose>

<%--END OF  PAYMENT POLICY LINKS --%>
   </div><!-- END firstColSidesShadow -->
 <div class="firstColBottomShadow"></div><!-- Added by N Ng on 22 July 2008 -->

<%-------------------------- CV2AVS block starts ---------------------------%>
  <div class="subheaderBar">
   <img src="/cms-cps/firstchoice/falcon/images/icons/header-postal-address.gif" alt=""/>
   <h2>Cardholder address</h2>
</div>
<div class="clearer"></div>
<div class="firstColSidesShadow">
<div class="confirmLeadPassengerAddress" id="confirmLeadPassengerAddressBlock" style="display:none">
<p>
    To help ensure your card details remain secure, please confirm the address of the cardholder.<br/>
    If this is the same as the lead passenger address, you just need to check the box.
</p>
    <input type="checkbox" id="confirmLeadPassengerAddress" name="confirmLeadPassengerAddress"/>
    <label for="confirmLeadPassengerAddress">Same as lead passenger</label>
</div>
<%-- Start of Lead passengr Address details --%>
<%@include file="/falcon/cardHolderAddress.jspf" %>
<%-- End of Lead passengr Address details --%>
</div><%-- END firstColSidesShadow --%>
<div class="firstColBottomShadow" style="margin-top:-14px;"></div>

 <%--Essential fields.   --%>
   <input type="hidden" id="payment_0_totalTrans" value="1" name="payment_totalTrans"/>
   <input type="hidden" name="total_transamt"  id="total_transamt"  value="NA" />

   <%-- *******End Essential fields ********************  --%>
<script language="JavaScript" type="text/javascript">
   window.onload = function()
   {
     setToDefaultSelection();
      initializeDepositSelection();
      setUpAppConfig( clientApp );

      //Make an ajax call to check for booking button
      checkBookingButton("CheckoutPaymentDetailsBookNowButton");

      initialiseDepositEvents();
      initialiseCreditCardTypeEvents();
      // initialisePromotionalCodeEvents();

      var options = $('payment_type_0').options;
      for(var i=0; i<options.length ; i++)
      {
        paymenttypeoptions.push(options[i].text);
      }
      updateChargesInDropDown();

      showMessage(errorFields,"<c:out value="${bookingComponent.errorMessage}"/>");

      throbberInitialise("SummaryTotalPriceThrobber");
      <c:if test="${cvvEnabled=='true'}">
         $("confirmLeadPassengerAddressBlock").style.display = "block";
      </c:if>
   }
   jQuery(document).ready(function()
   {
      //toggleOverlay();
      toggleOverlayFalcon();
      jQuery("#confirmLeadPassengerAddress").click(function(){fillAddressDetails();});
   });
</script>
