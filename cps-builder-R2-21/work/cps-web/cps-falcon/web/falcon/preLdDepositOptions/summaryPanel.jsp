<%@include file="/common/commonTagLibs.jspf"%>
<%--
   This is not ideal and has been flagged as to be reviewed with the Dev manager, as we
   should be looking at a longer term design and implementation for Summary panel logic in CPS.
 --%>
<c:set var="isDayTrip">
<c:choose>
  <c:when test="${bookingComponent.accommodationSummary.duration == 1}">
    <fmt:formatDate var="month" value="${bookingComponent.bookingData.bookingDate}" pattern="M" />
    <c:choose><c:when test="${month == 12}">true</c:when><c:otherwise>false</c:otherwise></c:choose></c:when>
  <c:otherwise>false</c:otherwise>
</c:choose></c:set>
 <div id="Summary">
   <div id="summaryFloatingPanel">
     <div class="summaryOverlay">
        <div class="summarySidesShadow">
         <div class="overlayWrapper">
           <div class="headerBlock">
              <h4><span class="padder">Your holiday details</span></h4>
          </div>
      <div class="overlayPadder">
        <div class="contentBlock">
          <%@ include file="summaryPanel/pricePanel.jspf" %>
          <%@ include file="summaryPanel/accomDetails.jspf" %>
          <%@ include file="summaryPanel/flightOptions.jspf"%>
          <%@ include file="summaryPanel/transportDetails.jspf" %>
          <%@ include file="summaryPanel/extras.jspf" %>
          <%@ include file="summaryPanel/passengerDetails.jspf" %>

           </div> <!-- contentBlock -->
        </div> <!-- overlayPadder -->
      </div> <!--end overlayWrapper-->
    </div> <!-- summarySidesShadow -->
  </div> <!-- SummaryOverlay -->
<div class="summaryBottomShadow">
   <div class="bottomDropBlock4"></div>
</div>
</div><!--End summaryFloatingPanel-->
</div><!--End Summary-->