 <%@include file="/common/commonTagLibs.jspf"%>
 <%@ page import="com.tui.uk.client.domain.BookingConstants" %>

  <c:set var="fullAmount" value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}" />

  <%@include file="paymentFields.jspf" %>

  <div class="subheaderBar">
    <div class="subheaderBarSidesShadow">
      <img src="/cms-cps/firstchoice/falcon/images/icons/header-payment-details-euro.gif" alt=""/>
      <h2>Payment Details</h2>
    </div>
  </div>

  <div class="firstColSidesShadow">
    <div id="TotalPriceContainer">
      Total holiday price:<span class="pricing"><c:out value="${currency}" escapeXml="false"/><span id="CheckoutPaymentTotalAmount" class="calPayableAmount"><fmt:formatNumber value="${fullAmount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##"/></span></span>
    </div>

    <%-----------------------------Promo details-------------------------------------%>
   <c:if test="${not empty bookingComponent.tracsBookMinusId}">
     <%@include file="promotionCode.jspf" %>
   </c:if>
   <div class="clearer"></div>
<%-----------------------------End of Promo details-------------------------------------%>

<%-- ----------------- DEPOSIT section-------------------------------------------- --%>
<c:set var="cardCharges" value="false"/>
<c:forEach var="cardCharge" items="${bookingComponent.cardChargesMap}">
   <c:if test='${cardCharge.value > 0}'>
      <c:set var="cardCharges" value="true"/>
   </c:if>
</c:forEach>
  <%@include file="depositSection.jspf" %>

<%-- ----------------- end of DEPOSIT section------------------------------------- --%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="cardChargeDetails" value="${bookingInfo.cardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<c:set var="cardCharge" value="${cardChargeArray[0]}" />
<c:set var="maxCardCharge" value="${cardChargeArray[2]}" />
<c:choose>
    <c:when test="${Irlandnorth == true}">
      <p><strong>There are no additional charges when paying by Maestro, MasterCard Debit or Visa/ Delta debit cards.</strong></p><br />
      <p id="depositChargeText">A fee of <fmt:formatNumber value="${cardCharge}" type="number" var="confCardCharge" maxFractionDigits="1" minFractionDigits="0"/><c:out value="${confCardCharge}"/>% applies to credit card payments, which is capped at �<c:out value="${maxCardCharge}" /> per transaction when using MasterCard Credit or Visa Credit cards.</p>
    </c:when>
    <c:otherwise></c:otherwise>
  </c:choose>

    <div class="clearer"></div>

    <%@include file="creditCardSection.jspf" %>

  <%------3D security images ------------%>
  <%@include file="threeDSecureImages.jspf" %>
  <%--------End 3D security images ------%>

    <%-- PAYMENT POLICY LINKS --%>
    <c:choose>
 <c:when test="${mastercardStickyLink == 'true' || visaStickyLink == 'true'}">
 <div class="threeDCheckoutPaymentPolicyLinks" >
   <div>
            <a rel="external" class="arrow-link" href="JavaScript:void(openPolicyWindow('<c:out value='${clientUrl}'/>/fcfalcon/com/tui/uk/view/common/travelinfo/privacyPolicy.jsp'))"  title="The following link opens in a new window"><img width="10" height="10" alt="The following link opens in a new window" src="/cms-cps/firstchoice/falcon/images/icons/link_new_window.gif"/> Our Privacy Policy</a>
   </div>
   <div>
           <a rel="external" class="arrow-link" href="JavaScript:void(openPolicyWindow('<c:out value='${clientUrl}'/>/fcfalcon/com/tui/uk/view/common/travelinfo/securityPolicy.jsp'))"  title="The following link opens in a new window"><img width="10" height="10" alt="The following link opens in a new window" src="/cms-cps/firstchoice/falcon/images/icons/link_new_window.gif"/> Our security policy</a>
   </div>
</div>
</c:when>
<c:otherwise>
    <div class="checkoutPaymentPolicyLinks" >
      <div>
        <a rel="external" class="arrow-link" href="JavaScript:void(openPolicyWindow('<c:out value='${clientUrl}'/>/fcfalcon/com/tui/uk/view/common/travelinfo/privacyPolicy.jsp'))"  title="The following link opens in a new window"><img width="10" height="10" alt="The following link opens in a new window" src="/cms-cps/firstchoice/falcon/images/icons/link_new_window.gif"/> Our Privacy Policy</a>
      </div>
      <div>
        <a rel="external" class="arrow-link" href="JavaScript:void(openPolicyWindow('<c:out value='${clientUrl}'/>/fcfalcon/com/tui/uk/view/common/travelinfo/securityPolicy.jsp'))"  title="The following link opens in a new window"><img width="10" height="10" alt="The following link opens in a new window" src="/cms-cps/firstchoice/falcon/images/icons/link_new_window.gif"/> Our security policy</a>
      </div>
    </div>
</c:otherwise>
</c:choose>
    <%--END OF  PAYMENT POLICY LINKS --%>
  </div><%-- END firstColSidesShadow --%>
  <div class="firstColBottomShadow"></div><%-- Added by N Ng on 22 July 2008 --%>

<%--------------------------------------------------------------------------%>

<%-------------------------- CV2AVS block starts ---------------------------%>
  <div class="subheaderBar">
   <img src="/cms-cps/firstchoice/falcon/images/icons/header-postal-address.gif" alt=""/>
   <h2>Cardholder address</h2>
</div>
<div class="clearer"></div>
<div class="firstColSidesShadow">
<div class="confirmLeadPassengerAddress" id="confirmLeadPassengerAddressBlock" style="display:none">
   <p>
    To help ensure your card details remain secure, please confirm the address of the cardholder.<br/>
    If this is the same as the lead passenger address, you just need to check the box.
   </p>
   <input type="checkbox" id="confirmLeadPassengerAddress" name="confirmLeadPassengerAddress"/>
   <label for="confirmLeadPassengerAddress">Same as lead passenger</label>
</div>
<%-- Start of Lead passengr Address details --%>
<%@include file="/falcon/cardHolderAddress.jspf" %>
<%-- End of Lead passengr Address details --%>
</div><%-- END firstColSidesShadow --%>
<div class="firstColBottomShadow" style="margin-top:-14px;" ></div>

<!-- *******Essential fields ********************  -->
<input type="hidden" name="total_transamt"  id="total_transamt"  value="NA" />
<%-- no of transaction is 1  --%>
<input type="hidden"  id="payment_0_totalTrans" value="1" name="payment_totalTrans"/>
<!-- *******End Essential fields ********************  -->
<script type="text/javascript">
<!--
function intialiseOnloadPayment()
{
  initialisePaymentEvents();
  showMessage(errorFields,'<c:out value="${bookingComponent.errorMessage}"/>');
  <c:if test="${cvvEnabled=='true'}">
     $("confirmLeadPassengerAddressBlock").style.display = "block";
  </c:if>
}

window.onunload = function(){};

addLoadEvent( intialiseOnloadPayment );
// -->
/*
window.onload=
   function()
   {
      <c:if test="${bookingInfo.newHoliday == 'true' }">
      initializeDepositSelection();
     </c:if>

      setToDefaultSelection();

      var options = $('payment_type_0').options;
      for(var i=0; i<options.length ; i++)
      {
        paymenttypeoptions.push(options[i].text);
      }
   <c:if test="${Irlandnorth=='true'}">
     updateChargesInDropDown();
   </c:if>
      showMessage(errorFields,"<c:out value="${bookingComponent.errorMessage}"/>");
   checkBookedStatus();
   throbberInitialise("SummaryTotalPriceThrobber");

  }
*/
   window.onunload = function(){};
   jQuery(document).ready(function()
  {
      //toggleOverlay();
      toggleOverlayFalcon();
      jQuery("#confirmLeadPassengerAddress").click(function(){fillAddressDetails();});
  });
</script>


