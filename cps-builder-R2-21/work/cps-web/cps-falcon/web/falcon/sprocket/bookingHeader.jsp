<%-- 200508 --%>
<%-- The QUERY_STRING is booking=true --%>
<div id="Header" class="noBottomMargin">
  <div id="headerContent">
    <%-- #Header --%>
    <div id="headerContentWrapper">
      <div id="masthead">
      <div id="logo"><a href="https://www.falconholidays.ie/"><img src="/cms-cps/firstchoice/falcon/images/header/falcon-header-logo.gif" alt="Falcon" width="139" height="34" /></a> </div>
        <div id="utilityMenu">
          <ul>

            <li><a href="javascript:Popup('/help/?popup=true',630,600,'scrollbars=yes');" rel="nofollow"><img src="/cms-cps/firstchoice/falcon/images/header/link_new_window.gif" alt="" width="10" height="10" /> Help</a></li>
            <li><a href="javascript:Popup('/contact-us/?popup=true',630,600,'scrollbars=yes');" rel="nofollow"><img src="/cms-cps/firstchoice/falcon/images/header/link_new_window.gif" alt="" width="10" height="10" /> Contact us</a></li>
          </ul>
        </div>
        <div id="printHeader"></div>
      </div>
    </div>

  </div>

</div>
<div class="clearer"></div>