<%@include file="/common/commonTagLibs.jspf"%>
<%-- 14082008 --%>


<%-- The QUERY_STRING is booking=true&amp;departure=north --%>



<c:choose>
	  	  <c:when test="${( bookingComponent.nonPaymentData['atol_flag'] ) &&( bookingComponent.nonPaymentData['atol_flag'] !=null )}">
				  <c:if test="${(bookingComponent.nonPaymentData['atol_protected']) &&(bookingComponent.nonPaymentData['atol_protected'] !=null)}">
				  </c:if>
		  </c:when>
		  <c:otherwise>
<div id="Footer">
  <a name="footer" id="footer"></a>
  

   
 <div id="atol_abta"><a href="http://www.abta.com/" title="ABTA"  rel="external" class="noicon" target="_blank"><img src="/cms-cps/firstchoice/falcon/images/footer/abta_logo.gif" width="32" height="41" alt ="ABTA logo" class="foot_img_1" /></a> 
 
<a href="http://www.atol.org.uk/" title="ATOL"  rel="external" class="noicon" target="_blank">  <img src="/cms-cps/firstchoice/falcon/images/footer/atol_logo.gif" width="41" height="41" alt ="ATOL logo" class="foot_img_2" /></a>

     <p><strong>The air holiday packages shown are ATOL protected by the Civil Aviation Authority.  Our ATOL number is ATOL 2524.<br/>
         ATOL protection does not apply to all holiday and travel services shown on this website. Please see our booking conditions for more information.</p>
  </div>
     

  </div>
  <script>
	jQuery(document).ready(function(){
		jQuery('#atol_abta').removeAttr('style')
  });
  </script>
  </c:otherwise>
	</c:choose>
  <%-- end atol_abta--%>


  <div id="copyrightUtilityMenu">
    <ul>


<c:choose>
	  	  <c:when test="${(bookingComponent.nonPaymentData['atol_flag']  != null) &&( bookingComponent.nonPaymentData['atol_flag'] )&& (bookingComponent.nonPaymentData['atol_protected'] )}">
	  	
    <div id="atol_abta" style="border:none;"><a href="http://www.abta.com/" title="ABTA"  rel="external" class="noicon" target="_blank"><img src="/cms-cps/firstchoice/falcon/images/footer/abta_logo.gif" width="32" height="41" alt ="ABTA logo" class="foot_img_1" /></a> 
        
   <a href="http://www.atol.org.uk/" title="ATOL"  rel="external" class="noicon" target="_blank">  <img src="/cms-cps/firstchoice/falcon/images/footer/atol_logo.gif" width="41" height="41" alt ="ATOL logo" class="foot_img_2" /></a> </div>
  </c:when>
				  <c:when test="${(bookingComponent.nonPaymentData['atol_flag']  != null) &&( bookingComponent.nonPaymentData['atol_flag'] )&& !(bookingComponent.nonPaymentData['atol_protected'])}">

			 <div id="atol_abta"><a href="http://www.abta.com/" title="ABTA"  rel="external" class="noicon" target="_blank"><img src="/cms-cps/firstchoice/falcon/images/footer/abta_logo.gif" width="32" height="41" alt ="ABTA logo" class="foot_img_1" /></a>  </div>
        
				  </c:when>
    <c:otherwise>
    
	</c:otherwise>
   </c:choose>
  



       <li><a href="javascript:Popup('/our-policies/accessibility/?popup=true',630,600,'scrollbars=yes');" rel="nofollow" class="noicon"><img src="/cms-cps/firstchoice/falcon/images/header/link_new_window.gif" alt="" width="10" height="10" /> Accessibility</a></li>
       <li><a href="javascript:Popup('/our-policies/terms-of-use/?popup=true',630,600,'scrollbars=yes');" rel="nofollow" class="noicon"><img src="/cms-cps/firstchoice/falcon/images/header/link_new_window.gif" alt="" width="10" height="10" /> Terms of use</a></li>
       <li><a href="javascript:Popup('/our-policies/security-policy/?popup=true',630,600,'scrollbars=yes');" rel="nofollow" class="noicon"><img src="/cms-cps/firstchoice/falcon/images/header/link_new_window.gif" alt="" width="10" height="10" /> Security policy</a></li>
       <li class="last"><a href="javascript:Popup('/our-policies/privacy-policy/?popup=true',630,600,'scrollbars=yes');" rel="nofollow" class="noicon"><img src="/cms-cps/firstchoice/falcon/images/header/link_new_window.gif" alt="" width="10" height="10" /> Privacy policy</a></li>
       <li class="last"><a href="javascript:Popup('/information/credit-card-payments-and-charges/index.html?popup=true',630,600,'scrollbars=yes');" rel="nofollow" class="noicon"><img src="/cms-cps/firstchoice/falcon/images/header/link_new_window.gif" alt="" width="10" height="10" /> Credit Card Fees</a></li>
    </ul>
  </div>
  <%-- end copyright and utilities--%>

