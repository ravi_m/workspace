 <%@include file="/common/commonTagLibs.jspf"%>

 <div id="CheckoutPaymentDetails" class="layoutEBodyColumn">
        <a name="main_content" id="main_content"></a>
       <h1>Payment Details</h1>

       <%-- Messages --%>
 <%-- WARNING DISPLAYED IF HOLIDAY PRICE HAS CHANGED SINCE LAST PAGE --%>
 <c:if test="${not empty bookingComponent.errorMessage}">
      <c:if test="${!fn:containsIgnoreCase(bookingComponent.errorMessage,'Promotional')}">
          <div id="PriceDifferenceWarningBlock" class="newWarningBlock">
             <div class="newWarningBlockInner">
                <img class="newWarningBlockIcon" src="/cms-cps/firstchoice/falcon/images/icons/warning.gif" alt="Alert!" title="Alert!" />
                  <div class="newWarningBlockContent">
                   <p class="first"><strong><c:out value="${bookingComponent.errorMessage}" escapeXml="false"/></strong></p>
                  </div>
              </div>
           </div>
           <div class="clearer"></div>
      </c:if>
 </c:if>
 <%-- VALIDATION WARNING BLOCK --%>
         <div id="ValidationWarningBlock" class="newWarningBlock pageTopMessage">
           <div class="newWarningBlockInner">
              <img class="newWarningBlockIcon" src="/cms-cps/firstchoice/falcon/images/icons/warning.gif" alt="Alert!" title="Alert!" />
             <div class="newWarningBlockContent">
                 <p class="first"><strong>Please check these details are correct.</strong></p>
                <ul class="warningValidationLinks">
                   <li class="hidden"></li>
                </ul>
               </div>
               <div class="clearer"></div>
           </div>
         </div>
         <div class="clearer"></div>
<%-- DOWN TIME ERROR MESSAGE BLOCK --%>
 <c:if test="${requestScope.disablePaymentButton == 'true'}">
    <c:if test="${not empty requestScope.downTimeErrorMsg}">
       <div id="downtimeerror" class="newWarningBlock pageTopMessage">
         <div class="newWarningBlockInner">
            <img class="newWarningBlockIcon" src="/cms-cps/firstchoice/falcon/images/icons/warning.gif" alt="Alert!" title="Alert!" />
            <div class="newWarningBlockContent">
               <p class="first"><strong><c:out value="${requestScope.downTimeErrorMsg}" escapeXml="false"/></strong></p>
            </div>
            <div class="clearer"></div>
         </div>
       </div>
       <div class="clearer"></div>
    </c:if>
 </c:if>

<%--   End of messages  --%>

<form id="CheckoutPaymentDetailsForm" method="post"
          action="/cps/processPayment?token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />"
           onsubmit="updateEssentialFields();return bookNowButtonClicked();" >

<%--***********************************************Payment details******************************************************************--%>
     <%@include file="paymentDetails.jsp" %>
<%--***********************************************End of Payment details******************************************************************--%>
<%--***********************************************Terms and conditions******************************************************************--%>
     <%@include file="termsAndCondition.jsp" %>
<%--***********************************************End of Terms and conditions******************************************************************--%>
</form>


<%------------------------------------SECURITY CODE blinky ----------------------------------------------------------------------------------------------------------%>
<c:set var="blinkyId" scope="page" value="blinky3digits"/>
      <c:set var="sprocketUrlInclude" scope="page">
          sprocket/payment-card-security-standard.jsp
      </c:set>
      <c:set var="sprocketCacheContent" scope="page" value="true"/>
      <%@ include file="genericSprocketBlinky.jspf"%>

     <c:set var="blinkyId" scope="page" value="blinky4digits"/>
      <c:set var="sprocketUrlInclude" scope="page">
          sprocket/payment-card-security-amex.jsp
      </c:set>
      <c:set var="sprocketCacheContent" scope="page" value="true"/>
       <%@ include file="genericSprocketBlinky.jspf"%>
<%------------------------------------end SECURITY CODE blinky ----------------------------------------------------------------------------------------------------------%>
  </div>
                  </div>
               </div>
            </div>
 <%-- 14082008 --%>

<%--****************Footer*********************--%>
<c:choose>
 <c:when test="${Irlandnorth=='true'}">
     <jsp:include page="sprocket/bookingFooterNorth.jsp"/>
 </c:when>
 <c:otherwise>
    <jsp:include page="sprocket/bookingFooterSouth.jsp"/>
 </c:otherwise>
 </c:choose>
<%--****************End Footer*********************--%>

</div>
<div id="PageColumn2">
    <jsp:include page="sprocket/customer_service.htm"/>
</div>