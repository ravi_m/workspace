<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<h3>Leaving</h3>
<c:set var="stops" value="-1" scope="page"/>
<c:forEach items="${bookingComponent.flightSummary.outboundFlight}" var="outboundSectors">
 <c:set var="stops" value="${stops+1}" scope="page"/>
  <ul class="flightOptions">
    <li>
      <div class="flightHeader">Depart&#58;</div>
      <div class="flightInfo">
        <p><c:out value="${outboundSectors.departureAirportName}"/>&nbsp;(<c:out value="${outboundSectors.departureAirportCode}"/>)</p>
        <span class="flightDate">
          <fmt:formatDate value="${outboundSectors.departureDateTime}" type="time" pattern="dd/MM/yy"/>
        </span>
        <span class="flightTime">
          <fmt:formatDate value="${outboundSectors.departureDateTime}" type="time" pattern="HH:mm"/>
        </span>
      </div>
    </li>
  </ul>
  <ul class="flightOptions">
    <li>
      <div class="flightHeader">Arrive&#58;</div>
      <div class="flightInfo">
        <p><c:out value="${outboundSectors.arrivalAirportName}"/>&nbsp;(<c:out value="${outboundSectors.arrivalAirportCode}"/>)</p>
        <span class="flightDate">
          <fmt:formatDate value="${outboundSectors.arrivalDateTime}" type="time" pattern="dd/MM/yy"/>
        </span>
        <span class="flightTime">
          <fmt:formatDate value="${outboundSectors.arrivalDateTime}" type="time" pattern="HH:mm"/>
        </span>
      </div>
    </li>
  </ul>
  <ul class="flightOptions">
    <li>
      <div class="flightHeader">Carrier&#58;</div>
      <div class="flightInfo">
        <p><c:out value="${outboundSectors.carrier}" escapeXml="false"/></p>
        <span class="flightCarrier"><c:out value="${outboundSectors.operatingAirlineCode}${outboundSectors.flightNumber}"/></span>
      </div>
    </li>
  </ul><!-- END Flight Departure -->
</c:forEach>
