<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div class="subheaderBar">
  <img alt="" src="/cms-cps/fcao//images/icons/header-passengers.gif" />
  <h2>Cardholder address</h2>
</div>
<div class="paxContactDetails">
  <div id="confirmLeadPassengerAddress">
  <p>
     To help ensure your card details remain secure, please confirm the address of the cardholder.
     Please note: the cardholders address <b>must</b> be the address that the card is registered to.
	 If this is the same as the lead passenger address, you just need to check the box.
  </p>
	  <p class="sameAddress">
    <input type="checkbox" id="LeadPassengerAddressCopy" name="AddressCopy"	onclick="javascript:copyLeadPassengerAddress()" />
	    Same address as lead passenger
	  </p>
  </div>
  <ul class="contactDetailsForm">
    <li>
      <p>House Name/No.</p>
      <input type="text" id="payment_0_street_address1" name="payment_0_street_address1"  maxlength="20" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/>" autocomplete="off" />
      <span>*</span>
    </li>
    <li>
      <p>Address</p>
      <input type="text" id="payment_0_street_address2" name="payment_0_street_address2" maxlength="25" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>" autocomplete="off" />
      <span>*</span>
    </li>
    <li class="townCity">
      <p>Town/City</p>
      <input type="text" id="payment_0_street_address3" name="payment_0_street_address3" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>"  maxlength="25" autocomplete="off" />
      <span>*</span>
    </li>
    <li>
      <p>County</p>
      <input type="text" id="payment_0_street_address4" name="payment_0_street_address4" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address4']}"/>" maxlength="16" autocomplete="off" />
      <span>*</span>
    </li>
    <li class="postcode">
      <p>Postcode</p>
      <input type="text" id="payment_0_postCode" name="payment_0_postCode" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/>" maxlength="8" autocomplete="off" />
      <span>*</span>
    </li>
    <li class="country">
      <input type="hidden" id="CHcountryName" name="payment_0_selectedCountryCode" value="GB"/>
      <p>Country</p>
      <select id="CHcountryName1" name="CHcountry" onchange="document.getElementById('CHcountryName').value=this.options[this.selectedIndex].value;">
        <c:choose>
          <c:when test="${bookingInfo.paymentDataMap['payment_0_selectedCountryCode'] != null && bookingInfo.paymentDataMap['payment_0_selectedCountryCode'] != '-1'}">
            <c:set var="selectedCountryCode" value="${bookingInfo.paymentDataMap['payment_0_selectedCountryCode']}" scope="page"/>
          </c:when>
          <c:otherwise>
            <c:set var="selectedCountryCode" value="GB" scope="page"/>
          </c:otherwise>
        </c:choose>
        <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
          <c:set var="countryCode" value="${CountriesList.key}" />
          <c:set var="countryName" value="${CountriesList.value}" />
          <option value='<c:out value="${countryCode}"/>'
            <c:if test='${countryCode==selectedCountryCode}'>selected='selected'</c:if>>
            <c:out value="${countryName}" />

          </option>
        </c:forEach>
      </select>
      <input type="hidden" id="payment_0_selectedCountry" name="payment_0_selectedCountry" value="" />
    </li>
 </ul>
 </div><!-- END paxContactDetails -->
<br clear="all"/>
 <p class="compulsoryInfo">All fields marked with * must be completed.</p>
