<%@include file="/common/commonTagLibs.jspf"%>
<script type="text/javascript" src="/cms-cps/fcao/js/popups.js"></script>
<!-- START Footer -->
<a id="footer" name="footer"/>
<div class="pnlOctober">
<div id="atol_abta">
  <a href="http://www.abta.com/" title="The following link opens in a new window" rel="external" target="_blank"><img src="/cms-cps/fcao/images/footer/logo-abta_2009.gif" alt="ABTA logo" class="foot_img_1" width="65" height="29"/></a>
  <c:choose>
  <c:when test="${( bookingComponent.nonPaymentData['atol_flag']  !=null) &&(bookingComponent.nonPaymentData['atol_flag']) &&(bookingComponent.nonPaymentData['atol_protected'])}">
	<a href="http://www.atol.org.uk/" title="The following link opens in a new window" rel="external" target="_blank"><img src="/cms-cps/fcao/images/footer/atol_logo.gif" alt="ATOL logo"
  class="foot_img_2" width="41" height="41"/></a>
   </c:when>
   <c:when test="${( bookingComponent.nonPaymentData['atol_flag']  !=null) &&(bookingComponent.nonPaymentData['atol_flag']) &&!(bookingComponent.nonPaymentData['atol_protected'])}">
   </c:when>
  <c:otherwise>
  <a href="http://www.atol.org.uk/" title="The following link opens in a new window" rel="external" target="_blank"><img src="/cms-cps/fcao/images/footer/atol_logo.gif" alt="ATOL logo"
  class="foot_img_2" width="41" height="41"/></a>
   <p>The air holiday packages shown are ATOL protected by the Civil Aviation Authority.  Our ATOL number is ATOL 2524.<br/>ATOL protection does not apply to all holiday and travel services shown on this website. Please see our booking conditions for more information.</p>

	<script>
	jQuery(document).ready(function (){
		jQuery('.pnlOctober').removeClass();
	});
	</script>

   </c:otherwise>
   </c:choose>
  </div>

  <div id="copyrightUtilityMenu">
    <ul>
       <li><a href="javascript:PopupForFcao('<c:out value='${clientUrl}'/>/our-policies/accessibility/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Accessibility</a></li>
       <li><a href="javascript:PopupForFcao('<c:out value='${clientUrl}'/>/our-policies/terms-of-use/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Terms of use</a></li>
       <li><a href="javascript:PopupForFcao('<c:out value='${clientUrl}'/>/our-policies/security-policy/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Security policy</a></li>
       <li><a href="javascript:PopupForFcao('<c:out value='${clientUrl}'/>/our-policies/privacy-policy/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Privacy policy</a></li>
       <li class="last"><a href="javascript:PopupForFcao('<c:out value='${clientUrl}'/>/help/booking-online/credit-card-payments-and-charges/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Credit Card Fees</a></li>
    </ul>
  </div>
  </div>
<!-- END Footer -->
<!-- Modified to remove ensighten from footer -->