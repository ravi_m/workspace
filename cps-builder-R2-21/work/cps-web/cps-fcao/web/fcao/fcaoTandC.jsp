<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="accommodationInventorySystem" value="" scope="page"/>
<c:set var="flightInventorySystem" value="" scope="page"/>
<c:set var="url" value='${bookingComponent.clientDomainURL}'/>

<c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem != null}">
  <c:set var="accommodationInventorySystem" value="${bookingComponent.accommodationSummary.accommodationInventorySystem}" scope="page"/>
</c:if>
<c:if test="${bookingComponent.flightSummary.flightSelected=='false'}">
  <c:if test="${bookingComponent.flightSummary.flightSupplierSystem != null}">
    <c:set var="flightInventorySystem" value="${bookingComponent.flightSummary.flightSupplierSystem}" scope="page"/>
  </c:if>
</c:if>
<c:set var="departureDate">
  <fmt:formatDate value="${bookingComponent.accommodationSummary.startDate}" pattern="yyyy-MM-dd"/>
</c:set>
<c:set var="key" value="${accommodationInventorySystem}${flightInventorySystem}" scope="page"/>
<c:set var="tcClassificationKey" value="${bookingComponent.termsAndCondition.tcClassificationKey}" scope="page"/>

<div class="subheaderBar">
  <img src="/cms-cps/fcao/images/icons/header-terms-and-conditions.gif" width="34" height="27" alt="" />
  <h2>Terms &amp; conditions</h2>
</div>
<div class="optionDetails">
  <p>Please read our
    <a class="inline-link" title="Privacy Policy" href="javascript:PopupForFcao('http://www.firstchoice.co.uk/our-policies/privacy-policy/index.html',800,600,'scrollbars=1,resizable=1');">
        <img height="10" width="10" alt="The following link opens in a new window" src="/cms-cps/fcao/images/icons/link-new-window.gif" />Privacy Policy
    </a>
    and
    <a title="Data Protection Notice" class="inline-link" href="javascript:showHiddenInformation('dataprotectionnoticebox')">
       Data Protection Notice
    </a>
    and confirm you agree to our use of your information provided above (which may in special situations include sensitive personal data) by ticking the box below.
  </p>
  <!-- Start of data protection text (hidden by default)-->
  <div id="dataprotectionnoticebox" style="display:none;">
    <p>
      <strong>Data Protection Notice:</strong><br/>
      We may from time to time contact you by post or e-communications with information on offers of goods and services, brochures, new products, forthcoming events or competitions from our holiday divisions and our holiday group companies. If you have not already done so, by providing your address, e-mail and phone number you agree to its use.
    </p>
    <div class="conditionsAcceptance">
      <input name="chkTuiMarketingAllowed" id="chkTuiMarketingAllowed" type="checkbox" value="true"  onClick="updateFormElementFromCheckBoxMarketing(this,'tuiMarketingAllowed');"/>
      <p class="termsConfirmation">
            <label for="chkTuiMarketingAllowed">If you would not like to receive e-communications including information on discounts from <a class="inline-link" title="www.firstchoice.co.uk" href="http://www.firstchoice.co.uk" target="_blank">firstchoice.co.uk</a>, please tick this box.</label>
      </p>
    </div>
    <div class="conditionsAcceptance">
      <input name="chkThirdPartyMarketingAllowed" id="chkThirdPartyMarketingAllowed" type="checkbox" value="true" onClick="updateFormElementFromCheckBoxMarketing(this,'thirdPartyMarketingAllowed');"/>
      <p class="termsConfirmation">
        <label for="chkThirdPartyMarketingAllowed">Our business partners and carefully selected companies outside our holiday group would like to send you information about their products and services by post. If you would not like to hear from them, please tick this box.</label>
      </p>
    </div>
  </div><!-- End of data protection text (hidden by default)-->
    <p>
      Please note that all members of your party require valid passports and any applicable visas before travelling. Visit the Foreign Office website at
      <c:choose>
        <c:when test="${bookingComponent.flightSummary !=null}">
          <a title="Foreign Office website" href="http://www.fco.gov.uk/travel?ico=FUPayment_ForeignOffice" target="_blank" class="inline-link">
            <img height="10" width="10" alt="The following link opens in a new window" src="/cms-cps/fcao/images/icons/link-new-window.gif" />www.fco.gov.uk/travel
          </a>
        </c:when>
        <c:otherwise>
          <a title="Foreign Office website" href="http://www.fco.gov.uk/travel?ico=aoPayment_ForeignOffice" target="_blank" class="inline-link">
            <img height="10" width="10" alt="The following link opens in a new window" src="/cms-cps/fcao/images/icons/link-new-window.gif" />www.fco.gov.uk/travel
          </a>
        </c:otherwise>
      </c:choose>
      to see visa and travel advice, or the Passport Office website at
      <c:choose>
        <c:when test="${bookingComponent.flightSummary !=null}">
          <a title="Passport Office website" href="http://www.passport.gov.uk?ico=FUPayment_PassportOffice" target="_blank" class="inline-link">
            <img height="10" width="10" alt="The following link opens in a new window" src="/cms-cps/fcao/images/icons/link-new-window.gif" />www.passport.gov.uk
          </a>
        </c:when>
        <c:otherwise>
          <a title="Passport Office website" href="http://www.passport.gov.uk?ico=AOPayment_PassportOffice" target="_blank" class="inline-link">
            <img height="10" width="10" alt="The following link opens in a new window" src="/cms-cps/fcao/images/icons/link-new-window.gif" />www.passport.gov.uk
          </a>
        </c:otherwise>
      </c:choose>
        for passport information.
    </p>
    	<c:if test="${( bookingComponent.nonPaymentData['atol_flag'] != null ) &&( bookingComponent.nonPaymentData['atol_flag'] )&&( bookingComponent.nonPaymentData['atol_protected'] )}">
				 <p>
    					This booking is authorised under the ATOL licence of TUI UK Limited, ATOL 2524, and is protected under the ATOL scheme, as set out in the ATOL certificate to be supplied.
    			  </p>
    		</c:if>

  <c:if test="${not empty flightInventorySystem }">
      <c:if test="${!(bookingComponent.flightSummary.flightSupplierSystem == 'Amadeus' || bookingComponent.flightSummary.flightSupplierSystem == 'NAV')}">
	      <p>
	        To view the notice summarising the liability rules applied by Community air carriers as required by Community legislation and the Montreal Convention, please view the
	        <a href="javascript:PopupForFcao('http://www.firstchoice.co.uk/our-policies/air-passenger-notice/',800,600,'scrollbars=1,resizable=1');" title="Air Passenger Notice" class="inline-link">
	           <img height="10" width="10" alt="The following link opens in a new window" src="/cms-cps/fcao/images/icons/link-new-window.gif" />Air Passenger Notice
	        </a>.
	      </p>
      </c:if>
  </c:if>
  <div class="conditionsAcceptance">
    <input type="checkbox" id="bookingConditionsChecked" name="bookingConditionsChecked" value="true" />
    <p class="termsConfirmation">
      <label for="bookingConditionsChecked">I confirm that:<br />
      (a) as the lead name/passenger, I am at least 18 years old.<br />
      (b) I have contacted the relevant supplier(s) regarding any special needs before booking and<br />
      (c) I have read and accept all of the following:</label>
    </p>
    <p class="conditionsPolicy">
      <a title="Booking Conditions" class="inline-link" href="javascript:void(0)" onclick="popUpBookingConditions('<c:out value='${url}'/>/accom/page/common/tandc/tandc.page','<c:out value="${departureDate}"/>')">
        <img height="10" width="10" alt="The following link opens in a new window" src="/cms-cps/fcao/images/icons/link-new-window.gif" />Booking Conditions
      </a><br />
       <a class="inline-link" title="Privacy Policy" href="javascript:PopupForFcao('http://www.firstchoice.co.uk/our-policies/privacy-policy/index.html',800,600,'scrollbars=1,resizable=1');">
          <img height="10" width="10" alt="The following link opens in a new window" src="/cms-cps/fcao/images/icons/link-new-window.gif" />Privacy Policy
       </a>
      and use of my information
    </p>
  </div>
  <p>Please note that First Choice or your travel suppliers' cancellation / amendment charges are applicable once your booking has been confirmed. By clicking 'pay' you are confirming your booking and you agree for your card to be debited with all amounts due. All transactions are conducted over our secure server.</p>
</div><!-- END optionDetails -->