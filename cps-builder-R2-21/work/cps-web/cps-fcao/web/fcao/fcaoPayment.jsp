<%@include file="/common/commonTagLibs.jspf"%>

<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request"/>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1" />
    <version-tag:version/>
    <title>First Choice | Passenger details and payment</title>
    <c:set var='clientapp' value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName} '/>
    <%
         String clientApp = ((String)pageContext.getAttribute("clientapp")).trim();
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
    %>
    <!-- Include Payment related CSS  here -->
    <link href="/cms-cps/fcao/css/fcao_cps.css" rel="stylesheet" type="text/css" media="screen,print" />
    <!--[if lte IE 6]>
          <link type="text/css" rel="stylesheet" href="/cms-cps/fcao/css/fcao_ie6.css" />
          <%--  This script is added to remove image caching in ie 6 which was a main cause for me
          losing sleep for days :-) This solves the flickering images problem in IE6.--%>

          <script type="text/javascript">
             try
             {
		        document.execCommand("BackgroundImageCache", false, true);
	         }
	         catch(err) { }
          </script>
    <![endif]-->
    <!-- End of css files -->
    <!-- Include Payment related JS Here -->
    <script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
    <script type="text/javascript" src="/cms-cps/fcao/js/genericFormValidation.js"></script>
    <script type="text/javascript" src="/cms-cps/fcao/js/general.js"></script>
    <script type="text/javascript" src="/cms-cps/fcao/js/paymentUpdation.js"></script>
    <script type="text/javascript" src="/cms-cps/fcao/js/popups.js"></script>
    <!-- End of JS files -->
    <link rel="shortcut icon" href="/cms-cps/fcao/images/favicon/favicon.ico"/>
    <link rel="icon" href="/cms-cps/fcao/images/favicon/favicon.ico"/>
    <!-- Modified to add ensighten in header -->
 <%@include file="tag.jsp"%>
  </head>
  <body>
    <%--Contains Essential configuration settings used thru out the page --%>
  <%@include file="fcaoConfigSettings.jspf"%>
<jsp:include page="/fcao/cookielegislation.jsp"></jsp:include>
    <div id="Page">
      <div id="PageColumn1">

        <div id="Header" class="noBottomMargin">
          <jsp:include page="fcaoHeader.jsp" />
        </div>

        <jsp:include page="fcaoBreadCrumb.jsp" />

        <div id="MainContent">
           <form name="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />" onsubmit="updateEssentialFields();return  validateForm();">
            <div id="PrimaryColumn">
              <jsp:include page="fcaoSummaryPanel.jsp"/>
            </div>
            <jsp:include page="fcaoPassengerandPayment.jsp"/>
          </form>
          <img src="https://www.firstchoice.co.uk/canberra/sale?AMOUNT='ORDER REVENUE'&CID=46&OID='ORDER NUMBER'&TYPE=1" height="1" width="1" alt=""/>
        </div>

        <div id="Footer">
          <jsp:include page="fcaoFooter.jsp"/>
        </div>
        <%-- Omniture implimentation starts --%>
        <%@include file="/common/intellitrackerSnippet.jspf" %>
        <script type='text/javascript'>isl=isl +"tui.intelli-direct.com/e/t3.dll?249&";</script>
       <%--Modified to remove existing tags --%>
        <%-- Omniture imlementation ends  --%>
      </div><%--  END #PageColumn1 --%>
    </div>
  </body>
</html>