<%@include file="/common/commonTagLibs.jspf"%>
  <div id="headerContent">
    <div id="headerContentWrapper">
      <div id="masthead">
        <div id="logo"><a href="${clientUrl}"><img height="36" width="151" src="/cms-cps/fcao/images/header/first-choice-logo.gif" alt="First Choice"/></a></div>
        <div id="utilityMenu">
          <ul>
            <li><a title="Help" href="javascript:PopupForFcao('<c:out value='${clientUrl}'/>/help/?popup=true',630,600,'scrollbars=yes');">Help</a></li>
            <li><a title="Contact us" href="javascript:PopupForFcao('<c:out value='${clientUrl}'/>/contact-us/?popup=true',630,600,'scrollbars=yes');">Contact us</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div id="headerBottomDropWrapper">
    <div id="headerBottomDrop">
      <div class="bottomDropBlock6"></div>
    </div>
  </div>