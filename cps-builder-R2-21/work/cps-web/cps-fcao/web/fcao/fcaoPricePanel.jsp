<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />

<%-------------------- Settings to be used through out this file ----------------------------------------%>
<c:set var="uniqueId" value="bookingComponent.accommodationSummary.uniqueId"/>
<fmt:setLocale value="en_GB" />
<c:set var="payableOnCheckout" value="false" scope="page"/>
<c:set var="invSystem" value="${bookingComponent.accommodationSummary.accommodationInventorySystem}" scope="page"/>
<c:set var="isHoplaPrePay" value="${bookingComponent.accommodationSummary.prepay}" scope="page"/>

 <%--card charge special  --%>
        <c:set var="insertCardChargeSection" value="true" scope="page"/>
        <c:set var="styleCardCharge"  value="display:none"/>
        <c:if test="${bookingInfo.newHoliday == 'false' && (bookingInfo.calculatedCardCharge.amount != null && bookingInfo.calculatedCardCharge.amount != 0)}">
              <c:set var="styleCardCharge"  value="display:block"/>
         </c:if>
<%--End of card charge special  --%>
        
<%------------------- End of Settings to be used through out this file ---------------------------------%>

<div id="price">
  <h2>Price summary</h2>
  <div class="panelContainer">
 
    <ul id="price" class="priceSummary">
      <c:forEach var="costingLine" items="${bookingComponent.priceComponents}" varStatus="iterationStatus">
        
        <%---------------------------- Logic  for currency to be displayed on pricepanel --------------------------------------%>
        <c:set var="pricePanelCurrency">
          <c:choose>
            <c:when test='${costingLine.amount.currency == "EUR"}'>&euro;</c:when>
            <c:when test='${costingLine.amount.currency == "GBP"}'>&pound;</c:when>
            <c:when test='${costingLine.amount.currency == "USD"}'>&#36;</c:when>
            <c:otherwise><c:out value="${costingLine.amount.currency}"/></c:otherwise>
          </c:choose>
        </c:set>
        <%-----------------------** End of  Logic for currency description **------------------------------------------------%>
         
        <%---------------------------- Logic to show costingLine description --------------------------------------------------%>
        <c:set var="description">
          <c:if test="${not empty costingLine.quantity}">
                    <c:out value="${costingLine.quantity}&nbsp;&times;" escapeXml="false"/>
          </c:if>
          <c:choose>
            <c:when test="${fn:containsIgnoreCase(costingLine.itemDescription, 'Total Base Price') }">
               <c:out value="Payable on checkout"/>:
            </c:when>
            <c:when test="${fn:containsIgnoreCase(costingLine.itemDescription, 'Travel Extras') }">
               <c:out value="${costingLine.itemDescription}"/>
            </c:when>
            <c:otherwise>
               <c:out value="${costingLine.itemDescription}"/>:
            </c:otherwise>
          </c:choose>
        </c:set>  
       <%--------------------------------*** End of Logic to show costingLine description ***---------------------------------------------------%>

       <%---------------------------------  Logic to show costingLine amount --------------------------------------------------------------------%>
        <c:set var="amount">
          <c:choose>
            <c:when test="${fn:containsIgnoreCase(costingLine.itemDescription, 'Payable on checkout') }">
                 (&pound;<c:out value="${costingLine.amount.amount}"/>&dagger;)
            </c:when>
            <c:when test="${fn:containsIgnoreCase(costingLine.itemDescription, 'Online saving') }">
                 -<c:out value="${pricePanelCurrency}" escapeXml="false"/><c:out value="${costingLine.amount.amount}"/>
            </c:when>
            <c:otherwise>
                <c:out value="${pricePanelCurrency}" escapeXml="false"/><c:out value="${costingLine.amount.amount}"/>
            </c:otherwise>
          </c:choose>
        </c:set>
       <%--------------------------------**  End of Logic to show costingLine amount **--------------------------------------------------------------------%>

       <%------------------------------- Logic to apply Styles for price panel items----------------------------------------------------%>
        <%--  Add span class as below
            <li  <c:out value='${listStyle}
                 .....span class="<c:out value='${styleDescriptionClasses}......Hotel
               ........span class="<c:out value='${styleAmountClasses}......    
         --%>
        <%-- item  styles  --%>
        <c:set var="styleDescriptionClasses" value="header" />
        <c:set var="styleAmountClasses" value="info" />
        <c:set var="costingLineAmountId" value="" />
        <c:set var="listStyle" value=""/>
        <c:choose>
          <%--Following are for normal bookings --%>
          <c:when test="${fn:contains(costingLine.itemDescription,'Travel Extras')}">
            <c:set var="styleDescriptionClasses" value="header  caption" />
            <c:set var="styleAmountClasses" value="info" />
          </c:when>
          <c:when test="${costingLine.itemDescription =='Total' }">
            <c:set var="costingLineAmountId" >id = totalAmount</c:set>
            <c:set var="styleAmountClasses" value="totalPrice" />
            <c:set var="styleDescriptionClasses" value="totalHeader" />
          </c:when>

          <%-- Following are for hopla bookings --%>
          <c:when test="${fn:contains(costingLine.itemDescription,'Payable now')}">
            <c:set var="costingLineAmountId" >id = totalAmount</c:set>
            <c:set var="styleAmountClasses" value="totalPrice" />
            <c:set var="styleDescriptionClasses" value="totalHeader" />
          </c:when>
          <c:when test="${costingLine.itemDescription =='Total cost' }">
            <c:set var="costingLineAmountId" >id = hoplaTotalAmount</c:set>
            <c:set var="styleAmountClasses" value="info caption hoplaTotalPrice" />
            <c:set var="styleDescriptionClasses" value="header caption hoplaTotalHeader" />
          </c:when>
          <c:when test="${fn:contains(costingLine.itemDescription,'Total Base Price')}">
            <c:set var="listStyle">class=hoplaCosts</c:set>
            <c:set var="styleDescriptionClasses" value="hoplaHeader" />
            <c:set var="styleAmountClasses" value="hoplaInfo" />
          </c:when>
          <c:when test="${fn:contains(costingLine.itemDescription,'Payable on checkout')}">
            <c:set var="listStyle">class=hoplaSecondaryCosts</c:set>
            <c:set var="styleDescriptionClasses" value="hoplaSecondHeader" />
            <c:set var="styleAmountClasses" value="hoplaSecondInfo" />
          </c:when>
          <c:otherwise>                    
          </c:otherwise>
        </c:choose>
        <%--end item styles  --%>
       <%----------------------******End of Logic to apply Styles for price panel elements*****----------------------------------------------------%>
       <%--------------------------Now display costingline/price items in summary panel -------------------------------------------------------------%>
        <%--  Insert card charge --%>
        <c:if test="${ ( fn:containsIgnoreCase(costingLine.itemDescription , 'Payable now' )  || costingLine.itemDescription =='Total'  ) && insertCardChargeSection }">
          <li id="cardChargeText" style="<c:out value='${styleCardCharge}'/>">
            <span class="header">Card charges:</span>
            <span id='cardChargeAmount'  class='info'>&pound;<c:out value="${bookingInfo.calculatedCardCharge.amount}"/></span>
          </li>
          <c:set var="insertCardChargeSection" value="false" scope="page"/>
        </c:if>
 
        <li <c:out value='${listStyle}'/>>
          <span class="<c:out value='${styleDescriptionClasses}'/>"><c:out value="${description}" escapeXml="false"/></span>
          <span <c:out value="${costingLineAmountId}"/>  class="<c:out value='${styleAmountClasses}'/>"><c:out value="${amount}" escapeXml="false"/></span>
        </li> 
       <%-------------------------***End  display costingline/price items in summary panel***----------------------------------------------%>
      </c:forEach>
       
     <%--------------------------- Hopla information text to be displayed  -------------------------%>

      <c:if test="${not empty isHoplaPrePay && isHoplaPrePay =='false' }">
        <li class="currencyTxt">&dagger; The pound sterling equivalent may vary depending on the currency conversion exchange rate applicable on the date of payment.</li>
        <br clear="all"/>
        <li class="currencyTxt">*Some travel suppliers may request full or part payment of balance at time of booking. Local taxes and charges may apply, please check 'Rate details' link or supplier's 'Terms and Conditions' for more information.</li>
      </c:if>
     <%--------------------------- End Hopla information text to be displayed  ----------------------%>
    </ul>
       
  </div>
</div>
