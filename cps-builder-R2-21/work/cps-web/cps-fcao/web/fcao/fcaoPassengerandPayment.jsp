<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div id="contentCol" class="paxPayment">
  <div id="TitleArea">
    <h1>Passenger details and payment</h1>
  </div>
  <%@include file="fcaoWarning.jsp" %>
  <c:set var="hoplaBonded" value="${bookingInfo.bookingComponent.accommodationSummary.hoplaBonded}" />
  <c:choose>
    <c:when test="${not empty hoplaBonded && hoplaBonded}">
      <%@include file="fcaoImportantInfoHotelBeds.jsp" %>
    </c:when>
    <c:otherwise>
      <%@include file="fcaoImportantInfo.jsp" %>
    </c:otherwise>
  </c:choose>
  
  <div class="optionSection">
    <%@include file="fcaoPassengerDetails.jsp" %>
  </div><!-- END Personal details -->
  <div class="optionSection">
    <%@include file="fcaoPaymentDetails.jsp" %>
    <%@include file="fcaoCardHolderAddress.jsp" %>
  </div><!-- END Card details -->
  <div class="optionSection">
    <%@include file="fcaoTandC.jsp" %>
  </div><!-- END Terms and conditions -->
  <div class="btnContinue">

    <a href="<c:out value='${bookingComponent.prePaymentUrl}' escapeXml='false'/>" class="arrow-link-left" title="Back to travel options">Back to travel options</a>
    <c:if test="${bookingInfo.newHoliday}">
      <span id="confirmButton">
          <input type="image" src="/cms-cps/fcao/images/buttons/form/pay.gif" alt="Pay" title="Pay"/>
      </span>
    </c:if>
  </div>
</div><!-- END #contentCol -->