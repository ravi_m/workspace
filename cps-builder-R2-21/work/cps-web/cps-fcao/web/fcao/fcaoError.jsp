<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request"/>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <c:choose>
      <c:when test="${not empty sessionScope=='true'}">
        <title>First Choice | Error</title>
      </c:when>
      <c:otherwise>
        <title>First Choice | Session Timed Out</title>
      </c:otherwise>
    </c:choose>
    <link href="/cms-cps/fcao/css/fcao_cps.css" rel="stylesheet" type="text/css" media="screen,print" />
    <link href="/cms-cps/fcao/css/fcao_errors.css" rel="stylesheet" type="text/css" media="screen,print" />
      <!--[if lte IE 6]>
        <link type="text/css" rel="stylesheet" href="/fcao/css/fcao_ie6.css" media="screen,print" />
      <![endif]-->
    <script type="text/javascript" src="/js/browserdetect_lite.js"></script>
    <script type="text/javascript" src="/js/functions.js"></script>
    <script type="text/javascript" src="/js/searchPanel.js"></script>
    <script type="text/javascript" src="/fcao/js/fcao_searchpanel.js"></script>
    <script type="text/javascript" src="/fcao/js/prototype.js"></script>
    <script type="text/javascript" src="/fcao/js/predictivesearch.js"></script>
    <script type="text/javascript" src="/fcao/js/fcao_general.js"></script>
    <script type="text/javascript" src="/fcao/js/dynamic_location.js"></script>
    <script type="text/javascript" src="/fcao/js/dynamic_core.js"></script>
    <script type="text/javascript" src="/fcao/js/fcao_calendar.js"></script>
    <script type="text/javascript" src="/fcao/js/interstitialDisplay.js"></script>
    <script type="text/javascript" src="/fcao/js/vs.js"></script>
    <script type="text/javascript" src="/cms-cps/fcao/js/popups.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
    <link rel="icon" href="/images/favicon/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="/images/favicon/favicon.ico" type="image/x-icon"/>
    <!-- Modified to move ensighten from footer to header -->
    <%@include file="tag.jsp"%>
  </head>
  <body class="withoutBackground systemDown">
 <jsp:include page="/fcao/cookielegislation.jsp"></jsp:include>
    <div id="Page">

         <div id="Header">
         <!-- #Header -->
           <div id="headerContentWrapper">
                       <div id="masthead">
               <a href="http://www.firstchoice.co.uk"><img src="/cms-cps/fcao/images/header/first-choice-logo.gif" alt="First Choice" width="151" height="36" /></a>
             </div>
           </div>
         </div>
         <div id="MainContent">
           <div id="PrimaryColumn">
           </div>
           <div id="fullBodyWidth" class="pageError">
             <c:choose>
               <c:when test="${not empty sessionScope=='true'}">
                 <h1>Sorry, we are currently experiencing technical difficulties</h1>
               </c:when>
               <c:otherwise>
                 <h1>Sorry, your session has timed out</h1>
               </c:otherwise>
             </c:choose>
             <div class="errorContent">
               <img src="/cms-cps/fcao/images/section_promo/content_pic.jpg" alt=""/>
               <div class="contentBlock">
               <c:choose>
                 <c:when test="${not empty sessionScope=='true'}">
                   <p>For further assistance please call us on 0871 200 7799(Mon to Fri 8am - 10pm, Sat and Sun 9am - 8pm). Alternatively please search again later.</p>
                   <a href="<c:out value="${bookingComponent.clientDomainURL}"/>/hotels/" class="errorLink" title="<c:out value="${bookingComponent.clientDomainURL}"/>/hotels">Return to homepage</a>
                 </c:when>
                 <c:otherwise>
                   <p>Due to inactivity you session has timed out, please click homepage to begin another search.</p>
                 <a href="http://www.firstchoice.co.uk" class="errorLink" title="http://www.firstchoice.co.uk">Click here to begin another search</a>
                 </c:otherwise>
               </c:choose>
               </div>
             </div>
           </div>
         </div>
         <!-- The QUERY_STRING is booking=true&amp;siteStatsFlag=FALSE -->


  <div class="pnlOctober">
  <div id="Footer">
<div id="atol_abta">
 <div id="copyrightUtilityMenu" style="border:none;width: 690px;">
  <a href="http://www.abta.com/" title="The following link opens in a new window" rel="external" target="_blank"><img src="/cms-cps/fcao/images/footer/logo-abta_2009.gif" alt="ABTA logo" class="foot_img_1" width="65" height="29"/></a>
  <a href="http://www.atol.org.uk/" title="The following link opens in a new window" rel="external" target="_blank"><img src="/cms-cps/fcao/images/footer/atol_logo.gif" alt="ATOL logo"
  class="foot_img_2" width="41" height="41"/></a>
           <ul>
               <li><a href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/accessibility/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Accessibility</a></li>
               <li><a href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/terms-of-use/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Terms of use</a></li>
               <li><a href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/security-policy/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Security policy</a></li>
               <li class="last"><a href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/privacy-policy/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Privacy policy</a></li>
             </ul>
           </div>
         </div>
         <!--include sitestat.js file-->
  <script type='text/javascript' src='/js-sitestat/sitestat.js'></script>
<!--include sitestat.js file //-->
</div>
</div>
  </body>
</html>