<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<!DOCTYPE HTML>
<html lang="en-US" class="" id="falcon">
<head>
	<meta charset="UTF-8">
	<title>Flights with TUI | Thomson now TUI Airways</title>
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
    <link rel="icon" type="image/png" href="/cms-cps/mflightonly/images/favicon.png" />
	<link rel="stylesheet" href="/cms-cps/mflightonly/css/base.css" />
	<link rel="stylesheet" href="/cms-cps/mflightonly/css/bf.css" />
	<!-- <script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script> -->
    <%@include file="javascript.jspf"%>
	<script type="text/javascript">
		var breadcrumbScroll = null;
		jQuery(document).ready(function() {
			enableSelectBoxes();
			enableCheckBoxes();
			summaryPanelModal();
			intiateIscroll(jQuery(".scroll"));
			changeAddr();
			radioGrpInit();
			initTooltip();

		});
		window.addEventListener("resize", function() {
			var scrollDiv = jQuery('#breadcrumb');
			setTimeout(function() {
				scrollintoView(scrollDiv)
			}, 200);
		}, false);
    </script>
	<script src="/cms-cps/mflightonly/js/config.js" type="text/javascript"></script>
	<script type="text/javascript">
		var ensLinkTrack = function(){};
</script>
</head>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag"%>
<version-tag:version />
</head>
<c:set var='clientapp'	value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName}' />
<%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         clientApp = (clientApp!=null)?clientApp.trim():clientApp;
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
         String cvvEnabled = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".CV2AVS.Enabled", "false");
         pageContext.setAttribute("cvvEnabled", cvvEnabled, PageContext.REQUEST_SCOPE);
         String isExcursion =
            com.tui.uk.config.ConfReader.getConfEntry(clientApp+".summaryPanelWithExcursionTickets", "false");
         pageContext.setAttribute("isExcursion", isExcursion, PageContext.REQUEST_SCOPE);
      %>
<%
	String applyCreditCardSurcharge = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".applyCreditCardSurcharge" ,null);
	pageContext.setAttribute("applyCreditCardSurcharge", applyCreditCardSurcharge, PageContext.REQUEST_SCOPE);
%>
<script>
applyCreditCardSurcharge = "<%= applyCreditCardSurcharge %>";
</script>

	<script type="text/javascript" language="javascript">
	function newPopup(url) {
		var win = window.open(url,"","width=700,height=600,scrollbars=yes,resizable=yes");
	    if (win && win.focus) win.focus();
	}

	var session_timeout= ${bookingInfo.bookingComponent.sessionTimeOut};
	var IDLE_TIMEOUT = (${bookingInfo.bookingComponent.sessionTimeOut})-(${bookingInfo.bookingComponent.sessionTimeAlert});
	var _idleSecondsCounter = 0;
	var IDLE_TIMEOUT_millisecond = IDLE_TIMEOUT *1000;
	var url="${bookingInfo.bookingComponent.breadCrumbTrail['SEARCH_RESULT']}";
	var sessionTimeOutFlag =  false;

	window.addEventListener('pageshow', function(event) {
		console.log(event.persisted);
		if(event.persisted) {
			location.reload();
		}
		  if(event.currentTarget.performance.navigation.type == 2)
			{
				 location.reload();
				 }
	});

	function noback(){
		window.history.forward();

	}

	//New code added for session time out pop-up
	var initialTime = Date.now();
    var currentTime = initialTime;
    var hidden = "hidden";
    var myVar = setInterval(CheckIdleTime, 1000);

	function CheckIdleTime() {
                _idleSecondsCounter++;
                if (  _idleSecondsCounter >= session_timeout) {
                    if (sessionTimeOutFlag == false){
                    document.getElementById("sessionTimeOut").style.display = "block";
					clearInterval(myVar);
                    }
                }

            }

            adjustTimer = function() {
               currentTime = Date.now();
               //timeDiff.innerHTML = parseInt((currentTime - initialTime)/1000);
               _idleSecondsCounter = parseInt((currentTime - initialTime)/1000);
            }

            // Standards:
            if (hidden in document)
                document.addEventListener("visibilitychange", onchange);
            else if ((hidden = "mozHidden") in document)
                document.addEventListener("mozvisibilitychange", onchange);
            else if ((hidden = "webkitHidden") in document)
                document.addEventListener("webkitvisibilitychange", onchange);
            else if ((hidden = "msHidden") in document)
                document.addEventListener("msvisibilitychange", onchange);

            // IE 9 and lower:
            else if ("onfocusin" in document)
                document.onfocusin = document.onfocusout = onchange;
            // All others:
            else
                window.onpageshow = window.onpagehide
                = window.onfocus = window.onblur = onchange;

            function onchange (evt) {
                var v = "visible", h = "hidden",
                    evtMap = {
                    focus:v, focusin:v, pageshow:v, blur:h, focusout:h, pagehide:h
                    };

                evt = evt || window.event;
                if (evt.type in evtMap) {
                    //document.body.className = evtMap[evt.type];
                    if(evtMap[evt.type] == 'visible') {
                        adjustTimer();
                    }
                }
                else {
                    document.body.className = this[hidden] ? "hidden" : "visible";
                    if(!this[hidden]) {
                        adjustTimer();
                    }
                }
            }

            // set the initial state (but only if browser supports the Page Visibility API)
            if( document[hidden] !== undefined )
                onchange({type: document[hidden] ? "blur" : "focus"});

	function homepage()
	{
		url="${bookingInfo.bookingComponent.clientURLLinks.homePageURL}";
		window.noback();

		window.location.replace(url);
	}

	function ajaxForCounterReset() {
		var token = "<c:out value='${param.token}' />";
		var url = "/cps/SessionTimeOutServlet?tomcat=<c:out value='${param.tomcat}'/>";
		var request = new Ajax.Request(url, {
			method : "POST"
		});
		window._idleSecondsCounter = 0;

	}

	function makePayment() {
		var paymentForm = $('CheckoutPaymentDetailsForm');
		var element = paymentForm.serialize();
		elementstring = element
				+ "&token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />";
		var url = "/cps/processMobilePayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />";
		elementstring = elementstring.replace("&title=&", "&title=" + leadTitle + "&");
		elementstring = elementstring.replace("payment_0_type=&", "");
		elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
		elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
		//if(isvalid && cardValid){
			//document.getElementById("modal").style.visibility = "visible";
			var request = new Ajax.Request(url, {
				method : "POST",
				parameters : elementstring,
				onSuccess : showThreeDOverlay
			});
		//}
	}

	function showThreeDOverlay(http_request) {
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			result = http_request.responseText;
			if (result.indexOf("3dspage") == -1) {
				if (result.indexOf("ERROR_MESSAGE") == -1) {
					document.postPaymentForm.action = result;
					//document.CheckoutPaymentDetailsForm.reset();
					var csrfTokenVal = "<c:out value="${bookingComponent.nonPaymentData['csrfToken']}"/>";
						if(csrfTokenVal != '')
						{
						                   var csrfParameter = document.createElement("input");
						                    csrfParameter.type = "hidden";
						                    csrfParameter.name = "CSRFToken";
						                    csrfParameter.value = csrfTokenVal;
						                    document.postPaymentForm.appendChild(csrfParameter);
						}
					document.postPaymentForm.submit();
				} else {
					var errorMsg = result.split(':');
						jQuery('.alert').html('<i class="caret warning"></i>'+errorMsg[1]);
						document.getElementById("commonError").style.display = "block";
						jQuery('#commonError').removeClass('hide');
						//document.getElementById('commonError').innerHTML = "<p><strong>"
						//		+ errorMsg[1] + "</p></strong>";
						//document.getElementById('commonError').className = "commonErrorSummary info-section clear padding10px mb20px";
						clearCardEntryElements();
				}
			} else {
				window.location.href = 	result;

			}
		} else {
			document.getElementById('errorMsg').innerHTML = "<h2>Payment failed. Please try again.</h2>";
		}
	}
	window.scrollTo(0, 0);
}

/**
 ** This method submits the bank form present in the overlay and fills the overlay in the iframe.
**/
function bankRedirect() {
	document.bankform.target = "ACSframe";
	document.bankform.submit();
}
function clearCardEntryElements(){
	jQuery("#cardType").val("");
	jQuery("#cardTypespan").html("Select type of credit card");
	jQuery("#cardNumber").val("");
	jQuery("#cardNumberDiv").removeClass("valid");
	jQuery("#cardName").val("");
	jQuery("#cardNameDiv").removeClass("valid");
	jQuery("#expiryDateMM").val("");
	jQuery("#monthspan").html("MM");
	jQuery("#expiryDateYY").val("");
	jQuery("#yearspan").html("YY");
	jQuery("#securityCode").val("");
	jQuery("#securityCodeDiv").removeClass("valid");
	jQuery("#creditPaymentTypeCode").hide();
	jQuery("#debitPaymentTypeCode").hide();
	jQuery("#issueNumber").val("");
    jQuery("#issue").hide();
	document.getElementById("card-img").className = "";
	document.getElementById("card-desc").innerHTML = "";
	}
	</script>

<body onload="setDefaultDepositOption();">

<form id="CheckoutPaymentDetailsForm" name="paymentdetails" method="post" action="javascript:makePayment();">
<%@include file="flightsOnlyConfigSettings.jspf"%>

	<div class="structure">

		<div id="page" class="payment">


				<jsp:include page="sprocket/header.jsp" />


			<div id="content" class="book-flow">
				<div class="content-width">

					<div id="main">
	<%@include file="topError.jspf" %>
						<p class="current-state uppercase">Book your flight</p>

						<div class="component-wrap">
			<!-- This code has been rewritten in topError.jsp , this is as part of 3PA Airport Changes requirement-->

			<!--			<c:if test="${not empty bookingComponent.errorMessage}">
				            <c:if test="${fn:containsIgnoreCase(bookingComponent.errorMessage,'Whilst')}">
						<div class="alert med marg-bottom-20">
						<c:if test="${fn:containsIgnoreCase(bookingComponent.errorMessage,'decreased')}">
								<h5 class="tui black size-22 marg-bottom-5">Your flight is now cheaper!</h5>
						</c:if>
						<c:if test="${!fn:containsIgnoreCase(bookingComponent.errorMessage,'decreased')}">
							<h5 class="tui black size-22 marg-bottom-5">The price of your flight has changed</h5>
						</c:if>

								<p class="grey-med"><c:out value="${bookingComponent.errorMessage}"
										escapeXml="false" /></p>
						</div>
						   </c:if>
					</c:if>
-->
							<div id="customer-form" class="full-width">
								<form name="paymentdetails" method="post" id="CheckoutPaymentDetailsForm" action="javascript:makePayment();" novalidate>

									<!-- Information -->
									<div class="component">
										<div class="section-heading pad-top-0">
											<h2>Information</h2>
										</div>
										<p>
											Please complete this page within 30 minutes.
											<span class="grey-med">After 30 minutes this page will time out and you'll have to re-enter all details.</span>
										</p>
									</div>

									<!-- Holiday Details -->
									<%@include file="holidayDetails.jspf"%>

									<!-- Secure Online payment -->
									<%@include file="depositSection.jspf"%>

									<!-- Card holder Details -->
									<%@include file="cardDetails.jspf"%>

									<!-- Card holder Details -->
									<%@include file="cardHolderAddress.jspf"%>

									<!-- Fare rules -->
									<%@include file="fareRules.jspf"%>

									<!-- Baggage check-in safety information -->
									<%@include file="baggageSafetyinformation.jspf"%>

									<!-- Terms and Conditions -->
									<%@include file="termsAndCondition.jspf"%>

									<!-- CTA -->
									<span id="paypalspanText">Book with</span>
									<div class="component text-c">
									<c:if test="${bookingInfo.newHoliday == true}">
										<c:choose>
											<c:when test="${requestScope.disablePaymentButton == 'true'}">
												<input type="submit" style="cursor:default;" disabled="disabled" value="Book this flight" class="book-flow button large cta disabled" id="paypalbutton"/>
											</c:when>
											<c:otherwise>
												<input type="submit" value="Book this flight" class="book-flow button large cta" id="paypalbutton"/>
											</c:otherwise>
										</c:choose>
									</c:if>

									</div>

								</form>
							</div>
						</div>


						<!-- Sidebar -->
						<%@include file="summaryPanel.jspf"%>

					</div>
				</div>
			</div>

			<!-- Include for Footer > Thomson -->

					<jsp:include page="sprocket/footer.jsp" />

			<div class="page-mask"></div>
		</div>
	</div>

	<div id="video-container" class="hide">
	</div>
<fmt:formatNumber value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}" var="totalCostingLine" type="number" pattern="####" maxFractionDigits="2" minFractionDigits="2"/>
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page"/>
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
	<!-- Summary Flyout -->
	<div class="modal flyout ">
		<div class="window" >

			<div class="summary-panel bg-light-grey">
				<div class="image-container rel">
					<div class="total c bg-dark-blue white">
						<h3>&pound;</h3><h2><c:out value="${totalcost[0]}."/><span><c:out value="${totalcost[1]}"/></span></h2>
						<div>
						Total price
						</div>
						<i class="caret close b abs"></i>
					</div>
				</div>
				<div class="scroll-container" data-scroll-options='{"scrollX": false, "scrollY": true, "keyBindings": true, "mouseWheel": true}'>
					<div class="summary-breakdown">
						<%@include file="summaryPanel/pricePanel.jspf"%>
						<%@include file="summaryPanel/flightDetails.jspf"%>
						<%@include file="summaryPanel/holidayExtras.jspf"%>
						<%@include file="summaryPanel/priceBreakdown.jspf"%>
					</div>

				</div>
			</div>

		</div>
	</div>
	<div class="tooltip top center tp" id="tooltipTmpl">
		<p></p>
		<span class="arrow"></span>
	</div>
	<!-- <script type="text/javascript" src="/cms-cps/mflightonly/js/vs.js"></script> -->
	<script src="/cms-cps/mflightonly/js/iscroll-lite.js" type="text/javascript"></script>
	</form>
<div id="overlay" class="posFix"></div>
<form novalidate name="postPaymentForm" method="post">
		<input type="hidden" name="emptyForm" class="disNone"></input>
	</form>
	<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,chkTuiMarketingAllowed,chkThirdPartyMarketingAllowed,communicateByPost,communicateByPhone,communicateByEmail,selected_country_code,TUIHeaderSwitch</c:set>
			<script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
				 tui.analytics.page.pageUid = "mobilePaymentDetailsPage";
				 <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
					 <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
					 tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
				    </c:if>
					<c:if test= "${analyticsDataEntry.key == 'Party'}">
						tui.analytics.page.Party= "${analyticsDataEntry.value}";
					</c:if>
			    </c:forEach>
				</script>
</body>
</html>
