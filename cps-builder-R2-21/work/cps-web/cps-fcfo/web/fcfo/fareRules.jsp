<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session"/>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<h2 class="fareRules">Fare rules</h2>
<div id="fareRules" class="control">
<ul>
	<li>Any changes must be made via the Call Centre up to 3 hours before departure. This is subject to availability.</li>
	<li>The purchased ticket is non-refundable. If you would like to amend your ticket, you will be charged for this. This charge will include a fee per change, per person. Depending on your amendment type, together with the  fee(s) you may also pay the difference in airfare and you may lose a portion of your original airfare depending on the proximity of the departure date on the day you request your change.</li>
	<li>Please see our <a  class="hasPopup"
   onclick="window.open('http://www.firstchoice.co.uk/our-policies/flight-conditions-of-carriage/index.html','COC',
   'width=595,height=795,scrollbars');"
				href="javascript:void(0);">Conditions of Carriage</a> for further details.</li>
	</ul>
</div>


<h2 class="fareRules">Baggage check-in safety information</h2>
<div id="fareRules" class="control">
<ul>
	<li><p> For safety reasons, you can't bring the following dangerous articles or substances in either your checked-in baggage or hand baggage:</p></li>	<br>
    <p id="dangerous_items"><img alt="dangerous items list" src="/cms-cps/fcfo/images/FC-dangerousGoods.png" style=""></p><br>
     <li><p>You can carry batteries as hand baggage, either in their original packaging or a protective case.You'need to make sure they're protected against contact with other metal items and not kept loose in your bag, so they don't short-circuit. Equipment containing correctly-installed batteries can be packed in your checked-in baggage.</p></li><br>
	</ul>
</div>

<h2 class="fareRules">Terms & Conditions</h2>
<div id="fareRules" class="control">
<ul>
	<li>Please read our <a  class="hasPopup"
   onclick="window.open('http://www.firstchoice.co.uk/our-policies/privacy-policy','PrivacyPolicy','width=595,height=795,scrollbars');"
				href="javascript:void(0);">Privacy Policy </a> &nbsp; and <a onclick="javascript:showHiddenInformation('dataprotectionnoticediv')" href="javascript:void(0);"> Data Protection Notice</a> and confirm you agree to our use of your information provided above (which may in special situations include sensitive personal data) by ticking the box below.</li>
	<li id="dataprotectionnoticediv" style="display:none;">

	<div class="shadedBoxAttn" style="margin-left:-10px;">
			<p  style="padding-right:10px;">

                <b> Data Protection Notice </b><br/>
				We may from time to time contact you by post or e-communications with information
				on offers of goods and services, brochures, new products, forthcoming events or competitions from our
				holiday divisions and our holiday group companies. If you have not already done so,  by providing
				your address, e-mail and phone number you agree to its use.
				</p>
				<p  style="padding-right:10px;">
						<input id="chkTuiMarketingAllowed" type="checkbox" name="tuiMarketingAllowed" value="true"  onclick="setChkTuiMarketingAllowed();"/>
 If you would not like to receive <b>e-communications</b> including information on discounts from Thomson Airways, please <b><u>tick</u></b> this box.
 </p>
 <p  style="padding-right:10px;">

						<input id="chkThirdPartyMarketingAllowed" type="checkbox" name="thirdPartyMarketingAllowed" value="true"
                           onclick="setChkThirdPartyMarketingAllowed();"/>
 Our business partners and carefully selected companies outside our holiday group would like to send you information about their products and services <b>by post</b>. If you would not like to hear from them, please <b><u>tick</u></b> this box.


			</p>
		</div>
			<div class="shadedBoxAttnBottom" style="margin-left:-10px;"></div>

	</li>
	<li>Please note that all members of your party require valid passports and any applicable visas before travelling. Visit the Foreign Office website at <a href="http://www.fco.gov.uk/travel" target="_blank"> http://www.fco.gov.uk/travel </a>  to see visa and travel advice.</li>
	<li>If you have special needs requirements on your flight, please call us on <b>0871 200 7799</b> prior to booking.</li>
	<li>To view the notice summarising the liability rules applied by Community air carriers as required by Community legislation and the Montreal Convention, please view the Air Passenger Notice.</li>
	<c:if test="${(bookingComponent.nonPaymentData['atol_protected']!= null) && (bookingComponent.nonPaymentData['atol_protected']) && (bookingComponent.nonPaymentData['atol_date']!=null )  }">
		<c:set var="atoldate" value="${(bookingComponent.nonPaymentData['atol_date'])}"/>
		<c:set var="temp" value="${outBoundFlight_operatingAirlineCode}"/>
		<c:if test="${!fn:contains(temp,'TOM')}">
	 			 <fmt:setTimeZone value="Europe/London" scope="session"/>
				 <c:set var="nowOct" value="${atoldate} "/>
				 <fmt:parseDate var="nowOct" value="${nowOct}" type="DATE" pattern="dd MMM yyyy"/>
				 <jsp:useBean id="now" class="java.util.Date" />
				 <fmt:formatDate value="${now}" var="now" pattern="dd MMM yyyy" />
				 <fmt:parseDate var="now" value="${now}" type="DATE" pattern="dd MMM yyyy"/>
				<c:if test="${ now gt nowOct}">
					<li>
					  This booking is authorised under the ATOL licence of TUI UK Limited, ATOL 2524, and is protected under the ATOL scheme, as set out in the ATOL certificate to be supplied.
					</li>
				</c:if>

		</c:if>
	</c:if>
	</ul>
</div>
<div class="shadedBoxAttn">
<table cellspacing="2" cellpadding="0" border="0">
	<tbody>
		<tr><td valign="top"><input
				id="termAndCondition"
				type="checkbox"
				name="termAndCondition" />  <input id="tourOperatorTermsAccepted"
                type="hidden" name="tourOperatorTermsAccepted"></td>
			<td valign="top">
			<p> I confirm that I have read and accept the <a class="hasPopup"
				onclick="window.open('${bookingComponent.nonPaymentData['tAndCUrl']}','conditions','width=570,height=550,scrollbars');"
				href="javascript:void(0);">Booking Conditions</a> , <a
				class="hasPopup"
			onclick="window.open('http://www.firstchoice.co.uk/our-policies/flight-conditions-of-carriage/index.html','COC','width=595,height=795,scrollbars,resizable');"
				href="javascript:void(0);">Conditions of Carriage</a> , <a
				class="hasPopup"
			   onclick="window.open('http://www.firstchoice.co.uk/our-policies/privacy-policy','privacy','width=595,height=795,scrollbars,resizable');"
				href="javascript:void(0);">Privacy Policy</a> and Baggage check-in safety information.</p>
			<strong> Please tick this box and continue .</strong></td>
		</tr>
	</tbody>
</table>
</div>

<div class="shadedBoxAttnBottom"></div>
<div id="fareRules" class="control">
<ul>
<li>
	  All transactions are conducted over our secure server.
</li>
</ul>
</div>
<!--Session Time out popup-->
<div id="sessionTime" class="posFix">
	<div id="sessionTimeTextbox" class="sessionTimeText">
		<div class="session_dialog_title fl">
					 <div class="sessionText fl">Are you still there?</div>
					 <a style="color:#73afdc" class="remove fr" href="#"
						onClick="closesessionTime()">close</a>
		</div>
		<div class="session_dialog_timeOut_content_container fl">
				<div class="SesTimeoutImg fl">
						<div class="sessionoutWarnClock fl"></div>
						<div class="clear"></div>
						<span class="sesDisTime">Timeout in</span>
						<span id="sessiontimeDisplay" class="sesDisTime"></span>
						<div class="clear"></div>
				</div>
				<div class="session_dialog_timeOut_content" style="line-height:21px">
					<p style="font-size:15.5px">This page will soon be timing out for security reasons. Let us know that <br/> you're still here , otherwise you'll have to re-enter your details.</p>
				</div>
				<div class="clear"></div>
				<div class="sessionTimeTextbox_border-divider"></div>
		</div>
		<div class="activestate fr">
					 <a class="button fr" href="#"
						onClick="activestate()" style="color:#000">I'm still here</a>
		</div>
	</div>
</div>
	
<div class="posFix" id="sessionTimeout">
	<div class="sessionTimeOutText" id="sessionTimeOutTextbox">
		<div class="session_dialog_title fl">
			 <div class="sessionText fl">Sorry, this page has timed out!</div>
			 <a style="color:#73afdc" onclick="reloadPage()" class="remove fr">close</a>
		</div>
		<div class="session_dialog_timeOut_content_container fl">
			<div class="sessiontimeout_content fl" style="line-height:20px">
			Unfortunately this page has timed out for security reasons. Don't worry, we're not going to make you start over again, but we do need you to re-enter your details on this page. If you don't want to continue you can go to the <a style="color:#73afdc;text-decoration:none" href="#" onclick="homepage()">homepage</a>.
			</div>
			<div class="clear"></div>
			<div class="border-divider_sessiontimeout"></div>
		</div>
		<div class="activestate fr">
			 <a onclick="reloadPage()"  class="button fr">ok</a>
		</div>
	</div>
</div>
<div id="modal" class="web_dialog_overlay"></div>

</html>