<%@include file="/common/commonTagLibs.jspf"%>
<c:set var ="nonPaymentData" value="${bookingComponent.nonPaymentData}" scope="session"/>
<h2 class="price">Pricing</h2>
<div class="shadedBoxNormal">
<table cellspacing="0" cellpadding="0" border="0" width="480">
<tbody>
		<c:forEach var="priceComponent" items="${bookingComponent.priceComponents}">
			   <c:choose>
				  <c:when test="${fn:contains(priceComponent.itemDescription, 'Round Trip')}">
				   <tr>
			<td colspan="5">
					 <h3> <c:out value="${priceComponent.itemDescription}"/></h3>
					 </td></tr>
				  </c:when>
				  <c:when test="${fn:contains(priceComponent.itemDescription, 'Outgoing')}">
               <tr>
         <td colspan="5">
                <h3> <c:out value="Departing (one-way)"/></h3>
                </td></tr>
              </c:when>
				  <c:when test="${fn:contains(priceComponent.itemDescription,'Extras')}">
				  <tr>
				<td colspan="5">
					<h3> <c:out value="${priceComponent.itemDescription}"/></h3>
					 </td></tr>
				  </c:when>
			   </c:choose>
				<c:if test="${priceComponent.itemDescription!='Total Price'
				&& priceComponent.itemDescription!='Round trip'
				&& priceComponent.itemDescription!='Outgoing'
				&& priceComponent.itemDescription!='Extras'}">
					<tr>
						<c:choose>
						   <c:when test="${fn:contains(priceComponent.itemDescription,'Taxes & charges')}">
							  <td nowrap="" colspan="2"><c:out value="${priceComponent.itemDescription}" escapeXml="false"/></td>
							  <td nowrap>
							   <c:set var="taxDetails" value=""/>
								 <c:set var="counter" value="${bookingComponent.nonPaymentData['tax_count']}"/>
								 <c:set var="outGoingRoundTripCaption" value="${bookingComponent.nonPaymentData['pricing_caption_0']}"/>
								 <c:choose>
									<c:when test="${fn:contains(outGoingRoundTripCaption,'Outgoing')}">
									   <c:set var="taxDetails" value="${taxDetails} (one-way):\n" />
									</c:when>
									<c:otherwise>
									   <c:set var="taxDetails" value="${taxDetails} (return):\n" />
									</c:otherwise>
								 </c:choose>
								 <c:set var="taxDetails" value="${taxDetails} Fare Tax and Fees\n" />
								 <c:forEach begin="0" end="${counter - 1}" varStatus="taxDsc">
									<c:set var="dscKey" value="tax_dsc_${taxDsc.index}"/>
									<c:set var="taxDescription" value="${bookingComponent.nonPaymentData[dscKey]}"/>
									<c:set var="amountKey" value="tax_amt_${taxDsc.index}"/>
									<c:choose>
									   <c:when test="${fn:contains(taxDescription,'Total Tax & Fees')}">
										  <c:set var="taxDetails" value="${taxDetails} \n" />
									   </c:when>
									</c:choose>
									<c:set var="taxDetails" value="${taxDetails} ${currency}" />
								  <c:set var="taxDetails" value="${taxDetails} ${bookingComponent.nonPaymentData[amountKey]}   " />
									<c:set var="taxDetails" value="${taxDetails} ${taxDescription} \n" />
								 </c:forEach>
							    <a class="hasPopup" onclick="javascript:feesTaxes_breakDowns('${taxDetails}')" href="javascript:void(0);">details</a>
							  </td>
						   </c:when>
						   <c:otherwise>
							  <td nowrap="" colspan="3">
              <c:if test="${priceComponent.quantity>0}">
			  <c:out value="${priceComponent.quantity}" escapeXml="false"/>
                          </c:if>

					 <c:choose>
                 <c:when test="${fn:contains(priceComponent.itemDescription,'Round Trip')}">
                 </c:when>
                 <c:otherwise>
                   <c:out value="${priceComponent.itemDescription}" escapeXml="false"/>
                 </c:otherwise>
              </c:choose>
					</td>
						   </c:otherwise>
						</c:choose>
						<c:if test="${priceComponent.amount.amount>=0 && priceComponent.itemDescription !=null}">
						   <td><c:out value="${currency}" escapeXml="false"/></td>
						   <td nowrap="" align="right">
								<fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
								 minFractionDigits="2" pattern="#,##,###.##"/>
						   </td>
						</c:if>


					</tr>
				</c:if>
		</c:forEach>
		<tr>
			<td>
				<img id="spacer20" height="1" width="20" src="/cms-cps/fcfo/images/o.gif"/>
			</td>
			<td>
				<img id="spacer210" height="1" width="210" src="/cms-cps/fcfo/images/o.gif"/>
			</td>
			<td>
				<img id="spacer100" height="1" width="100" src="/cms-cps/fcfo/images/o.gif"/>
			</td>
			<td>
				<img id="spacer30" height="1" width="30" src="/cms-cps/fcfo/images/o.gif"/>
			</td>
			<td>
				<img id="spacer100" height="1" width="100" src="/cms-cps/fcfo/images/o.gif"/>
			</td>
		</tr>
	</tbody>
	</table>
	<table cellspacing="0" cellpadding="0" border="0" width="480">
	<tbody>
	<tr>
	<td>
	<img id="spacer20" height="1" width="20" src="/cms-cps/fcfo/images/o.gif"/>
	</td>
	<td>
	<img id="spacer210" height="1" width="210" src="/cms-cps/fcfo/images/o.gif"/>
	</td>
	<td>
	<img id="spacer100" height="1" width="100" src="/cms-cps/fcfo/images/o.gif"/>
	</td>
	<td>
	<img id="spacer30" height="1" width="30" src="/cms-cps/fcfo/images/o.gif"/>
	</td>
	<td>
	<img id="spacer100" height="1" width="100" src="/cms-cps/fcfo/images/o.gif"/>
	</td>
	</tr>
 <tr>
	  <td colspan="5">

	    <div class="rowseparator">&nbsp;</div>
	  </td>
	</tr>
	<tr>
	  <td colspan="3" nowrap="nowrap" id="creditCardSurcharge"></td>
	  <td id='currencySymbol'></td>
	  <td align="right" nowrap="nowrap" id="surcharge"></td>

	</tr>

<tr>
<td colspan="5">
<div class="rowseparator"> </div>
</td>
</tr>

<tr>

<td nowrap="" style="font-size: 18px;" colspan="3">

<b>Total Price</b>

</td>
<td style="color: rgb(235, 13, 139); font-size: 18px;">
<b><c:out value="${currency}" escapeXml="false"/></b>
</td>
<td nowrap="" align="right" style="color: rgb(235, 13, 139); font-size: 18px;" id="calculatedTotalAmount">
<b>
<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2"
              minFractionDigits="2" pattern="#,##,###.##"/>
</b>
</td>

</tr>

</tbody>
</table>
</div>
<div class="shadedBoxNormalBottom"></div>
