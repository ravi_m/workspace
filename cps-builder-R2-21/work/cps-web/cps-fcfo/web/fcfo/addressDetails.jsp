<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<h2 class="leadpax">Card Holder Address Details</h2>
<div class="control">
To help ensure your card details remain secure,please confirm the address of the cardholder.
If this is same as lead passenger address, you just need to check the box.
<table cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="165">
				<div class="rowseparator" />
			</td>
			<td width="190">
				<div class="rowseparator" />
			</td>
			<td width="150">
				<div class="rowseparator" />
			</td>
		</tr>
		<tr>
			<td class="rightpadding" nowrap="" width="120"></td>
			<td class="rightpadding" width="300px">
               <input type = "checkbox" onclick="autoCompleteAddress()"/> Same address as the lead passenger
			</td>
			<td valign="top" rowspan="5" />
		</tr>
		<tr>
			<td colspan="3">
			<div class="rowseparator" />
			</td>
		</tr>
		<tr>
			<td class="rightpadding" nowrap="">Street Address*</td>
			<td nowrap="" colspan="2">
                     <input type="textfield" style="width:183px" id="cardHolderAddress_address1"
                      regexerror="Please enter only English alphanumeric characters or './()-& in the address lines."
                      regex="^[A-Za-z0-9'\./()\-& ]+$"
                      required="true"
                      requirederror="Street Address Field 1 is required." maxlength="25"
                      name="payment_0_street_address1"
                      value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/>
		      </td>
		</tr>
		<tr>
			<td colspan="2">
			<div class="rowseparator" />
			</td>
		</tr>
		<tr>
			<td class="rightpadding" valign="top"></td>
			<td colspan="2">
			   <input type="text" style="width: 183px;" id="cardHolderAddress_address2"
			    regexerror="Please enter only English alphanumeric characters or './()-& in the address lines."
			    regex="^[A-Za-z0-9'\./()\-& ]+$"
			    requirederror="Street Address Field 2 is required."
			    maxlength="25"
			    name="payment_0_street_address2"
                      value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="rowseparator" />
			</td>
		</tr>
		<tr>
			<td class="rightpadding" valign="top">Town/City*</td>
			<td colspan="2">
               <input type="text" style="width: 183px;" id="cardHolderAddress_city"
                regexerror="Please enter only English alphabetic characters or in the Town/City field."
                regex="^[A-Za-z ]+$"
                requirederror="Your town/city"
                required="true"
                maxlength="25"
                name="payment_0_street_address3"
                value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>
            </td>
		</tr>

		<tr>
      <td colspan="2">
        <div class="rowseparator"></div>
      </td>
    </tr>



		<tr>
		   <td class="rightpadding" valign="top">Postal code*</td>
		   <td colspan="2"><input
				type="text" style="width: 83px;" id="cardHolderAddress_postCode"
				regexerror="Please enter alphanumeric characters into the field for the postal code only (e.g. 'W2 4DJ')."
				regex="^[A-Za-z0-9 ]+$"
				requirederror="Your postal code"
				required="true"
				maxlength="8"
				name="payment_0_postCode"
                        value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/>
               </td>
		</tr>

		<tr>
			<td colspan="2">
				<div class="rowseparator" />
			</td>
		</tr>

		<tr>
			<td class="rightpadding" valign="top">Country*</td>
			<td colspan="2">
    			<c:set var="selectedCountryCode" value="GB"/>
            <c:choose>
               <c:when test="${not empty bookingInfo.paymentDataMap['payment_0_selectedCountryCode']}">
			      <c:set var="selectedCountryCode" value="${bookingInfo.paymentDataMap['payment_0_selectedCountryCode']}"/>
			   </c:when>
			</c:choose>
			   <select name="payment_0_selectedCountryCode" id="selectedCountry" required="true" requiredError="Your country">
			         <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
				      <c:set var="countryCode" value="${CountriesList.key}" />
					<c:set var="countryName" value="${CountriesList.value}" />
				      <option value='<c:out value="${countryCode}"/>'
					   <c:if test='${selectedCountryCode==countryCode}'>selected='selected'</c:if>>
								   <c:out value="${countryName}" />
					  </option>
			         </c:forEach>
		         </select>
		         <input type="hidden" name="payment_0_selectedCountry" id="payment_0_selectedCountry"  value="" />
                  </td>
		</tr>
	</tbody>
</table>
</div>