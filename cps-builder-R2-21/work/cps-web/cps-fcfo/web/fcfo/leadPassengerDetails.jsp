
<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
	<h2 class="leadPax">Lead passenger details</h2>
		<div class="control">
			<table cellspacing="0" cellpadding="0" border="0" width="500">
				<tbody>
					<tr>
						<td width="165">
							<div class="rowseparator"/>
						</td>
						<td width="190">
							<div class="rowseparator"/>
						</td>
						<td width="150">
							<div class="rowseparator"/>
						</td>
					</tr>
					<tr>
						<td>Street address*</td>
						<td colspan="2">
						 <c:set var="addressLine1" value="personaldetails_addressLine1"/>
							<input id="<c:out value='${addressLine1}'/>" type="text" style="width: 185px;" regexerror="Please enter only English alphanumeric characters or './()- in the address lines." regex="^[A-Za-z0-9'\./()\- ]+$" requirederror="Your street address" required="true" maxlength="25" name="addressLine1" value="${bookingComponent.nonPaymentData['addressLine1']}"/>
							<input type="hidden" name="houseName" value=""></input>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="rowseparator"/>
						</td>
					</tr>
					<tr>
						<td/>
						<td colspan="2">
						<c:set var="addressLine2" value="personaldetails_addressLine2"/>
							<input id="<c:out value='${addressLine2}'/>" type="text" style="width: 185px;" regexerror="Please enter only English alphanumeric characters or './()- in the address lines." regex="^[A-Za-z0-9'\./()\- ]+$" requirederror="Street Address Field 2 is required." maxlength="25" name="addressLine2" value="${bookingComponent.nonPaymentData['addressLine2']}"/>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="rowseparator"/>
						</td>
					</tr>
					<tr>
						<td>Town/city*</td>
						<td colspan="2">
						<c:set var="city" value="personaldetails_city"/>
							<input id="<c:out value='${city}'/>" type="text" style="width: 185px;" regexerror="Please enter only English alphabetic characters or in the Town/City field." regex="^[A-Za-z ]+$" requirederror="Your town/city" required="true" maxlength="25" name="city" value="${bookingComponent.nonPaymentData['city']}"/>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="rowseparator"/>
						</td>
					</tr>
					<tr>
						<td>Postal code*</td>
						<td colspan="2">
						<c:set var="postCode" value="personaldetails_postCode"/>
						<input id="<c:out value='${postCode}'/>" type="text" style="width: 75px;" regexerror="Please enter alphanumeric characters into the field for the postal code only (e.g. 'W2 4DJ')." regex="^[A-Za-z0-9 ]+$" requirederror="Your postal code" required="true" maxlength="8" name="postCode" value="${bookingComponent.nonPaymentData['postCode']}"/>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="rowseparator"/>
						</td>
					</tr>
					<tr>
						<td>Country*</td>
						<td colspan="2">
						<c:set var="selectedCountryCode" value="GB"/>
						<c:choose>
						   <c:when test="${not empty bookingComponent.nonPaymentData['country']}">
						      <c:set var="selectedCountryCode" value="${bookingComponent.nonPaymentData['country']}"/>
						   </c:when>
						</c:choose>
					       <select name="country" id="country" required="true" requiredError="Your country">
						   	 <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
								<c:set var="countryCode" value="${CountriesList.key}" />
								<c:set var="countryName" value="${CountriesList.value}" />
								<option value='<c:out value="${countryCode}"/>'
								  <c:if test='${selectedCountryCode==countryCode}'>selected='selected'</c:if>>
								   <c:out value="${countryName}" />
								</option>
							 </c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<div class="rowseparator"/>
						</td>
					</tr>
					<tr>
						<td>Phone number*</td>
						<td>
							<c:set var="phoneNumber" value="personaldetails_phoneNumber"/>
							<input id="<c:out value='${phoneNumber}'/>" type="text" style="width: 185px;" regexerror="Please enter only numbers in the phone fields." regex="^[0-9]+$" onchange="filterPhoneField(this);" maxlengtherror="Phone number may not exceed 15 digits." requirederror="Your contact phone" required="true" maxlength="15" name="phoneNumber" value="${bookingComponent.nonPaymentData['phoneNumber']}"/>
						</td>		   <input type ="hidden" name='dayTimePhone'  id="dayTimePhone"  />
						<td width="30%" valign="top" rowspan="8">
							<table cellpadding="0" border="0" cellpasing="0">
								<tbody>
									<tr>
										<td width="10"/>
										<td>
											<span class="reddot">
											Enter your phone/fax number(s) without any gaps, hyphens or punctuation e.g.
											<br/>
											004424761234567
											</span>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="rowseparator"/>
						</td>
					</tr>
					<tr>
						<td>Mobile/overseas contact number</td>
						<td>
							<input id="personaldetails_mobilePhone" type="text" style="width: 185px;" regexerror="Please enter only numbers in the phone fields." regex="^[0-9]+$" onchange="filterPhoneField(this);" maxlengtherror="Phone number may not exceed 15 digits." maxlength="15" name="mobilePhone" value="${bookingComponent.nonPaymentData['mobilePhone']}"/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="rowseparator"/>
						</td>
					</tr>
					<tr>
						<td>Fax number</td>
						<td>
							<input id="personaldetails_faxNumber" type="text" style="width: 185px;" regexerror="Please enter only numbers in the phone fields." regex="^[0-9]+$" onchange="filterPhoneField(this);" maxlengtherror="Phone number may not exceed 15 digits." maxlength="15" name="faxNumber" value="${bookingComponent.nonPaymentData['faxNumber']}"/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="rowseparator"/>
						</td>
					</tr>
					<tr>
						<td>Email address*</td>
						<td>
						<c:set var="emailAddress" value="personaldetails_emailAddress"/>
							<input id="<c:out value='${emailAddress}'/>" type="text" style="width: 185px;" validationtypeerror="Email address is not in the correct format" validationtype="Email" requirederror="Your email address" required="true" maxlength="266" name="emailAddress" value="${bookingComponent.nonPaymentData['emailAddress']}"/>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

