<%@include file="/common/commonTagLibs.jspf"%>
<div class="teaserSmall">
<h2 style="padding-left: 6px;">Help</h2>
<p style="padding: 6px;">
<c:set var="curr" value="${bookingInfo.bookingComponent.totalAmount.currency.currencyCode}"/>

<%

String currencyvalue = ((String)pageContext.getAttribute("curr")).trim();
 String[] defaultValue=null;

    String debitCardChargeDetails[] = com.tui.uk.config.ConfReader.getStringValues("FCFO."+currencyvalue+".debitCardChargeDetails" ,defaultValue,",");
    String cardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("FCFO."+currencyvalue+".cardChargeDetails","");
    pageContext.setAttribute("cardChargeDetails", cardChargeDetails, PageContext.REQUEST_SCOPE);
%>

<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<c:set var="cardCharge" value="${cardChargeArray[0]}" />

Please enter your payment information, check it carefully and click Pay and complete booking. Your booking will then be confirmed and any changes will incur an amendment fee.
<br/>
<br/>
Fields marked * are mandatory.
<br/>
<br/>
Please enter passenger names exactly as they appear on the Passport.
  <br/>
<br/>
  Please read our  <a href="javascript:void(0);" onclick="window.open('${bookingComponent.nonPaymentData['tAndCUrl']}','conditions','width=570,height=550,scrollbars');">
 Booking Conditions</a>	  before proceeding.
    <br/>
<br/>

<c:choose>
	<c:when test="${bookingInfo.bookingComponent.totalAmount.currency.currencyCode == 'EUR'}">
						Accepted forms of payment: Visa Credit, MasterCard and Laser.
		</c:when>
		 <c:otherwise>
								Accepted forms of payment: Mastercard, Maestro, Visa, Visa Debit, Electron.
	   </c:otherwise>
</c:choose>

<br/>
<br/>
<c:choose>
	<c:when test="${bookingInfo.bookingComponent.totalAmount.currency.currencyCode == 'EUR'}">
					  <img src="/cms-cps/fcfo/images/visa_card.gif"/><img src="/cms-cps/fcfo/images/mastercard_card.gif"/><img src="/cms-cps/fcfo/images/laser_card.gif"/>
   </c:when>
		 <c:otherwise>
				<img src="/cms-cps/fcfo/images/payment_cards_DPFO.gif"/>
		 </c:otherwise>
</c:choose>
<br/>
<br/>
<c:choose>
	    <c:when test="${bookingInfo.bookingComponent.totalAmount.currency.currencyCode == 'EUR'}">

		</c:when>
		 <c:otherwise>
Please note the fee in our dropdown menu when making payment on this website.
	  <br/>
<br/>
	   </c:otherwise>
</c:choose>


Please make sure all of the cardholder's details are entered accurately so that our payment authorisation process can be completed and your booking confirmed.
<br/>
<br/>


<c:choose>
  <c:when test="${bookingInfo.bookingComponent.totalAmount.currency.currencyCode == 'EUR'}">
    Bookings made by Visa Purchasing, MasterCard Credit or Visa Credit cards will incur a fee of <fmt:formatNumber value="${cardCharge}" type="number" var="confCardCharge" maxFractionDigits="1" minFractionDigits="0"/><c:out value="${confCardCharge}"/>% per transaction. There is no fee for debit cards.
  </c:when>
  <c:otherwise>
Bookings made by Visa Purchasing, MasterCard Credit or Visa Credit cards will incur a fee of <fmt:formatNumber value="${cardCharge}" type="number" var="confCardCharge" maxFractionDigits="1" minFractionDigits="0"/><c:out value="${confCardCharge}"/>% per transaction. If you are booking using Maestro, MasterCard Debit or Visa/ Delta debit cards, there is no charge.
  </c:otherwise>
</c:choose>

</p>
</div>