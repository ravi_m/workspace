// MOUSEOVER HIGHLIGHTING FUNCTIONS ////////////////////////////////////

// event handler to highlight rows on mouseover
function rowMouseOver( event, element ){
	var rowIsSelected = Element.hasClassName( element, "selected" );
	var rowIsPremier = Element.hasClassName( element, "flightOptionsPremiumService" );

	// because IE doesn't support multiple classes in CSS selectors,
	// we have to explicitly test the element state and set the class
	if( rowIsSelected )
	{
		Element.addClassName( element, "mouseOverSelected" );
	}
	else if( rowIsPremier )
	{
		Element.addClassName( element, "mouseOverPremier" );
	}
	else
	{
		Element.addClassName( element, "mouseOver" );
	}
}



// event handler to clear row highlighting when mouse leaves
function rowMouseOut( event, element ){
	rowClearMouseOver( element )
}



// remove any mouseover highlighting from a row
function rowClearMouseOver( element ){
	Element.removeClassName( element, "mouseOver" );
	Element.removeClassName( element, "mouseOverSelected" );
	Element.removeClassName( element, "mouseOverPremier" );
}

function completeEncodeURIComponent(uri) {
	uri = encodeURIComponent(uri);
	var newUri = uri.replace(/\./g, "%2E");
	return newUri;
}

