	function askForPayment()
	{
		return confirm("Thank you.\n\nWe are now ready to complete your reservation.\n\nPlease do not click the Pay, Refresh, Back or Stop buttons until the next page is displayed." +
				"" +
				"\n\nIt may take up to 45 seconds to validate your payment information." +
				" Thank you for your patience.\n\nAll booking details must be correct before proceeding. " +
				"Please click the 'OK' button to begin authorisation.\n\nYour booking will then be final and not reversible.");
	}
	function displayPopUpConverter()
	{
		var url = 'CurrencyConverter.aspx';

		if (!window.converterWindow || converterWindow.closed)
		{
		converterWindow = window.open(url,'converter','width=350,height=300,toolbar=0,status=0,location=0,menubar=0,scrollbars=0,resizable=0');
		}
		else
		{
		converterWindow.open(url,'converter','width=350,height=300,toolbar=0,status=0,location=0,menubar=0,scrollbars=0,resizable=0');
		converterWindow.focus();
		}
	}
	function filterPhoneField(inputField)
	{
		inputField.value = inputField.value.replace(/[^0-9]/g, "");
	}
	function validateAcct()
	{
		var paymentMethodCode = document.getElementById('payment_type_0').value;
		if ( paymentMethodCode == 'SW')
		{
			var accountNumber = document.getElementById('payment_0_cardNumber').value;
			var ccCheckRegExp = /\D/;
			var cardNumbersOnly = accountNumber.replace(/ /g, '');
			if (!ccCheckRegExp.test(cardNumbersOnly))
			{
				var numberProduct;
				var checkSumTotal = 0;

				while (cardNumbersOnly.length < 16)
				{
				cardNumbersOnly = '0' + cardNumbersOnly;
				}

				for(digitCounter = cardNumbersOnly.length - 1; 0 <= digitCounter; digitCounter -= 2 )
				{
					checkSumTotal+=parseInt(cardNumbersOnly.charAt(digitCounter));
					numberProduct=String((cardNumbersOnly.charAt(digitCounter-1)*2));
					for(var productDigitCounter=0;productDigitCounter < numberProduct.length; productDigitCounter++)
					{
					checkSumTotal += parseInt(numberProduct.charAt(productDigitCounter));
					}
				}
				var isValid = (checkSumTotal % 10 == 0);
				if (!isValid)
				{
				alert("Card number is invalid.");
				}

				return isValid;
			}
			else
			{
			return false;
			}
		}
		return true;
	}

	function validateRequiredField()
	 {
		 changePostCodeToUpperCase();
	 	 return true && passengerNamesInputValidation() &&
	 	 personalDetailsInputValidation() &&
		 paymentDetails_Validate() &&
		 contactAgreement_Validate()
			 && issuefieldvalidate() &&  cardAddressValidate();
	}
	var submitterClicked = false;

	function preventDoubleClick()
	{
	if (!submitterClicked)
	{
	submitterClicked = true;
	return true;
	}

	return false;
	}

	<!-- HIDE
	function passengerNamesInputValidation(ValidateEventArgs)
	{
	if (!validate('passenger'))
	{
	return false;
	}
	try
	{
	return true;
	}
	catch (e)
	{
	return false;
	}
	}
	function paymentDetails_Validate(ValidateEventArgs)
	{
	if (!validate('payment'))
	{
	return false;
	}
	var expireMonth = document.getElementById('payment_0_expiryMonth').value;

	var expireYear = document.getElementById('payment_0_expiryYear').value;
	 expireYear = "20" + expireYear;
	if ((expireYear < 2008) || ((expireYear == 2008) && (expireMonth < 12)))
	{
	alert("The expiry date specified has already passed.");
	return false;
	}
	return true;
	}

	function personalDetailsInputValidation(ValidateEventArgs)
	{
	   if (!validate('personaldetails'))
	   {
	      return false;
	   }
	   return true;
	}

	// END HIDE -->

	<!--
	function contactAgreement_Validate(eventArgs)
	{
	if (!document['SkySales']['termAndCondition'].checked)
	{
	alert("\nSorry, but to proceed with your booking you\nmust click checkbox to accept our \nTerms and conditions.\n");
	return false;
	}
	return true;
	}

	 var issueflag=false;
	function formSubmit()
	{
	//   updateEssentialFields();
	document.getElementById("tourOperatorTermsAccepted").value = document.getElementById("termAndCondition").value;
   document.getElementById('dayTimePhone').value = document.getElementById('personaldetails_phoneNumber').value ;
		   var tomcatInstance = document.getElementById("tomcatInstance").value;
		    var token = document.getElementById("token").value;
		    document.SkySales.method="post";
	        document.SkySales.action="/cps/processPayment?b=16000&token="+token+"&tomcat="+tomcatInstance;


	        var sel = document.getElementById('selectedCountry');
	        document.getElementById('payment_0_selectedCountry').value = sel.options[sel.selectedIndex].value;


	   document.getElementById('SkySales').submit();
	}

	function updateEssentialFields()
	{
	   var totalAmountPaid = parseFloat(document.getElementById("payment_0_transamt").value+parseFloat(document.getElementById("payment_0_chargeIdAmount").value)).toFixed(2);
	   document.getElementById("payment_0_transamt").value = document.getElementById("payment_totamtpaid").value;
	   document.getElementById('payment_0_paymentmethod').value = "Dcard";
	   document.getElementById('payment_0_totalTrans').value = 1;
	}
	function feesTaxes_breakDowns(taxDetails)
	{
		var msg = '';
		msg = "Here is a breakdown of the taxes and fees\nthat apply to your purchase." + '\n\n\n';
		alert(msg + taxDetails);
	}


/* updating card charges*/

function updateCardChargeForFCFO()
{
  changePayButton($('payment_type_0').value)
 if(PaymentInfo.totalAmount>0)
  {
	  handleDotNetCardSelection(0);
  }

}

/* updating card charges for down time scenario*/

function updateCardChargeForFCFODisabled()
{
  if(PaymentInfo.totalAmount>0)
  {
	  handleDotNetCardSelection(0);
  }
}
		   /*updating total cost */
function displayFieldsWithValuesForCard()
{

	   	   if(PaymentInfo.selectedCardType!="none" || PaymentInfo.chargeamt!=0)
		   {
						  $('creditCardSurcharge').innerHTML=cardChargeMap.get(PaymentInfo.selectedCardType).split(",")[4] + " card surcharge";
						  if(PaymentInfo.currency1=="EUR")
						  {
							  $('creditCardSurcharge').innerHTML="Fee";
						  }

						  $('currencySymbol').innerHTML= PaymentInfo.currency;
						  if(PaymentInfo.chargeamt == 0)
						  {
						  $('surcharge').innerHTML=calCardCharge;
						  }
						  else
						  {
						  	 $('surcharge').innerHTML=PaymentInfo.chargeamt.toFixed(2);
						  }
		   }
		   else
			{

					     $('creditCardSurcharge').innerHTML="";
						 $('currencySymbol').innerHTML="";
						$('surcharge').innerHTML="";
			}
		  $('totalAmountDue').innerHTML="<b>"+"<strong>&nbsp;&nbsp;"+addCommas(PaymentInfo.calculatedTotalAmount)+"</b>";
		  $('calculatedTotalAmount').innerHTML="<b>"+"<strong>&nbsp;&nbsp;"+addCommas(PaymentInfo.calculatedTotalAmount)+"</b>";

    	  document.getElementById("payment_0_paymenttypecode").value= PaymentInfo.selectedCardType;
		  document.getElementById("total_transamt").value=PaymentInfo.calculatedPayableAmount;

}

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
		return x1 + x2;
}
 function setIssueField()
 {
	 if(PaymentInfo.selectedCardType!='none')
	 {
			  $('payment_0_securityCode').setAttribute('maxlength',getCardDetails(PaymentInfo.selectedCardType)[1]);
			   $('payment_0_securityCode').setAttribute('minlength',getCardDetails(PaymentInfo.selectedCardType)[0]);

			  if(getCardDetails(PaymentInfo.selectedCardType)[2] == 'true')
			  {
					 document.getElementById("issuerequiredlabel").style.display="";
					 issueflag=true;
			 }
			 else
			 {
				  issueflag=false;
					document.getElementById("issuerequiredlabel").style.display="none";
			}
	 }
 }



	   function issuefieldvalidate()
 {

 	if(issueflag == true)
	{

		          var val = /^[0-9 ]*$/;
	     if(document.getElementById("payment_0_issueNumber").value.length > 0)
	     {

		      if(!val.test(document.getElementById("payment_0_issueNumber").value))
			 {
				alert("Valid Issue Number Required")
					document.getElementById("payment_0_issueNumber").focus();
				return false;
			 }
		    return true;
		}

	}
	return true;
  }

  function setnewsLetter()
 {
		document.getElementById("newsLetter1").value	 = document.getElementById("newsLetter").checked ;
 }


  /**This function changes the button based on whether the selected card belongs to the
   * Verified by Visa or Mastercard securecode*/
  function changePayButton(cardType)
  {
  	var payButtonDescription = threeDCards.get(cardType);
  	var element = document.getElementsByClassName('Pay')[0];
  	if (payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup")
  	{
  		element.innerHTML = "<img src='/cms-cps/fcfo/images/proceed-to-payment-btn.gif' alt='Confirm with Mastercard Securecode' id='bookingcontinue'  border='0' />";
  	}
  	else
  	{
  		element.innerHTML = "<img border='0' src='/cms-cps/fcfo/images/btn_bookflightnow.gif' id='bookingcontinue' alt='Book flight now'>";
  	}
  }

  /**This function validates the address fields of the card holder*/
  function cardAddressValidate()
  {
	   if (!validate('cardHolderAddress'))
	   {
	      return false;
	   }
	   return true;
  }

  function toggleFCFOOverlay()
  {
    var overlayZIndex = 99;
    var zIndex = 100;
    var prevOverlay;
    var stickyOpened = false;
    jQuery("a.stickyOwner").click(function(e){
      var overlay = "#" + this.id + "Overlay";
  	if (!stickyOpened)
  	{
  		prevOverlay = overlay;
  	}
  	if (prevOverlay != overlay)
  	{
  		jQuery(prevOverlay).hide();
  		stickyOpened = false;
  	}
  	var pos = jQuery("#"+this.id).offset();
  	var left = parseInt((1*pos.left+3),10);
  	var top = parseInt((1*pos.top+45),10);
  	jQuery(overlay).show();
  	jQuery(overlay).css("left",left);
  	jQuery(overlay).css("top",top);
  	prevOverlay = overlay;
  	stickyOpened = true;
  	jQuery(overlay + ".genericOverlay").css("z-index",zIndex);
  	zIndex++;

  	if (jQuery(overlay).parent(".overlay") != null){
  	  jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
  	  overlayZIndex++;
  	}
      return false;
    });

    jQuery("a.close").click(function(){
      var overlay = this.id.replace("Close","Overlay");
      jQuery("#" + overlay).hide();
      return false;
    });
  }

  function autoCompleteAddress()
  {
     jQuery('#cardHolderAddress_address1').val( jQuery('#personaldetails_addressLine1').val());
     jQuery('#cardHolderAddress_address2').val(jQuery('#personaldetails_addressLine2').val());
     jQuery('#cardHolderAddress_city').val(jQuery('#personaldetails_city').val());
     jQuery('#cardHolderAddress_postCode').val(jQuery('#personaldetails_postCode').val());
     jQuery('#selectedCountry').val(jQuery('#country').val());
  }

  //This function changes the postcode to upper case since the server side regex validates only capital letters.
  function changePostCodeToUpperCase()
  {
	  var postcode = document.getElementById('cardHolderAddress_postCode').value;
	  document.getElementById('cardHolderAddress_postCode').value = postcode.toUpperCase();
  }

 function showHiddenInformation()
  {
		if(document.getElementById('dataprotectionnoticediv').style.display=="none")
		document.getElementById('dataprotectionnoticediv').style.display="block";
		else
			 document.getElementById('dataprotectionnoticediv').style.display="none";
  }

 function setChkTuiMarketingAllowed()
 {
		if(document.getElementById("chkTuiMarketingAllowed").checked ==true)
			document.getElementById("chkTuiMarketingAllowed").value	 = "false" ;
 }

 function setChkThirdPartyMarketingAllowed()
 {

		if(document.getElementById("chkThirdPartyMarketingAllowed").checked ==true)
			document.getElementById("chkThirdPartyMarketingAllowed").value	 = "false" ;

 }
