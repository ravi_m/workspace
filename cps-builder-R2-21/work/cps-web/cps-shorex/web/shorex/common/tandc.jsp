<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>
<h2>
   <img id="hl_farerules" src="/cms-cps/tfly/images/tc.gif"
      alt="Fare rules" border="0">
</h2>
 <div class="shadedBoxAttn">
  <div>Please note that Thomsonfly's cancellation / amendment charges are applicable once your
			booking has been confirmed. By clicking 'continue' you are confirming your booking and
			you agree for your card to be debited with all amounts due. All transcations are conducted
			over our secure server.  </div>
			<br/>
    <table border="0" cellpadding="0" cellspacing="2">
		
	<tr>
	       
          <td valign="top">
             <input id="termAndCondition"
                type="checkbox" name="termAndCondition">	    <input id="tourOperatorTermsAccepted"
                type="hidden" name="tourOperatorTermsAccepted">
          </td>
          <td valign="top">
             <p>
                I have read and accept Thomsonfly's
                 <a	href="javascript:void(0);"onclick="window.open('${bookingComponent.termsAndCondition.relativeTAndCUrl}','conditions','width=570,height=550,scrollbars');">Booking Conditions</a> &amp;
                <a href="javascript:void(0);" onclick="window.open('http://flights.thomson.co.uk/en/633.html','privacy','width=570,height=550,scrollbars');">Privacy Policy</a>.
             </p>
             <strong> Please tick this box and continue.</strong>
          </td>
       </tr>
    </table>
 </div>
 <div class="shadedBoxAttnBottom"></div>