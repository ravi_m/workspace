<%@include file="/common/commonTagLibs.jspf"%>
<html>
<head>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<version-tag:version/>
</head>
<body>
	<c:choose>
      <c:when test="${not empty sessionScope=='true'}">
	     <jsp:include page="technicalerror.jsp"></jsp:include>
	   </c:when>
	   <c:otherwise>
	     <c:redirect url="http://flights.thomson.co.uk/en/index.html" />
	   </c:otherwise>
	</c:choose>
</body>
</html>