<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Thomson - Cruise Holidays and Cruise Deals - Cruises that suit your needs</title>
<%@ page isELIgnored="false" %>
<%@include file="/common/commonTagLibs.jspf"%>
<!-- Modified to add ensighten in header section -->
 <%@include file="tag.jsp"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>
<c:set var="TUIHeaderSwitch" value="${fn:toUpperCase(bookingComponent.nonPaymentData['TUIHeaderSwitch'])}"/>
<%
	String applyCreditCardSurcharge=com.tui.uk.config.ConfReader.getConfEntry("B2CShorex.applyCreditCardSurcharge" , null);
	pageContext.setAttribute("applyCreditCardSurcharge", applyCreditCardSurcharge, PageContext.REQUEST_SCOPE);
%>
<script>
applyCreditCardSurcharge="<%=applyCreditCardSurcharge%>";
</script>
<c:choose>

   <c:when test="${TUIHeaderSwitch eq 'ON'}">
      <c:set value="www.tui.co.uk" var="tuiFlagLink" />
   </c:when>
   <c:otherwise>
      <c:set value="www.thomson.co.uk" var="tuiFlagLink" />
   </c:otherwise>
</c:choose>


<c:choose>
<c:when test="${not empty bookingComponent.nonPaymentData['b2cshorex_home_url']}">
<c:set var="b2cshorex_home_url" value="${bookingComponent.nonPaymentData['b2cshorex_home_url']}"/>
</c:when>

<c:otherwise>
<c:set var="b2cshorex_home_url" value="https://${tuiFlagLink}/cruise/shore-excursions.html"/>

</c:otherwise>

</c:choose>








<jsp:useBean id="now" class="java.util.Date" /><fmt:formatDate var ="currentyear" type="date" value="${now}" pattern="yyyy"/>
<jsp:include page="css.jspf"></jsp:include>
<jsp:include page="javascript.jspf"></jsp:include>
<%@include file="configSettings.jspf" %>
<c:set var="samsung" value="http://www.gmail.com"/>

<script type="text/javascript">
   var  newHoliday  = "<c:out value='${bookingInfo.newHoliday}'/>";

   var streetaddress1="${bookingComponent.nonPaymentData['street_address1']}";
   var streetaddress2="${bookingComponent.nonPaymentData['street_address2']}";
   var streetaddress2_1="${bookingComponent.nonPaymentData['additional_address_line2_1']}";
   var streetaddress2_2="${bookingComponent.nonPaymentData['additional_address_line2_2']}";
   var streetaddress3="${bookingComponent.nonPaymentData['street_address3']}";
   var streetaddress4="${bookingComponent.nonPaymentData['street_address2']}";
   var county="${bookingComponent.nonPaymentData['county']}";
   var cardBillingPostcode="${bookingComponent.nonPaymentData['cardBillingPostcode']}";
   var selectedcountryforleadpassenger="${bookingComponent.nonPaymentData['selectedCountryCode']}";
</script>

<script type="text/javascript">


   var cardChargeMap = new Map();
   var pleaseSelect = new Array();
	cardChargeMap.put('none', pleaseSelect);

	  <c:forEach var="cardCharges" items="${bookingInfo.cardChargeMap}">
  		var cardChargeInfo = new Array();
		     cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');
     </c:forEach>


var cardDetails = new Map();
 <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">
			    var cardCharge_Max_Min_Issue = new Array();
        	         	   cardCharge_Max_Min_Issue.push('${cardDetails.cardType.securityCodeLength}');
						    cardCharge_Max_Min_Issue.push('${cardDetails.cardType.minSecurityCodeLength}');
							 cardCharge_Max_Min_Issue.push('${cardDetails.cardType.isIssueNumberRequired}');

			  cardDetails.put('${cardDetails.paymentCode}',cardCharge_Max_Min_Issue);

         </c:forEach>


        //  The following map stores the pay button labels in case of 3D cards.


  </script>


      <div class="pageContainer">
      <jsp:include page="/shorex/b2c/cookielegislation.jsp">
       <jsp:param name="paramSwitch" value='${TUIHeaderSwitch}'/>
      
      </jsp:include>
         <div id="headerSection">
         <%@include file="header.jspf" %>
         <!--
                     --></div>
         <!--end of HeaderSection-->

         <div id="contentSection">
         <jsp:include page="summaryPanel.jsp"></jsp:include>
                        <!--end of left panel-->

            <div id="contentCol2">
			   <div id="headings">
			      <h1>Payment details</h1>
			   </div>
			    <!-- Booking Flow Bar starts -->
<jsp:include page="breadCrumb.jsp"></jsp:include>
               <!-- Booking Flow Bar ends -->
               <div class="clearb"></div>

               <form id="paymentForm" method="post" action="/cps/processPayment?b=<c:out value='${param.b}' />&token=<c:out value='${param.token}' />&tomcat=<c:out value='${param.tomcat}' />" novalidate autocomplete="off">
	               	<%----------------------Payment fields used in scripting etc -----------------------------------------%>
					<c:forEach var="cardCharge" items="${bookingComponent.cardChargesMap}">
					  <c:out value='<input type="hidden" id="${cardCharge.key}_charge" value="${cardCharge.value} "/>' escapeXml='false'/>
					</c:forEach>
					<c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
					  <c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled' name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>
					  <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>
					  <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>
					  <c:out value="<input type='hidden' value='${paymentType.cardType.minSecurityCodeLength}'  id='${paymentType.paymentCode}_minSecurityCodeLength' name='${paymentType.paymentCode}_minSecurityCodeLength'/>" escapeXml="false"/>
					  <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>
					</c:forEach>
					<%-----------------*****END Payment fields used in scripting etc *****--------------------------------%>

               <%@include file="topError.jspf" %>
                  <!-- Payment -->
                  <div class="detailsPanel" id="paymentDetailsPanel">
                     <div class="top">
                        <div class="right"></div>
                     </div>
                     <div class="body">
                        <div class="right">
                           <div class="content">
                           <jsp:include page="paymentDetails.jsp"></jsp:include>
                           <!--

                              --><div class="section dashedBlackBorderBottom">
                              <jsp:include page="cardHolderAddress.jsp"></jsp:include>
                              <!--

                              --></div><!--End of Section 2 -->
                              <%@include file ="confirmBooking.jspf" %>
                           </div>

                        </div>
				     </div>
                     <div class="bottom">
                        <div class="right"></div>
                     </div>
                  </div>
		          <div class="pageControls">
                     <a class="backPage" href="<c:out value="${bookingComponent.nonPaymentData['pre_payment_url']}"/>" title="Back to passenger details">Return to passenger details</a>
                  </div>
               </form>
            </div>
            <!--end of contentCol2-->
			<div class="clearb"></div>
         </div><!--end of ContentSection-->
     <%@include file="footer.jspf" %>

         </div>
      <!--end of PageContainer-->

		<!--start overlay section-->

      <!-- Start overlay cvv -->
		<div class="genericOverlay overlay win" id="cvvDetailsOverlay">
			<div class="top">
				<div class="right"></div>
			</div>
			<div class="body">
				<div class="right">
					<div class="content">
						<div class="popupTitle">
							<h3>CVV number</h3>
							<a class="close" id="cvvDetailsClose" href="#" title="Close overlay">Close</a>
						</div>
						<p id="blinky">CVV number is your last 3 digits on the signature
						strip on the reverse of your card.</p>
					</div>
				</div>
			</div>
			<div class="bottom">
				<div class="right"></div>
			</div>
		</div>
		<!--end overlay-->

		<!--start overlay visa-->
		<div class="genericOverlay overlay win" id="visaDetailsOverlay">
			<div class="top">
				<div class="right"></div>
			</div>
			<div class="body">
				<div class="right">
				<div class="content">
					<div class="popupTitle">
						<h3>Verified by Visa</h3>
						<a class="close" id="visaDetailsClose" href="#" title="Close overlay">Close</a>
					</div>
						<p>We've introduced Verified by Visa - a security programme created by Visa to give you peace of mind when shopping online. All you need to do is register once whilst paying on our website. You'll be asked to set up a password that you can then use for any future payments you make with your card on participating websites.</p>
					</div>
				</div>
			</div>
			<div class="bottom">
				<div class="right"></div>
			</div>
		</div>
		<!--end overlay-->
		<!--start overlay mastercard-->
		<div class="genericOverlay overlay win" id="masterCardDetailsOverlay">
			<div class="top">
				<div class="right"></div>
			</div>
			<div class="body">
				<div class="right">
					<div class="content">
						<div class="popupTitle">
							<h3>MasterCard SecureCode</h3>
							<a class="close" id="masterCardDetailsClose" href="#" title="Close overlay">Close</a>
						</div>
						<p>We've introduced MasterCard SecureCode - a security programme created by MasterCard to give you peace of mind when shopping online. All you need to do is register once whilst paying on our website. You'll be asked to set up a password that you can then use for any future payments you make with your card on participating websites.</p>
					</div>
				</div>
			</div>
			<div class="bottom">
				<div class="right"></div>
			</div>
		</div>
		<!--end overlay-->
		<!--end overlay section-->

</body>
</html>
