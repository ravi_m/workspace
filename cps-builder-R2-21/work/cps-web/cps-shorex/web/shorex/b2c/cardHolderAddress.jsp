<%@include file="/common/commonTagLibs.jspf"%>
<%@ page isELIgnored="false" %>
                                 <h2 id="leadDetails"><span>Card holder address details</span></h2><div class="sectionContent">To help ensure your card details remain secure, please confirm the address of the cardholder.
                                    <ul class="controlGroups">
                                       <li class="controlGroup">
                                          <div>
                                             <input type="checkbox" wf_type="checkbox" name="autoCheckCardAddress" id="useAddress" onclick="AddressPopulationHandler.handle();"><span style="">Use same address as lead passenger</span>
                                          </div>
									   </li>
									   <li class="cardHouseNameErrorContainer">
                                          <p class="fieldLabel"><label>House no./name</label></p>
                                          <div class="controls">
                                             <input type="text" name="payment_0_street_address1" id="cardHouseName" maxlength="25" class="inputWhole" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/>" requiredErrorMsg="Please enter your House Name/No" wf_required="required" alt="CardHolder House Name/No"> <span class="mandatory">*</span>
									      </div>
									   </li>
                                       <li class="cardAddress1ErrorContainer">
                                          <p class="fieldLabel"><label>Address</label></p>
                                          <div class="controls">
                                             <input type="text" wf_type="alpha" name="payment_0_street_address2" id="cardAddress1" maxlength="25" class="textfield" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>" requiredErrorMsg="Please enter a Street Address" alphaErrorMsg="Please enter a valid Street Address" wf_required="required" alt="CardHolder Street Address"> <span class="mandatory">*</span>
									      </div>
									   </li>
                                       <li class="cardAddress2ErrorContainer">
									      <div class="controls" style="margin-left:123px;">
									         <input type="text" name="payment_0_additional_address_line1" id="cardAddress2" maxlength="25" class="textfield" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_additional_address_line1']}"/>">
                                          </div>
                                       </li>
									   <li class="controlGroup">
									      <div class="controls" style="margin-left:123px;">
									         <input type="text" name="payment_0_additional_address_line2" id="cardAddress3" maxlength="25" class="textfield" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_additional_address_line2']}"/>">
                                          </div>
                                       </li>
                                       <li class="cardTownCityErrorContainer">
                                          <p class="fieldLabel"><label>Town/City</label></p>
                                          <div class="controls">
                                             <input type="text" wf_type="alpha" name="payment_0_street_address3" id="cardTownCity" maxlength="25" class="textfield" alt="CardHolder Town/City" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>" requiredErrorMsg="Please enter a Town/City" alphaErrorMsg="Please enter a valid Town/City" wf_required="required"><span class="mandatory">*</span>
                                          </div>
                                       </li>
									   <li class="cardCountyErrorContainer">
                                          <p class="fieldLabel"><label>County</label></p>
                                          <div class="controls">
                                             <input type="text" name="payment_0_street_address4" id="cardCounty" maxlength="25" class="textfield" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address4']}"/>" alt="County" >
                                          </div>
                                       </li>

 <li class="cardPostCodeErrorContainer">
                                          <p class="fieldLabel"><label>Post code</label></p>
                                          <div class="controls">
                                             <input name="payment_0_postCode" id="cardPostCode" type="text" maxlength="8" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/>" class="inputWhole" alt="Post Code" requiredErrorMsg="Please enter a Post Code" wf_required="required"><span class="mandatory">*</span>
                                          </div>
									   </li>

              						   <li class="controlGroup">
                                          <p class="fieldLabel"><label>Country</label></p>
                                          <div class="controls">
									          <select name="payment_0_selectedCountryCode" id="payment_0_selectedCountryCode" wf_required="true" requiredErrorMsg="Your country" class="inputWhole longField" alt="Your Country">
 <c:choose>
               <c:when test="${bookingComponent.nonPaymentData['payment_0_selectedCountryCode'] != null}">
                  <c:set var="selectedCountryCode" value="${bookingComponent.nonPaymentData['payment_0_selectedCountryCode']}" scope="page"/>
               </c:when>
               <c:otherwise>
                  <c:set var="selectedCountryCode" value="GB" scope="page"/>
               </c:otherwise>
            </c:choose>
             <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
               <c:set var="countryCode" value="${CountriesList.key}" />
               <c:set var="countryName" value="${CountriesList.value}" />
               <option value='<c:out value="${countryCode}"/>'
                  <c:if test='${countryCode==selectedCountryCode}'>selected='selected'</c:if>>
                  <c:out value="${countryName}" />
               </option>
            </c:forEach>



		                                     </select>
		                                     <input type="hidden" id="payment_0_selectedCountry" name="payment_0_selectedCountry" value="" />
										     <span class="mandatory">*</span>
									      </div>
                                       </li>
									   <%--<li class="controlGroup">By clicking <strong>'Pay now'</strong> you are confirming your booking and your card will be debited with the due amount. All transactions are deducted over our secure server. Please note that cancellation/amendment charges are applicable once your excursion has been confirmed.</li>--%>
							        </ul>
						         </div>

