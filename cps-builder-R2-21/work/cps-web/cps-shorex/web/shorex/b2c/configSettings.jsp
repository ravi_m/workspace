<%-- ************************************Essential JSP settings. to be used through out the page ******************************** --%>

<%@ page isELIgnored="false" %>
<%@include file="/common/commonTagLibs.jspf"%>


<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<c:choose>
   <c:when test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
        <c:set var="currencySymbol" value="&pound;" scope="request"/>
        <c:set var="currencySymbolUnicode" value="\u00A3"/>
   </c:when>
   <c:when test="${bookingComponent.totalAmount.currency.currencyCode=='EUR'}">
        <c:set var="currencySymbol" value="&euro;" scope="request"/>
        <c:set var="currencySymbolUnicode" value="\u20AC"/>
   </c:when>
   <c:otherwise>
        <c:set var="currencySymbol" value="${bookingComponent.totalAmount.currency.currencyCode}" scope="request"/>
   </c:otherwise>
</c:choose>

<c:set var="visiturl" value="yes" scope="session" />
<c:set var="clientapp" value="${bookingComponent.clientApplication.clientApplicationName}"/>
<c:set var="clientDomainURL" value="${bookingComponent.clientDomainURL}" scope="request"/>

<%
    String cardChargePercent = com.tui.uk.config.ConfReader.getConfEntry("cardChargePercent" , "");
    pageContext.setAttribute("cardChargePercent", cardChargePercent, PageContext.REQUEST_SCOPE);
%>

<%
    String creditCardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("cardChargeDetails" , "");
    pageContext.setAttribute("creditCardChargeDetails", creditCardChargeDetails, PageContext.REQUEST_SCOPE);
%>

<%
    String debitCardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("debitCardChargeDetails" , "");
    pageContext.setAttribute("debitCardChargeDetails", debitCardChargeDetails, PageContext.REQUEST_SCOPE);
%>

<script type="text/javascript">
  var clientApp = "<c:out value='${clientapp} '/>";
  var clientDomainURL = "<c:out value='${clientDomainURL}'/>";
  var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
  var token = "<c:out value='${param.token}' />" ;
  var newHoliday  = "<c:out value='${bookingInfo.newHoliday}'/>";
  var cardChargePercent = '<c:out value="${cardChargePercent}"/>' ;
  var creditCardChargeDetails = '<c:out value="${creditCardChargeDetails}" />';
  var debitCardChargeDetails = '<c:out value="${debitCardChargeDetails}" />';
  var dispCardText = checkForDispCardText('',3);
<%----------------------Central payment object for using thru out the page----------------------------%>
  var PaymentInfo = {

  totalAmount :"<c:out value='${bookingComponent.totalAmount.amount}'/>",
  payableAmount : "<c:out value='${bookingComponent.payableAmount.amount}'/>",

  calculatedPayableAmount : "<c:out value='${bookingInfo.calculatedPayableAmount.amount}'/>",
  calculatedTotalAmount : "<c:out value='${bookingInfo.calculatedTotalAmount.amount}'/>",
  firstDepositAmount: "<c:out value='${bookingInfo.calculatedTotalAmount.amount}'/>",

  maxCardChargeAmount : "<c:out value='${bookingComponent.maxCardCharge}'/>",

  calculatedDiscount : "<c:out value='${bookingInfo.calculatedDiscount.amount}'/>",

  selectedCardType : null,

  currencySymbol : "<c:out value='${currencySymbolUnicode}' escapeXml='false'/>"
  };

  PaymentInfo.partialPaymentAmount = null;
  <c:if test="${(bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_PEG') && bookingComponent.accommodationSummary.prepay != 'true'}">
  PaymentInfo.payableAmount  = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
  PaymentInfo.partialPaymentAmount = 'true';
  </c:if>

  PaymentInfo.chargePercent = 0.0;

  if(StringUtils.trim(PaymentInfo.calculatedDiscount)=="")
  {
    PaymentInfo.calculatedDiscount = 0.0;
  }
  if(PaymentInfo.maxCardChargeAmount == "")
  {
     PaymentInfo.maxCardChargeAmount = null  ;
  }
<%-----------------*****Central payment object for using thru out the page*****-----------------------%>

<%----------------------Card Charge related config ---------------------------------------------------%>

  var cardChargeMap = new Map();
  cardChargeMap.put('', 0);
  cardChargeMap.put(null, 0);

   <c:forEach var="cardCharges" items="${bookingInfo.cardChargeMap}">
     cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');
   </c:forEach>
<%-----------------*****END Card Charge related config *****------------------------------------------%>

var bookingConstants = {FULL_COST:"fullCost", TOTAL_CLASS:"total"};

  <%-------------------------------Deposit component related config-------------------------------------------------%>
  var depositAmountsMap = new Map();
  <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}" varStatus="status">
    depositAmountsMap.put('<c:out value="${depositComponent.depositType}"/>', StringUtils.trim('<c:out value="${depositComponent.depositAmount.amount}"/>'));
    <c:if test="${status.first}">
      PaymentInfo.firstDepositAmount = '<c:out value="${depositComponent.depositAmount.amount}"/>';
    </c:if>
  <%---------------------Variables for displaying outstanding balance and due date in the deposit options section.----%>
  <c:if test="${depositComponent.depositType == 'deposit'}">
   <fmt:formatNumber var="outstandingBalance" value="${depositComponent.outstandingBalance.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" scope="request"/>
   <fmt:formatDate var="outstandingBalanceDueDate" value="${depositComponent.depositDueDate}" pattern="EE dd MMM yyyy" scope="request"/>
   <fmt:formatNumber var="outstandingBalanceCardCharge" value="${depositComponent.outstandingBalanceCardCharge.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" scope="request"/>
  </c:if>
  <c:if test="${depositComponent.depositType == 'fullCost'}">
    <fmt:formatNumber var="applicableCardChargeForTotalAmount" value="${depositComponent.applicableCardCharge.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" scope="request"/>
    <%------------------Logic added for checking if the deposit options are present or not.Based on the value of this variable either fullPayment or paymentOtions jspf will be rendered.-------------------------------%>
    <c:set var="depositOptionsPresent" scope="request">
     <c:choose>
      <c:when test="${fn:length(bookingComponent.depositComponents) == 1}">false</c:when>
      <c:otherwise>true</c:otherwise>
     </c:choose>
    </c:set>
    <%------------------End of the logic for checking if the deposit options are present or not.------------------------%>
  </c:if>

  <c:if test="${depositComponent.defaultDepositTypeToBeSelected == 'true'}">
     PaymentInfo.depositType = '<c:out value="${depositComponent.depositType}"/>';
     <fmt:formatNumber var="selectedDepositAmount" value="${depositComponent.depositAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"  scope="request" groupingUsed="false"/>
     <fmt:formatNumber var="applicableCardChargeForSelectedAmount" value="${depositComponent.applicableCardCharge.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"  scope="request" groupingUsed="false"/>
     <fmt:formatNumber var="applicableDebitCardChargeForSelectedAmount" value="${depositComponent.applicableDebitCardCharge.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" scope="request" groupingUsed="false"/>
  </c:if>

  </c:forEach>
  depositAmountsMap.put('fullCost', 1*PaymentInfo.totalAmount  - 1*PaymentInfo.calculatedDiscount);
  <%-------------------------------End of Deposit component related config---------------------------------------%>

  var threeDCards = new Map();
  <c:forEach var="threeDCards" items="${bookingInfo.payDescription}">
      threeDCards.put("<c:out value="${threeDCards.key}"/>","<c:out value="${threeDCards.value}"/>");
  </c:forEach>

  $(document).ready(function(){

			var errorMap = {
			    //On submit error messages
				houseNameSubmitErrorrequired: "Please enter your House Name/No",
				addressLine1SubmitErrorrequired: "Please enter a Street Address",
				citySubmitErrorrequired: "Please enter a Town/City",
				postCodeSubmitErrorrequired: "Please enter a Post Code",

				cardTypeSubmitErrorrequired: "Please select a Card Type",
				expiryDateYYSubmitErrorrequired: "Please enter an Expiry Date",
				expiryDateMMSubmitErrorrequired: "Please enter an Expiry Date",
				cardNumberSubmitErrorrequired: "Please enter your Card Number",
				cardNameSubmitErrorrequired: "Please enter Name as it appears on the Card",
				securityCodeSubmitErrorrequired: "Please enter a Security Code",
				cardHouseNameSubmitErrorrequired: "Please enter your House Name/No",
				cardAddress1SubmitErrorrequired: "Please enter a Street Address",
				cardTownCitySubmitErrorrequired: "Please enter a Town/City",
				cardPostCodeSubmitErrorrequired: "Please enter a Post Code",
				cardCountySubmitErrorrequired: "Please enter a Post Code",
				cardHouseNameSubmitErrornonblank: "Please enter a valid House Name/No",
				//On blur error messages
				houseNameBlurErrornonblank: "Please enter a valid House Name/No",
				addressLine1BlurErroralpha: "Please enter a valid Street Address",
				addressLine2BlurErroralpha: "Please enter a valid Street Address",
				cityBlurErroralpha: "Please enter a valid Town/City",
				postCodeBlurErrorpostcode: "Please enter a valid Post Code",
				cardPostCodeBlurErrorpostcode: "Please enter a valid Post Code",
				cardNumberBlurErrorcardnumber: "Please check your Card Number",
				cardNameBlurErroralpha: "Please enter a valid name on Card",
				cardPostCodeBlurErroralpha: "Please enter a valid post code",

				expiryDateYYBlurErrorexpirydate: "Expiry Date - Has The Card Expired?",
				expiryDateMMBlurErrorexpirydate: "Expiry Date - Has The Card Expired?",
				securityCodeBlurErrorsecuritycode: "Please enter a valid Security Code",
				issueNumberBlurErrornumeric:"Please enter a valid Issue Number",
				cardHouseNameBlurErrornonblank: "Please enter a valid House Name/No",
				cardAddress1BlurErrornonblank: "Please enter a valid Street Address",
				cardTownCityBlurErrornonblank: "Please enter a valid Town/City",
				cardPostCodeBlurErrornonblank: "Please enter a valid Post Code",
				cardCountyBlurErrornonblank: "Please enter a valid County ",

                        //top error messages for server side validation errors
				houseNameTopServerError: "House Name/No",
				addressLine1TopServerError: "Street Address",
				cityTopServerError: "Town/City",
				countyTopServerError: "County",
				postCodeTopServerError: "Post Code",
				cardHouseNameTopServerError: "CardHolder House Name/No",
				cardAddress1TopServerError: "CardHolder Street Address",
				cardTownCityTopServerError: "CardHolder Town/City",
				cardPostCodeTopServerError: "CardHolder Post Code",


                        // Field specific error messages
				houseNameServerError: "Please enter a valid House Name/No",
				addressLine1ServerError: "Please enter a valid Street Address",
				cityServerError: "Please enter a valid Town/City",
				countyServerError: "Please enter a valid County",
				postCodeServerError: "Please enter a valid Post Code",
				cardHouseNameServerError: "Please enter a valid House Name/No",
				cardAddress1ServerError: "Please enter a valid Street Address",
				cardTownCityServerError: "Please enter a valid Town/City",
				cardPostCodeServerError: "Please enter a valid Post Code"

			};



            stickyOverlay.threeDOverlay();
			stickyOverlay.summaryPanelOverlay();
			//Server side error mapping.
			<c:if test="${not empty bookingInfo.errorFields}">
			<c:forTokens var="errorField" items="cardName|houseName|addressLine1|addressLine2|city|postCode|county|dayTimePhone|emailAddress" delims="|" >
               <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,errorField)}">
			      var errorField = "<c:out value='${errorField}' escapeXml='false'/>";
                  //Convert Top Text first char to upper case. In utils.js have a common function.
                  var topText = errorMap['<c:out value='${errorField}' escapeXml='false'/>'+'TopServerError']==undefined?"<c:out value='${errorField}' escapeXml='false'/>":errorMap['<c:out value='${errorField}' escapeXml='false'/>'+'TopServerError'];
			      //var errorMessage = "<c:out value='${bookingComponent.errorMessage}' escapeXml='false'/>";
                        var errorMessage = errorMap['<c:out value='${errorField}' escapeXml='false'/>'+'ServerError']==undefined?"Invalid <c:out value='${errorField}' escapeXml='false'/>":errorMap['<c:out value='${errorField}' escapeXml='false'/>'+'ServerError'];

			      var contentClass = $("."+errorField+"Error");
			      var errorName = errorField+ "Error";
				  var topContainer = $("#topError");
			      contentClass.addClass("formError");
                  $("<p class='formErrorMessage' id='"+errorName+"'>"+errorMessage+"</p>").prependTo(contentClass);
			      $("<li id='"+errorField+"TopError'>"+topText+"</li>").appendTo(topContainer);
               </c:if>
            </c:forTokens>
			</c:if>

            var form2 = $('form').webForm({
				onSubmitErrorDisplay: function(event, args){
				   commonSubmitBlurErrorDisplay(args);
				},
				onBlurErrorDisplay: function(event, args){
				   commonSubmitBlurErrorDisplay(args);
     			},
				onBlurSucessfulDisplay: function(event, args){
				   var contentClass = $("." +args+ "Error");
				   if(args=="expiryDateMM" || args=="expiryDateYY")
				   {
				      contentClass.removeClass("formError");
					  $("#expiryDateYYError").remove();
					  $("#expiryDateMMError").remove();
					  $("#expiryDateYYTopError").remove();
					  $("#expiryDateMMTopError").remove();
				   }
				   forceRedraw();
				   if($("ul#topError li").length < 1)
					   $("#errorSummary").addClass("hide");
     			},
				beforeSubmit: function(event, args){
				    if($("."+ args +"Error #"+ args +"Error").length >1)
					{
			           $("#"+ args +"Error").remove();
					   $("#"+args+"TopError").remove();
					}

				    if($("."+ args +"Error p.formErrorMessage").length < 1)
				       $("."+ args +"Error").removeClass("formError");

					if($("ul#topError li").length < 1)
					   $("#errorSummary").addClass("hide");
					$("#cardDetailsError").addClass("hide");
					forceRedraw();
				},
				beforeBlur: function(event, args){
					$("#"+ args +"Error").remove();
					if($("li ."+ args +"Error p.formErrorMessage").length < 1)
                       $("."+ args +"Error").removeClass("formError");
					$("."+ args +"Error").removeClass("formSucess");
					$("#"+args+"TopError").remove();
					if($("ul#topError li").length < 1)
					   $("#errorSummary").addClass("hide");
					forceRedraw();
				},
				onSubmitLuhnErrorDisplay: function(event)
				{
				   $("#cardDetailsError").removeClass("hide");
				   var contentClass = $("." +"cardNumber"+ "Error");
				   contentClass.addClass("formError");
				   var topContainer = $("#topError");
				   $("<p class='formErrorMessage' id='cardNumber'>Card number failed Luhn check</p>").prependTo(contentClass);
				    $("<li id='cardNumberTopError'>"+"Card number failed Luhn check"+"</li>").appendTo(topContainer);
	  forceRedraw();
				}



			});




			$('form').webForm("addErrorMessage", errorMap);

    });


</script>


<%----------------------Payment fields used in scripting etc -----------------------------------------%>
<c:forEach var="cardCharge" items="${bookingComponent.cardChargesMap}">
  <c:out value='<input type="hidden" id="${cardCharge.key}_charge" value="${cardCharge.value} "/>' escapeXml='false'/>
</c:forEach>
<c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
  <c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled' name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>
  <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>
  <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>
  <c:out value="<input type='hidden' value='${paymentType.cardType.minSecurityCodeLength}'  id='${paymentType.paymentCode}_minSecurityCodeLength' name='${paymentType.paymentCode}_minSecurityCodeLength'/>" escapeXml="false"/>
  <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>
</c:forEach>
<%-----------------*****END Payment fields used in scripting etc *****--------------------------------%>
