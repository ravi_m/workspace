<%@include file="/common/commonTagLibs.jspf"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      <title>Payments - Error!!</title>

	  <jsp:include page="css.jspf"></jsp:include>
      <link rel="shortcut icon" href="/cms-cps/shorex/b2c/images/favicon.ico"/>
      <script src="/cms-cps/shorex/b2c/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<!-- Modified to add ensighten in head section -->
 <%@include file="tag.jsp"%>
 <c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="TUIHeaderSwitch" value="${fn:toUpperCase(bookingComponent.nonPaymentData['TUIHeaderSwitch'])}"/>
 
   </head>
   <body>
      <div class="pageContainer" id="errorPage">
      <jsp:include page="/shorex/b2c/cookielegislation.jsp">
      <jsp:param name="paramSwitch" value='${TUIHeaderSwitch}'/>
      </jsp:include>
	     <div id="headerSection">
	     	<%@include file="header.jspf" %>
		 </div>
		 <div id="contentSection">

		    	<div id="headings">
			      <h1>We're Sorry</h1>
			   	</div>

			   	<div class="errorPanel">
			        <c:choose>
                        <c:when test="${not empty bookingInfo.trackingData.sessionId}">
                           <%@include file="techDifficulties.jspf"%>
                        </c:when>
                        <c:otherwise>
                           <%@include file="sessionTimeOut.jspf"%>
                        </c:otherwise>
                     </c:choose>

			     </div>

         </div>
         <%@include file="footer.jspf" %>
      </div>
   </body>
</html>