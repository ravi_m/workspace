<%@ page isELIgnored="false" %>
<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="cardChargeDetails" value="${creditCardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<fmt:formatNumber value="${cardChargeArray[0]}" type="number" var="cardChargeb2c" maxFractionDigits="1" minFractionDigits="0"/>
<c:set var="maxCardCharge" value="${cardChargeArray[2]}" />
<fmt:formatNumber value="${maxCardCharge}" type="number" var="maxCap" maxFractionDigits="1" minFractionDigits="0"/>
<fmt:parseNumber var="parsemaxCap" type="number" value="${maxCap}" />
<c:set var="now" value="<%=new java.util.Date()%>" />
<!-- following changes are made by sreenivasulu k -->

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>
<c:set var="TUIHeaderSwitch2" value="${fn:toUpperCase(bookingComponent.nonPaymentData['TUIHeaderSwitch'])}"/>

<c:choose>

   <c:when test="${TUIHeaderSwitch2 eq 'ON'}">

      <c:set value="TUI" var="textValue"/>
   </c:when>
   <c:otherwise>

      <c:set value="thomson" var="textValue"/>
   </c:otherwise>
</c:choose>




                              <div class="section  dashedBlackBorderBottom">
							     <div id="paymentDetails" class="dashedBlackBorderBottom">
                                    <h2><span>Payment Details</span></h2>
                                    <div class="mandatoryKey">Please complete all fields marked with<span class="mandatory">&nbsp;*</span></div>
								 </div>
								  <c:if test='${not empty error}'>
                    <div class="errorContainer">
                       <c:out value="${bookingComponent.errorMessage}" escapeXml="false"/>
                    </div>
		        </c:if>
								 <div class="paymentSectionWrapper">
                                    <div class="widthController">
                                       <ul class="controlGroups">
                                          <li class="controlGroup">
                                             <p class="fieldLabel">Total amount due </p>
                                             <div class="amount"><c:out value="${bookingComponent.totalAmount.symbol}" escapeXml="false"/><span id="totalAmountDue"><fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
                                                minFractionDigits="2" pattern="#,##,###.##"/></span></div>
                                          </li>
                                          <li class="cardTypeErrorContainer">
                                             <p class="fieldLabel"><label for="payment_type_0">Card type</label></p>
                                             <div class="controls">
                                                <select  name="payment_0_type" id="cardType" wf_required="required" alt="Card Type" requiredErrorMsg="Please select a Card Type" class="longField">
                                                   <option selected value="Pleaseselect">Please select</option>
                                                   <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">
                                <option value='<c:out value='${cardDetails.paymentCode}' escapeXml='false'/>|<c:out value='${cardDetails.paymentMethod}' escapeXml='false'/>'>

									    <!-- followed code added by sreenivasulu k -->
							<c:set var="type" value="${cardDetails.paymentDescription}"/>
							<c:choose>
								<c:when test = "${fn:containsIgnoreCase(type, 'Thomson')}">
								  <c:if test = "${TUIHeaderSwitch2 eq 'ON'}">
								 TUI CreditCard
								 </c:if>
									<c:if test = "${TUIHeaderSwitch2 ne 'ON'}">
								    <c:out value="${cardDetails.paymentDescription}"
								escapeXml='false' />
								 </c:if>

								</c:when>
								<c:otherwise>
											<c:out value="${cardDetails.paymentDescription}"
								escapeXml='false' />
								</c:otherwise>
							</c:choose>






								 </option>
                              </c:forEach>
												</select>
												<span class="mandatory">*</span>
                                             </div>
                                          </li>
										  <li class="cardNumberErrorContainer">
                                             <p class="fieldLabel"><label for="cardNumber">Card number</label></p>
                                             <div class="controls">
                                                <input name="payment_0_cardNumber" type="text" wf_type="cardnumber" wf_luhncheck="required" luhncheckErrorMsg="Please enter a valid card number" id="cardNumber" class="longField" class="inputWhole" wf_required="required" requiredErrorMsg="Please enter your Card Number" cardnumberErrorMsg="Please check your Card Number" maxlength="19" alt="Card Number" autocomplete="off">
												<span class="mandatory">*</span>
											 </div>
                                          </li>
                                          <li class="cardNameErrorContainer">
										     <p class="fieldLabel"><label for="payment_0_nameOnCard">Name on card</label></p>
                                             <div class="controls">
                                                <input type="text" wf_type="alpha" name="payment_0_nameOnCard" id="cardName"
                                                maxlength="30" class="inputWhole longField" wf_required="required" requiredErrorMsg="Please enter Name as it appears on the Card" value="" alt="Name On Card" alphaErrorMsg="Please enter a valid name on Card">
												<span class="mandatory">*</span>
											 </div>
                                          </li>
<jsp:useBean id="now" class="java.util.Date" />
    <fmt:formatDate var ="expiryMonthSelected1" type="date" value="${now}" pattern="MM"/>
                                          <li class="expiryDate-YYErrorContainer expiryDate-MMErrorContainer">
                                             <p class="fieldLabel"><label for="payment_expiryDateMonth">Expiry date</label></p>
											 <div class="controls">
						                        <select id="expiryDate-MM" name="payment_0_expiryMonth" wf_dateafter='<fmt:formatDate value="${now}" pattern="dd/MM/yyyy" />' wf_required="required" alt="Expiry Date" requiredErrorMsg="Please enter an Expiry Month" dateafterErrorMsg="Please select a valid date range">
						                        <option value="">mm</option>
												  <c:forEach begin="1" end="12" varStatus="loopStatus">
            <fmt:formatNumber var="expiryMonth" value="${loopStatus.count}" type="number" minIntegerDigits="2" pattern="##"/>
            <c:choose>
               <c:when test="${expiryMonthSelected1 ==  expiryMonth}">
                  <option value="<c:out value="${expiryMonth}"/>"><c:out value="${expiryMonth}"/></option>
               </c:when>
               <c:otherwise>
                  <option value="<c:out value="${expiryMonth}"/>"><c:out value="${expiryMonth}" /></option>
               </c:otherwise>
            </c:choose>
         </c:forEach>
						                        </select>

                                                <select id="expiryDate-YY" name="payment_0_expiryYear" wf_dateafter='<fmt:formatDate value="${now}" pattern="dd/MM/yyyy" />' wf_required="required" alt="Expiry Date" requiredErrorMsg="Please enter an Expiry Year" dateafterErrorMsg="Please select a valid date range">
												   <option value="">yy</option>
												   <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
               <option value='<c:out value='${expiryYear}'/>' wf_year="<c:out value='${2000+expiryYear}'/>" ><c:out value='${expiryYear}'/></option>
         </c:forEach>
                                                </select>
                                                <span class="mandatory">*</span>
                                             </div>
                                          </li>
									   </ul>
									</div>

			<fmt:formatNumber var="amountWithCardCharge" value="${selectedDepositAmount+applicableCardChargeForSelectedAmount}" type="number" maxFractionDigits="2" minFractionDigits="2"  scope="request" groupingUsed="false"/>
            <fmt:formatNumber var="amountWithDebitCardCharge" value="${selectedDepositAmount+applicableDebitCardChargeForSelectedAmount}" type="number" maxFractionDigits="2" minFractionDigits="2"  scope="request" groupingUsed="false"/>


                                    <div class="sectionContent widthController">
                                       <ul class="paymentAmount">
                                          <li class="fontWeightNormal" id="amountWithCardCharge">
                                             <span id="spanAmountWithCardCharge" class="amount"><c:out value="${currencySymbol}" escapeXml="false"/><c:out value="${amountWithCardCharge}"/>XXXXXXXXXX</span>
                                             <span>Pay by Credit Card</span>
											 <div class="clearb"></div>
	                                         <c:if test="${applyCreditCardSurcharge eq 'true'}">
	                                         <p class="note">Includes <c:out value="${cardChargePercent}"/>% Credit Card surcharge</p>
	                                         </c:if>
                                          </li>
                                          <li class="fontWeightNormal" id="thomsonCardCharge" style ="display : none" >
                                             <span id="spanAmountthomsonCardCharge" class="amount"><c:out value="${currencySymbol}" escapeXml="false"/><c:out value="${amountWithCardCharge}"/>XXXXXXXXXX</span>
                                             <span>Pay by <c:out value="${textValue}"/> Credit Card</span>
											 <div class="clearb"></div>
	                                         <!-- <p class="note">Includes no extra charge </p> -->
                                          </li>
                                          <li class="fontWeightNormal" id="amountWithoutCardCharge">
                                             <span id="spanAmountWithoutCardCharge" class="amount"><c:out value="${currencySymbol}" escapeXml="false"/><c:out value="${amountWithDebitCardCharge}"/></span>
                                             <span>Pay by Debit Card</span>
                                          </li>

                                       </ul>
                                    </div>
                                 </div>
                                 <div class="paymentSectionWrapper">
                                    <div class="sectionContent ">
                                       <ul class="controlGroups">
                                          <li class="securityCodeErrorContainer">
                                             <p class="fieldLabel"><label for="payment_0_securityCode">Security code</label></p>
                                             <div class="controls">
                                                <input name="payment_0_securityCode" id="securityCode" type="text" wf_type="numeric" wf_securitycode="required" securitycodeErrorMsg="Please enter a valid Security Code" autocomplete="off" maxlength="4" class="inputWhole" wf_required="required" requiredErrorMsg="Please enter a Security Code" numericErrorMsg="The Security Code must be numeric"  alt="Security Code" /><span class="mandatory">*</span>
											 </div>
                                             <p class="fieldInformation">More about <a id="cvvDetailsOverlay-link" href="#" title="View CVV details" rel="external">CVV/CID code</a></p>

                                          </li>

                                          <li class="issueNumberErrorContainer hide">
                                          	 <p class="fieldInformation">For Maestro / Solo Card Users only - Please enter the issue number of your card.</p>
                                             <p class="fieldLabel"><label for="payment_0_issueNumber">Issue Number</label></p>
                                             <div class="controls">
                                                <input name="payment_0_issueNumber" alt="Issue Number" type="text" wf_type="numeric" id="issueNumber" maxlength="2" requiredErrorMsg="Please enter Issue Number." numericErrorMsg="Please enter a valid Issue Number." wf_required="required" />
                                                <span class="mandatory">*</span>
                                             </div>

										  </li>
										  <li class="controlGroup">
                                             <div class="controls" style="margin-left:123px;margin-top:-10px;">

             <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
		     <c:if test="${threeDLogos == 'mastercardgroup'}">
                     <a class="logoMasterCard threeDSstickyOwner" id="masterCardDetailsOverlay-link" href="javascript:void(0);"><img src="/cms-cps/shorex/b2c/images/logos/logo-mastercard.gif" title="MasterCard">Learn more</a>
             </c:if>
             </c:forEach>
            <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
		    <c:if test="${threeDLogos == 'visagroup'}">
		       <a class="logoVisa threeDSstickyOwner" id="visaDetailsOverlay-link" href="javascript:void(0);"><img src="/cms-cps/shorex/b2c/images/logos/logo-visa.gif" title="Visa">Learn more</a>
		    </c:if>
	       </c:forEach>
	                                       </div>

										  </li>

					                      <c:choose>
											<c:when test="${applyCreditCardSurcharge eq 'true'}">
											<li class="controlGroup">
					                      There are no additional charges when paying by Maestro, MasterCard Debit, Visa/ Delta debit cards or <c:out value="${textValue}"/> Credit Card. A  fee of <c:out value='${cardChargeb2c}'/>% applies to credit card payments <c:if test="${parsemaxCap > 0}">, which is capped at &pound;<c:out value='${maxCap}'/> </c:if>per transaction when using American Express, MasterCard Credit or Visa Credit cards.
					                      </li>
					                      </c:when>
					                       <c:otherwise>
					                       <li class="controlGroup">
					                       There are no additional charges when paying by Debit or Credit Cards.
					                       </li>
					                       </c:otherwise>
					                       </c:choose>

                                       </ul>

                                    </div>
                                 </div><!-- end div paymentSectionWrapper-->
                              </div><!-- end div section1-->



	<input  type='hidden' id='total_transamt' name='total_transamt' value='NA'/>
	<input type="hidden" id="payment_0_paymenttypecode" value="" name="payment_0_paymenttypecode"/>