
<div id="toyLinksSection">
            <div id="toyBtmNav">
               <div id="toyBtmNavLBG"> </div>
               <div id="toyBtmNavMBG">
                  <h3 id="toyBtmNavHead">Popular:</h3>
                  <ul>
                     <li><a href="http://www.thomson.co.uk/cruise/ships/thomson-celebration-ship-details.html">Thomson Celebration</a><span>|</span></li>
                     <li><a href="http://www.thomson.co.uk/cruise/ships/thomson-destiny-ship-details.html">Thomson Destiny</a><span>|</span></li>
                     <li><a href="http://www.thomson.co.uk/cruise/ships/thomson-spirit-ship-details.html">Thomson Spirit</a><span>|</span></li>
                     <li><a href="http://www.thomson.co.uk/cruise/ships/island-escape-ship-details.html">The Island Escape</a><span>|</span></li>
                     <li><a href="http://www.thomson.co.uk/cruise/ships/thomson-dream-ship-details.html">Thomson Dream</a></li>
                  </ul>
               </div><!-- toyBtmNavMBG -->
               <div id="toyBtmNavRBG"></div>
               <div class="toyClearBoth"></div>
            </div><!-- toyBtmNav -->
         </div>
         <!--end of toyLinksSection-->

<div id="footerSection">
            <div id="TOYFooterLeft">
               <div class="TOYexploreLinks">
                  <ul>
                     <li><a title="All Inclusive Holidays" href="http://www.thomson.co.uk/all-inclusive-holidays.html">All Inclusive Holidays</a></li>
              		 <li><a title="Family Holidays" href="http://www.thomson.co.uk/family-holidays.html">Family Holidays</a></li>
                     <li><a title="Last Minute Holidays" href="http://www.thomson.co.uk/late-deals/late-deals.html">Last Minute Holidays</a></li>
                     <li><a title="Cheap Holidays" href="http://www.thomson.co.uk/">Cheap Holidays</a></li>
                     <li><a title="Winter Holidays" href="http://www.thomson.co.uk/deals/winter-2011-deals.html">Winter Holidays</a></li>
                     <li><a title="Summer Holidays" href="http://www.thomson.co.uk/deals/summer-2011-deals.html">Summer Holidays</a></li>
                     <li><a title="Tenerife Holidays" href="http://www.thomson.co.uk/destinations/europe/spain/tenerife/holidays-tenerife.html">Tenerife Holidays</a></li>
                     <li><a title="Canary Island Holidays" href="http://www.thomson.co.uk/editorial/features/canary-islands-holidays.html">Canary Island Holidays</a></li>
		             <li><a title="Ibiza Holidays" href="http://www.thomson.co.uk/destinations/europe/spain/ibiza/holidays-ibiza.html">Ibiza Holidays</a></li>
		             <li><a title="Florida Holidays" href="http://www.thomson.co.uk/destinations/the-americas/united-states-of-america/florida/holidays-florida.html">Florida Holidays</a></li>
		             <li><a title="Cuba Holidays" href="http://www.thomson.co.uk/destinations/caribbean/cuba/holidays-cuba.html">Cuba Holidays</a></li>
		             <li class="lastOne"><a title="Fuerteventura Holidays" href="http://www.thomson.co.uk/destinations/europe/spain/fuerteventura/holidays-fuerteventura.html">Fuerteventura Holidays</a></li>
                  </ul>
                  <ul class="last">
		             <li><a title="About Thomson" href="http://www.thomson.co.uk/editorial/legal/about-thomson.html">About Thomson</a></li>
                     <li><a title="Communication Centre" href="http://communicationcentre.thomson.co.uk ">Communications Centre</a></li>
                     <li><a title="Travel Jobs" href="http://www.tuitraveljobs.co.uk">Travel Jobs</a></li>
                     <li><a title="Affiliates" href="http://www.thomson.co.uk/partners/thomson-affiliate-programme.html">Affiliates</a></li>
                     <li><a title="Sitemap" href="http://www.thomson.co.uk/destinations/sitemap/site-map.html">Sitemap</a></li>
                     <li><a title="Register for Offers" href="http://www.thomson.co.uk/myholidayalerts/">Register for Offers</a></li>
                     <li><a rel="nofollow" title="My Thomson" href="http://www.thomson.co.uk/mythomson/">My Thomson</a></li>
                     <li class="lastOne"><a title="RSS" href="http://www.thomson.co.uk/editorial/deals/rss.html"><img width="32" height="15" title="RSS" alt="RSS" src="/cms-cps/shorex/b2c/images/footer/icon_rss.gif"></a></li>
                  </ul>
               </div>
               <div class="TOYFooterDisclaimer">
                  <img id="footerparagraph" alt="" src="/cms-cps/shorex/b2c/images/footer/TOYfooterDisclaimer.gif">
               </div>
               <div class="TOYFooterCopyright">
                  <ul>
                     <li class="first lastOne">&copy; 2011 TUI UK</li>
                     <li><a rel="nofollow" title="Terms &amp; Conditions" href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html">Terms &amp; Conditions</a></li>
                     <li><a rel="nofollow" title="Privacy &amp; Cookies Policy" href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html">Privacy &amp; Cookies Policy</a></li>
                     <li class="lastOne"><a title="TUI Travel plc" href="http://www.tuitravelplc.com">TUI Travel plc</a></li>
                     <li class="lastOne card">We accept:</li>
                     <li class="lastOne cardtype"><img width="32" height="20" alt="Visa Logo" src="/cms-cps/shorex/b2c/images/footer/logo_visa.gif"><img width="32" height="20" alt="Delta Logo" src="/cms-cps/shorex/b2c/images/footer/logo_delta.gif"><img width="32" height="20" alt="MasterCard Logo" src="/cms-cps/shorex/b2c/images/footer/icon_mastercard.gif"><img width="33" height="20" alt="Maestro Logo" src="/cms-cps/shorex/b2c/images/footer/icon_maestro.gif"><img width="33" height="20" alt="Solo Logo" src="/cms-cps/shorex/b2c/images/footer/icon_solo.gif"><img width="32" height="20" alt="American Express Logo" src="/cms-cps/shorex/b2c/images/footer/icon_americanexpress.gif"></li>
                  </ul>
               </div>
            </div>
            <div id="TOYFooterRight">
               <div class="TOYFooterHelp">
                  <ul>
                     <li><a title="Manage my booking" href="http://www.thomson.co.uk/managemybooking/">Manage my booking</a></li>
                     <li><a onclick="window.open('http://www.thomson.co.uk/shopfinder/shop-finder.html','name','height=745,width=820,toolbar=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no'); return false;" target="_new" title="Shop finder" href="http://www.thomson.co.uk/shopfinder/shop-finder.html">Shop finder</a></li>
                     <li><a title="Ask us a question" href="http://www.thomson.co.uk/editorial/faqs/faqs.html">Ask us a question</a></li>
                     <li><a href="http://www.thomson.co.uk/editorial/legal/contact-us.html">Contact us</a></li>
                     <li class="number">0871 231 4691</li>
		             <li class="callCharges">
        		        <p>Calls cost 10p per minute plus network extras</p>
                     </li>
                  </ul>
               </div>
	           <div class="TOYFooterPartners">
   	              <a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&amp;dn=www.thomson.co.uk&amp;lang=en">
	                 <img width="82" height="45" title="This Web site has chosen one or more VeriSign SSL Certificate or online payment solutions to improve the security of e-commerce and other confidential communication" alt="The Verisign logo" src="/cms-cps/shorex/b2c/images/footer/logo_verisignNew.gif">
	              </a>
	  	          <a onclick="openWindow(this); return false;" href="http://www.abta.com/find-a-holiday/member-search/5736">
	                 <img title="Verify ABTA membership - ABTA Number V5126" alt="The ABTA logo" src="/cms-cps/shorex/b2c/images/footer/logo-abta_2009.gif" class="abtaPos">
	              </a>
	  	          <a onclick="openWindow(this); return false;" href="http://www.caa.co.uk/applicationmodules/atol/atolverify.aspx?atolnumber=2524">
                     <img title="ATOL Protected - ATOL Number 2524. Click on the ATOL Logo if you want to know more" alt="The ATOL logo" src="/cms-cps/shorex/b2c/images/footer/logo_atolNew.gif">
			      </a>
      	          <ul>
			         <li>
				        <a onclick="openWindow(this); return false;" href="http://www.abta.com/find-a-holiday/member-search/5736">ABTA</a>
				     </li>
				     <li class="lastOne">
	                    <a onclick="openWindow(this); return false;" href="http://www.caa.co.uk/applicationmodules/atol/atolverify.aspx?atolnumber=2524">ATOL</a>
				     </li>
			      </ul>
               </div>
            </div>
            <div class="clearb"></div>
		 </div>
