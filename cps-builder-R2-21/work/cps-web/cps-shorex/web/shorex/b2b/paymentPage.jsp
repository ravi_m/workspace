<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Thomson - Cruise Holidays and Cruise Deals - Cruises that suit your needs</title>

<jsp:include page="css.jspf"></jsp:include>
<%@include file="javascript.jspf" %>
<%@include file="configSettings.jspf" %>
<c:set var="bookingflow" value="${bookingComponent.nonPaymentData['booking_flow']}"/>
<c:set var="paymentflow" value="${bookingComponent.nonPaymentData['payment_flow']}"/>
<c:set var="exitBookingURL" value="${bookingComponent.nonPaymentData['exit_booking_url']}"/>
<c:set var="logoutURL" value="${bookingComponent.nonPaymentData['logout_url']}"/>
<%
	String applyCreditCardSurcharge=com.tui.uk.config.ConfReader.getConfEntry("B2BShorex.applyCreditCardSurcharge" , null);
	pageContext.setAttribute("applyCreditCardSurcharge", applyCreditCardSurcharge, PageContext.REQUEST_SCOPE);
%>
<script>
applyCreditCardSurcharge="<%=applyCreditCardSurcharge%>";
</script>
<script type="text/javascript">
   var cardChargeMap = new Map();
   var pleaseSelect = new Array();

   cardChargeMap.put('none', pleaseSelect);
   <c:forEach var="cardCharges" items="${bookingInfo.cardChargeMap}">
   var cardChargeInfo = new Array();
        cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');
   </c:forEach>

   var cardDetails = new Map();
   <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">
          var cardCharge_Max_Min_Issue = new Array();
                   cardCharge_Max_Min_Issue.push('${cardDetails.cardType.securityCodeLength}');
                   cardCharge_Max_Min_Issue.push('${cardDetails.cardType.minSecurityCodeLength}');
                   cardCharge_Max_Min_Issue.push('${cardDetails.cardType.isIssueNumberRequired}');
           cardDetails.put('${cardDetails.paymentCode}',cardCharge_Max_Min_Issue);
         </c:forEach>
     //  The following map stores the pay button labels in case of 3D cards.
</script>
<!--[if lt IE 7]>
   <link rel="stylesheet" type="text/css" href="./css/flexi-box/core-ie6.css" title="Thomson" media="screen" />
<![endif]-->
<!--[if lte IE 6]>
   <script type="text/javascript">
      try{
        document.execCommand("BackgroundImageCache", false, true);
      }
      catch(err) { }
   </script>
<![endif]-->


<c:set var="TUIHeaderSwitch" value="${fn:toUpperCase(bookingComponent.nonPaymentData['TUIHeaderSwitch'])}"/>

<c:choose>

   <c:when test="${TUIHeaderSwitch eq 'ON'}">
      <c:set value="www.tui.co.uk" var="tuiFlagLink" />
   </c:when>
   <c:otherwise>
      <c:set value="www.thomson.co.uk" var="tuiFlagLink" />
   </c:otherwise>
</c:choose>

   </head>
   <body>
      <div class="pageContainer">
         <div id="headerSection">
            <div id="toyHeaderPanel">
               <div id="headerTop">


                <c:choose>
                    <c:when test="${TUIHeaderSwitch eq 'ON'}">


                  <a alt="MARELLA CRUISES" title="MARELLA CRUISES" href="${tuiFlagLink}/"><img src="/cms-cps/shorex/b2b/images/header/tuiLogo.png" id="toyHeaderLogo"></a>
              <span id="authentication" class="logout"><a href="<c:out value="${logoutURL}" />">Log out</a></span>

					 </c:when>
               		<c:otherwise>


                  <a alt="Thomson logo" title="Thomson logo" href="http://www.thomson.co.uk/"><img src="/cms-cps/shorex/b2b/images/header/logo-thomson.png" id="toyHeaderLogo"></a>
              <span id="authentication" class="logout"><a href="<c:out value="${logoutURL}" />">Log out</a></span>


 					 </c:otherwise>
                 </c:choose>


               </div>
            </div>
         <!-- Booking Flow Bar starts -->
          <%@include file="breadCrumb.jspf" %>
            <!-- Booking Flow Bar ends -->
            <c:if test="${fn:containsIgnoreCase(bookingflow, 'ViewBooking') && fn:containsIgnoreCase(paymentflow, 'REFUND')}">
              <div id="headings">
               <h1>Refund</h1>
              </div>
            </c:if>
            <c:if test="${(fn:containsIgnoreCase(bookingflow, 'ViewBooking') || fn:containsIgnoreCase(bookingflow, 'MakeBooking')) && fn:containsIgnoreCase(paymentflow, 'PAYMENT')}">
             <div id="headings">
               <h1>Payment</h1>
             </div>
            </c:if>
            <div class="clearb"></div>
         </div>
         <!--end of HeaderSection-->

         <div id="contentSection">
            <!--left panel-->
             <%@include file="summaryPanel.jspf"%>
             <div id="contentCol2">
               <form  method="post" action="/cps/processPayment?b=<c:out value='${param.b}' />&token=<c:out value='${param.token}' />&tomcat=<c:out value='${param.tomcat}' />">

                  <%@include file="topError.jspf" %>
                  <!-- Payment -->
                  <div class="detailsPanel" id="paymentDetailsPanel">
                     <div class="top">
                        <div class="right"></div>
                     </div>
                     <div class="body">
                        <div class="right">

                           <div class="content">
                            <c:if test="${fn:containsIgnoreCase(bookingflow, 'ViewBooking') && fn:containsIgnoreCase(paymentflow, 'REFUND')}">
                              <div class="section">
                                  <div class="sectionHeader">
                                    <h2><span>Refund Details</span></h2>
                                  </div>
                                  <%@include file="refundDetails.jspf"%>
                              </div>

                           </c:if>

                         <c:if test="${(fn:containsIgnoreCase(bookingflow, 'ViewBooking') || fn:containsIgnoreCase(bookingflow, 'MakeBooking')) && fn:containsIgnoreCase(paymentflow, 'PAYMENT')}">
                             <div class="section">
                                <div class="sectionHeader">
                                    <h2><span>Payment Details</span></h2>
                               </div>
                               <%@include file="paymentDetails.jspf"%>
                             </div>

                           </c:if>
                           <%@include file="buttonGroup.jspf"%>
                           </div>
                        </div>

                 </div>
                   <div class="bottom">
                      <div class="right"></div>
                   </div>
                </div>
            <div class="pageControls"></div>
            </form>
            <form  method="post" action="${exitBookingURL}">
	           <div class="buttonGroup" id="exitBooking">
	              <input type="submit" value="Exit Booking" class="primary bookingcontinue" title="Exit Booking"/>
	           </div>
            </form>
          </div>
          <!--end of contentCol2-->
         <div class="clearb"></div>
         </div><!--end of ContentSection-->
      </div>

      <!--end of PageContainer-->
   </body>
   <script type="text/javascript">
            $(document).ready(function(){
               stickyOverlay.threeDOverlay();
               stickyOverlay.commonOverlay();
            });
   </script>


</html>


