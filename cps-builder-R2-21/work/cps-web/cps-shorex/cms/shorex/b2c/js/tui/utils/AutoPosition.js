var AutoPosition = {};

//Constants for center positions.
AutoPosition.CENTER = "center";
AutoPosition.CENTER_LEFT = "centerLeft";
AutoPosition.CENTER_RIGHT = "centerRight";

// Constants for bottom positions.
AutoPosition.BOTTOM_LEFT = "bottomLeft";
AutoPosition.BOTTOM_RIGHT = "bottomRight";
AutoPosition.BOTTOM_CENTER = "bottomCenter";

// Constants for top positions.
AutoPosition.TOP_LEFT   = "topLeft";
AutoPosition.TOP_RIGHT  = "topRight";
AutoPosition.TOP_CENTER = "topCenter";

// Constants for positions offset options.
AutoPosition.POSITION = "position";
AutoPosition.OFFSET = "offset";

(function($){

	tui.ui.AutoPosition = {

		options : {
			relative : null,
			position : "center",
			offsetPosition : "position",
			offset : {t : 0,l : 0}
		},

    	/**
    	 * Method: constructor
    	 * Called on object creation, and initialise class member variable.
    	 */
    	_init : function() {
			var autoPosition = this;
			autoPosition.position();
		},

		/**
		 * Method: position
		 * Positions an element based on given options past.
		 */
		position : function(options){
			var autoPosition = this;
			$.extend(autoPosition.options, options);

	        var relative = (this.options.relative) ? $(this.options.relative) : $(window);
	        var isParent = (this.element.parent(this.options.relative).length > 0 || relative[0].location);
	        var dimensions = {
	            w: ($.browser.opera) ?  relative.innerWidth() : relative.width(),
	            h: ($.browser.opera) ? relative.innerHeight() : relative.height() ,
	            t: (relative[0].location) ? 0 : relative[this.options.offsetPosition]().top,
	            l: (relative[0].location) ? 0 : relative[this.options.offsetPosition]().left
	        }

	        autoPosition._trigger("beforePosition", null, [ autoPosition, relative, dimensions ]);

	        var dialogWinDimensions = {
	        	h : ($.browser.opera) ? this.element.innerHeight() : this.element.height(),
	        	w : ($.browser.opera) ? this.element.innerWidth() : this.element.width()
	        }

			var top = 0, left = 0;

	        switch (this.options.position) {

	            case AutoPosition.BOTTOM_LEFT:
					left = dimensions.l;
	                top = (isParent) ? (dimensions.h + dimensions.t) - dialogWinDimensions.h() : dimensions.h + dimensions.t;
				break;

				case AutoPosition.BOTTOM_RIGHT:
					left = (isParent) ? (dimensions.l + dimensions.w) - dialogWinDimensions.w: (dimensions.l + dimensions.w)
	                top = (isParent) ? (dimensions.h + dimensions.t) - dialogWinDimensions.h : dimensions.h + dimensions.t;
				break;

				case AutoPosition.BOTTOM_CENTER:
					left = (dimensions.w / 2) + dimensions.l - (dialogWinDimensions.w / 2);
	                top = (isParent) ? (dimensions.h + dimensions.t) - dialogWinDimensions.h : dimensions.h + dimensions.t;
				break;

	            case AutoPosition.CENTER:
	                left = dimensions.w / 2 - (dialogWinDimensions.w / 2);
	                top = dimensions.h / 2 - (dialogWinDimensions.h / 2);
				break;

				case AutoPosition.CENTER_LEFT:
	                left = (isParent) ? dimensions.l : (dimensions.l - dialogWinDimensions.w);
	                top = (isParent) ? dimensions.h / 2 - (dialogWinDimensions.h/ 2) : (dimensions.h / 2 + dimensions.t) - (dialogWinDimensions.h / 2)
				break;

				case AutoPosition.CENTER_RIGHT:
	              	left = (isParent) ? (dimensions.l + dimensions.w) - dialogWinDimensions.w : (dimensions.l + dimensions.w);
	                top = (isParent) ? dimensions.h / 2 - (dialogWinDimensions.h / 2) : (dimensions.h / 2 + dimensions.t) - (dialogWinDimensions.h / 2) ;
				break;

				case AutoPosition.TOP_LEFT:
					left = dimensions.l;
	                top = (isParent) ? dimensions.t : dimensions.t - dialogWinDimensions.h;
				break;

				case AutoPosition.TOP_RIGHT:
					left = (isParent) ? (dimensions.l + dimensions.w) - dialogWinDimensions.w : (dimensions.l + dimensions.w)
	                top = (isParent) ? dimensions.t : dimensions.t - dialogWinDimensions.h;
				break;

				case AutoPosition.TOP_CENTER:
					left = (dimensions.w / 2) + dimensions.l - (dialogWinDimensions.w / 2);
	                top = (isParent) ? dimensions.t : dimensions.t - dialogWinDimensions.h;
				break;
	        }

			var scrollLeft = (isParent) ? relative.scrollLeft() : 0;
			var scrollTop = (isParent) ? relative.scrollTop() : 0;

			this.element.css({
	            'left': left + scrollLeft + this.options.offset.l,
	            'top': top  + scrollTop + this.options.offset.t
	        });
		}
	}

	$.widget("ui.autoPosition", tui.ui.AutoPosition);

})(jQuery);