										// Simple JavaScript Templating
// John Resig - http://ejohn.org/ - MIT Licensed
(function(){
  var cache = {};

  tui.utils.Template = {}
  tui.utils.Template.getTmpl = function(str, data){
    // Figure out if we're getting a template, or if we need to
    // load the template - and be sure to cache the result.
    var fn = !/\W/.test(str) ?
      cache[str] = cache[str] ||
        tui.utils.Template.getTmpl(document.getElementById(str).innerHTML) :

      // Generate a reusable function that will serve as a template
      // generator (and which will be cached).
      new Function("obj",
        "var p=[],print=function(){p.push.apply(p,arguments);};" +

        // Introduce the data as local variables using with(){}
        "with(obj){p.push('" +

        // Convert the template into pure JavaScript
        str
          .replace(/[\r\t\n]/g, " ")
          .split("<%").join("\t")
          .replace(/((^|%>)[^\t]*)'/g, "$1\r")
          .replace(/\t=(.*?)%>/g, "',$1,'")
          .split("\t").join("');")
          .split("%>").join("p.push('")
          .split("\r").join("\\'")
      + "');}return p.join('');");

    // Provide some basic currying to the user
    return data ? fn( data ) : fn;
  };

})();

tui.utils.String = {
	substitute : function(stringValue, object, regexp) {
		return stringValue.replace(regexp || (/\\?\{([^{}]+)\}/g), function(
				match, name) {
			if (match.charAt(0) == '\\')
				return match.slice(1);
			return (object[name] != undefined) ? object[name] : '';
		});
	},

	capitalise : function(text) {
		return text.replace(/\b[a-z]/g, function(match) {
			return match.toUpperCase();
		});
	}
}

tui.utils.Number = {
	toInt: function(value, base){
		return parseInt(value, base || 10);
	},

	formatCurrency : function(num, options) {
		options = (options || {})
		currencySign =  (options.currencySign == undefined) ? '&pound;' : options.currencySign;
		removeDecimal = (options.removeDecimal == undefined) ? false : options.removeDecimal;

		num = isNaN(num) || num === '' || num === null ? 0.00 : num;

		sign = (parseFloat(num) < 0) ? '-' : (options.showSign) ? '+':'';

		num = parseFloat(num);
		num = (num < 0) ? num * -1 : num;

		if ((String(num).indexOf(".") != -1) && removeDecimal) {
			var splitString = num.split(".");
			return (splitString[1] == 00) ? sign + currencySign + splitString[0] : sign
					+ currencySign + num.toFixed(2);
		}

		return sign + currencySign + num.toFixed(2);

	}

}