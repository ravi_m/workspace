/**
 * This script defines a global namespace for tui objects to append to.
 * @author Maurice Morgan
 */
var tui = {}

/**
 * Namespace for pages.
 */
tui.pages = {}

/**
 * Namespace for ui compoments.
 */
tui.ui = {}

/**
 * Namespace for view objects.
 */
tui.models = {}

/**
 * Namespace for view objects.
 */
tui.views = {}

/**
 * Namespace for controller objects.
 */
tui.controllers = {}

/**
* Namespace for util objects.
*/
tui.utils = {}
