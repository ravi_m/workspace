var refundByChequeChecked = false;
var refundByChequeIndex;
/***
 *  File containing functionality specific script for the WSS.
 *
 *  It contains following
 *  a. Constants (id, class name etc.)
 *  b. Common functions (functions used by more than one functionality)
 *
 *  It contains following functionalities
 *  1. On-load functionality
 *  2. Adding events for payment fields Unobtrusively
 *  3. Updating payment page for deposit type selection
 *  4. Updating payment page for values entered in amount to pay textfield
 *  5. Updating payment page for card type selection
 *  6. Browser back functionality
 *
 */

/*----------------------b. Common functions---------------------------------------------------------*/
/**
 * Gets the currency that is currently used for the page.
 */
function getCurrency()
{
  var currency = $("#currencyText");
  if( currency )
  {
    currency = currency.val();
  }
  return currency;
}

/**
 * Updates amounts
 */
function updateAmountsOnPaymentPage()
{
   //Update the textfields
    displayTheAmountsInTextFields(PaymentCssClasses.DEPOSIT_AMOUNT_CLASS , PaymentInfo.selectedDepositAmount );
    displayTheAmountsInTextFields(PaymentCssClasses.PAYABLE_AMT_CLASS , PaymentInfo.calculatedPayableAmount );

    //display the amounts on payment page
    displayTheAmounts(PaymentCssClasses.TOTAL_AMOUNT_CLASS , 1*PaymentInfo.totalHolidayPrice + 1*PaymentInfo.totalCardCharge );
    displayTheAmounts(PaymentCssClasses.AMOUNT_PAID_CLASS , 1*PaymentInfo.amountPaid + 1*PaymentInfo.totalCardCharge );
    displayTheAmounts(PaymentCssClasses.TOTAL_CARD_CHARGE_CLASS , 1*PaymentInfo.totalCardCharge );
}

/** Function for updating amount based on the
 ** style class and amount.
**/
function displayTheAmounts(amountClassName , amount)
{
  //Display the fields which have given class name with the amount.
   var currency = getCurrency();
   $('.'+amountClassName).html(currency + parseFloat(amount).toFixed(2));
}

/** Function for updating amounts in text fields based on the
 **  style class and amount.
**/
function displayTheAmountsInTextFields(amountClassName , amount)
{
 //  //alert("display amt in text fields");
  //Display the fields which have given class name with the amount.
  $('.'+amountClassName).val(roundOff(1*amount ,2));
}
/*-----------------*****END Common functions*****---------------------------------------------------*/


/*----------------------1. Onload functionality-------------------------------------------------*/
/**
 * Makes an ajax call to check whether a holiday is new or already booked
 *
 */
function initialisePaymentEvents()
{
  //If javascript is disabled following is not displayed
  
  amountToPayField = $("#"+PaymentPageIds.AMOUNT_TO_PAY_ID);
  amountToPayField.val(PaymentInfo.firstDepositAmount);
  amountToPayField.attr("readOnly", "readOnly");
  $("."+PaymentCssClasses.DEPOSIT_RADIO_CLASS+":first").attr('checked','checked');
  if (!$("#totalAmountPayableControlGroup").hasClass("cancel"))
  {
     $("#totalAmountPayableControlGroup").removeClass(PaymentCssClasses.HIDE).addClass(PaymentCssClasses.SHOW);
  }
  $("#"+PaymentPageIds.TOTAL_AMT_PAYABLE).val(PaymentInfo.firstDepositAmount);

  issueNoContainer = $("#"+PaymentPageIds.ISSUE_NO_CONTROL_GROUP)
  if(!(issueNoContainer.hasClass("inError")))
  {
     issueNoContainer.addClass(PaymentCssClasses.HIDE);
  }
  //Initialize payment events or backbutton functionality based on status of new holiday
  performJQueryAjaxUpdate( "ConfirmBookingServlet", null, responseCheckForNewHoliday );
}

/**
 * Call back function of checkForNewHolidayAndInitializeEvents().
 * Sets
 *
 * @param request
 * @return isNewHoliday
 */
function responseCheckForNewHoliday(request)
{
    initializeOnloadEvenets(request);
}

/**
 * Initialises:
 * - Payment Events, Low Deposit events, Promotional Code event
 * - Setup the App config object
 * -
 *
 */
function initializeOnloadEvenets(isNewHoliday)
{
    //Refund total amount field should be shown only when javascript is enabled.
   //Hence the below two lines of the code are added to remove the hide property of refund fields.
   $("#totalRefundAmountField").removeClass(PaymentCssClasses.HIDE);
    $("#totalRefundAmountText").removeClass(PaymentCssClasses.HIDE);
   $("#refundBtn").removeClass(PaymentCssClasses.HIDE);

   if( isNewHoliday == "true" )
    {
     if ($("#paymentFlow").val() == "REFUND")
     {
     setToDefault();
     }
      //Adding event to card type drop down.
    // $("#"+PaymentPageIds.CARD_TYPE_ID).change(function(){cardTypeHandler(this.value)});
      //$("."+PaymentCssClasses.DEPOSIT_RADIO_CLASS).click(function(){depositTypeHandler(this.value)});
      //$("#"+PaymentPageIds.AMOUNT_TO_PAY_ID).blur(function() { amountToPayHandler()});

     //$("."+PaymentCssClasses.REFUND_C   //alert("inside refundss");
     $("."+PaymentCssClasses.REFUND_CHECKBOX_CLASS).click(function(){refundFieldClickController(this)});
     $("."+PaymentCssClasses.REFUND_TEXTCLASS).blur(function(){refundFieldChangeController(this)});

	  $("."+PaymentCssClasses.REFUND_CC_CHECKBOX_CLASS).click(function(){refundCCFieldClickController(this)});
     $("."+PaymentCssClasses.REFUND_CC_TEXTCLASS).blur(function(){refundCCFieldChangeController(this)});
       PaymentInfo.depositType = $("."+PaymentCssClasses.DEPOSIT_RADIO_CLASS+":first").val();
      if(StringUtils.isEmpty(PaymentInfo.depositType))
      {
           PaymentInfo.depositType = "fullCost";
      }
     //Show deposit amount  in amount to pay and total amount payable based on deposit radio selection when page loads
      updatePaymentInfo();

      displayTheAmountsInTextFields(PaymentCssClasses.DEPOSIT_AMOUNT_CLASS, PaymentInfo.selectedDepositAmount );
      displayTheAmountsInTextFields(PaymentCssClasses.PAYABLE_AMT_CLASSs, PaymentInfo.selectedDepositAmount );
    }
    else
    {
      backButtonFunctionality();
    }
}

/**
 ** This function is responsible for updating the transaction
 ** amount into the total text box incase of refunds.
 **/
function calculateRefunds()
{
 
       var totalRefundAmount = 0.0;
      //1. Read all text fields as an array
	  var refundCCAmounts = document.getElementsByClassName("hiddenCCRefund");
      var refundAmounts = document.getElementsByClassName("hiddenRefund");


      for(var i = 0, len = refundAmounts.length; i<len; i++)
      {
 
 
         //2. Read the value in each text field and add it to totalRefundAmount.
         //(Apply check if the text field is empty before this step if necessary)
 
         if (refundAmounts[i].value != "")
         {

            totalRefundAmount += 1 * refundAmounts[i].value;
         }
      }
     for(var j = 0, leng = refundCCAmounts.length; j<leng; j++)
      {


 totalRefundAmount += 1 * refundCCAmounts[j].value;

	  }

var totrefundtext=document.getElementById("payment_totalRefundText").value;
var totrefund=document.getElementById("payment_totalRefund").value;

     

      //3. Update totalRefundAmt in PaymentInfo(if necessary)
      PaymentInfo.totalRefundAmt = roundOff(totalRefundAmount,2);



      //4. Update total refund in text field
	  $("#payment_totalRefundText").val(roundOff(PaymentInfo.totalRefundAmt,2));
      $("#payment_totalRefund").val(PaymentInfo.totalRefundAmt);

   }



   function calculateCCRefunds()
{

       var totalRefundAmount = 0.0;
      //1. Read all text fields as an array
      var refundAmounts = document.getElementsByClassName("hiddenRefund");
var refundCCAmounts = document.getElementsByClassName("hiddenCCRefund");


          for(var j = 0, leng = refundCCAmounts.length; j<leng; j++)
      {
 
 
         //2. Read the value in each text field and add it to totalRefundAmount.
         //(Apply check if the text field is empty before this step if necessary)
 
         if (refundCCAmounts[j].value != "")
         {

            totalRefundAmount += 1 * refundCCAmounts[j].value;
         }
      }



          for(var j = 0, leng = refundAmounts.length; j<leng; j++)
      {


 totalRefundAmount += 1 * refundAmounts[j].value;

	  }


 
var totrefundtext=document.getElementById("payment_totalRefundText").value;
var totrefund=document.getElementById("payment_totalRefund").value;

      //3. Update totalRefundAmt in PaymentInfo(if necessary)
      PaymentInfo.totalRefundAmt = roundOff(totalRefundAmount,2);


      //4. Update total refund in text field
	  $("#payment_totalRefundText").val(roundOff(PaymentInfo.totalRefundAmt,2));
      $("#payment_totalRefund").val(PaymentInfo.totalRefundAmt);

   }

/**
* Event handler to be called onblur of refundField.
*/
function refundFieldChangeController(val)
{
  
   var chkBoxIndex = getIndexNumber(val.name);
   //1. Check whether entered amount is valid. If no, empty the text field and return from the function.
   //2. If entered amount is valid, check the check box.
   var refundAmounts = document.getElementsByClassName("refundText");

   var hiddenRefundAmounts = document.getElementsByClassName("hiddenRefund");
   var maxRefundAmountArray = $(".maxRefundAmount");



   var checkBoxStatus = document.getElementsByClassName("confirm");
   var regex =  /^(\-)?[\d]*([\.]?[\d]*)$/;
   errorFields = [];
// clearAllValidationMessages();
   $("#"+PaymentPageIds.PAGE_OUTCOME).addClass(PaymentCssClasses.HIDE);


   if (!isNaN(chkBoxIndex) && refundAmounts[chkBoxIndex].value != "")
   {
	   
      if (!regex.test(1*refundAmounts[chkBoxIndex].value))
         {
         if (checkBoxStatus[chkBoxIndex].checked)
         {
            $('#payment_'+chkBoxIndex+'_datacashreference').attr("checked", "");
         }
         refundAmounts[chkBoxIndex].value = "";
      //   showRefundErrorMessageAtTop($("#"+PaymentPageIds.PAGE_OUTCOME), PaymentConstants.TOP_ERROR_MESSAGE, chkBoxIndex);
         hiddenRefundAmounts[chkBoxIndex].value = "";
		  
         }
      else if (parseFloat(1*refundAmounts[chkBoxIndex].value) < 0)
         {
          alert("Refundable card amount cannot be less than zero ");
		   refundAmounts[chkBoxIndex].value =0;
           hiddenRefundAmounts[chkBoxIndex].value =0;
         }

      else if (maxRefundAmountArray[chkBoxIndex].innerHTML != "" && Math.abs(1*refundAmounts[chkBoxIndex].value) > 1*stripChars(maxRefundAmountArray[chkBoxIndex].innerHTML, $('#currencyText').val()+String.fromCharCode(163)+''+','+' '+'�') || !regex.test(1*refundAmounts[chkBoxIndex].value))
      {
     
         if (checkBoxStatus[chkBoxIndex].checked)
         {
            $('#payment_'+chkBoxIndex+'_datacashreference').attr("checked", "");
         }
		  alert("Refund amount  cannot exceed "+1*stripChars(maxRefundAmountArray[chkBoxIndex].innerHTML,$('#currencyText').val()+String.fromCharCode(163)+''+','+' '+'�'));
         refundAmounts[chkBoxIndex].value = "";
       //  showRefundErrorMessageAtTop($("#"+PaymentPageIds.PAGE_OUTCOME), PaymentConstants.TOP_ERROR_MESSAGE, chkBoxIndex);
         hiddenRefundAmounts[chkBoxIndex].value = "";
       //   refundAmounts[chkBoxIndex].focus();
      }

      else if (maxRefundAmountArray[chkBoxIndex].innerHTML != "" && Math.abs(1*refundAmounts[chkBoxIndex].value) <= 1*stripChars(maxRefundAmountArray[chkBoxIndex].innerHTML, $('#currencyText').val()+String.fromCharCode(163)+''+','+' '+'�'))
      {
     
         refundAmounts[chkBoxIndex].value = roundOff(refundAmounts[chkBoxIndex].value,2);

		 if(StringUtils.equalsIgnoresCase(refundAmounts[chkBoxIndex].value, '0.00')){
	
			document.getElementById('refund_'+chkBoxIndex+'_cardchecked').value = 'false';
		 }else{
	
			document.getElementById('refund_'+chkBoxIndex+'_cardchecked').value = 'true';
		 }
         updateTransAmt(refundAmounts[chkBoxIndex].value, chkBoxIndex);
	     if (refundByChequeChecked)
         {
	
            refundByCardAmountChangeController(chkBoxIndex);
         }
    
      }
      else if (maxRefundAmountArray[chkBoxIndex].innerHTML == "" && $('#payment_'+chkBoxIndex+'_datacashreference').val() == "RefundCheque")
      {
    
         refundAmounts[chkBoxIndex].value = roundOff(refundAmounts[chkBoxIndex].value,2);
         updateTransAmt(refundAmounts[chkBoxIndex].value, chkBoxIndex);
      }
    
      calculateRefunds();
   }
   else if (!isNaN(chkBoxIndex) && refundAmounts[chkBoxIndex].value == "")
   {
      if (checkBoxStatus[chkBoxIndex].checked)
      {
         $('#payment_'+chkBoxIndex+'_datacashreference').attr("checked", "");
      }
      refundAmounts[chkBoxIndex].value = "";
      hiddenRefundAmounts[chkBoxIndex].value = "";
      calculateRefunds();
   }
}

function refundCCFieldChangeController(val)
{
    
   var chkBoxIndex = getIndexNumber(val.name);
   //1. Check whether entered amount is valid. If no, empty the text field and return from the function.
   //2. If entered amount is valid, check the check box.
   var refundAmounts = document.getElementsByClassName("refundCCText");

   var hiddenRefundAmounts = document.getElementsByClassName("hiddenCCRefund");
   var maxRefundAmountArray = $(".maxRefundCCAmount");
   
   

   var checkBoxStatus = document.getElementsByClassName("confirmCC");
   var regex =  /^(\-)?[\d]*([\.]?[\d]*)$/;
   errorFields = [];
// clearAllValidationMessages();
   $("#"+PaymentPageIds.PAGE_OUTCOME).addClass(PaymentCssClasses.HIDE);


   if (!isNaN(chkBoxIndex) && refundAmounts[chkBoxIndex].value != "")
   {
	
        if (!regex.test(1*refundAmounts[chkBoxIndex].value))
         {
         if (checkBoxStatus[chkBoxIndex].checked)
         {
            $('#payment_'+chkBoxIndex+'_datacashreferenc').attr("checked", "");
         }
         refundAmounts[chkBoxIndex].value = "";
        // showRefundErrorMessageAtTop($("#"+PaymentPageIds.PAGE_OUTCOME), PaymentConstants.TOP_ERROR_MESSAGE, chkBoxIndex);
         hiddenRefundAmounts[chkBoxIndex].value = "";
		  
         }
       else if (parseFloat(1*refundAmounts[chkBoxIndex].value) < 0)
         {
          alert("Refundable card amount cannot be less than zero ");
		   refundAmounts[chkBoxIndex].value =0;
           hiddenRefundAmounts[chkBoxIndex].value =0;
         }
      else if (maxRefundAmountArray[chkBoxIndex].innerHTML != "" && Math.abs(1*refundAmounts[chkBoxIndex].value) > 1*stripChars(maxRefundAmountArray[chkBoxIndex].innerHTML, $('#currencyText').val()+String.fromCharCode(163)+''+','+' '+'�') || !regex.test(1*refundAmounts[chkBoxIndex].value))
      {
    
         if (checkBoxStatus[chkBoxIndex].checked)
         {
            $('#payment_'+chkBoxIndex+'_datacashreferenc').attr("checked", "");
         }
         alert("Total refundable card charge cannot exceed "+1*stripChars(maxRefundAmountArray[chkBoxIndex].innerHTML,$('#currencyText').val()+String.fromCharCode(163)+''+','+' '+'�'));
         refundAmounts[chkBoxIndex].value = 1*stripChars(maxRefundAmountArray[chkBoxIndex].innerHTML,$('#currencyText').val()+String.fromCharCode(163)+''+','+' '+'�');
        // showRefundErrorMessageAtTop($("#"+PaymentPageIds.PAGE_OUTCOME), PaymentConstants.TOP_ERROR_MESSAGE, chkBoxIndex);
         hiddenRefundAmounts[chkBoxIndex].value = "";
         refundAmounts[chkBoxIndex].value = roundOff(refundAmounts[chkBoxIndex].value,2);
		  updateCCTransAmt(refundAmounts[chkBoxIndex].value, chkBoxIndex);
    
      }

         
      else if (maxRefundAmountArray[chkBoxIndex].innerHTML != "" && Math.abs(1*refundAmounts[chkBoxIndex].value) <= 1*stripChars(maxRefundAmountArray[chkBoxIndex].innerHTML, $('#currencyText').val()+String.fromCharCode(163)+''+','+' '+'�'))
      {
    
         refundAmounts[chkBoxIndex].value = roundOff(refundAmounts[chkBoxIndex].value,2);

		 if(StringUtils.equalsIgnoresCase(refundAmounts[chkBoxIndex].value, '0.00')){
			
			document.getElementById('refund_'+chkBoxIndex+'_cardchecked').value = 'false';
		 }else{
						document.getElementById('refund_'+chkBoxIndex+'_cardchecked').value = 'true';
		 }
         updateCCTransAmt(refundAmounts[chkBoxIndex].value, chkBoxIndex);
	     if (refundByChequeChecked)
         {
		
            refundByCardAmountChangeController(chkBoxIndex);
         }
       
      }
      else if (maxRefundAmountArray[chkBoxIndex].innerHTML == "" && $('#payment_'+chkBoxIndex+'_datacashreference').val() == "RefundCheque")
      {
    
         refundAmounts[chkBoxIndex].value = roundOff(refundAmounts[chkBoxIndex].value,2);
         updateCCTransAmt(refundAmounts[chkBoxIndex].value, chkBoxIndex);
      }
    
      calculateCCRefunds();
   }
   else if (!isNaN(chkBoxIndex) && refundAmounts[chkBoxIndex].value == "")
   {
      if (checkBoxStatus[chkBoxIndex].checked)
      {
         $('#payment_'+chkBoxIndex+'_datacashreferenc').attr("checked", "");
      }
      refundAmounts[chkBoxIndex].value = "";
      hiddenRefundAmounts[chkBoxIndex].value = "";
      calculateCCRefunds();
   }
}

function refundCCFieldClickController(val)
{


//$(PaymentPageIds.ERROR_LIST_CONTAINER).removeClassName(PaymentCssClasses.HIDE);

   var chkBoxIndex = getIndexNumber(val.name);





     document.getElementById("payment_"+chkBoxIndex+"_transamtCCText")
   var refundAmounts = document.getElementsByClassName("refundCCText");
 
   var hiddenRefundAmounts = document.getElementsByClassName("hiddenCCRefund");
   
   var checkBoxStatus = document.getElementsByClassName("confirmCC");
   var refundByChequeBoxStatus = document.getElementById("refundbycheque_checked");
   var refundByChequeBoxValue = document.getElementById("payment_"+chkBoxIndex+"_datacashreference")

   if (checkBoxStatus[chkBoxIndex].checked)
   {

		document.getElementById("payment_"+chkBoxIndex+"_transamtCCText").style.display='block';
		document.getElementById("refundspan").style.display='block';
      //The below condition is added so that If the checkbox for "refund by cheque" is selected, the input field for that line is calculated as the total
      //refund amount, less any entered amounts for the above card payments.

      if (refundByChequeBoxValue.value == "RefundCheque")
      {


         refundByChequeBoxStatus.value='true';
         refundByChequeIndex = chkBoxIndex;
         updateRefundChequeAmount(chkBoxIndex, val);
      }
      else
      {
       refundAmounts[chkBoxIndex].focus();
      }

   }
   else
   {

  // document.getElementById("payment_"+chkBoxIndex+"_transamtCCText").style.display = 'none';
   //document.getElementById("refundspan").style.display='none';
      refundByChequeChecked = false;
      refundAmounts[chkBoxIndex].value = "";
      hiddenRefundAmounts[chkBoxIndex].value = "";
      calculateCCRefunds();
   }

}

function refundFieldClickController(val)
{

   var chkBoxIndex = getIndexNumber(val.name);
     
   var refundAmounts = document.getElementsByClassName("refundText");
 
   var hiddenRefundAmounts = document.getElementsByClassName("hiddenRefund");
    
   var checkBoxStatus = document.getElementsByClassName("confirm");
   var refundByChequeBoxStatus = document.getElementById("refundbycheque_checked");
   var refundByChequeBoxValue = document.getElementById("payment_"+chkBoxIndex+"_datacashreference")

   if (checkBoxStatus[chkBoxIndex].checked)
   {

      //The below condition is added so that If the checkbox for "refund by cheque" is selected, the input field for that line is calculated as the total
      //refund amount, less any entered amounts for the above card payments.

      if (refundByChequeBoxValue.value == "RefundCheque")
      {


         refundByChequeBoxStatus.value='true';
         refundByChequeIndex = chkBoxIndex;
         updateRefundChequeAmount(chkBoxIndex, val);
      }
      else
      {
       refundAmounts[chkBoxIndex].focus();
      }

   }
   else
   {

      refundByChequeChecked = false;
      refundAmounts[chkBoxIndex].value = "";
      hiddenRefundAmounts[chkBoxIndex].value = "";
      calculateRefunds();
   }

}

/**
 ** This function is used to parse the text box name
 ** and return the index value back to the caller.
 ** @param name - name of the object.
 **/
function getIndexNumber(name)
{
   var indexArray = name.split("_");
   var count = Number(indexArray[1]);
   return count;
}




/**
 *Updates essential fields when user clicks submit button.
*/
 function updateEssentialFields()
 {
 $("#total_transamt").val(PaymentInfo.calculatedPayableAmount);
  var sel = document.getElementById('country');
  document.getElementById('payment_0_selectedCountry').value = sel.options[sel.selectedIndex].value;
}


/**
 * Contains field names which has to be set to default when page loads.
 *
 */
function setToDefault()
{
  //Set to default
  //Don't replace it by Jquery. Required for Chrome and Safari.
  document.getElementById(PaymentPageIds.CARD_TYPE_ID).selectedIndex=0;
  document.getElementById(PaymentPageIds.EXPIRY_MONTH).selectedIndex=0;
  document.getElementById(PaymentPageIds.EXPIRY_YEAR).selectedIndex=0;
  //Don't replace it by Jquery. Required for Chrome and Safari.
  $("#"+PaymentPageIds.CARD_NUMBER).val("");
  $("#"+PaymentPageIds.CARD_NAME).val("");

  $("#"+PaymentPageIds.SECURITY_CODE).val("");

  $("#"+PaymentPageIds.ISSUE_NO).val("");

}



/*----------------------PopUp function -----------------------------------------------------------*/
function toggleOverlay(){
  var zIndex = 100;

  $(".genericOverlay").hide();
  $("a.blinkyOwner").click(function(e){
    var overlay = this.id + "Overlay";
    $(overlay).show();

    $(overlay + ".genericOverlay").css("z-index",zIndex);
    zIndex++;
  });

  $("a.close").click(function(){
    var overlay = this.id.replace("Close","Overlay");
    $(overlay).hide();
  });
}

//Convert links with rel external to target _blank to maintain strict compat
function targetBlank()
{
   if (!document.getElementsByTagName) return;
   var anchors = document.getElementsByTagName("a");
   for (var i=0; i<anchors.length; i++)
   {
      var anchor = anchors[i];
      if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external")
      {
         anchor.title = "The following link opens in a new window";
         var theScript = 'JavaScript:void(Popup("' + anchor.getAttribute('href') + '"));';

         anchor.setAttribute("href", theScript);

         // We don't want the target attribute as this will cause problems with the script.
         anchor.removeAttribute("target");
      }
   }
}

/*----------------------*****END PopUp function *****-------------------------------------------------*/

document.getElementsByClassName = function(clsName)
{
   var retVal = new Array();
   var elements = document.getElementsByTagName("*");
   for(var i = 0;i < elements.length;i++)
   {
      if(elements[i].className.indexOf(" ") >= 0)
      {
         var classes = elements[i].className.split(" ");
         for(var j = 0;j < classes.length;j++)
         {
            if(classes[j] == clsName)
               retVal.push(elements[i]);
         }
      }
      else if(elements[i].className == clsName)
         retVal.push(elements[i]);
   }
   return retVal;
}

function negateAmount()
{
   var refundBalanceAmt = document.getElementsByClassName("totalcalculatedPayableAmount");
   for(var i = 0; i < refundBalanceAmt.length; i++)
   {
      refundBalanceAmt[i].innerHTML = roundOff(Math.abs(1*refundBalanceAmt[i].innerHTML),2);
   }
}


function updateTransAmt(refundAmount, chkBoxIndex)
{

	//alert(StringUtils.equalsIgnoresCase(refundAmount, '0.00'));
   if(StringUtils.equalsIgnoresCase(refundAmount, '0.00')){

		$('#payment_'+chkBoxIndex+'_datacashreference').attr("checked", "");
	}else{
		$('#payment_'+chkBoxIndex+'_datacashreference').attr("checked", "checked");
	}

   var absoluteRefundAmount = Math.abs(refundAmount);


   if (absoluteRefundAmount != refundAmount)
   {

      $('#payment_'+chkBoxIndex+'_transamt').val(refundAmount);
      $('#payment_'+chkBoxIndex+'_transamtText').val(absoluteRefundAmount);
   }
   else
   {

     $('#payment_'+chkBoxIndex+'_transamt').val(refundAmount);
	 


   }
}

function updateCCTransAmt(refundAmount, chkBoxIndex)
{

	//alert(StringUtils.equalsIgnoresCase(refundAmount, '0.00'));
   if(StringUtils.equalsIgnoresCase(refundAmount, '0.00')){

		$('#payment_'+chkBoxIndex+'_datacashreferenc').attr("checked", "");
	}else{
		$('#payment_'+chkBoxIndex+'_datacashreferenc').attr("checked", "checked");
	}

   var absoluteRefundAmount = Math.abs(refundAmount);


   if (absoluteRefundAmount != refundAmount)
   {

      $('#payment_'+chkBoxIndex+'_transCCamt').val(refundAmount);
      $('#payment_'+chkBoxIndex+'_transamtCCText').val(absoluteRefundAmount);
   }
   else
   {

     
	 $('#payment_'+chkBoxIndex+'_transCCamt').val(refundAmount);
	


   }
}






/* The below function updates the refund amount field automatically if the user checks the refund by cheque option*/
function updateRefundChequeAmount(chkBoxIndex, val)
{
   var totalRefundAmount = 0.0;
   //1. Read all text fields as an array
   var refundAmounts = document.getElementsByClassName("hiddenRefund");
   for(var i = 0, len = refundAmounts.length; i<len; i++)
   {
      //2. Read the value in each text field and add it to totalRefundAmount.
      //(Apply check if the text field is empty before this step if necessary)
      if (refundAmounts[i].value != "")
      {
         totalRefundAmount += 1 * refundAmounts[i].value;
      }
   }
   //The refund Amount
   var refundAmount = Math.abs(stripChars($(".totalcalculatedPayableAmount").html(), String.fromCharCode(163)+''+','+' '+'�'));
   if (refundAmount > Math.abs(totalRefundAmount))
   {
     $('#payment_'+chkBoxIndex+'_transamtText').val(roundOff(refundAmount - Math.abs(totalRefundAmount),2));
     refundFieldChangeController(val);
   }
   else
   {
     $('#payment_'+chkBoxIndex+'_transamtText').val("");
     $('#payment_'+chkBoxIndex+'_datacashreference').attr("checked", "");
   }
}

/* The below function updates the refund cheque amount field automatically if the user enters or changes the amount in the refund by card option*/
function refundByCardAmountChangeController(chkBoxIndex, val)
{


   PaymentInfo.totalRefundAmt

   var totalRefundAmount = 0.0;
   //1. Read all text fields as an array
   var refundAmounts = document.getElementsByClassName("hiddenRefund");



   for(var i = 0, len = refundAmounts.length - 1; i<len; i++)
   {
      //2. Read the value in each text field and add it to totalRefundAmount.Here we ignore the amount paid by cheque.
      //(Apply check if the text field is empty before this step if necessary)
      if (refundAmounts[i].value != "")
      {
        //alert("refundAmounts[i].value ::"+refundAmounts[i].value);
         totalRefundAmount += 1 * refundAmounts[i].value;

      }
   }
   //The refund Amount

   refundByChequeIndex = chkBoxIndex;
   var refundAmount = Math.abs(stripChars($(".totalcalculatedPayableAmount").html(), String.fromCharCode(163)+''+','+' '+'�'));

    var refundByCardAmount = Math.abs(totalRefundAmount);
    var hiddenRefundAmounts = document.getElementsByClassName("hiddenRefund");


    if (refundAmount > refundByCardAmount)
    {
      $('#payment_'+refundByChequeIndex+'_transamtText').val(roundOff(refundAmount - refundByCardAmount,2));
      $('#payment_'+refundByChequeIndex+'_datacashreference').attr("checked", "checked");
     //hiddenRefundAmounts[refundByChequeIndex].value =  -(roundOff(refundAmount - refundByCardAmount,2));
    }
    else
    {
      // $('#payment_'+refundByChequeIndex+'_transamtText').val("");
      //$('#payment_'+refundByChequeIndex+'_datacashreference').attr("checked", "");
      //hiddenRefundAmounts[refundByChequeIndex].value = "";
    }
}

/*---------------------------a. Constants -------------------------------------------------*/
/**
 * Object containing Class names used in Payment Page.
 */
var PaymentCssClasses =
{
  /***Class names used for payment update on payment page */
  PAYABLE_AMT_CLASS      : "calPayableAmount",
  TOTAL_AMOUNT_CLASS     :"totalAmount",
  DEPOSIT_AMOUNT_CLASS   :"depositAmount",
  AMOUNT_PAID_CLASS      :"amountPaid",

  /** Deposit radio button class names */
  DEPOSIT_RADIO_CLASS : "deposits",

  HIDE                 :"hide",
  SHOW                 :"show",

  IN_ERROR             :"inError",
  REFUND_CHECKBOX_CLASS:"confirm",
  REFUND_TEXTCLASS     :"refundAmount",
  TOTAL_CARD_CHARGE_CLASS : "totalCardChargeClass",
   REFUND_CC_TEXTCLASS:"refundCCAmount",
  REFUND_CC_CHECKBOX_CLASS:"confirmCC"

};

/***
 * Object holding ids in payment page
 */
var PaymentPageIds =
{
  /** card type id */
  CARD_TYPE_ID              : "cardType",
  /** Amount to Pay  Id . It allows user to enter amount if pay in part radio button is selected  */
  AMOUNT_TO_PAY_ID          : "amountToPay",
  TOTAL_AMT_PAYABLE         :"totalAmountPayable",

  EXPIRY_MONTH              :"expiryMonth",
  EXPIRY_YEAR               :"expiryYear",

  CARD_NUMBER               :"cardNumber",
  CARD_NAME                 :"nameOnCard",

  SECURITY_CODE             :"cardSecurityCode",
  /** Id of span/div tag containing the text which appears next to security code field. */
  SECURITY_CODE_CAPTION     :"cardSecurityCodeLink",

  ISSUE_NO                  :"issueNumber",
  ISSUE_NO_CONTROL_GROUP    :"issueNumberControlGroup",

  PAGE_OUTCOME         : "pageOutcome",
  ERROR_LIST           : "errorList",
  ERROR_LIST_CONTAINER :"errorListContainer",

  PAGE_MESSAGE         :"pageMessage",
  ERROR_INTRO          : "errorIntro"
}

var PaymentConstants =
{
  /**Full cost deposit radio option value */
  FULL_COST           : "fullCost",

  /** Pay in Part is a deposit radio option value, to allow user to enter deposit amount in amount to pay field */
  PAY_IN_PART          : "partialDeposit",

  DEFAULT_CARD_SELECTION    : "",
  DEFAULT_SECURITY_CODE_LEN : 3,
  TOP_ERROR_MESSAGE    :"We cannot process your payment",
  ERROR_INTRO_TEXT     : "Please check the following details are correct"
}

/*-----------------*****END constants *****-----------------------------------------*/


/*----------------------Browser back functionality ------------------------------------------------*/
/**
 * function will be called when user clicks on the browser back button in the confirmation page
 * It makes all fields read only and changes submit button to action neutral (act as "Forward").
 */
function backButtonFunctionality()
{
  /*$(PaymentPageIds.PAGE_OUTCOME).removeClassName(PaymentCssClasses.HIDE);
  $(PaymentPageIds.PAGE_MESSAGE).innerHTML = "Your payment has been confirmed";
  $(PaymentPageIds.ERROR_LIST_CONTAINER).addClassName(PaymentCssClasses.HIDE);

  makeDropDownsReadOnly(PaymentPageIds.CARD_TYPE_ID);
  makeDropDownsReadOnly(PaymentPageIds.EXPIRY_MONTH);
  makeDropDownsReadOnly(PaymentPageIds.EXPIRY_YEAR);

  $(PaymentPageIds.CARD_NUMBER).readOnly = true;
  $(PaymentPageIds.CARD_NAME).readOnly = true;

  $(PaymentPageIds.SECURITY_CODE).readOnly = true;

  $(PaymentPageIds.ISSUE_NO).readOnly = true;
  */
  if ($("#paymentFlow").val() == "PAYMENT")
  {
  setToDefault();
  }
  hidePayNowButton();
}

/**
 *
 *
 */
function hidePayNowButton()
{
  var payNowButtonSpan = $("#forwardButton");
  if(payNowButtonSpan)
  {
     payNowButtonSpan.html("");
     //payNowButtonSpan.innerHTML = '<input type="button" id="Forward" title="Forward" class="primary" alt="" value="Forward" onClick="history.forward()" />';
  }
}

/**
 * Make all drop downs read only. So user will not be able to change the drop down
 *
 * @param dropdownId
 * @return
 */
function makeDropDownsReadOnly(dropdownId)
{
  var dropdown = $("#"+dropdownId);
  var chosen = dropdown.options.selectedIndex;
  dropdown.onchange = function()
  {
   dropdown.options.selectedIndex = chosen;
  }
  dropdown.className = "greyed";
}
/*-----------------*****END Browser back functionality *****----------------------------------------*/