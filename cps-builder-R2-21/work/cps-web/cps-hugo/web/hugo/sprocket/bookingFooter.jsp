<%-- 200508 --%>
<%-- The QUERY_STRING is booking=true --%>
<%@include file="/common/commonTagLibs.jspf"%>
<div class="pnlOctober">
<div id="Footer">
	<a name="footer" id="footer"></a>
	<div id="atol_abta">
		<c:choose><c:when test="${(bookingComponent.nonPaymentData['atol_flag']  != null) &&( bookingComponent.nonPaymentData['atol_flag'] )&& (bookingComponent.nonPaymentData['atol_protected'] )}">
			 <a href="http://www.abta.com/" title="ABTA" rel="external"
			class="noicon" target="_blank"> <img
			src="/cms-cps/firstchoice/hugo/images/footer/logo-abta_2009.gif"
			height="29" width="65" alt="ABTA logo" class="foot_img_1" /> </a><a href="http://www.atol.org.uk/" title="ATOL" rel="external" class="noicon" target="_blank"> <img src="/cms-cps/firstchoice/hugo/images/footer/atol_logo.gif" 			width="41" height="41" alt="ATOL logo" class="foot_img_2" /> </a>
			</c:when>

			<c:when test="${(bookingComponent.nonPaymentData['atol_flag']  != null) &&( bookingComponent.nonPaymentData['atol_flag'] )&& (!bookingComponent.nonPaymentData['atol_protected'] )}"><a href="http://www.abta.com/" title="ABTA" rel="external"
			class="noicon" target="_blank"> <img
			src="/cms-cps/firstchoice/hugo/images/footer/logo-abta_2009.gif"
			height="29" width="65" alt="ABTA logo" class="foot_img_1" /> </a>
			</c:when>

			<c:otherwise>
			<a href="http://www.abta.com/" title="ABTA" rel="external"
			class="noicon" target="_blank"> <img
			src="/cms-cps/firstchoice/hugo/images/footer/logo-abta_2009.gif"
			height="29" width="65" alt="ABTA logo" class="foot_img_1" /> </a><a href="http://www.atol.org.uk/" title="ATOL" rel="external" class="noicon" target="_blank"> <img src="/cms-cps/firstchoice/hugo/images/footer/atol_logo.gif" 			width="41" height="41" alt="ATOL logo" class="foot_img_2" /> </a>
			</c:otherwise>
			</c:choose>
			<c:choose>
	  			<c:when test="${( bookingComponent.nonPaymentData['atol_flag']  !=null ) &&(bookingComponent.nonPaymentData['atol_flag'] ) &&(bookingComponent.nonPaymentData['atol_protected'] )}">
			</c:when>
			<c:otherwise>
				<p>
					The air holiday packages shown are ATOL protected by the Civil
					Aviation Authority. Our ATOL number is ATOL 2524.<br /> ATOL
					protection does not apply to all holiday and travel services shown on
					this website. Please see our booking conditions for more information.
				</p>

				<script>
				jQuery(document).ready(function (){
					jQuery('.pnlOctober').removeClass();
				});
			</script>
		</c:otherwise>
	</c:choose>
	</div>
	<%-- end atol_abta--%>
	<div id="copyrightUtilityMenu">
		<ul>
			<li><a class="noicon" rel="nofollow"
				href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/accessibility/?popup=true',630,600,'scrollbars=yes');">Accessibility</a>
			</li>
			<li><a class="noicon" rel="nofollow"
				href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/terms-of-use/?popup=true',630,600,'scrollbars=yes');">Terms
					of use</a></li>
			<li><a class="noicon" rel="nofollow"
				href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/security-policy/?popup=true',630,600,'scrollbars=yes');">Security
					policy</a></li>
			<li><a class="noicon" rel="nofollow"
				href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/privacy-policy/?popup=true',630,600,'scrollbars=yes');">Privacy
					policy</a></li>
   <li class="last"><a class="noicon" rel="nofollow"
    href="javascript:Popup('http://www.firstchoice.co.uk/help/booking-online/credit-card-payments-and-charges/?popup=true',630,600,'scrollbars=yes');">Credit Card Fees</a></li>
		</ul>
	</div>
	<%-- end copyright and utilities--%>
</div>
<%--Omniture VS Tag START--%>
<!--Deleted script tag for INC000012246207 -->
<script type="text/javascript">
var pqry = '<c:out value="${bookingComponent.intelliTrackSnippet.intelliTrackerString}"/>';
<c:set var="pqry" value="${bookingComponent.intelliTrackSnippet.intelliTrackerString}"/>
   <c:if test="${not empty pqry}">

        <c:choose>
           <c:when test="${fn:contains(bookingComponent.errorMessage, 'Please re-enter your card details or try again with a different card.')}">
               pqry = pqry + "%26CpsServer%3D<c:out value='${param.tomcat}'/>%26PageName%3DPayment%26declined%3Dtrue";
           </c:when>
           <c:otherwise>
               pqry = pqry + "%26CpsServer%3D<c:out value='${param.tomcat}'/>%26PageName%3DPayment%26declined%3Dfalse";

           </c:otherwise>
         </c:choose>

    </c:if>


</script>


</div>
<%--Omniture VS Tag END//--%>
