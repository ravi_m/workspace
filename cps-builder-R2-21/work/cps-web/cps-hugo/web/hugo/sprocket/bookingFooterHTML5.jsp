<%-- 200508 --%>
<%-- The QUERY_STRING is booking=true --%>
<%@include file="/common/commonTagLibs.jspf"%>

<div id="Footer">


	<div class="footerInnerWrapper">
		<p id="footerCopyright">&copy;2013 First Choice UK</p>
		<ul id="footerPolicy">
			<li><a href="http://www.firstchoice.co.uk/about-us/" target="_blank">About us</a></li>
			<li><a href="http://www.firstchoice.co.uk/our-policies/accessibility/" target="_blank">Accessibility</a></li>
			<li><a href="http://www.firstchoice.co.uk/affiliates.html" target="_blank">Affiliates</a></li>
			<li><a href="http://www.firstchoice.co.uk/our-policies/terms-of-use/" target="_blank">Terms of Use</a></li>
			<li><a href="http://www.firstchoice.co.uk/our-policies/security-policy/" target="_blank">Security Policy</a></li>
			<li><a href="http://www.firstchoice.co.uk/our-policies/privacy-policy/" target="_blank">Privacy Policy</a></li>
			<li><a href="http://www.firstchoice.co.uk/our-policies/statement-on-cookies/" target="_blank">Statement on Cookies</a></li>
			<li class="last"><a href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges/" target="_blank">Credit Card Fees</a></li>
		</ul>
		<p id="footerSocialLinks">Follow us on
			<a href="http://twitter.com/Firstchoiceuk#" class="twitterIcon" target="_blank">&nbsp;</a>
			<a href="http://www.facebook.com/#!/Firstchoiceholidays" class="facebookIcon" target="_blank">&nbsp;</a>
		</p>
		<div class="clear"></div>
		<!--#if expr="$QUERY_STRING = /(atolrequired=true)/"-->
		<!--#else -->
		<p id="msgATOL">The air holiday packages shown are ATOL protected by the Civil Aviation Authority. Our ATOL number is ATOL 2524.
		ATOL protection does not apply to all holiday and travel services shown on this website.
		Please see our booking conditions for more information
		</p>
		<!--#endif-->
	</div>





</div>
<!--Deleted script tag for INC000012246207 -->
<%--Omniture VS Tag START--%>
<script type="text/javascript">
var pqry = '<c:out value="${bookingComponent.intelliTrackSnippet.intelliTrackerString}"/>';
<c:set var="pqry" value="${bookingComponent.intelliTrackSnippet.intelliTrackerString}"/>
   <c:if test="${not empty pqry}">

        <c:choose>
           <c:when test="${fn:contains(bookingComponent.errorMessage, 'Please re-enter your card details or try again with a different card.')}">
               pqry = pqry + "%26CpsServer%3D<c:out value='${param.tomcat}'/>%26PageName%3DPayment%26declined%3Dtrue";
           </c:when>
           <c:otherwise>
               pqry = pqry + "%26CpsServer%3D<c:out value='${param.tomcat}'/>%26PageName%3DPayment%26declined%3Dfalse";

           </c:otherwise>
         </c:choose>

    </c:if>


</script>



<%--Omniture VS Tag END//--%>
