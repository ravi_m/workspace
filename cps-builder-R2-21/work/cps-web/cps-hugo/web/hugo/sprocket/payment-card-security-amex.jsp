<div class="overlayWrapper pads">
  <div class="headerBlock">
    <h4><span class="padder"><%-- InstanceBeginEditable name="overlay-heading" --%>Card Security Code <%-- InstanceEndEditable --%></span></h4>
  </div>
  <div class="overlayPadder">
    <div class="contentBlock">

	  	<%-- InstanceBeginEditable name="overlay-content" --%>
	  	<p><span class="marginBefore">For American Express cards, you can find your four digit verification number (CID) on the front of the card above the card number on either the left or right side.</span> As an added protection against card fraud, we ask for this number when you book   one of our holidays online.</p>
	  	<p>&nbsp;</p>
	  	<p><img src="/cms-cps/firstchoice/hugo/images/bookings/amex-cvc-illustration.jpg" width="250" height="158" /> </p>
	  	<%-- InstanceEndEditable --%>

      <div class="clearer"></div>
    </div>
    <%--end contentBlock--%>
  </div>
  <%--end overlayPadder--%>
</div>
<%--end overlayWrapper--%>
