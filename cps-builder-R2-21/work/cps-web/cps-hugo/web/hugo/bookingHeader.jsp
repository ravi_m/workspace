<!--#if expr="$REQUEST_URI=/\/latestinfo.page*/ || $REQUEST_URI=/\/passengerdetails.page*/" -->

<!--#set var="poHeader" value="false"-->
 
<!--#else -->
 
<!--#set var="poHeader" value="true"-->
 
<!--#endif-->


<!--#if expr="$poHeader=true" -->

<!--#endif-->
<!-- 200508 -->

<!--#set var="qstr" value="$QUERY_STRING" -->
<!-- The QUERY_STRING is <!--#echo var="qstr" --> -->

<!--#if expr="$QUERY_STRING = /(^booking=true*)/"-->
   <!--#set var="ext" value="true"-->
<!--#endif-->
<!--#if expr="$HTTP_COOKIE != /(consentcookie=true*)/"-->
<!--#include virtual="/CookieLegislation/cookielegislationview?brand=fcsun" -->
<!--#endif-->


		<!-- Header Start -->
			<div id="header" >
				<div class="beta">
					<p>Welcome to our brand new website.</p>
					<p class="tell-us">
						<a href="https://www.tuisurveys.co.uk/se.ashx?s=705E3F6F32F1F661" target="_blank" class="ensLinkTrack" data-componentId="beta_site_banner">Tell us what you think</a> or <a href="http://www.firstchoice.co.uk/beta/contact-us/send-us-an-email/report-a-problem/" target="_blank" class="ensLinkTrack" data-componentId="beta_site_banner">Report a problem</a>
					</p>
					<a data-dojo-type="tui.widget.popup.DynamicPopup" data-dojo-props="widgetId:'tellUsWhatYouThink',modal:true,scrollable:true" href="#" class="button">Switch to main site</a>
				</div>
				<div class="message-window" style="display:none" id="tellUsWhatYouThink">
					<a href="#" class="close ensLinkTrack" data-componentId="beta_site_banner"> <span class="text">Close</span></a>
					<div class="message-content small-pop beta-pop">
						<h3>Before you go...</h3>
						<p>We'd love to know what you made of the new site:</p>
						<ul>
							<li><a href="http://www.firstchoice.co.uk/" data-dojo-type="tui.widget.MultiLinkAnchor" data-dojo-props="newWindowLink:'https://www.tuisurveys.co.uk/se.ashx?s=705E3F6F32F1F661'" class="ensLinkTrack" data-componentId="beta_site_banner">Tell us what you think</a></li>
							<li><a href="http://www.firstchoice.co.uk/?customer=switch" class="ensLinkTrack" data-componentId="beta_site_banner">Switch to our main site</a></li>
						</ul>
					</div>
				</div>
				<div id="utils">
					<ul>
						<li><a href="http://www.firstchoice.co.uk/our-policies/statement-on-cookies/" title="Statement on Cookies" class="ensLinkTrack" data-componentId="global_header_utils_para_comp">Statement on Cookies</a></li>
						<li><a href="https://eapi.thomson.co.uk/eborders/login.jsp" title="API login" class="ensLinkTrack" data-componentId="global_header_utils_para_comp">API login</a></li>
					</ul>
				</div>
				<script>var domainName;dojoConfig.addModuleName('tui/widget/popup/DynamicPopup','tui/widget/CookieNotifier');</script>
				<div id="logo" data-dojo-type="tui.widget.popup.DynamicPopup" data-dojo-props="widgetId:'justSoYouKnow',modal:true, scrollable:true">
					<a data-componentid="global_header_logo_para_comp" class="ensLinkTrack" href="http://www.firstchoice.co.uk">First Choice</a>
					<img class="print-only" alt="First Choice" src="/holiday/images/logo-first-choice-print.gif" />
				</div>
				<div class="message-window" style="display:none" id="justSoYouKnow">
					<a href="#" class="close ensLinkTrack" data-componentId="global_header_logo_para_comp">
					<span class="text">Close</span></a>
				   <div class="message-content medium-pop beta-pop">
					  <h3>Just so you know...</h3>
					  <p>you're about to switch from our new beta site to our main site</p>
					  <h4>Before you go, we'd love to know what you made of the site.</h4>
					  <ul>
						 <li><a href="http://www.firstchoice.co.uk/"
				data-dojo-type="tui.widget.MultiLinkAnchor"
				data-dojo-props="newWindowLink:'https://www.tuisurveys.co.uk/se.ashx?s=705E3F6F32F1F661'"
				class="ensLinkTrack" data-componentId="global_header_logo_para_comp">Tell us what you
				think</a></li>
						 <li><a href="http://www.firstchoice.co.uk/?customer=switch" class="ensLinkTrack"
				data-componentId="global_header_logo_para_comp">Switch to our main site</a></li>
					  </ul>
				   </div>
				</div>
				<div id="nav">
					<ul>
						<li><a href="http://www.firstchoice.co.uk/sun-holidays/" class="ensLinkTrack" data-componentId="global_header_nav" data-dojo-type="tui.widget.popup.DynamicPopup" data-dojo-props="widgetId:'justSoYouKnowHolidays',modal:true, scrollable:true">Holidays</a></li>
						<li class="active"><a href="http://www.firstchoice.co.uk/holiday/destinations" class="ensLinkTrack" data-componentId="global_header_nav">Destinations</a></li>
						<li><a href="http://www.firstchoice.co.uk/last-minute-deals/" class="ensLinkTrack" data-componentId="global_header_nav" data-dojo-type="tui.widget.popup.DynamicPopup" data-dojo-props="widgetId:'justSoYouKnowDeals',modal:true, scrollable:true">Deals</a></li>
					</ul>
				</div>

	<!-- Holidays -->
	
				<div class="message-window" style="display:none" id="justSoYouKnowHolidays">
				   <a href="#" class="close ensLinkTrack" data-componentId="global_header_nav">
						<span class="text">Close</span>
					</a>
				   <div class="message-content medium-pop beta-pop">
					  <h3>Just so you know...</h3>
					  <p>you're about to switch from our new beta site to our main site</p>
					  <h4>Before you go, we'd love to know what you made of the site.</h4>
					  <ul>
						 <li><a href="http://www.firstchoice.co.uk/"
				data-dojo-type="tui.widget.MultiLinkAnchor"
				data-dojo-props="newWindowLink:'https://www.tuisurveys.co.uk/se.ashx?s=705E3F6F32F1F661'"
				class="ensLinkTrack" data-componentId="global_header_nav">Tell us what you
				think</a></li>
						 <li><a href="http://www.firstchoice.co.uk/sun-holidays/?customer=switch" class="ensLinkTrack"
				data-componentId="global_header_nav">Switch to our main site</a></li>
					  </ul>
				   </div>
				</div>
				
	<!-- Deals -->
	
				<div class="message-window" style="display:none" id="justSoYouKnowDeals">
					<a href="#" class="close ensLinkTrack" data-componentId="global_header_nav">
						<span class="text">Close</span>
					</a>
					<div class="message-content medium-pop beta-pop">
					  <h3>Just so you know...</h3>
					  <p>you're about to switch from our new beta site to our main site</p>
					  <h4>Before you go, we'd love to know what you made of the site.</h4>
					  <ul>
						 <li><a href="http://www.firstchoice.co.uk/"
				data-dojo-type="tui.widget.MultiLinkAnchor"
				data-dojo-props="newWindowLink:'https://www.tuisurveys.co.uk/se.ashx?s=705E3F6F32F1F661'"
				class="ensLinkTrack" data-componentId="global_header_nav">Tell us what you
				think</a></li>
						 <li><a href="http://www.firstchoice.co.uk/last-minute-deals/?customer=switch" class="ensLinkTrack"
				data-componentId="global_header_nav">Switch to our main site</a></li>
					  </ul>
				   </div>
				</div>
				
				<div id='quick-search'>
					<form method='get' action='http://www.firstchoice.co.uk/gsa/search'>
						<input type='text' placeholder='Search for anything...' class='textfield icon icon-search' name='q' />
						<input type='hidden' name='site' value='firstchoice_collection' />
						<input type='hidden' name='client' value='fc-hugo-main' />
						<input type='hidden' name='proxystylesheet' value='fc-hugo-main' />
						<input type='hidden' name='output' value='xml_no_dtd' />
						<input type='hidden' name='submit' value='Go' />
					</form>
				</div>
			
				<script type="text/javascript">dojoConfig.addModuleName("tui/widget/search/Search");</script>

				<script type="text/javascript">
					dojoConfig.addModuleName("tui/widget/search/Search");
				</script>
		
				<form data-dojo-type="tui.widget.search.SearchComponent" method="post" action="http://iscape5sat/fcsun/page/waiting/searchwaiting.page">

					
					<!-- Search Panel -->
						<div  id="search">

						</div>
						
					<!-- /Search Panel -->	

					</form>

			</div>
			
		<!-- Header Ends -->
		
<div class="clearer"></div>
<div class="mboxDefault"></div> 
<script type="text/javascript">   
  mboxCreate("F_Recording_Mbox");
</script>