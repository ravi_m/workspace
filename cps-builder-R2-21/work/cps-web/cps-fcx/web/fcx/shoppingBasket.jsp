<fmt:formatNumber var="totalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2"
              minFractionDigits="2" groupingUsed="false" pattern="#,##,###.##"/>
<div id="shopping_basket" >
<p></p>
<div class="underlined">
<div id="basket_image">
<img src="/cms-cps/fcx/images/basket_image.gif">
</div>
</div>
<c:set var="priceComponentLength"
	value="${fn:length(bookingComponent.priceComponents)}" /> <c:if
	test="${priceComponentLength > 0}">

	<c:forEach var="priceComponent"
		items="${bookingComponent.priceComponents}">
		<p class="shoppingDetails"><c:out value="${priceComponent.itemDescription}"
			escapeXml="false" /> </p>
		
		<fmt:formatDate value="${priceComponent.excursionDate}"
			pattern="EEE dd MMM yyyy" />
		<c:set var="priceComponentLengths"
			value="${fn:length(priceComponent.priceComponents)}" />
		

<div class="underlined"></div>

	</c:forEach>
</c:if>
<div id="total">
<b>Total Cost: <c:out
	value="${currencySymbol}" escapeXml="false" /> <c:out
	value="${totalAmount}" /></b>
</div>
</div>