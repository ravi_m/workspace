<input type='hidden' id='panelType' value='CNP' />
<input type='hidden' value='' name='payment_0_paymenttypecode'
	id='payment_0_paymenttypecode' />
<input type='hidden' id='total_amount_0' name='payment_0_amountPaid'
	value='0.0' />
<input name='payment_0_cardCharge' type='hidden' id='cardChargeAmount_0' />
<input name='payment_0_cardLevyPercentage' type='hidden'
	id='cardLevyCharge_0' value='0.0' />
<fmt:formatNumber value="${cardChargePercent}" type="number" var="cardChargeFCX" maxFractionDigits="1" minFractionDigits="0"/>
<br />
<br />
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}"
	scope="request" />

<div id="payment_details">
	<h2 class="underline">PAYMENT DETAILS</h2>
	<c:choose>
		<c:when test="${applyCreditCardSurcharge eq 'true'}">
	<p class="underlined">There are no additional charges when paying by Maestro, MasterCard Debit or Visa/ Delta debit cards. A fee of <c:out value="${cardChargeFCX}" />% applies to credit card payments when using American Express, MasterCard Credit or Visa Credit cards</p>
		</c:when>
		<c:otherwise>
	</c:otherwise>
</c:choose>
<br/><br>
	&nbsp;We accept:
	<div id="card_details_img">
		&nbsp;<img src="/cms-cps/thx/images/card-icons.gif">
	</div>
	<br />

	<fmt:formatNumber var="transactionTotal"
		value="${bookingInfo.calculatedTotalAmount.amount}" type="number"
		maxFractionDigits="2" minFractionDigits="2" scope="request" pattern="######.##" />
	<fmt:formatNumber var="amountWithCardCharge"
		value="${transactionTotal+applicableCardChargeForSelectedAmount}"
		type="number" maxFractionDigits="2" minFractionDigits="2"
		scope="request" groupingUsed="false" />
	<fmt:formatNumber var="amountWithDebitCardCharge"
		value="${transactionTotal+applicableDebitCardChargeForSelectedAmount}"
		type="number" maxFractionDigits="2" minFractionDigits="2"
		scope="request" groupingUsed="false" />


	<ul class="paymentAmountRight">
		<li class="fontWeightNormal" id="amountWithoutCardCharge" style=" float: left;">
		<span id="spanAmountWithoutCardCharge" class="amount">
		<c:out value="${currencySymbol}" escapeXml="false" /><c:out
			value="${amountWithDebitCardCharge}" /></span> <span>Payable today if
		paying by Debit Card</span></li>


<li class="fontWeightNormal" id="thomsonCardCharge" style="float: right; margin-left: 10px; display :none">
        <span id="spanthomsonCardCharge" class="amount">
        <c:out value="${currencySymbol}" escapeXml="false" /><c:out
            value="${amountWithDebitCardCharge}" /></span>
            <span>Includes no extra charge for using TUI Credit Card</span></li>



		<li class="fontWeightNormal" id="amountWithCardCharge" style="float: right; margin-left: 10px;">
		<span id="spanAmountWithCardCharge" class="amount">
		<c:out value="${currencySymbol}" escapeXml="false" /><c:out
			value="${amountWithCardCharge}" /></span> <span>Payable today if
		paying by Credit Card<br>
		<c:if test="${applyCreditCardSurcharge eq 'true'}">
		<p class="note">Includes <c:out value="${cardChargeFCX}" />%
		Credit Card surcharge</p></c:if>
		</span></li>
		
	</ul>
	<div class="formrow pers-details">
		<ul>
			<li class="payment-details"><label for="cardtype">Card
					Type *</label>
				<select id="cardType" name="payment_0_type" gfv_required="required"
				onchange="CardTypeChangeHandler.handleCardSelection(),amountUpdationWithCardCharge.updatePaymentInfoForFCX()"
				alt="Card Type" class="card_select mandatory" data-validation-engine="validate[required]">
					<option id="pleaseSelect" value='' selected="selected">Please
						select</option>
					<c:forEach var="paymentType"
						items="${bookingComponent.paymentType['CNP']}">
						<option
							value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'>


							<%-- <c:out value="${paymentType.paymentDescription}"
								escapeXml='false' /> --%>

								<%-- following code added by sreenivasulu k --%>

									<c:set var="type" value="${paymentType.paymentDescription}"/>
							<c:choose>
								<c:when test = "${fn:containsIgnoreCase(type, 'Thomson')}">
								 TUI CreditCard
								 </c:when>
								<c:otherwise>
											<c:out value="${paymentType.paymentDescription}"
								escapeXml='false' />
								</c:otherwise>
							</c:choose>



						</option>
					</c:forEach>
					<%--  <c:forEach var="paymentType" items="${bookingComponent.paymentType['PPG']}">
                 <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/> '><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option>
         </c:forEach>  --%>
			</select></li>
			<li><label for="cardno">Card Number *</label>
				<input id="cardNumber" class="textfield  mandatory"
				type="cardnumber" alt="Card Number" autocomplete="off"
				maxlength="20" gfv_required="required" name="payment_0_cardNumber" data-validation-engine="validate[required,custom[cardnumber]]">
			</li>
			<li><label for="cardname">Name on Card *</label>
				 <input type="alpha" id="cardName" name="payment_0_nameOnCard"
				gfv_required="required" class="textfield  mandatory" maxlength='25'
				autocomplete="off" alt="Name On Card" data-validation-engine="validate[required, maxSize[100], custom[alpha]]"/></li>


			<li><label for="expirydate">Expiry Date *</label>
				 <select id="expiryDateMM" name="payment_0_expiryMonth"
				class="smallList" gfv_required="required" alt="Expiry Date"
				type="expirydate" data-validation-engine="validate[required]">
					<option value="">mm</option>
					<option value='01'>01</option>
					<option value='02'>02</option>
					<option value='03'>03</option>
					<option value='04'>04</option>
					<option value='05'>05</option>
					<option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
			</select> <select id="expiryDateYY" name="payment_0_expiryYear"
				class="smallList" gfv_required="required" alt="Expiry Year"
				type="expirydate" data-validation-engine="validate[required],custom[expirydate]">
					<option value="">yy</option>
					<c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
						<option value='<c:out value='${expiryYear}'/>'>
							<c:out value='${expiryYear}' />
						</option>
					</c:forEach>
			</select></li>

			<li><label for="securityno">Last 3 Digit Security Number
					on the reverse of your card.* </label>
				 <input type="numeric" id="securityCode"
				name="payment_0_securityCode" class="textfield  xsmall mandatory"
				gfv_required="required" maxLength="4" autocomplete="off"
				alt="Security Code" data-validation-engine="validate[required, custom[securitycode]]"/>
			</li>


			<li>
				<ul class="formFormat cardDetailsForm" style="margin-left: -40px; margin-top: -30px;"">
					<li class="hide" id="issueNumber"><br /> <label
						for="issueno">Issue Number</label> <input type="numeric"
						id="issueNumber" name="payment_0_issueNumber"
						class="textfield  xsmall" maxlength="2" autocomplete="off"
						alt="Issue Number" data-validation-engine="validate[custom[onlyNumberSp]]"/></li>
				</ul></li>


			<li class="underlined" style="width: 700px;">

				<div id="3DSLogo">
					<c:forEach var="threeDLogos"
						items="${bookingInfo.threeDEnabledLogos}">
						<c:choose>
							<c:when test="${threeDLogos == 'mastercardgroup'}">
								<c:set var="isMastercardLogoPresent" value="true" />
								<div style="float:left;">
									<a href="javascript:void(0);" class="clickTipMaster" id="masterCard"><img
										src="/cms-cps/fcx/images/logo-mastercard.gif" class="removeBorder">
									</a><br /> <a class="learnMore clickTipMaster">Learn More</a>
								</div>
							</c:when>
						</c:choose>
					</c:forEach>
					<c:forEach var="threeDLogos"
						items="${bookingInfo.threeDEnabledLogos}">
						<c:choose>
							<c:when test="${threeDLogos == 'visagroup'}">
								<c:set var="isVisaLogoPresent" value="true" />
								<div <c:if test="${isMastercardLogoPresent}"></c:if> style="float:left;">
									<a href="javascript:void(0);" class="clickTipVisa" id="visaCard"><img
										src="/cms-cps/fcx/images/logo-visa.gif" class="removeBorder">
									</a><br /> <a class="learnMore clickTipVisa" >Learn More</a>
								</div>
							</c:when>
						</c:choose>
					</c:forEach>

					<c:forEach var="threeDLogos"
						items="${bookingInfo.threeDEnabledLogos}">
						<c:choose>
							<c:when test="${threeDLogos == 'americanexpressgroup'}">
								<div <c:if test="${isVisaLogoPresent}"></c:if> style="float:left;">
									<a href="javascript:void(0);" class="clickTipAmEx" id="safeKey"><img
										src="/cms-cps/fcx/images/logo-amex.gif" class="removeBorder">
									</a><br /> <a class="learnMore clickTipAmEx">Learn More</a>
								</div>
							</c:when>
						</c:choose>
					</c:forEach>

					<br />
				</div></li>



			<!-- Card Holder Details JSP -->
			<%@include file="cardHolderDetails.jsp"%>