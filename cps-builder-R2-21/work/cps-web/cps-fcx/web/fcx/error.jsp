<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
 
<title>Payments - Error!!</title>
<link type="image/x-icon" href="/cms-cps/fcx/images/favicon.ico" rel="icon"/>
<link type="image/x-icon" href="/cms-cps/fcx/images/favicon.ico" rel="shortcut icon"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
</head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<body>
	<c:choose>
		<c:when test="${not empty bookingInfo.trackingData.sessionId}">
			<%@include file="errorTechDifficulties.jspf"%>
		</c:when>
		<c:otherwise>
			<%@include file="errorSessionTimeOut.jspf"%>
		</c:otherwise>
	</c:choose>
</body>
</html>