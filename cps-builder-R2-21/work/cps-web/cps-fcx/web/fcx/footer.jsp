 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%
 String footerlink = com.tui.uk.config.ConfReader.getConfEntry("fcx.footerlink" , "");
 pageContext.setAttribute("footerlink", footerlink, PageContext.REQUEST_SCOPE);
%> 
<c:if test="${footerlink== 'true'}">
<footer id="outer-footer" >
	<ul id="footer-links-bar">
	<%
    String home = com.tui.uk.config.ConfReader.getConfEntry("fcx.home" , "");
    pageContext.setAttribute("home", home, PageContext.REQUEST_SCOPE);
	String destinations = com.tui.uk.config.ConfReader.getConfEntry("fcx.destinations" , "");
    pageContext.setAttribute("destinations", destinations, PageContext.REQUEST_SCOPE);
	String contactus = com.tui.uk.config.ConfReader.getConfEntry("fcx.contactus" , "");
    pageContext.setAttribute("contactus", contactus, PageContext.REQUEST_SCOPE);
	String faq = com.tui.uk.config.ConfReader.getConfEntry("fcx.faq" , "");
    pageContext.setAttribute("faq", faq, PageContext.REQUEST_SCOPE);
	
    %>

		<li><a target="_blank" href="${home}">Home</a></li>
		<li><a target="_blank" href="${destinations}">Destinations</a></li>
		<li><a target="_blank" href="${contactus}">Contact us</a></li>
		<li><a target="_blank" href="${faq}">FAQ's</a></li>
	</ul>
</footer>
</c:if>

<footer id="outside-footer">
        <ul></br>
	<li style="border:0px;">&copy; 2016 <a data-componentid="WF_COM_200-4" class="ensLinkTrack" href="http://www.tuitravelplc.com/" target="_blank">TUI Travel plc</a></li>
	<li><a data-componentid="WF_COM_200-4" class="ensLinkTrack" href="http://www.firstchoice.co.uk/our-policies/terms-of-use/" target="_blank">Terms &amp; Conditions</a></li>
	<li><a data-componentid="WF_COM_200-4" class="ensLinkTrack" href="http://www.firstchoice.co.uk/our-policies/accessibility/" target="_blank">Accessibility</a></li>
	<li><a data-componentid="WF_COM_200-4" class="ensLinkTrack" href="http://www.firstchoice.co.uk/our-policies/privacy-policy/" target="_blank">Privacy Policy</a></li>
	<li><a data-componentid="WF_COM_200-4" class="ensLinkTrack" href="http://www.firstchoice.co.uk/our-policies/statement-on-cookies/" target="_blank">Statement on Cookies</a></li>
	<li>
		<c:choose>
			<c:when test="${applyCreditCardSurcharge eq 'true'}">
	<a data-componentid="WF_COM_200-4" class="ensLinkTrack" href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges/" target="_blank">Credit Card Fees</br></br></a>
	</c:when>
	<c:otherwise>
	<a data-componentid="WF_COM_200-4" class="ensLinkTrack" href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges/" target="_blank">Ways to Pay</br></br>
	</c:otherwise>
	</c:choose>
	</li>
</ul>
 
</footer>
