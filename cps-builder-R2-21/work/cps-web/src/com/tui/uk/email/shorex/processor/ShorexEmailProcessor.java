package com.tui.uk.email.shorex.processor;

import static com.tui.uk.email.constants.EmailConstants.AMT_REFUND;
import static com.tui.uk.email.constants.EmailConstants.BREAK;
import static com.tui.uk.email.constants.EmailConstants.CONTENT;
import static com.tui.uk.email.constants.EmailConstants.CRUISE_HOLIDAY_BOOKING_REF_NO;
import static com.tui.uk.email.constants.EmailConstants.DEFAULT_HOST;
import static com.tui.uk.email.constants.EmailConstants.DEFAULT_SUBJECT;
import static com.tui.uk.email.constants.EmailConstants.FROM_ADDRESS;
import static com.tui.uk.email.constants.EmailConstants.HOLIDAY_START_DATE;
import static com.tui.uk.email.constants.EmailConstants.HOST;
import static com.tui.uk.email.constants.EmailConstants.ITINERARY;
import static com.tui.uk.email.constants.EmailConstants.LEAD_PASSENGER_DETAILS;
import static com.tui.uk.email.constants.EmailConstants.REASON;
import static com.tui.uk.email.constants.EmailConstants.RECIPIENTS;
import static com.tui.uk.email.constants.EmailConstants.REFUND_CHEQUE_BUSINESSINFO;
import static com.tui.uk.email.constants.EmailConstants.REFUND_CHEQUE_SUBJECT;
import static com.tui.uk.email.constants.EmailConstants.SAILING_DATE;
import static com.tui.uk.email.constants.EmailConstants.SHIP;
import static com.tui.uk.email.constants.EmailConstants.SHOREX_BOOKING_REF_NO;
import static com.tui.uk.email.constants.EmailConstants.SMTP_CONNECTION_STRING;
import static com.tui.uk.email.constants.EmailConstants.SPACE;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.config.ConfReader;
import com.tui.uk.email.exception.EmailProcessorException;
import com.tui.uk.email.service.impl.EmailServiceImpl;
import com.tui.uk.log.LogWriter;

/**
 * This performs all the operations required for Email processing for B2B Application.
 * 
 * @author Suman.p
 * 
 */
public class ShorexEmailProcessor extends EmailServiceImpl
{

   /**
    * Responsible for creation of message content for email.
    * 
    * @param message Message
    * @throws EmailProcessorException EmailProcessorException
    */
   public void sendEmail(Message message) throws EmailProcessorException
   {
      super.sendEmail(message);
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    * 
    * @param transactionAmount Money
    * @param bookingComponent BookingComponent
    * @throws EmailProcessorException EmailProcessorException
    */
   public void populateEmailContent(Money transactionAmount, BookingComponent bookingComponent)
      throws EmailProcessorException
   {
      Properties props = new Properties();
      props.put(ConfReader.getConfEntry(SMTP_CONNECTION_STRING, DEFAULT_HOST),
         ConfReader.getConfEntry(HOST, ""));
      Session session = Session.getInstance(props);
      String clientApp = bookingComponent.getClientApplication().getClientApplicationName();
      try
      {
         // Instantiate the message
         Message message = new MimeMessage(session);
         message.setFrom(new InternetAddress(ConfReader.getConfEntry(
            clientApp + "." + FROM_ADDRESS, "")));
         String[] recipients = ConfReader.getStringValues(clientApp + "." + RECIPIENTS, null, ",");
         int noOfRecipents = recipients.length;
         InternetAddress[] toAddress = new InternetAddress[noOfRecipents];
         for (int i = 0; i < noOfRecipents; i++)
         {
            toAddress[i] = new InternetAddress(recipients[i]);
         }
         message.setRecipients(Message.RecipientType.TO, toAddress);
         message.setSubject(ConfReader.getConfEntry(clientApp + "." + REFUND_CHEQUE_SUBJECT,
            DEFAULT_SUBJECT)
            + " "
            + (StringUtils
               .isNotBlank(bookingComponent.getNonPaymentData().get("booking_reference"))
               ? bookingComponent.getNonPaymentData().get("booking_reference") : ""));
         message.setSentDate(new Date());
         message.setContent(getContent(transactionAmount, bookingComponent), CONTENT);
         sendEmail(message);
      }
      catch (MessagingException mex)
      {
         String msg = mex.getMessage();
         LogWriter.logErrorMessage(msg, mex);
         throw new EmailProcessorException(msg, mex);
      }

   }

   /**
    * This method will form the content of the email.
    * 
    * @param transactionAmount the transactionAmount.
    * @param bookingComponent the bookingComponent.
    * 
    * @return String the content of email.
    * 
    */

   private String getContent(Money transactionAmount, BookingComponent bookingComponent)
   {
      StringBuilder content = new StringBuilder();
      // Map<String, String> nonPaymentData = bookingComponent.getNonPaymentData();
      String bookingReferenceId =
         (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("booking_reference"))
            ? bookingComponent.getNonPaymentData().get("booking_reference") : "");

      String cruiseReference =
         (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("shipping_reference"))
            ? bookingComponent.getNonPaymentData().get("shipping_reference") : "");

      String sailingDate =
         (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("sailing_date"))
            ? bookingComponent.getNonPaymentData().get("sailing_date") : "");

      String itenary =
         (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("itinerary"))
            ? bookingComponent.getNonPaymentData().get("itinerary") : "");

      String totalRefundAmt =
         (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("total_refund_amount"))
            ? bookingComponent.getNonPaymentData().get("total_refund_amount") : "0");

      String reason =
         (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("reason"))
            ? bookingComponent.getNonPaymentData().get("reason") : "");

      String shipDesc =
         (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("ship_desc"))
            ? bookingComponent.getNonPaymentData().get("ship_desc") : "");

      BigDecimal refundAmount =
         new BigDecimal(Math.abs(Double.valueOf(totalRefundAmt.trim()).doubleValue())).setScale(2,
            BigDecimal.ROUND_HALF_UP);

      content.append("<html><body>");
      content.append(SHOREX_BOOKING_REF_NO);
      content.append(bookingReferenceId);
      content.append(BREAK);
      content.append(CRUISE_HOLIDAY_BOOKING_REF_NO);
      content.append(cruiseReference);
      content.append(BREAK);
      content.append(LEAD_PASSENGER_DETAILS);
      content.append(BREAK);
      content.append(getLeadName(bookingComponent));
      content.append(BREAK);
      content.append(getLeadAddress(bookingComponent));
      content.append("</p>");
      content.append(HOLIDAY_START_DATE);
      content.append(BREAK);
      content.append(sailingDate);
      content.append(BREAK);
      content.append(SAILING_DATE);
      content.append(sailingDate);
      content.append(BREAK);
      content.append(ITINERARY);
      content.append(itenary);
      content.append(BREAK);
      content.append(SHIP);
      content.append(shipDesc);
      content.append(BREAK);
      content.append(AMT_REFUND);
      content.append(getCurrencySymbol(transactionAmount));
      content.append(refundAmount);
      content.append(BREAK);
      content.append(REASON);
      content.append(reason).append("</p>");

      String refundChequebusinessInfo = ConfReader.getConfEntry(REFUND_CHEQUE_BUSINESSINFO, "");
      if (StringUtils.isNotBlank(refundChequebusinessInfo))
      {
         content.append(refundChequebusinessInfo);
      }
      content.append("</body></html>");
      return content.toString();
   }

   /**
    * This method will return the customer address.
    * 
    * @param bookingComponent BookingComponent
    * 
    * @return String the customer address details.
    * 
    */
   private String getLeadAddress(BookingComponent bookingComponent)
   {
      StringBuilder address = new StringBuilder();
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address1")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address1")).append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address2")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address2")).append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address2_1")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address2_1"))
            .append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address2_2")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address2_2"))
            .append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address3")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address3")).append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("street_address4")))
      {
         address.append(bookingComponent.getNonPaymentData().get("street_address4")).append(BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("cardBillingPostcode")))
      {
         address.append(bookingComponent.getNonPaymentData().get("cardBillingPostcode")).append(
            BREAK);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("selectedCountry")))
      {
         address.append(bookingComponent.getNonPaymentData().get("selectedCountry")).append(BREAK);
      }
      return address.toString();
   }

   /**
    * This method will return the lead passenger name.
    * 
    * @param bookingComponent BookingComponent
    * 
    * @return String the lead passenger name details.
    * 
    */
   private String getLeadName(BookingComponent bookingComponent)
   {
      StringBuilder leadName = new StringBuilder();
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("lead_title")))
      {
         leadName.append(bookingComponent.getNonPaymentData().get("lead_title"));
         leadName.append(SPACE);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("lead_firstname")))
      {
         leadName.append(bookingComponent.getNonPaymentData().get("lead_firstname"));
         leadName.append(SPACE);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("lead_middle")))
      {
         leadName.append(bookingComponent.getNonPaymentData().get("lead_middle"));
         leadName.append(SPACE);
      }
      if (StringUtils.isNotBlank(bookingComponent.getNonPaymentData().get("lead_surname")))
      {
         leadName.append(bookingComponent.getNonPaymentData().get("lead_surname"));
      }
      return leadName.toString();
   }

   /**
    * This method will take the amount of the type Money and returns the corresponding currency
    * symbol.
    * 
    * @param amount the amount.
    * 
    * @return String the currency symbol.
    * 
    */
   private String getCurrencySymbol(Money amount)
   {
      String currency = "";
      if (amount.getCurrency().getCurrencyCode().equalsIgnoreCase("GBP"))
      {
         currency = "&pound;";
      }
      else if (amount.getCurrency().getCurrencyCode().equalsIgnoreCase("EUR"))
      {
         currency = "&euro;";
      }
      else if (amount.getCurrency().getCurrencyCode().equalsIgnoreCase("USD"))
      {
         currency = "&#36;";
      }
      return currency;
   }

}
