package com.tui.uk.payment.processor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents some of the known field names used in passenger data panel in cps payment page. This
 * enum is used for validating some of the non payment data in passenger data panel. Enum
 * constructor contain two parameters key and pattern. Key represent field name, which is used in
 * passenger details section and card holder address section in payment page. Pattern represent
 * pattern, which is used for validating field value.
 *
 * @author atul.rao
 */
public enum ThomsonCruisePatternMatchDataPanel implements Serializable
{

   /** The Passenger Details County. */
   county("county", "[a-zA-Z\\., \\-\\s]+"),

   /** The Card Holder Address County. */
   payment_0_street_address4("payment_0_street_address4", "[a-zA-Z\\., \\-\\s]+");

   // CHECKSTYLE:ON

   /** The key is field name which is used in cape visa. */
   private String key;

   /** The pattern, used for validate field value. */
   private String pattern;

   /**
    * Constructor with key.
    *
    * @param key the field name, which is used in cape visa.
    * @param pattern the pattern, which is used for validating field value.
    */
   private ThomsonCruisePatternMatchDataPanel(String key, String pattern)
   {
      this.key = key;
      this.pattern = pattern;
   }

   /**
    * It will give key.
    *
    * @return key
    */
   public String getKey()
   {
      return key;
   }

   /**
    * This method is responsible for populating map with the details of the specified fields in the
    * validation list.
    * 
    * @return the patternMap, the patternMap with all cape visa data panel values.
    */
   public static Map<String, String> getAllPatterns()
   {
      Map<String, String> patternMap = new HashMap<String, String>();
      for (ThomsonCruisePatternMatchDataPanel patternMatchDataPanel : values())
      {
         patternMap.put(patternMatchDataPanel.key, patternMatchDataPanel.pattern);
      }
      return patternMap;
   }

}