/*
 * Copyright (C)2009 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 * 
 * $RCSfile: $
 * 
 * $Revision: $
 * 
 * $Date: $
 * 
 * $Author: $
 * 
 * $Log: $
 */

package com.tui.uk.payment.processor;

import java.util.Map;

import com.tui.uk.config.ConfReader;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for FCX application.
 * 
 * @author Saleem Shaik
 * 
 */

public class FCXPostPaymentProcessor extends BasePostPaymentProcessor
{
	// CHECKSTYLE:OFF
   /**
    * Constructor with Payment Data.
    * 
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public FCXPostPaymentProcessor(PaymentData paymentData, 
      Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    * 
    * @throws PostPaymentProcessorException if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
   {
		if ((requestParameterMap.get("payment_0_cardNumber") != null)
				&& (requestParameterMap.get("payment_0_type") != null)) {

			String thCCBinRange = "ThomsonCreditcard.BINRange";
			String thCCNumberSelected = null;
			String thCCConfiguration = ConfReader.getConfEntry(thCCBinRange, null);
			String cardNumber = requestParameterMap.get("payment_0_cardNumber");
			String[] thCCNumberList = thCCConfiguration.split(",");
			for (String thCCNumber : thCCNumberList) {
				if (cardNumber.startsWith(thCCNumber)) {
					if (requestParameterMap.get("payment_0_type").contains("TUI_MASTERCARD")) {
						thCCNumberSelected = thCCNumber;
						break;
					} else {
						throw new PostPaymentProcessorException(
								"datacash.cardtype.mismatch");
					}

				}
			}
			if (thCCNumberSelected != null) {
				if (!(requestParameterMap
						.get("payment_0_type")
						.contains("TUI_MASTERCARD"))) {
					throw new PostPaymentProcessorException(
							"datacash.cardtype.mismatch");
				}
			}
			else
			{
				if ((requestParameterMap
						.get("payment_0_type")
						.contains("TUI_MASTERCARD"))) {
					throw new PostPaymentProcessorException(
							"datacash.cardtype.mismatch");
				}
			}
		}
      super.preProcess();
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    * 
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      verifyAndUpdateCardCharge();
      super.process();
   }
}