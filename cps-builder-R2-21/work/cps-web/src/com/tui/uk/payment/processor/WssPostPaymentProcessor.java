/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: WssPostPaymentProcessor.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-09-22 08:25:26 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: $
*/
package com.tui.uk.payment.processor;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.math.BigDecimal;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentFlow;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.email.exception.EmailProcessorException;
import com.tui.uk.email.service.EmailService;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.ChequePaymentTransaction;
import com.tui.uk.payment.domain.Payment;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.domain.PaymentTransactionFactory;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This class is responsible for processing the data entered in WSS payment page.
 *
 * @author sindhushree.g
 *
 */
public final class WssPostPaymentProcessor extends BasePostPaymentProcessor
{

   /** The pattern for the part payment amount. */
   private static final Pattern AMOUNT_PATTERN = Pattern.compile("^[-]?[\\d]*[.]?[\\d]+$");

   /** The minimum amount. */
   private static final double MIN_AMOUNT = 50.00;

   /** The constant BOOKING. */
   private static final String BOOKING = "booking.";

   /** The constant INVALID. */
   private static final String INVALID = ".invalid";

   /** The constant REFUNDAMT. */
   private static final String REFUNDAMT = "refundAmount";

   /** The WSS_PHASE1 is used as key to access conf entry.  */
   private static final String WSS_PHASE1 = "WSS.WSS_PHASE1";

   /** The constant BOOKING_AMTTOPAY_PARTPAYMENT.  */
   private static final String BOOKING_AMTTOPAY_PARTPAYMENT = "booking.amountTopay.partpayment";

   /** The constant PIPELINE.  */
   private static final String PIPELINE = "|";

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public WssPostPaymentProcessor(PaymentData paymentData,
          Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the
    * payment page. This may include, updating non payment data map,
    * validation.
    *
    * @throws PostPaymentProcessorException
    *             if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
   {
      String conf = "TUIGiftCard.BINRange";
      String giftCardNumber = ConfReader.getConfEntry(conf, null);
      String cardNumber=requestParameterMap.get("payment_0_cardNumber");
       if (cardNumber != null) {
           if (cardNumber.startsWith(giftCardNumber)) 
            {
            if (!(requestParameterMap.get("payment_0_type").toUpperCase()
                  .contains("MAESTRO")))
               {
                 throw new PostPaymentProcessorException(
                  "datacash.cardtype.mismatch");
                }
              }
         if (requestParameterMap.get("payment_0_type").toUpperCase()
                      .contains("MAESTRO"))
           {
             if (!(cardNumber.startsWith(giftCardNumber)))
              {
              throw new PostPaymentProcessorException(
             "datacash.cardtype.mismatch");
               }
           }
  }
  super.preProcess();
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   @Override
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      //TODO: The below IF construct should be removed once WSS PHASE2 goes live.
      if (!ConfReader.getBooleanEntry(WSS_PHASE1, false))
      {
         PaymentFlow paymentFlow = bookingComponent.getBookingData().getBookingFlowDetails()
               .getPaymentFlow();

         if (!paymentFlow.equals(PaymentFlow.REFUND))
         {
      //Get the selected card type
      String paymentTypeCode = setSelectedPaymentDetails();

      //Transaction amount without card charge.
      Money transactionAmount = getTransactionAmountForPhase2();

      bookingInfo.setCalculatedPayableAmount(transactionAmount);
      bookingInfo.getUpdatedCardCharge(paymentTypeCode, transactionAmount);

      //Sets transaction amount and card charge into request parameter map
      setTransactonDetails(transactionAmount);
      validateTotalTransactionAmount();
            super.process();
         }
         else
         {
            processRefundTransactions();
         }
      }
      else
      {
         //Get the selected card type
         String paymentTypeCode = setSelectedPaymentDetails();

         //Transaction amount without card charge.
         Money transactionAmount = getTransactionAmount();

         bookingInfo.setCalculatedPayableAmount(transactionAmount);
         bookingInfo.getUpdatedCardCharge(paymentTypeCode, transactionAmount);

         //Sets transaction amount and card charge into request parameter map
         setTransactonDetails(transactionAmount);
         validateTotalTransactionAmount();
         super.process();

      }
      //TODO: The below IF construct should be removed once WSS PHASE2 goes live.
   }

   /**
    * This method is responsible for getting the transaction amount entered in the UI.
    *
    * @return transactionAmount.
    *
    * @throws PaymentValidationException if validation of the transaction amount fails.
    */
   private Money getTransactionAmount() throws PaymentValidationException
   {
      Money transactionAmount = bookingComponent.getPayableAmount();
      if (requestParameterMap.get(BookingConstants.DEPOSIT_TYPE) != null)
      {
         String depositType = requestParameterMap.get(BookingConstants.DEPOSIT_TYPE);
         String amountToPay = requestParameterMap.get(BookingConstants.TRANSACTION_AMOUNT);
         if (depositType.equalsIgnoreCase(BookingConstants.PARTIAL_DEPOSIT))
         {
            validatePartPaymentAmount(amountToPay);
            Double partPaymentAmount = Double.valueOf(amountToPay);
            transactionAmount =
               new Money(BigDecimal.valueOf(partPaymentAmount), bookingComponent.getTotalAmount()
                  .getCurrency());
         }
         else
         {
            if (bookingComponent.getDepositComponents() != null)
            {
               for (DepositComponent depositComponent : bookingComponent.getDepositComponents())
               {
                  if (depositType.equalsIgnoreCase(depositComponent.getDepositType()))
                  {
                     transactionAmount = depositComponent.getDepositAmount();
                    //Compare and validate server side amount with amount to pay read from JSP
                     validateAmountToPay(amountToPay, transactionAmount.getAmount().doubleValue(),
                      depositType);
                  }
               }
            }
         }
      }
      return transactionAmount;
   }

   /**
    * This method validates the amount entered by the user in case of part payment.
    *
    * @param partAmount the part payment amount entered by the user.
    *
    * @throws PaymentValidationException if validation fails.
    */
   private void validatePartPaymentAmount(String partAmount) throws PaymentValidationException
   {
      patternCheckForAmount(partAmount, "booking.partialPaymentAmount.invalid" + PIPELINE
         + bookingComponent.getTotalAmount().getSymbol());
      double amountToPay = Double.parseDouble(partAmount);
      if (amountToPay < MIN_AMOUNT
         || bookingInfo.getCalculatedTotalAmount().getAmount().doubleValue()
             - amountToPay < MIN_AMOUNT)
      {
         populateException("booking.partialPaymentAmount.invalid" + PIPELINE
            + bookingComponent.getTotalAmount().getSymbol());
      }
   }

   /**
    * This method validates the amount to pay field. If the amount to pay field is not blank and the
    * value does not match with the transaction amount of the server side, then an exception will be
    * thrown. In case of non js, the amount to pay field can be blank.
    *
    * @param amountToPay the amount entered by the user.
    * @param transactionAmount the server side calculated transaction amount.
    * @param depositType the depositType selected by user.
    *
    * @throws PaymentValidationException if the client side entered amount, does not match with the
    *            server side calculated amount.
    */
   private void validateAmountToPay(String amountToPay, double transactionAmount,
       String depositType) throws PaymentValidationException
   {
      if (StringUtils.isNotBlank(amountToPay))
      {
         patternCheckForAmount(amountToPay, BOOKING + depositType + INVALID);
         if (Double.parseDouble(amountToPay) != transactionAmount)
         {
            populateException(BOOKING + depositType + INVALID);
         }
      }
   }

   /**
    * Method to check whether amount complies with allowed pattern. Exception will be thrown if
    * amount is not within the allowed pattern.
    *
    * @param amountToPay the amount associated with amountToPay field.
    * @param messageKey to differentiate show particular error message for the amount.
    *
    * @throws PaymentValidationException if the client side entered amount, does not match with the
    *            server side calculated amount.
    */
   private void patternCheckForAmount(String amountToPay, String messageKey)
      throws PaymentValidationException
   {
      Matcher amountToPayMatcher = AMOUNT_PATTERN.matcher(String.valueOf(amountToPay));

      if (!amountToPayMatcher.matches())
      {
        populateException(messageKey);
      }
   }

   /**
    * This method gets the message for a particular key and logs the error message and throws the
    * PaymentValidationException.
    *
    * @param messageKey the message key.
    *
    * @throws PaymentValidationException with appropriate message.
    */
   private void populateException(String messageKey) throws PaymentValidationException
   {
      String message = PropertyResource.getProperty(messageKey, MESSAGES_PROPERTY);
      LogWriter.logErrorMessage(message);
      String fieldName = "AmountToPay";
      if (bookingComponent.getBookingData().getBookingFlowDetails()
              .getPaymentFlow().equals(PaymentFlow.REFUND))
      {
         fieldName = "Refund Amount";
      }
      throw new PaymentValidationException(messageKey, fieldName);
   }

   /**
    * This method is responsible for validating the refund transaction amount and creating transactions.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   private void processRefundTransactions()
           throws PaymentValidationException, PostPaymentProcessorException
   {
      int noOfRefunds =
         Integer.parseInt(requestParameterMap.get(
              PaymentConstants.PAYMENT + PaymentConstants.NO_OF_REFUNDS));
      setSelectedRefundDetails(noOfRefunds);
      Money transactionAmount = getRefundTransactionAmount(noOfRefunds);
      validateAmount(transactionAmount);
      //bookingInfo.setCalculatedPayableAmount(transactionAmount);
      String totalRefundAmount = requestParameterMap.get(PaymentConstants.PAYMENT
         + PaymentConstants.TOTAL_REFUND);
      if (StringUtils.isNotBlank(totalRefundAmount))
      {
         validateRefundAmount(totalRefundAmount);
      }
      createAndProcessPayments(noOfRefunds);
      for (PaymentTransaction paymentTransaction : paymentData.getPayment()
         .getPaymentTransactions())
      {
         if (paymentTransaction instanceof ChequePaymentTransaction)
         {
            try
            {
               notifyRefundTransaction(paymentTransaction.getTransactionAmount());
            }
            catch (EmailProcessorException e)
            {
                LogWriter.logErrorMessage(e.getMessage());
                e.printStackTrace();
            }
         }
      }
   }

   /**
    * This method will return the transaction amount for refunds.
    *
    * @param totalTransactions the number of refund transactions.
    *
    * @throws PaymentValidationException the if the refund amount will not satisfy the pattern check.
    *
    * @return Money the transaction amount.
    */
   private Money getRefundTransactionAmount(Integer totalTransactions)
           throws PaymentValidationException
   {
      BigDecimal totalTransactionAmount = BigDecimal.ZERO;
      for (int i = 0; i < totalTransactions; i++)
      {
         String amount = requestParameterMap
         .get(PaymentConstants.PAYMENT + i + PaymentConstants.TRANSACTION_AMOUNT_TEXT);
         if (StringUtils.isNotBlank(amount))
         {
         patternCheckForAmount(amount, BOOKING + REFUNDAMT + INVALID);
            totalTransactionAmount = totalTransactionAmount.add(new BigDecimal(amount));
         }
      }
      return new Money(totalTransactionAmount.setScale(2, BigDecimal.ROUND_HALF_UP),
         bookingComponent.getTotalAmount().getCurrency());
   }

   /**
    * This method is used to validate the Amount depending on:if it is Refund the
    * transaction amount should be equal to the calculated payable amount. It checks for the above
    * condition and if the condition is not satisfied then a PostPaymentProcessorException is thrown.
    *
    * @param amount the amount paid.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   private void validateAmount(Money amount) throws PostPaymentProcessorException
   {
      if (Math.abs(amount.getAmount().doubleValue())
               != Math.abs(bookingInfo.getCalculatedPayableAmount().getAmount().doubleValue()))
      {
         populateException();
      }
   }

   /**
    * This method is used to validate if the total refund amount calculated at the
    * client side is equal to the calculated payable amount.It checks for the above
    * condition and if the condition is not satisfied then a PostPaymentProcessorException
    * is thrown.
    *
    * @param amount the amount paid.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   private void validateRefundAmount(String amount) throws PostPaymentProcessorException
   {
      if (Double.parseDouble(amount) != bookingInfo.getCalculatedPayableAmount().getAmount()
         .doubleValue())
      {
         populateException();
      }
   }

   /**
    * This method will notify a refund transaction through a java mail when refund cheque payment
    * type is selected.
    *
    * @param transactionAmount the refund cheque transactionAmount.
    *
    * @throws EmailProcessorException the emailProcessorException.
    */
   public void notifyRefundTransaction(Money transactionAmount) throws EmailProcessorException
   {
      WSSEmailProcessor emailProcessor = new WSSEmailProcessor();
      emailProcessor.populateEmailContent(transactionAmount, bookingComponent);

   }

   /**
    * This method logs and throws <code>PostPaymentProcessorException</code>.
    *
    * @throws PostPaymentProcessorException after logging the exception message.
    */
   private void populateException() throws PostPaymentProcessorException
   {
      LogWriter.logErrorMessage(PropertyResource.getProperty(ERROR_MESSAGE_KEY, MESSAGES_PROPERTY));
      throw new PostPaymentProcessorException(ERROR_MESSAGE_KEY);
   }

   /**
    * Sets refundTypeCode(e.g. MASTERCARD etc.) and paymentMethod into request parameter map.
    * @param noOfRefunds Total number of refunds.
    * @throws PaymentValidationException the if the refund amount will not satisfy the pattern check.
    */
   private void setSelectedRefundDetails(int noOfRefunds) throws PaymentValidationException
   {
      for (int i = 0; i < noOfRefunds; i++)
      {
         //Get the selected refund type
         String selectedRefund = requestParameterMap.get(PaymentConstants.PAYMENT + i
            + PaymentConstants.TYPE);
         //From selected refund type extract payment type code and payment method
         String refundTypeCode =  StringUtils.split(selectedRefund, '|')[0];
         String refundMethod = StringUtils.split(selectedRefund, '|')[1];
         String amount = requestParameterMap.get(
            PaymentConstants.PAYMENT + i + PaymentConstants.TRANSACTION_AMOUNT_TEXT);
         if (StringUtils.isNotBlank(amount))
         {
            patternCheckForAmount(amount, BOOKING + REFUNDAMT + INVALID);
            if (Double.parseDouble(amount) > 0)
            {
               amount = "-" + amount;
            }
         }
         //VI or Cheque
         requestParameterMap.put(PaymentConstants.PAYMENT + i + PaymentConstants.PAYMENT_TYPE_CODE,
            refundTypeCode);
         //Cheque or CardRefund
         requestParameterMap.put(PaymentConstants.PAYMENT + i + PaymentConstants.PAYMENT_METHOD,
            refundMethod);
         //Set transaction amount
         requestParameterMap.put(PaymentConstants.PAYMENT + i + PaymentConstants.TRANSACTION_AMOUNT,
            amount);
      }
   }

   /**
    * This method is responsible for getting the transaction amount entered in the UI.
    *
    * @return transactionAmount.
    *
    * @throws PaymentValidationException if validation of the transaction amount fails.
    */
   private Money getTransactionAmountForPhase2() throws PaymentValidationException
   {
      Money transactionAmount = bookingComponent.getPayableAmount();
      if (requestParameterMap.get(BookingConstants.DEPOSIT_TYPE) != null)
      {
         String depositType = requestParameterMap.get(BookingConstants.DEPOSIT_TYPE);
         String amountToPay = requestParameterMap.get(BookingConstants.TRANSACTION_AMOUNT);
         if (depositType.equalsIgnoreCase(BookingConstants.PARTIAL_DEPOSIT))
         {
            for (DepositComponent depositComp : bookingComponent.getDepositComponents())
            {
               if (depositType.equalsIgnoreCase(depositComp.getDepositType()))
               {
                  Money partialDeposit = new Money(new BigDecimal(Double.valueOf("50")),
                        bookingComponent.getTotalAmount().getCurrency());
                  if (depositComp.getDepositAmount().getAmount().doubleValue() > 0)
                  {
                    partialDeposit = depositComp.getDepositAmount();
                  }
                  validatePartPaymentAmountForPhase2(amountToPay, partialDeposit);
                  Double partPaymentAmount = Double.valueOf(amountToPay);
                  transactionAmount =
                     new Money(BigDecimal.valueOf(partPaymentAmount),
                         bookingComponent.getTotalAmount().getCurrency());
               }
            }
         }
         else
         {
            if (bookingComponent.getDepositComponents() != null)
            {
               for (DepositComponent depositComponent : bookingComponent.getDepositComponents())
               {
                  if (depositType.equalsIgnoreCase(depositComponent.getDepositType()))
                  {
                     transactionAmount = depositComponent.getDepositAmount();
                    //Compare and validate server side amount with amount to pay read from JSP
                     validateAmountToPay(amountToPay, transactionAmount.getAmount().doubleValue(),
                      depositType);
                  }
               }
            }
         }
      }
      return transactionAmount;
   }

   /**
    * This method validates the amount entered by the user in case of part payment.
    *
    * @param partAmount the part payment amount entered by the user.
    * @param payableToday the amount to be paid today.
    *
    * @throws PaymentValidationException if validation fails.
    */
   private void validatePartPaymentAmountForPhase2(String partAmount, Money payableToday)
           throws PaymentValidationException
   {
      double amountPayableToday = payableToday.getAmount().doubleValue();
      patternCheckForAmount(partAmount, BOOKING_AMTTOPAY_PARTPAYMENT + PIPELINE
         + bookingComponent.getTotalAmount().getSymbol() + PIPELINE + amountPayableToday);
      double amountToPay = Double.parseDouble(partAmount);
      if (amountToPay < amountPayableToday
         || bookingInfo.getCalculatedTotalAmount().getAmount().doubleValue()
             - amountToPay < MIN_AMOUNT)
      {
         populateException(BOOKING_AMTTOPAY_PARTPAYMENT + PIPELINE
            + bookingComponent.getTotalAmount().getSymbol() + PIPELINE
              + amountPayableToday);
      }
   }


   /**
    * This method is responsible for creating the payments and adding the
    * transactions to payment object. Gets the amount from payment transaction
    * object and validates it against the bookingInfo object's payable amount.
    *
    * @param contributionCount Number of transactions to be processed.
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   protected void createAndProcessPayments(final int contributionCount)
       throws PaymentValidationException, PostPaymentProcessorException
   {
      final Payment payment = new Payment();
      bookingComponent.getNonPaymentData().put(BookingConstants.IS_DATACASH_SELECTED,
         String.valueOf(false));
      for (int i = 0; i < contributionCount; i++)
      {
         if (StringUtils.isNotBlank(requestParameterMap.get(PaymentConstants.PAYMENT + i
            + PaymentConstants.TRANSACTION_AMOUNT)))
         {
            payment.addPaymentTransaction(PaymentTransactionFactory.getPaymentTransactions(i,
               requestParameterMap, bookingInfo));
         }
      }
      paymentData.setPayment(payment);

      BigDecimal totalAmountPaid = BigDecimal.ZERO;
      for (PaymentTransaction paymentTransaction : paymentData.getPayment()
               .getPaymentTransactions())
      {
         totalAmountPaid = totalAmountPaid.add(paymentTransaction.getTransactionAmount()
            .getAmount());
      }
      if (totalAmountPaid.doubleValue() != bookingInfo.getCalculatedPayableAmount()
               .getAmount().doubleValue())
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(
            ERROR_MESSAGE_KEY, MESSAGES_PROPERTY));
         throw new PostPaymentProcessorException(ERROR_MESSAGE_KEY);
      }
   }
}
