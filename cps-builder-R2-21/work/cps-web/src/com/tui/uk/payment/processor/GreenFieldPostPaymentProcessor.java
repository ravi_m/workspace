/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: GreenFieldPostPaymentProcessor.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: vinayak $
 *
 * $Log: $
 */
package com.tui.uk.payment.processor;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for Greenfield application.
 *
 * @author Vinayak
 *
 */
public class GreenFieldPostPaymentProcessor extends BasePostPaymentProcessor
{

   /** The deposit type. */
   private static final String DEPOSIT_TYPE = "depositType";

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public GreenFieldPostPaymentProcessor(PaymentData paymentData,
                                         Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
   {
      if (bookingInfo.getNewHoliday())
      {
         super.preProcess();
      }
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {

      if (bookingInfo.getNewHoliday())
      {
         verifyAndUpdateCardCharge();
         super.process();
      }
   }

   /**
    * This method is responsible for card holder verification in 3D secure transactions.
    *
    * @param termUrl the termUrl.
    * @param userAgent the userAgent.
    *
    * @return the URL, the URL to which redirection should take place.
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public String postProcess(String termUrl, String userAgent)
      throws PaymentValidationException, PostPaymentProcessorException
   {
      if (bookingInfo.getNewHoliday())
      {
         bookingInfo.setThreeDAuth(Boolean.FALSE);
         return super.postProcess(termUrl, userAgent);
      }
      else
      {
        return null;
      }
   }

   /**
    * Gets the updated card charge for non selected amount.
    *
    * @param selectedCard the selected card.
    * @param amtWithoutCardCharge amount for which card charges should be calculated.
    *
    * @return the calculated card charge.
    */
   protected Money updateCardChargeForNonSelectedDeposit(String selectedCard,
      Money amtWithoutCardCharge)
   {
      BigDecimal updatedCardCharge = BigDecimal.ZERO;
      BigDecimal cardCharge = bookingComponent.getCardChargesMap().get(selectedCard);

      if (cardCharge != null)
      {
         cardCharge = getAllowedCardChargePercent(cardCharge);
         updatedCardCharge =
            cardCharge.multiply(amtWithoutCardCharge.getAmount()).divide(
               BigDecimal.valueOf(DispatcherConstants.PERCENTAGE_FACTOR));
         updatedCardCharge = getAllowedCardCharge(updatedCardCharge);
         updatedCardCharge = updatedCardCharge.setScale(2, BigDecimal.ROUND_HALF_UP);
      }
      return new Money(updatedCardCharge, bookingComponent.getTotalAmount().getCurrency());
   }

   /**
    * Updates the card charge.
    *
    * @return the response string.
    */
   public String updateCardCharges()
   {
      // Transaction amount without card charge.
      Money amtWithoutCardCharge = null;
      Money updatedCardCharge = null;
      Money transactionAmount = null;

      String depositType = null;
      if (requestParameterMap.get(DEPOSIT_TYPE) != null)
      {
         depositType = requestParameterMap.get(DEPOSIT_TYPE).trim();
      }

      String selectedCard = requestParameterMap.get(DispatcherConstants.SELECTED_CARD);

      StringBuilder result = new StringBuilder();

      List<DepositComponent> depositComponents = bookingComponent.getDepositComponents();
      if (depositComponents != null && depositComponents.size() > 0)
      {
         for (DepositComponent depositComponent : depositComponents)
         {
            amtWithoutCardCharge = depositComponent.getDepositAmount();
            Money cardCharge =
               updateCardChargeForNonSelectedDeposit(selectedCard, amtWithoutCardCharge);
            result.append((cardCharge.add(amtWithoutCardCharge)).getAmount());
            result.append("|");
            if (StringUtils.equalsIgnoreCase(depositComponent.getDepositType(), depositType))
            {
               updatedCardCharge = cardCharge;
               transactionAmount = depositComponent.getDepositAmount();
            }
         }
      }
      amtWithoutCardCharge =
         bookingComponent.getTotalAmount().subtract(bookingInfo.getCalculatedDiscount());
      Money cardCharge = updateCardChargeForNonSelectedDeposit(selectedCard, amtWithoutCardCharge);
      result.append(amtWithoutCardCharge.add(cardCharge).getAmount());
      result.append("|");

      if (updatedCardCharge == null)
      {
         updatedCardCharge = cardCharge;
      }
      if (transactionAmount == null)
      {
         transactionAmount =
            bookingComponent.getTotalAmount().subtract(bookingInfo.getCalculatedDiscount());
      }

      bookingInfo.setCalculatedCardCharge(updatedCardCharge);
      bookingInfo.setCalculatedPayableAmount(transactionAmount.add(updatedCardCharge));
      bookingInfo.setCalculatedTotalAmount(bookingComponent.getTotalAmount().subtract(
         bookingInfo.getCalculatedDiscount()).add(updatedCardCharge));
      result.append(bookingInfo.getCalculatedTotalAmount().getAmount());
      result.append("|");
      result.append(updatedCardCharge.getAmount());

      return result.toString();
   }

   /**
    * Returns the allowed card charge percent after comparing with max card charge percent .
    *
    * @param cardChargePercent the card charge percent of selected card
    * @return allowed card charge percent
    */
   private BigDecimal getAllowedCardChargePercent(BigDecimal cardChargePercent)
   {
      if (bookingComponent.getMaxCardChargePercent() != null
         && cardChargePercent.doubleValue() > bookingComponent.getMaxCardChargePercent()
            .doubleValue())
      {
         cardChargePercent = bookingComponent.getMaxCardChargePercent();
      }
      return cardChargePercent;
   }

   /**
    * Returns the allowed card charge amount after comparing with max card charge .
    *
    * @param cardCharge the calculated card charge of selected card
    * @return allowed card charge
    */
   private BigDecimal getAllowedCardCharge(BigDecimal cardCharge)
   {
      if (bookingComponent.getMaxCardCharge() != null
         && cardCharge.doubleValue() > bookingComponent.getMaxCardCharge().doubleValue())
      {
         cardCharge = bookingComponent.getMaxCardCharge();
      }
      return cardCharge;
   }

}
