package com.tui.uk.payment.processor;

import java.util.Map;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
/**
 * This performs all the operations required for post payment for Shorex B2C application.
 * 
 * @author rajarao.r
 * 
 */
public class B2CShorexPostPaymentProcessor extends DotNetClientPostPaymentProcessor
{
// CHECKSTYLE:OFF
   /**
    * Constructor with Payment Data.
    * 
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public B2CShorexPostPaymentProcessor(PaymentData paymentData,
                                        Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);

   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    * 
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   @Override
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      // verifyAndUpdateCardCharge();
      super.process();
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    * 
    * @throws PostPaymentProcessorException if validation fails.
    */
   @Override
   public void preProcess() throws PostPaymentProcessorException
   {
		if ((requestParameterMap.get("payment_0_cardNumber") != null)
				&& (requestParameterMap.get("payment_0_paymenttypecode") != null)) {

			String thCCBinRange = "ThomsonCreditcard.BINRange";
			String thCCNumberSelected = null;
			String thCCConfiguration = ConfReader.getConfEntry(thCCBinRange, null);
			String cardNumber = requestParameterMap.get("payment_0_cardNumber");
			String[] thCCNumberList = thCCConfiguration.split(",");
			for (String thCCNumber : thCCNumberList) {
				if (cardNumber.startsWith(thCCNumber)) {
					if (requestParameterMap.get("payment_0_paymenttypecode").contains("TUI_MASTERCARD")) {
						thCCNumberSelected = thCCNumber;
						break;
					} else {
						throw new PostPaymentProcessorException(
								"datacash.cardtype.mismatch");
					}

				}
			}
			if (thCCNumberSelected != null) {
				if (!(requestParameterMap
						.get("payment_0_paymenttypecode")
						.contains("TUI_MASTERCARD"))) {
					throw new PostPaymentProcessorException(
							"datacash.cardtype.mismatch");
				}
			}else
			{
				if ((requestParameterMap
						.get("payment_0_paymenttypecode")
						.contains("TUI_MASTERCARD"))) {
					throw new PostPaymentProcessorException(
							"datacash.cardtype.mismatch");
				}
			}
		}
      super.preProcess();
      BookingComponent bookingComponent = paymentData.getBookingInfo().getBookingComponent();
      bookingComponent.getNonPaymentData().put(BookingConstants.JSESSIONID,
         bookingInfo.getTrackingData().getSessionId());
   }
}
