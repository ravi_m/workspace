/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: KronosPostPaymentProcessor.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: jaleelakbar.m $
 *
 * $Log: $
 */
package com.tui.uk.payment.processor;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for Kronos
 * application.
 *
 * @author jaleelakbar.m
 *
 */
public class KronosPostPaymentProcessor extends BasePostPaymentProcessor
{

   /** The index. */
   private static final int INDEX = 0;

   /** The selected card name. */
   private static final String SELECTED_CARD = "payment_0_type";

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public KronosPostPaymentProcessor(PaymentData paymentData, Map<String, String[]>
      requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      // For refunds
      if (bookingInfo.getCalculatedTotalAmount().getAmount().doubleValue() <= 0)
      {
         if (!(requestParameterMap.get(PaymentConstants.TOTAL_TRANS_AMOUNT))
                  .equalsIgnoreCase(PaymentConstants.NOT_APPLICABLE))
         {
            validateAmount(requestParameterMap.get(PaymentConstants.TOTAL_TRANS_AMOUNT));
         }
         //Get the selected card type
         String selectedCard = requestParameterMap.get(SELECTED_CARD);
         String prefix = PaymentConstants.PAYMENT + INDEX;
         //From selected card type extract payment type code and payment method
         if (StringUtils.isNotBlank(selectedCard))
         {
            //MASTERCARD , AMERICAN_EXPRESS etc
            requestParameterMap.put(prefix + PaymentConstants.PAYMENT_TYPE_CODE,
                                            StringUtils.split(selectedCard, '|')[0]);
            //DCard etc
            requestParameterMap.put(prefix + PaymentConstants.PAYMENT_METHOD,
                                            StringUtils.split(selectedCard, '|')[1]);
         }

         //Set transaction amount determined as above to requestParameterMap
         requestParameterMap.put(prefix + PaymentConstants.TRANSACTION_AMOUNT, bookingInfo
            .getCalculatedPayableAmount().getAmount().toString());
         requestParameterMap.put(prefix + PaymentConstants.CHARGE, bookingInfo
            .getCalculatedCardCharge().getAmount().toString());

      }
      else
      {
         verifyAndUpdateCardCharge();
      }
      super.process();
   }

}
