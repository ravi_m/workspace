package com.tui.uk.payment.processor;

import java.util.Map;

import com.tui.uk.config.ConfReader;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
/**
 * This performs all the operations required for post payment for .NET applications.
 *
 * @author sindhushree.g
 *
 */
public class TflyPostPaymentProcessor extends DotNetClientPostPaymentProcessor
{
   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public TflyPostPaymentProcessor(PaymentData paymentData,
      Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the
    * payment page. This may include, updating non payment data map,
    * validation.
    *
    * @throws PostPaymentProcessorException
    *             if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
   {
      String conf = "TUIGiftCard.BINRange";
      String giftCardNumber = ConfReader.getConfEntry(conf, null);
      String cardNumber=requestParameterMap.get("payment_0_cardNumber");
      if (cardNumber.startsWith(giftCardNumber))
      {
         if(!(requestParameterMap.get("payment_0_type").equalsIgnoreCase("MAESTRO")))
         {
            throw new PostPaymentProcessorException
            ("datacash.cardtype.mismatch");
         }
      }
      if(requestParameterMap.get("payment_0_type").equalsIgnoreCase("MAESTRO"))
      {
         if (!(cardNumber.startsWith(giftCardNumber)))
         {
            throw new PostPaymentProcessorException
            ("datacash.cardtype.mismatch");
         }
      }
      super.preProcess();
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   @Override
   public void process() throws PaymentValidationException,
   PostPaymentProcessorException
   {
      super.process();
   }
}
