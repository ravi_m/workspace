/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: TracsPostPaymentProcessor.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: $
*/
package com.tui.uk.payment.processor;

import java.util.Map;

import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for .NET applications.
 *
 * @author sindhushree.g
 *
 */
public final class TracsPostPaymentProcessor extends BasePostPaymentProcessor
{

   /** The zero'th index. */
   private static final String ZERO_INDEX = "0";

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public TracsPostPaymentProcessor(PaymentData paymentData, Map<String, String[]>
      requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
       requestParameterMap.put(PaymentConstants.PAYMENT + ZERO_INDEX + PaymentConstants
          .PAYMENT_METHOD, PaymentMethod.DATACASH.getCode());
       requestParameterMap.put(PaymentConstants.PAYMENT + ZERO_INDEX + PaymentConstants
          .TRANSACTION_AMOUNT, String.valueOf(bookingInfo.getCalculatedPayableAmount()
             .getAmount().add(bookingInfo.getCalculatedCardCharge().getAmount())));
       requestParameterMap.put(PaymentConstants.PAYMENT + ZERO_INDEX + PaymentConstants.CHARGE,
         updateCardCharges());
       if (!(requestParameterMap.get(PaymentConstants.TOTAL_TRANS_AMOUNT))
                .equalsIgnoreCase(PaymentConstants.NOT_APPLICABLE))
       {
          validateAmount(requestParameterMap.get(PaymentConstants.TOTAL_TRANS_AMOUNT));
       }
      super.process();
   }

   /**
    * Updates the card charge.
    *
    * @return the response string.
    */
   public String updateCardCharges()
   {
      bookingInfo.resetCardCharge();
      bookingInfo.getUpdatedCardCharge(requestParameterMap.get(PaymentConstants.PAYMENT
         + ZERO_INDEX + PaymentConstants.PAYMENT_TYPE_CODE), bookingInfo
         .getCalculatedPayableAmount());
      return bookingInfo.getCalculatedCardCharge().getAmount().toString();
   }
}
