package com.tui.uk.payment.processor;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
* Represents all the possible known field name used in important 
* information data panel in cps payment page.
* This enum is used for validating the non payment data in important 
* information data panel.
* Enum constructor contain two parameters key and pattern. 
* Key represent field name, which is used in important information section in payment page. 
* Pattern represent pattern, which is used for validating field value.
* 
* @author shwetha.rb
*/

public enum HybrisTHCruiseDataPanel implements Serializable
{
   // CHECKSTYLE:OFF
    // CHECKSTYLE:OFF
      /** The streetAddress1. */
      streetAddress1("streetAddress1","([a-zA-Z0-9 ]+[ \\-\\&()\'.\\/\\s*]*[a-zA-Z0-9 \\s*]*)+"),
      
      /** The streetAddress2. */
      streetAddress2("streetAddress2","([a-zA-Z0-9 ]+[ \\-\\&()\'.\\/\\s*]*[a-zA-Z0-9 \\s*]*)+"),
      
      /** The town. */
      town("town", "([a-zA-Z ]+(_[ a-zA-Z]+)*)$"),
      
      /** The postcode. */
      postcode("postcode","^([A-Pa-pR-UWYZr-uwyz0-9][A-Ha-hK-Yk-y0-9][AEHMNPRTUVXYaehmnprtuvxy0-9]?[ABEHMNPRVWXYabehmnprvwxy0-9]?[ \\s]{0,1}[0-9][ABD-HJLN-UW-Zabd-hjln-uw-z]{2}|GIR 0AA)$"),
     
      /** The surName. */
      surName("surName", "^[-a-zA-Z&@ \"\',\\.\\x27]*$"),
      
      /** The firstName. */
      firstName("firstName", "^[-a-zA-Z&@ \"\',\\.\\x27]*$");
   
   // CHECKSTYLE:ON

   /** The key is field name which is used in cape visa. */
   private String key;

   /** The pattern, used for validate field value. */
   private String pattern;

   /**
    * Constructor with key.
    *
    * @param key the field name, which is used in cape visa.
    * @param pattern the pattern, which is used for validating field value.
    */
   private HybrisTHCruiseDataPanel(String key, String pattern)
   {
      this.key = key;
      this.pattern = pattern;
   }

   /**
    * It will give key.
    *
    * @return key
    */
   public String getKey()
   {
      return key;
   }

   /**
    * This method is responsible for populating map with the details of the
    * specified fields in the validation list.
    * @return the patternMap, the patternMap with all cape visa data panel values.
    */
   public static Map<String, String> getAllPatterns()
   {
      Map<String, String> patternMap = new HashMap<String, String>();
      for (HybrisTHCruiseDataPanel hybrisTHCruiseDataPanel : values())
      {
            patternMap.put(hybrisTHCruiseDataPanel.key, hybrisTHCruiseDataPanel.pattern);
      }
      return patternMap;
   }

}

