/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PortlandPostPaymentProcessor.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: $
 */
package com.tui.uk.payment.processor;

import java.util.List;
import java.util.Map;


import com.tui.uk.client.domain.PassengerSummary;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.config.ConfReader;

/**
 * This performs all the operations required for post payment for .NET applications.
 *
 * @author sindhushree.g
 *
 */
public final class FcfoPostPaymentProcessor extends DotNetClientPostPaymentProcessor
{
   /** The passenger key. */
   private static final String PASSENGER_KEY = "passenger_";

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public FcfoPostPaymentProcessor(PaymentData paymentData,
      Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the
    * payment page. This may include, updating non payment data map,
    * validation.
    *
    * @throws PostPaymentProcessorException
    *             if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
   {
      String conf = "TUIGiftCard.BINRange";
      String giftCardNumber = ConfReader.getConfEntry(conf, null);
      String cardNumber=requestParameterMap.get("payment_0_cardNumber");
      if (cardNumber.startsWith(giftCardNumber))
      {
         if(!(requestParameterMap.get("payment_0_type").equalsIgnoreCase("MAESTRO")))
         {
            throw new PostPaymentProcessorException
            ("datacash.cardtype.mismatch");
         }
      }
      if(requestParameterMap.get("payment_0_type").equalsIgnoreCase("MAESTRO"))
      {
         if (!(cardNumber.startsWith(giftCardNumber)))
         {
            throw new PostPaymentProcessorException
            ("datacash.cardtype.mismatch");
         }
      }
      super.preProcess();
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   @Override
   public void process() throws PaymentValidationException,
   PostPaymentProcessorException
   {
      super.process();
   }

   /**
    * Updates the passenger information.
    */
   @Override
   protected void updatePassengerInformation()
   {
      int passengerIndex = 1;
      int childPassengerIndex = 1;
      int infantPassengerIndex = 1;

      int adultCount = 0;

      Map<String, String> nonPaymentData = bookingComponent.getNonPaymentData();
      if (nonPaymentData.containsKey("passenger_ADT_count"))
      {
         adultCount = Integer.valueOf(nonPaymentData.get("passenger_ADT_count"));
      }

      int childCount = 0;
      if (nonPaymentData.containsKey("passenger_CHD_count"))
      {
         childCount = Integer.valueOf(nonPaymentData.get("passenger_CHD_count"));
      }

      int infantCount = 0;
      if (nonPaymentData.containsKey("passenger_INF_count"))
      {
         infantCount = Integer.valueOf(nonPaymentData.get("passenger_INF_count"));
      }


      if (bookingComponent.getPassengerRoomSummary() != null)
      {
         for (List<PassengerSummary> passengerSummaryList : bookingComponent
                  .getPassengerRoomSummary().values())
         {
            for (PassengerSummary passengerSummary : passengerSummaryList)
            {
               if (passengerIndex <= adultCount)
               {
                  if (requestParameterMap.containsKey(PASSENGER_KEY
                     + passengerIndex +  "_title"))
                  {
                     passengerSummary.setTitle(requestParameterMap.get(PASSENGER_KEY
                        + passengerIndex +  "_title"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY + passengerIndex
                     + "_foreName"))
                  {
                     passengerSummary.setForeName(requestParameterMap.get(PASSENGER_KEY
                        + passengerIndex + "_foreName"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY + passengerIndex
                     + "_lastName"))
                  {
                     passengerSummary.setLastName(requestParameterMap.get(PASSENGER_KEY
                        + passengerIndex + "_lastName"));
                  }
                  if(passengerIndex == 1)
                  {
                     passengerSummary.setLeadPassenger(true);
                  }
                  passengerIndex++;
               }
               else if (childPassengerIndex <= childCount)
               {
                  if (requestParameterMap.containsKey(PASSENGER_KEY
                     + childPassengerIndex +  "_childTitle"))
                  {
                     passengerSummary.setTitle(requestParameterMap.get(PASSENGER_KEY
                        + childPassengerIndex +  "_childTitle"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY
                     + childPassengerIndex + "_childForeName"))
                  {
                     passengerSummary.setForeName(requestParameterMap.get(PASSENGER_KEY
                        + childPassengerIndex + "_childForeName"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY
                     + childPassengerIndex + "_childLastName"))
                  {
                     passengerSummary.setLastName(requestParameterMap.get(PASSENGER_KEY
                        + childPassengerIndex + "_childLastName"));
                  }
                  childPassengerIndex++;
               }
               else if (infantPassengerIndex <= infantCount)
               {
                  if (requestParameterMap.containsKey(PASSENGER_KEY
                     + infantPassengerIndex +  "_infantTitle"))
                  {
                     passengerSummary.setTitle(requestParameterMap.get(PASSENGER_KEY
                        + infantPassengerIndex +  "_infantTitle"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY
                     + infantPassengerIndex + "_infantForeName"))
                  {
                     passengerSummary.setForeName(requestParameterMap.get(PASSENGER_KEY
                        + infantPassengerIndex + "_infantForeName"));
                  }
                  if (requestParameterMap.containsKey(PASSENGER_KEY
                     + infantPassengerIndex + "_infantLastName"))
                  {
                     passengerSummary.setLastName(requestParameterMap.get(PASSENGER_KEY
                        + infantPassengerIndex + "_infantLastName"));
                  }
                  infantPassengerIndex++;
               }
            }
         }
      }
   }
}
