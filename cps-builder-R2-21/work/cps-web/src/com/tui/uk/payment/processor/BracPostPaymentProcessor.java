/*
 *
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: BracPostPaymentProcessor.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: $
 */
package com.tui.uk.payment.processor;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.client.domain.PaymentMode;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for BRAC application.
 *
 * @author sindhushree.g
 *
 */
public final class BracPostPaymentProcessor extends BasePostPaymentProcessor
{
   /** The Constant to hold additional deposit type value. */
   private static final String ADDITIONAL_DEPOSIT_TYPE = "ADDITIONAL";

   /** The Constant to hold the SeachMode. */
   private static final String SEARCH_MODE = "CSS Mode";

   /** The currency of the bookingcomponent. */
   private final Currency currency = bookingComponent.getTotalAmount().getCurrency();

   /** The MANUAL_SEGMENTS is used as key to access conf entry.  */
   private static final String MANUAL_SEGMENTS = "BRACApplication.manualSegments";

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public BracPostPaymentProcessor(PaymentData paymentData,
                                   Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   @Override
   public void preProcess() throws PostPaymentProcessorException
   {
      final int totalTransactions = getContributionCount();
      super.updateSelectedDepositAmount();
      // if the selected payment method is NoPayment as the transaction amount is zero we should
      // make the calculatePayable amount to zero.
      if (totalTransactions == 1
         && requestParameterMap.get(
            PaymentConstants.PAYMENT + "0" + PaymentConstants.PAYMENT_METHOD).trim().equals(
            PaymentMethod.NO_PAYMENT.getCode()))
      {
         bookingInfo.resetPayableAmount();
      }
     if (bookingInfo.getCalculatedPayableAmount().getAmount().doubleValue() < 0
               && SEARCH_MODE.equalsIgnoreCase(bookingComponent.getSearchType())
                   && totalTransactions > 1)
      {
         for (int i = 0; i < totalTransactions; i++)
         {
            if (requestParameterMap.get(
               PaymentConstants.PAYMENT + i + PaymentConstants.PAYMENT_METHOD).trim().equals(
               PaymentMethod.NO_PAYMENT.getCode()))
               {
                 BigDecimal payableAmount = bookingInfo.getCalculatedPayableAmount().getAmount()
                         .subtract(new BigDecimal(requestParameterMap.get(
                             PaymentConstants.PAYMENT + i + PaymentConstants.AMOUNT_PAID)));
                 bookingInfo.setCalculatedPayableAmount(new Money(payableAmount, currency));
                 requestParameterMap.put(
                  PaymentConstants.PAYMENT + i + PaymentConstants.AMOUNT_PAID, "0.0");
                 requestParameterMap.put(PaymentConstants.TOTAL_TRANS_AMOUNT,
                         String.valueOf(payableAmount));
              }
         }
      }
      super.preProcess();
      //TODO: The below IF condition should be removed once Manual Segments goes live.
      if (!ConfReader.getBooleanEntry(MANUAL_SEGMENTS, false))
      {
         validateAmount(getTotalTransactionAmount(totalTransactions));
         verifyAndUpdateCardCharge(totalTransactions);
      }
      else
      {
         validateAmountForManualSegments(getTotalTransactionAmount(totalTransactions));
         verifyAndUpdateCardChargeForManualSegments(totalTransactions);
      }
      getPinPadContributionCount();
      setCardDetailsForReferredTransactions();
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while validating payment data.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   @Override
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      int contributionCount = getContributionCount();

      // checking if it is a hopla postPay and it is a first time
      // amendment then incrementing the contribution count.
      if (bookingComponent.getAccommodationSummary() != null
            && bookingComponent.getPaymentType().containsKey(
            PaymentMode.POSTPAYMENTGUARANTEE.getCode())
            && !bookingComponent.getAccommodationSummary().getPrepay())
      {
         contributionCount++;
      }
      if (paymentData.getPayment() != null)
      {
         paymentData.getPayment().clearPaymentTransactions();
      }
      createAndProcessPayments(contributionCount);
   }

   /**
    * This returns the total amount that is entered by the client with out any card charges by adding
    * the individual transaction amount.
    *
    * @param totalTransactions Number of transactions
    * @return totalTransactionAmount totalAmount
    */
   private Money getTotalTransactionAmount(Integer totalTransactions)
   {
      BigDecimal totalTransactionAmount = BigDecimal.ZERO;
      for (int i = 0; i < totalTransactions; i++)
      {
         totalTransactionAmount = totalTransactionAmount.add(new BigDecimal(requestParameterMap
               .get(PaymentConstants.PAYMENT + i + PaymentConstants.AMOUNT_PAID)));
      }
      return new Money(totalTransactionAmount.setScale(2, BigDecimal.ROUND_HALF_UP), currency);
   }

   /**
    * This method is responsible for calculating the  the cardcharges for individual transaction from server side
    * and sums up and gives the total card charges.
    *@param  totalTransactions Total Number of Transactions.
    *@return Money  cumulativeCardCharge
    */
   private Money calculateCardChargesForMulitpleTransactions(int totalTransactions)
   {
      BigDecimal cumulativeCardCharge = BigDecimal.ZERO;
      for (int i = 0; i < totalTransactions; i++)
      {
         //BigDecimal cardChargePercent = BigDecimal.ZERO;
         // DCard etc
         final String paymentMethod = requestParameterMap.get(PaymentConstants.PAYMENT
            + i + PaymentConstants.PAYMENT_METHOD).trim();
         // MASTERCARD, AMERICAN_EXPRESS etc
         final String paymentCode = requestParameterMap.get(PaymentConstants.PAYMENT
            + i + PaymentConstants.PAYMENT_TYPE_CODE).trim();
         requestParameterMap.put(PaymentConstants.PAYMENT + i
            + PaymentConstants.PAYMENT_TYPE_CODE, paymentCode);
         requestParameterMap.put(PaymentConstants.PAYMENT + i + PaymentConstants.PAYMENT_METHOD,
            paymentMethod);

         BigDecimal cardCharge = BigDecimal.ZERO;
         BigDecimal amountPaid = BigDecimal.valueOf(Double.valueOf(requestParameterMap.get(
                 PaymentConstants.PAYMENT + i + PaymentConstants.AMOUNT_PAID)));
         if ((paymentMethod.equalsIgnoreCase(PaymentConstants.DCARD) || paymentMethod
            .equalsIgnoreCase(PaymentConstants.CARD))
               && bookingComponent.getTotalAmount().getAmount().doubleValue() > 0)
         {
            cardCharge = getCardChargePerTransaction(requestParameterMap.get(PaymentConstants
                    .PAYMENT + i + PaymentConstants.PAYMENT_TYPE_CODE), amountPaid);
         }
         else if (paymentMethod.equalsIgnoreCase(PaymentConstants.CARDPINPAD))
         {
            cardCharge = getCardChargePerTransaction(requestParameterMap.get(PaymentConstants
                    .PAYMENT + PaymentConstants.CARD_TYPE + i), amountPaid);
         }
         cumulativeCardCharge = cumulativeCardCharge.add(cardCharge).
            setScale(2, BigDecimal.ROUND_HALF_UP);

         // Set transaction amount determined as above to requestParameterMap
         requestParameterMap.put(PaymentConstants.PAYMENT + i + PaymentConstants.TRANSACTION_AMOUNT,
            (String) requestParameterMap.get(PaymentConstants.PAYMENT + i
               + PaymentConstants.AMOUNT_PAID));
         requestParameterMap.put(PaymentConstants.PAYMENT + i + PaymentConstants.CHARGE,
            (String) requestParameterMap.get(PaymentConstants.PAYMENT + i
               + PaymentConstants.CHARGE));
      }
      return new Money(cumulativeCardCharge, currency);
   }

   /**
    * This method is responsible for checking whether card charge has been applied and if not will
    * update the card charges in the calculate payable amount. it first checks whther the amount
    * that is sent from client is equal to the sum of totaltransaction and server side calculated
    * card charges then it will updated the caculated payable amount. else throws an exception.
    * @param totalTransactions Number of Transactions.
    * @throws PostPaymentProcessorException the post payment processor exception.
    *
    */
   private void verifyAndUpdateCardCharge(int totalTransactions)
      throws PostPaymentProcessorException
   {
      Money totalAmountFromClient =
         new Money(new BigDecimal(requestParameterMap.get(PaymentConstants.TOTAL_TRANS_AMOUNT)),
            currency);
      Money totalAmount = getTotalTransactionAmount(totalTransactions);
      Money cumulativeCardCharge = calculateCardChargesForMulitpleTransactions(totalTransactions);
      bookingInfo.setCalculatedCardCharge(cumulativeCardCharge);
      if (totalAmountFromClient.getAmount().doubleValue() == totalAmount
               .add(cumulativeCardCharge).getAmount().doubleValue())
      {
         if (ADDITIONAL_DEPOSIT_TYPE.equalsIgnoreCase(bookingComponent.getNonPaymentData().get(
            BookingConstants.DEPOSIT_TYPE).trim()))
         {
            bookingInfo.setCalculatedPayableAmount(totalAmount.add(cumulativeCardCharge));
         }
         else
         {
            bookingInfo.setCalculatedPayableAmount(bookingInfo.getCalculatedPayableAmount().add(
               cumulativeCardCharge));
         }
      }
      else
      {
         populateException();
      }
   }


   /**
    * This method is responsible for checking whether card charge has been applied and if not will
    * update the card charges in the calculate payable amount. it first checks whther the amount
    * that is sent from client is equal to the sum of totaltransaction and server side calculated
    * card charges then it will updated the caculated payable amount. else throws an exception.
    * @param totalTransactions Number of Transactions.
    * @throws PostPaymentProcessorException the post payment processor exception.
    *
    */
   private void verifyAndUpdateCardChargeForManualSegments(int totalTransactions)
      throws PostPaymentProcessorException
   {
      Money totalAmountFromClient =
         new Money(new BigDecimal(requestParameterMap.get(PaymentConstants.TOTAL_TRANS_AMOUNT)),
            currency);
      Money totalAmount = getTotalTransactionAmount(totalTransactions);
      Money cumulativeCardCharge = calculateCardChargesForMulitpleTransactions(totalTransactions);
      bookingInfo.setCalculatedCardCharge(cumulativeCardCharge);
      if (totalAmountFromClient.getAmount().doubleValue() == totalAmount
               .add(cumulativeCardCharge).getAmount().doubleValue())
      {
         if (BookingConstants.PARTIAL_DEPOSIT.equalsIgnoreCase(bookingComponent.getNonPaymentData()
               .get(BookingConstants.DEPOSIT_TYPE).trim()))
         {
            bookingInfo.setCalculatedPayableAmount(totalAmount.add(cumulativeCardCharge));
            bookingInfo.setCalculatedTotalAmount(totalAmount.add(cumulativeCardCharge));
         }
         else
         {
            bookingInfo.setCalculatedPayableAmount(bookingInfo.getCalculatedPayableAmount().add(
               cumulativeCardCharge));
            bookingInfo.setCalculatedTotalAmount(bookingInfo.getCalculatedTotalAmount().add(
                    cumulativeCardCharge));
         }
      }
      else
      {
         populateException();
      }
   }

   /**
    * This Method is used to validate the Amount for BRAC depending on the several conditions . 1.
    * If it is an additional payment then it accepts the amount with in the range of calculated
    * payable amount to the total amount of bookingComponent. 2. if
    * it is Refund in CSSMode search then it accepts range upto calculated payable amount. 3. and in
    * all the other cases the transaction amount should be equal to the calculated payable amount.
    * it checks for the above three conditions and if any condition is failed it throws a
    * PostPaymentProcessorException.
    *
    * @param amount the amount paid.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   private void validateAmount(Money amount) throws PostPaymentProcessorException
   {
      if (ADDITIONAL_DEPOSIT_TYPE.equalsIgnoreCase(bookingComponent.getNonPaymentData().get(
         BookingConstants.DEPOSIT_TYPE).trim()))
      {
         if (amount.getAmount().compareTo(bookingInfo.getCalculatedPayableAmount().getAmount()) < 0
            || amount.getAmount().compareTo(bookingComponent.getTotalAmount().getAmount()) > 0)
         {
            populateException();
         }
      }
      else if (bookingInfo.getCalculatedPayableAmount().getAmount().doubleValue() < 0
               && SEARCH_MODE.equalsIgnoreCase(bookingComponent.getSearchType()))
      {
         if (amount.getAmount().compareTo(bookingInfo.getCalculatedPayableAmount().getAmount()) < 0)
         {
            populateException();
         }
      }
      else if (amount.getAmount().doubleValue() != bookingInfo.getCalculatedPayableAmount()
         .getAmount().doubleValue())
      {
         populateException();
      }
    }

   /**
    * This Method is used to validate the Amount for BRAC depending on the several conditions . 1.
    * If it is partial deposit then it accepts the amount with in the range of calculated
    * payable amount to the total amount of bookingComponent. 2. if
    * it is Refund in CSSMode search then it accepts range upto calculated payable amount. 3. and in
    * all the other cases the transaction amount should be equal to the calculated payable amount.
    * it checks for the above three conditions and if any condition is failed it throws a
    * PostPaymentProcessorException.
    *
    * @param amount the amount paid.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   private void validateAmountForManualSegments(Money amount) throws PostPaymentProcessorException
   {
      if (BookingConstants.PARTIAL_DEPOSIT.equalsIgnoreCase(bookingComponent.getNonPaymentData()
             .get(BookingConstants.DEPOSIT_TYPE).trim()))
      {
         if (amount.getAmount().compareTo(bookingInfo.getCalculatedPayableAmount().getAmount()) < 0
            || amount.getAmount().compareTo(bookingComponent.getTotalAmount().getAmount()) > 0)
         {
            populateException();
         }
      }
      else if (bookingInfo.getCalculatedPayableAmount().getAmount().doubleValue() < 0
               && SEARCH_MODE.equalsIgnoreCase(bookingComponent.getSearchType()))
      {
         if (amount.getAmount().compareTo(bookingInfo.getCalculatedPayableAmount().getAmount()) < 0)
         {
            populateException();
         }
      }
      else if (amount.getAmount().doubleValue() != bookingInfo.getCalculatedPayableAmount()
         .getAmount().doubleValue())
      {
         populateException();
      }
    }



   /**
    * This method logs and throws <code>PostPaymentProcessorException</code>.
    *
    * @throws PostPaymentProcessorException after logging the exception message.
    */
   private void populateException() throws PostPaymentProcessorException
   {
      LogWriter.logErrorMessage(PropertyResource.getProperty(ERROR_MESSAGE_KEY, MESSAGES_PROPERTY));
      throw new PostPaymentProcessorException(ERROR_MESSAGE_KEY);
   }

   /**
    * This method will check if the masked card number and masked cv2 is present in the request parameter
    * map and reads the card number and cv2 from transaction object and sets back into the
    * request parameter map.
    *
    *
    */
   private void setCardDetailsForReferredTransactions()
   {
       if (paymentData.getPayment() != null
                && paymentData.getPayment().getPaymentTransactions().size() == 1)
          {
             for (int i = 0; i < paymentData.getPayment().getPaymentTransactions().size(); i++)
             {
                String prefix = PaymentConstants.PAYMENT + i;
                if (StringUtils.isNotBlank(requestParameterMap.get(prefix
                   + PaymentConstants.CARD_NUMBER)) && requestParameterMap.get(prefix
                      + PaymentConstants.CARD_NUMBER).startsWith("*"))
                {
                   requestParameterMap.put(prefix + PaymentConstants.CARD_NUMBER,
                      String.valueOf(((DataCashPaymentTransaction) paymentData.getPayment()
                         .getPaymentTransactions().get(i)).getCard().getPan()));
                }
                if (StringUtils.isNotBlank(requestParameterMap.get(prefix
                   + PaymentConstants.SECURITY_CODE)) && requestParameterMap.get(prefix
                      + PaymentConstants.SECURITY_CODE).startsWith("*"))
                {
                   requestParameterMap.put(prefix + PaymentConstants.SECURITY_CODE,
                      String.valueOf(((DataCashPaymentTransaction) paymentData.getPayment()
                         .getPaymentTransactions().get(i)).getCard().getCv2()));
                }
             }
          }
   }

   /**
    * This method is responsible to calculate the card charge for each of the transactions.
    *
    * @param paymentTypeCode the selected payment type code.
    * @param amountPaidPerTransaction the amount for this transaction.
    *
    * @return the calculated card charge.
    */
   private BigDecimal getCardChargePerTransaction(String paymentTypeCode,
           BigDecimal amountPaidPerTransaction)
   {
      String[] cardChargeData = bookingInfo.getCardChargeMap().get(paymentTypeCode).split(",");

      BigDecimal cardChargePercent = BigDecimal.valueOf(Double.valueOf(cardChargeData[0]));
      BigDecimal minCardCharge = BigDecimal.valueOf(Double.valueOf(cardChargeData[1]));
      BigDecimal maxCardCharge = BigDecimal.valueOf(Double.valueOf(cardChargeData[2]));

      BigDecimal calculatedCardChargePerTransaction = cardChargePercent.multiply(
              amountPaidPerTransaction).divide(BigDecimal.valueOf(DispatcherConstants
                      .PERCENTAGE_FACTOR));

      if (calculatedCardChargePerTransaction.doubleValue() < minCardCharge.doubleValue())
      {
          calculatedCardChargePerTransaction = minCardCharge;
      }

      if (calculatedCardChargePerTransaction.doubleValue() > maxCardCharge.doubleValue())
      {
          calculatedCardChargePerTransaction = maxCardCharge;
      }

      return calculatedCardChargePerTransaction;
   }

}
