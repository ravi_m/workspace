/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: WishPostPaymentProcessor.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: $
*/
package com.tui.uk.payment.processor;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for WISH application.
 *
 * @author sindhushree.g
 *
 */
public final class WishPostPaymentProcessor extends BasePostPaymentProcessor
{

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public WishPostPaymentProcessor(PaymentData paymentData, Map<String, String[]>
      requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page.
    * This may include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   @Override
   public void preProcess() throws PostPaymentProcessorException
   {
      updateSelectedDepositAmount();
      if (requestParameterMap.get(BookingConstants.DEPOSIT_TYPE) != null)
      {
         String depositType = requestParameterMap.get(BookingConstants.DEPOSIT_TYPE).trim();
         /*TODO Need to remove the condition once all the applications are compatible with
         one of the versions.*/
         if (!ConfReader.getBooleanEntry(bookingComponent.getClientApplication()
            .getClientApplicationName() + ".lowDepositFeatureAvailable", false))
         {
            bookingComponent.getNonPaymentData().put(
               BookingConstants.SELECTED_DEPOSIT_TYPE, depositType);
         }
         bookingComponent.getNonPaymentData().put(BookingConstants.DEPOSIT_TYPE, depositType);
      }
      setCardDetailsForReferredTransactions();
      super.preProcess();
      calculateCardChargesForMulitpleTransactions();
      getPinPadContributionCount();
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   @Override
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      final String promoCode = bookingComponent.getNonPaymentData().get("promotionalCode");
      if (StringUtils.isNotBlank(promoCode) && !Boolean.parseBoolean(bookingComponent
         .getNonPaymentData().get("isPromoCodeApplied")))
      {
         validatePromotionalCode(promoCode, requestParameterMap.get(DispatcherConstants
            .BALANCE_TYPE));
      }
      int contributionCount = getContributionCount();

      //TODO: The following condition should be removed or modified once all the units are bonded.
      if (bookingComponent.getAccommodationSummary() != null
            && StringUtils.isNotBlank(bookingComponent.getAccommodationSummary().getHoplaBonded()))
      {
         if (bookingComponent.getAccommodationSummary().getPrepay() != null
            && !bookingComponent.getAccommodationSummary().getPrepay() && isHopla())
         {
            contributionCount++;
         }
      }
      else
      {
         if (isHopla())
         {
                 contributionCount++;
         }
      }
      if (paymentData.getPayment() != null)
      {
         paymentData.getPayment().clearPaymentTransactions();
      }
      bookingComponent.getNonPaymentData().put(DispatcherConstants.CUSTOMER_TYPE,
         requestParameterMap.get(DispatcherConstants.CUSTOMER_TYPE));
      createAndProcessPayments(contributionCount);
   }

   /**
    * This method is responsible for checking all the transaction
    * amount, card charges and their total amount and validate against
    * the client side calculated total.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   private void calculateCardChargesForMulitpleTransactions() throws PostPaymentProcessorException
   {
      BigDecimal totalAmountFromClient = new BigDecimal(requestParameterMap
         .get(PaymentConstants.TOTAL_TRANS_AMOUNT));
      totalAmountFromClient = totalAmountFromClient.setScale(2, BigDecimal.ROUND_HALF_UP);
      BigDecimal totalTransactionAmount = BigDecimal.ZERO;
      BigDecimal calculatedCardCharge = BigDecimal.ZERO;
      BigDecimal totalCardCharge = BigDecimal.ZERO;
      for (int i = 0; i < getContributionCount(); i++)
      {
         BigDecimal calculatedCardChargePerTransaction = BigDecimal.ZERO;
         BigDecimal amountPaidPerTransaction = new BigDecimal(requestParameterMap
            .get(PaymentConstants.PAYMENT + i + PaymentConstants.AMOUNT_PAID));
         BigDecimal cardChargePerTransaction = BigDecimal.ZERO;
         totalTransactionAmount = totalTransactionAmount.add(amountPaidPerTransaction);
         if (requestParameterMap.get(PaymentConstants.PAYMENT
                  + i + PaymentConstants.CHARGE) != null)
         {
            cardChargePerTransaction = new BigDecimal(requestParameterMap
               .get(PaymentConstants.PAYMENT + i + PaymentConstants.CHARGE));
            totalCardCharge = totalCardCharge.add(new BigDecimal(
               requestParameterMap.get(PaymentConstants.PAYMENT + i + PaymentConstants.CHARGE)));
            totalCardCharge = totalCardCharge.setScale(2, BigDecimal.ROUND_HALF_UP);
         }
         //MASTERCARD, AMERICAN_EXPRESS etc
         String paymentCode = requestParameterMap.get(PaymentConstants.PAYMENT
            + i + PaymentConstants.PAYMENT_TYPE_CODE).trim();
         //DCard etc
         String paymentMethod = requestParameterMap.get(PaymentConstants.PAYMENT
            + i + PaymentConstants.PAYMENT_METHOD).trim();
         if (paymentMethod.equalsIgnoreCase(PaymentMethod.PIN_PAD.getCode()))
         {
            paymentCode = requestParameterMap.get(PaymentConstants.PAYMENT
               + PaymentConstants.CARD_TYPE + i);
         }
         if (paymentMethod.equalsIgnoreCase(PaymentMethod.DATACASH.getCode())
                  || paymentMethod.equalsIgnoreCase(PaymentMethod.PDQ.getCode())
                  || paymentMethod.equalsIgnoreCase(PaymentMethod.PIN_PAD.getCode()))
         {
            /*calculatedCardChargePerTransaction = bookingComponent.getCardChargesMap().
               get(paymentCode).multiply(amountPaidPerTransaction).divide(BigDecimal.valueOf(
                  DispatcherConstants.PERCENTAGE_FACTOR));
            if (calculatedCardChargePerTransaction.doubleValue() > bookingComponent
                     .getMaxCardCharge().doubleValue())
            {
               calculatedCardChargePerTransaction = bookingComponent.getMaxCardCharge();
            }*/
            calculatedCardChargePerTransaction = getCardChargePerTransaction(paymentCode,
                    amountPaidPerTransaction);
            calculatedCardCharge = calculatedCardCharge.add(calculatedCardChargePerTransaction)
               .setScale(2, BigDecimal.ROUND_HALF_UP);

            if (totalCardCharge.doubleValue() != calculatedCardCharge.doubleValue())
            {
               LogWriter.logErrorMessage(PropertyResource.getProperty(ERROR_MESSAGE_KEY,
                  MESSAGES_PROPERTY));
               throw new PostPaymentProcessorException(ERROR_MESSAGE_KEY);
            }
         }
         //Set transaction amount, payment type/code determined as above to requestParameterMap
         requestParameterMap.put(PaymentConstants.PAYMENT + i
            + PaymentConstants.PAYMENT_TYPE_CODE, paymentCode);
         requestParameterMap.put(PaymentConstants.PAYMENT + i
            + PaymentConstants.PAYMENT_METHOD, paymentMethod);
         requestParameterMap.put(PaymentConstants.PAYMENT + i + PaymentConstants.TRANSACTION_AMOUNT,
            amountPaidPerTransaction.toString());
         requestParameterMap.put(PaymentConstants.PAYMENT + i + PaymentConstants.CHARGE,
            cardChargePerTransaction.toString());
      }
      processAndSetAmounts(calculatedCardCharge, totalTransactionAmount, totalAmountFromClient);
   }

   /**
    * This method is responsible for setting the calculated total amount,
    * calculated card charge and calculated payable amount into bookingInfo
    * object, if everything goes well. Else, appropriate error will be thrown.
    *
    * @param calculatedCardCharge Calculated card charge amount.
    * @param totalTransactionAmount Total transaction amount.
    * @param totalAmountFromClient Total amount calculated at client side.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   private void processAndSetAmounts(BigDecimal calculatedCardCharge,
      BigDecimal totalTransactionAmount, BigDecimal totalAmountFromClient)
      throws PostPaymentProcessorException
   {
      BigDecimal totalAmount = bookingInfo.getCalculatedPayableAmount()
      .getAmount().add(calculatedCardCharge);
      totalAmount = totalAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
      if (totalTransactionAmount.doubleValue()
            == bookingInfo.getCalculatedPayableAmount().getAmount().doubleValue()
            && totalAmountFromClient.doubleValue() == totalAmount.doubleValue())
      {
         final Currency currency = bookingComponent.getTotalAmount().getCurrency();
         final Money calculatedCardCharges = new Money(calculatedCardCharge, currency);
         bookingInfo.setCalculatedPayableAmount(new Money(totalAmount, currency));
         bookingInfo.setCalculatedCardCharge(calculatedCardCharges);
         bookingInfo.setCalculatedTotalAmount(bookingInfo.getCalculatedTotalAmount()
            .add(calculatedCardCharges));
      }
      else
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(
            ERROR_MESSAGE_KEY, MESSAGES_PROPERTY));
         throw new PostPaymentProcessorException(ERROR_MESSAGE_KEY);
      }
   }

   /**
    * This method will check if the masked card number and masked cv2 is present in the request parameter
    * map and reads the card number and cv2 from transaction object and sets back into the
    * request parameter map.
    *
    */
   private void setCardDetailsForReferredTransactions()
   {
      if (paymentData.getPayment() != null
               && paymentData.getPayment().getPaymentTransactions().size() == 1)
      {
         for (int i = 0; i < paymentData.getPayment().getPaymentTransactions().size(); i++)
         {
            String prefix = PaymentConstants.PAYMENT + i;
            if (StringUtils.isNotBlank(requestParameterMap.get(prefix
               + PaymentConstants.CARD_NUMBER)) && requestParameterMap.get(prefix
                  + PaymentConstants.CARD_NUMBER).startsWith("*"))
            {
               requestParameterMap.put(prefix + PaymentConstants.CARD_NUMBER,
                  String.valueOf(((DataCashPaymentTransaction) paymentData.getPayment()
                     .getPaymentTransactions().get(i)).getCard().getPan()));
            }
            if (StringUtils.isNotBlank(requestParameterMap.get(prefix
               + PaymentConstants.SECURITY_CODE)) && requestParameterMap.get(prefix
                  + PaymentConstants.SECURITY_CODE).startsWith("*"))
            {
               requestParameterMap.put(prefix + PaymentConstants.SECURITY_CODE,
                  String.valueOf(((DataCashPaymentTransaction) paymentData.getPayment()
                     .getPaymentTransactions().get(i)).getCard().getCv2()));
            }
         }
      }
   }

   /**
    * This method is responsible to calculate the card charge for each of the transactions.
    *
    * @param paymentTypeCode the selected payment type code.
    * @param amountPaidPerTransaction the amount for this transaction.
    *
    * @return the calculated card charge.
    */
   private BigDecimal getCardChargePerTransaction(String paymentTypeCode,
           BigDecimal amountPaidPerTransaction)
   {
      String[] cardChargeData = bookingInfo.getCardChargeMap().get(paymentTypeCode).split(",");

      BigDecimal cardChargePercent = BigDecimal.valueOf(Double.valueOf(cardChargeData[0]));
      BigDecimal minCardCharge = BigDecimal.valueOf(Double.valueOf(cardChargeData[1]));
      BigDecimal maxCardCharge = BigDecimal.valueOf(Double.valueOf(cardChargeData[2]));

      BigDecimal calculatedCardChargePerTransaction = cardChargePercent.multiply(
              amountPaidPerTransaction).divide(BigDecimal.valueOf(DispatcherConstants
                      .PERCENTAGE_FACTOR));

      if (calculatedCardChargePerTransaction.doubleValue() < minCardCharge.doubleValue())
      {
          calculatedCardChargePerTransaction = minCardCharge;
      }

      if (calculatedCardChargePerTransaction.doubleValue() > maxCardCharge.doubleValue())
      {
          calculatedCardChargePerTransaction = maxCardCharge;
      }

      return calculatedCardChargePerTransaction;
   }

}