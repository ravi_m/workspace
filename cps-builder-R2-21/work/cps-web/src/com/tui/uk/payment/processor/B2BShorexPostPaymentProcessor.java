package com.tui.uk.payment.processor;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.math.BigDecimal;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import org.apache.commons.lang.StringUtils;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentFlow;
import com.tui.uk.email.exception.EmailProcessorException;
import com.tui.uk.email.shorex.processor.ShorexEmailProcessor;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.ChequePaymentTransaction;
import com.tui.uk.payment.domain.Payment;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.domain.PaymentTransactionFactory;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for .NET applications.
 * 
 * @author Suman.P
 * 
 */

public class B2BShorexPostPaymentProcessor extends DotNetClientPostPaymentProcessor
{
	// CHECKSTYLE:OFF
   /** The constant BOOKING. */
   private static final String BOOKING = "booking.";

   /** The constant INVALID. */
   private static final String INVALID = ".invalid";

   /** The constant REFUNDAMT. */
   private static final String REFUNDAMT = "refundAmount";

   /** The pattern for the part payment amount. */
   private static final Pattern AMOUNT_PATTERN = Pattern.compile("^[-]?[\\d]*[.]?[\\d]+$");

   /** The zero'th index. */
   private static final String ZERO_INDEX = "0";

   /** Constant for phoneNumber. */
   private static final String PHONENUMBER = "phoneNumber";

   /** Constant for mobilePhone. */
   private static final String MOBILEPHONE = "mobilePhone";

   /** Constant for card charge amount. */
   private static final String TRANSAMTCCTEXT = "_transamtCCText";

   /** Constant for card charge amount. */
   private static final String REFUNDED_CARDCHARGE = "_refundedCardCharge";

   /**
    * Constructor with Payment Data.
    * 
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public B2BShorexPostPaymentProcessor(PaymentData paymentData,
                                        Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    * 
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      // String prefix = PaymentConstants.PAYMENT + ZERO_INDEX;
      String paymentFlow = bookingComponent.getNonPaymentData().get("payment_flow");
      if (!StringUtils.equalsIgnoreCase(paymentFlow, (PaymentFlow.REFUND).toString()))
      {
         updateBillingAddressData();
         super.process();
      }
      else
      {
         processRefundTransactions();
      }

   }

   /**
    * This method is updates the request parameter with the billing details as necessary
    * 
    */
   private void updateBillingAddressData()
   {
      String prefix = PaymentConstants.PAYMENT + ZERO_INDEX;
      requestParameterMap.put(prefix + PaymentConstants.POST_CODE, bookingComponent
         .getContactInfo().getPostCode());
      requestParameterMap.put(prefix + PHONENUMBER, bookingComponent.getContactInfo()
         .getPhoneNumber());
      requestParameterMap.put(prefix + MOBILEPHONE, bookingComponent.getContactInfo()
         .getAltPhoneNumber());
      requestParameterMap.put(prefix + PaymentConstants.HOUSENO, bookingComponent.getContactInfo()
         .getStreetAddress1());
      requestParameterMap.put(prefix + PaymentConstants.STREETADDRESS, bookingComponent
         .getContactInfo().getStreetAddress2());
      requestParameterMap.put(prefix + PaymentConstants.TOWN, bookingComponent.getContactInfo()
         .getCity());
      requestParameterMap.put(prefix + PaymentConstants.COUNTY, bookingComponent.getContactInfo()
         .getCounty());
      requestParameterMap.put(prefix + PaymentConstants.SELECTED_COUNTRY, bookingComponent
         .getContactInfo().getCountry());
      requestParameterMap.put(prefix + PaymentConstants.SELECTED_COUNTRY_CODE, bookingComponent
         .getContactInfo().getCountry());
      // requestParameterMap.put(prefix + PaymentConstants.ADDITIONAL_ADDRESS_LINE1,
      // bookingComponent
      // .getContactInfo().getStreetAddress2());

   }

   /**
    * This method is responsible for validating the refund transaction amount and creating
    * transactions.
    * 
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */

   private void processRefundTransactions() throws PaymentValidationException,
      PostPaymentProcessorException
   {
      int noOfRefunds =
         Integer.parseInt(requestParameterMap.get(PaymentConstants.PAYMENT
            + PaymentConstants.NO_OF_REFUNDS));

      // validating card charges individually for admin users
      if (bookingComponent.getNonPaymentData().get("is_admin_user") != null
         && bookingComponent.getNonPaymentData().get("is_admin_user").equals("true"))
      {
         validateRefundCardChargeAmount(noOfRefunds);
      }
      double totccamt = getRefundCardChargeAmount(noOfRefunds);

      setSelectedRefundDetails(noOfRefunds);
      Money transactionAmount = getRefundTransactionAmount(noOfRefunds);
      validateAmount(transactionAmount);
      String totalRefundAmount =
         requestParameterMap.get(PaymentConstants.PAYMENT + PaymentConstants.TOTAL_REFUND);
      if (StringUtils.isNotBlank(totalRefundAmount))
      {
         validateRefundAmount(totalRefundAmount);
      }

      createAndProcessPayments(noOfRefunds);
      for (PaymentTransaction paymentTransaction : paymentData.getPayment()
         .getPaymentTransactions())
      {
         if (paymentTransaction instanceof ChequePaymentTransaction)
         {
            try
            {
               notifyRefundTransaction(paymentTransaction.getTransactionAmount());
            }
            catch (EmailProcessorException e)
            {
               LogWriter.logErrorMessage(e.getMessage());
               e.printStackTrace();
            }
         }
      }
   }

   /**
    * gets the valid refund count from the request parameter map.
    * 
    * @param requestParameterMap Map<String, String>.
    * @return refundCount Total number of refunds
    */
   public int getNoOfRefunds(Map<String, String> requestParameterMap)
   {
      int count =
         Integer.parseInt(requestParameterMap.get(PaymentConstants.PAYMENT
            + PaymentConstants.NO_OF_REFUNDS));
      int refundCount = 0;
      for (int i = 0; i < count; i++)
      {
         if (StringUtils.isNotEmpty(requestParameterMap.get(PaymentConstants.PAYMENT + i
            + PaymentConstants.DATACASH_REFERENCE)))
         {
            refundCount = refundCount + 1;
         }

      }
      return refundCount;
   }

   /**
    * Sets refundTypeCode(e.g. MASTERCARD etc.) and paymentMethod into request parameter map.
    * 
    * @param noOfRefunds Total number of refunds.
    * @throws PaymentValidationException the if the refund amount will not satisfy the pattern
    *         check.
    */
   private void setSelectedRefundDetails(int noOfRefunds) throws PaymentValidationException
   {

      for (int i = 0; i < noOfRefunds; i++)
      {
         // Get the selected refund type
         String selectedRefund =
            requestParameterMap.get(PaymentConstants.PAYMENT + i + PaymentConstants.TYPE);
         // From selected refund type extract payment type code and payment method
         String refundTypeCode = StringUtils.split(selectedRefund, '|')[0];
         String refundMethod = StringUtils.split(selectedRefund, '|')[1];
         String amount =
            requestParameterMap.get(PaymentConstants.PAYMENT + i
               + PaymentConstants.TRANSACTION_AMOUNT_TEXT);
         String ccamount = requestParameterMap.get(PaymentConstants.PAYMENT + i + TRANSAMTCCTEXT);

         // card charge amount validation
         if (StringUtils.isNotBlank(ccamount))
         {
            patternCheckForAmount(ccamount, BOOKING + REFUNDAMT + INVALID);
            /*
             * if (Double.parseDouble(amount) > 0) { amount = "-" + amount; }
             */
         }
         if (StringUtils.isNotBlank(amount))
         {
            patternCheckForAmount(amount, BOOKING + REFUNDAMT + INVALID);
            /*
             * if (Double.parseDouble(amount) > 0) { amount = "-" + amount; }
             */
         }
         double tempccamt = 0.0;
         double tempamt = 0.0;
         if (StringUtils.isNotBlank(ccamount))
         {
            tempccamt = Double.parseDouble(ccamount);
            // tempamt = Double.parseDouble(amount);
         }

         if (StringUtils.isNotBlank(amount))
         {
            tempamt = Double.parseDouble(amount);
         }

         double tempamtfinal = tempccamt + tempamt;
         String totalamout = Double.toString(tempamtfinal);

         // VI or Cheque
         requestParameterMap.put(PaymentConstants.PAYMENT + i + PaymentConstants.PAYMENT_TYPE_CODE,
            refundTypeCode);
         // Cheque or CardRefund
         requestParameterMap.put(PaymentConstants.PAYMENT + i + PaymentConstants.PAYMENT_METHOD,
            refundMethod);

         // Set transaction amount
         requestParameterMap.put(
            PaymentConstants.PAYMENT + i + PaymentConstants.TRANSACTION_AMOUNT, totalamout);

         // set the non payment data with the refund keys required for dot net client
         addNonPaymentDataRefundParams(amount, refundMethod, i);
         addNonPaymentDataRefundCardChargeParams(ccamount, refundMethod, i);
      }
      bookingComponent.getNonPaymentData().put(PaymentConstants.REFUND + "count",
         String.valueOf(noOfRefunds - 1));
   }

   /**
    * Method to update refund keys and update them in non payment data map of bookingComponent
    * 
    * @param amount the amount associated with amount.
    * @param refundMethod to differentiate the type of refund method.
    * 
    */
   private void addNonPaymentDataRefundParams(String amount, String refundMethod, int index)
   {
      bookingComponent.getNonPaymentData().put(
         PaymentConstants.REFUND + index + PaymentConstants.TRANSACTION_AMOUNT, amount);
      if (StringUtils.equalsIgnoreCase(refundMethod, PaymentConstants.CHEQUE))
      {
         if (StringUtils.isNotBlank(amount))
         {
            bookingComponent.getNonPaymentData().put(PaymentConstants.REFUND_BY_CHEQUE_AMOUNT,
               amount);
            bookingComponent.getNonPaymentData()
               .put(PaymentConstants.REFUND_CHEQUE_CHECKED, "true");
         }
         else
         {
            bookingComponent.getNonPaymentData().put(PaymentConstants.REFUND_BY_CHEQUE_AMOUNT,
               amount);
            bookingComponent.getNonPaymentData().put(PaymentConstants.REFUND_CHEQUE_CHECKED,
               "false");

         }
      }
   }

   /**
    * Method to update refund keys and update them in non payment data map of bookingComponent
    * 
    * @param amount the amount associated with amount.
    * @param refundMethod to differentiate the type of refund method.
    * 
    */
   private void addNonPaymentDataRefundCardChargeParams(String amount, String refundMethod,
      int index)
   {
      if (amount != null)
      {
         bookingComponent.getNonPaymentData().put(
            PaymentConstants.REFUND + index + REFUNDED_CARDCHARGE, amount);
      }
   }

   /**
    * Method to check whether amount complies with allowed pattern. Exception will be thrown if
    * amount is not within the allowed pattern.
    * 
    * @param amountToPay the amount associated with amountToPay field.
    * @param messageKey to differentiate show particular error message for the amount.
    * 
    * @throws PaymentValidationException if the client side entered amount, does not match with the
    *         server side calculated amount.
    */
   private void patternCheckForAmount(String amountToPay, String messageKey)
      throws PaymentValidationException
   {
      Matcher amountToPayMatcher = AMOUNT_PATTERN.matcher(String.valueOf(amountToPay));

      if (!amountToPayMatcher.matches())
      {
         populateException(messageKey);
      }
   }

   /**
    * This method gets the message for a particular key and logs the error message and throws the
    * PaymentValidationException.
    * 
    * @param messageKey the message key.
    * 
    * @throws PaymentValidationException with appropriate message.
    */
   private void populateException(String messageKey) throws PaymentValidationException
   {
      String message = PropertyResource.getProperty(messageKey, MESSAGES_PROPERTY);
      LogWriter.logErrorMessage(message);
      String fieldName = "AmountToPay";
      if (bookingComponent.getBookingData().getBookingFlowDetails().getPaymentFlow()
         .equals(PaymentFlow.REFUND))
      {
         fieldName = "Refund Amount";
      }
      throw new PaymentValidationException(messageKey, fieldName);
   }

   /**
    * This method will return the transaction amount for refunds.
    * 
    * @param totalTransactions the number of refund transactions.
    * 
    * @throws PaymentValidationException the if the refund amount will not satisfy the pattern
    *         check.
    * 
    * @return Money the transaction amount.
    */
   private Money getRefundTransactionAmount(Integer totalTransactions)
      throws PaymentValidationException
   {
      BigDecimal totalTransactionAmount = BigDecimal.ZERO;
      for (int i = 0; i < totalTransactions; i++)
      {
         String amount =
            requestParameterMap.get(PaymentConstants.PAYMENT + i
               + PaymentConstants.TRANSACTION_AMOUNT_TEXT);
         if (StringUtils.isNotBlank(amount))
         {
            patternCheckForAmount(amount, BOOKING + REFUNDAMT + INVALID);
            totalTransactionAmount = totalTransactionAmount.add(new BigDecimal(amount));
         }
      }
      return new Money(totalTransactionAmount.setScale(2, BigDecimal.ROUND_HALF_UP),
         bookingComponent.getTotalAmount().getCurrency());
   }

   /**
    * This method is used to validate the Amount depending on:if it is Refund the transaction amount
    * should be equal to the calculated payable amount. It checks for the above condition and if the
    * condition is not satisfied then a PostPaymentProcessorException is thrown.
    * 
    * @param amount the amount paid.
    * 
    * @throws PostPaymentProcessorException if validation fails.
    */
   private void validateAmount(Money amount) throws PostPaymentProcessorException
   {
      if (Math.abs(amount.getAmount().doubleValue()) != Math.abs(bookingInfo
         .getCalculatedPayableAmount().getAmount().doubleValue()))
      {
         populateException();
      }
   }

   /**
    * This method logs and throws <code>PostPaymentProcessorException</code>.
    * 
    * @throws PostPaymentProcessorException after logging the exception message.
    */
   private void populateException() throws PostPaymentProcessorException
   {
      LogWriter.logErrorMessage(PropertyResource.getProperty(ERROR_MESSAGE_KEY, MESSAGES_PROPERTY));
      throw new PostPaymentProcessorException(ERROR_MESSAGE_KEY);
   }

   /**
    * This method is used to validate if the total refund amount calculated at the client side is
    * equal to the calculated payable amount.It checks for the above condition and if the condition
    * is not satisfied then a PostPaymentProcessorException is thrown.
    * 
    * @param amount the amount paid.
    * 
    * @throws PostPaymentProcessorException if validation fails.
    */
   private void validateRefundAmount(String amount) throws PostPaymentProcessorException
   {

      if (bookingComponent.getNonPaymentData().get("is_admin_user") != null
         && bookingComponent.getNonPaymentData().get("is_admin_user").equals("true"))
      {
         // validateRefundCardChargeAmount(noOfRefunds);

         validateTotalAmount(Double.parseDouble(amount));
      }
      else
      {
         validateExactAmount(amount);
      }

      // if (Double.parseDouble(amount) != bookingInfo.getCalculatedPayableAmount().getAmount()
      // .doubleValue())
      // {
      // populateException();
      // }
   }

   /**
    * This method validates exact amount for non admin users.
    * 
    * @param amount total amount.
    * @throws PostPaymentProcessorException
    */
   private void validateExactAmount(String amount) throws PostPaymentProcessorException
   {
      if (Double.parseDouble(amount) != bookingInfo.getCalculatedPayableAmount().getAmount()
         .doubleValue())
      {
         populateException();
      }
   }

   /**
    * This method is for validating total amount and the amount entered by user with card charges.
    * 
    * @param amount the actual amount.
    * @throws PostPaymentProcessorException
    */
   private void validateTotalAmount(double amount) throws PostPaymentProcessorException
   {
      int noOfRefunds =
         Integer.parseInt(requestParameterMap.get(PaymentConstants.PAYMENT
            + PaymentConstants.NO_OF_REFUNDS));

      double totalrefundcc = 0.0;

      for (int i = 0; i < noOfRefunds; i++)
      {
         String totCCamt =
            bookingComponent.getNonPaymentData().get(PaymentConstants.REFUND + i + "_cardCharge");

         if (StringUtils.isNotBlank(totCCamt))
         {
            totalrefundcc += Double.parseDouble(totCCamt);
         }
      }

      double totamt =
         totalrefundcc + bookingInfo.getCalculatedPayableAmount().getAmount().doubleValue();

      if (amount > totamt)
      {
         populateException();
      }
   }

   /**
    * This method is used to validate if the total refund card charge amount calculated at the
    * client side is equal to the card charge amount which was charged earlier .It checks for the
    * above condition and if the condition is not satisfied then a PostPaymentProcessorException is
    * thrown.
    * 
    * @param noOfRefunds the noOfRefunds.
    * 
    * @throws PostPaymentProcessorException if validation fails.
    */
   private void validateRefundCardChargeAmount(int noOfRefunds)
      throws PostPaymentProcessorException
   {

      for (int i = 0; i < noOfRefunds; i++)
      {
         String ccAmountText =
            requestParameterMap.get(PaymentConstants.PAYMENT + i + TRANSAMTCCTEXT);

         String totCCamt =
            bookingComponent.getNonPaymentData().get(PaymentConstants.REFUND + i + "_cardCharge");

         if (StringUtils.isNotBlank(ccAmountText) && StringUtils.isNotBlank(totCCamt))
         {

            if (Double.parseDouble(ccAmountText) > Double.parseDouble(totCCamt))
            {
               populateException();
            }
         }
      }
   }

   /**
    * This method calculates total refund card charge entered by the user .
    * 
    * @param noOfRefunds noOfRefunds.
    * @return totCCAmt total cc amount.
    * @throws PostPaymentProcessorException
    */
   private Double getRefundCardChargeAmount(int noOfRefunds) throws PostPaymentProcessorException
   {

      double totCCAmt = 0.0;
      for (int i = 0; i < noOfRefunds; i++)
      {
         String ccAmountText =
            requestParameterMap.get(PaymentConstants.PAYMENT + i + TRANSAMTCCTEXT);

         if (StringUtils.isNotBlank(ccAmountText))
         {

            totCCAmt += Double.parseDouble(ccAmountText);
         }
      }
      return totCCAmt;
   }

   /**
    * This method is responsible for creating the payments and adding the transactions to payment
    * object. Gets the amount from payment transaction object and validates it against the
    * bookingInfo object's payable amount.
    * 
    * @param contributionCount Number of transactions to be processed.
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   protected void createAndProcessPayments(final int contributionCount)
      throws PaymentValidationException, PostPaymentProcessorException
   {
      final Payment payment = new Payment();
      paymentData.setPayment(payment);
      bookingComponent.getNonPaymentData().put(BookingConstants.IS_DATACASH_SELECTED,
         String.valueOf(false));
      for (int i = 0; i < contributionCount; i++)
      {
         if (StringUtils.isNotBlank(requestParameterMap.get(PaymentConstants.PAYMENT + i
            + PaymentConstants.TRANSACTION_AMOUNT))
            && StringUtils.isNotBlank(requestParameterMap.get(PaymentConstants.PAYMENT + i
               + PaymentConstants.DATACASH_REFERENCE)))
         {
            payment.addPaymentTransaction(PaymentTransactionFactory.getPaymentTransactions(i,
               requestParameterMap, bookingInfo));
         }
      }

      BigDecimal totalAmountPaid = BigDecimal.ZERO;
      for (PaymentTransaction paymentTransaction : paymentData.getPayment()
         .getPaymentTransactions())
      {
         totalAmountPaid =
            totalAmountPaid.add(paymentTransaction.getTransactionAmount().getAmount());
      }

      if (bookingComponent.getNonPaymentData().get("is_admin_user") != null
         && bookingComponent.getNonPaymentData().get("is_admin_user").equals("true"))
      {

         int noOfRefunds =
            Integer.parseInt(requestParameterMap.get(PaymentConstants.PAYMENT
               + PaymentConstants.NO_OF_REFUNDS));

         double totalrefundcc = 0.0;

         for (int i = 0; i < noOfRefunds; i++)
         {
            String totCCamt =
               bookingComponent.getNonPaymentData()
                  .get(PaymentConstants.REFUND + i + "_cardCharge");
            if (totCCamt != null)
            {
               totalrefundcc += Double.parseDouble(totCCamt);
            }
         }

         double totamt =
            totalrefundcc + bookingInfo.getCalculatedPayableAmount().getAmount().doubleValue();

         if (totalAmountPaid.doubleValue() > totamt)
         {
            LogWriter.logErrorMessage(PropertyResource.getProperty(ERROR_MESSAGE_KEY,
               MESSAGES_PROPERTY));
            throw new PostPaymentProcessorException(ERROR_MESSAGE_KEY);
         }
      }
      else
      {
         if (totalAmountPaid.doubleValue() != bookingInfo.getCalculatedPayableAmount().getAmount()
            .doubleValue())
         {
            LogWriter.logErrorMessage(PropertyResource.getProperty(ERROR_MESSAGE_KEY,
               MESSAGES_PROPERTY));
            throw new PostPaymentProcessorException(ERROR_MESSAGE_KEY);
         }

      }

   }

   /**
    * This method will notify a refund transaction through a java mail when refund cheque payment
    * type is selected.
    * 
    * @param transactionAmount the refund cheque transactionAmount.
    * 
    * @throws EmailProcessorException the emailProcessorException.
    */
   public void notifyRefundTransaction(Money transactionAmount) throws EmailProcessorException
   {
      try
      {
         ShorexEmailProcessor emailProcessor = new ShorexEmailProcessor();
         emailProcessor.populateEmailContent(transactionAmount, bookingComponent);

      }
      catch (EmailProcessorException ex)
      {

         LogWriter.logErrorMessage(PropertyResource.getProperty(
            SHOREX_SEND_EMAIL_ERROR_MESSAGE_KEY, MESSAGES_PROPERTY));
      }
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   @Override
   public void preProcess() throws PostPaymentProcessorException
   {
		if ((requestParameterMap.get("payment_0_cardNumber") != null)
				&& (requestParameterMap.get("payment_0_paymenttypecode") != null)) {

			String thCCBinRange = "ThomsonCreditcard.BINRange";
			String thCCNumberSelected = null;
			String thCCConfiguration = ConfReader.getConfEntry(thCCBinRange, null);
			String cardNumber = requestParameterMap.get("payment_0_cardNumber");
			String[] thCCNumberList = thCCConfiguration.split(",");
			for (String thCCNumber : thCCNumberList) {
				if (cardNumber.startsWith(thCCNumber)) {
					if (requestParameterMap.get("payment_0_paymenttypecode").contains("TUI_MASTERCARD")) {
						thCCNumberSelected = thCCNumber;
						break;
					} else {
						throw new PostPaymentProcessorException(
								"datacash.cardtype.mismatch");
					}

				}
			}
			if (thCCNumberSelected != null) {
				if (!(requestParameterMap
						.get("payment_0_paymenttypecode")
						.contains("TUI_MASTERCARD"))) {
					throw new PostPaymentProcessorException(
							"datacash.cardtype.mismatch");
				}
			}else
			{
				if ((requestParameterMap
						.get("payment_0_paymenttypecode")
						.contains("TUI_MASTERCARD"))) {
					throw new PostPaymentProcessorException(
							"datacash.cardtype.mismatch");
				}
			}
		}
      super.preProcess();
}
}

