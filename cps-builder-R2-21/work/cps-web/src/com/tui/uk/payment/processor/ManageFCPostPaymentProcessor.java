/*
 * Copyright (C)2013 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
 */
package com.tui.uk.payment.processor;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.Map;

import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.config.ConfReader;

/**
 * This performs all the operations required for post payment for FirstChoice Retrieve & Pay
 * application.
 *
 * @author pushparaja.g
 *
 */
public class ManageFCPostPaymentProcessor extends FirstChoicePostPaymentProcessor
{
   /** The DEPOSIT TYPE. */
   private static final Object DEPOSIT = "depositType";

   // CHECKSTYLE:OFF
   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public ManageFCPostPaymentProcessor(PaymentData paymentData,
                                       Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
   {
      validateGiftCard();
      ManageFCValidationErrors errors = new ManageFCValidationErrors();
      BigDecimal fullcost = null;
      fullcost = bookingComponent.getPayableAmount().getAmount();

     
      if (requestParameterMap.get(DEPOSIT).equalsIgnoreCase("PART_PAYMENT"))
      {
    	  if(((requestParameterMap.get("part") == null)) || (requestParameterMap.get("part").equals("") )){
   	       throw new PostPaymentProcessorException("nonpaymentdatavalidation.managefc.amount");
   	      }
         for (DepositComponent depositComponent : bookingComponent.getDepositComponents())
         {
            if (depositComponent.getDepositType().equalsIgnoreCase("PART_PAYMENT"))
            {
               BigDecimal PART_PAYMENT = new BigDecimal(requestParameterMap.get("part"));
               Currency GBP = Currency.getInstance(Locale.UK);
               depositComponent.setDepositAmount(new Money(PART_PAYMENT, GBP));
               if (PART_PAYMENT.compareTo(BigDecimal.ZERO) == -1)
               {
                  throw new PostPaymentProcessorException(
                     "nonpaymentdatavalidation.managefc.amount");
               }
               if (fullcost.compareTo(PART_PAYMENT) == -1)
               {
                  throw new PostPaymentProcessorException(
                     "nonpaymentdatavalidation.managefc.amount");
               }
            
            }

         }
      }

      if (!(errors.patternCheck("firstName", requestParameterMap.get("firstName"))))
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.managefc.firstName");
      }

      if (!(errors.patternCheck("surName", requestParameterMap.get("surName"))))
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.managefc.surName");
      }

      if (!(errors.patternCheck("streetAddress1",
         requestParameterMap.get("payment_0_street_address1"))))
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.managefc.streetAddress1");
      }

      if (requestParameterMap.get("payment_0_street_address2") != "")
      {
         if (!(errors.patternCheck("streetAddress2",
            requestParameterMap.get("payment_0_street_address2"))))
         {
            throw new PostPaymentProcessorException(
               "nonpaymentdatavalidation.managefc.streetAddress2");
         }
      }
      if (!(errors.patternCheck("town", requestParameterMap.get("payment_0_street_address3"))))
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.managefc.town");
      }
      if (!(errors.patternCheck("postcode", requestParameterMap.get("payment_0_postCode"))))
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.managefc.postCode");
      }
      if (requestParameterMap.get("payment_0_selectedCountryCode") == "")
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.managefc.country");
      }

      if (requestParameterMap.get("payment_0_street_address4") == "")
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.managefc.country");
      }

      if (requestParameterMap.get("title") == "")
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.managefc.title");
      }

      if(requestParameterMap.get("payment_0_type") != null
    		  && requestParameterMap.get("payment_0_type").contains("TUI_MASTERCARD")){

    	  String thCCBinRange = "ThomsonCreditcard.BINRange";
          String thCCNumberSelected = null;
          String thCCConfiguration = ConfReader.getConfEntry(thCCBinRange, null);
          String cardNumber = requestParameterMap.get("payment_0_cardNumber");
          String[] thCCNumberList = thCCConfiguration.split(",");

             for (String thCCNumber : thCCNumberList) {
                   if (cardNumber.startsWith(thCCNumber)) {
                         thCCNumberSelected = thCCNumber;
                         break;
                   }
             }
             if (thCCNumberSelected != null) {
                   if (!(requestParameterMap.get("payment_0_type")
                               .contains("TUI_MASTERCARD"))) {
                         throw new PostPaymentProcessorException(
                                     "datacash.cardtype.mismatch");
                   }
             }
         }

      super.preProcess();
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      String depositType = requestParameterMap.get(BookingConstants.DEPOSIT_TYPE).trim();
      if (depositType.equalsIgnoreCase("fullcost"))
      {

         bookingInfo.setCalculatedTotalAmount(bookingComponent.getPayableAmount());
      }
      super.process();
   }

}