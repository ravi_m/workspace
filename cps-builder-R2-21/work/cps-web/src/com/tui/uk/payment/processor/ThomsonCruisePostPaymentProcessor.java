/*
 * Copyright (C)2009 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 * 
 * $RCSfile: ThomsonCruisePostPaymentProcessor.java,v $
 * 
 * $Revision: 1.0 $
 * 
 * $Date: 2008-07-23 10:07:06 $
 * 
 * $Author: arpan.k $
 * 
 * $Log: $
 */
package com.tui.uk.payment.processor;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.domain.PaymentTransactionFactory;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.validation.constants.ValidationConstants;

/**
 * This performs all the operations required for post payment for Thomson application.
 *
 * @author sindhushree.g
 *
 */
public class ThomsonCruisePostPaymentProcessor extends ThomsonPostPaymentProcessor
{

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public ThomsonCruisePostPaymentProcessor(PaymentData paymentData,
                                            Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
   {
      super.preProcess();
      ThomsonCruiseValidationErrors tcErrors = new ThomsonCruiseValidationErrors();
      // Server side validation for county irrespective of 3D secure enabled or disabled.
      validateCounty(tcErrors);
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      super.process();
   }

   /**
    * This method is responsible for validating the county field if available.
    * 
    * @param errors the ThomsonCruiseValidationErrors object.
    * 
    * @throws PostPaymentProcessorException if county validation fails.
    */
   public void validateCounty(ThomsonCruiseValidationErrors errors)
      throws PostPaymentProcessorException
   {
      if (errors != null)
      {
         String countyVal = null;
         String cardHolderCountyVal = null;
         String countyErrorMsg = null;

         if (requestParameterMap != null)
         {
            countyVal = (String) requestParameterMap.get("county");
            cardHolderCountyVal = (String) requestParameterMap.get("payment_0_street_address4");
         }

         if ((bookingComponent != null) && (bookingComponent.getClientApplication() != null))
         {
            countyErrorMsg =
               ValidationConstants.NON_PAYMENT_DATA_VALIDATION
                  + bookingComponent.getClientApplication().getClientApplicationName() + ".county";
         }
         else
         {
            countyErrorMsg = ValidationConstants.NON_PAYMENT_DATA_VALIDATION + "default.county";
         }

         if (countyVal != null && !StringUtils.isEmpty(countyVal))
         {
            if (!(errors.patternCheck("county", countyVal)))
            {
               if (countyErrorMsg != null)
               {
                  throw new PostPaymentProcessorException(countyErrorMsg);
               }
               else
               {
                  throw new PostPaymentProcessorException();
               }
            }
         }
         if (cardHolderCountyVal != null && !StringUtils.isEmpty(cardHolderCountyVal))
         {
            if (!(errors.patternCheck("payment_0_street_address4", cardHolderCountyVal)))
            {
               if (countyErrorMsg != null)
               {
                  throw new PostPaymentProcessorException(countyErrorMsg);
               }
               else
               {
                  throw new PostPaymentProcessorException();
               }
            }
         }
      }
   }
}
