/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ThomsonBYOPostPaymentProcessor.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-07-23 10:07:06 $
 *
 * $Author: arpan.k $
 *
 * $Log: $
*/
package com.tui.uk.payment.processor;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.ConfReader;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.exception.ValidationException;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.domain.PaymentTransactionFactory;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.validation.validator.NonPaymentDataValidator;
import com.tui.uk.validation.validator.Validator;

/**
 * This performs all the operations required for post payment for Thomson application.
 *
 * @author sindhushree.g
 *
 */
public class ThomsonBYOPostPaymentProcessor extends ThomsonPostPaymentProcessor
{

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public ThomsonBYOPostPaymentProcessor(PaymentData paymentData, Map<String, String[]>
      requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page.
    * This may include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
   {
      super.preProcess();
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      super.process();
   }
   
   /**
    * This method is responsible for non payment data validation.It will iterate on
    * nonPaymentDataPanelMap that contain key as a data panel name and value as
    * a field value.
    * @throws PostPaymentProcessorException if validation fails
    */
   @Override
   protected void validate() throws PostPaymentProcessorException
   {
      // if you pass true, this method will throw exception if any
      // validation fail.
      // if you pass false, this method will give error map if validation
      // fails.
      try
      {
            Validator validator =
                       new NonPaymentDataValidator(bookingComponent.getClientApplication());
            Map < String , String > errorMap  = validator.validate(
                    getNonPaymentDataMapForValidate(),
                    !ConfReader.getBooleanEntry(bookingComponent.getClientApplication()
                            .getClientApplicationName() + ".isErrorAccumulated", false));
            if (!errorMap.isEmpty())
            {
               processAccumulatedErrors(errorMap);
            }
      }
      catch (ValidationException ve)
      {
         // The below coding is done so that in case individual errors are shot up from server side
         // then the current display mechanism in jsp doesn't need to change (ie it can still listen
         // to the bookinginfo.errorfields object which will now contain only one field instead of 
         // the aggregated list.)
         bookingInfo.setErrorFields(StringUtils.substringAfterLast(ve.getMessage(), "."));
         throw new PostPaymentProcessorException(ve.getMessage(), ve);
      }
   }
   
   /**
    * This method accumulates all the errors and throws it in the 
    * form of NonPaymentValidationException.
    * @param errorMap the errorMap holding the accumulated errors.
    * 
    * @throws PostPaymentProcessorException.
    */
   public void processAccumulatedErrors(Map<String, String> errorMap)
   throws PostPaymentProcessorException
   {
      StringBuffer errorFieldAccumulator = new StringBuffer();
      for (String key : errorMap.keySet())
      {
         errorFieldAccumulator.append(key).append("|");
      }
      bookingInfo.setErrorFields(errorFieldAccumulator.toString());
      //throw new NonPaymentDataValidationException(errorFieldAccumulator.toString(), "");
      
      // Putting the first error message into the PPPE because 
      // 1. Empty message in PPPE causes havoc in the commonpaymentservlet
      // 2. If the BYO (old app) by chance sets the accumulation of errors to true(which is 
      // wrong behaviour) even then only the first message will be displayed.(Since old page uses
      // bookingcomponent.errorMessage to display errors instead of bookingInfo.errorFields)
      // If in future old BYO app needs aggregated errors, then its already available in 
      // bookingInfo.errorFields
      throw new PostPaymentProcessorException(errorMap.get(errorMap.keySet().toArray()[0]));
   }
}
