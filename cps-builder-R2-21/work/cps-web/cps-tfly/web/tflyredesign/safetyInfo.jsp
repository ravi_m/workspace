<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>

<!--safety info-->
           <div class="detailsPanel" id="fareDetailsPanel">
             <div class="top">
               <div class="right"></div>
             </div>
             <div class="body">
               <div class="right">
                 <div class="content">
                   <div class="section">
                     <h2 id="fareRules"><span>Baggage check-in safety information</span></h2>
                     <div class="sectionContent">

                         <p> For safety reasons, you can't bring the following dangerous articles or substances in either your checked-in baggage <br>or hand baggage:</p>	<br>
                         <p id="dangerous_items" style="margin-left: -26px;"><img alt="dangerous items list" src="/cms-cps/tflyredesign/images/TH-dangerousGoods.png" style=""></p><br>
                         <p>You can carry batteries as hand baggage, either in their original packaging or a protective case.You'll need to make sure they're protected against contact with other metal items and not kept loose in your bag, so they don't short-circuit. Equipment containing correctly-installed batteries can be packed in your checked-in baggage.</p><br>

                     </div>
                   </div>
                 </div>
               </div>
             </div>
             <div class="bottom">
               <div class="right"></div>
             </div>
           </div>
           <!--safety info-->



