<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />

<div class="foot line">
   <div class="unit size3of4">
      <ul class="horizontal navFoot contentBox firstNav">
         <li><a rel="nofollow" title="Package holidays" href="http://www.thomson.co.uk/package-holidays.html">Package holidays</a></li>
         <li><a rel="nofollow" title="Flights" href="http://flights.thomson.co.uk/en/index.html">Flights</a></li>
         <li><a rel="nofollow" title="Hotels" href="http://www.thomson.co.uk/hotels.html">Hotels</a></li>
         <li><a rel="nofollow" title="Cruises" href="http://www.thomson.co.uk/cruise.html">Cruises</a></li>

         <li><a rel="nofollow" title="Villas" href="http://www.thomson.co.uk/villas.html">Villas</a></li>
         <li><a rel="nofollow" title="Deals" href="http://www.thomson.co.uk/deals.html">Deals</a></li>
         <li><a rel="nofollow" title="Holiday extras" href="http://www.thomson.co.uk/holiday-extras.html">Holiday Extras</a></li>
         <li class="last"><a rel="nofollow" title="Holiday destinations" href="http://www.thomson.co.uk/holiday-destinations.html">Holiday Destinations</a></li>
      </ul>
      <ul class="horizontal navFoot contentBox">
         <li><a rel="nofollow" title="About Thomson" href="http://www.thomson.co.uk/editorial/legal/about-thomson.html">About Thomson</a></li>
         <li><a rel="nofollow" title="Press" href="http://www.thomson.co.uk/editorial/press-centre/news-releases.html">Press</a></li>
         <li><a rel="nofollow" title="Travel Jobs" href="http://www.tuitraveljobs.co.uk/">Travel Jobs</a></li>
         <li><a rel="nofollow" title="Affiliates" href="http://www.thomson.co.uk/partners/thomson-affiliate-programme.html">Affiliates</a></li>
         <li><a rel="nofollow" title="Advertising" href="http://www.thomson.co.uk/partners/advertise-with-us.html">Advertising</a></li>
         
         <li class="last"><a title="Register for offers" href="http://flights.thomson.co.uk/en/standalone_white_4173.html">Register for offers</a></li>
         <li class="last rss" title="RSS">&nbsp;</li>
      </ul>
      <div class="contentBox copyright">
      <jsp:useBean id='CurrDate3' class='java.util.Date'/>
         <fmt:formatDate var='currentYear' value='${CurrDate3}' pattern='yyyy'/>
         <div>� <c:out value="${currentYear}" /> TUI UK</div>
         <ul class="horizontal navFoot cardCharge">
            <li><a rel="nofollow" title="Terms &amp; Conditions" href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html">Terms & Conditions</a></li>
            <li><a rel="nofollow" title="Privacy &amp; Cookies Policy" href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html">Privacy & Cookies Policy</a></li>
             <li><a rel="nofollow" title="TUI Travel plc" href="http://www.tuitravelplc.com">TUI Travel plc</a></li>
           <li class="last"><a rel="nofollow" title="Credit Card Fees" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">Credit Card Fees</a></li>
         </ul>
         <div>We accept:</div>
         <ul class="ccLogos horizontal">
            <li class="visa"><span class="hide">Visa Logo</span></li>
            <li class="delta"><span class="hide">Delta Logo</span></li>
            <li class="mastercard"><span class="hide">MasterCard Logo</span></li>
            <li class="maestro"><span class="hide">Maestro Logo</span></li>
            <li class="solo"><span class="hide">Solo Logo</span></li>
         </ul>
      </div>
   </div>
   <div class="unit size1of4 lastUnit">
      <div class="hereToHelp">
         <ul>
            <li><a onclick="window.open('http://www.thomson.co.uk/shopfinder/shop-finder.html','name','height=745,width=820,toolbar=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no'); return false;" rel="nofollow" target="_blank" title="Shop finder" href="http://www.thomson.co.uk/shopfinder/shop-finder.html">Shop finder</a></li>
            <li><a rel="nofollow" title="Ask us a question" target="_blank" href="http://www.thomson.co.uk/editorial/faqs/flights/flight-faqs.html">Ask us a question</a></li>
            <li><a rel="nofollow" target="_blank" href="http://www.thomson.co.uk/editorial/legal/contact-us-flights.html">Contact us</a></li>
         </ul>
         <address>0871 231 4787</address>
         <p>Calls cost 10p per minute plus network extras</p>
      </div>
      <ul class="horizontal partner">
         <li><a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&amp;dn=www.thomson.co.uk&amp;lang=en" title="This Web site has chosen one or more VeriSign SSL Certificate or online payment solutions to improve the security of e-commerce and other confidential communication" class="verisign" target="_blank"><span class="hide">Verisign Logo</span></a></li> 
   <!--  <li><a href="http://www.abta.com/find-a-holiday/member-search/5736" title="Verify ABTA membership - ABTA Number V5126" class="abta" target="_blank"><span class="hide">The ABTA logo</span>ABTA</a></li>
   --> </ul>
   </div>
</div>
<!-- Modified to remove ensighten from footer -->
