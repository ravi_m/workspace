<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />

<div class="head">
   <div class="contentBox">
      <a href="http://www.thomson.co.uk" class="logo" title="Thomson Logo"><span class="hide">Thomson</span></a>
      <ul class="navBooking horizontal supersleight">
          <li class="package-holidays"><a href="http://www.thomson.co.uk/package-holidays.html">Package holidays</a></li>
          <li class="flights current"><a href="http://flights.thomson.co.uk/en/index.html">Flights</a></li>
          <li class="hotels"><a href="http://www.thomson.co.uk/hotels.html">Hotels</a></li>
       
          <li class="cruises"><a href="http://www.thomson.co.uk/cruise.html">Cruises</a></li>
          <li class="villas"><a href="http://www.thomson.co.uk/villas.html">Villas</a></li>
          <li class="deals"><a href="http://www.thomson.co.uk/deals.html">Deals</a></li>
          <li class="extras"><a href="http://www.thomson.co.uk/holiday-extras.html">Extras</a></li>
          <li class="destinations"><a href="http://www.thomson.co.uk/holiday-destinations.html">Destinations</a></li>
      </ul>
   </div>
</div>