<%@include file="/common/commonTagLibs.jspf"%>
<%@ page import="com.tui.uk.domain.BookingInfo" %>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>
<%

	    com.tui.uk.domain.BookingInfo tempbooking  = (com.tui.uk.domain.BookingInfo)request.getSession().getAttribute("bookingInfo");


							   java.util.Date tempdate = new java.util.Date(tempbooking.getBookingComponent().getNonPaymentData().get("outBoundFlight_departureDate"));
			pageContext.setAttribute("deptalldate", tempdate);%>
<c:set var="custdeptdate" value="${deptalldate}" />

	   	  <fmt:formatDate var ="deptalldatefortc" type="date" value="${custdeptdate}" pattern="yyyy-MM-dd"/>
           <!--Terms-->
           <div class="detailsPanel" id="fareDetailsPanel">
             <div class="top">
               <div class="right"></div>
             </div>
             <div class="body">
               <div class="right">
                 <div class="content">
                   <div class="section">
                     <h2 id="fareRules"><span>Terms & Conditions</span></h2>
                     <div class="sectionContent">
                                  <p>
			   Please note that all members of your party require valid passports and any applicable visas before travelling. Visit the Foreign Office website at <a href="http://www.fco.gov.uk/travel" target="_blank">http://www.fco.gov.uk/travel</a> to see visa and travel advice.	  </p><br/>
			         <p>
                If you have special needs requirements on your flight, please call us on <strong>0871 231 4787</strong>  prior to booking.
				  </p>	 <br/>
				      <p>
                To view the notice summarising the liability rules applied by Community air carriers as required by Community legislation and the Montreal Convention, please view the <a href="javascript:void(0);" onclick="Popup('http://www.thomson.co.uk/editorial/legal/montreal-convention.html',795,595,'location=0,status=0,left=100,top=50,toolbar=0,menubar=0,titlebar=0,resizable=1,scrollbars=1')">
			   Air Passenger Notice.</a></p>
		<c:if test="${(bookingComponent.nonPaymentData['atol_date'])!=null }">
			   <c:set var="atoldate" value="${(bookingComponent.nonPaymentData['atol_date'])}"/>

			   <c:if test="${!(fn:contains(bookingComponent.nonPaymentData['outBoundFlight_operatingAirlineCode'],'TOM'))|| (( bookingComponent.nonPaymentData['atol_protected'] != null ) &&( bookingComponent.nonPaymentData['atol_protected'] ))}">
				  <fmt:setTimeZone value="Europe/London" scope="session"/>
						<c:set var="nowOct" value="${atoldate} "/>
						<fmt:parseDate var="nowOct" value="${nowOct}" type="DATE" pattern="dd MMM yyyy"/>
							   <jsp:useBean id="now" class="java.util.Date" />
							   <fmt:formatDate value="${now}" var="now" pattern="dd MMM yyyy" />
							    <fmt:parseDate var="now" value="${now}" type="DATE" pattern="dd MMM yyyy"/>
							<c:if test="${ now ge nowOct}">
								  <br/>
								  <p>This booking is authorised under the ATOL licence of TUI UK Limited, ATOL 2524, and is protected under the ATOL scheme, as set out in the ATOL certificate to be supplied</p>
				            </c:if>
					</c:if>
					</c:if>

			     </p>	  <br/>
                    <p><input class="checkbox" type="checkbox" name="termAndCondition" id="termAndCondition" required="true" requiredError=">Terms and conditions is required." />
               <input id="tourOperatorTermsAccepted"  type="hidden" name="tourOperatorTermsAccepted">
			   I confirm that I have read and accept the   <a	href="javascript:void(0);"
			   onclick="window.open('http://www.thomson.co.uk/thomson/page/tandc/tandc.page?depDate=<c:out value='${deptalldatefortc}'/>&accommInvSys=TPP&flightInvSys=TPP&lang=en','conditions','width=570,height=550,scrollbars');">Booking Conditions</a> ,  <a href="javascript:void(0);" onclick="Popup('http://www.thomson.co.uk/editorial/legal/flight-conditions-of-carriage.html',795,595,'scrollbars=yes,resizable=yes');" >
			Conditions of Carriage  </a> and <a href="javascript:void(0);" onclick="Popup('https://www.thomson.co.uk/editorial/legal/privacy-policy.html',795,595,'scrollbars=yes,resizable=yes');" >
			Privacy Policy</a>.


			   </p>	   <br/>

						     <p>  All transactions are conducted over our secure server.</p>

                     </div>
                   </div>
                 </div>
               </div>
             </div>
             <div class="bottom">
               <div class="right"></div>
             </div>
           </div>
           <!--Terms-->