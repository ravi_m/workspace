<%@include file="/common/commonTagLibs.jspf"%>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate var ="currentyear" type="date" value="${now}" pattern="yyyy"/>
<div id="copy_foot"></div></div></div></div>
<div class="destfooter" style="width:540px;"></div>
<div id="footer">
<div id="bottomnav">
   <a href="https://flights.thomson.co.uk/en/3924.htm" target="_blank">Ask us a question / Contact us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://flights.thomson.co.uk/en/company.html">About us</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://flights.thomson.co.uk/en/privacy.html">Privacy &amp; security</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://flights.thomson.co.uk/en/terms.html">Terms &amp; conditions</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://flights.thomson.co.uk/en/company_221.html">Jobs</a>&nbsp;&nbsp;|&nbsp;&nbsp; <br/>
       &copy; ${currentyear} TUI UK
</div>
<div style="width: 19%; float: left;">
   <a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=www.thomsonfly.com&lang=en" target="_blank">
   <img src="/cms-cps/tfly/images/lgo_secure_site_blue.gif"></a>
</div>
