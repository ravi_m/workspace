									   <%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />
<c:set var="tomcatInstance" value="${param.tomcat}" />
<c:set var="token" value="${param.token}" />

<h2>
   <img id="hl_payment" src="/cms-cps/tfly/images/head_detailspay_dkblauhg.gif"
      alt="Pricing" border="0">
</h2>

<div class="control">
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="165">
        <div class="rowseparator"></div>
      </td>
      <td width="190">
        <div class="rowseparator"></div>
      </td>
      <td width="150">
        <div class="rowseparator"></div>
      </td>
    </tr>
    <tr>
      <td nowrap class="rightpadding" width="120">
        <strong>Total amount due</strong>
      </td>
      <td class="rightpadding">
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td align="left" id='symbol'>
               <b><c:out value="${bookingComponent.totalAmount.symbol}" escapeXml="false"/></b>
            </td>
            <td nowrap id='totalAmountDue'>
               <strong>
               <fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
                  minFractionDigits="2" pattern="#,##,###.##"/>
               </strong>
            </td>
          </tr>
        </table>
      </td>
      <td rowspan="5" valign="top"></td>
    </tr>
    <tr>
      <td colspan="3">
        <div class="rowseparator"></div>
      </td>
    </tr>
    <tr>
      <td nowrap class="rightpadding">Payment method*</td>
      <td nowrap colspan="2">

         <select name="payment_0_type"
            onchange="javascript:updateCardChargeForTfly(),setIssueField(this)" id="payment_type_0" required="true"
            requiredError="Please select payment method." requiredEmpty="none" style="width:245px;">
            <option selected value="none">Please select</option>
            <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">
            <option value='${cardDetails.paymentCode}'>
                          ${cardDetails.paymentDescription}
            </option>
         </c:forEach>
         </select>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <div class="rowseparator"></div>
      </td>
    </tr>
    <tr>
      <td valign="top" class="rightpadding">Name of card holder*</td>
      <td colspan="2">
      <c:set var="cardHolderName" value="paymentDetails_cardHolderName"/>
        <input type="text"
           name="payment_0_nameOnCard" id="payment_0_nameOnCard"
           maxlength="30" class="inputWhole"
           required="true" requiredError="Card holder name is required."
           value="<c:out value="${bookingComponent.nonPaymentData[cardHolderName]}" />"
           validationTypeError="Card holder name is invalid." style="width: 183px;">
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <div class="rowseparator"></div>
      </td>
    </tr>
    <tr>
      <td valign="top" class="rightpadding">Card number*</td>
      <td colspan="2">
        <input name="payment_0_cardNumber" type="text"
           id="payment_cardNumber" class="inputWhole" required="true"
           requiredError="Card number is required." validationTypeError="Card number is invalid."
           maxLengthError="Card number is invalid. Too short?" style="width: 183px;" maxlength="20"
           minLengthError="Card number is invalid. Too long?" validationType="Mod10" autocomplete="off">
      </td>
    </tr>

   <tr>
      <td colspan="2">
        <div class="rowseparator"></div>
      </td>
    </tr>

  <tr id="issuerequiredlabel"  style="display:none">
      <td>Issue Number</td>
      <td id="issuerequiredfield" >
        <input name="payment_0_issueNumber" 	 id="payment_0_issueNumber"
		type="text"
           id="payment_0_issueNumber"
        style="width: 83px;" maxlength="4" >
      </td>
    </tr>

  <tr>
      <td colspan="2">
        <div class="rowseparator"></div>
      </td>
    </tr>
	<jsp:useBean id="now" class="java.util.Date" />
    <fmt:formatDate var ="expiryMonthSelected1" type="date" value="${now}" pattern="MM"/>
    <tr>
   <td valign="top" class="rightpadding">Expiry date*</td>
      <td colspan="2">
      <select id="payment_expiryDateMonth" name="payment_0_expiryMonth"
         style="width: 85px; margin-right: 10px;" required="true" requiredError="Expiry date is required.">


         <c:forEach begin="1" end="12" varStatus="loopStatus">
            <fmt:formatNumber var="expiryMonth" value="${loopStatus.count}" type="number" minIntegerDigits="2" pattern="##"/>
            <c:choose>
               <c:when test="${expiryMonthSelected1 ==  expiryMonth}">
                  <option value="<c:out value="${expiryMonth}"/>" selected="selected"><c:out value="${expiryMonth}"/></option>
               </c:when>
               <c:otherwise>
                  <option value="<c:out value="${expiryMonth}"/>"><c:out value="${expiryMonth}" /></option>
               </c:otherwise>
            </c:choose>
         </c:forEach>
      </select>

      <select id="payment_expiryDateYear" name="payment_0_expiryYear" required="true"
         requiredError="Expiry Year is required." validationTypeError="Please select a valid year in the future." style="width: 86px;">

          <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
               <option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>
         </c:forEach>
       </select></td>
        <c:if test="${is3DSecure == true}"><td><a onclick="window.open('http://www.visaeurope.com/personal/onlineshopping/verifiedbyvisa/main.jsp','visa','width=670,height=495,resizable=yes');" href="javascript:void(0);"><img src="/cms-cps/tfly/images/flights-visa.gif"/></a></td>
      	</c:if>

       </tr>
    <tr>
      <td colspan="3">
        <div class="rowseparator"></div>
      </td>
    </tr>

    <tr>
      <td valign="top" class="rightpadding">CVV/CID code*</td>
      <td colspan="2">
        <input name="payment_0_securityCode" id="payment_0_securityCode" type="text"  autocomplete="off"
           maxlength="4" id="payment_securityCode"
           class="inputWhole" required="true" requiredError="CVV Code is a mandatory field."
           validationtype="Numeric" validationTypeError="CVC Code is invalid." minlength="3"
            maxlengtherror="CVC Code is invalid. Too Large?"  minLengthError="CVC Code is invalid. Too short?" style="width: 83px;">
         More about <a onclick="window.open('http://flights.thomson.co.uk/en/popup_cvv.html','cvc','width=570,height=495');" href="javascript:void(0);">CVV/CID Code</a>
        </td>
          <c:if test="${is3DSecure == true}">
        <td>
		<a onclick=
	"window.open('http://www.mastercard.com/uk/personal/en/cardholderservices/securecode/index.html',
    'mastercard','width=570,height=495,resizable=yes');" href="javascript:void(0);"><img src="/cms-cps/tfly/images/flights-mastercard-secure.gif"/></a></td>
	</c:if>


    </tr>
  </table>
</div>

<%-- *******Essential fields ********************  --%>
<input type="hidden" name="payment_0_chargeAmount" id="payment_0_chargeIdAmount"  value="0" />
<input type="hidden" name="total_transamt"  id="total_transamt"
   value="<fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
                  minFractionDigits="2" pattern="#####.##"/>" />

<input type="hidden" name="payment_totamtpaid"  id="payment_totamtpaid"
   value="<fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
   minFractionDigits="2" pattern="#####.##"/>" />

<%-- should carry payment method - Dcard etc  --%>
<input type="hidden" id="payment_0_paymentmethod" name="payment_0_paymentmethod"/>

<%-- no of transaction is 1  --%>
<input type="hidden"  id="payment_0_totalTrans" value="1" name="payment_totalTrans"/>

<%-- should carry cardtype like AMERICAN_EXPRESS etc  --%>
<input type="hidden" id="payment_0_paymenttypecode" value="" name="payment_0_paymenttypecode"/>

<%-- *******End Essential fields ********************  --%>
