var refundByChequeChecked = false;
var refundByChequeIndex;
/***
 *  File containing functionality specific script for the WSS.
 *
 *  It contains following
 *  a. Constants (id, class name etc.)
 *  b. Common functions (functions used by more than one functionality)
 *
 *  It contains following functionalities
 *  1. On-load functionality
 *  2. Adding events for payment fields Unobtrusively
 *  3. Updating payment page for deposit type selection
 *  4. Updating payment page for values entered in amount to pay textfield
 *  5. Updating payment page for card type selection
 *  6. Browser back functionality
 *
 */

/*---------------------------a. Constants -------------------------------------------------*/
/**
 * Object containing Class names used in Payment Page.
 */
var PaymentCssClasses =
{
  /***Class names used for payment update on payment page */
  PAYABLE_AMT_CLASS      : "calPayableAmount",
  TOTAL_AMOUNT_CLASS     :"totalAmount",
  DEPOSIT_AMOUNT_CLASS   :"depositAmount",
  AMOUNT_PAID_CLASS      :"amountPaid",

  /** Deposit radio button class names */
  DEPOSIT_RADIO_CLASS : "deposits",

  HIDE                 :"hide",
  SHOW                 :"show",

  IN_ERROR             :"inError",
  REFUND_CHECKBOX_CLASS:"confirm",
  REFUND_TEXTCLASS     :"refundAmount",
  TOTAL_CARD_CHARGE_CLASS : "totalCardChargeClass"
};

/***
 * Object holding ids in payment page
 */
var PaymentPageIds =
{
  /** card type id */
  CARD_TYPE_ID              : "cardType",
  /** Amount to Pay  Id . It allows user to enter amount if pay in part radio button is selected  */
  AMOUNT_TO_PAY_ID          : "amountToPay",
  TOTAL_AMT_PAYABLE         :"totalAmountPayable",

  EXPIRY_MONTH              :"expiryMonth",
  EXPIRY_YEAR               :"expiryYear",

  CARD_NUMBER               :"cardNumber",
  CARD_NAME                 :"nameOnCard",

  SECURITY_CODE             :"cardSecurityCode",
  /** Id of span/div tag containing the text which appears next to security code field. */
  SECURITY_CODE_CAPTION     :"cardSecurityCodeLink",

  ISSUE_NO                  :"issueNumber",
  ISSUE_NO_CONTROL_GROUP    :"issueNumberControlGroup",

  PAGE_OUTCOME         : "pageOutcome",
  ERROR_LIST           : "errorList",
  ERROR_LIST_CONTAINER :"errorListContainer",

  PAGE_MESSAGE         :"pageMessage",
  ERROR_INTRO          : "errorIntro"
}

var PaymentConstants =
{
  /**Full cost deposit radio option value */
  FULL_COST           : "fullCost",

  /** Pay in Part is a deposit radio option value, to allow user to enter deposit amount in amount to pay field */
  PAY_IN_PART          : "partialDeposit",

  DEFAULT_CARD_SELECTION    : "",
  DEFAULT_SECURITY_CODE_LEN : 3,
  TOP_ERROR_MESSAGE    :"We cannot process your payment",
  ERROR_INTRO_TEXT     : "Please check the following details are correct"
}

/*-----------------*****END constants *****-----------------------------------------*/

/*----------------------b. Common functions---------------------------------------------------------*/
/**
 * Gets the currency that is currently used for the page.
 */
function getCurrency()
{
  var currency = $j("#currencyText");

  if( currency )
  {
    currency = currency.val();
  }

  return currency;
}

/**
 * Updates amounts
 */
function updateAmountsOnPaymentPage()
{
	//Update the textfields
    displayTheAmountsInTextFields(PaymentCssClasses.DEPOSIT_AMOUNT_CLASS , PaymentInfo.selectedDepositAmount );
    displayTheAmountsInTextFields(PaymentCssClasses.PAYABLE_AMT_CLASS , PaymentInfo.calculatedPayableAmount );

    //display the amounts on payment page
    displayTheAmounts(PaymentCssClasses.TOTAL_AMOUNT_CLASS , 1*PaymentInfo.totalHolidayPrice + 1*PaymentInfo.totalCardCharge );
    displayTheAmounts(PaymentCssClasses.AMOUNT_PAID_CLASS , 1*PaymentInfo.amountPaid + 1*PaymentInfo.totalCardCharge );
    displayTheAmounts(PaymentCssClasses.TOTAL_CARD_CHARGE_CLASS , 1*PaymentInfo.totalCardCharge );
}

/** Function for updating amount based on the
 ** style class and amount.
**/
function displayTheAmounts(amountClassName , amount)
{
  //Display the fields which have given class name with the amount.
   var currency = getCurrency();
   $j('.'+amountClassName).html(currency + parseFloat(amount).toFixed(2));
}

/** Function for updating amounts in text fields based on the
 **  style class and amount.
**/
function displayTheAmountsInTextFields(amountClassName , amount)
{
  //Display the fields which have given class name with the amount.
  $j('.'+amountClassName).val(roundOff(1*amount ,2));
}
/*-----------------*****END Common functions*****---------------------------------------------------*/


/*----------------------1. Onload functionality-------------------------------------------------*/
/**
 * Makes an ajax call to check whether a holiday is new or already booked
 *
 */
function initialisePaymentEvents()
{
  //If javascript is disabled following is not displayed
  amountToPayField = $j("#"+PaymentPageIds.AMOUNT_TO_PAY_ID);
  amountToPayField.val(PaymentInfo.firstDepositAmount);
  amountToPayField.attr("readOnly", "readOnly");
  $j("."+PaymentCssClasses.DEPOSIT_RADIO_CLASS+":first").attr('checked','checked');
  if (!$j("#totalAmountPayableControlGroup").hasClass("cancel"))
  {
	  $j("#totalAmountPayableControlGroup").removeClass(PaymentCssClasses.HIDE).addClass(PaymentCssClasses.SHOW);
  }
  $j("#"+PaymentPageIds.TOTAL_AMT_PAYABLE).val(PaymentInfo.firstDepositAmount);

  issueNoContainer = $j("#"+PaymentPageIds.ISSUE_NO_CONTROL_GROUP)
  if(!(issueNoContainer.hasClass("inError")))
  {
	  issueNoContainer.addClass(PaymentCssClasses.HIDE);
  }
  //Initialize payment events or backbutton functionality based on status of new holiday
  performJQueryAjaxUpdate( "ConfirmBookingServlet", null, responseCheckForNewHoliday );
}

/**
 * Call back function of checkForNewHolidayAndInitializeEvents().
 * Sets
 *
 * @param request
 * @return isNewHoliday
 */
function responseCheckForNewHoliday(request)
{
    initializeOnloadEvenets(request);
}

/**
 * Initialises:
 * - Payment Events, Low Deposit events, Promotional Code event
 * - Setup the App config object
 * -
 *
 */
function initializeOnloadEvenets(isNewHoliday)
{
    //Refund total amount field should be shown only when javascript is enabled.
	//Hence the below two lines of the code are added to remove the hide property of refund fields.
	$j("#totalRefundAmountField").removeClass(PaymentCssClasses.HIDE);
    $j("#totalRefundAmountText").removeClass(PaymentCssClasses.HIDE);

	if( isNewHoliday == "true" )
    {
	  if ($j("#paymentFlow").val() == "PAYMENT")
	  {
	  setToDefault();
	  }
      //Adding event to card type drop down.
  	  $j("#"+PaymentPageIds.CARD_TYPE_ID).change(function(){cardTypeHandler(this.value)});
      $j("."+PaymentCssClasses.DEPOSIT_RADIO_CLASS).click(function(){depositTypeHandler(this.value)});
      $j("#"+PaymentPageIds.AMOUNT_TO_PAY_ID).blur(function() { amountToPayHandler()});
	  //$j("."+PaymentCssClasses.REFUND_CHECKBOX_CLASS).click(function(){updateAmountForRefunds(this.name)});
	  //$j("."+PaymentCssClasses.REFUND_TEXTCLASS).blur(function(){checkBoxChecker(this.value, this.name)});
	  $j("."+PaymentCssClasses.REFUND_CHECKBOX_CLASS).click(function(){refundFieldClickController(this)});
	  $j("."+PaymentCssClasses.REFUND_TEXTCLASS).blur(function(){refundFieldChangeController(this)});
       PaymentInfo.depositType = $j("."+PaymentCssClasses.DEPOSIT_RADIO_CLASS+":first").val();
	   if(StringUtils.isEmpty(PaymentInfo.depositType))
		{
           PaymentInfo.depositType = "fullCost";
		}
	  //Show deposit amount  in amount to pay and total amount payable based on deposit radio selection when page loads
      updatePaymentInfo();

      displayTheAmountsInTextFields(PaymentCssClasses.DEPOSIT_AMOUNT_CLASS, PaymentInfo.selectedDepositAmount );
      displayTheAmountsInTextFields(PaymentCssClasses.PAYABLE_AMT_CLASSs, PaymentInfo.selectedDepositAmount );
    }
    else
    {
    	backButtonFunctionality();
    }
}

/**
 * Intitializes onclick event for deposit type radio button
 *
 */
//function initializeDepositRadioButtons()
//{
//   var depositRadios = $j('.'+PaymentCssClasses.DEPOSIT_RADIO_CLASS);
//   depositRadios.
//
//   for(var i=0,len = depositRadioClassNames.length ; i<len ; i++)
//   {
//      depositValue  = depositRadioClassNames[i].value;
//      depositRadio = $j(depositRadioClassNames[i].id);
//
//      depositRadio.onclick = function(event)
//      {
//        //Non-ajax: handler in cardCharges.js will be called when radio button is clicked
//    	//We need to send the selected radio button's value dynamically
//          depositTypeHandler(this.value );
//      }
//
//   }
//}

/**
 * Intitializes onchange event for card type drop down
 * @return
 */
//function initializeCardTypeDropDown()
//{
//  $j(PaymentPageIds.CARD_TYPE_ID).onchange = function( event )
//  {
//	cardTypeHandler();
//  };
//}

/**
 * Intitializes onblur event for amount to pay text field
 * @return
 */
//function initializeAmountToPayTextField()
//{
//  field = $j(PaymentPageIds.AMOUNT_TO_PAY_ID);
//  if(field)
//  {
//	field.onchange = function( event )
//    {
//      amountToPayHandler();
//    };
//  }
//}
/*-------------------------*****END Onload functionality *****-------------------*/


/*--------------------------2. Updating payment page for deposit type selection--------------------------------*/
/**
 * Function will be called from common function handleDepositSelection() when a radio button is selected
 **/
function depositTypeHandler(selectedDepositType)
{
	//Set depositValue to PaymentInfo.depositType.
	PaymentInfo.depositType = trimSpaces((selectedDepositType));

	PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
    displayTheAmountsInTextFields(PaymentCssClasses.DEPOSIT_AMOUNT_CLASS , PaymentInfo.selectedDepositAmount );

	amountTopayField = $j("#"+PaymentPageIds.AMOUNT_TO_PAY_ID);
	if(amountTopayField)
	{
	  //Refresh the field by removing any validation error.
	  //removeValidationError( PaymentPageIds.AMOUNT_TO_PAY_ID  );
	  if(PaymentInfo.depositType == PaymentConstants.PAY_IN_PART )
	  {
	    amountTopayField.val(PaymentInfo.minPayableAmount);
		//amountTopayField.val("");
		$j("#totalAmountPayable").val("");
		amountTopayField.attr("readOnly", "");
		amountToPayHandler();
		//amountTopayField.focus();
	  }
	  else
	  {
	    amountTopayField.attr("readOnly", "readOnly");
	    amountToPayHandler();
	  }
	}

}

/*--------------*****END Updating payment page for deposit type selection *****-------------------*/


/*-------------- Updating payment page for values entered in amount to pay textfield ----------------------*/
/**
*  This function will be called to update payment page based on value entered in "amount to pay" field
*/
function amountToPayHandler()
{
   var amountToPayValue = $j("#"+PaymentPageIds.AMOUNT_TO_PAY_ID).val();

   //Test whether entered amount is valid number
   if(isNaN(1*amountToPayValue))
   {
	   $j("#totalAmountPayable").val("");
   }
   //Test whether entered amount is negative
   else if(1*amountToPayValue<0)
   {
	 $j("#totalAmountPayable").val("");
   }
   else
   {
	 depositAmountsMap.put(PaymentConstants.PAY_IN_PART , amountToPayValue);
     //We have to update payment info and show card charge etc
     updatePaymentInfo();
     updateAmountsOnPaymentPage();
   }
}
/*------------*****END Updating payment page for values entered in amount to pay textfield *****--------------*/

/*--------------------------3. Updating payment page for card type selection--------------------------------*/
/**
 *  This function is invoked from common function "handleCardSelection(index)"
 *   when user selects a card type in drop down
 **/
function cardTypeHandler()
{
  PaymentInfo.selectedCardType = getCardType();

  PaymentInfo.isAMEX = (PaymentInfo.selectedCardType &&
	                         (PaymentInfo.selectedCardType).indexOf("AMERICAN_EXPRESS") != -1)? true : false;

  var cardPaymentObject = toPaymentObject( $j("#"+PaymentPageIds.CARD_TYPE_ID).val());

  //Show issue number field if card is a debit card, otherwise hide it.
  showOrHideIssueNumber( cardPaymentObject );

  //Updates card charges in deposits section.
  updateChargesForDeposits();

  // Change max length of security number field to card's security code length
  refreshSecurityCodeSection( cardPaymentObject );

  //Call amount to pay handler to payment page
  amountToPayHandler();

  //Show or hide "include credit card charge"
  if(1*PaymentInfo.totalCardCharge>0.0)
  {
    $j("#totalAmountPayableCaption").removeClass(PaymentCssClasses.HIDE);
  }
  else
  {
	  $j("#totalAmountPayableCaption").addClass(PaymentCssClasses.HIDE);
  }

  // This function changes the pay button text depending on whether 3DS is enabled for the card or not.
  changePayButton();
}

/*
 * This function changes the pay button text based on its 3DS enabled/disabled status.
 */
function changePayButton()
{
    var payButtonDescription = threeDCards.get(PaymentInfo.selectedCardType);
	if (payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup")
	{
		$j('#payNow').val('Proceed to Payment');
		$j('#payNow').attr('title','Proceed to Payment')
	}
	else
	{
		$j('#payNow').val('I want to pay now');
		$j('#payNow').attr('title','I want to pay now')
	}
}
/*
 * Updates the card charge amounts for each of the deposit types.
 */
function updateChargesForDeposits()
{
  var depositTypes = document.getElementsByClassName('deposits');
  var deposCardChargeDivs =  document.getElementsByClassName('cardCharges');
  var depositAmount = null;
  var cardCharge = null;
  for(var i=0; i<deposCardChargeDivs.length; i++)
  {
    //Deposit amount associated with the deposit radio button
    depositAmount = (depositTypes[i].value)?depositAmountsMap.get(StringUtils.trim(depositTypes[i].value)): depositAmountsMap.get("fullCost");

    //Get the cardCharge applicable for the Deposit amount
    cardCharge = calculateCardCharges(depositAmount);

    //Update deposit card charge
    deposCardChargeDivs[i].innerHTML = (parseFloat(cardCharge).toFixed(2));
  }
}

/**
 ** This function is responsible for updating the transaction
 ** amount into the total text box incase of refunds.
 **/
function calculateRefunds()
{
	    var totalRefundAmount = 0.0;
		//1. Read all text fields as an array
		var refundAmounts = document.getElementsByClassName("hiddenRefund");
		for(var i = 0, len = refundAmounts.length; i<len; i++)
		{
			//2. Read the value in each text field and add it to totalRefundAmount.
			//(Apply check if the text field is empty before this step if necessary)
			if (refundAmounts[i].value != "")
			{
				totalRefundAmount += 1 * refundAmounts[i].value;
			}
		}
		//3. Update totalRefundAmt in PaymentInfo(if necessary)
		PaymentInfo.totalRefundAmt = roundOff(totalRefundAmount,2);


		//4. Update total refund in text field
		$j("#payment_totalRefundText").val(Math.abs(PaymentInfo.totalRefundAmt));
		$j("#payment_totalRefund").val(PaymentInfo.totalRefundAmt);
	}

/**
* Event handler to be called onblur of refundField.
*/
function refundFieldChangeController(val)
{
	var chkBoxIndex = getIndexNumber(val.name);
	//1. Check whether entered amount is valid. If no, empty the text field and return from the function.
	//2. If entered amount is valid, check the check box.
	var refundAmounts = document.getElementsByClassName("refundText");

	var hiddenRefundAmounts = document.getElementsByClassName("hiddenRefund");
	var maxRefundAmountArray = $j(".maxRefundAmount");
	var checkBoxStatus = document.getElementsByClassName("confirm");
	var regex =  /^(\-)?[\d]*([\.]?[\d]*)$/;
	errorFields = [];
	clearAllValidationMessages();
	$j("#"+PaymentPageIds.PAGE_OUTCOME).addClass(PaymentCssClasses.HIDE);
	if (!isNaN(chkBoxIndex) && refundAmounts[chkBoxIndex].value != "")
	{
		if (maxRefundAmountArray[chkBoxIndex].innerHTML != "" && Math.abs(1*refundAmounts[chkBoxIndex].value) > 1*stripChars(maxRefundAmountArray[chkBoxIndex].innerHTML, $j('#currencyText').val()+String.fromCharCode(163)+''+','+' '+'�') || !regex.test(1*refundAmounts[chkBoxIndex].value))
		{
			if (checkBoxStatus[chkBoxIndex].checked)
			{
				$j('#payment_'+chkBoxIndex+'_datacashreference').attr("checked", "");
			}
			refundAmounts[chkBoxIndex].value = "";
			showRefundErrorMessageAtTop($j("#"+PaymentPageIds.PAGE_OUTCOME), PaymentConstants.TOP_ERROR_MESSAGE, chkBoxIndex);
			hiddenRefundAmounts[chkBoxIndex].value = "";
		}
		else if (maxRefundAmountArray[chkBoxIndex].innerHTML != "" && Math.abs(1*refundAmounts[chkBoxIndex].value) <= 1*stripChars(maxRefundAmountArray[chkBoxIndex].innerHTML, $j('#currencyText').val()+String.fromCharCode(163)+''+','+' '+'�'))
		{
			refundAmounts[chkBoxIndex].value = roundOff(refundAmounts[chkBoxIndex].value,2);
			updateTransAmt(refundAmounts[chkBoxIndex].value, chkBoxIndex);
			if (refundByChequeChecked)
			{
				refundByCardAmountChangeController(chkBoxIndex);
			}
		}
		else if (maxRefundAmountArray[chkBoxIndex].innerHTML == "" && $j('#payment_'+chkBoxIndex+'_datacashreference').val() == "RefundCheque")
		{
			refundAmounts[chkBoxIndex].value = roundOff(refundAmounts[chkBoxIndex].value,2);
			updateTransAmt(refundAmounts[chkBoxIndex].value, chkBoxIndex);
		}
		calculateRefunds();
	}
	else if (!isNaN(chkBoxIndex) && refundAmounts[chkBoxIndex].value == "")
	{
		if (checkBoxStatus[chkBoxIndex].checked)
		{
			$j('#payment_'+chkBoxIndex+'_datacashreference').attr("checked", "");
		}
		refundAmounts[chkBoxIndex].value = "";
		hiddenRefundAmounts[chkBoxIndex].value = "";
		calculateRefunds();
	}
}

function refundFieldClickController(val)
{
	var chkBoxIndex = getIndexNumber(val.name);
	var refundAmounts = document.getElementsByClassName("refundText");
	var hiddenRefundAmounts = document.getElementsByClassName("hiddenRefund");
	var checkBoxStatus = document.getElementsByClassName("confirm");
	if (checkBoxStatus[chkBoxIndex].checked)
	{
		//The below condition is added so that If the checkbox for "refund by cheque" is selected, the input field for that line is calculated as the total
		//refund amount, less any entered amounts for the above card payments.
		if ($j('#payment_'+chkBoxIndex+'_datacashreference').val() == "RefundCheque")
		{
		   refundByChequeChecked = true;
		   refundByChequeIndex = chkBoxIndex;
		   updateRefundChequeAmount(chkBoxIndex, val);
		}
		else
		{
		   refundAmounts[chkBoxIndex].focus();
		}
	}
	else
	{
		refundByChequeChecked = false;
		refundAmounts[chkBoxIndex].value = "";
		hiddenRefundAmounts[chkBoxIndex].value = "";
		calculateRefunds();
	}
}

/**
 ** This function is used to parse the text box name
 ** and return the index value back to the caller.
 ** @param name - name of the object.
 **/
function getIndexNumber(name)
{
	var indexArray = name.split("_");
	var count = Number(indexArray[1]);
	return count;
}

function getCardType()
{
  var selectedCardValue = $j("#"+PaymentPageIds.CARD_TYPE_ID).val();
  var selectedCardArray = selectedCardValue.split("|");
  var selectedCard = (selectedCardArray!=""&&selectedCardArray) ? selectedCardArray[0] : null;
  return selectedCard;
}

/**
 * Returns an object which represents the values contained for the parameter, cardTypeSelectionValue.
 *
 * The the value cannot be parsed then null will be returned.
 *
 * The properties are:
 * - cardType (VISA, AMERICAN_EXPRESS, etc)
 * - eNumValue (DCard, CCard, etc)
 * - securityCodeLength
 * - cardCharge
 * - hasIssueNumber (true or false)
 * - chargePercentage
 */
function toPaymentObject( cardTypeSelectionValue )
{
  var METHOD   = 0;
  var ENUM     = 1;
  var tmpArray = cardTypeSelectionValue.split( "|" );

  var cardPaymentObject = null;

  if( tmpArray.length > 1 )
  {
    cardPaymentObject                    = new Object();
    cardPaymentObject.cardType           = tmpArray[METHOD];
    cardPaymentObject.enumValue          = tmpArray[ENUM];
    cardPaymentObject.securityCodeLength = 3;
    cardPaymentObject.cardCharge         = 0.0;
    cardPaymentObject.hasIssueNumber     = false;
    cardPaymentObject.chargePercentage   = 0;

    var securityCodeLength = $j("#"+cardPaymentObject.cardType + "_securityCodeLength");
    var cardCharge         = $j("#"+cardPaymentObject.cardType + "_charges");
    var chargePercentage   = $j("#"+cardPaymentObject.cardType + "_charge" );
    var cardHasIssueNumber = $j("#"+cardPaymentObject.cardType + "_issueNo" );

    if( securityCodeLength )
    {
      cardPaymentObject.securityCodeLength = securityCodeLength.val();
    }

    if( cardCharge )
    {
      cardPaymentObject.cardCharge = cardCharge.val();
    }

    if( chargePercentage )
    {
      cardPaymentObject.chargePercentage = chargePercentage.val();
    }

    if( cardHasIssueNumber )
    {
      cardPaymentObject.hasIssueNumber = (cardHasIssueNumber.val());
    }
  }
  return cardPaymentObject;
}

/** Displays or hides issue number fields based on card type selection */
function showOrHideIssueNumber( cardPaymentObject )
{
  // Show or hide issue number field
  if( cardPaymentObject && cardPaymentObject.hasIssueNumber == "true")
  {
	 issueNoGroup =  $j("#"+PaymentPageIds.ISSUE_NO_CONTROL_GROUP);
	 issueNoGroup.removeClass(PaymentCssClasses.HIDE).addClass(PaymentCssClasses.SHOW);
	 $j("#issueNumberErrorMessage").removeClass(PaymentCssClasses.HIDE).addClass(PaymentCssClasses.SHOW);
    /*Element.removeClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.HIDE);
    Element.addClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.SHOW);
    Element.removeClassName("issueNumberErrorMessage", PaymentCssClasses.SHOW);
    Element.addClassName("issueNumberErrorMessage", PaymentCssClasses.HIDE);*/
  }
  else
  {
    $j("#"+PaymentPageIds.ISSUE_NO).val("");
	 issueNoGroup =  $j("#"+PaymentPageIds.ISSUE_NO_CONTROL_GROUP);
	 issueNoGroup.removeClass(PaymentCssClasses.SHOW).addClass(PaymentCssClasses.HIDE);
	 $j("#issueNumberErrorMessage").removeClass(PaymentCssClasses.IN_ERROR+" "+PaymentCssClasses.SHOW).addClass(PaymentCssClasses.HIDE);
    /*Element.removeClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.IN_ERROR);
    Element.addClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.HIDE);
    Element.addClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.HIDE);
    Element.removeClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.SHOW);*/
  }
}

/*** Based on card type selection
 * i. Changes maxLength attribute of security number field
 * ii. Updates security code message if applicable
 * @return
 */
function refreshSecurityCodeSection( cardPaymentObject )
{
  var securityCodeField = $j("#"+PaymentPageIds.SECURITY_CODE);

  // If the selected card has a different length security number than field allows
  if(cardPaymentObject && cardPaymentObject.securityCodeLength != securityCodeField.maxLength)
  {
	  securityCodeField.val("");
      securityCodeField.maxLength = parseInt(cardPaymentObject.securityCodeLength);
      updateSecurityMessages( cardPaymentObject.cardType, cardPaymentObject.securityCodeLength );
  }
  else
  {
  //
  updateSecurityMessages(PaymentConstants.DEFAULT_CARD_SELECTION, PaymentConstants.DEFAULT_SECURITY_CODE_LEN);
  }
}

/* Updates the security number validation and info messages based
 * on selected card's security number length */
function updateSecurityMessages(cardType, securityNumLen)
{
  var infoMsg       = "The last " + securityNumLen + " digits on the reverse of your card";
  var validationMessage = PaymentFields['cardSecurityCode'].msgNonAmex;
  // If Amex card is selected, change validation and info messages

  if( PaymentInfo.isAMEX )
  {
    infoMsg       = "The 4 digits above your account number on the front of your card";
    validationMessage = PaymentFields['cardSecurityCode'].msgAmex;
  }
  $j("#"+PaymentPageIds.SECURITY_CODE_CAPTION).html(infoMsg);
  $j("#cardSecurityCodeErrorMessage").html(validationMessage);
  //Element.update( document.getElementById(PaymentPageIds.SECURITY_CODE_CAPTION) , infoMsg );
  //Element.update( document.getElementById("cardSecurityCodeErrorMessage") , validationMessage );
}

/*-------------------------*****END Updating payment page for card type selection *****-------------------*/

/*----------------------Browser back functionality ------------------------------------------------*/
/**
 * function will be called when user clicks on the browser back button in the confirmation page
 * It makes all fields read only and changes submit button to action neutral (act as "Forward").
 */
function backButtonFunctionality()
{
  /*$j(PaymentPageIds.PAGE_OUTCOME).removeClassName(PaymentCssClasses.HIDE);
  $j(PaymentPageIds.PAGE_MESSAGE).innerHTML = "Your payment has been confirmed";
  $j(PaymentPageIds.ERROR_LIST_CONTAINER).addClassName(PaymentCssClasses.HIDE);

  makeDropDownsReadOnly(PaymentPageIds.CARD_TYPE_ID);
  makeDropDownsReadOnly(PaymentPageIds.EXPIRY_MONTH);
  makeDropDownsReadOnly(PaymentPageIds.EXPIRY_YEAR);

  $j(PaymentPageIds.CARD_NUMBER).readOnly = true;
  $j(PaymentPageIds.CARD_NAME).readOnly = true;

  $j(PaymentPageIds.SECURITY_CODE).readOnly = true;

  $j(PaymentPageIds.ISSUE_NO).readOnly = true;
  */
  if ($j("#paymentFlow").val() == "PAYMENT")
  {
  setToDefault();
  }
  hidePayNowButton();
}

/**
 *
 *
 */
function hidePayNowButton()
{
  var payNowButtonSpan = $j("#forwardButton");
  if(payNowButtonSpan)
  {
	  payNowButtonSpan.html("");
	  //payNowButtonSpan.innerHTML = '<input type="button" id="Forward" title="Forward" class="primary" alt="" value="Forward" onClick="history.forward()" />';
  }
}

/**
 * Make all drop downs read only. So user will not be able to change the drop down
 *
 * @param dropdownId
 * @return
 */
function makeDropDownsReadOnly(dropdownId)
{
  var dropdown = $j("#"+dropdownId);
  var chosen = dropdown.options.selectedIndex;
  dropdown.onchange = function()
  {
	dropdown.options.selectedIndex = chosen;
  }
  dropdown.className = "greyed";
}
/*-----------------*****END Browser back functionality *****----------------------------------------*/

/**
 *Updates essential fields when user clicks submit button.
 */
function updateEssentialFields()
{
  $j("#total_transamt").val(PaymentInfo.calculatedPayableAmount);
  var sel = document.getElementById('country');
  document.getElementById('payment_0_selectedCountry').value = sel.options[sel.selectedIndex].value;
}


/**
 * Contains field names which has to be set to default when page loads.
 *
 */
function setToDefault()
{
  //Set to default
  //Don't replace it by Jquery. Required for Chrome and Safari.
  document.getElementById(PaymentPageIds.CARD_TYPE_ID).selectedIndex=0;
  document.getElementById(PaymentPageIds.EXPIRY_MONTH).selectedIndex=0;
  document.getElementById(PaymentPageIds.EXPIRY_YEAR).selectedIndex=0;
  //Don't replace it by Jquery. Required for Chrome and Safari.
  $j("#"+PaymentPageIds.CARD_NUMBER).val("");
  $j("#"+PaymentPageIds.CARD_NAME).val("");

  $j("#"+PaymentPageIds.SECURITY_CODE).val("");

  $j("#"+PaymentPageIds.ISSUE_NO).val("");

}



/*----------------------PopUp function -----------------------------------------------------------*/
function toggleOverlay(){
  var zIndex = 100;

  $j(".genericOverlay").hide();
  $j("a.blinkyOwner").click(function(e){
    var overlay = "#" + this.id + "Overlay";
    $j(overlay).show();

    $j(overlay + ".genericOverlay").css("z-index",zIndex);
    zIndex++;
  });

  $j("a.close").click(function(){
    var overlay = this.id.replace("Close","Overlay");
    $j("#" + overlay).hide();
  });
}

//Convert links with rel external to target _blank to maintain strict compat
function targetBlank()
{
	if (!document.getElementsByTagName) return;
	var anchors = document.getElementsByTagName("a");
	for (var i=0; i<anchors.length; i++)
	{
		var anchor = anchors[i];
		if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external")
		{
			anchor.title = "The following link opens in a new window";
			var theScript = 'JavaScript:void(Popup("' + anchor.getAttribute('href') + '"));';

			anchor.setAttribute("href", theScript);

			// We don't want the target attribute as this will cause problems with the script.
			anchor.removeAttribute("target");
		}
	}
}

/*----------------------*****END PopUp function *****-------------------------------------------------*/

document.getElementsByClassName = function(clsName)
{
	var retVal = new Array();
	var elements = document.getElementsByTagName("*");
	for(var i = 0;i < elements.length;i++)
	{
		if(elements[i].className.indexOf(" ") >= 0)
		{
			var classes = elements[i].className.split(" ");
			for(var j = 0;j < classes.length;j++)
			{
				if(classes[j] == clsName)
					retVal.push(elements[i]);
			}
		}
		else if(elements[i].className == clsName)
			retVal.push(elements[i]);
	}
	return retVal;
}

function negateAmount()
{
	var refundBalanceAmt = document.getElementsByClassName("totalcalculatedPayableAmount");
	for(var i = 0; i < refundBalanceAmt.length; i++)
	{
		refundBalanceAmt[i].innerHTML = roundOff(Math.abs(1*refundBalanceAmt[i].innerHTML),2);
	}
}


function updateTransAmt(refundAmount, chkBoxIndex)
{
	$j('#payment_'+chkBoxIndex+'_datacashreference').attr("checked", "checked");
	var absoluteRefundAmount = Math.abs(refundAmount);
	if (absoluteRefundAmount != refundAmount)
	{
		$j('#payment_'+chkBoxIndex+'_transamt').val(refundAmount);
		$j('#payment_'+chkBoxIndex+'_transamtText').val(absoluteRefundAmount);
	}
	else
	{
	  $j('#payment_'+chkBoxIndex+'_transamt').val(-refundAmount);
	}
}

/* The below function updates the refund amount field automatically if the user checks the refund by cheque option*/
function updateRefundChequeAmount(chkBoxIndex, val)
{
	var totalRefundAmount = 0.0;
	//1. Read all text fields as an array
	var refundAmounts = document.getElementsByClassName("hiddenRefund");
	for(var i = 0, len = refundAmounts.length; i<len; i++)
	{
		//2. Read the value in each text field and add it to totalRefundAmount.
		//(Apply check if the text field is empty before this step if necessary)
		if (refundAmounts[i].value != "")
		{
			totalRefundAmount += 1 * refundAmounts[i].value;
		}
	}
	//The refund Amount
	var refundAmount = Math.abs(stripChars($j(".totalcalculatedPayableAmount").html(), String.fromCharCode(163)+''+','+' '+'�'));
	if (refundAmount > Math.abs(totalRefundAmount))
	{
	  $j('#payment_'+chkBoxIndex+'_transamtText').val(roundOff(refundAmount - Math.abs(totalRefundAmount),2));
	  refundFieldChangeController(val);
	}
	else
	{
	  $j('#payment_'+chkBoxIndex+'_transamtText').val("");
	  $j('#payment_'+chkBoxIndex+'_datacashreference').attr("checked", "");
	}
}

/* The below function updates the refund cheque amount field automatically if the user enters or changes the amount in the refund by card option*/
function refundByCardAmountChangeController(chkBoxIndex, val)
{
	var totalRefundAmount = 0.0;
	//1. Read all text fields as an array
	var refundAmounts = document.getElementsByClassName("hiddenRefund");
	for(var i = 0, len = refundAmounts.length - 1; i<len; i++)
	{
		//2. Read the value in each text field and add it to totalRefundAmount.Here we ignore the amount paid by cheque.
		//(Apply check if the text field is empty before this step if necessary)
		if (refundAmounts[i].value != "")
		{
			totalRefundAmount += 1 * refundAmounts[i].value;
		}
	}
	//The refund Amount
	var refundAmount = Math.abs(stripChars($j(".totalcalculatedPayableAmount").html(), String.fromCharCode(163)+''+','+' '+'�'));
    var refundByCardAmount = Math.abs(totalRefundAmount);
    var hiddenRefundAmounts = document.getElementsByClassName("hiddenRefund");

    if (refundAmount > refundByCardAmount)
    {
    	$j('#payment_'+refundByChequeIndex+'_transamtText').val(roundOff(refundAmount - refundByCardAmount,2));
    	$j('#payment_'+refundByChequeIndex+'_datacashreference').attr("checked", "checked");
    	hiddenRefundAmounts[refundByChequeIndex].value = -(roundOff(refundAmount - refundByCardAmount,2));
    }
    else
    {
       $j('#payment_'+refundByChequeIndex+'_transamtText').val("");
  	   $j('#payment_'+refundByChequeIndex+'_datacashreference').attr("checked", "");
  	   hiddenRefundAmounts[refundByChequeIndex].value = "";
    }
}

