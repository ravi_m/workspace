/**
 * formValidation file contains following functionalities
 * 1. Validation of fields once pay now button is pressed
 * 2. Highlighting field for any server side errors
 *
 */

/*-------------------------Configuration-------------------------------------------------------------*/
/**
 * Should be over-ridden by brand specific JS if they want to define brand specific configurations .
 * Acts as an interface for brand specific js to change the default functionality provided in common js.
 *
 */
var AppConfig =
{
	/** Whether error message to be shown next to a field */
	isErrorMessageApplicable : true,
	/** function to Over-ride default behavior */
	setIsErrorMessageApplicable : function(isErrorMessageAppl)
	{
		this.isErrorMessageApplicable = isErrorMessageAppl;
	}
};


/*--------------------------*****END Configuration*****----------------------------------------------*/

/*--------------------------------String Utils ------------------------------------------------------*/
/**
 * Utility functions defined for manipulating String
 *
 * Contains following functions
 * isNotEmpty:
 * isEmpty:
 * trim:
 *
 */
var StringUtils = {

  /**
   * returns true if the value passed is not empty/null/undefined
   *
   */
  isNotEmpty : function(value)
  {
	return(value!=undefined && value!=null && value.length>0 )
  },

  /**
   * returns true if the value passed is  empty/null/undefined
   *
   */
  isEmpty : function(value)
  {
	return(value==undefined || value==null || this.trim(value).length==0 )
  },

  /**
   * Trims spaces from both sides of the string
   *
   */
  trim : function(value)
  {
	return value.replace( /^\s+|\s+$/g, "" );
  },

  /**
   * returns a new String after changing first char of the input string to lower case.
   */
  firstCharToLowerCase: function(value)
  {
	if(this.isNotEmpty(value))
	{
	  value = this.trim(value);
	  firstChar = value.substring(0,1).toLowerCase();
	  otherChars = value.substring(1);
	  value = firstChar+otherChars;
	}
	return value;
  }

};
/*---------------------------*****END String Utils*****----------------------------------------------*/


var errorFields = [];

/**
 * Contains all the information for validating payment fields.
 */
var currency = getCurrency();
var PaymentFields =
{
  amountToPay       :{ validate : function()
	                   {
	                     if(!validateAmountToPay('amountToPay'))
	                     {
	                    	 errorFields.push("amountToPay");
	                     }
	                   } ,
	                   topText:"You must pay a minimum of "+currency+PaymentInfo.amountPayableToday+" and not leave a balance less than "+currency+"50",
	                   msgBlank :"You must pay a minimum of "+currency+PaymentInfo.amountPayableToday+" and not leave a balance less than "+currency+"50",
	                   msgInvalid :"You must pay a minimum of "+currency+PaymentInfo.amountPayableToday+" and not leave a balance less than "+currency+"50",
	                   anchor:" "
	                 },

  totalAmountPayable:{ validate : function(){ return true;   } ,
	                   topText:"Total Amount Payable",
	                   msgBlank :"",
	                   msgInvalid :"",
	                   anchor:""
	                 },

  cardType    :{ validate : function()
	                   {
	                	 if(!validateBlank("cardType")){
	                		 errorFields.push("cardType");
	                	  }
	                   } ,
	                   topText:"Card Type " ,
	                   msgBlank :"Please select your card type from the list.",
                       anchor:" "
	                 },

  cardNumber        :{ validate : function()
	                   {
	                	 if(!validateCardNumber("cardNumber")) {
	                		 errorFields.push("cardNumber");
	                	 }
	                   },
	                   topText:"Card Number" ,
	                   msgBlank :"Before continuing, we need your card number.",
	                   msgInvalid:"Please enter a valid card number." ,
	                   //Message Luhn check
	                   msgLuhnCheck:"It appears that your Card Type and Card Number don't match. Could you check your credit card details and try again.",
	                   regex     : /^\d+$/ ,
	                   anchor:" "
	                 },

  expiryYear        :{ validate : function(){ return true;  },
	                   topText:"",
	                   msgBlank :""
                     },

  expiryMonth       :{ validate : function()
	                   {
	                	 if(!validateExpiryDate("expiryMonth", "expiryYear")) {
	                		errorFields.push("expiryMonth");
	                	 }
	                   } ,
	                   topText:"Expiry Date " ,
	                   msgBlank :"Please enter a valid 'Expires End' date." ,
	                   msgInvalid :"You have entered an incorrect expiry date." ,
	                   anchor:" "
	                 },

  nameOnCard        :{ validate : function()
	                   { if(!genericValidation("nameOnCard"))
	                     {
	                	   errorFields.push("nameOnCard");
	                     }
	                   } ,
	                   topText:"Name On Card" ,
	                   msgBlank :"Please enter the name on the card." ,
	                   msgInvalid :"Please enter a valid name on the card." ,
	                   regex:/^[A-Za-z\ ]*$/,
	                   anchor:" "
	                 },

  issueNumber       :{ validate : function()
	                   {
	                	 if(!validateIssueNo("issueNumber"))  {
	                		 errorFields.push("issueNumber");
	                	 }
	                   } ,
	                   topText:"Issue Number" ,
	                   msgBlank :"" ,
	                   msgInvalid :"Please enter the issue number found on your card." ,
	                   regex : /^[0-9 ]*$/,
	                   anchor:" "
	                 },

  cardSecurityCode  :{ validate : function()
	                   {
	                	 if(!validateSecurityCode("cardSecurityCode")) {
	                		 errorFields.push("cardSecurityCode");
	                	 }
                       } ,
	                   topText:"Security Code" ,
	                   //Default message
	                   msgBlank :"Please enter your 3 digit security code." ,
	                   msgInvalid :"Please enter your 3 digit security code." ,
	                   //messages when a card type is selected
	                   msgNonAmex:"Please enter the last 3 digits of your card security code",
	                   msgAmex:"Please enter your 4 digit security code",
	                   regex : /^[0-9 ]*$/,
	                   anchor:" "
	                 },

   houseName        :{ validate : function()
		                   {
		                	 if(!genericValidation("houseName")) {
		                		 errorFields.push("houseName");
		                	 }
	                       } ,
		                   topText:"House Name/Number" ,
		                   //Default message
		                   msgBlank :"Please enter your House Name/Number." ,
		                   msgInvalid :"Please enter a valid House Name/Number." ,
		                   regex : /^[A-Za-z0-9 ]*$/,
		                   anchor:" "
		              },

   address           :{ validate : function()
			            {
			               if(!genericValidation("address")) {
			                  errorFields.push("address");
			               }
		                 } ,
			             topText:"Address" ,
			             //Default message
			             msgBlank :"Please enter your address." ,
			             msgInvalid :"Please enter a valid House Name/Number." ,
			             regex : /^[A-Za-z0-9 ]*$/,
			             anchor:" "
			            },

   townCity           :{ validate : function()
				         {
				                	 if(!genericValidation("townCity")) {
				                		 errorFields.push("townCity");
				                	 }
			              } ,
				          topText:"Town/City" ,
				          //Default message
				          msgBlank :"Please enter your Town/City." ,
				          msgInvalid :"Please enter a valid Town/City." ,
				          regex : /^[A-Za-z ]*$/,
				          anchor:" "
				         },

   county               :{ validate : function()
					         {
		                	 if(!genericValidation("county")) {
		                		 errorFields.push("county");
		                	 }
	                      } ,
		                  topText:"County" ,
		                  //Default message
		                  msgBlank :"Please enter your County." ,
		                  msgInvalid :"Please enter a valid County." ,
		                  regex : /^[A-Za-z ]*$/,
		                  anchor:" "
		                 },

   postcode             :{ validate: function()
		                	 {
		                	    if(!validatePostCode("postcode"))
		                	    {
		                		 errorFields.push("postcode");
		                	    }
		                	 },
			                 topText:"Postcode" ,
			                 //Default message
			                 msgBlank :"Please enter your post code." ,
			                 msgInvalid :"Please enter a valid post code." ,
			                 regex : /^[A-Za-z ]*$/,
			                 anchor:" "
		                  },

   country              :{ validate:function()
		                   {
		                	 return true;
		                   }
		                 }
};


/****************************************************************************************************/
/**
 * The starting point of client side form validation. It will be called when user clicks on "pay Now" button
 *
 * @return true if validation is successful
 */
function payNowButtonClicked()
{
	  errorFields = [];
	  clearAllValidationMessages();
	  //hide the error div
	  $j("#"+PaymentPageIds.PAGE_OUTCOME).addClass(PaymentCssClasses.HIDE);
	  if ($j("#paymentFlow").val() == "PAYMENT")
	  {
		  validatePaymentDetails();
	  }
	  else if ($j("#paymentFlow").val() == "REFUND")
	  {
		  validateRefundDetails();
	  }
	  //If validation is not successful then show warnings at the top(if applicable)
	  if(errorFields.length>0)
	  {
		showErrorMessageAtTop($j("#"+PaymentPageIds.PAGE_OUTCOME), PaymentConstants.TOP_ERROR_MESSAGE );
	    return false;
	  }
	  if (document.getElementById("confirmTandC") && !document.getElementById("confirmTandC").checked)
	  {
		  alert("Before we continue with your booking, please confirm that you agree to our \nterms and conditions.");
		  document.getElementById("confirmTandC").focus();
		  return false;
	  }
	  disablePayNowButton();

	  var sel = document.getElementById('country');
	  document.getElementById('payment_0_selectedCountry').value = sel.options[sel.selectedIndex].value;
	  return true;
}

function disablePayNowButton()
{
  var payNowButtonSpan = $j("#forwardButton");
  if(payNowButtonSpan)
  {
	  if ($j("#paymentFlow").val() == "PAYMENT")
	  {
		  var payButtonDescription = threeDCards.get(PaymentInfo.selectedCardType);
	      if (payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup")
		  {
	    	  payNowButtonSpan.html('<input type="button" value="Proceed to Payment" class="primary" title="Proceed to Payment" id="payNow">');
		  }
		  else
		  {
			  payNowButtonSpan.html('<input type="button" value="I want to pay now" class="primary" title="I want to pay now" id="payNow">');
		  }
	  }
	  else if ($j("#paymentFlow").val() == "REFUND")
	  {
		  if ($j("#bookingFlow").val() == "CANCEL")
		  {
			  payNowButtonSpan.html('<input type="button" value="Confirm refund" class="primary" title="Confirm refund" id="payNow">');
		  }
		  else
		  {
			  payNowButtonSpan.html('<input type="button" value="Continue" class="primary" title="Continue" id="payNow">');
		  }
	  }
  }
}

/**
 * Iterates over all tags in payment section. Delegates to  matching validate function in PaymentFields
 *  for input tags.
 *
 * @return
 */
function validatePaymentDetails()
{
   var paymentDetailsLists = document.getElementsByClassName("paymentOptionDetails");
   //Get all tags inside paymentDetailsList
   for (var j=0;j< paymentDetailsLists.length; j++ )
   {
	   payDetListElems = paymentDetailsLists[j].getElementsByTagName("*");
	   for(var i = 0; i< payDetListElems.length; i++)
	   {
	      elem = payDetListElems[i];
	      paymentFieldObject = PaymentFields[elem.id];
	      //We need to call validate function for user input tags only
	      switch(elem.type)
	      {
	        case "text"       :  paymentFieldObject.validate();  break;
	        case "checkbox"   :  break;
	        case "select-one" :  paymentFieldObject.validate(); break;
	      }
	   }
   }
}

function validateRefundDetails()
{
	refundFieldObject = RefundFields["payment_totalRefund"];
	refundFieldObject.validate();
}

var RefundFields =
{
  payment_totalRefund:{ validate : function()
	                   {
	                     if(!validateRefundAmt('payment_totalRefund'))
	                     {
	                    	 errorFields.push("payment_totalRefund");
	                     }
	                   } ,
					   topText:"Refund Amount" ,
	                   msgBlank :"Refund amount cannot be empty.",
	                   msgInvalid :"Calculated Refund amount is invalid.",
	                   anchor:" "
	                 }
}


/**
 * This function validates the postcode and returns true or false accordingly.
 */
function validatePostCode(postCodeID)
{
  var obj = PaymentFields[postCodeID];
  postCode= ($j("#"+postCodeID).val()).toUpperCase();

	if($j("#country").val() == "GB")
	{
		validateBlank(postCodeID);
		Pat1 = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXYU0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;

		if (!Pat1.test(postCode))
		{
			registerValidationError(obj.msgInvalid,'postcode');
			return false;
		}
	}
	return true;

}


/**
 *  Shows error message at the top with the links to different payment fields
 * @return
 */
function showErrorMessageAtTop(warningBlock , topMessage)
{
  var validationLinkList = $j("#"+PaymentPageIds.ERROR_LIST);
  $j("#"+PaymentPageIds.PAGE_MESSAGE).html(topMessage);
  $j("#"+PaymentPageIds.ERROR_INTRO).removeClass(PaymentCssClasses.HIDE);

  for(var i=0;i<errorFields.length;i++)
  {
	  if ($j("#paymentFlow").val() == "PAYMENT")
	  {
		  PaymentFieldsElem = PaymentFields[errorFields[i]];
		  var hrf='#' + PaymentFieldsElem.anchor;
		  if(PaymentFieldsElem)
		  {
		      hrf = 'javascript:formSetFocus(document.forms[0].' + errorFields[i] + ', \'#' + PaymentFieldsElem.anchor + '\')';
		  }
		  var link = '<li><a href="' + hrf + '">' + PaymentFieldsElem.topText + '</a></li>' ;
	  }
	  else
	  {
		  PaymentFieldsElem = RefundFields[errorFields[i]];
		  var link = PaymentFieldsElem.topText ;
	  }

      //listLi.appendChild( link );

	  validationLinkList.append( link);
  }
  // display the warning block
  warningBlock.removeClass(PaymentCssClasses.HIDE);


  // scroll to the top left of the page
  //setBrowserScrollingOffsets( 0, 0 );
  setTimeout("window.scrollTo(0, 0)",1);
}


/**
 *  Shows refund error message at the top with the links to different payment fields
 * @return
 */
function showRefundErrorMessageAtTop(warningBlock , topMessage, chkBoxIndex)
{
  var validationLinkList = $j("#"+PaymentPageIds.ERROR_LIST);
  $j("#"+PaymentPageIds.PAGE_MESSAGE).html(topMessage);
  $j("#"+PaymentPageIds.ERROR_INTRO).removeClass(PaymentCssClasses.HIDE);

  var obj = RefundFields['payment_totalRefund'];
  errorFields.push("payment_totalRefund");
  registerValidationError(obj.topText, 'payment_totalRefund');

  for(var i=0;i<errorFields.length;i++)
  {
	  PaymentFieldsElem = RefundFields[errorFields[i]];
	  var link = PaymentFieldsElem.topText ;

	  validationLinkList.append( link);
  }
  // display the warning block
  warningBlock.removeClass(PaymentCssClasses.HIDE);


  // scroll to the top left of the page
  //setBrowserScrollingOffsets( 0, 0 );
  setTimeout("window.scrollTo(0, 0)",1);
}

/**
 * Registering validation error if validation fails for a field. *
 * i. Make the field container highlighted.
 * ii. Show an error message shown next to the error fields
 *
 * @param message message to be displayed next error field
 * @param field field for which validation is failing
 */
function registerValidationError(message,field)
{
	$j("#"+field+"ControlGroup").addClass("inError");
  //Show error message next to error field
  if(AppConfig.isErrorMessageApplicable)
  {
    fldErrorMessage = $j("#"+field+"ErrorMessage");
    if(fldErrorMessage)
    {
	  fldErrorMessage.removeClass(PaymentCssClasses.HIDE);
	  fldErrorMessage.html(message);
	  //fldErrorMessage.style.display = "block";
    }
  }
}

/**
 *  Clears highlighting for a given payment field container
 *
 * @param field
 */
function removeValidationError(field)
{
  $j("#"+field+"ControlGroup").removeClass("inError");
}


/**
 *  Clears all validation messages on the payment page
 *  i.  clears server side exception messages
 *  ii. clears error messages in alert box at the top
 *  iii.clears highlighting from payment field containers
 *  iv. hides error messages shown for the fields
 */
function clearAllValidationMessages()
{
  // clear any existing server side exception messages
  var processingErrorsBlock = $j("#"+PaymentPageIds.PAGE_MESSAGE);
  if( processingErrorsBlock )
  {
	processingErrorsBlock.html("");
  }
  //Clear any existing client side exception messages
  removeAllChildNodes(PaymentPageIds.ERROR_LIST);

  //Clear any highlighting from payment field containers
  var containers = $j('li.controlGroup');
  for(var i=0,len = containers.length; i<len;i++)
  {
	containers.removeClass("inError");
  }

  //Hide error messages shown next to the error fields
  /*var errorMessageTags = $$('p.errorMessage');
  for(var i=0,len = errorMessageTags.length; i<len;i++)
  {
    errorMessageTags[i].addClassName(PaymentCssClasses.HIDE);
  }*/
}

/**
 * remove all a node's children
 */
function removeAllChildNodes( parentNode )
{
  // make sure our arguments are nodes, not just IDs
  /*var parentNode = $(parentNode);

  var childNodes = $(parentNode.children());

  childNodes.each( function( childNode ){
    parentNode.remove( childNode );
  } );*/
  $j("#"+parentNode).html("");
}

/**
 *
 * @param field
 * @param anchorLoc
 */
function fieldSetFocus(fieldName, anchorLoc)
{
	fieldName = StringUtils.firstCharToLowerCase(fieldName);
	formSetFocus(document.getElementById(fieldName) , anchorLoc);
}

/**
 *
 * @param field
 * @param anchorLoc
 */
function formSetFocus(field, anchorLoc)
{
  window.location.href = anchorLoc;
  field.focus();
  if(field.type == "text")
  field.select();
}


/*----------------------------------common validation functions--------------------------------------*/
/**
 *
 * @param id
 * @return
 */
function validateBlank(id)
{
  var value = $j("#"+id).val();
  if(StringUtils.isEmpty(value))
  {
	registerValidationError(PaymentFields[id].msgBlank,id);
    return false;
  }
  return true;
}

/**
 * A generic validation function.
 * It does blank check and pattern check and registers errors, if validation fails.
 *
 * @param id
 * @return
 */
function genericValidation(id)
{
   var value = $j("#"+id).val();
   var obj = PaymentFields[id];

   //Check for empty
   if(StringUtils.isEmpty(value))
   {
 	registerValidationError(obj.msgBlank,id);
     return false;
   }

   //regex pattern check
   else if(!(obj.regex).test(value))
   {
	 registerValidationError(obj.msgInvalid,id);
	 return false;
   }

   return true;
}
/*-----------------------------*****END common validation functions *****---------------------------------*/

/*----------------------------------Amount to pay/part payment validation----------------------------*/
function validateAmountToPay(amountToPay)
{
   var amountToPayValue = $j("#"+amountToPay).val();
   var obj = PaymentFields[amountToPay];
  //Test whether entered amount is valid number
   if(isNaN(1*amountToPayValue))
   {
	   registerValidationError( obj.msgInvalid , amountToPay  );
	   //$j(amountToPay).value = "";
	   return false;
   }

   //Test whether entered amount is in allowed limit
   if(PaymentInfo.depositType == PaymentConstants.PAY_IN_PART)
   {
	 //if(1*amountToPayValue>(1*PaymentInfo.maxAmtToPay-1*PaymentInfo.minAmtToPay) ||1*amountToPayValue< 1*PaymentInfo.minAmtToPay)

	   if ((1 * amountToPayValue < 1 * PaymentInfo.amountPayableToday)
				|| (((1 * PaymentInfo.maxAmtToPay - 1 * amountToPayValue) != 0) && ((1 * PaymentInfo.maxAmtToPay - 1 * amountToPayValue) < 1 * PaymentInfo.minAmtToPay)))
		{
			registerValidationError(obj.msgInvalid, amountToPay);
			// $j(amountToPay).value = "";
			return false;
		}
   }
   return true;
}
/*-------------------------------*****END Amount to pay/part payment validation*****------------------*/

/*----------------------------------START Refund fields validation----------------------------*/
function validateRefundAmt(payment_totalRefund)
{
	var obj = RefundFields[payment_totalRefund];
	var totalRefundAmount = $j(".totalRefundAmount").val();
	var refundAmount = Math.abs(stripChars($j(".totalcalculatedPayableAmount").html(), String.fromCharCode(163)+''+','+' '+'�'));
	if (1*totalRefundAmount == 0 || 1*totalRefundAmount == "")
	{
		registerValidationError(obj.msgBlank, payment_totalRefund);
		return false;
	}

	if (Math.abs(1*totalRefundAmount) != Math.abs(1*refundAmount))
	{
		registerValidationError(obj.msgInvalid, payment_totalRefund);
		return false;
	}
	return true;
}

function validateRefundCheckBox(id)
{
	var refundCbStatus = document.getElementById(id).checked;
	//alert(refundCbStatus)
}
/*-------------------------------*****END Refund fields validation*****------------------*/

/*----------------------------------Card number validation ------------------------------------------*/
/**
 * Validates card number
 *
 * @param cardNumber is the id of card number field
 * @return true if validation is successful
 */
function validateCardNumber(cardNumber)
{
  var value = $j("#"+cardNumber).val();
  var obj = PaymentFields[cardNumber];

  if(!genericValidation(cardNumber))
  {
	return false;
  }
  else if(!cardNumberLuhnCheck(value))
  {
	registerValidationError(obj.msgLuhnCheck,cardNumber);
	return false;
  }
  return true;
}

/**
 * check whether a card number is valid for its type
 *
 */
function cardNumberLuhnCheck( cardNumberVal )
{
  var numberLength = cardNumberVal.length;
  // check that the card number has an allowable number of digits
  if (!(numberLength >= 13 && numberLength <= 19)){ return false; }

  // mod 10 check
  var oddOrEven = numberLength & 1;
  var sum = 0;
  for( var count = 0; count < numberLength; count++ )
  {
    var digit = parseInt( cardNumberVal.charAt( count ) );
    if( !((count & 1) ^ oddOrEven))
    {
      digit *= 2;
      if( digit > 9 ){ digit -= 9; }
    }
    sum += digit;
  }

  var validNumber = ( (sum % 10) == 0 );

  return validNumber;
}

/*-----------------------------****END Card number validation *****-------------------------------*/

/*----------------------------------Expiry Date validation --------------------------------------*/
/**
*
* @param inputMonth
* @param inputYear
* @return true if date is valid
*/
function validateExpiryDate(monthId,yearId)
{
  var currentYear = year2digit();
  var currentTime = new Date();
  var currentMonth = currentTime.getMonth() + 1;
  currentYear = 1*currentYear;
  inputMonth = $j("#"+monthId).val();
  inputYear  = $j("#"+yearId).val();

  if(StringUtils.isEmpty(inputMonth)|| StringUtils.isEmpty(inputYear))
  {
	registerValidationError(PaymentFields[monthId].msgBlank,"expiryMonth");
	return false;
  }
  else
  {
    var dateTestCondition = (1*inputYear < currentYear) || ( (1*inputYear == currentYear) && (1*inputMonth < currentMonth) );

    if (dateTestCondition)
    {
      registerValidationError(PaymentFields[monthId].msgBlank,"expiryMonth");
      return false;
    }
  }

  return true;
}

/* Returns 2 digit year **/
function year2digit()
{
  RightNow = new Date();
  return /..$/.exec(RightNow.getYear())
}
/*-----------------------------****END Expiry Date validation *****-------------------------------*/


/*----------------------------------Security code validation --------------------------------------*/
function validateSecurityCode(securityCode)
{
  securityCodeObj = PaymentFields[securityCode];
  var securityCodeVal = $j("#"+securityCode).val();

  errorMsgBlnk = securityCodeObj.msgBlank;
  errorMsgInvalid = securityCodeObj.msgInvalid;

  //We have different error message for Amex
  if( PaymentInfo.isAMEX )
  {
	errorMsgBlnk = securityCodeObj.msgAmex;
	errorMsgInvalid = errorMsgBlnk;
  }

  //Max length and min. length allowed for the card type
  var maxDigitLength = PaymentConstants.DEFAULT_SECURITY_CODE_LEN;
  var minDigitLength = PaymentConstants.DEFAULT_SECURITY_CODE_LEN;


  if(StringUtils.isNotEmpty(PaymentInfo.selectedCardType))
  {
	  //The below two lines are reading the value of maxDigitLength and minDigitLength using $j.
	  //If the id name will contain space inbetween, jquery will not be able to read the value
	  //ex:When the id is DEBIT MASTERCARD_securityCodeLength the value is being read as undefined.
	  //So the below two lines have been commented and the value is read using document.getElementById

	  //maxDigitLength = $j("#"+PaymentInfo.selectedCardType+"_securityCodeLength").val();
	  //minDigitLength = $j("#"+PaymentInfo.selectedCardType+"_minSecurityCodeLength").val();

	  maxDigitLength = document.getElementById(PaymentInfo.selectedCardType+"_securityCodeLength").value;
	  minDigitLength = document.getElementById(PaymentInfo.selectedCardType+"_minSecurityCodeLength").value;
  }

  //length of security code entered
  if(((securityCodeVal.length) != 1*maxDigitLength) && ((securityCodeVal.length) != 1*minDigitLength ))
  {
	  registerValidationError(errorMsgInvalid,securityCode);
	  return false;
  }
  else if(1*minDigitLength>0 || StringUtils.isNotEmpty(securityCodeVal) )
  {
  	if(StringUtils.isEmpty(securityCodeVal))
    {
	  registerValidationError(errorMsgBlnk,securityCode);
	  return false;
    }
    //regex pattern check
    else if(!(securityCodeObj.regex).test(securityCodeVal))
    {
	  registerValidationError(errorMsgInvalid,securityCode);
	  return false;
    }
  }
  return true;
}

/*-----------------------------****END Security code validation *****-------------------------------*/

/*----------------------------------Issue No validation --------------------------------------*/
function validateIssueNo(issueNo)
{
  if(!$j("#"+issueNo+"ControlGroup").hasClass("."+PaymentCssClasses.HIDE))
  {
	var obj = PaymentFields[issueNo];
	var value = $j("#"+issueNo).val();
	if(StringUtils.isEmpty(value))
	{
	  return true;
	}
	//regex pattern check
	else if(!(obj.regex).test(value))
	{
	  registerValidationError(obj.msgInvalid,issueNo);
	  return false;
	}
  }
  return true;
}
/*-----------------------------****END Issue No validation *****--------------------------------------*/
/*--------------------------------Server side Error highlighting -------------------------------------*/
/**
 * Function to highlight error fields and show error field names at the top
 *  in case server side error occurs
 */
/*function processingErrors(processingErrorFields,errorMessages)
{
  if(StringUtils.trim(processingErrorFields)!="")
  {
	errorFields = [];
	var processingErrorFields = processingErrorFields.split("|");

	for(var i = 0,len= processingErrorFields.length; i < len; i++)
	{
	  var errorField = processingErrorFields[i];
	  if(errorField=="CardType")
	  {
	     errorField = "cardType";
	  }
	  else if(errorField == "ExpiryDate")
	  {
		  errorField = "expiryMonth";
	  }
	  else
	  {
	    errorField = errorField.substring(0,1).toLowerCase()+errorField.substring(1);
	  }
	  errorFields.push(errorField);
    }
	showErrorMessageAtTop($j( PaymentPageIds.PAGE_OUTCOME ),$j( PaymentPageIds.ERROR_LIST ), errorMessages);
  }
}
*/
/*-----------------------------*****END Server side Error highlighting *****--------------------------*/