/*----------------------PopUp function -----------------------------------------------------------*/
/**
 * Shows the popup
 */
function Popup(popURL,popW,popH,attr)
{
   if (!popH) { popH = 350 }
   if (!popW) { popW = 600 }
   if(popURL.indexOf("http")==-1)
   {
     popURL = clientDomainURL + popURL;
   }
   var winLeft = (screen.width-popW)/2;
   var winTop = (screen.height-popH-30)/2;
   var winProp='scrollbars=1,width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
   //popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
   //popupWin.close()
   popupWin=window.open(popURL,"popupWindow",winProp);
   popupWin.window.focus()
}

function hideSecurityOverlay()
{
	$j("#cardSecurityCodeLinkOverlay").hide("slow");
}

function showHideDataProtectionNotice()
{
	if ($j("#dataProtectionNotice").attr("class") == "hide")
	{
		$j("#dataProtectionNotice").removeClass(PaymentCssClasses.HIDE).addClass(PaymentCssClasses.SHOW);
	}
	else
	{
		$j("#dataProtectionNotice").removeClass(PaymentCssClasses.SHOW).addClass(PaymentCssClasses.HIDE);
	}
}
/*----------------------*****END PopUp function *****-------------------------------------------------*/