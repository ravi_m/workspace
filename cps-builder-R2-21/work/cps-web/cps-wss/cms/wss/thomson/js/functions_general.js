/**
 * A function for setting brand specific functionality. 
 * It should be used to over-ride some of the default functionality defined in whitelabel js files
 */
function setAppConfig()
{
	showFieldValidationMessage();
}

function showFieldValidationMessage()
{
	AppConfig.setIsErrorMessageApplicable = false;
}

