<%----------------------Declare variables used in different parts of this JSP---------------------------%>
  <c:set var="isFalconROI" value="${bookingComponent.companyCode == 'FALS'}"/>
  <c:set var="isFalconNI" value="${bookingComponent.companyCode == 'FALN'}"/>
  <c:set var="isFalcon" value="${isFalconNI ||isFalconROI }"/>
<%------------------*****END Declare variables used in different parts of this JSP*****-----------------%>


<div class="section paymentSection">
  <h2 id="paymentDetails"><span>Payment details</span></h2>

  <div class="sectionContent">
    <p>Your total balance outstanding is <span class="paymentOutstanding"><c:out value="${currency}" escapeXml="false"/><fmt:formatNumber
        value="${bookingInfo.calculatedTotalAmount.amount}" type="number" pattern="##.##" maxFractionDigits="2"
        minFractionDigits="2"/></span></p>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="cardChargeDetails" value="${bookingInfo.cardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<c:set var="cardCharge" value="${cardChargeArray[0]}" />
<c:set var="maxCardCharge" value="${cardChargeArray[2]}" />
    <c:if test="${!isFalconROI}">
      <p><strong>There are no additional charges when paying by Maestro, MasterCard Debit or Visa/ Delta debit cards</strong>
      <br />A  fee of <fmt:formatNumber value="${cardCharge}" type="number" var="confCardCharge" maxFractionDigits="1" minFractionDigits="0"/>
                <c:out value="${confCardCharge}"/>% applies to credit card payments, which is capped at �<c:out value="${maxCardCharge}" /> per transaction when using American Express, MasterCard Credit or Visa Credit cards</p>
    </c:if>
    <p>All payments made are secure.</p>
    <p>Please choose how you would like to pay for your holiday:</p>

    <c:if test="${!fn:containsIgnoreCase(bookingFlow, 'CANCEL')}">
        <%/*-----------------------Included Payment Options(Deposit options)----------------------------------*/%>
        <c:choose>
         <c:when test="${fn:containsIgnoreCase(bookingFlow, 'OLBP')}">
             <%@include file="OLBPPaymentOptions.jspf" %>
         </c:when>
         <c:otherwise>
             <%@include file="paymentOptions.jspf" %>
         </c:otherwise>
        </c:choose>

        <%/*------------------*****END of Including Payment Options(Deposit options)*****---------------------*/%>
    </c:if>
    <ul id="paymentOptionDetails" class="controlGroups paymentOptionDetails">
     <%-----------------------Amount to pay section------------------------------------------------------%>
      <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>
      <c:choose>
        <c:when test="${fn:containsIgnoreCase(bookingFlow, 'CANCEL') && !fn:containsIgnoreCase(paymentFlow, 'PAYMENT')}">
            <c:set var="fieldControlGroupDisplayStyle" value="hide"/>
        </c:when>
        <c:when test="${fn:containsIgnoreCase(bookingInfo.errorFields,'amountToPay')}">
            <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
            <c:set var="fieldDisplayStyle" value=""/>
            <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
        </c:when>
      </c:choose>

      <li id="amountToPayControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="amountToPayErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="amountToPay">Amount to pay:</label></p>

        <div class="controls">
          <p>
            <span class="currency"><c:out value="${currency}" escapeXml="false"/></span>
            <input type="text" id="amountToPay" name="transactionAmount" value="<c:out value='${depositAmountForFirstDepositType}'/>" <c:out value="${attributeForAmountToPay}"/> class="depositAmount" autocomplete="off" />
          </p>
        </div>
      </li>
     <%------------------*****END Amount to pay section*****---------------------------------------------%>

     <%-----------------------Card type section----------------------------------------------------------%>
     <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'CardType')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

      <li id="cardTypeControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="cardTypeErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="cardType">Card type:</label></p>

        <div class="controls">
          <select name="payment_0_type" id="cardType">
            <option value="">Please select</option>
            <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}"><option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option></c:forEach>
          </select>

          <%-----Show different card type image based on brands -----%>
          <c:set var="cardTypeStyle" value="GB"/>
          <c:set var="cardTypeTitle" value="We accept Visa, Delta, MasterCard, Maestro, Solo and American Express."/>

          <c:if test="${isFalconROI}">
            <c:set var="cardTypeStyle" value="IE"/>
            <c:set var="cardTypeTitle" value="We accept Visa, MasterCard and Laser."/>
          </c:if>

          <c:if test="${isFalconNI}">
            <c:set var="cardTypeStyle" value="northIE"/>
            <c:set var="cardTypeTitle" value="We accept Visa, Delta, MasterCard, Maestro and Solo."/>
          </c:if>

          <span class="cardType <c:out value='${cardTypeStyle}'/>" title="<c:out value='${cardTypeTitle}'/>"><c:out value='${cardTypeTitle}'/></span>
          <%---**END of showing card type images**---%>
        </div>
      </li>
     <%------------------*****Card type section*****-----------------------------------------------------%>

     <%-----------------------Total amount payable section-----------------------------------------------%>
    <c:if test="${fn:containsIgnoreCase(bookingFlow, 'CANCEL') && !fn:containsIgnoreCase(paymentFlow, 'PAYMENT')}">
       <c:set var="cancelClass" value="cancel"/>
    </c:if>
      <li id="totalAmountPayableControlGroup" class="controlGroup hide <c:out value='${cancelClass}'/>">
        <p id="totalAmountPayableErrorMessage" class="errorMessage hide"></p>
        <p class="fieldLabel"><label for="totalAmountPayable">Total amount payable:</label></p>

        <div class="controls">
          <p>
            <span class="currency"><c:out value="${currency}" escapeXml="false"/></span>
            <input type="text" id="totalAmountPayable" class="calPayableAmount" readonly="readonly" />
          </p>
          <p id="totalAmountPayableCaption" class="hide">(include credit card charge)</p>
        </div>
      </li>

     <%------------------*****Total amount payable section*****------------------------------------------%>

     <%-----------------------Card number section--------------------------------------------------------%>
     <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'CardNumber')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

      <li id="cardNumberControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="cardNumberErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <a id="cardNumberAnchor" name="cardNumberAnchor"></a>
        <p class="fieldLabel"><label for="cardNumber">Card number:</label></p>

        <div class="controls">
          <input type="text" name="payment_0_cardNumber" id="cardNumber" class="longField" maxlength="20" autocomplete="off"  />
        </div>
      </li>
     <%------------------*****END Card number section*****-----------------------------------------------%>

     <%-----------------------Expiry date section--------------------------------------------------------%>
<c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'ExpiryDate')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

     <li id="expiryMonthControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="expiryMonthErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="expiryMonth">Expiry date:</label></p>

        <div class="controls">
          <select name="payment_0_expiryMonth" id="expiryMonth">
            <c:forEach begin="1" end="12" varStatus="loopStatus">
              <fmt:formatNumber var="expiryMonth" value="${loopStatus.count}" type="number" minIntegerDigits="2" pattern="##"/>
              <option value="<c:out value="${expiryMonth}"/>"><c:out value="${expiryMonth}" /></option>
            </c:forEach>
          </select>

          <select name="payment_0_expiryYear" id="expiryYear">
            <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
              <option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>
            </c:forEach>
          </select>
        </div>
      </li>
     <%------------------*****END Expiry date section*****-----------------------------------------------%>

     <%-----------------------Name on card section-------------------------------------------------------%>
     <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'CardName')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

      <li id="nameOnCardControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="nameOnCardErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="nameOnCard">Name on card:</label></p>

        <div class="controls">
          <input type="text" name="payment_0_nameOnCard" id="nameOnCard" class="longField" maxlength="80" autocomplete="off" />
        </div>
      </li>
     <%------------------*****END Name on card section*****----------------------------------------------%>

     <%-----------------------Issue number section-------------------------------------------------------%>
      <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'IssueNumber')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

      <li id="issueNumberControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="issueNumberErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="issueNumber">Issue number:</label></p>

        <div class="controls">
          <input type="text" name="payment_0_issueNumber" id="issueNumber" maxlength="2" autocomplete="off" />
          <p>Leave blank if not on your card</p>
        </div>
      </li>
     <%------------------*****Issue number section*****--------------------------------------------------%>

     <%-----------------------Card security section------------------------------------------------------%>
      <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'SecurityCode')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

      <li id="cardSecurityCodeControlGroup" class="controlGroup cardSecurityCode <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="cardSecurityCodeErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="cardSecurityCode">Card security code:</label></p>

        <div class="controls">
          <input type="text" name="payment_0_securityCode" id="cardSecurityCode" maxlength="4" autocomplete="off" />
          <p class="securityCode"><a id="cardSecurityCodeLink" href="#" class="popupLink blinkyOwner" title="The last 3 digits on the reverse of your card">The last 3 digits on the reverse of your card</a></p>

          <%@include file="cardSecurityCodeOverlay.jspf" %>
        </div>
      </li>
     <%------------------*****END Card security section*****---------------------------------------------%>

     <%-----------------------3D Secure section------------------------------------------------------%>
      <li class="controlGroup cardSecurityCode">
        <div class="controls">
          <div class="cardSecurity">
        <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
       <c:if test="${threeDLogos == 'mastercardgroup'}">
                <div class="cardSecurityType">
                  <span class="masterCard">MasterCard SecureCode</span>
                  <a id="masterCardSecureLink" href="#" class="popupLink blinkyOwner" title="MasterCard SecureCode">Learn more</a>
                </div>

                <%@include file="masterCardSecureOverlay.jspf" %>
        </c:if>
            </c:forEach>

            <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
              <c:if test="${threeDLogos == 'visagroup'}">
                <div class="cardSecurityType">
                 <span class="visa">Verified by Visa</span>
                 <a id="verifiedByVisaLink" href="#" class="popupLink blinkyOwner" title="Verified by VISA">Learn more</a>
                </div>

                <%@include file="verifiedByVisaOverlay.jspf" %>
        </c:if>
            </c:forEach>

          </div>
        </div>
      </li>
     <%------------------*****END 3D Secure section*****---------------------------------------------%>
    </ul><%--END paymentOptionDetails --%>
  </div><%-- END sectionContent --%>

  <%@include file="mainContentBottom.jspf" %>
</div><%-- END section --%>

<%@include file="cardHolderAddress.jspf" %>
