<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <c:choose>
    <c:when test='${param["b"]==19000}'>
      <c:set var="brand" value="thomson" />
    </c:when>
    <c:when test='${param["b"]==20000}'>
      <c:set var="brand" value="fcsun"/>
    </c:when>
    <c:when test='${param["b"]==21000}'>
      <c:set var="brand" value="fcfalcon"/>
    </c:when>
    <c:otherwise>
      <c:set var="brand" value="fcsun"/>
    </c:otherwise>
  </c:choose>

  <% String brand = (String)pageContext.getAttribute("brand"); %>
  <title>Payment Details - Errors</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <%@include file="resources.jspf"%>
  <script src="/cms-cps/wss/global/js/popUp.js" type="text/javascript"></script>
  <%--Modified to add ensighten in header --%>
  <jsp:include page="tag.jsp"/>
</head>

<body class="errors">
<%@include file="cookieheader.jspf"%>
  <div class="wrapper">

    <%-------------------------------Include brand specific header --------------------------------------%>
    <div id="header">
      <%@include file="header.jspf"%>
    </div><%-- End of Header --%>
   <%--------------------------*****End brand specific header*****--------------------------------------%>

    <div id="contentContainer">
      <div class="mainContent">
        <c:if test="${not empty bookingInfo.trackingData.sessionId}">
           <%@include file="logout.jspf"%>
        </c:if>

        <div class="mainContentMiddle">
          <div id="pageOutcome" class="panelOuter2 pageError">
            <div class="panelInner2">
              <span class="iconAlert">Alert!</span>
              <c:choose>
                <c:when test="${not empty bookingInfo.trackingData.sessionId}">
                  <%@include file="errorTechDifficulties.jspf"%>
                </c:when>
                <c:otherwise>
                  <%@include file="errorSessionTimeOut.jspf"%>
                </c:otherwise>
              </c:choose>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <%--------------------------------Include brand specific Footer --------------------------------------%>
  <div id="footer">
    <%@include file="errorFooter.jspf"%>
  </div>
</body>
</html>
