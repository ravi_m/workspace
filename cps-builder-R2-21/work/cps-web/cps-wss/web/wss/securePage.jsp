<div id="verified">
  <div class="wrapper">
    <div id="contentContainer">
      <div class="mainContent">
        <div id="pageTitle" class="titleSection">
          <h1>Nearly done&hellip;</h1>
        </div>

        <div class="mainContentMiddle">
          <div class="sectionContainer">
            <div class="primaryContentPanel">
              <%@include file="mainContentTop.jspf" %>

              <div class="section">
                <iframe name="ACSframe" class="secureframe" frameborder="0" style="overflow:auto;" src="common/bank3dwaiting.jsp?b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}'/>">&nbsp;</iframe>

                <div class="sectionContent">
                  <p class="header">To complete your booking please verify your card</p>
                  <p class="intro">We'd like you to book online with us knowing your details are 100% secure. That's why we've introduced Verified by Visa and MasterCard SecureCode - security programmes from your card provider designed to give you even more peace of mind when paying online.</p>

                  <p class="header">First time you've seen this?</p>
                  <p class="intro">Registering for Verified by Visa or MasterCard SecureCode is simple. Complete your card details in the box opposite. You can then set up a personal password to use for any future purchases you make with your card on participating websites - you only need to register once. We don't store any of your details - they're provided to your card issuer securely.</p>

                  <p class="header">Already registered?</p>
                  <p class="intro">If you've already used Verified by Visa or MasterCard SecureCode, simply enter your personal password in the box on the right to continue your payment.</p>

                  <p class="contactUs">If you need any help, please call us on <strong>0871 231 4691</strong>. (Calls cost 10p per minute plus network extras) or contact your card provider.</p>
                </div><!-- END sectionContent -->

                <%@include file="mainContentBottom.jspf" %>
              </div><!-- END section -->

              <%@include file="mainContentBottom.jspf" %>

              <div class="buttonGroup">
                <span class="backwardButtonOuter">
                  <span class="backwardButtonInner">
                    <a href="<c:out value='${postPaymentUrl}'/>" title="Back to Payment">Back to Payment</a>
                  </span>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- END wrapper -->
</div>
<%
      String brand = (String)pageContext.getAttribute("brand");
      %>

  <%@include file="threeDfooter.jspf"%>
