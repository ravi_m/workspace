<c:choose>
   <c:when test='${param["b"]==19000}'>
      <c:set var="brand" value="thomson" />
   </c:when>
   <c:when test='${param["b"]==20000}'>
      <c:set var="brand" value="fcsun"/>
   </c:when>
   <c:when test='${param["b"]==21000}'>
      <c:set var="brand" value="fcfalcon"/>
   </c:when>
   <c:otherwise>
      <c:set var="brand" value="fcsun"/>
   </c:otherwise>
</c:choose>
<%
   String brand = (String)pageContext.getAttribute("brand");
   String privacyPolicyURL = com.tui.uk.config.ConfReader.getConfEntry(brand+".privacypolicy.url", "");
   pageContext.setAttribute("privacyPolicyURL", privacyPolicyURL, PageContext.REQUEST_SCOPE) ;
   String airPassengerNoticeURL = com.tui.uk.config.ConfReader.getConfEntry(brand+".airPassengerNotice.url" , "");
   pageContext.setAttribute("airPassengerNoticeURL", airPassengerNoticeURL, PageContext.REQUEST_SCOPE);
%>
<title>Secure payment</title>
<%@include file="/wss/cookieheader.jspf"%>
<%@include file="css.jspf" %>
<div id="header">
   <%@include file="header.jspf"%>
      <!-- Modified to move ensighten from footer to head section -->
	<jsp:include page="/wss/tag.jsp"/>
</div><!-- END #header -->