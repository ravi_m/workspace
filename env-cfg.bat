@echo off
REM *************************************************************
REM ***      TUI-UK WEB Development Environment Config        ***
REM ***                                                       ***
REM *************************************************************

REM SETTINGS (MODIFY THIS) **************************************

REM Development Environment Directory (Where you have copied 
REM these files).

SET DEV_DIR=D:\cpsinstance


REM *************************************************************
REM *********** DO NOT MODIFY ANYTHING BELOW HERE ***************
REM *************************************************************

REM *************************************************************
REM ********* NO - I REALLY MEAN IT! DO NOT MODIFY **************
REM *************************************************************

ECHO **** TUI-UK WEB Development Environment Config Initialisation ****

REM If you are changing this environment, please increment the version
REM below and say who you are
set WEB_DEV_ENV_VER=1.00
set WEB_DEV_ENV_CREATED=6th May 2006
set WEB_DEV_ENV_AUTH=Yas Poptani

REM JDK
REM set JAVA_HOME=D:\cpsinstance\java\jdk1.8.0_291
set JAVA_HOME=D:\cpsinstance\java\jdk1.8.0_144
set PATH=%JAVA_HOME%\bin
set CLASSPATH=D:\cpsinstance\java\jdk1.8.0_144\bin\lib

              D:\cpsinstance\apache-ant-1.7.0

REM ANT
set ANT_HOME=%DEV_DIR%\apache-ant-1.7.0
set PATH=%PATH%;%ANT_HOME%\bin;%ANT_HOME%\lib
ant.ant-contrib.path=D:\cpsinstance\apache-ant-1.7.0\lib\ant-contrib.jar



ECHO %ANT_HOME%
ECHO %PATH%
REM Tomcat
set TOMCAT_HOME=D:\cpsinstance\apache-tomcat-9.0.41
set CATALINA_HOME=D:\cpsinstance\apache-tomcat-9.0.41
set PATH=%PATH%;D:\cpsinstance\apache-tomcat-9.0.41\bin


REM set ORACLE9I_CLIENT_BIN=C:\oraclexe\app\oracle\product\11.2.0\server\bin
REM set AMADEUS_API_HOME=c:\wishmain\xdistdev\xdist\amadeusbinfiles
REM set SQUID_PROXY_HOST=10.174.0.36

echo Web Development Environment v%WEB_DEV_ENV_VER% (%WEB_DEV_ENV_CREATED%) by %WEB_DEV_ENV_AUTH%


java -version
call ant -version
call D:\cpsinstance\apache-tomcat-9.0.41\bin\version.bat
ECHO **** TUI-UK WEB Development Environment Config Initialisated *****