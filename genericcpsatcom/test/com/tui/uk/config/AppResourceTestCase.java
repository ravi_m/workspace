/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ConfigTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-06-27 $
 *
 * $Author:  $
 *
 * $Log: $
 */
package com.tui.uk.config;

import java.util.Locale;

import junit.framework.TestCase;

/**
 * Unit test for the iScape Configuration class.<br/> This test requires
 * test.conf exist in the testResources folder.
 */
public class AppResourceTestCase extends TestCase
{
   /** The class name. */
   private static String fqcn = AppResourceTestCase.class.getName();

   /** The Constant LONG_NUMBER_ONE. */
   private static final long LONG_NUMBER_ONE = 33720368L;

   /** The Constant LONG_NUMBER_TWO. */
   private static final long LONG_NUMBER_TWO = 33720368L;

   /** The Constant LONG_NUMBER_THREE. */
   private static final long LONG_NUMBER_THREE = 9223372036854775807L;

   /** The Constant LONG_NUMBER_FOUR. */
   private static final long LONG_NUMBER_FOUR = 92233720775807111L;

   /** The Constant INTEGER_NUMBER. */
   private static final int INTEGER_NUMBER_ONE = 214748388;

   /** The Constant INTEGER_NUMBER_TWO. */
   private static final int INTEGER_NUMBER_TWO = 2147483647;

   /**
    * Reads the test.conf from [dev.home]/global/testResources location
    * Assums that system property dev.home being set before calling this
    * class For iScape build mechanism this has been set in junit task.
    *
    * @throws Exception the exception
    */
   protected void setUp() throws Exception
   {
      super.setUp();
      AppResource.setAppName("UNITTEST");
      AppResource.setConfigFileName(System.getProperty("configfile"));
      AppResource.setMsgResourceName(System.getProperty("messagefile"));
      AppResource.loadConfiguration();
   }

   /**
    * test all entry types being accuratly retrieved.
    */
   public void testEntries()
   {
      assertEquals("Hello World", AppResource.getConfEntry(fqcn + ".STRING", null));
      assertEquals(INTEGER_NUMBER_TWO, AppResource.getIntEntry(fqcn + ".INT", 0, null));
      assertEquals(LONG_NUMBER_THREE, AppResource.getLongEntry(fqcn + ".LONG", 0, null));
      assertTrue(AppResource.getBooleanEntry(fqcn + ".BOOLEAN", false, null));
      String[] values = AppResource.getStringValues(fqcn + ".STRINGVALUES", null, ",", null);
      assertEquals("Hello", values[0]);
      assertEquals("World", values[1]);
   }

   /**
    * Test the non existing entries returns the code default values.
    */
   public void testCodeDefaultEntries()
   {
      assertEquals("Hello World", AppResource.getConfEntry(fqcn + ".XSTRING", "Hello World"));
      assertEquals(INTEGER_NUMBER_ONE,
         AppResource.getIntEntry(fqcn + ".XINT", INTEGER_NUMBER_ONE, null));
      assertEquals(LONG_NUMBER_FOUR, AppResource.getLongEntry(fqcn + ".XLONG", LONG_NUMBER_FOUR,
               null));
      assertFalse(AppResource.getBooleanEntry(fqcn + ".XBOOLEAN", false, null));
      String[] defaultValues = new String[] {"XHello", "XWorld" };
      String[] values =
          AppResource.getStringValues(fqcn + ".XSTRINGVALUES", defaultValues, ",", null);
      assertEquals("XHello", values[0]);
      assertEquals("XWorld", values[1]);

   }

   /**
    * test the overide behavior of entry.
    */
   public void testOverridenEntry()
   {
      assertEquals("Hello iScape", AppResource.getConfEntry(fqcn + ".OVERRIDE_STRING", null));
   }

   /**
    * test the same entry for different local As cofig file has differnt
    * values for each locale.
    */
   public void testLocaleEntries()
   {
      assertEquals("Hello England", AppResource.getEntry(fqcn + ".LOCALESTRING", null, Locale.UK));
      assertEquals("Hello France",
                    AppResource.getEntry(fqcn + ".LOCALESTRING", null, Locale.FRANCE));
   }

   /**
    * test for negative entries for long int & boolean.
    */
   // CHECKSTYLE:OFF
   public void testNegativeEntries()
   {
      assertEquals(-2147483647, AppResource.getIntEntry(fqcn + ".-INT", 0, null));
      assertEquals(-9223372036854775807L, AppResource.getLongEntry(fqcn + ".-LONG", 0, null));
      assertFalse(AppResource.getBooleanEntry(fqcn + ".-BOOLEAN", true, null));
   }

   // CHECKSTYLE:ON

   /**
    * test not a number.
    */
   public void testNaNEntries()
   {
      assertEquals(1, AppResource.getIntEntry(fqcn + ".NAN_INT", 1, null));
      assertEquals(LONG_NUMBER_TWO ,
         AppResource.getLongEntry(fqcn + ".NAN_LONG", LONG_NUMBER_ONE, null));
   }

   /**
    * test for incorrect configuration file [in this case non existing]
    * which should throw an ConfigException exception.
    */
   public void testIncorrectConfigFilePath()
   {
      String file = "/abcxzs.zcvbs." + System.currentTimeMillis();
      System.setProperty("configfile", "test1.conf");
      try
      {
         AppResource.loadConfiguration();
         fail("Should have thrown ConfigException as config file[" + file + "] does not exist");
      }
      catch (AppResourceException e)
      {
         assertTrue(true);
      }
   }

   /**
    * Tear down.
    *
    * @throws Exception the exception
    */
   protected void tearDown() throws Exception
   {
      super.tearDown();
   }
}
