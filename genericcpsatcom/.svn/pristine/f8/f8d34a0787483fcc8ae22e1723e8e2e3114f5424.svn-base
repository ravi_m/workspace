package com.tui.uk.payment.generic;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcStreamTransport;
import org.apache.xmlrpc.client.XmlRpcSunHttpTransport;
import org.apache.xmlrpc.common.XmlRpcStreamRequestConfig;
import org.xml.sax.SAXException;

/**
 * This is a custom XML-RPC transport which logs the outgoing and incoming XML-RPC messages.
 */
public class XmlRpcLogging extends XmlRpcSunHttpTransport
{

   /** logger for XmlRpcLogging class  .*/
   private static final Logger LOGGER = Logger.getLogger(XmlRpcLogging.class.getName());
   
   /**
    * Constructor for call super class constructor.
    * @param pClient client.
    */
   public XmlRpcLogging(XmlRpcClient pClient)
   {
      super(pClient);
   }

   /**
    * Dumps outgoing XML-RPC requests to the log.
    * @param pWriter writer
    * @throws  IOException exception.
    * @throws  XmlRpcException exception.
    * @throws  SAXException exception.
    *
    */
   @Override
   protected void writeRequest(final XmlRpcStreamTransport.ReqWriter pWriter) throws IOException,
      XmlRpcException, SAXException
   {
      final ByteArrayOutputStream baos = new ByteArrayOutputStream();
      pWriter.write(baos);
      LOGGER.info(baos.toString());
      super.writeRequest(pWriter);
   }

   /**
    * Dumps incoming XML-RPC responses to the log
    */
   @Override
   protected Object readResponse(XmlRpcStreamRequestConfig pConfig, InputStream pStream)
      throws XmlRpcException
   {
      final StringBuffer sb = new StringBuffer();

      try
      {
         final BufferedReader reader = new BufferedReader(new InputStreamReader(pStream));
         String line = reader.readLine();
         while (line != null)
         {
            sb.append(line);
            line = reader.readLine();
         }
      }
      catch (final IOException e)
      {
         LOGGER.log(Level.SEVERE, "While reading server response", e);
      }

      LOGGER.info(sb.toString());

      final ByteArrayInputStream bais = new ByteArrayInputStream(sb.toString().getBytes());
      return super.readResponse(pConfig, bais);
   }
}
