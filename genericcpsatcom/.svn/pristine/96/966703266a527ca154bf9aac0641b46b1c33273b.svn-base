/*
 * Copyright (C)2008 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 * 
 * $RCSfile: $
 * 
 * $Revision: $
 * 
 * $Date: $
 * 
 * $Author: $
 * 
 * $Log: $
 */
package com.tui.uk.payment.generic;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xmlrpc.XmlRpcException;

import com.tui.uk.client.CommonPaymentClient;
import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.config.AppResource;

/**
 * The Class ConfirmationPaymentServlet. This class is used to complete authpayment and fullfill the
 * transaction.
 * 
 * @author Jaleel
 */
@SuppressWarnings("serial")
public class ConfirmationPaymentServlet extends HttpServlet
{

   /** The constant THREE_D_SECURE_CONF_VALUE. */
   public static final String THREE_D_SECURE_CONF_VALUE = ".3DSecure";

   /** The constant for client. */
   private static final String CLIENT = "client";

   /**
    * This method call the doPost() method.
    * 
    * @param response the HttpServletResponse object that contains the response the servlet sends to
    *           the client.
    * @param request the HttpServletRequest object that contains the request the client has made of
    *           the servlet.
    * @throws IOException the IOException thrown if an input or output error is detected when the
    *            servlet handles the request
    * @throws ServletException the ServletException thrown if the request for the GET could not be
    *            handled.
    */

   public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      doPost(request, response);
   }

   /**
    * This method reads the token and client code and fullfill the transaction.
    * 
    * @param request the HttpServletRequest object that contains the request the client has made of
    *           the servlet.
    * @param response the HttpServletResponse object that contains the response the servlet sends to
    *           the client.
    * 
    * @throws IOException the IOException thrown if an input or output error is detected when the
    *            servlet handles the request
    * @throws ServletException the ServletException thrown if the request for the GET could not be
    *            handled.
    */
   @SuppressWarnings("deprecation")
   public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {

      String token = (String) request.getSession().getAttribute("token");
      String tomcatInstance = (String) request.getSession().getAttribute("tomcatInstance");
      String clientcode = (String) request.getSession().getAttribute("clientcode");
      String clientApplication = (String) request.getSession().getAttribute("clientapplication");
      String threeDSecure = null;

      if (clientApplication != null)
      {
         threeDSecure = clientApplication + THREE_D_SECURE_CONF_VALUE;
      }

      if (threeDSecure == null || !AppResource.getBooleanEntry(threeDSecure, false, null))
      {
         // B2B Flow
         if (clientcode.equals(GenericConstants.JAVACLIENT)
            || clientcode.equals(GenericConstants.JAVAEXAMPLECLIENT))
         {
            CommonPaymentClient cpc =
               new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null) + "?"
                  + tomcatInstance);
            try
            {
               cpc.authPayment(token, "", AppResource.getConfEntry(CLIENT, null));
               cpc.fulfill(token, AppResource.getConfEntry(CLIENT, null));
               List<EssentialTransactionData> essentialtransactiondatat =
                  cpc.getEssentialTransactionData(token);
               request.setAttribute("essentialtransactiondata", essentialtransactiondatat);

               cpc.purgeSensitiveData(token);
               cpc.notifyBookingCompletion(token);
               request.getRequestDispatcher(AppResource.getConfEntry("confirmationpage", null))
                  .forward(request, response);
            }
            catch (XmlRpcException xmle)
            {
               xmle.printStackTrace();
            }
         }
         else
         {

            Map<String, String> paymentDataMap = new HashMap<String, String>();
            try
            {
               DotNetBookingComponent dotNetBookingComponent = new DotNetBookingComponent();
               dotNetBookingComponent
                  .authPayment(token, "", AppResource.getConfEntry(CLIENT, null));
               dotNetBookingComponent.fulfill(token, AppResource.getConfEntry(CLIENT, null));

               paymentDataMap = dotNetBookingComponent.getPaymentData(token);

               request.setAttribute("paymentdata", paymentDataMap);
               dotNetBookingComponent.purgeSensitiveData(token);

               dotNetBookingComponent.notifyBookingCompletion(token);
               request.getRequestDispatcher(
                  AppResource.getConfEntry("dotnetconfirmationpage", null)).forward(request,
                  response);
            }
            catch (XmlRpcException xmle)
            {
               xmle.printStackTrace();
            }
         }
      }
      else
      {
         // B2C Flow
         if (clientcode.equals(GenericConstants.JAVACLIENT)
            || clientcode.equals(GenericConstants.JAVAEXAMPLECLIENT))
         {
            CommonPaymentClient cpc =
               new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null) + "?"
                  + tomcatInstance);
            try
            {
               cpc.fulfill(token, AppResource.getConfEntry(CLIENT, null));
               List<EssentialTransactionData> essentialtransactiondatat =
                  cpc.getEssentialTransactionData(token);
               request.setAttribute("essentialtransactiondata", essentialtransactiondatat);

               Map<String, String> nonpaymentdata = cpc.getNonPaymentData(token);
               request.setAttribute("nonpaymentdata", nonpaymentdata);

               cpc.purgeSensitiveData(token);
               cpc.purgeClientPaymentPageData(token);
               cpc.notifyBookingCompletion(token);
               request.getRequestDispatcher(AppResource.getConfEntry("confirmationpage", null))
                  .forward(request, response);
            }
            catch (XmlRpcException xmle)
            {
               xmle.printStackTrace();
            }
         }
         else
         {

            DotNetBookingComponent dotNetBookingComponent = new DotNetBookingComponent();
            Map<String, String> paymentDataMap = new HashMap<String, String>();

            dotNetBookingComponent.fulfill(token, AppResource.getConfEntry(CLIENT, null));

            paymentDataMap = dotNetBookingComponent.getPaymentData(token);

            Map<String, String> nonpaymentdata = dotNetBookingComponent.getApplicationData(token);
            request.setAttribute("nonpaymentdata", nonpaymentdata);

            request.setAttribute("paymentdata", paymentDataMap);
            dotNetBookingComponent.purgeSensitiveData(token);

            dotNetBookingComponent.notifyBookingCompletion(token);
            request.getRequestDispatcher(AppResource.getConfEntry("dotnetconfirmationpage", null))
               .forward(request, response);

         }

      }
   }
}