<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:template match="qalab">
	<html>
	<head>
		<title>Checkstyle comparison</title>
		<style type="text/css">
			.bad {color: red;}
			.good {color: green;}
		</style>
	</head>
	<body>
	<table border="1">
		<tr>
			<th>filename</th>
			<th>current checkstyle errors</th>
			<th>previous checkstyle errors</th>
			<th>difference</th>
		</tr>
	<xsl:for-each select="file[result/@type = 'checkstyle']">
			<tr>
			    <xsl:if test="result[@type='checkstyle'][position()=last()]/@statvalue - result[@type='checkstyle'][position()=last()-1]/@statvalue &gt; 0">
					<xsl:attribute name="class">bad</xsl:attribute>
			    </xsl:if>
			    <xsl:if test="result[@type='checkstyle'][position()=last()]/@statvalue - result[@type='checkstyle'][position()=last()-1]/@statvalue &lt; 0">
					<xsl:attribute name="class">good</xsl:attribute>
			    </xsl:if>
				<td><xsl:value-of select="@path"/></td>
				<td><xsl:value-of select="result[@type='checkstyle'][position()=last()]/@statvalue"/></td>
				<td><xsl:value-of select="result[@type='checkstyle'][position()=last()-1]/@statvalue"/><xsl:text></xsl:text></td>
				<td><xsl:value-of select="result[@type='checkstyle'][position()=last()]/@statvalue - result[@type='checkstyle'][position()=last()-1]/@statvalue"/></td>
			</tr>
	</xsl:for-each>
	</table>
	</body>
	</html>
</xsl:template>

</xsl:stylesheet>
