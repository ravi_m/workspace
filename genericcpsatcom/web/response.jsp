<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DATACASH TESTER RESPONSE</title>
</head>
<body>
<center><h1>DATACASH TESTER</h1></center>
<a href="/DataCashTester/request.jsp" style= "float:right">Back</a>
<center>
<h3>THE DATACASH RESPONSE</h3>
<p>
   <textarea rows="30" cols="80"  ><c:out value = "${responsexml}" escapeXml ="true"/></textarea>
</p>
<c:if test = "${not empty cardres}">
	<table border="1" align="center">
	 <tr>
	  <td>Card Type</td>
	  <td><c:out value = "${cardres.cardScheme}" escapeXml ="true"/></td>
	 </tr>
	 <tr>
	  <td>DataCash reference</td>
	  <td><c:out value = "${cardres.datacashReference}" escapeXml ="true"/></td>
	 </tr>
	 <tr>
	  <td>Merchant Reference</td>
	  <td><c:out value = "${cardres.merchantReference}" /></td>
	 </tr>
	 <tr>
	  <td>Mode</td>
	  <td><c:out value = "${cardres.mode}"/></td>
	 </tr>
	 <tr>
	  <td>Status Code</td>
	  <td><c:out value = "${cardres.code}"/></td>
	 </tr>
	 <tr>
	  <td>Status Message</td>
	  <td><c:out value = "${cardres.description}"/></td>
	 </tr>
	 <c:if test = "${not empty cardres.authCode}">
	 <tr>
	  <td>AuthCode</td>
	  <td><c:out value = "${cardres.authCode}"/></td>
	 </tr>
	 </c:if>
	</table>
</c:if>
</center>
<a href="/DataCashTester/request.jsp" style= "float:right">Back</a>
</body>
</html>