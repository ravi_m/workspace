<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
   <TITLE> New Document </TITLE>
      <style>
   #container{
   width:400px;
   _width:300px;
   clear:both;
   }
   #passcont{
   width:560px;
   }
   #passcont .titlecol{
   width:60px;
   float:left;
   padding:2px;
   }
   #passcont .col{
   width:150px;
   float:left;
   padding:2px;
   }
   #container .item{
   float:left;
   padding:2px;
   display:block;
   }
   #container .col{
   float:left;
   padding:2px;
   }
   </style>

</HEAD>
<script language="javascript">

function addRow(tblId)
{
  var tableBody = document.getElementById("passengerDetailsTable").tBodies[0];
  var newRow = tableBody.insertRow(-1);
  var rowIndex = newRow.sectionRowIndex;

  var newCell0 = newRow.insertCell(0);
  newCell0.innerHTML = '<input type="text" value="" name="appDataKey">';

  var newCell1 = newRow.insertCell(1);
  newCell1.innerHTML = "<input type='text' name='appDataValue'> &nbsp;&nbsp;<input type='button' value='Delete' onclick='deleteCurrentRow(this);'/>";
  document.dotnet.rowIndex.value=rowIndex;
}

function deleteCurrentRow(obj)
{
      var delRow = obj.parentNode.parentNode;
      var tbl = delRow.parentNode.parentNode;
      var rIndex = delRow.sectionRowIndex;
      var rowArray = new Array(delRow);
      deleteRows(rowArray);
      document.dotnet.rowIndex.value = document.dotnet.rowIndex.value-1;
}

function deleteRows(rowObjArray)
{
      for (var i=0; i<rowObjArray.length; i++) {
         var rIndex = rowObjArray[i].sectionRowIndex;
         rowObjArray[i].parentNode.deleteRow(rIndex);
      }
}

</script>
<BODY>
<HR align="left" width="54%">

<form action="/genericcpsclient/prepayment" method="post" name="dotnet">
<input type="hidden" name="clientcode" value="4"/>
<input type="hidden" name="rowIndex" value="1"/>

<TABLE id="bookingcomponent" name="bookingcomponent" cellpadding="2" cellspacing="2">
  <TBODY>
  <TR>
     <TD>Client Applications:</TD>
     <TD>
        <select  name="clientapplication" id="clientapplication">
        
           <option value="AtcomRes"/>AtcomRes</option>
        </select>
     </TD>
  <TR>
  <TR>
     <TD>Currency:</TD>
     <TD><input type="text" name="currency" value="GBP"/></TD>
     <TD>Allowed Amount:</TD>
     <TD><input type="text" name="bookingcomponent_amount" value="30"/></TD>
  </TR>
  </TBODY>
</TABLE>
<hr>
<h4> Transaction Type</h4>
<input type="radio" name="transactiontype"  checked="true" value="Payment" />Payment
<input type="radio" name="transactiontype" value="Refund"/>Refund

<HR align="left" width="54%">
<TABLE id='passengerDetailsTable' border="0">
   <TR>
      <TD><font color="red"><strong>Application Data</strong></font></TD>
   </TR>
   <TR>
      <TD><font color="blue">Key</font></TD>
      <TD><font color="blue">Value</font></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="transaction_total"></TD>
      <TD><input type="text" name="appDataValue" value="271.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="amount_paid"></TD>
      <TD><input type="text" name="appDataValue" value="153.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="balance_amount"></TD>
      <TD><input type="text" name="appDataValue" value="118.00"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="client_domain_url"></TD>
      <TD><input type="text" name="appDataValue" value="@comRes"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pre_payment_url"></TD>
      <TD><input type="text" name="appDataValue" value="google.com"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="post_payment_url"></TD>
      <TD><input type="text" name="appDataValue" value="yahoo.com"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="failure_payment_url"></TD>
      <TD><input type="text" name="appDataValue" value="google.com"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="deposit_high_available"></TD>
      <TD><input type="text" name="appDataValue" value="false"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="deposit_high_amount"></TD>
      <TD><input type="text" name="appDataValue" value="330"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="deposit_high_due_date"></TD>
      <TD><input type="text" name="appDataValue" value=" 5th june 14"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="deposit_low_available"></TD>
      <TD><input type="text" name="appDataValue" value="false"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="deposit_low_amount"></TD>
      <TD><input type="text" name="appDataValue" value="250"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="deposit_low_due_date"></TD>
      <TD><input type="text" name="appDataValue" value="5th june 14"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="part_payment_available"></TD>
      <TD><input type="text" name="appDataValue" value="false"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="part_payment_min_amount"></TD>
      <TD><input type="text" name="appDataValue" value="22.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="agent_code"></TD>
      <TD><input type="text" name="appDataValue" value="Atcom123"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="user_id"></TD>
      <TD><input type="text" name="appDataValue" value="Atcom123"></TD>
   </TR>
   
     <TR>
      <TD><input type="text" name="appDataKey" value="total_paid"></TD>
      <TD><input type="text" name="appDataValue" value=""></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="reservation_id"></TD>
      <TD><input type="text" name="appDataValue" value="Atcom123"></TD>
   </TR>

   <TR>
      <TD><input type="text" name="appDataKey" value="creditcard_charges"></TD>
      <TD><input type="text" name="appDataValue" value="0.0"></TD>
   </TR>
   
   <TR>
      <TD><input type="text" name="appDataKey" value="holiday_cost"></TD>
      <TD><input type="text" name="appDataValue" value="271.00"></TD>
   </TR>
   
   <TR>
      <TD><input type="text" name="appDataKey" value="other_payments_available"></TD>
      <TD><input type="text" name="appDataValue" value="false"></TD>
   </TR>
   
   <TR>
      <TD><input type="text" name="appDataKey" value="othert_payment_amount"></TD>
      <TD><input type="text" name="appDataValue" value="1256.25"></TD>
   </TR>
   
   <TR>
      <TD><input type="text" name="appDataKey" value="compensation_amount"></TD>
      <TD><input type="text" name="appDataValue" value="0.0"></TD>
   </TR>
   
   <TR>
      <TD><input type="text" name="appDataKey" value="balance_due_date"></TD>
      <TD><input type="text" name="appDataValue" value="20-oct-2015"></TD>
   </TR>

 <TR>
      <TD><input type="text" name="appDataKey" value="street_address1"></TD>
      <TD><input type="text" name="appDataValue" value="Bangalore"></TD>
   </TR>
   
   <TR>
      <TD><input type="text" name="appDataKey" value="street_address2"></TD>
      <TD><input type="text" name="appDataValue" value="Delhi"></TD>
   </TR>
   
   <TR>
      <TD><input type="text" name="appDataKey" value="street_address3"></TD>
      <TD><input type="text" name="appDataValue" value="Mumbai"></TD>
   </TR>
   
    <TR>
      <TD><input type="text" name="appDataKey" value="cardBillingPostcode"></TD>
      <TD><input type="text" name="appDataValue" value="AA22AA"></TD>
   </TR>
   
   
    <TR>
      <TD><input type="text" name="appDataKey" value="selectedCountryCode"></TD>
      <TD><input type="text" name="appDataValue" value="IR"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="card_expired"></TD>
      <TD><input type="text" name="appDataValue" value="true"></TD>
   </TR>
   
   
    <TR>
      <TD><input type="text" name="appDataKey" value="payee_name_0"></TD>
      <TD><input type="text" name="appDataValue" value="Naresh"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="payment_date_0"></TD>
      <TD><input type="text" name="appDataValue" value="11-05-2015"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="payment_type_0"></TD>
      <TD><input type="text" name="appDataValue" value="VISA"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="datacash_ref_0"></TD>
      <TD><input type="text" name="appDataValue" value="4500203094954487"></TD>
   </TR>
   
 <TR>
      <TD><input type="text" name="appDataKey" value="card_number_0"></TD>
      <TD><input type="text" name="appDataValue" value="42424250000009"></TD>
   </TR>
 <TR>
      <TD><input type="text" name="appDataKey" value="expiry_date_0"></TD>
      <TD><input type="text" name="appDataValue" value="05/2015"></TD>
   </TR>
 <TR>
      <TD><input type="text" name="appDataKey" value="amount_0"></TD>
      <TD><input type="text" name="appDataValue" value="300.0"></TD>
   </TR>   
   
   
   
   <TR>
      <TD><input type="text" name="appDataKey" value="payee_name_1"></TD>
      <TD><input type="text" name="appDataValue" value="Savitha"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="payment_date_1"></TD>
      <TD><input type="text" name="appDataValue" value="04-05-13"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="payment_type_1"></TD>
      <TD><input type="text" name="appDataValue" value="Amex"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="datacash_ref_1"></TD>
      <TD><input type="text" name="appDataValue" value="4500203094954487"></TD>
   </TR>
   
 <TR>
      <TD><input type="text" name="appDataKey" value="card_number_1"></TD>
      <TD><input type="text" name="appDataValue" value="340000000000009"></TD>
   </TR>
 <TR>
      <TD><input type="text" name="appDataKey" value="expiry_date_1"></TD>
      <TD><input type="text" name="appDataValue" value="04-05-15"></TD>
   </TR>
 <TR>
      <TD><input type="text" name="appDataKey" value="amount_1"></TD>
      <TD><input type="text" name="appDataValue" value="400.0"></TD>
   </TR>   
   
   
   
     <TR>
      <TD><input type="text" name="appDataKey" value="payee_name_2"></TD>
      <TD><input type="text" name="appDataValue" value="Saleem"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="payment_date_2"></TD>
      <TD><input type="text" name="appDataValue" value="04-06-2016"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="payment_type_2"></TD>
      <TD><input type="text" name="appDataValue" value="Debit mastercard"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="datacash_ref_2"></TD>
      <TD><input type="text" name="appDataValue" value="5642874"></TD>
   </TR>
   
 <TR>
      <TD><input type="text" name="appDataKey" value="card_number_2"></TD>
      <TD><input type="text" name="appDataValue" value="1001190000000006"></TD>
   </TR>
 <TR>
      <TD><input type="text" name="appDataKey" value="expiry_date_2"></TD>
      <TD><input type="text" name="appDataValue" value="04/2015"></TD>
   </TR>
 <TR>
      <TD><input type="text" name="appDataKey" value="amount_2"></TD>
      <TD><input type="text" name="appDataValue" value="200.0"></TD>
   </TR>
   
</TABLE>
<input type="button" value="Add Row" onClick="javascript:addRow(this)"/>
<input type="submit" value="Submit" />

<HR align="left" width="54%">

<td><font color="red"><strong>Card Types</strong></font><br/>
   <SELECT MULTIPLE name="cardtype" SIZE="11">
   <OPTION VALUE="American Express">American Express
   <OPTION VALUE="ATM ">ATM.
   <OPTION VALUE="Debit Mastercard">Debit Mastercard
   <OPTION VALUE="Diners Club">Diners Club
   <OPTION VALUE="Discover ">Discover
   <OPTION VALUE="EnRoute">EnRoute
   <OPTION VALUE="GE Capital">GE Capital
   <OPTION VALUE="JCB">JCB
   <OPTION VALUE="Laser">Laser
   <OPTION VALUE="Maestro">Maestro
   <OPTION VALUE="Mastercard" selected>Mastercard
   <OPTION VALUE="Platima">Platima
   <OPTION VALUE="Solo">Solo
   <OPTION VALUE="Switch">Switch
   <OPTION VALUE="VISA">VISA
   <OPTION VALUE="VISA Delta">VISA Delta
   <OPTION VALUE="VISA Electron">VISA Electron
   <OPTION VALUE="VISA Purchasing">VISA Purchasing
   </SELECT>
</td>
<HR align="left" width="54%">

</form>
</BODY>
<script language="javascript">
var selectmenu=document.getElementById("clientapplication");
selectmenu.onchange=function(){
	if(this.options[this.selectedIndex].value=="AtcomRes")
	{
	document.location.href ="atcomres.jsp";
	}
	else
	{
	      document.location.href ="dynamicDotNetPrePayment.jsp";
	}
}
</script>
</HTML>
