<html>
<head>
<script  language="javascript1.1" type = "text/javascript">
function emptyChecker()
 {
    if (document.getElementById('requestxml').value == '')
    {
      alert('please enter the request xml to proceed');
    }
    else
    {
      document.getElementById('datacashTester').submit();
    }
 }

 function loadexample(option)
 {
   var optionvalue = option.value;
   var requestxml = document.getElementById('requestxml');
   if (optionvalue == 'pre' || optionvalue=='auth' || optionvalue=='erp' || optionvalue=='refund')
   {
   	requestxml.value = '<?xml version="1.0" encoding="UTF-8"?>\n<Request>\n<UserAgent>\n<architecture version="5.1">x86-Windows XP</architecture>\n<language version="1.5.0_06-b05" vm-name="Java HotSpot(TM) Client VM" vendor="Sun Microsystems Inc.">Java</language>\n<Libraries>\n<lib version="v2-0-10">XMLDocument</lib>\n</Libraries>\n</UserAgent>\n<Authentication>\n<client>99490800</client>\n<password>VgtNrsQX6</password>\n</Authentication>\n<Transaction>\n<CardTxn>\n<Card>\n<Cv2Avs>\n<postcode>aa22aa</postcode>\n<ExtendedPolicy>\n<cv2_policy matched="accept" notmatched="reject" notprovided="reject" partialmatch="reject" notchecked="accept" />\n\n<postcode_policy matched="accept" notmatched="accept" notprovided="accept" partialmatch="accept" notchecked="accept" />\n\n<address_policy matched="accept" notmatched="accept" notprovided="accept" partialmatch="accept" notchecked="accept" />\n\n</ExtendedPolicy>\n<cv2>444</cv2>\n</Cv2Avs>\n<expirydate>02/14</expirydate>\n<pan>4242425000000009</pan>\n</Card>\n<authcode />\n<method>';
   }
   else
   {
    requestxml.value = '<?xml version="1.0" encoding="UTF-8"?>\n<Request>\n<UserAgent>\n<architecture version="5.1">x86-Windows XP</architecture>\n<language version="1.5.0_06-b05" vm-name="Java HotSpot(TM) Client VM" vendor="Sun Microsystems Inc.">Java</language>\n<Libraries>\n<lib version="v2-0-10">XMLDocument</lib>\n</Libraries>\n</UserAgent>\n<Authentication>\n<client>99490800</client>\n<password>VgtNrsQX6</password>\n</Authentication>\n<Transaction>\n<HistoricTxn>\n<reference>4200200062890345</reference>\n<authcode>860627</authcode>\n<method>';
   }
   switch(optionvalue)
   {
     case 'pre':requestxml.value = requestxml.value+'pre';
                                   break;
     case 'auth':requestxml.value = requestxml.value+'auth';
                                   break;
     case 'erp':requestxml.value = requestxml.value+'erp';
                                   break;
     case 'refund':requestxml.value = requestxml.value+'refund';
                                   break;
     case 'fulfill':requestxml.value = requestxml.value+'fulfill';
                                   break;
     case 'cancel':requestxml.value = requestxml.value+'cancel';
                                   break;
     case 'txn_refund':requestxml.value = requestxml.value+'txn_refund';
                                   break;
     case 'test':requestxml.value = requestxml.value+'test';
     break;
     case 'def':requestxml.value = '';
                                   break;
   }
   if(requestxml.value != '' && ( optionvalue == 'pre' || optionvalue=='auth' || optionvalue=='erp' || optionvalue=='refund'))
   {
     requestxml.value = requestxml.value+'</method>\n</CardTxn>\n<TxnDetails>\n<amount currency="GBP">358.71</amount>\n<merchantreference>7929456943744006342</merchantreference>\n</TxnDetails>\n</Transaction>\n</Request>';
   }
   else if (optionvalue =='fulfill' || optionvalue =='cancel' || optionvalue =='txn_refund'  || optionvalue =='test')
   {
     requestxml.value = requestxml.value+'</method>\n</HistoricTxn>\n<TxnDetails>\n<amount>332.06</amount>\n</TxnDetails>\n</Transaction>\n</Request>';
   }
   else
   {
    requestxml.value = '';
   }
 }

</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DATACASH TESTER</title>
</head>
<body>
<center><h1>DATACASH TESTER</h1></center>
<form id="datacashTester" action="ProcessRequestServlet" method="post" />
<div style="height: 510px; width: 600px; float: right; position: absolute; top: 100px; left: 400px;">
<b>HOST URL:</b><input type="text" id="host" name="host" value="https://testserver.datacash.com/Transaction" style="width: 300px"/><br/>
<br/>
<b>Paste your request xml here:</b><br>
<textarea rows="30" cols="60" name = "requestxml" id ="requestxml"></textarea>
</div>
<div style = "position: absolute; top: 100px; left: 70px; width: 300px; float: left;">
<select id = "method" onchange="loadexample(this)">
<option value="def">Select a type of transaction</option>
<option value="pre">Pre Transaction(authPayment)</option>
<option value="auth">Auth Transaction(doPayment)</option>
<option value="erp">ERP Transaction(authRefund)</option>
<option value="refund">Refund Transaction(doRefund)</option>
<option value="fulfill">Fulfil Transaction(fulfill)</option>
<option value="cancel">Cancel Transaction(cancel)</option>
<option value="txn_refund">Reverse Transaction(refund)</option>
<option value="test">test</option>
</select>
</div>




</form>
<div style = 'position: absolute; top: 250px; left: 80px; width: 300px;'>
<input type = "button" value = "SEND REQUEST"  onClick="emptyChecker();"/><br/>
<b><p>*NOTE:</p><p>1. Please change the merchant reference before submitting in case you are using the
request from the drop down list.</p>
<p>2.In case of fulfill, first do an auth, if successful take its datacash reference and put it in the
appropriate section in the xml request for fulfill.</p>
<p>3.In case of reverse transaction, first do an auth, then fulfill, copy the datacash reference of the
"fulfill" response and put it into the appropriate section in the request xml.</p></b>

</div>
</body>
</html>