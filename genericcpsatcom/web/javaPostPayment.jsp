<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<b><font color="red"> NonPayment Details:</b></font> <br/>
<table class="nonpaymentdata">
<c:forEach var="nonpaydata" items="${nonpaymentdata}">

	<tr>
		<td><c:out value="${nonpaydata.key}" /> </td>
		<td><c:out value="${nonpaydata.value}" /> </td>
	</tr>

 </c:forEach>
</table>
<form action="/genericcpsclient/confirmation">
<input type="submit" value="Continue"/>
</form>
<hr/>