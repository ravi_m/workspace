<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
   <TITLE> New Document </TITLE>
      <style>
   #container{
   width:400px;
   _width:300px;
   clear:both;
   }
   #passcont{
   width:560px;
   }
   #passcont .titlecol{
   width:60px;
   float:left;
   padding:2px;
   }
   #passcont .col{
   width:150px;
   float:left;
   padding:2px;
   }
   #container .item{
   float:left;
   padding:2px;
   display:block;
   }
   #container .col{
   float:left;
   padding:2px;
   }
   </style>

</HEAD>
<script language="javascript">

function addRow(tblId)
{
  var tableBody = document.getElementById("passengerDetailsTable").tBodies[0];
  var newRow = tableBody.insertRow(-1);
  var rowIndex = newRow.sectionRowIndex;

  var newCell0 = newRow.insertCell(0);
  newCell0.innerHTML = '<input type="text" value="" name="appDataKey">';

  var newCell1 = newRow.insertCell(1);
  newCell1.innerHTML = "<input type='text' name='appDataValue'> &nbsp;&nbsp;<input type='button' value='Delete' onclick='deleteCurrentRow(this);'/>";
  document.dotnet.rowIndex.value=rowIndex;
}

function deleteCurrentRow(obj)
{
      var delRow = obj.parentNode.parentNode;
      var tbl = delRow.parentNode.parentNode;
      var rIndex = delRow.sectionRowIndex;
      var rowArray = new Array(delRow);
      deleteRows(rowArray);
      document.dotnet.rowIndex.value = document.dotnet.rowIndex.value-1;
}

function deleteRows(rowObjArray)
{
      for (var i=0; i<rowObjArray.length; i++) {
         var rIndex = rowObjArray[i].sectionRowIndex;
         rowObjArray[i].parentNode.deleteRow(rIndex);
      }
}

</script>
<BODY>
<HR align="left" width="54%">

<form action="/genericcpsclient/prepayment" method="post" name="dotnet">
<input type="hidden" name="clientcode" value="4"/>
<input type="hidden" name="rowIndex" value="1"/>

<TABLE id="bookingcomponent" name="bookingcomponent" cellpadding="2" cellspacing="2">
  <TBODY>
  <TR>
     <TD>Client Application:</TD>
     <TD>
        <select  name="clientapplication" id="clientapplication">
			<option value="select" />select</option>
		<option value="TFly" />TFlyRedesign</option>
           <option value="Portland"/>Portland</option>
           <option value="TFly" />TFly</option>
           <option value="FCFO"/>FCFO</option>
		   <option value="Tracs"/>TRACS</option>
		   <option value="NEWSKIES"/>Newskies</option>
		   <option value="FIRSTCHOICE"/>FIRSTCHOICE</option>
		   <option value="AtcomRes"/>AtcomRes</option>
		   <option value="NEWSKIESM"/>Newskiesm</option>
        </select>
     </TD>
  <TR>
  <TR>
     <TD>Currency:</TD>
     <TD><input type="text" name="currency" value="GBP"/></TD>
     <TD>Allowed Amount:</TD>
     <TD><input type="text" name="bookingcomponent_amount" value="297.84"/></TD>
  </TR>
  </TBODY>
</TABLE>

<HR align="left" width="54%">
<TABLE id='passengerDetailsTable' border="0">
   <TR>
      <TD><font color="red"><strong>Application Data</strong></font></TD>
   </TR>
   <TR>
      <TD><font color="blue">Key</font></TD>
      <TD><font color="blue">Value</font></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="passenger_ADT_count"></TD>
      <TD><input type="text" name="appDataValue" value="1"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="passenger_caption_count"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="passenger_caption_0"></TD>
      <TD><input type="text" name="appDataValue" value="Adults"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="passenger_caption_1"></TD>
      <TD><input type="text" name="appDataValue" value="Children"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_caption_count"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="pricing_caption_0"></TD>
      <TD><input type="text" name="appDataValue" value="Outgoing"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_caption_1"></TD>
      <TD><input type="text" name="appDataValue" value="Extras"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_count"></TD>
      <TD><input type="text" name="appDataValue" value="3"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_count"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_0"></TD>
      <TD><input type="text" name="appDataValue" value="221.34"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_1"></TD>
      <TD><input type="text" name="appDataValue" value="56.50"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_2"></TD>
      <TD><input type="text" name="appDataValue" value="12.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_amt_0"></TD>
      <TD><input type="text" name="appDataValue" value="6.00"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_amt_1"></TD>
      <TD><input type="text" name="appDataValue" value="2.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_0"></TD>
      <TD><input type="text" name="appDataValue" value="Adult at �110.67"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_1"></TD>
      <TD><input type="text" name="appDataValue" value="Child at &pound;56.50"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_2"></TD>
      <TD><input type="text" name="appDataValue" value="Taxes & charges"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_dsc_0"></TD>
      <TD><input type="text" name="appDataValue" value="Adult Flight Meal at �6.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_dsc_1"></TD>
      <TD><input type="text" name="appDataValue" value="World Care Fund at �2.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_qty_0"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_qty_0"></TD>
      <TD><input type="text" name="appDataValue" value="1"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="country_count"></TD>
      <TD><input type="text" name="appDataValue" value="1"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="country_code_0"></TD>
      <TD><input type="text" name="appDataValue" value="GB"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="country_dsc_0"></TD>
      <TD><input type="text" name="appDataValue" value="United Kingdom"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_count"></TD>
      <TD><input type="text" name="appDataValue" value="3"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_dsc_0"></TD>
      <TD><input type="text" name="appDataValue" value="Air Passenger Duty - Government"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_amt_0"></TD>
      <TD><input type="text" name="appDataValue" value="20.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_dsc_1"></TD>
      <TD><input type="text" name="appDataValue" value="Fuel Surcharge and Airport Taxes"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_amt_1"></TD>
      <TD><input type="text" name="appDataValue" value="21.62"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_dsc_2"></TD>
      <TD><input type="text" name="appDataValue" value="Total Tax & Fees"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_amt_2"></TD>
      <TD><input type="text" name="appDataValue" value="41.62"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="Is3dEnabled"></TD>
      <TD><input type="text" name="appDataValue" value="false"></TD>
   </TR>
	 <TR>
      <TD><input type="text" name="appDataKey" value="passenger_ADTInsured_1"></TD>
      <TD><input type="text" name="appDataValue" value="TRUE"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="passenger_ADTInsured_2"></TD>
      <TD><input type="text" name="appDataValue" value="FALSE"></TD>
   </TR>
	   <TR>
      <TD><input type="text" name="appDataKey" value="passenger_ADTInsured_3"></TD>
      <TD><input type="text" name="appDataValue" value="TRUE"></TD>
   </TR>
	<TR>
      <TD><input type="text" name="appDataKey" value="passenger_ADTAgeBand_1"></TD>
      <TD><input type="text" name="appDataValue" value="18-64"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="passenger_ADTAgeBand_2"></TD>
      <TD><input type="text" name="appDataValue" value="18-64"></TD>
   </TR>
	   <TR>
      <TD><input type="text" name="appDataKey" value="passenger_ADTAgeBand_3"></TD>
      <TD><input type="text" name="appDataValue" value="65-74"></TD>
   </TR>
     <TR>
      <TD><input type="text" name="appDataKey" value="passenger_CHD_count"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>

    <TR>
      <TD><input type="text" name="appDataKey" value="passenger_CHDInsured_1"></TD>
      <TD><input type="text" name="appDataValue" value="TRUE"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="passenger_CHDInsured_2"></TD>
      <TD><input type="text" name="appDataValue" value="TRUE"></TD>
   </TR>

	<TR>
      <TD><input type="text" name="appDataKey" value="passenger_CHDAgeBand_1"></TD>
      <TD><input type="text" name="appDataValue" value="5"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="passenger_CHDAgeBand_2"></TD>
      <TD><input type="text" name="appDataValue" value="<2"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="passenger_total_count"></TD>
      <TD><input type="text" name="appDataValue" value="6"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="passenger_ADT_count"></TD>
      <TD><input type="text" name="appDataValue" value="4"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="passenger_CHD_count"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
   
               <TR>
      <TD><input type="text" name="appDataKey" value="passenger_1_title"></TD>
      <TD><input type="text" name="appDataValue" value="Mr"></TD>
   </TR>

                  <TR>
      <TD><input type="text" name="appDataKey" value="passenger_1_foreName"></TD>
      <TD><input type="text" name="appDataValue" value="Jeremy1"></TD>
   </TR>
   
                     <TR>
      <TD><input type="text" name="appDataKey" value="passenger_1_lastName"></TD>
      <TD><input type="text" name="appDataValue" value="Smith"></TD>
   </TR>
                  <TR>
      <TD><input type="text" name="appDataKey" value="passenger_2_title"></TD>
      <TD><input type="text" name="appDataValue" value="Mrs"></TD>
   </TR>

                  <TR>
      <TD><input type="text" name="appDataKey" value="passenger_2_foreName"></TD>
      <TD><input type="text" name="appDataValue" value="Jeremy1"></TD>
   </TR>
   
                     <TR>
      <TD><input type="text" name="appDataKey" value="passenger_2_lastName"></TD>
      <TD><input type="text" name="appDataValue" value="Smith"></TD>
   </TR>
                     <TR>
      <TD><input type="text" name="appDataKey" value="passenger_3_title"></TD>
      <TD><input type="text" name="appDataValue" value="Mr"></TD>
   </TR>

                  <TR>
      <TD><input type="text" name="appDataKey" value="passenger_3_foreName"></TD>
      <TD><input type="text" name="appDataValue" value="Jenny"></TD>
   </TR>
   
                     <TR>
      <TD><input type="text" name="appDataKey" value="passenger_3_lastName"></TD>
      <TD><input type="text" name="appDataValue" value="Tate"></TD>
   </TR>

                        <TR>
      <TD><input type="text" name="appDataKey" value="passenger_4_title"></TD>
      <TD><input type="text" name="appDataValue" value="Mrs"></TD>
   </TR>

                  <TR>
      <TD><input type="text" name="appDataKey" value="passenger_4_foreName"></TD>
      <TD><input type="text" name="appDataValue" value="Jenny"></TD>
   </TR>
   
                     <TR>
      <TD><input type="text" name="appDataKey" value="passenger_4_lastName"></TD>
      <TD><input type="text" name="appDataValue" value="Tate"></TD>
   </TR>

                        <TR>
      <TD><input type="text" name="appDataKey" value="passenger_5_title"></TD>
      <TD><input type="text" name="appDataValue" value="Ms"></TD>
   </TR>

                  <TR>
      <TD><input type="text" name="appDataKey" value="passenger_5_foreName"></TD>
      <TD><input type="text" name="appDataValue" value="Jenny"></TD>
   </TR>
   
                     <TR>
      <TD><input type="text" name="appDataKey" value="passenger_5_lastName"></TD>
      <TD><input type="text" name="appDataValue" value="Tate"></TD>
   </TR>

   
                     <TR>
      <TD><input type="text" name="appDataKey" value="passenger_6_title"></TD>
      <TD><input type="text" name="appDataValue" value="Ms"></TD>
   </TR>

                  <TR>
      <TD><input type="text" name="appDataKey" value="passenger_6_foreName"></TD>
      <TD><input type="text" name="appDataValue" value="Anna"></TD>
   </TR>
   
                  <TR>
      <TD><input type="text" name="appDataKey" value="passenger_6_lastName"></TD>
      <TD><input type="text" name="appDataValue" value="Tate"></TD>
   </TR>

</TABLE>
<input type="button" value="Add Row" onClick="javascript:addRow(this)"/>
<input type="submit" value="Submit" />

<HR align="left" width="54%">

<td><font color="red"><strong>Card Types</strong></font><br/>
   <SELECT MULTIPLE name="cardtype" SIZE="11">
   <OPTION VALUE="American Express">American Express
   <OPTION VALUE="ATM ">ATM.
   <OPTION VALUE="Debit Mastercard">Debit Mastercard
   <OPTION VALUE="Diners Club">Diners Club
   <OPTION VALUE="Discover ">Discover
   <OPTION VALUE="EnRoute">EnRoute
   <OPTION VALUE="GE Capital">GE Capital
   <OPTION VALUE="JCB">JCB
   <OPTION VALUE="Laser">Laser
   <OPTION VALUE="Maestro" selected>Maestro
   <OPTION VALUE="Mastercard">Mastercard
   <OPTION VALUE="Platima">Platima
   <OPTION VALUE="Solo">Solo
   <OPTION VALUE="Switch">Switch
   <OPTION VALUE="VISA">VISA
   <OPTION VALUE="VISA Delta">VISA Delta
   <OPTION VALUE="VISA Electron">VISA Electron
   <OPTION VALUE="VISA Purchasing">VISA Purchasing
   </SELECT>
</td>
<HR align="left" width="54%">

</form>
</BODY>
<script language="javascript">
var selectmenu=document.getElementById("clientapplication");

selectmenu.onchange=function(){

	var selectedClient=this.options[this.selectedIndex].value;
	if(this.options[this.selectedIndex].value=="TFly")
	{
	document.location.href ="tflyredesign.jsp";
	}
	
	if(this.options[this.selectedIndex].value=="AtcomRes")
	{
	document.location.href ="atcomres.jsp";
	}
	if(selectedClient=="NEWSKIES")
	{
	   document.location.href="newskies.jsp";
	}

	if(this.options[this.selectedIndex].value=="NEWSKIESM")
	{
		 document.location.href="newskiesm.jsp";
	   
	}
	else
	{
	//   document.location.href ="dynamicDotNetPrePayment.jsp";
	}
}
</script>
</HTML>
