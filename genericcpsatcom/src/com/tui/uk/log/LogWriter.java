/*
* Copyright (C)2008 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
* $RCSfile:   LogWriter.java$
*
* $Revision:   $
*
* $Date:   Jun 13, 2008$
*
* Author: VijayaLakshmi.d
*
*
* $Log:   $
*/
package com.tui.uk.log;

/**
 * The interface for logging DEBUG/INFO/WARNING/ ERROR messages.
 *
 * @author VijayaLakshmi.d@sonata-software.com
 */
public final class LogWriter
{
   /** The keyword to represent the method entry point. */
   private static final String ENTRY_KEYWORD = ":Start";

   /** The keyword to represent the method exit point. */
   private static final String EXIT_KEYWORD = ":End";

   /** Holds reference of logger object. */
   private static Logger logger;

   /** Instantiate logger object.*/
   static
   {
      logger = new LoggerLog4jImpl(LogWriter.class.getName());
   }

   /** Private constructor to prevent instantiation. */
   private LogWriter()
   {
   }

   /**
    * Logs given message at debug level.
    *
    * @param message the message to be logged.
    */
   public static void logDebugMessage(String message)
   {
      logger.logDebugMessage(message);
   }

   /**
    * Logs given message and exception at debug level.
    *
    * @param message the message to be logged.
    * @param throwable the Exception object.
    */
   public static void logDebugMessage(String message, Throwable throwable)
   {
      logger.logDebugMessage(message, throwable);
   }

   /**
    * Logs given message at info level.
    *
    * @param message the message to be logged.
    */
   public static void logInfoMessage(String message)
   {
      logger.logInfoMessage(message);
   }

   /**
    * Logs given message and exception at info level.
    *
    * @param message the message to be logged.
    * @param throwable the Exception object.
    */
   public static void logInfoMessage(String message, Throwable throwable)
   {
      logger.logInfoMessage(message, throwable);
   }

   /**
    * Logs given message at warning level.
    *
    * @param message the message to be logged.
    */
   public static void logWarningMessage(String message)
   {
      logger.logWarningMessage(message);
   }

   /**
    * Logs given message and exception at warning level.
    *
    * @param message the message to be logged.
    * @param throwable the Exception object.
    */
   public static void logWarningMessage(String message, Throwable throwable)
   {
      logger.logWarningMessage(message, throwable);
   }

   /**
    * Logs given message at error level.
    *
    * @param message the message to be logged.
    */
   public static void logErrorMessage(String message)
   {
      logger.logErrorMessage(message);
   }

   /**
    * Logs given message and exception at error level.
    *
    * @param message the message to be logged.
    * @param throwable the Exception object.
    */
   public static void logErrorMessage(String message, Throwable throwable)
   {
      logger.logErrorMessage(message, throwable);
   }

   /**
    * Logs the method entry point.
    *
    * @param methodName the method name.
    */
   public static void logMethodStart(String methodName)
   {
      logger.logDebugMessage(methodName + ENTRY_KEYWORD);
   }

   /**
    * Logs the method entry point with some extra information like parameters and their values.
    *
    * @param methodName the method name.
    * @param extraInfo the extra information.
    */
   public static void logMethodStart(String methodName, String extraInfo)
   {
      logger.logDebugMessage(methodName + ENTRY_KEYWORD + "\r\n" + extraInfo);
   }

   /**
    * Logs the method entry point with passed in parameters.
    *
    * @param methodName the method name.
    * @param params the parameters of the method.
    */
   public static void logMethodStart(String methodName, Object... params)
   {
      logger.logDebugMessage(methodName + ENTRY_KEYWORD + "\r\n" + formatParams(params));
   }

   /**
    * Logs the method exit point.
    *
    * @param methodName the method name.
    */
   public static void logMethodEnd(String methodName)
   {
      logger.logDebugMessage(methodName + EXIT_KEYWORD);
   }

   /**
    * Formats the parameters which are passed in as Object array.
    * The parameters are formated as shown below:
    * <code> param 1 = &lt;value of param1&gt;</code>
    * <code> param 2 = &lt;value of param2&gt;</code>
    *
    * @param params the parameters which need to be printed.
    * @return the formated parameters, if the passed in object array is null or empty then it
    * returns an empty string.
    */
   private static String formatParams(Object[] params)
   {
      StringBuilder buff = new StringBuilder();

      if (params != null)
      {
         for (int i = 0; i < params.length; i++)
         {
            Object param = params[i];
            buff.append("param ").append((i + 1)).append(" = ");

            if (param == null)
            {
               buff.append(param);
            }
            else
            {
               buff.append(param.toString());
            }
            buff.append("\r\n");
         }
      }
      return buff.toString();
   }
}
