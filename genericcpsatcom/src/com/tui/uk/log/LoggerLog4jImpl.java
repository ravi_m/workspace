/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: LoggerLog4jImpl.java$
 *
 * $Revision: $
 *
 * $Date: Jun 13, 2008$
 *
 * Author: VijayaLakshmi.d.
 *
 *
 *
 */
package com.tui.uk.log;

import java.util.Properties;

import org.apache.log4j.Appender;
import org.apache.log4j.AsyncAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.spi.LoggingEvent;

import com.tui.uk.config.AppResource;

/**
 * Log4j implementation of the Logger.
 *
 * @author Vijayalakhsmi.dsonata-software.com
 */

public class LoggerLog4jImpl implements Logger
{
   /** The global logger Object to be used for logging. */
   private org.apache.log4j.Logger globalLogger;

   /** Determines what level of logging should be used( default : WARN ). */
   private Level logLevel = Level.WARN;

   /** Determines whether we should log to console. */
   private boolean consoleOut = false;

   /** Determines whether we should log to file. */
   private boolean fileOut = false;

   /** Determines whether we should use asynchronous logging.  */
   private boolean asyncLogging = false;

   /** Determines whether logging is enabled( default : true ). */
   private boolean globalLoggingEnabled = true;

   /** Fully Qualified Class Name. Will be used as the base name for configuration.*/
   private final String className;

   /**
    * Construct the logger.
    *
    * @param fullyQualifiedClassName the fully qualified class name to use as the base.
    */
   public LoggerLog4jImpl(String fullyQualifiedClassName)
   {
      if (fullyQualifiedClassName == null)
      {
         fullyQualifiedClassName = LoggerLog4jImpl.class.getName();
      }
      className = fullyQualifiedClassName;
      // obtain our main logger
      globalLogger = org.apache.log4j.Logger.getLogger(fullyQualifiedClassName);
      globalLogger.setAdditivity(false);
      // load actual config
      getDefaultConfiguration();
   }

   /**
    * Obtains the default configuration from AppResource. If the behaviour of the log writer has
    * been changed, running this method will return the logger to it's default behaviour
    * ( as per AppResource ).Entries that do not exist in AppResource will be unchanged, and not
    * necessarily set to what they were when the LoggerLog4jImpl was first created.
    */
   public final synchronized void getDefaultConfiguration()
   {
      LogLog.debug("[LoggerLog4jImpl] [" + className + "] restoring default configuration");
      consoleOut = AppResource.getBooleanEntry(className + ".consoleout", consoleOut, null);
      fileOut = AppResource.getBooleanEntry(className + ".fileout", fileOut, null);
      asyncLogging = AppResource.getBooleanEntry(className + ".asyncLogging", asyncLogging, null);
      globalLoggingEnabled = AppResource.getBooleanEntry(className + ".enabled",
          globalLoggingEnabled, null);
      String level = AppResource.getEntry(className + ".LogLevel", logLevel.toString(), null);
      logLevel = Level.toLevel(level);
      // sanity check the enabled state
      if (globalLoggingEnabled)
      {
         globalLoggingEnabled = consoleOut | fileOut;
      }
      configure();
   }

   /**
    *  Build the category string ie., something like "DEBUG#,roll file".
    *  Here DATE prints date and time in the following format: "01 MAY 2002 09:15,123".
    */
   private void configure()
   {
      LogLog.debug("[LoggerLog4jImpl] [" + className + "] configure() enabled="
         + globalLoggingEnabled + ",logLevel=" + logLevel + ",consoleOut=" + consoleOut
         + ",fileOut=" + fileOut);
      Properties props = new Properties();
      String config = logLevel.toString();
      String conversion = AppResource.getEntry(className + ".ConversionPattern",
               "[%d{DATE}] %-5p %x - %m%n", null);
      if (consoleOut)
      {
         config += ",Console";
         props.put("log4j.appender.Console", "org.apache.log4j.ConsoleAppender");
         props.put("log4j.appender.Console.layout", "org.apache.log4j.PatternLayout");
         props.put("log4j.appender.Console.layout.ConversionPattern", conversion);
      }

      // Create and set properties for rollfile appender
      if (fileOut)
      {
         config += ",rollfile";
         props.put("log4j.appender.rollfile", "org.apache.log4j.RollingFileAppender");
         props.put("log4j.appender.rollfile.File", AppResource.getEntry(className + ".filename",
            "../log/tui-server.log", null));
         props.put("log4j.appender.rollfile.MaxFileSize", AppResource.getEntry(className
            + ".maxFileSize", "500KB", null));
         props.put("log4j.appender.rollfile.MaxBackupIndex", AppResource.getEntry(className
            + ".maxBackupSize", "3", null));
         props.put("log4j.appender.rollfile.layout", "org.apache.log4j.PatternLayout");
         props.put("log4j.appender.rollfile.layout.ConversionPattern", conversion);
      }

      // if we enable asyn logging we will need file logging as well
      if (fileOut && asyncLogging)
      {
         config += ",Async";
         props.put("log4j.appender.Async", "org.apache.log4j.AsyncAppender");
         props.put("log4j.appender.Async.BufferSize", AppResource.getEntry(className
            + ".asyncBufferSize", "200", null));
      }

      // Setup Logger category message destinations
      props.put("log4j.category." + className, config);

      // perform configuration.
      globalLogger.removeAllAppenders();
      new PropertyConfigurator().doConfigure(props, globalLogger.getLoggerRepository());

      // This is work around to add rollfile appender to async Appender.Reason - It is not possible
      // using property configurator to add appender to async appender.
      if (asyncLogging && fileOut)
      {
         Appender rollfile = globalLogger.getAppender("rollfile");
         AsyncAppender async = (AsyncAppender) globalLogger.getAppender("Async");
         async.addAppender(rollfile);
         globalLogger.removeAppender(rollfile);
      }

   }

   /**
    * This method logs this message and exception to either the global logger or a user
    * logger or not at all, as appropriate.
    *
    * @param priority hold the priority of logging as DEBUG or INFO level.
    * @param message message to be printed in the log
    * @param throwable the exception to be printed
    */
   public void logMessage(Level priority, String message, Throwable throwable)
   {
      logMessage(priority, message, throwable, null);
   }

   /**
    * This method logs this message and exception to either the global logger or a user logger
    * or not at all, as appropriate.
    *
    * @param priority hold the priority of logging as DEBUG or INFO level.
    * @param message to be printed in the log
    * @param throwable the exception to be printed
    * @param caller if specified the implementation will not throw an exception to find out
    * who was the caller but it will use this value.
    */
   public void logMessage(Level priority, String message, Throwable throwable, String caller)
   {
      // this will either point to the user logger or the global logger or be null
      org.apache.log4j.Logger logger = getLogger();
      if (logger == null)
      {
         // we have no intention of logging anything
         return;
      }

      // check enabling, as would be done by calling logger.log()
      if (!logger.isEnabledFor(priority))
      {
         // escape, avoiding constructing the full message, if we don't plan to log it at all.
         return;
      }

      // prepend call point
      StringBuilder buf = new StringBuilder();
      buf.append("[");
      if (caller == null)
      {
         buf.append(getCaller());
      }
      else
      {
         buf.append(caller);
      }
      buf.append("] [").append(Thread.currentThread().getName()).append("] ").append(message);
      LoggingEvent evt = new LoggingEvent(className, logger, priority, buf.toString(), throwable);
      logger.callAppenders(evt);
   }

   /**
    * Returns the logger to use, based on the state of <code>globalLoggingEnabled</code> However,
    * a <code>null</code> return value indicates that the logger is disabled.
    *
    * @return either a logger or null
    */
   private org.apache.log4j.Logger getLogger()
   {
      if (globalLoggingEnabled)
      {
         return globalLogger;
      }
      else
      {
         return null;
      }
   }

   /**
    * This method return the class of the caller of this method.
    *
    * @return the class of the caller of this method.
    */
   private String getCaller()
   {
      String callStack;
      // share the string writer for same thread
      StackWriterThreadLocal writer = StackWriterThreadLocal.getWriter();
      new Throwable().printStackTrace(writer.getPrintWriter());
      callStack = writer.getStringWriter().toString();
      // reset to empty
      writer.getStringWriter().getBuffer().setLength(0);

      // the last couple of layers were probably internal to the logging.
      // So we want to skip the first few lines.
      int atPos = callStack.indexOf("at ");
      if (atPos < 0)
      {
         return "Call Site Unknown";
      }

      int atNext = callStack.indexOf("at ", atPos + 1);

      // skip over the top few entries that are from the log system.
      while (true)
      {
         if (atNext < 0)
         {
            break;
         }

         String s = callStack.substring(atPos, atNext);

         if (s.indexOf(".log.") < 0)
         {
            break;
         }

         // skip this call site
         atPos = atNext;
         atNext = callStack.indexOf("at ", atPos + 1);
      }

      // extract the class name, typically from "at foo.Bar.alpha( "
      int bPos = callStack.indexOf(" ( ", atPos);
      if (bPos < 0)
      {
         // otherwise look for a line ending
         bPos = callStack.indexOf("\n", atPos);
         if (bPos < 0)
         {
            // if all else fails, return the end of the stack trace
            return callStack.substring(atPos + 2).trim();
         }
      }

      // the expected case
      return callStack.substring(atPos + 2, bPos).trim();
   }

   /**
    * Method to log the debug message.
    *
    * @param message the message to be logged.
    */
   public void logDebugMessage(String message)
   {
      logMessage(Level.DEBUG, message, null);
   }

   /**
    * Method to log the debug message with the exception.
    *
    * @param message the message to be logged.
    * @param throwable the exception to be logged.
    */
   public void logDebugMessage(String message, Throwable throwable)
   {
      logMessage(Level.DEBUG, message, throwable);
   }

   /**
    * Method to log the info message.
    *
    * @param message the message to be logged.
    */
   public void logInfoMessage(String message)
   {
      logMessage(Level.INFO, message, null);
   }

   /**
    * Method to log the info message with the exception.
    *
    * @param message the message to be logged.
    * @param throwable the exception to be logged.
    */
   public void logInfoMessage(String message, Throwable throwable)
   {
      logMessage(Level.INFO, message, throwable);
   }

   /**
    * Method to log the warning message.
    *
    * @param message the message to be logged.
    */
   public void logWarningMessage(String message)
   {
      logMessage(Level.WARN, message, null);
   }

   /**
    * Method to log the warning message with the exception.
    *
    * @param message the message to be logged.
    * @param throwable the exception to be logged.
    */
   public void logWarningMessage(String message, Throwable throwable)
   {
      logMessage(Level.WARN, message, throwable);
   }

   /**
    * Method to log the error message.
    *
    * @param message the message to be logged.
    */
   public void logErrorMessage(String message)
   {
      logMessage(Level.ERROR, message, null);
   }

   /**
    * Method to log the error message with the exception.
    *
    * @param message the message to be logged.
    * @param throwable the exception to be logged.
    */
   public void logErrorMessage(String message, Throwable throwable)
   {
      logMessage(Level.ERROR, message, throwable);
   }

}
