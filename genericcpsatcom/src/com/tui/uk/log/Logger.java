/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: Logger.java$
 *
 * $Revision: 1.0$
 *
 * $Date: Jun 13, 2008$
 *
 * Author: Vijayalakshmi.d@sonata-software.com
 *
 * $Log: $
 */
package com.tui.uk.log;

/**
 * The logger interface.The idea of having this interface is to change to any implementation if
 * needed in future without affecting the application code.
 *
 * @author Vijayalakhsmi.dsonata-software.com
 *
 */
public interface Logger
{

   /**
    * Method to log the debug message.
    *
    * @param message the message to be printed in the log
    */
   void logDebugMessage(String message);

   /**
    * Method to log the debug message with the exception.
    *
    * @param message message to be printed in the log
    * @param throwable the Exception object.
    */
   void logDebugMessage(String message, Throwable throwable);

   /**
    * Method to log the info message.
    *
    * @param message the message to be printed in the log.
    */
   void logInfoMessage(String message);

   /**
    * Method to log the info message with the exception.
    *
    * @param message message to be printed in the log
    * @param throwable the Exception object.
    */
   void logInfoMessage(String message, Throwable throwable);

   /**
    * Method to log the warning message.
    *
    * @param message the message to be printed in the log.
    */
   void logWarningMessage(String message);

   /**
    * Method to log the warning message with the exception.
    *
    * @param message message to be printed in the log
    * @param throwable the Exception object.
    */
   void logWarningMessage(String message, Throwable throwable);

   /**
    * Method to log the error message.
    *
    * @param message the message to be printed in the log.
    */

   void logErrorMessage(String message);

   /**
    * Method to log the error message with the exception.
    *
    * @param message message to be printed in the log
    * @param throwable the Exception object.
    */
   void logErrorMessage(String message, Throwable throwable);

}
