/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: AppResource.java,v $
 *
 * $Revision: 1.3 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: not supported by cvs2svn $ Revision 1.2 2008/05/07 11:18:36
 * sindhushree.g Add file revision history comment.
 *
 */

package com.tui.uk.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;

import com.tui.uk.log.LogWriter;

/**
 * Loads configuration entries/messages for Common Payment Service.
 *
 * @author sindhushree.g
 */
public final class AppResource
{
   /** This stores configuration entries as key/value pair. */
   private static final Properties CONFIG = new Properties();

   /** Application Name, this is used for software configuration entries. */
   private static String appName;

   /** Name of the configuration properties file used. */
   private static String  configFileName;

   /** This stores messages entries as key/value pair. */
   private static final Properties MESSAGES = new Properties();

   /** Name of the message properties file used. */
   private static String  messageResName;

   /**
    * Private constructor to stop instantiation by third parties.
    */
   private AppResource()
   {
      // Nothing to be done.
   }

   /**
    * Sets the application name.
    *
    * @param newAppName the application name to be used
    */
   public static void setAppName(String newAppName)
   {
      appName = newAppName;
   }

   /**
    * Sets the configuration filename.
    *
    * @param filename absolute or relative path.
    */
   public static void setConfigFileName(String filename)
   {
      configFileName = filename;
   }
   /**
    * Sets the message filename.
    *
    * @param filename absolute or relative path.
    */
   public static void setMsgResourceName(String filename)
   {
      messageResName = filename;
   }

   /**
    * Loads configuration  entries.
    *
    * @throws AppResourceException in case of an error.
    */
   public static void loadConfiguration() throws AppResourceException
   {
      loadConfigEntries(CONFIG);
      loadMessages(MESSAGES);
   }

   /**
    * Gets the message.
    *
    * @param key the key.
    * @return String the message.
    */
   public static String getMessage(String key)
   {
      if (!checkConfiguration(MESSAGES))
      {
         try
         {
            loadMessages(MESSAGES);
         }
         catch (AppResourceException cfe)
         {
            LogWriter.logDebugMessage("Failed to load the message resources...........");
            LogWriter.logErrorMessage(cfe.getMessage());
         }
      }
      return MESSAGES.getProperty(key);
   }

   /**
    * Gets entry passing key and defaultStr.
    *
    * @param key the key.
    * @param defaultStr if no value is found.
    *
    * @return the value retrieved from configuration
    */
   public static String getConfEntry(String key, String defaultStr)
   {
      return getEntry(key, defaultStr, null);
   }

   /**
    * Gets entry passing key and defaultStr and locale.
    *
    * @param key the key.
    * @param defaultStr if no value is found.
    * @param locale leave as null if not relevant
    * @return the value retrieved from configuration
    */
   public static String getEntry(String key, String defaultStr, Locale locale)
   {
      if (null == appName)
      {
         appName = System.getProperty("appname");
      }
      String keyname = appName + "." + key;

      String localeSuffix;

      if (locale == null)
      {
         localeSuffix = "";
      }
      else
      {
         localeSuffix = "." + locale.toString();
      }

      String result;
      result = get(keyname + localeSuffix, null);
      if (result != null)
      {
         return result;
      }
      return defaultStr;
   }

   /**
    * Gets entry(int) passing key and defaultStr and locale.
    *
    * @param key the key.
    * @param defaultInt if no value is found.
    * @param locale leave as null if not relevant
    * @return the value retrieved from configuration
    */
   public static int getIntEntry(String key, int defaultInt, Locale locale)
   {
      try
      {
         String value = getEntry(key, null, locale);
         if (value == null)
         {
            return defaultInt;
         }
         else
         {
            return Integer.parseInt(value);
         }
      }
      catch (NumberFormatException nfe)
      {
         LogWriter.logDebugMessage("Failed to retrieve entry for the given key");
         LogWriter.logErrorMessage(nfe.getMessage());
         return defaultInt;
      }
   }

   /**
    * Gets entry(long) passing key and defaultStr and locale.
    *
    * @param key the key.
    * @param defaultLong if no value is found.
    * @param locale leave as null if not relevant
    * @return the value retrieved from configuration
    */
   public static long getLongEntry(String key, long defaultLong, Locale locale)
   {
      try
      {
         String value = getEntry(key, null, locale);
         if (value == null)
         {
            return defaultLong;
         }
         else
         {
            return Long.parseLong(value);
         }
      }
      catch (NumberFormatException nfe)
      {
         LogWriter.logDebugMessage("Failed to retrieve entry for the given key");
         LogWriter.logErrorMessage(nfe.getMessage());
         return defaultLong;
      }
   }

   /**
    * Gets entry(Boolean) passing key and defaultStr and locale.
    *
    * @param key the key.
    * @param defaultBoolean if no value is found.
    * @param locale leave as null if not relevant
    * @return the boolean value retrieved from configuration
    */
   public static boolean getBooleanEntry(String key, boolean defaultBoolean, Locale locale)
   {
      String value = getEntry(key, null, locale);
      if (value == null)
      {
         return defaultBoolean;
      }
      return value.toLowerCase().equals("true");
   }

   /**
    * The method to retrieve an array of values that are stored in the configuration file
    * concatenated using a delimiter.
    *
    * @param key the key.
    * @param defaultValue if no value is found
    * @param delimiter The character or String used as delimitter e.g. "," ,"|" etc OR can be Regex
    * @param locale leave as null if not relevant
    * @return the delimited values as an array of Strings
    */
   public static String[] getStringValues(String key, String[] defaultValue, String delimiter,
             Locale locale)
   {

      String concatenatedEntry = AppResource.getEntry(key, null, locale);
      if (concatenatedEntry == null)
      {
         return defaultValue;
      }
      return concatenatedEntry.trim().split(delimiter);

   }

   /**
    * Helper method which returns an enumeration of all the configuration keys.
    *
    * @param props  the properties.
    * @return all the configuration keys.
    */
   @SuppressWarnings("unchecked")
   public static Enumeration<String> getAllKeys(Properties props)
   {
      if (checkConfiguration(props))
      {
         return (Enumeration<String>) props.propertyNames();
      }
      else
      {
         return null;
      }

   }

   /**
    * Read configuration entries and stores it as key/value pair in CONFIG.
    *
    * @param config the configuration properties.
    * @throws AppResourceException in case of an error.
    */
   private static void loadConfigEntries(Properties config) throws AppResourceException
   {
      loadEntries(config, "configfile", configFileName);
   }

   /**
    * Read message entries and stores it as key/value pair in MESSAGE.
    *
    * @param message the message properties.
    * @throws AppResourceException in case of an error.
    */
   private static void loadMessages(Properties message) throws AppResourceException
   {
      loadEntries(message, "messagefile", messageResName);
   }

   /**
    * The method which loads the configuration/message entries.
    *
    * @param props the properties file.
    * @param name the file name.
    * @param defaultName the default name.
    * @throws AppResourceException the Exception.
    */
   private static void loadEntries(Properties props, String name, String defaultName)
       throws AppResourceException
   {
      String propertyFileName = defaultName;
      props.clear();
      String filename = System.getProperty(name);
      if (filename != null)
      {
         propertyFileName = filename;
      }
      Properties localFileProperties = readPropertyFile(propertyFileName);
      // avoid NPE
      if (localFileProperties != null && !localFileProperties.isEmpty())
      {
         props.putAll(localFileProperties);
      }
   }

   /**
    * This takes a filename and loads it from the file system.
    *
    * @param filename the filename containing configuration
    * @return the properties loaded from file
    * @throws AppResourceException if the configuration cannot be loaded
    */
   private static Properties readPropertyFile(String filename)
      throws AppResourceException
   {
      File file = new File(filename);
      FileInputStream fis = null;
      Properties prop = null;

      try
      {
         fis = new FileInputStream(file);
         prop = new Properties();
         prop.load(fis);
      }
      catch (FileNotFoundException ex)
      {
         throw new AppResourceException("Configuration file (" + file.getAbsolutePath()
            + ") not found, Ensure that the file exists and is readable" + ex.getMessage());
      }
      catch (IOException ex)
      {
         throw new AppResourceException("Configuration file (" + file.getAbsolutePath()
            + ") cannot be read :" + ex.getMessage());
      }
      finally
      {
         if (fis != null)
         {
            try
            {
               fis.close();
            }
            catch (IOException e)
            {
               throw new AppResourceException("Configuration file (" + file.getAbsolutePath()
                  + ") cannot be closed" + e.getMessage());
            }
         }
      }
      return prop;
   }

   /**
    * Returns true if a configuration has been loaded, false if no configuration is present.
    *
    * @param props the properties.
    * @return boolean true.
    */
   private static boolean checkConfiguration(Properties props)
   {
      if (props == null)
      {
         return false;
      }
      synchronized (props)
      {
         return !props.isEmpty();
      }
   }

   /**
    * Obtains the value for a given Configuration Entry, returns defaultStr if the configuration
    * entry does not exist.
    *
    * @param configEntry Configuration Entry, note this is case sensitive.
    * @param defaultStr if no value is found.
    * @return The Value held for that configuration entry.
    */
   private static String get(String configEntry, String defaultStr)
   {
      if (!checkConfiguration(CONFIG))
      {
         try
         {
            loadConfigEntries(CONFIG);
         }
         catch (AppResourceException cfe)
         {
            LogWriter.logDebugMessage("Failed to load the configuration...........");
            LogWriter.logErrorMessage(cfe.getMessage());
            return defaultStr;
         }
      }
      String value = CONFIG.getProperty(configEntry);
      if (value == null)
      {
         return defaultStr;
      }
      else
      {
         return value;
      }
   }

}
