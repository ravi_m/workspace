/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ContextStartup.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-08 10:07:06 $
 *
 * $Author: bibin.j@sonata-software.com $
 *
 * $Log: not supported by cvs2svn $ Revision 1.2 2008/05/07 11:18:36
 * sindhushree.g Add file revision history comment.
 *
 */
package com.tui.uk.startup;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.tui.uk.config.AppResource;
import com.tui.uk.config.AppResourceException;
import com.tui.uk.log.LogWriter;

/**
 * Listener for Common Payment Application.Loads configuration file/message resources.
 * Include listener entry in the web.xml to ensure that this task is executed during application
 * startup.
 *
 * @author author: bibin.j@sonata-software.com
 */
public class ContextStartup implements ServletContextListener
{
   /** The system property that can be used to alter the location of the configuration file.*/
   private static final String CONFIG_FILEPATH_SYSTEM_PROPERTY = "config.file.path";

   /** The default configuration file location. */
   private static final String DEFAULT_CONFIG_FILEPATH = "../../conf/";

   /** The system property that can be used to alter the location of the message resources file. */
   private static final String MESSAGE_FILEPATH_SYSTEM_PROPERTY = "messages.file.path";

   /** The default message resources file location. */
   private static final String DEFAULT_MESSAGE_FILEPATH = "WEB-INF/classes/";

   /**
    * Loads the application configuration/message resources file.
    * a)If the system property "config.file.path" is set,this is used as the file path suffix,
    * otherwise the default value "../../conf/[appname].conf" is used.This suffix value is appended
    * to the servlet real path to obtain the path for the configuration file.
    * b)For loading message resources it is assumed that message resources file is placed in the
    * classpath.
    *
    *
    * @param contextEvent the event triggering this listener
    */
   public final void contextInitialized(ServletContextEvent contextEvent)
   {
      String appname = contextEvent.getServletContext().getInitParameter("APP-NAME");
      String msgResourcesName = contextEvent.getServletContext().getInitParameter("APP-RESOURCES");
      System.out.println("Loading the configuration for " + appname + ".............");
      AppResource.setAppName(appname);
      String filepathSuffix = System.getProperty(CONFIG_FILEPATH_SYSTEM_PROPERTY,
         DEFAULT_CONFIG_FILEPATH + appname.toLowerCase() + ".conf");
      AppResource.setConfigFileName(contextEvent.getServletContext().getRealPath(
         File.separator) + filepathSuffix);

      filepathSuffix = System.getProperty(MESSAGE_FILEPATH_SYSTEM_PROPERTY,
         DEFAULT_MESSAGE_FILEPATH + msgResourcesName.toLowerCase() + ".properties");
      AppResource.setMsgResourceName(contextEvent.getServletContext().getRealPath(
         File.separator) + filepathSuffix);

      try
      {
         AppResource.loadConfiguration();
         LogWriter.logInfoMessage(appname + " loaded sucessfully.............");
      }
      catch (AppResourceException cfe)
      {
         LogWriter.logErrorMessage(cfe.getMessage(), cfe);
         throw new RuntimeException(cfe);
      }
   }

   /**
    * Unloads the application when the context is destroyed.
    *
    * @param contextEvent the event instance for context destruction
    */
   public final void contextDestroyed(ServletContextEvent contextEvent)
   {
      String appname = contextEvent.getServletContext().getInitParameter("APP-NAME");
      LogWriter.logInfoMessage("UnLoaded the application " + appname + ".............");
   }
}
