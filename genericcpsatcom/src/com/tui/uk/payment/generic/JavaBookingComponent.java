/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
 */
package com.tui.uk.payment.generic;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import com.tui.uk.client.domain.AbtaDetails;
import com.tui.uk.client.domain.Accommodation;
import com.tui.uk.client.domain.AccommodationSummary;
import com.tui.uk.client.domain.AgeClassification;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.BookingData;
import com.tui.uk.client.domain.CardType;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.CruiseSummary;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.DiscountComponent;
import com.tui.uk.client.domain.FlightDetails;
import com.tui.uk.client.domain.FlightSummary;
import com.tui.uk.client.domain.ImportantInformation;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PackageType;
import com.tui.uk.client.domain.PassengerSummary;
import com.tui.uk.client.domain.PaymentType;
import com.tui.uk.client.domain.PriceComponent;
import com.tui.uk.client.domain.ShopDetails;
import com.tui.uk.log.LogWriter;

/**
 * This JavaBookingComponent class reads the booking component data from
 * request parameters.
 */
public class JavaBookingComponent
{

   /**
    * A three character ISO currency indicator. For most applications this
    * will be "GBP" or "EUR".
    */
   private static final Currency GBP = Currency.getInstance(Locale.UK);

   /** The constant for cruisesummary_duration. */
   private static final String CRUISE_SUMMARY_DURATION =
      "cruisesummary_duration";

   /** The constant for passengersummary_ageclassification_1. */
   private static final String PASSENGERSUMMARY_AGECLARRIFICATION_1 =
      "passengersummary_ageclassification_1";

   /** The constant for passengersummary_ageclassification_1. */
   private static final String PASSENGERSUMMARY_AGECLARRIFICATION_2 =
      "passengersummary_ageclassification_2";

   /** The constant for Persons Title. */
   private static final String PERSONALEDETAILS_0_TITLE =
      "personaldetails_0_title";

   /** The constant for Persons Surname. */
   private static final String PERSONALEDETAILS_1_SURNAME =
      "personaldetails_1_surName";

   /** The constant for Persons Surname. */
   private static final String PERSONALEDETAILS_0_SURNAME =
      "personaldetails_0_surName";

   /** The constant for Persons Surname. */
   private static final String PERSONALEDETAILS_2_SURNAME =
      "personaldetails_2_surName";

   /** The constant for Persons Title. */
   private static final String PERSONALEDETAILS_1_TITLE =
      "personaldetails_1_title";

   /** The constant for Persons Title. */
   private static final String PERSONALEDETAILS_2_TITLE =
      "personaldetails_2_title";

   /** The constant for First Persons ForeName. */
   private static final String PERSONALEDETAILS_0_FORENAME =
      "personaldetails_0_foreName";

   /** The constant for Second Persons ForeName. */
   private static final String PERSONALEDETAILS_1_FORENAME =
      "personaldetails_1_foreName";

   /** The constant for Third Persons ForeName. */
   private static final String PERSONALEDETAILS_2_FORENAME =
      "personaldetails_2_foreName";

   /** The constant for First Persons MiddleIntial. */
   private static final String PERSONALEDETAILS_0_MIDDLEINTIAL =
      "personaldetails_0_middleInitial";

   /** The constant for Second Persons MiddleIntial. */
   private static final String PERSONALEDETAILS_1_MIDDLEINTIAL =
      "personaldetails_1_middleInitial";

   /** The constant for Second Persons MiddleIntial. */
   private static final String PERSONALEDETAILS_2_MIDDLEINTIAL =
      "personaldetails_2_middleInitial";

   /** The constant for Seperator. */
   private static final String SPLIT_TOKEN = "\\|";

   /** The <code>BookingComponent</code> object. */
   private BookingComponent bookingComponent;;

   /**
    * The constructor for creating the <code>BookingDetails</code>
    * object.
    *
    * @param requestParameterMap the map containing request field name as
    *           key and value as its corresponding entry.
    * @param clientUrl the domain url of the client.
    *
    * @throws IOException the IOException thrown if an input or output
    *            error is detected when the servlet handles the request
    *
    */
   public JavaBookingComponent(Map<String, String[]> requestParameterMap,
                               String clientUrl) throws IOException
   {
      BigDecimal amount =
         new BigDecimal(
            requestParameterMap.get("bookingcomponent_amount")[0]);
      Money amount1 = new Money(amount, GBP);
      bookingComponent =
        new BookingComponent(ClientApplication
            .findByCode(requestParameterMap.get("clientapplication")[0]),
            clientUrl, amount1);

      bookingComponent.setCallType(requestParameterMap.get("callType")[0]);
      bookingComponent
         .setSearchType(requestParameterMap.get("searchType")[0]);
      bookingComponent.setNoOfTransactions(Integer
         .parseInt(requestParameterMap.get("noOfTransactions")[0]));
      bookingComponent.setMaxCardCharge(new BigDecimal(requestParameterMap
         .get("maxCardCharge")[0]));
      bookingComponent.setMaxCardChargePercent(new BigDecimal(
         requestParameterMap.get("maxCardChargePercent")[0]));
      bookingComponent.setPackageType(PackageType
         .findByCode(requestParameterMap.get("packageType")[0]));
      bookingComponent.setAbtaDetails(new AbtaDetails(requestParameterMap
         .get("abtaNumber")[0], requestParameterMap.get("abtaUserId")[0],
         requestParameterMap.get("tracsWebSiteId")[0]));
      bookingComponent.setTracsBookMinusId(requestParameterMap
         .get("TracsBookMinusId")[0]);
      bookingComponent.setPayableAmount(amount1);
      bookingComponent.setErrorMessage(requestParameterMap
         .get("ErrorMessage")[0]);
      bookingComponent.setShopDetails(new ShopDetails(requestParameterMap
         .get("shopId")[0], requestParameterMap.get("shopId")[0]));
      //bookingComponent.setPaymentFailureURL(requestParameterMap.get("payment_failure_url")[0]);
      //bookingComponent.setPaymentSuccessUrl(requestParameterMap.get("payment_sucess_url")[0]);
      //bookingComponent.setPrePaymentUrl(requestParameterMap.get("pre_payment_url")[0]);
      List<String> a = new ArrayList<String>();
      a.add("CHECK WITH MOROCCON EMBASSY CHILDRENS ENTRY CONDITIONS."
         + "LICENSING LAWS PREVENT MOROCCAN NATIONALS FROM BEING ABLE TO TRAVEL ON THIS FLIGHT.");
      ImportantInformation imp = new ImportantInformation(a);
      bookingComponent.setImportantInformation(imp);
      setBookingComponent(requestParameterMap);
   }

   /**
    * Sets the other bookingcomponent data.
    *
    * @param requestParameterMap reads the request data.
    */
   private void setBookingComponent(
      Map<String, String[]> requestParameterMap)
   {
      try
      {
         setAccommodationSummary(requestParameterMap);
         setCruiseSummary(requestParameterMap);
         setBookingDetails(requestParameterMap);
         setPriceComponents(requestParameterMap);
         setPricingDetails();
         setDepostiComponents(requestParameterMap);
         setPassengerRoomSummary(requestParameterMap);
         // setSourceOfBusiness(bc, requestParameterMap);
         setFlightSummary(requestParameterMap);
         setDiscountInformation(requestParameterMap);
         setCardChargesMap(requestParameterMap);
         setCountryMap(requestParameterMap);
         setNonPaymentData(requestParameterMap);
         setPaymenttypes(requestParameterMap);
         setExtraDetails(requestParameterMap);
         setBreadCrumbUrl();
      }
      catch (ParseException pe)
      {
         LogWriter.logErrorMessage(pe.getMessage(), pe);
         throw new RuntimeException(pe.getMessage());
      }
   }

   /**
    * sets the AccommodationSummary.
    *
    * @param requestParameterMap to read request data.
    *
    * @throws ParseException if invalid date is given.
    */
   private void setAccommodationSummary(
      Map<String, String[]> requestParameterMap) throws ParseException
   {
      Accommodation accommodation =
         new Accommodation(requestParameterMap
            .get("accommodationsummary_country")[0], requestParameterMap
            .get("accommodationsummary_destination")[0],
            requestParameterMap.get("accommodationsummary_resort")[0],
            requestParameterMap.get("accommodationsummary_hotel")[0],
            requestParameterMap.get("accommodationsummary_boardBasis")[0]);
      accommodation.setRating(requestParameterMap
         .get("accommodationsummary_rating")[0]);
      accommodation.setRoomDescription(requestParameterMap
         .get("accommodationsummary_RoomDescription")[0]);
      AccommodationSummary accommodationSummary =
         new AccommodationSummary(
            accommodation,
            Integer.parseInt(requestParameterMap
               .get("accommodationsummary_duration")[0]),
            Boolean.valueOf(requestParameterMap
               .get("accommodationsummary_accommodationSelected")[0]),
               "HOPLA_HOTELBEDS");
      accommodationSummary
         .setStartDate(GenericConstants.FORMATDATE
            .parse(requestParameterMap
               .get("accommodationsummary_startDate")[0]));
      accommodationSummary
         .setEndDate(GenericConstants.FORMATDATE
            .parse((requestParameterMap
               .get("accommodationsummary_endDate")[0])));
      accommodationSummary.setPrepay(Boolean.valueOf(requestParameterMap
         .get("accommodationsummary_isPrepay")[0]));
      accommodationSummary.setHoplaBonded(Boolean.TRUE);
      bookingComponent.setAccommodationSummary(accommodationSummary);
      bookingComponent.setPaymentGatewayVirtualTerminalId("99639700");
   }

   /**
    * sets the CruiseSummary.
    *
    * @param requestParameterMap to read request data.
    *
    * @throws ParseException if invalid date is given.
    *
    */
   private void setCruiseSummary(Map<String, String[]> requestParameterMap)
      throws ParseException
   {
      Accommodation ship =
         new Accommodation(requestParameterMap
            .get("cruisesummary_ship_country")[0], requestParameterMap
            .get("cruisesummary_ship_destination")[0], requestParameterMap
            .get("cruisesummary_ship_resort")[0], requestParameterMap
            .get("cruisesummary_ship_hotel")[0], requestParameterMap
            .get("cruisesummary_ship_boardBasis")[0]);
      Accommodation accommodation =
         new Accommodation(requestParameterMap
            .get("cruisesummary_stayacommodation_country")[0],
            requestParameterMap
               .get("cruisesummary_stayacommodation_destination")[0],
            requestParameterMap
               .get("cruisesummary_stayacommodation_resort")[0],
            requestParameterMap
               .get("cruisesummary_stayacommodation_hotel")[0],
            requestParameterMap
               .get("cruisesummary_stayacommodation_boardBasis")[0]);
      accommodation.setRating(requestParameterMap
         .get("cruisesummary_stayacommodation_rating")[0]);
      accommodation.setRoomDescription(requestParameterMap
         .get("cruisesummary_stayacommodation_RoomDescription")[0]);
      AccommodationSummary accommodationSummary =
         new AccommodationSummary(accommodation,
            Integer.parseInt(requestParameterMap
               .get(CRUISE_SUMMARY_DURATION)[0]), Boolean
               .valueOf(requestParameterMap
                  .get("cruisesummary_accommodationSelected")[0]), "HOPLA_HOTELBEDS");
      accommodationSummary.setStartDate(GenericConstants.FORMATDATE
         .parse(requestParameterMap.get("cruisesummary_startDate")[0]));
      accommodationSummary.setEndDate(GenericConstants.FORMATDATE
         .parse(requestParameterMap.get("cruisesummary_endDate")[0]));
      accommodationSummary.setPrepay(Boolean.valueOf(requestParameterMap
         .get("cruisesummary_isPrepay")[0]));
      accommodationSummary.setHoplaBonded(Boolean.TRUE);
      bookingComponent.setAccommodationSummary(accommodationSummary);
      bookingComponent.setCruiseSummary(new CruiseSummary(ship, Integer
         .parseInt(requestParameterMap.get(CRUISE_SUMMARY_DURATION)[0]),
         accommodation));
   }

   /**
    * sets the bookingdetails.
    *
    * @param requestParameterMap to read request data.
    *
    * @throws ParseException if invalid date is given.
    */
   private void setBookingDetails(Map<String, String[]> requestParameterMap)
      throws ParseException
   {
      bookingComponent.setBookingData(new BookingData(requestParameterMap
         .get("bookingdetails_bookingReference")[0], requestParameterMap
         .get("bookingdetails_bookingStatus")[0],
         GenericConstants.FORMATDATE.parse(requestParameterMap
            .get("bookingdetails_bookingdate")[0])));

   }

   /**
    * sets the priceComponents.
    *
    * @param requestParameterMap to read request data
    */
   private void setPriceComponents(
      Map<String, String[]> requestParameterMap)
   {
      BigDecimal amount1 =
         new BigDecimal(
            requestParameterMap.get("pricecomponent_amount_1")[0]);
      BigDecimal amount2 =
         new BigDecimal(
            requestParameterMap.get("pricecomponent_amount_2")[0]);
      BigDecimal amount3 =
         new BigDecimal(
            requestParameterMap.get("pricecomponent_amount_3")[0]);
      List<PriceComponent> priceComponents =
         new ArrayList<PriceComponent>();
      PriceComponent priceComponent1 =
         new PriceComponent(requestParameterMap
            .get("pricecomponent_itemDescription_1")[0], new Money(
            amount1, GBP));
      PriceComponent priceComponent2 =
         new PriceComponent(requestParameterMap
            .get("pricecomponent_itemDescription_2")[0], new Money(
            amount2, GBP));
      PriceComponent priceComponent3 =
         new PriceComponent(requestParameterMap
            .get("pricecomponent_itemDescription_3")[0], new Money(
            amount3, GBP));
      priceComponent1.setQuantity(Integer.parseInt(requestParameterMap
         .get("pricecomponent_quantity_1")[0]));
      priceComponent1.setDiscount(Boolean.valueOf(requestParameterMap
         .get("pricecomponent_discount_1")[0]));
      priceComponent2.setQuantity(Integer.parseInt(requestParameterMap
         .get("pricecomponent_quantity_2")[0]));
      priceComponent2.setDiscount(Boolean.valueOf(requestParameterMap
         .get("pricecomponent_discount_2")[0]));
      priceComponent3.setQuantity(Integer.parseInt(requestParameterMap
         .get("pricecomponent_quantity_3")[0]));
      priceComponent3.setDiscount(Boolean.valueOf(requestParameterMap
         .get("pricecomponent_discount_3")[0]));
      priceComponents.add(priceComponent2);
      priceComponents.add(priceComponent1);
      priceComponents.add(priceComponent3);
      bookingComponent.setPriceComponents(priceComponents);
   }

   /**
    * sets the depositComponents.
    *
    * @param requestParameterMap to read request data.
    *
    * @throws ParseException if invalid date is given.
    */
   private void setDepostiComponents(
      Map<String, String[]> requestParameterMap) throws ParseException
   {
      BigDecimal amount2 =
         new BigDecimal(requestParameterMap
            .get("depositcomponents_amount")[0]);
      List<DepositComponent> depositComponentList =
         new ArrayList<DepositComponent>();
      depositComponentList.add(new DepositComponent(requestParameterMap
         .get("depositcomponents_depositType")[0],
         new Money(amount2, GBP), GenericConstants.FORMATDATE
            .parse(requestParameterMap
               .get("depositcomponents_depositDueDate")[0]), new Money(
            amount2, GBP)));
      bookingComponent.setDepositComponents(depositComponentList);
   }

   /**
    * sets the details of the passengers.
    *
    * @param requestParameterMap to read request data
    */
   private void setPassengerRoomSummary(
      Map<String, String[]> requestParameterMap)
   {
      Map<Integer, List<PassengerSummary>> passengerRoomSummary =
         new TreeMap<Integer, List<PassengerSummary>>();
      PassengerSummary passengerSummary =
         new PassengerSummary(AgeClassification
            .findByCode(requestParameterMap
               .get(PASSENGERSUMMARY_AGECLARRIFICATION_1)[0]), Boolean
            .valueOf(requestParameterMap
               .get("passengersummary_leadPassenger_1")[0]), Integer
            .parseInt(requestParameterMap
               .get("passengersummary_identifier_1")[0]));
      PassengerSummary passengerSummary1 =
         new PassengerSummary(AgeClassification
            .findByCode(requestParameterMap
               .get(PASSENGERSUMMARY_AGECLARRIFICATION_2)[0]), Boolean
            .valueOf(requestParameterMap
               .get("passengersummary_leadPassenger_2")[0]), Integer
            .parseInt(requestParameterMap
               .get("passengersummary_identifier_2")[0]));
      passengerSummary.setLabel(requestParameterMap
         .get(PASSENGERSUMMARY_AGECLARRIFICATION_1)[0] + 1);
      passengerSummary1.setLabel(requestParameterMap
         .get(PASSENGERSUMMARY_AGECLARRIFICATION_2)[0] + 2);
      List<PassengerSummary> passengerSummaryList =
         new ArrayList<PassengerSummary>();
      passengerSummaryList.add(passengerSummary);
      passengerSummaryList.add(passengerSummary1);
      passengerRoomSummary.put(1,
         (List<PassengerSummary>) passengerSummaryList);
      bookingComponent.setPassengerRoomSummary(passengerRoomSummary);
   }

   /**
    * sets the FlightSummary.
    *
    * @param requestParameterMap to read request data
    */
   private void setFlightSummary(Map<String, String[]> requestParameterMap)
   {
      List<FlightDetails> outBoundFlights = new ArrayList<FlightDetails>();
      List<FlightDetails> inBoundFlights = new ArrayList<FlightDetails>();
      FlightDetails outBoundFlight =
         new FlightDetails(
            requestParameterMap.get("outboundflight_departureAirportName")[0],
            new Date(Long.valueOf(requestParameterMap
               .get("outboundflight_departureDateTime")[0])),
            requestParameterMap.get("outboundflight_arrivalAirportName")[0],
            new Date(Long.valueOf(requestParameterMap
               .get("outboundflight_arrivalDateTime")[0])),
            requestParameterMap.get("outboundflight_carrier")[0],
            requestParameterMap.get("outboundflight_operatingAirlineCode")[0],
            requestParameterMap.get("outboundflight_flightNumber")[0]);
      FlightDetails inBoundFlight =
         new FlightDetails(
            requestParameterMap.get("inboundflight_departureAirportName")[0],
            new Date(Long.valueOf(requestParameterMap
               .get("inboundflight_departureDateTime")[0])),
            requestParameterMap.get("inboundflight_arrivalAirportName")[0],
            new Date(Long.valueOf(requestParameterMap
               .get("inboundflight_arrivalDateTime")[0])),
            requestParameterMap.get("inboundflight_carrier")[0],
            requestParameterMap.get("inboundflight_operatingAirlineCode")[0],
            requestParameterMap.get("inboundflight_flightNumber")[0]);
      outBoundFlights.add(outBoundFlight);
      outBoundFlights.add(inBoundFlight);
      inBoundFlights.add(outBoundFlight);
      inBoundFlights.add(inBoundFlight);
      bookingComponent.setFlightSummary(new FlightSummary(outBoundFlights,
         inBoundFlights, requestParameterMap
            .get("flightsummary_flightSupplierSystem")[0], Boolean
            .valueOf(requestParameterMap
               .get("flightsummary_flightSelected")[0])));
      List<String> flightExtras = new ArrayList<String>();
      flightExtras.add("Baggage allowance");
      flightExtras.add("Adult 1 - 20kg");
      flightExtras.add("Adult 2 - 25kg");
      flightExtras.add("Senior Citizen 1 - 20kg");
      flightExtras.add("Senior Citizen 2 - 25kg");
      flightExtras.add("Child 1  (Age 10) - 20kg");
      flightExtras.add("Child 2  (Age 6) - 25kg");
      bookingComponent.getFlightSummary().setFlightExtras(flightExtras);
   }

   /**
    * sets the discount information applicable for the client.
    *
    * @param requestParameterMap to read request data
    */
   private void setDiscountInformation(
      Map<String, String[]> requestParameterMap)
   {
      BigDecimal amount1 =
         new BigDecimal(requestParameterMap
            .get("discounttype_maxDiscountAmount")[0]);
      BigDecimal amount2 =
         new BigDecimal(requestParameterMap
            .get("discounttype_maxDiscountAmount_1")[0]);
      List<DiscountComponent> discountComponents =
         new ArrayList<DiscountComponent>();
      discountComponents.add(new DiscountComponent(requestParameterMap
         .get("discounttype_discountType")[0], new Money(amount1, GBP),
         Boolean.valueOf(requestParameterMap
            .get("discounttype_applicableForHotel")[0]), Boolean
            .valueOf(requestParameterMap
               .get("discounttype_applicableForFlight")[0])));
      discountComponents.add(new DiscountComponent(requestParameterMap
         .get("discounttype_discountType_1")[0], new Money(amount2, GBP),
         Boolean.valueOf(requestParameterMap
            .get("discounttype_applicableForHotel_1")[0]), Boolean
            .valueOf(requestParameterMap
               .get("discounttype_applicableForFlight_1")[0])));
   }

   /**
    * sets the card charge percent for the each of acceptable card by the
    * client.
    *
    * @param requestParameterMap to read request data
    */
   private void setCardChargesMap(Map<String, String[]> requestParameterMap)
   {
      Map<String, BigDecimal> cardChargesMap =
         new HashMap<String, BigDecimal>();
      String[] cnp = null;
      String[] eachPayment = null;
      cnp = requestParameterMap.get("Paymenttypes_CNP");
      for (int count = 0; count < cnp.length; count++)
      {
         eachPayment = cnp[count].split(SPLIT_TOKEN);
         if (Boolean.valueOf(eachPayment[2]) == Boolean.TRUE)
         {
            cardChargesMap.put(eachPayment[0], new BigDecimal(
               GenericConstants.CARDCHARGE));
         }
         else
         {
            cardChargesMap.put(eachPayment[0], new BigDecimal(0));
         }
      }
      bookingComponent.setCardChargesMap(cardChargesMap);
   }

   /**
    * sets the list of countries accepted by the Client.
    *
    * @param requestParameterMap to read request data
    */
   private void setCountryMap(Map<String, String[]> requestParameterMap)
   {
      Map<String, String> countryMap = new HashMap<String, String>();
      String[] countrydata = null;
      String[] eachCountry = null;
      countrydata = requestParameterMap.get("country");
      for (int count = 0; count < countrydata.length; count++)
      {
         eachCountry = countrydata[count].split("-");
         countryMap.put(eachCountry[0], eachCountry[1]);
      }
      bookingComponent.setCountryMap(countryMap);
   }

   /**
    * sets the nonpaymentdata such as passenger details and the address.
    *
    * @param requestParameterMap to read request data
    */
   private void setNonPaymentData(Map<String, String[]> requestParameterMap)
   {
      HashMap<String, String> nonPaymentData =
         new HashMap<String, String>();
      nonPaymentData.put(PERSONALEDETAILS_1_FORENAME, requestParameterMap
         .get(PERSONALEDETAILS_1_FORENAME)[0]);
      nonPaymentData.put(PERSONALEDETAILS_0_TITLE, requestParameterMap
         .get(PERSONALEDETAILS_0_TITLE)[0]);
      nonPaymentData.put(PERSONALEDETAILS_0_FORENAME, requestParameterMap
         .get(PERSONALEDETAILS_0_FORENAME)[0]);
      nonPaymentData.put(PERSONALEDETAILS_1_MIDDLEINTIAL,
         requestParameterMap.get(PERSONALEDETAILS_1_MIDDLEINTIAL)[0]);
      nonPaymentData.put(PERSONALEDETAILS_1_TITLE, requestParameterMap
         .get(PERSONALEDETAILS_1_TITLE)[0]);
      nonPaymentData.put(PERSONALEDETAILS_0_MIDDLEINTIAL,
         requestParameterMap.get(PERSONALEDETAILS_0_MIDDLEINTIAL)[0]);
      nonPaymentData.put(PERSONALEDETAILS_1_SURNAME, requestParameterMap
         .get(PERSONALEDETAILS_1_SURNAME)[0]);
      nonPaymentData.put(PERSONALEDETAILS_0_SURNAME, requestParameterMap
         .get(PERSONALEDETAILS_0_SURNAME)[0]);
      nonPaymentData.put(PERSONALEDETAILS_2_FORENAME, requestParameterMap
         .get(PERSONALEDETAILS_2_FORENAME)[0]);
      nonPaymentData.put(PERSONALEDETAILS_2_TITLE, requestParameterMap
         .get(PERSONALEDETAILS_2_TITLE)[0]);
      nonPaymentData.put(PERSONALEDETAILS_2_SURNAME, requestParameterMap
         .get(PERSONALEDETAILS_2_SURNAME)[0]);
      nonPaymentData.put(PERSONALEDETAILS_2_MIDDLEINTIAL,
         requestParameterMap.get(PERSONALEDETAILS_2_MIDDLEINTIAL)[0]);
      nonPaymentData.put(BookingConstants.STREET_ADDRESS1, "67");
      nonPaymentData.put(BookingConstants.STREET_ADDRESS2, "Wigmore House");
      nonPaymentData.put(BookingConstants.STREET_ADDRESS3, "Luton");
      nonPaymentData.put(BookingConstants.STREET_ADDRESS4, "London");
      nonPaymentData.put(BookingConstants.CARD_BILLING_POSTCODE, "cv14aq");
      nonPaymentData.put(BookingConstants.SELECTED_COUNTRY_CODE, "GB");
      nonPaymentData.put(BookingConstants.SELECTED_COUNTRY, "United Kingdom");
      nonPaymentData.put(BookingConstants.ADDITIONAL_ADDRESS_LINE1, "Wigmore Lane");
      nonPaymentData.put(BookingConstants.ADDITIONAL_ADDRESS_LINE2, "wigmore");
      bookingComponent.setNonPaymentData(nonPaymentData);
   }

   /**
    * Method is used to set the acceptable payment types for an
    * application.
    *
    * @param requestParameterMap to read request data
    */
   private void setPaymenttypes(Map<String, String[]> requestParameterMap)
   {
      Map<String, List<PaymentType>> paymentType =
         new HashMap<String, List<PaymentType>>();
      String[] cp = null;
      String[] cnp = null;
      String[] cpnp = null;
      String[] eachPayment = null;
      List<PaymentType> cpList = new ArrayList<PaymentType>();
      List<PaymentType> cnpList = new ArrayList<PaymentType>();
      List<PaymentType> cpnpList = new ArrayList<PaymentType>();
      cp = requestParameterMap.get("Paymenttypes_CP");
      cnp = requestParameterMap.get("Paymenttypes_CNP");
      cpnp = requestParameterMap.get("Paymenttypes_CPNA");
      PaymentType ptype;
      CardType ctype;
      for (int count = 0; count < cp.length; count++)
      {
         eachPayment = cp[count].split(SPLIT_TOKEN);
         ptype =
            new PaymentType(eachPayment[0], eachPayment[1],
               eachPayment[GenericConstants.INDEX_FOUR]);
         ctype =
            new CardType(Integer
               .parseInt(eachPayment[GenericConstants.INDEX_THREE]));
         ptype.setCardType(ctype);
         cpList.add(ptype);
      }
      paymentType.put("CP", cpList);
      for (int count = 0; count < cnp.length; count++)
      {
         eachPayment = cnp[count].split(SPLIT_TOKEN);
         ptype =
            new PaymentType(eachPayment[0], eachPayment[1], "Dcard");
         ctype =
            new CardType(Integer
               .parseInt(eachPayment[GenericConstants.INDEX_FIVE]));
         ctype.setIsIssueNumberRequired(Boolean
            .valueOf(eachPayment[GenericConstants.INDEX_SEVEN]));
         ctype.setIsStartDateRequired(Boolean
            .valueOf(eachPayment[GenericConstants.INDEX_SIX]));
         ptype.setCardType(ctype);
         cnpList.add(ptype);
      }
      paymentType.put("CNP", cnpList);
      for (int count = 0; count < cpnp.length; count++)
      {
         eachPayment = cpnp[count].split(SPLIT_TOKEN);
         ptype =
            new PaymentType(eachPayment[0], eachPayment[1],
               eachPayment[GenericConstants.INDEX_FOUR]);
         ctype =
            new CardType(Integer
               .parseInt(eachPayment[GenericConstants.INDEX_THREE]));
         ptype.setCardType(ctype);
         cpnpList.add(ptype);
      }
      paymentType.put("CPNA", cpnpList);
      bookingComponent.setPaymentType(paymentType);
   }

   /**
    * sets the list of ExtraDetails accepted by the Client.
    *
    * @param requestParameterMap to read request data
    */
   private void setExtraDetails(Map<String, String[]> requestParameterMap)
   {
      Map<String, List<String>> extraFacilities = new HashMap<String, List<String>>();
      List<String> extrainfo = new ArrayList<String>();
      extrainfo.add("Reserved Seating");
      extrainfo.add("Inflight meals");
      extrainfo.add("Ski carriage");
      extrainfo.add("Watersports carriage");
      extrainfo.add("Champagne");
      extraFacilities.put("ExtraDetails", extrainfo);
      bookingComponent.setExtraFacilities(extraFacilities);
   }


   /**
    * Sets the breadcrumb url's.
    *
    */
   private void setBreadCrumbUrl()
   {
      Map<String, String> breadCrumb = new HashMap<String, String>();
      breadCrumb.put("Who's travelling", "http://localhost:8080/whoistravelling");
      breadCrumb.put("Personal details", "http://localhost:8080/personaldetails");
      breadCrumb.put("Payment", "");
      breadCrumb.put("Confirmation", "");
      bookingComponent.setBreadCrumbTrail(breadCrumb);
   }

   /**
    * sets the list of Pricing details.
    *
    */
   private void setPricingDetails()
   {
      Map<String, List<PriceComponent>> pricingDetails
         = new HashMap<String, List<PriceComponent>>();
      List<PriceComponent> pricecomponent1 = new ArrayList<PriceComponent>();
      PriceComponent p1 = new PriceComponent("Jewels of the Mediterannean",
         new Money(BigDecimal.ZERO, GBP));
      pricecomponent1.add(p1);
      List<PriceComponent> pricecomponent2 = new ArrayList<PriceComponent>();
      PriceComponent p2 = new PriceComponent("2 adults, 1 child",
         new Money(new BigDecimal("200"), GBP));
      pricecomponent2.add(p2);
      List<PriceComponent> pricecomponent3 = new ArrayList<PriceComponent>();
      PriceComponent p3 = new PriceComponent("2 adults, 1 child",
         new Money(new BigDecimal("200"), GBP));
      pricecomponent3.add(p3);
      List<PriceComponent> pricecomponent4 = new ArrayList<PriceComponent>();
      PriceComponent p4 = new PriceComponent("2 adults, 1 child",
         new Money(new BigDecimal("200"), GBP));
      pricecomponent4.add(p4);
      pricingDetails.put("Palma, Majorca", pricecomponent1);
      pricingDetails.put("Barcelona City Tour", pricecomponent2);
      pricingDetails.put("Majorca City Tour", pricecomponent3);
      pricingDetails.put("Palma City Tour", pricecomponent4);
      bookingComponent.setPricingDetails(pricingDetails);
   }

   /**
    * Get the bookingComponent.
    *
    * @return the BookingComponent.
    */
   public BookingComponent getJavaBookingComponent()
   {
      return bookingComponent;
   }

}