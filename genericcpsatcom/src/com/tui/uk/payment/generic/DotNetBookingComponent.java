/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
 */
package com.tui.uk.payment.generic;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransport;
import org.apache.xmlrpc.client.XmlRpcTransportFactory;

import com.tui.uk.config.AppResource;
import com.tui.uk.log.LogWriter;

/**
 * This class for creating token.
 *
 * @author rajasekhar.asv@sonata-software.com
 */
public class DotNetBookingComponent
{

   /** Constant for personal details key. */
   private static final String PERSONAL_DETAILS = "personalDetails_";

   /** Card charge percentage. */
   private static final double CARD_CHARGE_PERCENT = 2.5;

   /** Minimum Card charge. */
   private static final double CARD_CHARGE_MIN = 0.0;

   /** Maximum card charge. */
   private static final double CARD_CHARGE_MAX = 60.0;

   /** The XmlRpcClient object. */
   private XmlRpcClient xmlRpcClient;

   /** The HttpClient object. */
   private HttpClient httpClient;

   /** The BookingDetails Map. */
   private Map<String, String[]> bookingDetailsMap = null;

   /**
    * Constructor for creating this object. This creates Xml Rpc Client.
    */
   public DotNetBookingComponent()
   {
      getXmlRpcClient();
   }

   /**
    * This method gets the corresponding booking details.
    *
    * @param bookingDetails booking details.
    * @param clientDomain client domain.
    *
    * @return String token.
    */
   public final String[] generateToken2(Map<String, String[]> bookingDetails, String clientDomain)
   {
      Object[] allowedAmounts = new Object[GenericConstants.INDEX_THREE];
      Object[] paymentTypes = null;
      String[] middle = null;
      String[] surname = null;
      String[] forename = null;
      String[] initial = null;
      String[] country = null;
      String[] countrydata = null;

      this.bookingDetailsMap = bookingDetails;
      allowedAmounts[0] = Double.parseDouble(bookingDetails.get("payment.full")[0]);
      allowedAmounts[1] = Double.parseDouble(bookingDetails.get("payment.deposit")[0]);
      allowedAmounts[2] = Double.parseDouble(bookingDetails.get("payment.lowdeposit")[0]);
      paymentTypes = bookingDetails.get("paymenttype");
      middle = bookingDetails.get("passenger_middlename");
      surname = bookingDetails.get("passenger_surname");
      initial = bookingDetails.get("passenger_initial");
      forename = bookingDetails.get("passenger_forename");
      country = bookingDetails.get("country");
      Map<String, String> applicationData = new HashMap<String, String>();

      for (int passengercount = 0; passengercount < surname.length; passengercount++)
      {
         applicationData.put(PERSONAL_DETAILS + passengercount + "_title", initial[passengercount]);
         applicationData.put(PERSONAL_DETAILS + passengercount + "_foreName",
            forename[passengercount]);
         applicationData.put(PERSONAL_DETAILS + passengercount + "_middleInitial",
            middle[passengercount]);
         applicationData.put(PERSONAL_DETAILS + passengercount + "_surName",
            surname[passengercount]);
      }

      for (int k = 0; k < country.length; k++)
      {
         countrydata = country[k].split("-");
         applicationData.put("country_code_" + k, countrydata[0]);
         applicationData.put("country_dsc_" + k, countrydata[1]);
      }

      applicationData.put("country_count", Integer.toString(country.length));
      applicationData.put("passengercount", Integer.toString(surname.length));
      applicationData.put("client_domain_url", "");
      applicationData.put("intellitrackerSnippet", "snippet");
      applicationData.put("prevPage", "prePayment.page");

      applicationData.put("post_payment_url", "/genericcpsclient/postpayment");

      Object[] params =
         new Object[] {"GBP", "GB", GenericConstants.GENERICDOTNET, clientDomain, allowedAmounts,
            paymentTypes, applicationData};
      return getTokenDetails(params);
   }

   /**
    * Generates the token and gets the tomcat instance.
    *
    * @param params data containing the details required to send through Xml Rpc call.
    *
    * @return an array of token and tomcat instance.
    */
   private String[] getTokenDetails(Object[] params)
   {
      xmlRpcClient = getXmlRpcClient();
      String token = null;
      try
      {
         token = (String) xmlRpcClient.execute("commonPaymentService.generateToken2", params);
         setAllowedCards(token, bookingDetailsMap);
      }
      catch (XmlRpcException xre)
      {
         LogWriter.logErrorMessage(xre.getMessage(), xre);
         throw new RuntimeException(xre);
      }

      HttpState httpState = httpClient.getState();
      Cookie[] cookies = httpState.getCookies();
      String tomcatInstance = null;
      for (Cookie cookie : cookies)
      {
         if (cookie.getName().equals("tomcat"))
         {
            tomcatInstance = "tomcat=" + cookie.getValue();
            break;
         }
      }
      return new String[] {token, tomcatInstance};
   }

   /**
    * This method gets the token and the properties.
    *
    * @param token the token.
    *
    * @return Map of application data.
    *
    * @throws IOException IOException, if there is a problem while contacting CPS.
    */
   @SuppressWarnings("unchecked")
   public final Map<String, String> getApplicationData(String token) throws IOException
   {
      Map<String, String> applicationDataMap = null;

      try
      {
         xmlRpcClient = getXmlRpcClient();
         Object[] params = new Object[] {token};
         applicationDataMap =
            (Map<String, String>) xmlRpcClient.execute("commonPaymentService.getNonPaymentData",
               params);
      }
      catch (XmlRpcException xmle)
      {
         LogWriter.logErrorMessage(xmle.getMessage(), xmle);
      }
      return applicationDataMap;
   }

   /**
    * This method gets the token.
    *
    * @param token the token.
    *
    * @return Map of payment data.
    *
    * @throws IOException IOException, if there is a problem while contacting CPS.
    */
   @SuppressWarnings("unchecked")
   public final Map<String, String> getPaymentData(String token) throws IOException
   {
      Map<String, String> applicationDataMap = null;

      try
      {
         xmlRpcClient = getXmlRpcClient();
         Object[] params = new Object[] {token};
         applicationDataMap =
            (Map<String, String>) xmlRpcClient.execute("commonPaymentService.getPaymentData",
               params);
      }
      catch (XmlRpcException xmle)
      {
         LogWriter.logErrorMessage(xmle.getMessage(), xmle);
      }
      return applicationDataMap;
   }

   /**
    * This method sets the card details.
    *
    * @param token the token.
    * @param requestParameterMap the request parameter map.
    */
   public final void setAllowedCards(String token, Map<String, String[]> requestParameterMap)
   {
      String[] allowedCards = requestParameterMap.get("cardtype");
      Map<String, Double[]> allowedCardsMap = new HashMap<String, Double[]>();
      for (int i = 0; i < allowedCards.length; i++)
      {
         Double[] cardCharges = {CARD_CHARGE_PERCENT, CARD_CHARGE_MIN, CARD_CHARGE_MAX};
         allowedCardsMap.put(allowedCards[i], cardCharges);
      }

      try
      {
         xmlRpcClient = getXmlRpcClient();
         Object[] params = new Object[] {token, allowedCardsMap};
         xmlRpcClient.execute("commonPaymentService.setAllowedCards", params);
      }
      catch (XmlRpcException xmle)
      {
         LogWriter.logErrorMessage(xmle.getMessage(), xmle);
      }
   }

   /**
    * This method gets the <code>XmlRpcClient</code> object. This is used for generating token and
    * retrieving application data from CPS.
    *
    * @return XmlRpcClient XmlRpcClient.
    */
   private XmlRpcClient getXmlRpcClient()
   {
      final XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
      config.setEnabledForExtensions(true);
      try
      {
         config.setServerURL(new URL(AppResource.getConfEntry("xmlrpcURL", null)));
      }
      catch (MalformedURLException mue)
      {
         LogWriter.logErrorMessage(mue.getMessage(), mue);
         throw new RuntimeException(mue.getMessage());
      }

      xmlRpcClient = new XmlRpcClient();
      
      //commented  code is for logging the xml rpc request and response.
  /*    final XmlRpcTransportFactory transportFactory = new XmlRpcTransportFactory()
      {
          public XmlRpcTransport getTransport()
          {
              return new XmlRpcLogging(xmlRpcClient);
          }
      }; 
      xmlRpcClient.setTransportFactory(transportFactory);*/

      
      xmlRpcClient.setTransportFactory(new XmlRpcCommonsTransportFactory(xmlRpcClient));

      xmlRpcClient.setConfig(config);

      httpClient = new HttpClient();

      final XmlRpcCommonsTransportFactory factory = 
      new XmlRpcCommonsTransportFactory(xmlRpcClient);
     factory.setHttpClient(httpClient);
     xmlRpcClient.setTransportFactory(factory);

     return xmlRpcClient;
   }

   /**
    * This method authorises the payment using DataCash. This method should be called after the
    * client application has validated any non-payment data, but before it has done any
    * non-reversable back-end processing, such as taking a holiday out of inventory.
    *
    * @param token the token
    * @param client the client
    * @param authCode the auth code This is optional.
    *
    * @throws XmlRpcException the xml rpc exception
    *
    * @since 1.0 API
    */
   public void authPayment(String token, String authCode, String client) throws XmlRpcException
   {
      Object[] params = new Object[] { token, authCode, client };
      xmlRpcClient.execute("datacash.authPayment", params);
   }

   /**
    * This fulfills an authorised DataCash payment. The token identifies the payment and the client
    * identifies the DataCash account number. No error is expected from this method. Since the
    * payment has been authorised, the fulfilment should complete without error.
    *
    * @param token the token
    * @param client the client
    * @throws XmlRpcException the xml rpc exception
    *
    * @since 1.0 API
    */
   public void fulfill(String token, String client)
   {
      try
      {
         Object[] params = new Object[] { token, client };
         xmlRpcClient.execute("datacash.fulfill", params);
      }
      catch (XmlRpcException xml)
      {
         xml.printStackTrace();
      }
   }

   /**
    * This clears all payment data(booking information and payment transaction information)
    * associated with the token. The payment card pan and cvv are overwritten before being
    * dereferenced.After this method is called, the token is no longer valid. Any call to a
    * token-related method, with this token, will fail.This method returns either TRUE or exception.
    * In case of exception client should catch the exception and should proceed with its application
    * flow.
    *
    * @param token the token
    * @throws XmlRpcException the xml rpc exception
    *
    * @since 1.0 API
    */
   public void purgeSensitiveData(String token)
   {
      try
      {
         Object[] params = new Object[] { token };
         xmlRpcClient.execute("commonPaymentService.purgeSensitiveData", params);
      }
      catch (XmlRpcException xml)
      {
         xml.printStackTrace();
      }
   }

   /**
    * Notifies to CPS that booking has been completed.
    *
    * @param token the token.
    * @throws XmlRpcException the xml rpc exception.
    *
    * @since 1.0 API
    */
   public void notifyBookingCompletion(String token)
   {
      try
      {
         Object[] params = new Object[] { token };
         xmlRpcClient.execute("commonPaymentService.notifyBookingCompletion", params);
      }
      catch (XmlRpcException xml)
      {
         xml.printStackTrace();
      }

   }

}
