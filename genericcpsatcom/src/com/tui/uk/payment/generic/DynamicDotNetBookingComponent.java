/*
 * Copyright (C)2008 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 * 
 * $RCSfile: $
 * 
 * $Revision: $
 * 
 * $Date: $
 * 
 * $Author: $
 * 
 * $Log: $
 */
package com.tui.uk.payment.generic;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;

import com.tui.uk.config.AppResource;
import com.tui.uk.log.LogWriter;

/**
 * This class for creating token.
 * 
 * @author santosh.ks@sonata-software.com
 */
@SuppressWarnings("serial")
public class DynamicDotNetBookingComponent
{

   /** Card charge percentage. */
   private static final double CARD_CHARGE_PERCENT = 0;

   /** Minimum Card charge. */
   private static final double CARD_CHARGE_MIN = 2.5;

   /** Maximum card charge. */
   private static final double CARD_CHARGE_MAX = 10.00;

   /** The XmlRpcClient object. */
   private XmlRpcClient xmlRpcClient;

   /** The HttpClient object. */
   private HttpClient httpClient;

   /** The BookingDetails Map. */
   private Map<String, String[]> bookingDetailsMap = null;

   /** The BookingDetails Map. */
   private ResourceBundle bundle = ResourceBundle.getBundle("messages");

   /**
    * Constructor for creating this object. This creates Xml Rpc Client.
    */
   public DynamicDotNetBookingComponent()
   {
      getXmlRpcClient();
   }

   /**
    * This method gets the corresponding booking details.
    * 
    * @param bookingDetails booking details.
    * @param clientDomain client domain.
    * 
    * @return String token.
    */
   public final String[] generateToken2(Map<String, String[]> bookingDetails, String clientDomain)
   {
      Double[] allowedAmounts = new Double[1];
      String[] paymentTypes = {"DCard"};
      String[] clientApplication = bookingDetails.get("clientapplication");
      String[] currency = bookingDetails.get("currency");

      this.bookingDetailsMap = bookingDetails;
      allowedAmounts[0] = Double.parseDouble(bookingDetails.get("bookingcomponent_amount")[0]);

      Map<String, String> applicationData = new HashMap<String, String>();

      String[] appDataKey = bookingDetails.get("appDataKey");
      String[] appDataValue = bookingDetails.get("appDataValue");
      for (int rIndex = 0; rIndex <= appDataKey.length - 1; rIndex++)
      {
         applicationData.put(appDataKey[rIndex], appDataValue[rIndex]);
      }
      applicationData.put("post_payment_url", "/genericcpsclient/postpayment");
      applicationData.put("pre_payment_url", "/prepayment.page");
      applicationData.put("failure_payment_url", "http://10.174.0.189/genericcpsclient/failureurl");
      // applicationData.put("client_domain_url", clientDomain);
      applicationData.put("client_domain_url", "");
      applicationData.put("intellitrackerSnippet", "ab");
      applicationData.put("prevPage", "prePayment.page");
      applicationData.put("legal_info_url",
         "http://flights.thomson.co.uk/en/popup_legal_info_atol.html");
      applicationData.put("booking_Reference_Number", "SON123");
      //applicationData.put("InboundDays", bookingDetails.get("InboundDays").toString());
     // applicationData.put("CheckIn", bookingDetails.get("CheckIn").toString());
      String countryLocale = null;
      if (currency[0].equalsIgnoreCase("GBP"))
      {
         countryLocale = "GB";
      }
      else
      {
         countryLocale = "IE";
      }

      Object[] params =
         new Object[] {currency[0], countryLocale, clientApplication[0], bundle.getString("vTid"),
            allowedAmounts, paymentTypes, applicationData};
      return getTokenDetails(params);
   }

   /**
    * Generates the token and gets the tomcat instance.
    * 
    * @param params data containing the details required to send through Xml Rpc call.
    * 
    * @return an array of token and tomcat instance.
    */
   private String[] getTokenDetails(Object[] params)
   {
      xmlRpcClient = getXmlRpcClient();
      String token = null;
      try
      {
         token = (String) xmlRpcClient.execute("commonPaymentService.generateToken2", params);
         setAllowedCards(token, bookingDetailsMap);
      }
      catch (XmlRpcException xre)
      {
         LogWriter.logErrorMessage(xre.getMessage(), xre);
         throw new RuntimeException(xre);
      }

      HttpState httpState = httpClient.getState();
      Cookie[] cookies = httpState.getCookies();
      String tomcatInstance = null;
      for (Cookie cookie : cookies)
      {
         if (cookie.getName().equals("tomcat"))
         {
            tomcatInstance = "tomcat=" + cookie.getValue();
            break;
         }
      }
      return new String[] {token, tomcatInstance};
   }

   /**
    * This method sets the card details.
    * 
    * @param token the token.
    * @param requestParameterMap the request parameter map.
    */
   public final void setAllowedCards(String token, Map<String, String[]> requestParameterMap)
   {
      String[] allowedCards = requestParameterMap.get("cardtype");
      Map<String, Object[]> allowedCardsMap = new HashMap<String, Object[]>();
      Object[] cardCharges;
      for (int i = 0; i < allowedCards.length; i++)
      {
         if (allowedCards[i].equalsIgnoreCase("Maestro"))
         {
            cardCharges =
               new Object[] {CARD_CHARGE_PERCENT, CARD_CHARGE_MIN, CARD_CHARGE_MAX, "Switch"};
         }
         else
         {
            cardCharges =
               new Object[] {CARD_CHARGE_PERCENT, CARD_CHARGE_MIN, CARD_CHARGE_MAX,
                  allowedCards[i]};
         }
         allowedCardsMap.put(allowedCards[i], cardCharges);
      }

      try
      {
         xmlRpcClient = getXmlRpcClient();
         Object[] params = new Object[] {token, allowedCardsMap};
         xmlRpcClient.execute("commonPaymentService.setAllowedCards", params);
      }
      catch (XmlRpcException xmle)
      {
         LogWriter.logErrorMessage(xmle.getMessage(), xmle);
      }
   }

   /**
    * This method gets the <code>XmlRpcClient</code> object. This is used for generating token and
    * retrieving application data from CPS.
    * 
    * @return XmlRpcClient XmlRpcClient.
    */
   private XmlRpcClient getXmlRpcClient()
   {
      final XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
      config.setEnabledForExtensions(true);
      try
      {
         config.setServerURL(new URL(AppResource.getConfEntry("xmlrpcURL", null)));
      }
      catch (MalformedURLException mue)
      {
         LogWriter.logErrorMessage(mue.getMessage(), mue);
         throw new RuntimeException(mue.getMessage());
      }

      xmlRpcClient = new XmlRpcClient();
      xmlRpcClient.setTransportFactory(new XmlRpcCommonsTransportFactory(xmlRpcClient));
      xmlRpcClient.setConfig(config);

      httpClient = new HttpClient();

      final XmlRpcCommonsTransportFactory factory = new XmlRpcCommonsTransportFactory(xmlRpcClient);
      factory.setHttpClient(httpClient);

      xmlRpcClient.setTransportFactory(factory);

      return xmlRpcClient;
   }

   /**
    * 
    * generateTokenForRefund.
    * 
    * @param currency currency
    * @param countryLocale countryLocale
    * @param clientApplication clientApplication
    * @param datacashVTid datacashVTid
    * @param refundDetails refundDetails
    * @param paymentTypes paymentTypes
    * @param applicationData applicationData
    * @param refundAmount refundAmount
    * @return String array
    * @param requestParameterMap requestParameterMap
    */
   public String[] generateTokenForRefund(String currency, String countryLocale,
      String clientApplication, String datacashVTid, String refundAmount, Object[][]refundDetails ,
      Object[] paymentTypes, Map<String, String> applicationData,
      Map<String, String[]> requestParameterMap)
   {
      final int firstRow = 0;
      int firstColumn = 0;
      final int secondRow = 1;
      int secondColumn = 0; 
      final  int thirdRow = 2;
      int thirdColumn = 0; 
      
      final String refundEnabled = "true";
      final String integratedRefund = "true";
      clientApplication = requestParameterMap.get("clientapplication")[0];
      currency = requestParameterMap.get("currency")[0];
      refundAmount = requestParameterMap.get("bookingcomponent_amount")[0];
      countryLocale = null;
      if (currency.equalsIgnoreCase("GBP"))
      {
         countryLocale = "GB";
      }
      else
      {
         countryLocale = "IE";
      }
      refundDetails[firstRow][firstColumn++] = applicationData.get("amount_0");
      refundDetails[firstRow][firstColumn++] = applicationData.get("payment_date_0");
      refundDetails[firstRow][firstColumn++] = applicationData.get("datacash_ref_0");
      refundDetails[firstRow][firstColumn++] = applicationData.get("payee_name_0");
      refundDetails[firstRow][firstColumn++] = applicationData.get("card_number_0");
      refundDetails[firstRow][firstColumn++] = applicationData.get("expiry_date_0");
      refundDetails[firstRow][firstColumn++] = applicationData.get("payment_type_0");
      refundDetails[firstRow][firstColumn++] = "PAYMETHO2";
      refundDetails[firstRow][firstColumn++] = refundEnabled;
      refundDetails[firstRow][firstColumn++] = integratedRefund;
    
      
      refundDetails[secondRow][secondColumn++] = applicationData.get("amount_1");
      refundDetails[secondRow][secondColumn++] = applicationData.get("payment_date_1");
      refundDetails[secondRow][secondColumn++] = applicationData.get("datacash_ref_1");
      refundDetails[secondRow][secondColumn++] = applicationData.get("payee_name_1");
      refundDetails[secondRow][secondColumn++] = applicationData.get("card_number_1");
      refundDetails[secondRow][secondColumn++] = applicationData.get("expiry_date_1");
      refundDetails[secondRow][secondColumn++] = applicationData.get("payment_type_1");
      refundDetails[secondRow][secondColumn++] = "PAYMETHO2";
      refundDetails[secondRow][secondColumn++] = refundEnabled;
      refundDetails[secondRow][secondColumn++] = integratedRefund;
      
      refundDetails[thirdRow][thirdColumn++] = applicationData.get("amount_2");
      refundDetails[thirdRow][thirdColumn++] = applicationData.get("payment_date_2");
      refundDetails[thirdRow][thirdColumn++] = applicationData.get("datacash_ref_2");
      refundDetails[thirdRow][thirdColumn++] = applicationData.get("payee_name_2");
      refundDetails[thirdRow][thirdColumn++] = applicationData.get("card_number_2");
      refundDetails[thirdRow][thirdColumn++] = applicationData.get("expiry_date_2");
      refundDetails[thirdRow][thirdColumn++] = applicationData.get("payment_type_2");
      refundDetails[thirdRow][thirdColumn++] = "PAYMETHO2";
      refundDetails[thirdRow][thirdColumn++] = refundEnabled;
      refundDetails[thirdRow][thirdColumn++] = integratedRefund;
     
      Object[] params =
         new Object[] {currency, countryLocale, clientApplication, bundle.getString("vTid"),
            refundAmount, refundDetails, paymentTypes, applicationData};

      xmlRpcClient = getXmlRpcClient();
      String token = null;
      try
      {
         token =
            (String) xmlRpcClient.execute("commonPaymentService.generateTokenForRefund", params);
         setAllowedCards(token, requestParameterMap);
      }
      catch (XmlRpcException xre)
      {
         LogWriter.logErrorMessage(xre.getMessage(), xre);
         throw new RuntimeException(xre);
      }

      HttpState httpState = httpClient.getState();
      Cookie[] cookies = httpState.getCookies();
      String tomcatInstance = null;
      for (Cookie cookie : cookies)
      {
         if (cookie.getName().equals("tomcat"))
         {
            tomcatInstance = "tomcat=" + cookie.getValue();
            break;
         }
      }
      return new String[] {token, tomcatInstance};

   }
}
