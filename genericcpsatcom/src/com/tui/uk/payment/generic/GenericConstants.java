/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
*/
package com.tui.uk.payment.generic;

import java.text.SimpleDateFormat;
import java.util.Currency;

/**
 * The Interface DispatcherConstants.This contains all the constants for charges and discounts.
 */
public interface GenericConstants
{
   /** The Constant GBP. */

   Currency GBP = Currency.getInstance("GBP");

   /** The constant for selected card. */
   String CARDCHARGE = "2.5";

   /** The constant for index 3. */
   int INDEX_THREE = 3;

   /** The constant for index 4. */
   int INDEX_FOUR = 4;

   /** The constant for index 5. */
   int INDEX_FIVE = 5;

   /** The constant for index 6. */
   int INDEX_SIX = 6;

   /** The constant for index 7. */
   int INDEX_SEVEN = 7;

   /** The constant for JAVAEXAMPLECLIENT. */
   String JAVAEXAMPLECLIENT = "2";

   /** The constant for JAVACLIENT. */
   String JAVACLIENT = "3";

   /** The constant for DOTNETCLIENT. */
   String DOTNETCLIENT = "1";

   /** The constant for GENERICDOTNET. */
   String GENERICDOTNET = "GENERICDOTNET";

   /** The Constant For FORMATDATE. */
   SimpleDateFormat FORMATDATE = new SimpleDateFormat("dd/MM/yyyy");

   /** The Constant For Token. */
   String TOKEN = "token";

   /** The Constant For TOMCATINSTANCE. */
   String TOMCATINSTANCE = "tomcatInstance";

   /** The constant for DOTNETCLIENT. */
   String DYNAMICDOTNETCLIENT = "4";

}
