/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
 */
package com.tui.uk.payment.generic;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xmlrpc.XmlRpcException;

import com.tui.uk.client.CommonPaymentClient;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.client.domain.PaymentType;
import com.tui.uk.config.AppResource;
import java.util.ResourceBundle;


/**
 * This class is responsible for Dispatching . This class gets the client code and bookingdetails
 * information from the request parameter.
 *
 * @author Jaleel
 *
 */
@SuppressWarnings("serial")
public class PrePaymentServlet extends HttpServlet
{

/** The bundle loads messages.properties file. */
private ResourceBundle bundle = ResourceBundle.getBundle("messages");
   /**
    * This method reads the clientcode and dispatches to appropriate server URL.
    *
    * @param request the HttpServletRequest object that contains the request the client has made of
    *           the servlet.
    * @param response the HttpServletResponse object that contains the response the servlet sends to
    *           the client.
    *
    * @throws IOException the IOException thrown if an input or output error is detected when the
    *            servlet handles the request
    * @throws ServletException the ServletException thrown if the request for the GET could not be
    *            handled.
    */
   @SuppressWarnings("unchecked")
   public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
 {



      String[] rpcdata = null;
      String redirectUrl = null;

      String clientURL = request.getHeader("host");
      String clientcode = request.getParameter("clientcode");

      String token = (String) request.getSession().getAttribute(GenericConstants.TOKEN);
      String tomcatInstance =
         (String) request.getSession().getAttribute(GenericConstants.TOMCATINSTANCE);




      if (token != null || tomcatInstance != null)
      {
         CommonPaymentClient cpc =
            new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null) + "?"
               + tomcatInstance);
         try
         {
            cpc.purgeClientPaymentPageData(token);
            request.getSession().removeAttribute(GenericConstants.TOKEN);
            request.getSession().removeAttribute(GenericConstants.TOMCATINSTANCE);
         }
         catch (XmlRpcException xmle)
         {
            xmle.printStackTrace();
         }
      }

      CommonPaymentClient cpc =
         new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null));

      if (clientcode.equals(GenericConstants.DOTNETCLIENT))
      {
         DotNetBookingComponent dotNetBookingComponent = new DotNetBookingComponent();
         rpcdata =
            dotNetBookingComponent.generateToken2(request.getParameterMap(),
               request.getHeader("host"));
         dotNetBookingComponent.setAllowedCards(rpcdata[0], request.getParameterMap());
         redirectUrl = AppResource.getConfEntry("genericURL", null);
      }
      else if (clientcode.equals(GenericConstants.JAVAEXAMPLECLIENT))
      {
         try
         {
            rpcdata =
               cpc.generateToken(new ExampleBookingComponent(clientURL).getBookingComponent());
            redirectUrl = AppResource.getConfEntry("genericURL", null);









/*



Map<String,String> ddDetails=new HashMap<String,String>();
String hsortCode=bundle.getString("sortcode");
String accountNumber=bundle.getString("accountnumber");
String firstName=bundle.getString("firstName");
String lastName=bundle.getString("lastName");
String hmethod=bundle.getString("method");
String merchantReference=bundle.getString("merchantreference");

ddDetails.put("sortcode",hsortCode);
ddDetails.put("accountnumber",accountNumber);
ddDetails.put("firstName",firstName);
ddDetails.put("lastName",lastName);
ddDetails.put("method",hmethod);
ddDetails.put("merchantreference",merchantReference);






Map<String,String> setupResult=cpc.ddSetUpMandate("GBP","GB","GB","THOMSON",
bundle.getString("dvTid"),ddDetails);
System.out.println("====\n"+setupResult);


      */















         }
         catch (XmlRpcException xmle)
         {
            xmle.printStackTrace();
         }
      }
      else if (clientcode.equals(GenericConstants.JAVACLIENT))
      {
         try
         {
            rpcdata =
               cpc.generateToken(new JavaBookingComponent(request.getParameterMap(), clientURL)
                  .getJavaBookingComponent());
            redirectUrl = AppResource.getConfEntry("cpsURL", null);
         }
         catch (XmlRpcException xmle)
         {
            xmle.printStackTrace();
         }
      }

      else if (clientcode.equals(GenericConstants.DYNAMICDOTNETCLIENT))
      {
         String transactionType = request.getParameter("transactiontype");
         if (transactionType != null)
         {
            if (transactionType.equalsIgnoreCase("payment"))
            {

               DynamicDotNetBookingComponent dotNetBookingComponent =
                  new DynamicDotNetBookingComponent();
               rpcdata =
                  dotNetBookingComponent.generateToken2(request.getParameterMap(),
                     request.getHeader("host"));
               redirectUrl = AppResource.getConfEntry("genericURL", null);
            }
            else if (transactionType.equalsIgnoreCase("refund"))
            {
               DynamicDotNetBookingComponent dotNetBookingComponent =
                  new DynamicDotNetBookingComponent();
               Map<String, String[]> requestParameterMap = request.getParameterMap();

               // Double[] allowedAmounts = new Double[1];
               Object[] paymentTypes = { PaymentMethod.DATACASH };
               String clientApplication = "";
               String currency = "";
               Object[][] refundDetails = new Object[3][11];
               String countryLocale="";
               String refundAmount="";

               Map<String, String> applicationData = new HashMap<String, String>();
               String[] appDataKey = requestParameterMap.get("appDataKey");
               String[] appDataValue = requestParameterMap.get("appDataValue");
               for (int rIndex = 0; rIndex <= appDataKey.length - 1; rIndex++)
               {
                  applicationData.put(appDataKey[rIndex], appDataValue[rIndex]);
               }
               applicationData.put("post_payment_url", "/genericcpsclient/postpayment");
               applicationData.put("pre_payment_url", "/prepayment.page");
               applicationData.put("failure_payment_url",
                  "http://10.174.0.189/genericcpsclient/failureurl");
               // applicationData.put("client_domain_url", clientDomain);
               applicationData.put("client_domain_url", "");
               applicationData.put("intellitrackerSnippet", "ab");
               applicationData.put("prevPage", "prePayment.page");
               applicationData.put("legal_info_url",
                  "http://flights.thomson.co.uk/en/popup_legal_info_atol.html");

               rpcdata =
                  dotNetBookingComponent.generateTokenForRefund(currency, countryLocale,
                     clientApplication, "99639992", refundAmount, refundDetails, paymentTypes,
                     applicationData,requestParameterMap);

               redirectUrl = AppResource.getConfEntry("genericURL", null);
            }

         }

         else
         {
            DynamicDotNetBookingComponent dotNetBookingComponent =
               new DynamicDotNetBookingComponent();
            rpcdata =
               dotNetBookingComponent.generateToken2(request.getParameterMap(),
                  request.getHeader("host"));
            redirectUrl = AppResource.getConfEntry("genericURL", null);

         }

      }

      tomcatInstance = rpcdata[1];
      if (request.getParameter("clientapplication") != null)
      {
         request.getSession().setAttribute("clientapplication",
            request.getParameter("clientapplication"));
      }
      this.getServletConfig().getServletContext().setAttribute("tokenValue",rpcdata[0]);
      request.getSession().setAttribute("tomcatInstance", tomcatInstance);
      request.getSession().setAttribute("token", rpcdata[0]);
      request.getSession().setAttribute("clientcode", clientcode);

      response.sendRedirect(redirectUrl + rpcdata[0] + "&" + tomcatInstance);
   }

}