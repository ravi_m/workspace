package com.tui.uk.payment.generic;


import java.net.MalformedURLException;

import com.datacash.util.XMLDocument;
import com.tui.uk.payment.exception.IllegalHostException;
import com.tui.uk.payment.pojo.CardResponse;
import com.tui.uk.payment.pojo.DataCashService;
import com.tui.uk.payment.pojo.DocBuilder;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import org.xml.sax.SAXException;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.List;
import java.util.Iterator;

import com.tui.uk.config.AppResource;
import com.tui.uk.log.LogWriter;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransport;

import com.tui.uk.client.domain.PayPalAuthorizeResponse;
import com.tui.uk.client.domain.PayPalCustomerVerificationDetails;
import com.tui.uk.client.domain.PaymentScheduleData;
import com.tui.uk.client.CommonPaymentClient;
import com.tui.uk.client.domain.AirlineItenaryData;
import com.tui.uk.client.domain.PayPalCaptureRequest;
import com.tui.uk.client.domain.PayPalCaptureResponse;
import com.tui.uk.client.domain.PayPalFundReleaseResponse;

import com.tui.uk.client.domain.AuthorizePayPalTransaction;
import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.PayPalSetExpressCheckoutResponse;
import com.tui.uk.client.domain.PayPalCustomer;
import com.tui.uk.client.domain.PaypalFlightDetails;

/**
 * This class is responsible for sending xml request to datacash
 * and displays the response in the form of xml.
 *
 */
public class PaypalServletClient extends HttpServlet
{
/** The serialVersionUID. */
private static final long serialVersionUID = 6L;

/** The xmlRpcClient. */
private XmlRpcClient xmlRpcClient;

/** The HttpClient object. */
private HttpClient httpClient;

/** The Resource Bundle object. */
private ResourceBundle bundle = ResourceBundle.getBundle("messages");

/** The Resource Bundle object. */
private ResourceBundle paypalProperties = ResourceBundle.getBundle("paypal");

/** The Resource Bundle object. */
private PayPalCustomerVerificationDetails paypalCustomerVerificationDetails = null;

/** The Resource Bundle object. */
private PayPalAuthorizeResponse payPalAuthorizeResponse = null;

/** The token object. */
private String token = null;

/** The Resource Bundle object. */
private String tokenValue = null;

/** The Resource Bundle object. */
private String paypalDataCashReference = null;


/**
* This method process xml request and gives response. *
* @param request **this is request of servlet**
* @param response the ** response ** response.
* @throws ServletException ServletException.
* @throws IOException the IOException.
* *
*/
 protected void doGet(HttpServletRequest request, HttpServletResponse response)
 throws ServletException,IOException
 {

 String[] rpcdata = null;
 String clientURL = request.getHeader("host");
 String clientcode = request.getParameter("clientcode");
 String redirectUrl = null;
 PayPalSetExpressCheckoutResponse paypalExpressCheckoutResponse = null;
 String service=request.getParameter("paypalComponent");
 String payPalPage=paypalProperties.getString("pReturnUrl");


 if("setexpresscheckout".equals(service)){
 System.out.println(service+" has been invoked at ========"
 + "===================================================");
 CommonPaymentClient cpc =new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null));
 try{
 rpcdata =
 cpc.generateToken(new ExampleBookingComponent(clientURL).getBookingComponent());
 redirectUrl = AppResource.getConfEntry("genericURL", null);
 token=rpcdata[0];
 tokenValue=token;
 this.getServletConfig().getServletContext().setAttribute("tokenValue",token);
 String paypalUrl = AppResource.getConfEntry("paypalUrl", null);
 System.out.println(paypalUrl+"=================================================");

 String url=paypalUrl+"?token="+rpcdata[0]+"&tomcat=local";

 /*String url="http://10.174.0.229:9090/cps/paypal"+"?token="+rpcdata[0]+"&tomcat=local";*/

 response.sendRedirect(url);

 }
 catch (XmlRpcException xmle)
 {
 xmle.printStackTrace();
 }


 }else if("verifyCustomerDetails".equals(service)){


 try{
tokenValue=(String)this.getServletConfig().getServletContext().getAttribute("tokenValue");
 System.out.println(tokenValue+"============================================="
 + "=====================================");

 CommonPaymentClient cpc =new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null));
 Object object=cpc.verifyPaypalCustomerDetails(tokenValue);
 paypalCustomerVerificationDetails=(PayPalCustomerVerificationDetails)object;

 System.out.println(paypalCustomerVerificationDetails.getFirstName()
 +"=============================================="+paypalCustomerVerificationDetails.getLastName());
 System.out.println("ddpayment payment servlet==========="
 + "========================================="+object.getClass().getName());

 response.sendRedirect(payPalPage);
 paypalDataCashReference=paypalCustomerVerificationDetails.getDatacashReferance();

 }
 catch(XmlRpcException ex){
 System.out.println(ex);
 }

 }else if("doAuthorize".equals(service)){


 System.out.println(this.getServletConfig().getServletContext().
 getAttribute("tokenValue")+"=======================================================");

 try{
 CommonPaymentClient cpc2 =new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null));


 String str=(String)this.getServletConfig().getServletContext().getAttribute("tokenValue");
 Object trasactionData=cpc2.getEssentialTransactionData(str);
 List<EssentialTransactionData> essentialTransactionData=
 (List<EssentialTransactionData>)trasactionData;
 paypalExpressCheckoutResponse=
 essentialTransactionData.get(0).getPaypalExpressCheckoutResponse();


 AuthorizePayPalTransaction authorizePayPalTransaction=
 new AuthorizePayPalTransaction("do_authorization",
 paypalExpressCheckoutResponse.getDatacashReference());
 authorizePayPalTransaction.setAmount(paypalCustomerVerificationDetails.getAmount());
 authorizePayPalTransaction.setCountryCode(paypalCustomerVerificationDetails.getCountryCode());
 authorizePayPalTransaction.setCustomerEmailId
 (paypalCustomerVerificationDetails.getCustomerEmailId());
 authorizePayPalTransaction.setFirstname(paypalCustomerVerificationDetails.getFirstName());
 authorizePayPalTransaction.setLastname(paypalCustomerVerificationDetails.getLastName());
 // authorizePayPalTransaction.setMiddlename("test");
 authorizePayPalTransaction.setPayerId(paypalCustomerVerificationDetails.getPayerId());
 authorizePayPalTransaction.setPayerStatus(paypalCustomerVerificationDetails.getPayerStatus());
 authorizePayPalTransaction.setPaymentStatus
 (paypalCustomerVerificationDetails.getPaymentStatus());
 authorizePayPalTransaction.setPaymentType
 (paypalCustomerVerificationDetails.getPamentType());
 authorizePayPalTransaction.setDatacashReferance
 (paypalExpressCheckoutResponse.getDatacashReference());


 Object object=cpc2.doAuthorizePayPalTransaction(str,authorizePayPalTransaction);
 payPalAuthorizeResponse=(PayPalAuthorizeResponse)object;
 System.out.println(object.getClass().getName()+"=================================");

 paypalDataCashReference=payPalAuthorizeResponse.getDatacashReferance();

 response.sendRedirect(payPalPage);
 }
 catch(XmlRpcException ex){
 System.out.println(ex);
 }




 }else if("doCapture".equals(service)){

 try{
tokenValue=(String)this.getServletConfig().getServletContext().getAttribute("tokenValue");
 CommonPaymentClient cpc3 =new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null));
 PayPalCaptureRequest paypalCaptureRequest=new PayPalCaptureRequest();
paypalCaptureRequest.setDatacashReferance(payPalAuthorizeResponse.getDatacashReferance());
paypalCaptureRequest.setTransactionMethod(paypalProperties.getString("capture_transaction_method"));
paypalCaptureRequest.setIsCompleted(paypalProperties.getString("capture_iscompleted"));
paypalCaptureRequest.setInvnum(paypalProperties.getString("pinvnum"));
paypalCaptureRequest.setMessageSubmissionId(payPalAuthorizeResponse.getMessageSubmissionId());
paypalCaptureRequest.setNoteText(paypalProperties.getString("capture_noteText"));
paypalCaptureRequest.setSoftDescriptor(paypalProperties.getString("capture_softDescriptor"));


AirlineItenaryData airlineItenaryData= new AirlineItenaryData();
airlineItenaryData.setClearingCount(
paypalProperties.getString("capture_AirlineItenaryData_ClearingCount"));
airlineItenaryData.setClearingSequence(paypalProperties.
getString("capture_AirlineItenaryData_clearingSequence"));
airlineItenaryData.setIssuingCarrierCode(paypalProperties.
getString("capture_AirlineItenaryData_issuingCarrierCode"));
airlineItenaryData.setPassengerName(
paypalProperties.getString("capture_AirlineItenaryData_passengerName"));
airlineItenaryData.setRestrictedTicket(
paypalProperties.getString("capture_AirlineItenaryData_restrictedTicket"));
airlineItenaryData.setTicketNumber(
paypalProperties.getString("capture_AirlineItenaryData_ticketNumber"));
airlineItenaryData.setCustomerCode(
paypalProperties.getString("capture_AirlineItenaryData_customerCode"));
airlineItenaryData.setIssueDate(
paypalProperties.getString("capture_AirlineItenaryData_issueDate"));
airlineItenaryData.setTotalFare(
paypalProperties.getString("capture_AirlineItenaryData_totalFare"));
airlineItenaryData.setTotalFee(
paypalProperties.getString("capture_AirlineItenaryData_totalFee"));
airlineItenaryData.setTotal_taxes(
paypalProperties.getString("capture_AirlineItenaryData_total_taxes"));
airlineItenaryData.setTravelAgencyCode(
paypalProperties.getString("capture_AirlineItenaryData_travelAgencyCode"));
airlineItenaryData.setTravelAgencyName(
paypalProperties.getString("capture_AirlineItenaryData_travelAgencyName"));

PaypalFlightDetails flightDetails=new PaypalFlightDetails();
flightDetails.setArrivalAirportCode(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_arrivalAirportCode"));
flightDetails.setCarrierCode(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_carrierCode"));
flightDetails.setDepartureTime(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_departureTime"));
flightDetails.setFareBasisCode(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_fareBasisCode"));
flightDetails.setFlightNumber(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_flightNumber"));
flightDetails.setServiceClass(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_serviceClass"));
flightDetails.setStopoverCode(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_stopoverCode"));
flightDetails.setTravelDate(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_travelDate"));
flightDetails.setDepartureAirportCode(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_departureAirportCode"));
flightDetails.setFare(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_fare"));
flightDetails.setConjunctionTicket(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_conjunctionTicket"));
flightDetails.setCouponNumber(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_couponNumber"));
flightDetails.setEndorsementRestrictions(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_endorsementRestrictions"));
flightDetails.setExchangeTicket(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_exchangeTicket"));
flightDetails.setFee(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_fee"));
flightDetails.setTaxes(
paypalProperties.getString("capture_AirlineItenaryData_FlightDetails_taxes"));

airlineItenaryData.setFlightDetails(flightDetails);
paypalCaptureRequest.setAirlineItenaryData(airlineItenaryData);









 paypalCaptureRequest.setAmount(paypalCustomerVerificationDetails.getAmount());
 Object obj=cpc3.doCapturePaypalTransaction(tokenValue,paypalCaptureRequest);
 PayPalCaptureResponse paypalCaptureResponse=(PayPalCaptureResponse)obj;
 System.out.println(obj.getClass().getName()+"=================================");


 paypalDataCashReference=paypalCaptureResponse.getDatacashReferance();

response.sendRedirect(payPalPage);
 }
 catch(XmlRpcException ex){
 System.out.println(ex);
 }
 }else if("doVoid".equals(service)){

 try{
tokenValue=(String)this.getServletConfig().getServletContext().getAttribute("tokenValue");
 CommonPaymentClient cpc3 =new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null));
 String str=(String)this.getServletConfig().getServletContext().getAttribute("tokenValue");
 Object trasactionData=cpc3.getEssentialTransactionData(str);
 List<EssentialTransactionData> essentialTransactionData=
 (List<EssentialTransactionData>)trasactionData;
 paypalExpressCheckoutResponse=
 essentialTransactionData.get(0).getPaypalExpressCheckoutResponse();
 PayPalCustomer cust=
 new PayPalCustomer("do_void",
 paypalExpressCheckoutResponse.getDatacashReference());
 Object obj=cpc3.cancelPayPalTransaction(tokenValue,cust);
 System.out.println(obj.getClass().getName()+"=================================");
 PayPalFundReleaseResponse paypalFundReleaseResponse=(PayPalFundReleaseResponse)obj;
 paypalDataCashReference=paypalFundReleaseResponse.getDatacashReferance();

 response.sendRedirect(payPalPage);

 }
 catch(XmlRpcException ex){
 System.out.println(ex);
 }
 }
 else if("doQuery".equals(service)){

 try{
tokenValue=(String)this.getServletConfig().getServletContext().getAttribute("tokenValue");
 CommonPaymentClient cpc3 =new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null));
 String str=(String)this.getServletConfig().getServletContext().getAttribute("tokenValue");
 Object trasactionData=cpc3.getEssentialTransactionData(str);
 List<EssentialTransactionData> essentialTransactionData=
 (List<EssentialTransactionData>)trasactionData;
 paypalExpressCheckoutResponse=
 essentialTransactionData.get(0).getPaypalExpressCheckoutResponse();
/* PayPalCustomer cust=
 new PayPalCustomer("query",
 paypalExpressCheckoutResponse.getDatacashReference());*/
 PayPalCustomer cust=
 new PayPalCustomer("query",paypalDataCashReference);
 Object obj=cpc3.queryPayPalTransaction(tokenValue,cust);
 System.out.println(obj.getClass().getName()+"=================================");

 response.sendRedirect(payPalPage);
 }catch(XmlRpcException ex){
System.out.println(ex);
}
}

 }
 /**
  * This method process xml request and gives response
  *
  * @param req.
  * @param resp.
  * @throws ServletException.
  * @throws IOException.
  *
  */
 protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
    IOException
 {
    doGet(req, resp);
 }

}