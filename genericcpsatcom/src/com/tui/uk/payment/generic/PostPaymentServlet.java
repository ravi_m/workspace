/*
 * Copyright (C)2008 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 * 
 * $RCSfile: $
 * 
 * $Revision: $
 * 
 * $Date: $
 * 
 * $Author: $
 * 
 * $Log: $
 */
package com.tui.uk.payment.generic;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xmlrpc.XmlRpcException;

import com.tui.uk.client.CommonPaymentClient;
import com.tui.uk.config.AppResource;

/**
 * The Class PaymentServlet. This class is used to show the Non payment Data section from CPS
 * server.
 * 
 * @author Jaleel
 */
@SuppressWarnings("serial")
public class PostPaymentServlet extends HttpServlet
{

   /** The constant THREE_D_SECURE_CONF_VALUE. */
   public static final String THREE_D_SECURE_CONF_VALUE = ".3DSecure";

   /**
    * 
    * This method reads the token and client code to show the Non payment data.
    * 
    * @param request the HttpServletRequest object that contains the request the client has made of
    *           the servlet.
    * @param response the HttpServletResponse object that contains the response the servlet sends to
    *           the client.
    * @throws IOException the IOException thrown if an input or output error is detected when the
    *            servlet handles the request
    * @throws ServletException the ServletException thrown if the request for the GET could not be
    *            handled.
    */

   public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      doPost(request, response);
   }

   public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {

      String clientApplication = (String) request.getSession().getAttribute("clientapplication");
      String threeDSecure = null;
      if (clientApplication != null)
      {
         threeDSecure = clientApplication + THREE_D_SECURE_CONF_VALUE;
      }

      Map<String, String> nonpaymentdata = null;

      String token = (String) request.getSession().getAttribute("token");
      String tomcatInstance = (String) request.getSession().getAttribute("tomcatInstance");
      try
      {
         String ccode = (String) request.getSession().getAttribute("clientcode");
         if (ccode.equals(GenericConstants.JAVACLIENT)
            || ccode.equals(GenericConstants.JAVAEXAMPLECLIENT))
         {
            if (threeDSecure == null || !AppResource.getBooleanEntry(threeDSecure, false, null))
            {
               // B2B Flow
               CommonPaymentClient cpc =
                  new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null) + "?"
                     + tomcatInstance);
               nonpaymentdata = cpc.getNonPaymentData(token);
               request.setAttribute("nonpaymentdata", nonpaymentdata);
            }
            request
               .getRequestDispatcher(AppResource.getConfEntry("java.generic.postpayment", null))
               .forward(request, response);
         }
         else
         {
            if (threeDSecure == null || !AppResource.getBooleanEntry(threeDSecure, false, null))
            {
               // B2B Flow
               DotNetBookingComponent dotNetBookingComponent = new DotNetBookingComponent();
               nonpaymentdata = dotNetBookingComponent.getApplicationData(token);
               request.setAttribute("nonpaymentdata", nonpaymentdata);
               request.setAttribute("paymentData", dotNetBookingComponent.getPaymentData(token));
            }
            request.getRequestDispatcher(
               AppResource.getConfEntry("dotnet.generic.postpayment", null)).forward(request,
               response);
         }
      }
      catch (XmlRpcException xmle)
      {
         xmle.printStackTrace();
      }
   }
}