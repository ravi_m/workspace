package com.tui.uk.payment.generic;


import java.net.MalformedURLException;
import com.datacash.util.XMLDocument;
import com.tui.uk.payment.exception.IllegalHostException;
import com.tui.uk.payment.pojo.CardResponse;
import com.tui.uk.payment.pojo.DataCashService;
import com.tui.uk.payment.pojo.DocBuilder;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.xml.sax.SAXException;


import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.List;
import java.util.Iterator;
import com.tui.uk.config.AppResource;
import com.tui.uk.log.LogWriter;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpState;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransportFactory;
import org.apache.xmlrpc.client.XmlRpcTransport;
import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.PaymentScheduleData;

import com.tui.uk.client.CommonPaymentClient;



/**
 * This class is responsible for sending  xml request to datacash
 * and displays the response in the form of xml.
 *
 */
public class DDPaymentServlet extends HttpServlet
{
   /** The serialVersionUID. */
   private static final long serialVersionUID = 5L;

   /** The xmlRpcClient. */
   private XmlRpcClient xmlRpcClient;

   /** The HttpClient object. */
   private HttpClient httpClient;

   /** The Resource Bundle object. */
   private ResourceBundle bundle = ResourceBundle.getBundle("messages");

   /**
    * This method process xml request and gives response.    *
    * @param request **this is request of servlet**
    * @param response the   ** response   ** response.
    * @throws ServletException ServletException.
    * @throws IOException the IOException.
    *
    */
protected void doGet(HttpServletRequest request, HttpServletResponse response)
throws ServletException,IOException
{
HttpSession session = request.getSession(true);
String ddi=request.getParameter("ddi");
System.out.println("======================="+ddi+"=============================\n\n");
xmlRpcClient = getXmlRpcClient();
//Map<String,String> applicationData=new HashMap<String,String>();
Map<String,String> applicationData=null;
//applicationData.put("cardtype","debit");
//applicationData.put("payment_0_type","DIRECTDEBIT");


Object sortCode=bundle.getString("sortcode");
Object accountnumber=bundle.getString("accountnumber");
Object name=bundle.getString("accountname");
String firstName1=bundle.getString("firstName");
String lastName1=bundle.getString("lastName");
String edob1=bundle.getString("experian.dob");
String ehousenumber1=bundle.getString("experian.housenumber");
String estreet1=bundle.getString("experian.street");
String epostcode1=bundle.getString("experian.postcode");
String ehousename1=bundle.getString("experian.housename");
String eflat1=bundle.getString("experian.flat");





//Object name="SREENIVASULU KAMINI";
Object method=bundle.getString("method");
Object merchantreference=bundle.getString("merchantreference");



/*  Object directDebitDetails[]={bundle.getString("sortcode"),bundle.getString("accountnumber"),
"SREENIVASULU KAMINI","setup",bundle.getString("merchantreference")}; */

Object directDebitDetails[]={sortCode,accountnumber,firstName1,lastName1,method,merchantreference,
ehousenumber1,estreet1,epostcode1,edob1,eflat1,ehousename1,
"zyxvutsrqponmlkji" , "thomson@1234", "03/14/2018"};


/*Object directDebitDetails[]={sortCode,accountnumber,firstName1,lastName1,method,merchantreference,
ehousenumber1,estreet1,epostcode1,edob1,eflat1,ehousename1,
"" , "", ""};*/


//    Object directDebitDetails[]={"826300","80000990","THOMSON","setup","I-10031643"};




/*   Object directDebitDetails[]={"123456","33329536630",
"SREENIVASULU KAMINI","setup","55555555"};   */


Object[] params2 =new Object[] {"GBP","GB","GB", "THOMSON", "99010328",applicationData,
directDebitDetails};
      if(ddi.equals("ddSetUpMandate")){

      try
      {
        Object object = (Object)xmlRpcClient.execute("datacash.ddSetUpMandate", params2);
        HashMap<String, String> results=(HashMap<String,String>)object;
System.out.println("==================================="
+ "====================================================");
System.out.println(results);
System.out.println("==================================="
+ "====================================================");
        Set s=results.keySet();
        Iterator iterator=s.iterator();
        String sss="";
        while(iterator.hasNext()){
sss=sss+(String)iterator.next()+"=";
sss=sss+(String)results.get((String)iterator.next())+"\t";

        }

        System.out.println(sss);


        String str=(String)results.get("responseXml");
        if(str==null)
str=sss;

        session.setAttribute("responseXML", str);

        response.sendRedirect("/genericcpsatcom/ddresponse.jsp");

      }
      catch (XmlRpcException xre)
      {
         LogWriter.logErrorMessage(xre.getMessage(), xre);
         throw new RuntimeException(xre);
      }
      catch(Exception ex){

System.out.println(ex.toString());
      }
   }
    else if(ddi.equals("submitddDrawdownBatch")){
    try{




Object o1[]={bundle.getString("datacashreference"),bundle.getString("ddamount1"),
bundle.getString("date1"),
bundle.getString("tran_code"),bundle.getString("ddmethod1"),
bundle.getString("merchantreference")};
Object o2[]={bundle.getString("datacashreference2"),
bundle.getString("ddamount2"),bundle.getString("date2"),
bundle.getString("tran_code"),bundle.getString("ddmethod2"),
bundle.getString("merchantreference2")};

Object o3[]={"DATACASH@789","700","20170831","22","refund","8888888"};


Object drawdownDetails[]={o1,o2};





/*Object drawdownDetails[]=new Object[50];

Object details[];
for(int i=0;i<50;i++)
{
details=new Object[6];
details[0]=bundle.getString("datacashreference");
details[1]="500"+i;
details[2]="2017090"+(i%30);
details[3]="19";
details[4]="drawdown";
details[5]=bundle.getString("merchantreference");



drawdownDetails[i]=details;
}
*/







Object[] params3 =new Object[] {"GBP","GB", "THOMSON", bundle.getString("dvTid"),applicationData,
drawdownDetails};
           Object o = (Object)xmlRpcClient.execute("datacash.submitddDrawdownBatch", params3);

           HashMap<String,String> hm=(HashMap<String,String>)o;







           Set s=hm.keySet();
           Iterator iterator=s.iterator();
           String sss="";
           String key;
           while(iterator.hasNext()){
key=(String)iterator.next();

sss+=key+" = ";
sss+=(String)hm.get(key)+"\t";



           }

           System.out.println(sss);




           String str2=(String)hm.get("responseXml");
           if(str2==null)
str2=sss;
           session.setAttribute("responseXML", str2);




             response.sendRedirect("/genericcpsatcom/ddresponse.jsp");

           }
           catch (XmlRpcException xre)
           {
              LogWriter.logErrorMessage(xre.getMessage(), xre);
              throw new RuntimeException(xre);
           }
           catch(Exception ex){
System.out.println(ex.toString());
           }



       }
       else if(ddi.equals("getddDrawdownBatchStatus")){


HashMap<String,String> drawdownstatus=new HashMap<String,String>();
drawdownstatus.put("datacashReferenceNumber",bundle.getString("batchdatacashreference"));
drawdownstatus.put("method","query");
drawdownstatus.put("merchantReferenceNumber",bundle.getString("batchmerchantreference"));

Object[] params4 =new Object[] {"GBP","GB", "THOMSON", bundle.getString("dvTid"),
applicationData,drawdownstatus};
try{
Object o = (Object)xmlRpcClient.execute("datacash.getddDrawdownBatchStatus", params4);

Object object[]=(Object[])o;
Object innerObject[]=null;

for(int i=0;i<object.length;i++){

innerObject=(Object[])object[i];
for(int j=0;j<innerObject.length;j++)
System.out.print(innerObject[j]+"\t");
System.out.println("\n");

}
}
catch(Exception ex){

System.out.println(ex.toString());
}


       }
       else if(ddi.equals("submitCancelMandateBatch"))
       {




try{
Object o1[]={bundle.getString("datacashreference"),"cancel",
bundle.getString("merchantreference")};
Object o2[]={bundle.getString("datacashreference2"),"cancel",
bundle.getString("merchantreference2")};
Object o3[]={"DATACASH@789","revoke","8888888"};

Object cancelMandateDetails[]={o1,o2};




/*Object cancelMandateDetails[]=new Object[50];
Object data[];

for(int i=0;i<50;i++)
{
data=new Object[3];
data[0]=bundle.getString("datacashreference");
data[1]="cancel";
data[2]=bundle.getString("merchantreference");


cancelMandateDetails[i]=data;
}*/








Object[] params5 =new Object[] {"GBP","GB", "THOMSON", bundle.getString("dvTid"),applicationData,
cancelMandateDetails};

Object o = (Object)xmlRpcClient.execute("datacash.submitCancelMandateBatch", params5);

           HashMap<String,String> hm=(HashMap<String,String>)o;







           Set s=hm.keySet();
           Iterator iterator=s.iterator();
           String sss="";
           while(iterator.hasNext()){
sss=sss+(String)iterator.next()+"=";
sss=sss+(String)hm.get((String)iterator.next())+"\t";

           }

           System.out.println(sss);









           String str2=(String)hm.get("responseXml");
           if(str2==null)
str2=sss;

           session.setAttribute("responseXML", str2);



             response.sendRedirect("/genericcpsatcom/ddresponse.jsp");

           }
           catch (XmlRpcException xre)
           {
              LogWriter.logErrorMessage(xre.getMessage(), xre);
              throw new RuntimeException(xre);
           }
           catch(Exception ex){
System.out.println(ex.toString());
           }



       }
       else if("getCancelMandateBatchStatus".equals(ddi))
       {






HashMap<String,String> cancelMandateDetails=new HashMap<String,String>();
cancelMandateDetails.put("datacashReferenceNumber",bundle.getString("batchdatacashreference"));
cancelMandateDetails.put("method","query");
cancelMandateDetails.put("merchantReferenceNumber",bundle.getString("batchmerchantreference"));

Object[] params6 =new Object[] {"GBP","GB", "THOMSON", bundle.getString("dvTid"),
applicationData,cancelMandateDetails};
try{
Object o = (Object)xmlRpcClient.execute("datacash.getCancelMandateBatchStatus", params6);

Object object[]=(Object[])o;
Object innerObject[]=null;

for(int i=0;i<object.length;i++){

innerObject=(Object[])object[i];
for(int j=0;j<innerObject.length;j++)
System.out.print(innerObject[j]+"\t");
System.out.println("\n");

}
}
catch(Exception ex){

System.out.println(ex.toString());
}







       }
       else if(ddi.equals("hybrisDDSetupMandate")){


try{
Map<String,String> ddDetails=new HashMap<String,String>();
String hsortCode=bundle.getString("sortcode");
String accountNumber=bundle.getString("accountnumber");
String firstName=bundle.getString("firstName");
String lastName=bundle.getString("lastName");
String hmethod=bundle.getString("method");
String merchantReference=bundle.getString("merchantreference");

ddDetails.put("sortCode",hsortCode);
ddDetails.put("accountNumber",accountNumber);
ddDetails.put("firstName",firstName);
ddDetails.put("lastName",lastName);
ddDetails.put("method",hmethod);
ddDetails.put("merchantreference",merchantReference);
ddDetails.put("houseNumber",bundle.getString("experian.housename"));
ddDetails.put("dob",bundle.getString("experian.dob"));
ddDetails.put("street",bundle.getString("experian.street"));
ddDetails.put("postCode",bundle.getString("experian.postcode"));
ddDetails.put("houseNumber",bundle.getString("experian.housenumber"));
ddDetails.put("flat",bundle.getString("experian.flat"));
ddDetails.put("bookingReference","thomson@1234");
ddDetails.put("bookingDepartureDate","03/14/2018");
ddDetails.put("token","zyxwvutsrqponmlkjihgfedcba");

Object[] param= new Object[] {"GBP","GB", "GB","THOMSON", bundle.getString("dvTid"),
ddDetails};

Object object = (Object)xmlRpcClient.execute("datacash.ddSetUpMandate", param);

HashMap<String, String> results=(HashMap<String,String>)object;



System.out.println("========================================"
+ "=======================================================================");
System.out.println(results);
System.out.println("========================================="
+"=====================================================================");
Set s=results.keySet();
Iterator iterator=s.iterator();
String sss="";
while(iterator.hasNext()){

sss=sss+(String)iterator.next()+"=";
sss=sss+(String)results.get((String)iterator.next())+"\t";

}

System.out.println(sss);


session.setAttribute("responseXML", sss);



response.sendRedirect("/genericcpsatcom/ddresponse.jsp");




}
catch(Exception ex){System.out.println(ex.toString());}





       }
       else if(ddi.equals("validateAccountDetails")){

try{

String esortcode=bundle.getString("experian.sortcode");
String eaccountnumber=bundle.getString("experian.accountnumber");

                Map validateDetails=new HashMap();

                validateDetails.put("sortCode",esortcode);
                validateDetails.put("accountNumber",eaccountnumber);


 Object[] validateParams =new Object[] {"GB",validateDetails};

 Object o = (Object)xmlRpcClient.execute("datacash.validateAccountDetails", validateParams);

HashMap<Object,Object> hm=(HashMap<Object,Object>)o;
Object obj=hm.get("bankAddress");
List<String> bankAddress=(List<String>)obj;

System.out.println("==============================================================="
+ "========================================================================"
+ "==========================");
System.out.println(hm);
System.out.println(bankAddress);

System.out.println("==========================================="
+ "================================================================="
+ "=====================================================");


Set s=hm.keySet();
Iterator iterator=s.iterator();
String sss="";
 while(iterator.hasNext()){
sss=sss+(Object)iterator.next()+"=";
sss=sss+hm.get(iterator.next())+"\t";

 }

System.out.println(sss);

String str2=(String)hm.get("responseXml");
if(str2==null)
str2=sss;

session.setAttribute("responseXML", str2);



response.sendRedirect("/genericcpsatcom/ddresponse.jsp");
}
catch (XmlRpcException xre)
{
LogWriter.logErrorMessage(xre.getMessage(), xre);
throw new RuntimeException(xre);
}
catch(Exception ex){
System.out.println(ex.toString());
}


}
else if(ddi.equals("verifyAccountDetails")){

try{

String esortcode=bundle.getString("experian.sortcode");
String eaccountnumber=bundle.getString("experian.accountnumber");
String efirstname=bundle.getString("experian.firstname");
String elastname=bundle.getString("experian.lastname");
String edob=bundle.getString("experian.dob");
String ehousenumber=bundle.getString("experian.housenumber");
String estreet=bundle.getString("experian.street");
String epostcode=bundle.getString("experian.postcode");
String ehousename=bundle.getString("experian.housename");
String eflat=bundle.getString("experian.flat");

Map verifyAccountDetails=new HashMap();

verifyAccountDetails.put("sortCode",esortcode);
verifyAccountDetails.put("accountNumber",eaccountnumber);
verifyAccountDetails.put("firstName",efirstname);
verifyAccountDetails.put("lastName",elastname);
verifyAccountDetails.put("dob",edob);
verifyAccountDetails.put("houseNumber",ehousenumber);
verifyAccountDetails.put("street",estreet);
verifyAccountDetails.put("postCode",epostcode);
verifyAccountDetails.put("houseName",ehousename);
verifyAccountDetails.put("flat",eflat);
verifyAccountDetails.put("bookingReference","thomson@1234");
verifyAccountDetails.put("bookingDepartureDate","03/14/2018");
verifyAccountDetails.put("token","zyxwvutsrqponmlkjihgfedcba");


Object[] verifyParams =new Object[] {"GB",verifyAccountDetails};

Object o = (Object)xmlRpcClient.execute("datacash.verifyAccountDetails", verifyParams);

HashMap<String,String> hm=(HashMap<String,String>)o;

System.out.println("========================================================"
+ "=================================================================");

System.out.println(hm);
System.out.println("========================================================"
+ "=================================================================");



Set s=hm.keySet();
Iterator iterator=s.iterator();
String sss="";
while(iterator.hasNext()){
sss=sss+(String)iterator.next()+"=";
sss=sss+(String)hm.get((String)iterator.next())+"\t";

}

System.out.println(sss);

String str2=(String)hm.get("responseXml");
if(str2==null)
str2=sss;

session.setAttribute("responseXML", str2);



response.sendRedirect("/genericcpsatcom/ddresponse.jsp");

}
catch (XmlRpcException xre)
{
LogWriter.logErrorMessage(xre.getMessage(), xre);
throw new RuntimeException(xre);
}
catch(Exception ex){
System.out.println(ex.toString());
}







}






























if("sss".equals(ddi)){


try{
String token=(String)request.getSession().getAttribute("token");
Object[] param6={token};
System.out.println("getPaymentData===============================================");
Object objectMap=xmlRpcClient.execute("commonPaymentService.getPaymentData",param6);

Map<String,String> paymentInfo=(HashMap<String,String>)objectMap;
System.out.println("getPaymentData========================================="+paymentInfo);

/*
Object essentialData=xmlRpcClient.execute("commonPaymentService.getEssentialTransactionData",
param6);


System.out.println("getEssential Transaction Data=");
ArrayList<EssentialTransactionData> essentialTransactionData=
(ArrayList<EssentialTransactionData>)essentialData;
for(Object etData:essentialTransactionData){
EssentialTransactionData etd=(EssentialTransactionData)etData;
PaymentScheduleData psd=(PaymentScheduleData)etd.getPaymentScheduleData().get(0);
System.out.println(psd.getAmount().getAmount()+"\t"+
psd.getPaymentScheduleId()+"\t"+psd.getPayCount());
}
System.out.println("getEssentialTransactionData================================="+essentialData);*/



Object[] transactions=(Object[])xmlRpcClient.execute("commonPaymentService."
+"getEssentialTransactionData",param6);

int length = transactions.length;
for (int i = 0; i < length; i++)
{
EssentialTransactionData essentialTransactionData =
                                  (EssentialTransactionData) transactions[i];


PaymentScheduleData psd=(PaymentScheduleData)
(essentialTransactionData.getPaymentScheduleData().get(0));
System.out.println(psd.getAmount().getAmount()+"\t"+
psd.getPaymentScheduleId()+"\t"+psd.getPayCount()+psd.getFirstInstallmentDate());

System.out.println("============================================"
+ "========================================== ");
System.out.println("DDSelected by the user : "+
essentialTransactionData.getDdSelectedByUser());
System.out.println("============================================="
+ "=======================================");


}

}
catch(Exception ex){

System.out.println(ex.toString());


}


}
else if("paypal".equals(ddi)){
String token = (String)request.getParameter("paypalToken");

String tomcatInstance ="A12";
try{
System.out.println(token+"============================"
+ "======================================================");

CommonPaymentClient cpc =new CommonPaymentClient(AppResource.getConfEntry("xmlrpcURL", null));
Object object=cpc.verifyPaypalCustomerDetails(token);
System.out.println("ddpayment payment servlet======================"
+ "=============================="+object.getClass().getName());

}
catch(XmlRpcException ex){
System.out.println(ex);

}
}





















































   }

   /**
    * This method process xml request and gives response
    *
    * @param req.
    * @param resp.
    * @throws ServletException.
    * @throws IOException.
    *
    */
   protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException
   {
      doGet(req, resp);
   }







   private XmlRpcClient getXmlRpcClient()
   {
      final XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
      config.setEnabledForExtensions(true);
      try
      {
         config.setServerURL(new URL(AppResource.getConfEntry("xmlrpcURL", null)));
      }
      catch (MalformedURLException mue)
      {
         LogWriter.logErrorMessage(mue.getMessage(), mue);
         throw new RuntimeException(mue.getMessage());
      }

      xmlRpcClient = new XmlRpcClient();


      final XmlRpcTransportFactory transportFactory = new XmlRpcTransportFactory()
      {
          public XmlRpcTransport getTransport()
          {
              return new XmlRpcLogging(xmlRpcClient);
          }
      };





   //   xmlRpcClient.setTransportFactory(new XmlRpcCommonsTransportFactory(xmlRpcClient));
      xmlRpcClient.setTransportFactory(transportFactory);



      xmlRpcClient.setConfig(config);

      httpClient = new HttpClient();

      final XmlRpcCommonsTransportFactory factory = new XmlRpcCommonsTransportFactory(xmlRpcClient);
      factory.setHttpClient(httpClient);

 //     xmlRpcClient.setTransportFactory(factory);
      xmlRpcClient.setTransportFactory(transportFactory);


      return xmlRpcClient;
   }




















}