package com.tui.uk.payment.generic;

import com.datacash.util.XMLDocument;
import com.tui.uk.payment.exception.IllegalHostException;
import com.tui.uk.payment.pojo.CardResponse;
import com.tui.uk.payment.pojo.DataCashService;
import com.tui.uk.payment.pojo.DocBuilder;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.xml.sax.SAXException;

/**
 * This class is responsible for sending  xml request to datacash 
 * and displays the response in the form of xml.
 * 
 */
public class ProcessRequestServlet extends HttpServlet
{
   /** The serialVersionUID. */
   private static final long serialVersionUID = 1L;

   /**
    * This method process xml request and gives response.
    * 
    * @param req the request.
    * @param resp the response.
    * @throws ServletException ServletException.
    * @throws IOException the IOException.
    * 
    */
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException
   {
      try
      {
         String requestxml = req.getParameter("requestxml");
         String hostURL = req.getParameter("host");
         requestxml.trim();
         String responsexml = new String();
         DocBuilder doc = new DocBuilder();
         DataCashService dataCashService = new DataCashService();
         XMLDocument response = dataCashService.processAuth(doc.getXMLDoc(requestxml), hostURL);
         responsexml = response.getSanitizedDoc();
         CardResponse cardResponse = null;
         if (responsexml.contains("<datacash_reference>"))
         {
            cardResponse = new CardResponse(response);
         }

         HttpSession session = req.getSession();
         session.setAttribute("requestxml", requestxml);
         session.setAttribute("responsexml", responsexml);
         session.setAttribute("cardres", cardResponse);
         resp.sendRedirect("response.jsp");
      }
      catch (SAXException e)
      {
         resp.sendRedirect("parsingError.jsp");
      }
      catch (IllegalHostException e)
      {
         resp.sendRedirect("illegalAgent.jsp");
      }
   }

   /**
    * This method process xml request and gives response
    * 
    * @param req.
    * @param resp.
    * @throws ServletException.
    * @throws IOException.
    * 
    */
   protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException
   {
      doGet(req, resp);
   }
}