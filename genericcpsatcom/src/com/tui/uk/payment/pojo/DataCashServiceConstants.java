/*
 * Copyright (C)2008 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 * 
 * $RCSfile: DataCashServiceConstants.java,v $
 * 
 * $Revision: 1.3 $
 * 
 * $Date: 2008-05-07 14:24:20 $
 * 
 * $Author: thomas.pm $
 * 
 * $Log: not supported by cvs2svn $
 */
package com.tui.uk.payment.pojo;

/**
 * This contains all the constants required for datacash service.
 * 
 * @author thomas.pm
 * 
 */
public final class DataCashServiceConstants
{
   /**
    * Private constructor to stop instantiation of this class.
    */
   private DataCashServiceConstants()
   {
      // Do nothing.
   }

   /** The constant for card number. */
   public static final String PAN = "pan";

   /** The constant for start date. */
   public static final String START_DATE = "startdate";

   /** The constant for end date. */
   public static final String EXPIRY_DATE = "expirydate";

   /** The constant for Issue Number. */
   public static final String ISSUE_NUMBER = "issuenumber";

   /** The constant for CV2 in card. */
   public static final String CV2 = "cv2";

   /** The constant for post code. */
   public static final String POST_CODE = "postcode";

   /** The constant for street number. */
   public static final String STREET_NUMBER = "street_address1";

   /** The constant for name on card. */
   public static final String NAME_ON_CARD = "name";

   /** The constant for amount. */
   public static final String AMOUNT = "Request.Transaction.TxnDetails.amount";

   /** The constant for SUCCESS STATUS. */
   public static final int SUCCESS_STATUS = 1;

   /** The constant for THREED_SUCCESS. */
   public static final int THREED_SUCCESS_STATUS = 150;

   /** The constant for FAILURE STATUS. */
   public static final int FAILURE_STATUS = 7;

   /** The constant for status. */
   public static final String STATUS_RS = "Response.status";

   /** The constant for reason. */
   public static final String REASON_RS = "Response.reason";

   /** The constant for description. */
   public static final String DESCRIPTION = "Response.description";

   /** The constant for historic transaction reference. */
   public static final String DATACASH_REFERENCE = "Request.Transaction.HistoricTxn.reference";

   /** The constant for data cash reference response. */
   public static final String DATACASH_REFERENCE_RS = "Response.datacash_reference";

   /** The constant for merchant reference request. */
   public static final String MERCHANT_REFERENCE =
      "Request.Transaction.TxnDetails.merchantreference";

   /** The constant for merchant reference response. */
   public static final String MERCHANT_REFERENCE_RS = "Response.merchantreference";

   /** The constant for issuer. */
   public static final String ISSUER_RS = "Response.CardTxn.issuer";

   /** The constant for country. */
   public static final String COUNTRY_RS = "Response.CardTxn.country";

   /** The constant for card scheme. */
   public static final String CARD_SCHEME_RS = "Response.CardTxn.card_scheme";

   /** The constant for card request authorization code. */
   public static final String AUTH_CODE = "Request.Transaction.CardTxn.authcode";

   /** The constant for historic authorization code. */
   public static final String AUTHORI_CODE = "Request.Transaction.HistoricTxn.authcode";

   /** The constant for card response authorization code. */
   public static final String AUTH_CODE_RS = "Response.CardTxn.authcode";

   /** The constant for time. */
   public static final String TIME_RS = "Response.time";

   /** The constant for mode. */
   public static final String MODE_RS = "Response.mode";

   /** The constant for host name. */
   public static final String HOST_NAME = "datacash.HostName";

   /** The constant for authorized transaction. */
   public static final String AUTH_TRANSACTION = "auth";

   /** The constant for pre authorized transaction. */
   public static final String PRE_TRANSACTION = "pre";

   /** The constant for erp transaction. */
   public static final String ERP_TRANSACTION = "erp";

   /** The constant for refund transaction. */
   public static final String REFUND_TRANSACTION = "refund";

   /** The constant for fulfill transaction. */
   public static final String FULFILL_TRANSACTION = "fulfill";

   /** The constant for cancel transaction. */
   public static final String CANCEL_TRANSACTION = "cancel";

   /** The constant for reverse transaction. */
   public static final String REVERSE_TRANSACTION = "txn_refund";

   /** The constant for redeem transaction. */
   public static final String REDEEM_TRANSACTION = "redeem";

   /** The constant for PPT reverse transaction. */
   public static final String PPT_REVERSE_TRANSACTION = "reverse";

   /** The constant for card transaction type. */
   public static final String TRANSACTION_TYPE = "Request.Transaction.CardTxn.method";

   /** The constant for historic transaction type. */
   public static final String TRANSACT_TYPE = "Request.Transaction.HistoricTxn.method";

   /** The constant for client. */
   public static final String CLIENT = "Request.Authentication.client";

   /** The constant for password. */
   public static final String PASSWORD = "Request.Authentication.password";

   /** The constant for capture method. */
   public static final String CAPTURE_METHOD = "Request.Transaction.TxnDetails.capturemethod";

   /** The constant for Terminal. */
   public static final String TERMINAL = "Request.Transaction.CardTxn.Terminal";

   /** The constant for terminal_capabilities. */
   public static final String TERMINAL_CAPABILTIES =
      "Request.Transaction.CardTxn.Terminal.terminal_capabilities";

   /** The constant for features_capabilities. */
   public static final String FEATURE_CAPABILTIES =
      "Request.Transaction.CardTxn.Terminal.features_capabilities";

   /** The constant for card_details. */
   public static final String CARD_DETAILS = "Request.Transaction.CardTxn.card_details";

   /** The constant for term_type. */
   public static final String ICC_TERM_TYPE = "Request.Transaction.CardTxn.ICC.term_type";

   /** The constant for term_type_value. */
   public static final String ICC_TERM_TYPE_VALUE = "01";

   /** The constant for ic_reader. */
   public static final String IC_READER = "ic_reader";

   /** The constant for magnetic_stripe_reader. */
   public static final String MAGNETIC_STRIPE_READER = "magnetic_stripe_reader";

   /** The constant for manual_card_entry. */
   public static final String MANUAL_CARD_ENTRY = "manual_card_entry";

   /** The constant for pin_pad_available. */
   public static final String PIN_PAD_AVAILABLE = "pin_pad_available";

   /** The constant for track2_data. */
   public static final String TRACK2DATA = "track2_data";

   /** The constant for track2_data type. */
   public static final String TRACK2DATA_TYPE = "type";

   /** The constant for terminal id type. */
   public static final String TERMINALID_TYPE = "id";

   /** The constant for TRUE. */
   public static final String TRUE = "true";

   /** The constant for FALSE. */
   public static final String FALSE = "false";

   /** The constant for capture swiped. */
   public static final String CAPTUREMETHOD_SWIPED = "swiped";

   /** The constant for capture CNP. */
   public static final String CAPTUREMETHOD_CNP = "cnp";

   /** The constant for cv2 not provided. */
   public static final String NOT_PROVIDED = "notprovided";

   /** The constant for cv2 not checked. */
   public static final String NOT_CHECKED = "notchecked";

   /** The constant for cv2 matched. */
   public static final String MATCHED = "matched";

   /** The constant for cv2 not matched. */
   public static final String NOT_MATCHED = "notmatched";

   /** The constant for cv2 partial match. */
   public static final String PARTIAL_MATCH = "partialmatch";

   /** The constant for cv2 policy . */
   public static final String CV2_POLICY =
      "Request.Transaction.CardTxn.Card.Cv2Avs.ExtendedPolicy.cv2_policy";

   /** The constant for post code policy . */
   public static final String POSTCODE_POLICY =
      "Request.Transaction.CardTxn.Card.Cv2Avs.ExtendedPolicy.postcode_policy";

   /** The constant for post code result . */
   public static final String POSTCODE_RESULT = "Response.CardTxn.Cv2Avs.postcode_result";

   /** The constant for post code result . */
   public static final String CVV_RESULT = "Response.CardTxn.Cv2Avs.cv2_result";

   /** The constant for address policy . */
   public static final String ADDRESS_POLICY =
      "Request.Transaction.CardTxn.Card.Cv2Avs.ExtendedPolicy.address_policy";

   /** The constant for address result . */
   public static final String ADDRESS_RESULT = "Response.CardTxn.Cv2Avs.address_result";

   /** The constant for accept. */
   public static final String ACCEPT = "accept";

   /** The constant for reject. */
   public static final String REJECT = "reject";

   /** The constant for card response ACS URL. */
   public static final String ACS_URL = "Response.CardTxn.ThreeDSecure.acs_url";

   /** The constant for card response authorization code. */
   public static final String PAREQ_MESSAGE = "Response.CardTxn.ThreeDSecure.pareq_message";

   /** The constant for 3D-Secure. */
   public static final String THREEDSECURE_VERIFY =
      "Request.Transaction.TxnDetails.ThreeDSecure.verify";

   /** The constant for 3D-Secure merchant URL. */
   public static final String THREEDSECURE_MERCHANT_URL =
      "Request.Transaction.TxnDetails.ThreeDSecure.merchant_url";

   /** The constant for 3D-Secure purchase description. */
   public static final String THREEDSECURE_PURCAHSE_DESC =
      "Request.Transaction.TxnDetails.ThreeDSecure.purchase_desc";

   /** The constant for 3D-Secure purchase date and time. */
   public static final String THREEDSECURE_PURCHASE_DATETIME =
      "Request.Transaction.TxnDetails.ThreeDSecure.purchase_datetime";

   /** The constant for 3D-Secure browser device category. */
   public static final String THREEDSECURE_BROWSER_DEVICE_CATEGORY =
      "Request.Transaction.TxnDetails.ThreeDSecure.Browser.device_category";

   /** The constant for 3D-Secure browser acceptable headers. */
   public static final String THREEDSECURE_BROWSER_HEADERS =
      "Request.Transaction.TxnDetails.ThreeDSecure.Browser.accept_headers";

   /** The constant for 3D-Secure browser user agent. */
   public static final String THREEDSECURE_BROWSER_USERAGENT =
      "Request.Transaction.TxnDetails.ThreeDSecure.Browser.user_agent";

   /** The constant for card enrollment check. */
   public static final int CARDENROLLMENT_STATUS = 150;

   /** The constant for card enrollment check. */
   public static final int CARDENROLLMENT_FAILED = 168;

   /** The multiplier constant for getting proper date. */
   public static final int TIME_MULTIPLIER = 1000;

   /** The constant for card response authorization code. */
   public static final String PARES_MESSAGE = "Request.Transaction.HistoricTxn.pares_message";

   /** The constant for PPT transaction pan. */
   public static final String PPT_TXN_PAN = "Request.Transaction.PPTCardTxn.pan";

   /** The constant for PPT transaction method. */
   public static final String PPT_TXN_METHOD = "Request.Transaction.PPTCardTxn.method";

   /** The constant for PPT transaction pin. */
   public static final String PPT_TXN_PIN = "Request.Transaction.PPTCardTxn.pin";

   /** The constant for PPT transaction reference. */
   public static final String PPT_TXN_REFERENCE = "Request.Transaction.PPTCardTxn.reference";

   /** The constant for card transaction card details. */
   public static final String CARDTXN_CARDDETAILS = "Request.Transaction.CardTxn.card_details";

   /** The constant for pre registered. */
   public static final String PREREGISTERED = "preregistered";

   /** The payment transaction type. */
   public static final String PAYMENT_TRANS_TYPE = "P";

   /** The refund transaction type. */
   public static final String REFUND_TRANS_TYPE = "R";

   /** The data-cash response code length. */
   public static final int DC_RESPONSE_CODE_LENGTH = 3;

   /** The data-cash success code. */
   public static final String DC_SUCCESS_CODE = "001";

   /** Error code if issue number is missing. */
   public static final int ERRORCODE_ISSUENOMISSING = 909;

   /** Error code if there is issue number mismatch. */
   public static final int ERRORCODE_ISSUENOMISMATCH = 910;

   /** Error code if issue number is invalid. */
   public static final int ERRORCODE_INVALIDISSUENO = 911;

   /** Error code if card start date is missing. */
   public static final int ERRORCODE_STARTDATEMISSING = 912;

   /** Error code if there is data-cash card type mismatch. */
   public static final int ERRORCODE_DATACASHCARDMISMATCH = 913;

   /** The carriage return. */
   public static final char CARRIAGE_RETURN = 0xD;

   /** The lined feed. */
   public static final char LINE_FEED = 0xA;

   /** Error code if there is fatal errors. */
   public static final int ERRORCODE_FOR_FATAL_ERRRORS = 555;

   /** Error code if there is general errors. */
   public static final int ERRORCODE_FOR_GENERAL_ERRRORS = 666;

   /** The street address two.Will hold remaining address details. */
   public static final String STREETADDRESS = "street_address2";

   /** The street address three.Will hold town/city. */
   public static final String TOWN = "street_address3";

   /** The street address four.Will hold county. */
   public static final String COUNTY = "street_address4";

   /** The constant for ECI. */
   public static final String ECI = "Response.QueryTxnResult.ThreeDSecure.eci";

}
