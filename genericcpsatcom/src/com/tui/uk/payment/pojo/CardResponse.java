package com.tui.uk.payment.pojo;

import com.datacash.util.XMLDocument;

/**
 * This class holds the details that the datacash returns for the transactions. This is mainly used
 * for authorization transactions.
 * 
 * @author thomas.pm
 */
public class CardResponse extends Response
{
   /**
    * The String holding The Card Issuing Bank.
    */
   private String issuer;

   /**
    * The String holding The card scheme.
    */
   private String cardScheme;

   /**
    * The String holding The Country of Issue.
    */
   private String country;

   /**
    * The String holding Authorisation code for successful transactions.
    */
   private String authCode;

   /**
    * This constructs CardResponse from the <code>XMLDocument</code> object.
    * 
    * @param xmlDocument the <code>XMLDocument</code> of the datacash.
    */
   public CardResponse(XMLDocument xmlDocument)
   {
      super(xmlDocument);
      issuer = xmlDocument.get(DataCashServiceConstants.ISSUER_RS);
      cardScheme = xmlDocument.get(DataCashServiceConstants.CARD_SCHEME_RS);
      country = xmlDocument.get(DataCashServiceConstants.COUNTRY_RS);
      authCode = xmlDocument.get(DataCashServiceConstants.AUTH_CODE_RS);
   }

   /**
    * Getting the Issuer.
    * 
    * @return the issuer
    */
   public String getIssuer()
   {
      return issuer;
   }

   /**
    * Getting the card scheme.
    * 
    * @return the cardScheme
    */
   public String getCardScheme()
   {
      return cardScheme;
   }

   /**
    * Getting the country.
    * 
    * @return the country
    */
   public String getCountry()
   {
      return country;
   }

   /**
    * Getting the authorized code.
    * 
    * @return the authCode
    */
   public String getAuthCode()
   {
      return authCode;
   }

}
