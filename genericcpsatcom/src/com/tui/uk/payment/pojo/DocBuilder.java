package com.tui.uk.payment.pojo;

import com.datacash.util.XMLDocument;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.jdom.JDOMException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This class reponsible for writing xml to file.
 *
 * @author sindhushree.g
 *
 */
public class DocBuilder
{
   /**
    * method responsible for giving response in xml.
    * 
    * @param xml the request xml.
    * @throws SAXException the SAXException.
    * @return XMLDocument the xml document.
    */
   public XMLDocument getXMLDoc(String xml) throws SAXException
   {
      ByteArrayInputStream xmlIn = new ByteArrayInputStream(xml.getBytes());
      DocumentBuilderFactory builderfac = DocumentBuilderFactory.newInstance();
      try
      {
         DocumentBuilder builder = builderfac.newDocumentBuilder();

         Document doc = builder.parse(xmlIn);
         StringWriter output = new StringWriter();

         TransformerFactory.newInstance().newTransformer()
            .transform(new DOMSource(doc), new StreamResult(output));

         File file = new File("D:\\datacash_request.xml");
         FileOutputStream fout = new FileOutputStream(file);

         fout.write(output.toString().getBytes());

         XMLDocument xmlDoc = new XMLDocument(file);
         return xmlDoc;
      }
      catch (IOException ioe)
      {
         ioe.printStackTrace();
      }
      catch (SAXException se)
      {
         se.printStackTrace();
         throw new SAXException();
      }
      catch (JDOMException jde)
      {
         jde.printStackTrace();
      }
      catch (TransformerException te)
      {
         te.printStackTrace();
      }
      catch (ParserConfigurationException pce)
      {
         pce.printStackTrace();
      }

      return null;
   }
}