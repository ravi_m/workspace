package com.tui.uk.payment.pojo;


import com.datacash.client.Agent;
import com.datacash.errors.FailureReport;
import com.datacash.util.XMLDocument;
import com.tui.uk.payment.exception.IllegalHostException;
/**
 * This class is responsible for sending  xml request to datacash.
 **/
public class DataCashService
{

   /**
    * method responsible for connecting to dataCash.
    * @param xmlDoc xmlDoc.
    * @param host host.
    * @throws IllegalHostException IllegalHostException.
    * @return xml.
    * 
    */
   public XMLDocument processAuth(XMLDocument xmlDoc, String host) throws IllegalHostException
   {
      try
      {
         String DatacashHost;
         // String DatacashHost;
         if ((host == null) || (host.length() < 1))
         {
            DatacashHost = "https://testserver.datacash.com/Transaction";
         }
         else
         {
            DatacashHost = host;
         }
         Agent agent = new Agent();
         agent.setHost(DatacashHost);
         XMLDocument response = agent.request(xmlDoc);
         if (response == null)
         {
            throw new IllegalHostException();
         }
         return response;
      }
      catch (FailureReport failureReport)
      {
         failureReport.getMessage();
      }
      throw new IllegalHostException();
   }
}