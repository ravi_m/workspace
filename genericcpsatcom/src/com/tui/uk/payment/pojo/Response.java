/*
 * Copyright (C)2008 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 * 
 * $RCSfile: Response.java,v $
 * 
 * $Revision: 1.3 $
 * 
 * $Date: 2008-05-07 14:21:10 $
 * 
 * $Author: thomas.pm $
 * 
 * $Log: not supported by cvs2svn $
 */

package com.tui.uk.payment.pojo;

import static com.tui.uk.payment.pojo.PropertyConstants.MESSAGES_PROPERTY;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

import com.datacash.util.XMLDocument;
import com.tui.uk.log.LogWriter;

/**
 * This class holds the details returned form different datacash transactions.
 * 
 * @author thomas.pm
 */
public class Response
{
   /** The String holding DataCash Reference. */
   private final String datacashReference;

   /** The String holding Merchant Reference. */
   private final String merchantReference;

   /** The String holding mode that the DataCash server is operating . */
   private final Mode mode;

   /**
    * The variable holding date and time of the DataCash server when it received the transaction.
    */
   private final Date time;

   /** The string holding description of the response. */
   private final String description;

   /** The status code. */
   private final int code;

   /**
    * The variable holding address policy code.
    */
   private final String addressPolicyResultCode;

   /**
    * The variable holding post code policy code.
    */
   private final String postCodePolicyResultCode;

   /**
    * The variable holding cvv policy result code.
    */
   private final String cvvPolicyResultCode;

   /**
    * Constructor with <code>XMLDocument</code> as parameter.
    * 
    * @param xmlDocument the <code>XMLDocument</code> of the datacash.
    */
   public Response(XMLDocument xmlDocument)
   {
      code = Integer.parseInt(xmlDocument.get(DataCashServiceConstants.STATUS_RS));
      description = xmlDocument.get(DataCashServiceConstants.REASON_RS);
      datacashReference = xmlDocument.get(DataCashServiceConstants.DATACASH_REFERENCE_RS);
      time =
         new Date(Long.parseLong(xmlDocument.get(DataCashServiceConstants.TIME_RS))
            * DataCashServiceConstants.TIME_MULTIPLIER);
      mode = Mode.findByCode(xmlDocument.get(DataCashServiceConstants.MODE_RS));
      merchantReference = xmlDocument.get(DataCashServiceConstants.MERCHANT_REFERENCE_RS);
      addressPolicyResultCode = xmlDocument.get(DataCashServiceConstants.ADDRESS_RESULT);
      postCodePolicyResultCode = xmlDocument.get(DataCashServiceConstants.POSTCODE_RESULT);
      cvvPolicyResultCode = xmlDocument.get(DataCashServiceConstants.CVV_RESULT);
   }

   /**
    * Getter for data cash reference.
    * 
    * @return datacashReference the DataCash Reference.
    */
   public final String getDatacashReference()
   {
      return datacashReference;
   }

   /**
    * Getter for Merchant reference.
    * 
    * @return MerchantReference the Merchant Reference.
    */
   public final String getMerchantReference()
   {
      return merchantReference;
   }

   /**
    * Getter for mode.
    * 
    * @return mode the mode of operation.
    */
   public final Mode getMode()
   {
      return mode;
   }

   /**
    * Getter for time.
    * 
    * @return time time of the DataCash server when it received the transaction.
    */
   public final Date getTime()
   {
      return new Date(time.getTime());
   }

   /**
    * Getter for description.
    * 
    * @return description description of the response.
    */
   public final String getDescription()
   {
      return description;
   }

   /**
    * Getter for the code.
    * 
    * @return the code.
    */
   public final int getCode()
   {
      return code;
   }

   /**
    * Method to make the class non cloneable.
    * 
    * @return Object.
    * @throws CloneNotSupportedException the clone not supported exception.
    */
   public final Object clone() throws CloneNotSupportedException
   {
      LogWriter.logErrorMessage(PropertyResource.getProperty("class.non.cloneable",
         MESSAGES_PROPERTY));
      throw new CloneNotSupportedException(PropertyResource.getProperty("class.non.cloneable",
         MESSAGES_PROPERTY));
   }

   /**
    * Method to non serialize the class.
    * 
    * @param out the ObjectOutputStream.
    * @throws IOException the IO Exception.
    */
   public final void writeObject(ObjectOutputStream out) throws IOException
   {
      LogWriter.logErrorMessage(PropertyResource.getProperty("class.non.serialized",
         MESSAGES_PROPERTY));
      throw new IOException(PropertyResource.getProperty("class.non.serialized", 
         MESSAGES_PROPERTY));
   }

   /**
    * Method to non deserialize the class.
    * 
    * @param inputStream the ObjectInputStream.
    * @throws IOException the IO Exception.
    */
   public final void readObject(ObjectInputStream inputStream) throws IOException
   {
      LogWriter.logErrorMessage(PropertyResource.getProperty("class.non.deserialized",
         MESSAGES_PROPERTY));
      throw new IOException(PropertyResource.getProperty("class.non.deserialized",
         MESSAGES_PROPERTY));
   }

   /**
    * Getting the address result code.
    * 
    * @return the addressPolicyResultCode.
    */
   public String getAddressPolicyResultCode()
   {
      return addressPolicyResultCode;
   }

   /**
    * Getting the post code result code.
    * 
    * @return the postCodePolicyResultCode.
    */
   public String getPostCodePolicyResultCode()
   {
      return postCodePolicyResultCode;
   }

   /**
    * Gets the cvv policy result code.
    * 
    * @return the cvvResultCode.
    */
   public String getCvvPolicyResultCode()
   {
      return cvvPolicyResultCode;
   }

}
