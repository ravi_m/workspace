package com.tui.uk.payment.exception;

/**
 * This class throws exception if proper hostName is not provided.
 * 
 **/
public class IllegalHostException
  extends Exception
{

   /**
    * serialVersionUID.
    */
   private static final long serialVersionUID = 1L;
  
  /**
   * errorMessage.
   */
  private String errorMessage;
  
  /**
   * IllegalHostException constructor.
   */
  public IllegalHostException()
  {
     
  }
    
  /**
   * IllegalHostException constructor.
   * 
   * @param errorMessage errorMessage.
   * 
   */
  public IllegalHostException(String errorMessage)
  {
    this.errorMessage = errorMessage;
  }
  
  
}
