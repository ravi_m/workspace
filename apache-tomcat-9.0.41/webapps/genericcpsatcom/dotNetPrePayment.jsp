<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<TITLE> New Document </TITLE>
    <script>
    var passcount=0;
	function addPass(){
	 passcount = document.getElementById('no_pass').value;
	var ni = document.getElementById('passcont');
	ni.innerHTML ="";
	var head = '<div class="titlecol">Title</div><div class="col">ForeName</div><div class="col">Middle Initials</div><div class="col">SurName</div>';
	var headdiv = document.createElement('div');
	headdiv.innerHTML = head;
	ni.appendChild(headdiv);
	var selector_options ="<option value='Mr'>Mr</option><option value='Mrs'>Mrs</option><option value='Miss'>Miss</option><option value='Ms'>Ms</option><option value='Dr'>Dr</option><option value='Rev'>Rev</option><option value='Prof'>Prof</option>";
		for(i=1;i<=passcount;i++)
		{
		var newdiv = document.createElement('div');
		newdiv.setAttribute("id","passinger"+i);
		newdiv.setAttribute("class","passinger");
		var html = '<div class="titlecol"><select id="title'+i+'" name="passenger_initial"/>'+selector_options+'</select></div><div class="col"><input type="text" id="forename'+i+'" name="passenger_forename"/></div><div class="col"><input type="text" id="middlename'+i+'" name="passenger_middlename"/></div><div class="col"><input type="text" id="surname'+i+'" name="passenger_surname"/></div>';
		newdiv.innerHTML = html;
		ni.appendChild(newdiv);
		}

	}


		function populatetable(){
		  for(i=1;i<parseInt(document.getElementById('passengercount').value);i++)
		{
				addRow();
		}

		}

		//add a new row to the table
			function addRow()
			{
			     //val=parseInt(document.getElementById('passengercount').value);
			    // val=val+1;
			    // tot=val+"";


				//add a row to the rows collection and get a reference to the newly added row
				var newRow = document.all("tblGrid").insertRow();

				//add 3 cells (<td>) to the new row and set the innerHTML to contain text boxes
				var oCell = newRow.insertCell();
				oCell.innerHTML = "<select name='passenger_initial'><option value='Mr'>Mr</option><option value='Mrs'>Mrs</option><option value='Miss'>Miss</option><option value='Ms'>Ms</option><option value='Dr'>Dr</option><option value='Rev'>Rev</option><option value='Prof'>Prof</option></select>";

			    oCell = newRow.insertCell();
				oCell.innerHTML = "<input type='text' name='passenger_forename'>";

				oCell = newRow.insertCell();
				oCell.innerHTML = "<input type='text' name='passenger_middlename'>";

				oCell = newRow.insertCell();
				oCell.innerHTML = "<input type='text' name='passenger_surname'> &nbsp;&nbsp;<input type='button' value='Delete' onclick='removeRow(this);'/>";
			}

			//deletes the specified row from the table
			function removeRow(src)
			{
				/* src refers to the input button that was clicked.
				   to get a reference to the containing <tr> element,
				   get the parent of the parent (in this case case <tr>)
				*/
				var oRow = src.parentElement.parentElement;

				//once the row reference is obtained, delete it passing in its rowIndex
				document.all("tblGrid").deleteRow(oRow.rowIndex);
				document.getElementById('passengercount').value=document.getElementById('passengercount').value-1;
			}
		</script>

	<style>
	#container{
	width:400px;
	_width:300px;
	clear:both;
	}
	#passcont{
	width:560px;
	}
	#passcont .titlecol{
	width:60px;
	float:left;
	padding:2px;
	}
	#passcont .col{
	width:150px;
	float:left;
	padding:2px;
	}
	#container .item{
	float:left;
	padding:2px;
	display:block;
	}
	#container .col{
	float:left;
	padding:2px;
	}
	</style>

</HEAD>
<BODY>
<hr/>
<form action="/genericcpsclient/prepayment" method="post">

<div class="col">
Payment Amount: &nbsp; &nbsp; &nbsp;
<input type="text" id="payment.full" name="payment.full" value="0"/>
</div>
<br/>

<div class="col">
Deposit Amount:&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp; <input type="text" id="payment.deposit" name="payment.deposit"  value="0"/>
</div>
<br/>


<div class="col">
Low Deposit Amount: <input type="text" id="payment.lowdeposit" name="payment.lowdeposit" value="0"/>
</div>
<br/>


<br/>
<div style="clear:both;">
<hr/>
</div>
<div id="passid">
Click To  Add More Passengers: <INPUT onclick=addRow(); type=button value="Add Row">
</div>
<TABLE id=tblGrid style="TABLE-LAYOUT: fixed">
  <TBODY>
  <TR>
    <TD width=50>Intial</TD>
    <TD width=150>ForeName</TD>
    <TD width=150>Middle Initials</TD>
	<TD width=530>SurName</TD></TR>
  <TR>
    <TD><select name='passenger_initial'><option value='Mr'>Mr</option><option value='Mrs'>Mrs</option><option value='Miss'>Miss</option><option value='Ms'>Ms</option><option value='Dr'>Dr</option><option value='Rev'>Rev</option><option value='Prof'>Prof</option></select></TD>
    <TD><INPUT name="passenger_forename" value="test"></TD>
    <TD><INPUT name="passenger_middlename" value="t"></TD>
    <TD><INPUT name="passenger_surname" value="testsur"></TD></TR>
<HR>

<div style="clear:both;">
<hr/>
</div>
<table border=0>
<tr>
<td>Payment Types:<br/>
<SELECT MULTIPLE name="paymenttype" SIZE="7">
<OPTION VALUE="Cash">Cash
<OPTION VALUE="Cheque">Cheque.
<OPTION VALUE="Voucher" selected>Voucher
<OPTION VALUE="Card using PDQ">Card using PDQ
<OPTION VALUE="Card using PIN pad">Card using PIN pad
<OPTION VALUE="Card,CNP">Card, CNP
<OPTION VALUE="Post payment guarantee">Post payment guarantee
</SELECT>
</td>
<td>CardTypes:<br/>
<SELECT MULTIPLE name="cardtype" SIZE="11">
<OPTION VALUE="American Express">American Express
<OPTION VALUE="ATM ">ATM .
<OPTION VALUE="Debit Mastercard">Mastercard Debit
<OPTION VALUE="Diners Club">Diners Club
<OPTION VALUE="Discover ">Discover
<OPTION VALUE="EnRoute">EnRoute
<OPTION VALUE="GE Capital">GE Capital
<OPTION VALUE="JCB">JCB
<OPTION VALUE="Laser">Laser
<OPTION VALUE="Maestro" selected>Maestro
<OPTION VALUE="Mastercard">Mastercard Credit
<OPTION VALUE="Platima">Platima
<OPTION VALUE="Solo">Solo
<OPTION VALUE="Switch">Switch
<OPTION VALUE="VISA">VISA
<OPTION VALUE="VISA Delta">VISA Delta
<OPTION VALUE="VISA Electron">VISA Electron
<OPTION VALUE="VISA Purchasing">VISA Purchasing
</SELECT>
</td>
<td>Country</td>
<td>
<select MULTIPLE name="country" id="country" SIZE="5">
<option value="Am-America" selected>America</option>
<option value="In-India">India</option>
<option value="Eu-Europe">Europe</option>
<option value="EN-England">England</option>
</select>
</td>
</tr>
</table>
<input type="hidden" name="clientcode" value="1"/>
<input type="submit" value="submit"/>
</form>
</BODY>
</HTML>

