<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
   <TITLE> New Document </TITLE>
      <style>
   #container{
   width:400px;
   _width:300px;
   clear:both;
   }
   #passcont{
   width:560px;
   }
   #passcont .titlecol{
   width:60px;
   float:left;
   padding:2px;
   }
   #passcont .col{
   width:150px;
   float:left;
   padding:2px;
   }
   #container .item{
   float:left;
   padding:2px;
   display:block;
   }
   #container .col{
   float:left;
   padding:2px;
   }
   </style>

</HEAD>
<script language="javascript">

function addRow(tblId)
{
  var tableBody = document.getElementById("passengerDetailsTable").tBodies[0];
  var newRow = tableBody.insertRow(-1);
  var rowIndex = newRow.sectionRowIndex;

  var newCell0 = newRow.insertCell(0);
  newCell0.innerHTML = '<input type="text" value="" name="appDataKey">';

  var newCell1 = newRow.insertCell(1);
  newCell1.innerHTML = "<input type='text' name='appDataValue'> &nbsp;&nbsp;<input type='button' value='Delete' onclick='deleteCurrentRow(this);'/>";
  document.dotnet.rowIndex.value=rowIndex;
}

function deleteCurrentRow(obj)
{
      var delRow = obj.parentNode.parentNode;
      var tbl = delRow.parentNode.parentNode;
      var rIndex = delRow.sectionRowIndex;
      var rowArray = new Array(delRow);
      deleteRows(rowArray);
      document.dotnet.rowIndex.value = document.dotnet.rowIndex.value-1;
}

function deleteRows(rowObjArray)
{
      for (var i=0; i<rowObjArray.length; i++) {
         var rIndex = rowObjArray[i].sectionRowIndex;
         rowObjArray[i].parentNode.deleteRow(rIndex);
      }
}

</script>
<BODY>
<HR align="left" width="54%">

<form action="/genericcpsclient/prepayment" method="post" name="dotnet">
<input type="hidden" name="clientcode" value="4"/>
<input type="hidden" name="rowIndex" value="1"/>

<TABLE id="bookingcomponent" name="bookingcomponent" cellpadding="2" cellspacing="2">
  <TBODY>
  <TR>
     <TD>Client Applications:</TD>
     <TD>
        <select  name="clientapplication" id="clientapplication">
          <option value="TFly" selected/>TFlyRedesign</option>
           <option value="Portland"/>Portland</option>
           <option value="TFly" />TFly</option>
           <option value="FCFO"/>FCFO</option>
        </select>
     </TD>
  <TR>
  <TR>
     <TD>Currency:</TD>
     <TD><input type="text" name="currency" value="GBP"/></TD>
     <TD>Allowed Amount:</TD>
     <TD><input type="text" name="bookingcomponent_amount" value="297.84"/></TD>
  </TR>
  </TBODY>
</TABLE>

<HR align="left" width="54%">
<TABLE id='passengerDetailsTable' border="0">
   <TR>
      <TD><font color="red"><strong>Application Data</strong></font></TD>
   </TR>
   <TR>
      <TD><font color="blue">Key</font></TD>
      <TD><font color="blue">Value</font></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_dsc_1"></TD>
      <TD><input type="text" name="appDataValue" value="Fuel Surcharge and Airport Taxes"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_dsc_0"></TD>
      <TD><input type="text" name="appDataValue" value="Air Passenger Duty - Government"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_dsc_2"></TD>
      <TD><input type="text" name="appDataValue" value="Total Tax &amp; Fees"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_0"></TD>
      <TD><input type="text" name="appDataValue" value="333.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_departureDate"></TD>
      <TD><input type="text" name="appDataValue" value="Sat 15 May 10"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_count"></TD>
      <TD><input type="text" name="appDataValue" value="3"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_saving_amount"></TD>
      <TD><input type="text" name="appDataValue" value="- �20.00"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_departureDate"></TD>
      <TD><input type="text" name="appDataValue" value="Sat 08 May 10"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_1"></TD>
      <TD><input type="text" name="appDataValue" value="56.96"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_qty_1"></TD>
      <TD><input type="text" name="appDataValue" value="1"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="failure_payment_url"></TD>
      <TD><input type="text" name="appDataValue" value="http://thomsonwbe/thomson/en-GB/booking/sorry"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_1"></TD>
      <TD><input type="text" name="appDataValue" value="Taxes &amp; charges"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tAndCUrl"></TD>
      <TD><input type="text" name="appDataValue" value="http://thomsonwbe/en/popup_booking_conditions.html"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_operatingAirlineCode"></TD>
      <TD><input type="text" name="appDataValue" value="TOM 7605"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="tax_amt_0"></TD>
      <TD><input type="text" name="appDataValue" value="22.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_0"></TD>
      <TD><input type="text" name="appDataValue" value="adults  at  &amp;#163;166.50"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_saving_desc"></TD>
      <TD><input type="text" name="appDataValue" value="Web saving"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_arrivalTime"></TD>
      <TD><input type="text" name="appDataValue" value="13:25"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tripDuration"></TD>
      <TD><input type="text" name="appDataValue" value="7 nights"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_amt_0"></TD>
      <TD><input type="text" name="appDataValue" value="24.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_count"></TD>
      <TD><input type="text" name="appDataValue" value="1"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="accommodationSelected"></TD>
      <TD><input type="text" name="appDataValue" value="false"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="longhaul_premium_service"></TD>
      <TD><input type="text" name="appDataValue" value="false"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="tax_amt_2"></TD>
      <TD><input type="text" name="appDataValue" value="56.96"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_caption_0"></TD>
      <TD><input type="text" name="appDataValue" value="Round Trip"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_caption_1"></TD>
      <TD><input type="text" name="appDataValue" value="Extras"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="legal_info_url"></TD>
      <TD><input type="text" name="appDataValue" value="http://thomsonwbe/en/popup_legal_info_atol.html"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_count"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_dsc_0"></TD>
      <TD><input type="text" name="appDataValue" value="Adult flight meals  at  &amp;#163;12.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="prevPage"></TD>
      <TD><input type="text" name="appDataValue" value="BookingConfirmAction"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="intellitrackerSnippet"></TD>
      <TD><input type="text" name="appDataValue" value="FOTFPAGNAME%3dCPCPayment%26LANG%3den%26FID%3dthomson%26FOTFTFPax%3d2%26FOTFADULTS%3d2%26FOTFCHILDREN%3d0%26FOTFINFANTS%3d0%26FOTFDEPARTURE%3dBHX%26FOTFDEST%3dALC%26FOTFDEPRES%3d%26FOTFDRETRES%3d%26FOTFDEPDAY%3d08%26FOTFDEPMONTH%3d05%26FOTFDEPYEAR%3d2010%26FOTFRETFL%3d1%26FOTFRETDAY%3d15%26FOTFRETMONTH%3d05%26FOTFRETYEAR%3d2010%26FOTFPREVPAG%3dBookingConfirmAction%26FOTFCURRENCY%3dGBP%26FOTFCROSS%3dtrue%26FOTFEXACTOFF%3dfalse%26FOTFSEARCHDIS%3dSelectFlightMatrix%26FOTFCROSSTYPE%3dDate%26FOTFDUR%3d7%26FOTFINSSEL%3dfalse%26FOTFMEALSEL%3dtrue%26FOTFPBSSEL%3dfalse%26FOTFCOACHTRANS%3d7%26FOTF"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_departureTime"></TD>
      <TD><input type="text" name="appDataValue" value="11:40"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_qty_0"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>


    <TR>
      <TD><input type="text" name="appDataKey" value="pricing_caption_count"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_departureAirportName"></TD>
      <TD><input type="text" name="appDataValue" value="Birmingham"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_link_5"></TD>
      <TD><input type="text" name="appDataValue" value="TOM 1234"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="pre_payment_url"></TD>
      <TD><input type="text" name="appDataValue" value="http://thomsonwbe/thomson/en-GB/booking/confirm"></TD>
   </TR>
	 <TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_departureTime"></TD>
      <TD><input type="text" name="appDataValue" value="07:05"></TD>
   </TR>

	  <TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_operatingAirlineCode"></TD>
      <TD><input type="text" name="appDataValue" value="TOM 7604"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="tax_amt_1"></TD>
      <TD><input type="text" name="appDataValue" value="34.96"></TD>
   </TR>

	   <TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_arrivalTime"></TD>
      <TD><input type="text" name="appDataValue" value="10:40"></TD>
   </TR>
	<TR>
      <TD><input type="text" name="appDataKey" value="client_domain_url"></TD>
      <TD><input type="text" name="appDataValue" value="http://thomsonwbe"></TD>
   </TR>
<TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_arrivalAirportName"></TD>
      <TD><input type="text" name="appDataValue" value="Alicante"></TD>
   </TR>
<TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_qty_0"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
<TR>
      <TD><input type="text" name="appDataKey" value="post_payment_url"></TD>
      <TD><input type="text" name="appDataValue" value="http://thomsonwbe/thomson/en-GB/booking/wait"></TD>
   </TR>

		<TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_count"></TD>
      <TD><input type="text" name="appDataValue" value="6"></TD>
   </TR>
<TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_item_0"></TD>
      <TD><input type="text" name="appDataValue" value="Flight extras"></TD>
   </TR>
<TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_item_1"></TD>
      <TD><input type="text" name="appDataValue" value="search"></TD>
   </TR>
	  <TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_item_2"></TD>
      <TD><input type="text" name="appDataValue" value="travel"></TD>
   </TR>
		   <TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_item_3"></TD>
      <TD><input type="text" name="appDataValue" value="passenger"></TD>
   </TR>
	    <TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_item_4"></TD>
      <TD><input type="text" name="appDataValue" value="payment"></TD>
   </TR>
	  <TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_item_5"></TD>
      <TD><input type="text" name="appDataValue" value="confirmation"></TD>
   </TR>


	   <TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_link_0"></TD>
      <TD><input type="text" name="appDataValue" value="http://Flightextras"></TD>
   </TR>

	   <TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_link_1"></TD>
      <TD><input type="text" name="appDataValue" value="http://search"></TD>
   </TR>



	  <TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_link_2"></TD>
      <TD><input type="text" name="appDataValue" value="http://travel"></TD>
   </TR>
  
	  <TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_link_3"></TD>
      <TD><input type="text" name="appDataValue" value="http://passenger"></TD>
   </TR>
 
	  <TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_link_4"></TD>
      <TD><input type="text" name="appDataValue" value="http://payment"></TD>
   </TR>

	  <TR>
      <TD><input type="text" name="appDataKey" value="breadcrumb_link_5"></TD>
      <TD><input type="text" name="appDataValue" value="http://cofirmation"></TD>
   </TR>
  <TR>
      <TD><input type="text" name="appDataKey" value="street_address1"></TD>
      <TD><input type="text" name="appDataValue" value="a1"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="street_address2"></TD>
      <TD><input type="text" name="appDataValue" value="a2"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="street_address3"></TD>
      <TD><input type="text" name="appDataValue" value="a3"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="cardBillingPostcode"></TD>
      <TD><input type="text" name="appDataValue" value="cv14aq"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="country_count"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="country_code_0"></TD>
      <TD><input type="text" name="appDataValue" value="GB"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="country_dsc_0"></TD>
      <TD><input type="text" name="appDataValue" value="United Kingdom"></TD>
   </TR>
<TR>
      <TD><input type="text" name="appDataKey" value="country_code_1"></TD>
      <TD><input type="text" name="appDataValue" value="SP"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="country_dsc_1"></TD>
      <TD><input type="text" name="appDataValue" value="France"></TD>
   </TR>

	   <TR>
      <TD><input type="text" name="appDataKey" value="selectedCountryCode"></TD>
      <TD><input type="text" name="appDataValue" value="SP"></TD>
   </TR>


</TABLE>
<input type="button" value="Add Row" onClick="javascript:addRow(this)"/>
<input type="submit" value="Submit" />

<HR align="left" width="54%">

<td><font color="red"><strong>Card Types</strong></font><br/>
   <SELECT MULTIPLE name="cardtype" SIZE="11">
   <OPTION VALUE="American Express">American Express
   <OPTION VALUE="ATM ">ATM.
   <OPTION VALUE="Debit Mastercard">Debit Mastercard
   <OPTION VALUE="Diners Club">Diners Club
   <OPTION VALUE="Discover ">Discover
   <OPTION VALUE="EnRoute">EnRoute
   <OPTION VALUE="GE Capital">GE Capital
   <OPTION VALUE="JCB">JCB
   <OPTION VALUE="Laser">Laser
   <OPTION VALUE="Maestro">Maestro
   <OPTION VALUE="Mastercard" selected>Mastercard
   <OPTION VALUE="Platima">Platima
   <OPTION VALUE="Solo">Solo
   <OPTION VALUE="Switch">Switch
   <OPTION VALUE="VISA">VISA
   <OPTION VALUE="VISA Delta">VISA Delta
   <OPTION VALUE="VISA Electron">VISA Electron
   <OPTION VALUE="VISA Purchasing">VISA Purchasing
   </SELECT>
</td>
<HR align="left" width="54%">

</form>
</BODY>
<script language="javascript">
var selectmenu=document.getElementById("clientapplication");
selectmenu.onchange=function(){
	if(this.options[this.selectedIndex].value=="TFly")
	{
	document.location.href ="tflyredesign.jsp";
	}
	else
	{
	      document.location.href ="dynamicDotNetPrePayment.jsp";
	}
}
</script>
</HTML>
