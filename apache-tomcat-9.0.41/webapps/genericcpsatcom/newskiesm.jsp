<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
   <TITLE> Newskiesm Payment Page </TITLE>
      <style>
   #container{
   width:400px;
   _width:300px;
   clear:both;
   }
   #passcont{
   width:560px;
   }
   #passcont .titlecol{
   width:60px;
   float:left;
   padding:2px;
   }
   #passcont .col{
   width:150px;
   float:left;
   padding:2px;
   }
   #container .item{
   float:left;
   padding:2px;
   display:block;
   }
   #container .col{
   float:left;
   padding:2px;
   }
   </style>

</HEAD>
<script language="javascript">

function addRow(tblId)
{
  var tableBody = document.getElementById("passengerDetailsTable").tBodies[0];
  var newRow = tableBody.insertRow(-1);
  var rowIndex = newRow.sectionRowIndex;

  var newCell0 = newRow.insertCell(0);
  newCell0.innerHTML = '<input type="text" value="" name="appDataKey">';

  var newCell1 = newRow.insertCell(1);
  newCell1.innerHTML = "<input type='text' name='appDataValue'> &nbsp;&nbsp;<input type='button' value='Delete' onclick='deleteCurrentRow(this);'/>";
  document.dotnet.rowIndex.value=rowIndex;
}

function deleteCurrentRow(obj)
{
      var delRow = obj.parentNode.parentNode;
      var tbl = delRow.parentNode.parentNode;
      var rIndex = delRow.sectionRowIndex;
      var rowArray = new Array(delRow);
      deleteRows(rowArray);
      document.dotnet.rowIndex.value = document.dotnet.rowIndex.value-1;
}

function deleteRows(rowObjArray)
{
      for (var i=0; i<rowObjArray.length; i++) {
         var rIndex = rowObjArray[i].sectionRowIndex;
         rowObjArray[i].parentNode.deleteRow(rIndex);
      }
}

</script>
<BODY>
<HR align="left" width="54%">

<form action="/genericcpsclient/prepayment" method="post" name="dotnet">
<input type="hidden" name="clientcode" value="4"/>
<input type="hidden" name="rowIndex" value="1"/>

<TABLE id="bookingcomponent" name="bookingcomponent" cellpadding="2" cellspacing="2">
  <TBODY>
  <TR>
     <TD>Client Application:</TD>
     <TD>
        <select  name="clientapplication" id="clientapplication">
			<option value="select" />select</option>
		<option value="TFly" />TFlyRedesign</option>
           <option value="Portland"/>Portland</option>
           <option value="TFly" />TFly</option>
           <option value="FCFO"/>FCFO</option>
		   <option value="Tracs"/>TRACS</option>
		   <option value="NEWSKIESM"selected="true"/>Newskiesm</option>
        </select>
     </TD>
  <TR>
  <TR>
     <TD>Currency:</TD>
     <TD><input type="text" name="currency" value="GBP"/></TD>
     <TD>Allowed Amount:(Balance Due)</TD>
     <TD><input type="text" name="bookingcomponent_amount" value="297.84"/></TD>

  </TR>
  </TBODY>
</TABLE>

<HR align="left" width="54%">
<TABLE id='passengerDetailsTable' border="0">
   <TR>
      <TD><font color="red"><strong>Application Data</strong></font></TD>
   </TR>
   <TR>
      <TD><font color="blue">Key</font></TD>
      <TD><font color="blue">Value</font></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="passenger_ADT_count"></TD>
      <TD><input type="text" name="appDataValue" value="1"></TD>
   </TR>

   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_caption_count"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="pricing_caption_0"></TD>
      <TD><input type="text" name="appDataValue" value="FLIGHT OUT"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_caption_1"></TD>
      <TD><input type="text" name="appDataValue" value="FLIGHT HOME"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_count"></TD>
      <TD><input type="text" name="appDataValue" value="7"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_count"></TD>
      <TD><input type="text" name="appDataValue" value="7"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_0"></TD>
      <TD><input type="text" name="appDataValue" value="66.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_1"></TD>
      <TD><input type="text" name="appDataValue" value="15.98"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_2"></TD>
      <TD><input type="text" name="appDataValue" value="15.98"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_3"></TD>
      <TD><input type="text" name="appDataValue" value="15.98"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_4"></TD>
      <TD><input type="text" name="appDataValue" value="67.96"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_5"></TD>
      <TD><input type="text" name="appDataValue" value="67.96"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_amt_6"></TD>
      <TD><input type="text" name="appDataValue" value="67.96"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_amt_0"></TD>
      <TD><input type="text" name="appDataValue" value="66.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_amt_1"></TD>
      <TD><input type="text" name="appDataValue" value="66.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_amt_2"></TD>
      <TD><input type="text" name="appDataValue" value="66.00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_amt_3"></TD>
      <TD><input type="text" name="appDataValue" value="15.98"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_amt_4"></TD>
      <TD><input type="text" name="appDataValue" value="67.96"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_amt_5"></TD>
      <TD><input type="text" name="appDataValue" value="67.96"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_amt_6"></TD>
      <TD><input type="text" name="appDataValue" value="67.96"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_0"></TD>
      <TD><input type="text" name="appDataValue" value="Single (Adult) "></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_1"></TD>
      <TD><input type="text" name="appDataValue" value="Children "></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_2"></TD>
      <TD><input type="text" name="appDataValue" value="Infant"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_3"></TD>
      <TD><input type="text" name="appDataValue" value="Meals"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_4"></TD>
      <TD><input type="text" name="appDataValue" value="Bags"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_5"></TD>
      <TD><input type="text" name="appDataValue" value="Airport Lounge"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_dsc_6"></TD>
      <TD><input type="text" name="appDataValue" value="Champainge"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_dsc_0"></TD>
      <TD><input type="text" name="appDataValue" value="Single (Adult)"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_dsc_1"></TD>
      <TD><input type="text" name="appDataValue" value="Children"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_dsc_2"></TD>
      <TD><input type="text" name="appDataValue" value="Infant"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_dsc_3"></TD>
      <TD><input type="text" name="appDataValue" value="Meals"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_dsc_4"></TD>
      <TD><input type="text" name="appDataValue" value="Bags"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_dsc_5"></TD>
      <TD><input type="text" name="appDataValue" value="Airport Lounge"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_dsc_6"></TD>
      <TD><input type="text" name="appDataValue" value="Champainge"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_qty_0"></TD>
      <TD><input type="text" name="appDataValue" value="3"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_qty_1"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_qty_2"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_qty_3"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_qty_4"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_qty_5"></TD>
      <TD><input type="text" name="appDataValue" value="1"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_0_qty_6"></TD>
      <TD><input type="text" name="appDataValue" value="1"></TD>
   </TR>
      <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_qty_0"></TD>
      <TD><input type="text" name="appDataValue" value="3"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_qty_1"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_qty_2"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
     <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_qty_3"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
     <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_qty_4"></TD>
      <TD><input type="text" name="appDataValue" value="2"></TD>
   </TR>
     <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_qty_5"></TD>
      <TD><input type="text" name="appDataValue" value="1"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pricing_1_qty_6"></TD>
      <TD><input type="text" name="appDataValue" value="1"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="Is3dEnabled"></TD>
      <TD><input type="text" name="appDataValue" value="false"></TD>
   </TR>

<TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_departureDate"></TD>
      <TD><input type="text" name="appDataValue" value="THU 07 MAR 13"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_departureTime"></TD>
      <TD><input type="text" name="appDataValue" value="09:40 AM"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_arrivalTime"></TD>
      <TD><input type="text" name="appDataValue" value="14:00 PM"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_operatingAirlineCode"></TD>
      <TD><input type="text" name="appDataValue" value="TOM 7604"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_departureDate"></TD>
      <TD><input type="text" name="appDataValue" value="THU 22 MAR 13"></TD>
	</TR>
	<TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_departureTime"></TD>
      <TD><input type="text" name="appDataValue" value="10:40 AM"></TD>
	 </TR>
	 <TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_arrivalTime"></TD>
      <TD><input type="text" name="appDataValue" value="15:00 PM"></TD>
	 </TR>
	 <TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_operatingAirlineCode"></TD>
      <TD><input type="text" name="appDataValue" value="TOM 7605"></TD>
	 </TR>
	 <TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_departureAirportName"></TD>
      <TD><input type="text" name="appDataValue" value="Birmingham"></TD>
	 </TR>
	 <TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_arrivalAirportName"></TD>
      <TD><input type="text" name="appDataValue" value="Alicante"></TD>
	 </TR>
	 <TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_departureAirportName"></TD>
      <TD><input type="text" name="appDataValue" value="Alicante"></TD>
	 </TR>
	 <TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_arrivalAirportName"></TD>
      <TD><input type="text" name="appDataValue" value="Birmingham"></TD>
	 </TR>

	 <TR>
      <TD><input type="text" name="appDataKey" value="intellitrackerSnippet"></TD>
      <TD><input type="text" name="appDataValue" value="FOTFPAGNAME%3dCPCPayment%26LANG%3den%26FID%3dthomson%26FOTFTFPax%3d2%26FOTFADULTS%3d2%26FOTFCHILDREN%3d0%26FOTFINFANTS%3d0%26FOTFDEPARTURE%3dBHX%26FOTFDEST%3dALC%26FOTFDEPRES%3d%26FOTFDRETRES%3d%26FOTFDEPDAY%3d08%26FOTFDEPMONTH%3d05%26FOTFDEPYEAR%3d2010%26FOTFRETFL%3d1%26FOTFRETDAY%3d15%26FOTFRETMONTH%3d05%26FOTFRETYEAR%3d2010%26FOTFPREVPAG%3dBookingConfirmAction%26FOTFCURRENCY%3dGBP%26FOTFCROSS%3dtrue%26FOTFEXACTOFF%3dfalse%26FOTFSEARCHDIS%3dSelectFlightMatrix%26FOTFCROSSTYPE%3dDate%26FOTFDUR%3d7%26FOTFINSSEL%3dfalse%26FOTFMEALSEL%3dtrue%26FOTFPBSSEL%3dfalse%26FOTFCOACHTRANS%3d7%26FOTF"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="passenger_total_count"></TD>
      <TD><input type="text" name="appDataValue" value="1"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="client_domain_url"></TD>
      <TD><input type="text" name="appDataValue" value="http://flightsextras.thomson.co.uk"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="pre_payment_url"></TD>
      <TD><input type="text" name="appDataValue" value="http://lflightsextras/prepayment.page"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="post_payment_url"></TD>
      <TD><input type="text" name="appDataValue" value="http://flightsextras/confirmation.page"></TD>
   </TR>
    <TR>
      <TD><input type="text" name="appDataKey" value="failure_payment_url"></TD>
      <TD><input type="text" name="appDataValue" value="http://flightsextras/error.page"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="legal_info_url"></TD>
      <TD><input type="text" name="appDataValue" value="http://flights.thomson.co.uk/en/popup_legal_info_atol.html"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="passenger_1_travelWith"></TD>
      <TD><input type="text" name="appDataValue" value="ZXhvZHJX"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="passenger_2_travelWith"></TD>
      <TD><input type="text" name="appDataValue" value="sgdasdggsd"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="amount_paid"></TD>
      <TD><input type="text" name="appDataValue" value="100"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="transaction_total"></TD>
      <TD><input type="text" name="appDataValue" value="397.84"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="bookingSessionId"></TD>
      <TD><input type="text" name="appDataValue" value="3A288E65338D1F607EA6706AEE56FE00"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="flight_ticketType"></TD>
      <TD><input type="text" name="appDataValue" value="Round Trip"></TD>
   </TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_departureAirportCode"></TD>
      <TD><input type="text" name="appDataValue" value="ATL"></TD>
	</TR>
   <TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_departureAirportCode"></TD>
      <TD><input type="text" name="appDataValue" value="BRM"></TD>
	</TR>
	<TR>
      <TD><input type="text" name="appDataKey" value="inBoundFlight_arrivalAirportCode"></TD>
      <TD><input type="text" name="appDataValue" value="BRM"></TD>
	</TR>
	<TR>
      <TD><input type="text" name="appDataKey" value="outBoundFlight_arrivalAirportCode"></TD>
      <TD><input type="text" name="appDataValue" value="ATL"></TD>
	</TR>
	<TR>
      <TD><input type="text" name="appDataKey" value="is_thirdparty_flight"></TD>
      <TD><input type="text" name="appDataValue" value="false"></TD>
	</TR>
	
</TABLE>
<input type="button" value="Add Row" onClick="javascript:addRow(this)"/>
<input type="submit" value="Submit" />

<HR align="left" width="54%">

<td><font color="red"><strong>Card Types</strong></font><br/>
   <SELECT MULTIPLE name="cardtype" SIZE="11">
   <OPTION VALUE="American Express">American Express
   <OPTION VALUE="ATM ">ATM.
   <OPTION VALUE="Debit Mastercard">Debit Mastercard
   <OPTION VALUE="Diners Club">Diners Club
   <OPTION VALUE="Discover ">Discover
   <OPTION VALUE="EnRoute">EnRoute
   <OPTION VALUE="GE Capital">GE Capital
   <OPTION VALUE="JCB">JCB
   <OPTION VALUE="Laser">Laser
   <OPTION VALUE="Maestro" selected>Maestro
   <OPTION VALUE="Mastercard">Mastercard
   <OPTION VALUE="Platima">Platima
   <OPTION VALUE="Solo">Solo
   <OPTION VALUE="Switch">Switch
   <OPTION VALUE="VISA">VISA
   <OPTION VALUE="VISA Delta">VISA Delta
   <OPTION VALUE="VISA Electron">VISA Electron
   <OPTION VALUE="VISA Purchasing">VISA Purchasing
   </SELECT>
</td>
<HR align="left" width="54%">

</form>
</BODY>
<script language="javascript">
var selectmenu=document.getElementById("clientapplication");

selectmenu.onchange=function(){

	var selectedClient=this.options[this.selectedIndex].value;
	if(this.options[this.selectedIndex].value=="TFly")
	{
	document.location.href ="tflyredesign.jsp";
	}
	if(selectedClient=="NEWSKIESM")
	{
	   document.location.href="newskiesm.jsp";
	}

	else
	{
	      document.location.href ="dynamicDotNetPrePayment.jsp";
	}
}
</script>
</HTML>
