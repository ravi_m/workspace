<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<TITLE> New Document </TITLE>
   	<style>
	#container{
	width:400px;
	_width:300px;
	clear:both;
	}
	#passcont{
	width:560px;
	}
	#passcont .titlecol{
	width:60px;
	float:left;
	padding:2px;
	}
	#passcont .col{
	width:150px;
	float:left;
	padding:2px;
	}
	#container .item{
	float:left;
	padding:2px;
	display:block;
	}
	#container .col{
	float:left;
	padding:2px;
	}
	</style>

</HEAD>
<BODY>
<hr/>

<form action="/genericcpsclient/prepayment" method="post">
<input type="hidden" name="clientcode" value="3"/>
<TABLE id="bookingcomponent" name="bookingcomponent" cellpadding="2" cellspacing="2">
  <TBODY>
<TR>
<TD>ClientApplication: <select  name="clientapplication"><option value="GENERICJAVA"/>GENERIC</option>
<option value="ThomsonAO"/>ThomsonAccommodationOnly</option>
<option value="BRACApplication"/>BRAC</option>
<option value="ThomsonBYO"/>ThomsonBuildYourOwn</option>
<option value="WiSHAO"/>WISHAccommodationOnly</option>
<option value="WiSHBYO"/>WISHBuildYorOwn</option>
<option value="Cruise"/>WISHCRUISE</option>
<option value="OLBP"/>WISHOLBP</option>
<option value="KRONOS"/>KRONOS</option>
<option value="KRONOSCALLCENTER"/>KRONOSCALLCENTER</option>
<option value="Tracs"/>TRACS</option>
</select></TD>
<TD>Amount:<input type="text" name="bookingcomponent_amount" value="300.0"/></TD>
<TD>callType:<input type="text" name="callType" value="DEFAULT"/></TD>
</TR>
<TR>
<TD>searchType:<input type="text" name="searchType" value="BYO"/></TD>
<TD>NoOfTransactions:<input type="text" name="noOfTransactions" value="8"/></TD>
<TD>MaxCardCharge:<input type="text" name="maxCardCharge" value="60"/></TD>
</TR>
<TR>
<TD>setMaxCardChargePercent:<input type="text" name="maxCardChargePercent" value="2.5"/></TD>
<TD>setPackageType:<input type="text" name="packageType" value="PDP"/></TD>
<TD>setTracsBookMinusId:<input type="text" name="TracsBookMinusId" value="291AE408C3266C0B0034BCA7"/></TD>
</TR>
<TR>
<TD>setPayableAmount:<input type="text" name="amount" value="300.0"/></TD>
<TD>setErrorMessage:<input type="text" name="ErrorMessage" value="This Message Gets Printed in Case of any error in the payment page"/></TD>
</TR>
</TBODY>
</TABLE>
<hr/>

<font color="red"><b>AbtaDetails:</b></font>
<TABLE id="AbtaDetails" name="AbtaDetails" cellpadding="2" cellspacing="2">
  <TBODY>
<TR>
<TD>abtaNumber:<input type="text" name="abtaNumber" value="K2310"/></TD>
<TD>abtaUserId:<input type="text" name="abtaUserId" value="JXB"/></TD>
<TD>tracsWebSiteId:<input type="text" name="tracsWebSiteId" value="MERLIN"/></TD>
</TR>
</TBODY>
</TABLE>
<hr/>
<font color="red"><b>ShopDetails:</b></font>
<TABLE id="ShopDetails" name="ShopDetails" cellpadding="2" cellspacing="2">
  <TBODY>
<TR>
<TD>shopName:<input type="text" name="shopName" value="Tracey McDonnell"/></TD>
<TD>shopId:<input type="text" name="shopId" value="0015"/></TD>
</TR>
</TBODY>
</TABLE>
<hr/>


<font color="red"><b>AccommodationSummary:</b></font>
<TABLE id="AccommodationSummary" name="AccommodationSummary" cellpadding="2" cellspacing="2">
  <TBODY>
<TR>
<TD>country:<input type="text" name="accommodationsummary_country" value="Spain"/></TD>
<TD>destination:<input type="text" name="accommodationsummary_destination" value="Lanzorete"/></TD>
<TD>resort:<input type="text" name="accommodationsummary_resort" value="puertoDelCarmen"/></TD>
</TR>
<TR>
<TD>hotel:<input type="text" name="accommodationsummary_hotel" value="Kamesvillas"/></TD>
<TD>boardBasis:<input type="text" name="accommodationsummary_boardBasis" value="Selfcatering"/></TD>
<TD>rating:<input type="text" name="accommodationsummary_rating" value="Trating"/></TD>
</TR>
<TR>
<TD>RoomDescription:<input type="text" name="accommodationsummary_RoomDescription" value="3 bedRoom apt- PrivatePool"/></TD>
<TD>duration:<input type="text" name="accommodationsummary_duration" value="7"/></TD>
<TD>accommodationSelected:<select name="accommodationsummary_accommodationSelected"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
</TR>
<TR>
<TD>accommodationInventorySystem:<input type="text" name="accommodationsummary_accommodationInventorySystem" value="tracs"/></TD>
<TD>startDate:<input type="text" name="accommodationsummary_startDate" value="7/10/2008"/></TD>
<TD>endDate:<input type="text" name="accommodationsummary_endDate" value="14/10/2008"/></TD>
<TD>isPrepay:<select name="accommodationsummary_isPrepay" /><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
</TR>
</TBODY>
</TABLE>
<hr/>



<font color="red"><b>CruiseSummary:</b></font>
<TABLE id="CruiseSummary" name="CruiseSummary" cellpadding="2" cellspacing="2">
  <TBODY>

<TABLE id="CruiseSummaryAccommodationship" name="CruiseSummaryAccommodationship" cellpadding="2" cellspacing="2">
  <TBODY>
 <TR>Accommodation ship Details:</TR>
<TR>
<TD>country:<input type="text" name="cruisesummary_ship_country" value=""/></TD>
<TD>destination:<input type="text" name="cruisesummary_ship_destination" value=""/></TD>
<TD>resort:<input type="text" name="cruisesummary_ship_resort" value="Colourful Coasts"/></TD>
</TR>
<TR>
<TD>hotel:<input type="text" name="cruisesummary_ship_hotel" value="Thomson Celebration"/></TD>
<TD>boardBasis:<input type="text" name="cruisesummary_ship_boardBasis" value="Full Board"/></TD>
</TR>
</TBODY>
</TABLE>

<TABLE id="CruiseSummaryStayAcommodation" name="CruiseSummaryStayAcommodation" cellpadding="2" cellspacing="2">
  <TBODY>
<TR>StayAcommodation Details:</TR>
<TD>country:<input type="text" name="cruisesummary_stayacommodation_country" value="Spain"/></TD>
<TD>destination:<input type="text" name="cruisesummary_stayacommodation_destination" value="Lanzorete"/></TD>
<TD>resort:<input type="text" name="cruisesummary_stayacommodation_resort" value="puertoDelCarmen"/></TD>
</TR>
<TR>
<TD>hotel:<input type="text" name="cruisesummary_stayacommodation_hotel" value="Kamesvillas"/></TD>
<TD>boardBasis:<input type="text" name="cruisesummary_stayacommodation_boardBasis" value="Selfcatering"/></TD>
<TD>rating:<input type="text" name="cruisesummary_stayacommodation_rating" value="Trating"/></TD>
</TR>
<TR>
<TD>RoomDescription:<input type="text" name="cruisesummary_stayacommodation_RoomDescription" value="3 bedRoom apt- PrivatePool"/></TD>
<TD>duration:<input type="text" name="cruisesummary_duration" value="7"/></TD>
<TD>accommodationSelected:<select name="cruisesummary_accommodationSelected"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
</TR>
<TR>
<TD>accommodationInventorySystem:<input type="text" name="cruisesummary_accommodationInventorySystem" value="tracs"/></TD>
<TD>startDate:<input type="text" name="cruisesummary_startDate" value="7/10/2008"/></TD>
<TD>endDate:<input type="text" name="cruisesummary_endDate" value="14/10/2008"/></TD>
<TD>Prepay:<select name="cruisesummary_isPrepay" /><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>

</TR>

</TBODY>
</TABLE>

</TBODY>
</TABLE>
<hr/>

<font color="red"><b>Booking Details :</b></font>
<TABLE id="bookingdetails" name="bookingdetails" cellpadding="2" cellspacing="2">
  <TBODY>
<TR>
<TD>bookingReference:<input type="text" name="bookingdetails_bookingReference" value="0015/232"/></TD>
<TD>bookingStatus:<input type="text" name="bookingdetails_bookingStatus" value="Successful"/></TD>
<TD>bookingdate:<input type="text" name="bookingdetails_bookingdate" value="29/11/2008"/></TD>
</TR>
</TBODY>
</TABLE>
<hr/>

<font color="red"><b>PriceComponents :</b></font>
<TABLE id="priceComponents" name="priceComponents" cellpadding="2" cellspacing="2">
  <TBODY>
<TR>
<TD>itemDescription:<input type="text" name="pricecomponent_itemDescription_1" value="Hotel"/></TD>
<TD>amount:<input type="text" name="pricecomponent_amount_1" value="100.0"/></TD>
<TD>quantity:<input type="text" name="pricecomponent_quantity_1" value="0"/></TD>
<TD>discount:<select name="pricecomponent_discount_1"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
</TR>

<TR>
<TD>itemDescription:<input type="text" name="pricecomponent_itemDescription_2" value="Flight"/></TD>
<TD>amount:<input type="text" name="pricecomponent_amount_2" value="100.0"/></TD>
<TD>quantity:<input type="text" name="pricecomponent_quantity_2" value="0"/></TD>
<TD>discount:<select name="pricecomponent_discount_2"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
</TR>

<TR>
<TD>itemDescription:<input type="text" name="pricecomponent_itemDescription_3" value="Insurance"/></TD>
<TD>amount:<input type="text" name="pricecomponent_amount_3" value="100.0"/></TD>
<TD>quantity:<input type="text" name="pricecomponent_quantity_3" value="3"/></TD>
<TD>discount:<select name="pricecomponent_discount_3"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
</TR>

</TBODY>
</TABLE>
<hr/>

<font color="red"><b>DepositComponents :</b></font>
<TABLE id="depositComponents" name="depositComponents" cellpadding="2" cellspacing="2">
  <TBODY>
<TR>
<TD>amount:<input type="text" name="depositcomponents_amount" value="50.0"/></TD>
<TD>depositType:<input type="text" name="depositcomponents_depositType" value="DEPOSIT"/></TD>
<TD>depositDueDate:<input type="text" name="depositcomponents_depositDueDate" value="29/9/2008"/></TD>
</TR>
</TBODY>
</TABLE>
<hr/>

<font color="red"><b>PassengerRoomSummary :</b></font>
<TABLE id="passengersummary" name="passengersummary" cellpadding="2" cellspacing="2">
  <TBODY>
<TR>
<TD>AgeClassification:<select name="passengersummary_ageclassification_1"/><option value="INF">INFANT</option>
<option value="CHD">CHILD</option><option value="ADT">ADULT</option><option value="SNR">SENIOR</option>
<option value="SENIOR2">SNR2</option></select></TD>
<TD>leadPassenger::<select name="passengersummary_leadPassenger_1"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
<TD>identifier:<input type="text" name="passengersummary_identifier_1" value="1"/></TD>
</TR>

<TR>
<TD>AgeClassification:<select name="passengersummary_ageclassification_2"/><option value="INF">INFANT</option>
<option value="CHD">CHILD</option><option value="ADT">ADULT</option><option value="SNR">SENIOR</option>
<option value="SENIOR2">SNR2</option></select></TD>
<TD>leadPassenger::<select name="passengersummary_leadPassenger_2"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
<TD>identifier:<input type="text" name="passengersummary_identifier_2" value="2"/></TD>
</TR>

</TBODY>
</TABLE>
<hr/>

<font color="red"><b>Source of business:</b></font>
<TABLE id="sourceofbusiness" name="sourceofbusiness" cellpadding="2" cellspacing="2">
  <TBODY>
<TR>


<TD>sourceofbusiness_key_1:<input type="text" name="sourceofbusiness_key_1" value="HNEW_HNEW"/></TD>
<TD>sourceofbusiness_value_1:<input type="text" name="sourceofbusiness_value_1" value="News of the World"/></TD>
</TR>
<TR>
<TD>sourceofbusiness_key_2:<input type="text" name="sourceofbusiness_key_2" value="STMC_STMC"/></TD>
<TD>sourceofbusiness_value_2:<input type="text" name="sourceofbusiness_value_2" value="STMC Skydeals Times"/></TD>
<TD>selectedSobCode:<input type="text" name="sourceofbusiness_selectedSobCode" value="AGEN_AGEN"/></TD>
</TR>

</TBODY>
</TABLE>
<hr/>

<font color="red"><b>FlightSummary</b></font>
<TABLE id="flightsummary" name="flightsummary" cellpadding="2" cellspacing="2">
  <TBODY>

<TR>

<TD>flightSupplierSystem:<input type="text" name="flightsummary_flightSupplierSystem" value="TRACSA"/></TD>
<TD>flightSelected:<select name="flightsummary_flightSelected"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
</TR>

<TABLE id="outboundflight" name="outboundflight" cellpadding="2" cellspacing="2">
<TBODY>
<TR>
outBoundFlights:
</TR>
<TR>
<TD>departureAirportName:<input type="text" name="outboundflight_departureAirportName" value="LondonGatWick"/></TD>
<TD>departureDateTime:<input type="text" name="outboundflight_departureDateTime" value="1000"/></TD>
<TD>arrivalAirportName:<input type="text" name="outboundflight_arrivalAirportName" value="Alicante"/></TD>
</TR>
<TR>
<TD>arrivalDateTime:<input type="text" name="outboundflight_arrivalDateTime" value="5000"/></TD>
<TD>carrier:<input type="text" name="outboundflight_carrier" value="ThosmonFly"/></TD>
<TD>operatingAirlineCode:<input type="text" name="outboundflight_operatingAirlineCode" value="TOM"/></TD>
<TD>flightNumber:<input type="text" name="outboundflight_flightNumber" value="4614"/></TD>
</TR>
</TBODY>
</TABLE>

<TABLE id="inboundflight" name="inboundflight" cellpadding="2" cellspacing="2">
<TBODY>
<TR>
InBoundFlights:
</TR>
<TR>
<TD>departureAirportName:<input type="text" name="inboundflight_departureAirportName" value="alicante"/></TD>
<TD>departureDateTime:<input type="text" name="inboundflight_departureDateTime" value="5000"/></TD>
<TD>arrivalAirportName:<input type="text" name="inboundflight_arrivalAirportName" value="LondonGatwick"/></TD>
</TR>
<TR>
<TD>arrivalDateTime:<input type="text" name="inboundflight_arrivalDateTime" value="25000"/></TD>
<TD>carrier:<input type="text" name="inboundflight_carrier" value="ThosmonFly"/></TD>
<TD>operatingAirlineCode:<input type="text" name="inboundflight_operatingAirlineCode" value="TOM"/></TD>
<TD>flightNumber:<input type="text" name="inboundflight_flightNumber" value="4615"/></TD>
</TR>
</TBODY>
</TABLE>

</TBODY>
</TABLE>
<hr/>

<font color="red"><b>PassengerDetails</b></font>
<TABLE id="passengerdetails" name="passengerdetails" cellpadding="2" cellspacing="2">
<TBODY>
<TH>Intial</TH><TH>ForeName</TH><TH>Middle Initials</TH><TH>SurName</TH>
<TR>
  <TD><select name='personaldetails_0_title'><option value='Mr'>Mr</option><option value='Mrs'>Mrs</option><option value='Miss'>Miss</option><option value='Ms'>Ms</option><option value='Dr'>Dr</option><option value='Rev'>Rev</option><option value='Prof'>Prof</option></select></TD>
    <TD><INPUT name="personaldetails_0_foreName" value="Sachin"></TD>
    <TD><INPUT name="personaldetails_0_middleInitial" value="Tendulkar"></TD>
    <TD><INPUT name="personaldetails_0_surName" value="SR"></TD>
</TR>
<TR>
  <TD><select name='personaldetails_1_title'><option value='Mr'>Mr</option><option value='Mrs'>Mrs</option><option value='Miss'>Miss</option><option value='Ms'>Ms</option><option value='Dr'>Dr</option><option value='Rev'>Rev</option><option value='Prof'>Prof</option></select></TD>
    <TD><INPUT name="personaldetails_1_foreName" value="Rahul"></TD>
    <TD><INPUT name="personaldetails_1_middleInitial" value="Dravid"></TD>
    <TD><INPUT name="personaldetails_1_surName" value="RD"></TD>
</TR>
<TR>
  <TD><select name='personaldetails_2_title'><option value='Mr'>Mr</option><option value='Mrs'>Mrs</option><option value='Miss'>Miss</option><option value='Ms'>Ms</option><option value='Dr'>Dr</option><option value='Rev'>Rev</option><option value='Prof'>Prof</option></select></TD>
    <TD><INPUT name="personaldetails_2_foreName" value="Sourav"></TD>
    <TD><INPUT name="personaldetails_2_middleInitial" value="Ganguly"></TD>
    <TD><INPUT name="personaldetails_2_surName" value="SG"></TD>
</TR>
</TBODY>
</TABLE>
<hr/>

<font color="red"><b>DiscountType</b></font>
<TABLE id="discounttype" name="discounttype" cellpadding="2" cellspacing="2">
<TBODY>
<TR>
 <TD>discountType:<INPUT name="discounttype_discountType" value="DISCRETIONARY"></TD>
 <TD>maxDiscountAmount:<INPUT name="discounttype_maxDiscountAmount" value="10.0"></TD>
 <TD>applicableForHotel:<select name="discounttype_applicableForHotel"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
 <TD>applicableForFlight:<select name="discounttype_applicableForFlight"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
</TR>
<TR>
 <TD>discountType:<INPUT name="discounttype_discountType_1" value="PRICEBEAT"></TD>
 <TD>maxDiscountAmount:<INPUT name="discounttype_maxDiscountAmount_1" value="10.0"></TD>
 <TD>applicableForHotel:<select name="discounttype_applicableForHotel_1"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
 <TD>applicableForFlight:<select name="discounttype_applicableForFlight_1"/><option value="true">TRUE</option><option value="false">FALSE</option></select></TD>
</TR>
</TBODY>
</TABLE>
<hr/>


<font color="red"><b>Country</b></font>
<TABLE id="country" name="country" cellpadding="2" cellspacing="2">
<TBODY>
<TR>
 <TD>
<select MULTIPLE name="country" id="country" SIZE="4">
<option value="Am-America" selected>America</option>
<option value="In-India">India</option>
<option value="FR-France">France</option>
<option value="GB-UnitedKingdom">England</option>
</select>
</TD>
</TR>
</TBODY>
</TABLE>
<hr/>

<font color="red"><b>PaymentTypes</b></font>
<TABLE id="Paymenttypes" name="Paymenttypes" cellpadding="2" cellspacing="2">
<TBODY>


 <TH>CP</TH><TH>CNP</TH><TH>CPNA </TH>
 <TR>
 <TD>
<select MULTIPLE name="Paymenttypes_CP" id="Paymenttypes_CP" SIZE="9">
<option value="AMERICAN_EXPRESS|American Express CC|true|4|Card" selected>American Express</option>
<option value="BQ|Building Society Cheque|true|3|Cheque">Building Society Cheque</option>
<option value="CA|Cash|true|3|Cash">Cash</option>
<option value="DINERS_CC|Diners CC|true|3|Card">Diners CC</option>
<option value="PQ|Personal Cheque|true|3|Cheque">Personal Cheque</option>
<option value="SWITCH|Switch/Maestro|true|3|Card">Switch/Maestro</option>
<option value="VISA|Visa & Access|true|3|Card">Visa & Access</option>
<option value="MASTERCARD|Mastercard|true|3|Card">Mastercard</option>
<option value="DEBIT MASTERCARD|Mastercard Debit|false|125|true|3|false|false" selected>Mastercard Debit </option>
<option value="MASTERCARD|Mastercard Credit|true|125|true|3|false|false" selected>Mastercard Credit</option>
<option value="VISA_DEBIT|Visa - Debit|false|3|Card">Visa - Debit</option>
<option value="TU|TUI Group Voucher|true|3|Voucher">TUI Group Voucher</option>
<option value="CV|Commercial Voucher|true|3|Voucher">Commercial Voucher</option>
<option value="TV|Thomson Concession Voucher|true|3|Voucher">Thomson Concession Voucher</option>
<option value="LS|Long Service Award|true|3|Voucher">Long Service Award</option>
<option value="VISA_DELTA|Card using Pinpad|false|4|CardPinPad">Card using Pinpad</option>
</select>
</TD>
<TD>
<select MULTIPLE name="Paymenttypes_CNP" id="Paymenttypes_CNP" SIZE="7">
<option value="VISA|VISA-Credit|true|127|true|3|false|false">VISA-Credit</option>
<option value="VISA_DELTA|VISA/Delta-Debit|false| |true|3|false|false">VISA/Delta-Debit</option>
<option value="SWITCH|Maestro|false| |true|3|true|true">Maestro</option>
<option value="SOLO|Solo|false| |true|3|true|true">Solo</option>
<option value="Laser|Laser|true|125|true|3|false|false">Laser</option>
<option value="AMERICAN_EXPRESS|American Express|true|124|true|4|false|false|2.5">American Express</option>
<option value="VISA_ELECTRON|Visa Electron|false| |true|3|false|false|2.5">Visa Electron</option>
<option value="DEBIT MASTERCARD|Mastercard Debit|false|125|true|3|false|false" selected>Mastercard Debit </option>
<option value="MASTERCARD|Mastercard Credit|true|125|true|3|false|false" selected>Mastercard Credit</option>
</select>
</TD>
<TD>
<select MULTIPLE name="Paymenttypes_CPNA" id="Paymenttypes_CPNA" SIZE="11">
<option value="BQ|Building Society Cheque|true|3|Cheque">Building Society Cheque</option>
<option value="CA|Cash|true|3|Cash">Cash</option>
<option value="PQ|Personal Cheque|true|3|Cheque">Personal Cheque</option>
<option value="TU|TUI Group Voucher|true|3|Voucher" selected>TUI Group Voucher</option>
<option value="CV|Commercial Voucher|true|3|Voucher">Commercial Voucher</option>
<option value="TV|Thomson Concession Voucher|true|3|Voucher">Thomson Concession Voucher</option>
<option value="LS|Long Service Award|true|3|Voucher">Long Service Award</option>
<option value="MASTERCARD|Mastercard|true|3|DATACASH">Mastercard</option>
<option value="VISA|VISA-Credit|true|3|DATACASH">VISA-Credit</option>
<option value="VISA_DELTA|VISA/Delta-Debit|true|3|DATACASH">VISA/Delta-Debit</option>
<option value="SWITCH|Maestro|true|3|DATACASH">Maestro</option>
<option value="SOLO|Solo|true|3|DATACASH">Solo</option>
<option value="AMERICAN_EXPRESS|American Express|true|4|DATACASH">American Express</option>
<option value="VISA_ELECTRON|Visa Electron|true|3|DATACASH">Visa Electron</option>
<option value="DEBIT MASTERCARD|Mastercard Debit|false|125|true|3|false|false" selected>Mastercard Debit </option>
<option value="MASTERCARD|Mastercard Credit|true|125|true|3|false|false" selected>Mastercard Credit</option>
</select>
</TD>


</TR>
</TBODY>
</TABLE>
<hr/>


<input type="submit" value="submit"/>
</form>
</BODY>
</HTML>

