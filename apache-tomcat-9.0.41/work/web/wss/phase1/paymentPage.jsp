<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <%@ page contentType="text/html; charset=UTF-8" %>
  <title>Payment Details</title>
  <version-tag:version/>
  <c:choose>
   <c:when test='${param["b"]==19000}'>
       <c:set var="brand" value="thomson" />
   </c:when>
   <c:when test='${param["b"]==20000}'>
       <c:set var="brand" value="fcsun"/>
   </c:when>
   <c:when test='${param["b"]==21000}'>
       <c:set var="brand" value="fcfalcon"/>
   </c:when>
   <c:otherwise>
       <c:set var="brand" value="fcsun"/>
   </c:otherwise>
   </c:choose>
  <% String brand = (String)pageContext.getAttribute("brand"); %>

  <%--Provide brand specific reference for css and js files --%>
  <%@include file="resources.jspf"%>

  <%--Include white label css file --%>
  <link href="/cms-cps/wss/phase1/global/css/global.css" rel="stylesheet" type="text/css" />

  <%--Common white label js files --%>
  <script src="/cms-cps/common/js/prototype.js" type="text/javascript"></script>
  <script src="/cms-cps/common/js/utilFunctions.js" type="text/javascript"></script>
  <script src="/cms-cps/common/js/cardCharges.js" type="text/javascript"></script>
  <script src="/cms-cps/common/js/commonAjaxCalls.js" type="text/javascript"></script>

  <script src="/cms-cps/wss/phase1/global/js/browserdetect_lite.js" type="text/javascript"></script>
  <script src="/cms-cps/wss/phase1/global/js/prototype-tui-util.js" type="text/javascript"></script>
  <script src="/cms-cps/wss/phase1/global/js/prototype-tui-overlay.js" type="text/javascript"></script>

</head>
<body>

<div class="wrapper">

 <%-------------------------------Include brand specific header --------------------------------------%>
  <div id="header">
    <%@include file="header.jspf"%>
  </div><%-- End of Header --%>
 <%--------------------------*****End brand specific header*****--------------------------------------%>
 <%------------------------------White label jsp content -------------------------------------------%>

  <div id="contentContainer">
    <div class="mainContent">

      <%@include file="logout.jspf"%>

      <div class="mainContentTop">
        <span class="outer">&nbsp;</span>
        <span class="inner">&nbsp;</span>
      </div>

      <div class="mainContentMiddle">
        <form action="/cps/processPayment?token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tui_brand=<c:out value='${param["tui_brand"]}'/>&tomcat=<c:out value='${param.tomcat}' />" method="post" onsubmit="return payNowButtonClicked();">
          <%--Contains Essential configuration settings used thru out the page --%>
         <%@include file="configSettings.jspf"%>
         <%@include file="pageErrors.jspf"%>
         <%@include file="bookingSummary.jspf"%>
         <%@include file="paymentDetails.jsp"%>
         <%@include file="buttonGroup.jspf"%>
         <%@include file="cardSecurityCodeOverlay.jspf" %>
        </form>
      </div><%-- END mainContentMiddle --%>

     <div class="mainContentBottom">
        <span class="outer">&nbsp;</span>
        <span class="inner">&nbsp;</span>
      </div>
    </div><%-- END mainContent --%>
  </div><%-- END #contentContainer --%>

 <%-------------------------*****End White label jsp content*****-----------------------------------%>

</div><%-- END wrapper --%>



<%--------------------------------Include brand specific Footer --------------------------------------%>
  <%@include file="footer.jspf"%>
<%---------------------------*****End brand specific Footer*****--------------------------------------%>


<%--------------------------------Include functional scripts and onload functions --------------------%>
<script src="/cms-cps/wss/phase1/global/js/wssPayment.js" type="text/javascript"></script>
<script src="/cms-cps/wss/phase1/global/js/formValidation.js" type="text/javascript"></script>

<script type="text/javascript">
   TuiUtil.addLoadEvent(initialisePaymentEvents);
   //TuiUtil.addLoadEvent(function(){processingErrors('<c:out value="${bookingInfo.errorFields}"/>','<c:out value="${bookingComponent.errorMessage}"/>')});

  window.onunload = function(){ };
</script>
<%---------------------------*****End Scripts*****----------------------------------------------------%>
</body>
</html>
