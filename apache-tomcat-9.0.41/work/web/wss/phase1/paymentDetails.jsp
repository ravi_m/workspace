<%----------------------Declare variables used in different parts of this JSP---------------------------%>
  <c:set var="isFalconROI" value="${bookingComponent.companyCode == 'FALS'}"/>
  <c:set var="isFalconNI" value="${bookingComponent.companyCode == 'FALN'}"/>
  <c:set var="isFalcon" value="${isFalconNI ||isFalconROI }"/>
<%------------------*****END Declare variables used in different parts of this JSP*****-----------------%>

<div class="section">
  <h2 id="paymentDetails"><span>Payment details</span></h2>

  <div class="sectionContent paymentDetails">
    <c:if test="${!isFalconROI}">
    <p class="intro"><strong>There is no additional charge when paying by Visa Debit, Visa Electron, Maestro or Solo.</strong>
    <br />For <c:if test="${!isFalcon}">American Express,</c:if> Master Card and Visa Credit, the credit card charge amount shown will be added to the total amount debited from your card.</p>
    </c:if>
    <ul id="paymentOptionDetails" class="controlGroups">

     <%-----------------------Included Payment Options(Deposit options)----------------------------------%>
      <%@include file="paymentOptions.jspf" %>
     <%------------------*****END of Including Payment Options(Deposit options)*****---------------------%>

     <%-----------------------Amount to pay section------------------------------------------------------%>
      <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'amountToPay')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

      <li id="amountToPayControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="amountToPayErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="amountToPay">Amount to pay:</label></p>

        <div class="controls">
          <p>
            <span class="currency"><c:out value="${currency}" escapeXml="false"/></span>
            <input type="text" id="amountToPay" name="transactionAmount" value="<c:out value='${depositAmountForFirstDepositType}'/>" <c:out value="${attributeForAmountToPay}"/> class="depositAmount" autocomplete="off" />
          </p>
        </div>
      </li>
     <%------------------*****END Amount to pay section*****---------------------------------------------%>

     <%-----------------------Card type section----------------------------------------------------------%>
     <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'CardType')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

      <li id="cardTypeControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="cardTypeErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="cardType">Card type:</label></p>

        <div class="controls">
          <select name="payment_0_type"  id="cardType"  >
            <option value="">Please select</option>
            <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}"><option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> </option></c:forEach>
          </select>

          <%-----Show different card type image based on brands -----%>
          <c:set var="cardTypeStyle" value="GB"/>
          <c:set var="cardTypeTitle" value="We accept Visa, Delta, MasterCard, Maestro, Solo and American Express."/>

          <c:if test="${isFalconROI}">
            <c:set var="cardTypeStyle" value="IE"/>
            <c:set var="cardTypeTitle" value="We accept Visa, MasterCard and Laser."/>
          </c:if>

          <c:if test="${isFalconNI}">
            <c:set var="cardTypeStyle" value="northIE"/>
            <c:set var="cardTypeTitle" value="We accept Visa, Delta, MasterCard, Maestro and Solo."/>
          </c:if>

          <span class="cardType <c:out value='${cardTypeStyle}'/>" title="<c:out value='${cardTypeTitle}'/>"><c:out value='${cardTypeTitle}'/></span>
          <%---**END of showing card type images**---%>
        </div>
      </li>
     <%------------------*****Card type section*****-----------------------------------------------------%>

     <%-----------------------Total amount payable section-----------------------------------------------%>
      <li id="totalAmountPayableControlGroup" class="controlGroup hide">
        <p id="totalAmountPayableErrorMessage" class="errorMessage hide"></p>
        <p class="fieldLabel"><label for="totalAmountPayable">Total amount payable:</label></p>

        <div class="controls">
          <p>
            <span class="currency"><c:out value="${currency}" escapeXml="false"/></span>
            <input type="text" id="totalAmountPayable" class="calPayableAmount" readonly="readonly" />
          </p>
          <p id="totalAmountPayableCaption" class="hide">(include credit card charge)</p>
        </div>
      </li>
     <%------------------*****Total amount payable section*****------------------------------------------%>

     <%-----------------------Card number section--------------------------------------------------------%>
     <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'CardNumber')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

      <li id="cardNumberControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="cardNumberErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <a id="cardNumberAnchor" name="cardNumberAnchor"></a>
        <p class="fieldLabel"><label for="cardNumber">Card number:</label></p>

        <div class="controls">
          <input type="text" name="payment_0_cardNumber" id="cardNumber" class="longField" maxlength="20" autocomplete="off"  />
        </div>
      </li>
     <%------------------*****END Card number section*****-----------------------------------------------%>

     <%-----------------------Expiry date section--------------------------------------------------------%>
<c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'ExpiryDate')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

     <li id="expiryMonthControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="expiryMonthErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="expiryDate">Expiry date:</label></p>

        <div class="controls">
          <select name="payment_0_expiryMonth" id="expiryMonth">
            <c:forEach begin="1" end="12" varStatus="loopStatus">
              <fmt:formatNumber var="expiryMonth" value="${loopStatus.count}" type="number" minIntegerDigits="2" pattern="##"/>
              <option value="<c:out value="${expiryMonth}"/>"><c:out value="${expiryMonth}" /></option>
            </c:forEach>
          </select>

          <select name="payment_0_expiryYear" id="expiryYear">
            <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
              <option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>
            </c:forEach>
          </select>
        </div>
      </li>
     <%------------------*****END Expiry date section*****-----------------------------------------------%>

     <%-----------------------Name on card section-------------------------------------------------------%>
     <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'CardName')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

      <li id="nameOnCardControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="nameOnCardErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="nameOnCard">Name on card:</label></p>

        <div class="controls">
          <input type="text" name="payment_0_nameOnCard" id="nameOnCard" class="longField" maxlength="80" autocomplete="off" />
        </div>
      </li>
     <%------------------*****END Name on card section*****----------------------------------------------%>

     <%-----------------------Issue number section-------------------------------------------------------%>
      <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'IssueNumber')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

      <li id="issueNumberControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="issueNumberErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="issueNumber">Issue number:</label></p>

        <div class="controls">
          <input type="text" name="payment_0_issueNumber" id="issueNumber" maxlength="2" autocomplete="off" />
          <p>Leave blank if not on your card</p>
        </div>
      </li>
     <%------------------*****Issue number section*****--------------------------------------------------%>

     <%-----------------------Card security section------------------------------------------------------%>
      <c:set var="fieldControlGroupDisplayStyle" value=""/>
      <c:set var="fieldDisplayStyle" value="hide"/>
      <c:set var="fieldErrorMessage" value=""/>

      <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'SecurityCode')}">
        <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
        <c:set var="fieldDisplayStyle" value=""/>
        <c:set var="fieldErrorMessage" value="${bookingComponent.errorMessage}"/>
      </c:if>

      <li id="cardSecurityCodeControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
        <p id="cardSecurityCodeErrorMessage" class="errorMessage <c:out value="${fieldDisplayStyle}"/>"><c:out value="${fieldErrorMessage}" escapeXml="false"/></p>
        <p class="fieldLabel"><label for="cardSecurityCode">Card security code:</label></p>

        <div class="controls">
          <input type="text" name="payment_0_securityCode" id="cardSecurityCode" maxlength="4" autocomplete="off" />

        </div>
        <p><a id="cardSecurityCodeLink" href="#cardSecurityCodeLinkOverlay" class="popupLink blinkyOwner">The last 3 digits on the reverse of your card:</a></p>
        <p class="cardSecurity">
          <a href="javaScript:void(Popup('http://www.mastercard.com/uk/personal/en/cardholderservices/securecode/index.html',630,600,''))" class="masterCard" title="MasterCard SecureCode">MasterCard SecureCode</a>
          <a href="javaScript:void(Popup('http://www.visaeurope.com/personal/onlineshopping/verifiedbyvisa/main.jsp',630,600,''))" class="visa" title="Verified by VISA">Verified by VISA</a>
        </p>
      </li>
     <%------------------*****END Card security section*****---------------------------------------------%>
    </ul><%--END paymentOptionDetails --%>
  </div><%-- END sectionContent --%>
</div><%-- END section --%>