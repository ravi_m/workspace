<%@include file="/common/commonTagLibs.jspf"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <c:choose>
   <c:when test='${param["b"]==19000}'>
       <c:set var="brand" value="thomson" />
   </c:when>
   <c:when test='${param["b"]==20000}'>
       <c:set var="brand" value="fcsun"/>
   </c:when>
   <c:when test='${param["b"]==21000}'>
       <c:set var="brand" value="fcfalcon"/>
   </c:when>
   <c:otherwise>
       <c:set var="brand" value="fcsun"/>
   </c:otherwise>
   </c:choose>
   
  <% String brand = (String)pageContext.getAttribute("brand"); %>
  <title>Payment Details - Errors</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <%@include file="resources.jspf"%>
  <script src="/cms-cps/wss/global/js/popUp.js" type="text/javascript"></script>
</head>

<body class="errors">
<div class="wrapper">
  <%-------------------------------Include brand specific header --------------------------------------%>
  <div id="header">
    <%@include file="header.jspf"%>
  </div><%-- End of Header --%>
 <%--------------------------*****End brand specific header*****--------------------------------------%>

  <div id="contentContainer">
    <div class="mainContent">
       <c:if test="${not empty bookingInfo.trackingData.sessionId}">
          <%@include file="logout.jspf"%>
       </c:if>
  
      <div class="mainContentTop">
        <span class="outer">&nbsp;</span>
        <span class="inner">&nbsp;</span>
      </div>

      <div class="mainContentMiddle">
         <div id="pageOutcome" class="panelOuter2 pageError">
             <c:choose>
               <c:when test="${not empty bookingInfo.trackingData.sessionId}">
                  <%@include file="errorTechDifficulties.jspf"%>
               </c:when>
               <c:otherwise>
                  <%@include file="errorSessionTimeOut.jspf"%>
               </c:otherwise>
              </c:choose>

        </div><!-- END #pageOutcome - Validation Errors -->
      </div><!-- END mainContentMiddle -->

      <div class="mainContentBottom">
        <span class="outer">&nbsp;</span>
        <span class="inner">&nbsp;</span>
      </div>
    </div><!-- END mainContent -->
  </div><!-- END #contentContainer -->
</div><!-- END wrapper -->

<%--------------------------------Include brand specific Footer --------------------------------------%>
<div id="footer">
  <%@include file="footer.jspf"%>
</div>
<%---------------------------*****End brand specific Footer*****--------------------------------------%>

</body>
</html>
