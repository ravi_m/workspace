<%@include file="/common/commonTagLibs.jspf"%>
<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <%@ page contentType="text/html; charset=UTF-8" %>
  <version-tag:version/>
  <c:choose>
      <c:when test='${param["b"]==19000}'>
        <c:set var="brand" value="thomson" />
        <script src="/cms-cps/wss/thomson/js/functions_general.js" type="text/javascript"></script>
      </c:when>
      <c:when test='${param["b"]==20000}'>
        <c:set var="brand" value="fcsun"/>
      </c:when>
      <c:when test='${param["b"]==21000}'>
        <c:set var="brand" value="fcfalcon"/>
      </c:when>
      <c:otherwise>
        <c:set var="brand" value="fcsun"/>
      </c:otherwise>
  </c:choose>
  <%
      String brand = (String)pageContext.getAttribute("brand");

      String privacyPolicyURL = com.tui.uk.config.ConfReader.getConfEntry(brand+".privacypolicy.url", "");
      pageContext.setAttribute("privacyPolicyURL", privacyPolicyURL, PageContext.REQUEST_SCOPE) ;

      String airPassengerNoticeURL = com.tui.uk.config.ConfReader.getConfEntry(brand+".airPassengerNotice.url" , "");
      pageContext.setAttribute("airPassengerNoticeURL", airPassengerNoticeURL, PageContext.REQUEST_SCOPE);
  %>
  <%
	  String getHomePageURL =(String)ConfReader.getConfEntry("WSS."+brand+".homepage.url","");
	  pageContext.setAttribute("getHomePageURL",getHomePageURL);
  %>
  <c:choose>
 	  <c:when test="${not empty bookingInfo.bookingComponent.clientURLLinks.homePageURL}">
  		  <c:set var='homePageURL' value="${bookingInfo.bookingComponent.clientURLLinks.homePageURL}" />
      </c:when>
      <c:otherwise>
 	  	  <c:set var='homePageURL' value='${getHomePageURL}'/>
      </c:otherwise>
  </c:choose>
  <%@include file="css.jspf" %>
  <%@include file="javascript.jspf" %>
  <%@include file="configSettings.jspf" %>
  <script type="text/javascript" src="/cms-cps/wss/phase1/global/js/prototype.js"></script>
  <script type="text/javascript">
     var session_timeout= ${bookingInfo.bookingComponent.sessionTimeOut};
	 var IDLE_TIMEOUT = (${bookingInfo.bookingComponent.sessionTimeOut})-(${bookingInfo.bookingComponent.sessionTimeAlert});
	 var _idleSecondsCounter = 0;
	 var IDLE_TIMEOUT_millisecond = IDLE_TIMEOUT *1000;
	 var url="${homePageURL}";
	 var sessionTimeOutFlag =  false;

	 window.setInterval(CheckIdleTime, 1000);
     window.alertMsg();

	 function noback()
	 {
		  window.history.forward();
	 }

	 function alertMsg()
	 {
		  var myvar = setTimeout(function(){
		  document.getElementById("sessionTime").style.visibility = "visible";
		  document.getElementById("modal").style.visibility = "visible";
		  document.getElementById("sessionTimeTextbox").style.visibility = "visible";
		  var count = Math.round((session_timeout - _idleSecondsCounter)/60);
		  document.getElementById("sessiontimeDisplay").innerHTML = count +"mins";
		  },IDLE_TIMEOUT_millisecond);
	 }
	
	 function CheckIdleTime()
	 {
		 _idleSecondsCounter++;
	
	    if (  _idleSecondsCounter >= session_timeout)
	    {
		   	if (window.sessionTimeOutFlag == false)
		   	{
		   		document.getElementById("sessionTime").style.visibility = "hidden";
		   		document.getElementById("modal").style.visibility = "hidden";
		   		document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
		   		document.getElementById("sessionTimeout").style.visibility = "visible";
		   		document.getElementById("modal").style.visibility = "visible";
		   		document.getElementById("sessionTimeOutTextbox").style.visibility = "visible";
		   	}
		   	else
		   	{
		   		document.getElementById("sessionTimeout").style.visibility = "hidden";
		   		document.getElementById("modal").style.visibility = "visible";
		   		document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";
		   	}
		   //navigate to technical difficulty page
		}
	}
	 
	function closesessionTime()
	{
		document.getElementById("sessionTime").style.visibility = "hidden";
	    document.getElementById("modal").style.visibility = "hidden";
		document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
	}
	
	function reloadPage()
	{
		 window.sessionTimeOutFlag= true;
		 document.getElementById("sessionTimeout").style.visibility = "hidden";
		 document.getElementById("modal").style.visibility = "hidden";
		 document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";
		 window.noback();
		 window.location.replace(window.url);
		 return(false);
	}
	
	function homepage()
	{
		 url="${homePageURL}";
		 window.noback();
		 window.location.replace(url);
	}
	function activestate()
	{
		 document.getElementById("sessionTime").style.visibility = "hidden";
		 document.getElementById("modal").style.visibility = "hidden";
		 document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
		 window.ajaxForCounterReset();
		 window.alertMsg();
		 window._idleSecondsCounter = 0;
	}
	
	function ajaxForCounterReset()
	{
		 var token = "<c:out value='${param.token}' />";
		 var url = "/cps/SessionTimeOutServlet?tomcat=<c:out value='${param.tomcat}'/>";
		 var request = new Ajax.Request(url, {
			 method : "POST"
		 });
		 window._idleSecondsCounter = 0;
	}
	
	function newPopup(url)
	{
		 var win = window.open(url,"","width=700,height=600,scrollbars=yes,resizable=yes");
		 if (win && win.focus) win.focus();
	}
  </script>
  <c:set var="title">
    <c:choose>
      <c:when test="${fn:containsIgnoreCase(bookingFlow, 'CANCEL') || fn:containsIgnoreCase(bookingFlow, 'AMEND')}">
        <c:choose>
          <c:when test="${fn:containsIgnoreCase(paymentFlow, 'REFUND') && fn:containsIgnoreCase(bookingFlow, 'CANCEL')}">
            Refund
          </c:when>
          <c:when test="${fn:containsIgnoreCase(paymentFlow, 'REFUND') && fn:containsIgnoreCase(bookingFlow, 'AMEND')}">
            Refund Details
          </c:when>
          <c:otherwise>
            Payment Details
          </c:otherwise>
        </c:choose>
      </c:when>
      <c:otherwise>
        Payment Details
      </c:otherwise>
    </c:choose>
  </c:set>

  <title><c:out value="${title}" escapeXml="false"/></title>
</head>

<body>
<c:set var="visiturl" value="yes" scope="session" />
 <!--Session Time out popup-->
	<div id="sessionTime" class="posFix">
		<div id="sessionTimeTextbox" class="sessionTimeText">
			<div class="session_dialog_title fl">
						 <div class="sessionText fl">Are you still there?</div>
						 <a style="color:#73afdc" class="remove fr" href="#"
							onClick="closesessionTime()">close</a>
			</div>
			<div class="session_dialog_timeOut_content_container fl">
					<div class="SesTimeoutImg fl">
							<div class="sessionoutWarnClock fl"></div>
							<div class="clear"></div>
							<span class="sesDisTime">Timeout in</span>
							<span <c:if test="${brand == 'thomson'}">style="line-height:8px"</c:if> id="sessiontimeDisplay" class="sesDisTime"></span>
							<div class="clear"></div>
					</div>
					<div class="session_dialog_timeOut_content"
					    <c:choose>
					      <c:when test="${brand == 'thomson'}">
					        style="line-height:20px;padding-left: 31px"
					      </c:when>
					      <c:otherwise>
					        style="line-height:20px"
					      </c:otherwise>
					    </c:choose>> 
						<p style="font-size:14.5px">This page will soon be timing out for security reasons. Let us know that <br/> you're still here , otherwise you'll have to re-enter your details.</p>
					</div>
					<div class="clear"></div>
					<div class="sessionTimeTextbox_border-divider"></div>
			</div>
			<div class="activestate fr">
						 <a class="button fr" href="#"
							onClick="activestate()" style="color:#000;font-weight:500">I'm still here</a>
			</div>
		</div>
	</div>
	
	<div class="posFix" id="sessionTimeout">
		<div class="sessionTimeOutText" id="sessionTimeOutTextbox">
			<div class="session_dialog_title fl">
				 <div class="sessionText fl">Sorry, this page has timed out!</div>
				 <a style="color:#73afdc" onclick="reloadPage()" class="remove fr">close</a>
			</div>
			<div class="session_dialog_timeOut_content_container fl">
				<div class="sessiontimeout_content fl" style="line-height:20px">
				Unfortunately this page has timed out for security reasons. Don't worry, we're not going to make you start over again, but we do need you to re-enter your details on this page. If you don't want to continue you can go to the <a style="color:#73afdc;text-decoration:none" href="#" onclick="homepage()">homepage</a>.
				</div>
				<div class="clear"></div>
				<div class="border-divider_sessiontimeout"></div>
			</div>
			<div class="activestate fr">
				 <a onclick="reloadPage()"  class="button fr">ok</a>
			</div>
		</div>
	</div>
	<div id="modal" class="web_dialog_overlay"></div> 
  <%@include file="cookieheader.jspf"%>
  <div class="wrapper">

    <div id="header">
      <%@include file="header.jspf"%>
    </div><%-- End of Header --%>

    <div id="contentContainer">
      <div class="mainContent">
        
        <%@include file="logout.jspf"%>
        <div class="mainContentMiddle">
          <div class="sectionContainer">
            <c:choose>
              <c:when test="${!(fn:containsIgnoreCase(bookingFlow, 'CANCEL'))}">
                <%@include file="changeSummary.jspf" %>
              </c:when>
            </c:choose>

            <div class="primaryContentPanel">
              <form action="/cps/processPayment?token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tui_brand=<c:out value='${param["tui_brand"]}'/>&tomcat=<c:out value='${param.tomcat}' />" method="post" onsubmit="updateEssentialFields();return payNowButtonClicked();">
                <%--Contains Essential configuration settings used thru out the page --%>
                <%@include file="pageErrors.jspf"%>
                <%@include file="timeoutInfo.jspf"%>
                <%@include file="mainContentTop.jspf" %>                
                <c:choose>
                  <c:when test="${fn:containsIgnoreCase(bookingFlow, 'OLBP')}">
                    <%-- /*bookingSummary.jspf is not required in Phase 2 any more. It is replaced by Change Summary panel on the left*/ --%>
                    <%-- /*@include file="bookingSummary.jspf"*/ --%>
                    <input type="hidden" name="bookingFlow" id="bookingFlow" value="OLBP"/>
                    <input type="hidden" name="paymentFlow" id="paymentFlow" value="PAYMENT"/>
                    <%@include file="paymentDetails.jsp"%>
                  </c:when>
                  <c:when test="${fn:containsIgnoreCase(bookingFlow, 'AMEND')}">
                    <input type="hidden" name="bookingFlow" id="bookingFlow" value="AMEND"/>
                    <%--@include file="promotionalCode.jspf" --%>
                    <c:choose>
                      <c:when test="${fn:containsIgnoreCase(paymentFlow, 'PAYMENT')}">
                        <input type="hidden" name="paymentFlow" id="paymentFlow" value="PAYMENT"/>
                        <%@include file="paymentDetails.jsp"%>
                      </c:when>
                      <c:when test="${fn:containsIgnoreCase(paymentFlow, 'REFUND')}">
                        <input type="hidden" name="paymentFlow" id="paymentFlow" value="REFUND"/>
                        <%@include file="refundPanel.jspf" %>
                      </c:when>
                      <c:otherwise>
                        <%-- /*No Payment panel should be added here. */--%>
                      </c:otherwise>
                    </c:choose>
                    <%@include file="termsAndConditions.jspf" %>
                  </c:when>
                  <c:when test="${fn:containsIgnoreCase(bookingFlow, 'CANCEL')}">
                    <input type="hidden" name="bookingFlow" id="bookingFlow" value="CANCEL"/>
                    <c:choose>
                      <c:when test="${fn:containsIgnoreCase(paymentFlow, 'PAYMENT')}">
                        <input type="hidden" name="paymentFlow" id="paymentFlow" value="PAYMENT"/>
                        <%@include file="paymentDetails.jsp"%>
                        <%@include file="termsAndConditions.jspf" %>
                      </c:when>
                      <c:when test="${fn:containsIgnoreCase(paymentFlow, 'REFUND')}">
                        <input type="hidden" name="paymentFlow" id="paymentFlow" value="REFUND"/>
                        <%@include file="refundPanel.jspf" %>
                      </c:when>
                    </c:choose>
                  </c:when>
                </c:choose>
                <%@include file="mainContentBottom.jspf" %>
                <%@include file="buttonGroup.jspf"%>

                <%----------------------Essential fields -------------------------------------------------------------%>
                <input type="hidden" name="total_transamt"  id="total_transamt"  value="NA" />
                <%-- no of transaction is 1  --%>
                <input type="hidden" id="payment_0_totalTrans" value="1" name="payment_totalTrans"/>
                <%-----------------*****END Essential fields *****----------------------------------------------------%>
              </form>
            </div><%-- END primaryContentPanel --%>
          </div><%-- END sectionContainer--%>
        </div><%-- END mainContentMiddle --%>
      </div><%-- END mainContent --%>
    </div><%-- END #contentContainer --%>
  </div><%-- END wrapper --%>

  <%@include file="footer.jspf"%>
    <%--------------------------------Include functional scripts and onload functions --------------------%>
    <script src="/cms-cps/wss/global/js/wssPayment.js" type="text/javascript"></script>
    <script src="/cms-cps/wss/global/js/formValidation.js" type="text/javascript"></script>

    <script type="text/javascript">
        $j(document).ready(function()
        {
            targetBlank();
            toggleOverlayWSS();
            initialisePaymentEvents();
            if ($j("#paymentFlow").val() == "REFUND")
            {
                negateAmount();
            }
        });
        window.onunload = function(){ };
    </script>
</body>
</html>
