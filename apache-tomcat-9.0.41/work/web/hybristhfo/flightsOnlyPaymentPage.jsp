<%@ page import="com.tui.uk.config.ConfReader"%>
<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html>
<html>
<head>
<!--[if IE 8 ]>    <html lang="en" id="firstchoice" class="ie8 "> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" id="firstchoice" class="ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="" id="falcon">
<!--<![endif]-->
<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="/cms-cps/hybristhfo/css/ie8-and-down.css" />
<![endif]-->
<!-- HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
<!--[if lte IE 9]>
			<link rel="shortcut icon" href="/cms-cps/hybristhfo/images/favicon.ico" type="image/x-icon" />
			<link rel="icon" href="/cms-cps/hybristhfo/images/favicon.ico" />
			<![endif]-->
<!--#endif-->
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}"
	scope="request" />
<link href="/cms-cps/hybristhfo/css/main.css" rel="stylesheet"
	type="text/css" />
<link href="/cms-cps/hybristhfo/css/print.css" rel="stylesheet"
	type="text/css" media="print" />
<link rel="icon" type="image/png"
	href="/cms-cps/hybristhfo/images/favicon.png" />
<script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
<script type="text/javascript"
	src="/cms-cps/common/js/commonAjaxCalls.js"></script>
<script type="text/javascript"
	src="/cms-cps/common/js/functions_toggle.js"></script>
<script type="text/javascript"
	src="/cms-cps/common/js/paymentPanelContent.js"></script>
<script type="text/javascript"
	src="/cms-cps/common/js/paymentPanelValidation.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
<script type="text/javascript" src="/cms-cps/hybristhfo/js/prototype.js"></script>
<script type="text/javascript"
	src="/cms-cps/hybristhfo/js/functions_checkout_payment_details.js"></script>

<script type="text/javascript">
		var ensLinkTrack = function(){};
</script>
</head>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag"%>
<version-tag:version />
<title>Flights with TUI | Thomson now TUI Airways</title>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

<c:set var='clientapp'
	value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName} ' />
<%
	String clientApp = (String) pageContext.getAttribute("clientapp");
	clientApp = (clientApp != null) ? clientApp.trim() : clientApp;
	String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(
			clientApp + ".3DSecure", "");
	pageContext.setAttribute("is3DSecure", is3DSecure,
			PageContext.REQUEST_SCOPE);
	String cvvEnabled = com.tui.uk.config.ConfReader.getConfEntry(
			clientApp + ".CV2AVS.Enabled", "false");
	pageContext.setAttribute("cvvEnabled", cvvEnabled,
			PageContext.REQUEST_SCOPE);
	String isExcursion = com.tui.uk.config.ConfReader.getConfEntry(
			clientApp + ".summaryPanelWithExcursionTickets", "false");
	pageContext.setAttribute("isExcursion", isExcursion,
			PageContext.REQUEST_SCOPE);
%>
<%
	String getHomePageURL = (String) ConfReader.getConfEntry(
			"hybristhfo.homepage.url", "");
	pageContext.setAttribute("getHomePageURL", getHomePageURL);
%>
<%
	String applyCreditCardSurcharge = com.tui.uk.config.ConfReader
			.getConfEntry(clientApp + ".applyCreditCardSurcharge", null);
	pageContext.setAttribute("applyCreditCardSurcharge",
			applyCreditCardSurcharge, PageContext.REQUEST_SCOPE);
%>
<%
		String testBootstrap = ConfReader.getConfEntry("test.bootstrap.file" , "");
		pageContext.setAttribute("testBootstrap", testBootstrap, PageContext.REQUEST_SCOPE);
		String prodBootstrap = ConfReader.getConfEntry("prod.bootstrap.file" , "");
		pageContext.setAttribute("prodBootstrap", prodBootstrap, PageContext.REQUEST_SCOPE);

%>
<script src="${testBootstrap}"></script>
<script src="${prodBootstrap}"></script>
<script>
applyCreditCardSurcharge = "<%=applyCreditCardSurcharge%>";
</script>
<c:choose>
	<c:when
		test="${not empty bookingInfo.bookingComponent.clientURLLinks.homePageURL}">
		<c:set var='homePageURL'
			value="${bookingInfo.bookingComponent.clientURLLinks.homePageURL}" />
	</c:when>
	<c:otherwise>
		<c:set var='homePageURL' value='${getHomePageURL}' />
	</c:otherwise>
</c:choose>

<script type="text/javascript"
	src="/cms-cps/hybristhfo/js/functions_fo_paymentPage.js"></script>
<script type="text/javascript"
	src="/cms-cps/hybristhfo/js/functions_manualAddress.js"></script>

<script type="text/javascript" language="javascript">
	 //seconds
	var session_timeout= ${bookingInfo.bookingComponent.sessionTimeOut};
	var IDLE_TIMEOUT = (${bookingInfo.bookingComponent.sessionTimeOut})-(${bookingInfo.bookingComponent.sessionTimeAlert});
	var _idleSecondsCounter = 0;
	var IDLE_TIMEOUT_millisecond = IDLE_TIMEOUT *1000;
	var url="${bookingInfo.bookingComponent.breadCrumbTrail['SEARCH_RESULT']}";
	var sessionTimeOutFlag =  false;

	window.addEventListener('pageshow', function(event) {
		console.log(event.persisted);
		if(navigator.userAgent.indexOf("MSIE") == -1){
		jQuery(document).ready(function()
        {
                var d = new Date();
                d = d.getTime();
                if (jQuery('#reloadValue').val().length == 0)
                {
                        jQuery('#reloadValue').val(d);

                }
                else
                {
                        jQuery('#reloadValue').val('');
			location.reload();


                }
        });
		}
		else{
		if(event.currentTarget.performance.navigation.type == 2)
		{
			 location.reload();
		}
		}
	});

	function noback(){
		window.history.forward();
		$('FieldExpiryDateMonth').selectedIndex = 0;
		$('FieldExpiryDateYear').selectedIndex  = 0;
		document.getElementById("tandc-info").checked=false;
	}

	window.setInterval(CheckIdleTime, 1000);
	window.alertMsg();
	function alertMsg(){

	var myvar = setTimeout(function(){
	document.getElementById("sessionTime").style.visibility = "visible";
	document.getElementById("modal").style.visibility = "visible";
	document.getElementById("sessionTimeTextbox").style.visibility = "visible";

	var count = Math.round((session_timeout - _idleSecondsCounter)/60);
	document.getElementById("sessiontimeDisplay").innerHTML = count +"mins";
	},IDLE_TIMEOUT_millisecond);

	}

	function CheckIdleTime() {
    _idleSecondsCounter++;

    if (  _idleSecondsCounter >= session_timeout) {

    	if (window.sessionTimeOutFlag == false){
		document.getElementById("sessionTime").style.visibility = "hidden";
		document.getElementById("modal").style.visibility = "hidden";
		document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
		document.getElementById("sessionTimeout").style.visibility = "visible";
		document.getElementById("modal").style.visibility = "visible";
		document.getElementById("sessionTimeOutTextbox").style.visibility = "visible";
    	}else{
    		document.getElementById("sessionTimeout").style.visibility = "hidden";
    		document.getElementById("modal").style.visibility = "visible";
    		document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";


    	}
	   //navigate to technical difficulty page

		}
	}
	function closesessionTime()
	{

		document.getElementById("sessionTime").style.visibility = "hidden";
		document.getElementById("modal").style.visibility = "hidden";
		document.getElementById("sessionTimeTextbox").style.visibility = "hidden";

	}
	function reloadPage(){
		window.sessionTimeOutFlag= true;
		document.getElementById("sessionTimeout").style.visibility = "hidden";
		document.getElementById("modal").style.visibility = "hidden";
		document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";
		window.noback();
		window.location.replace(window.url);
		return(false);

	}
	function homepage()
	{
		//url="${bookingInfo.bookingComponent.clientURLLinks.homePageURL}";
			url="${homePageURL}";
		window.noback();

		window.location.replace(url);
	}
	function activestate()
	{

		document.getElementById("sessionTime").style.visibility = "hidden";
		document.getElementById("modal").style.visibility = "hidden";
		document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
		window.ajaxForCounterReset();
		window.alertMsg();
		window._idleSecondsCounter = 0;
	}
	function ajaxForCounterReset() {
		var token = "<c:out value='${param.token}' />";
		var url = "/cps/SessionTimeOutServlet?tomcat=<c:out value='${param.tomcat}'/>";
		var request = new Ajax.Request(url, {
			method : "POST"
		});
		window._idleSecondsCounter = 0;

	}

	function newPopup(url) {
		var win = window.open(url,"","width=700,height=600,scrollbars=yes,resizable=yes");
	    if (win && win.focus) win.focus();
	}


	function makePayment() {
		document.getElementById('errorMessageToolTip').style.display = "none";
		if( document.getElementById("errorMsg") ){
			document.getElementById("errorMsg").style.display = "none";
		}
		var isvalid = true,
		cardValid = false;
		
		if(document.querySelectorAll('#cardNumber + #validation-fail').length === 0){
			if(allnumeric('cardNumber') && validateCVV('cvv') && allCharacters('cardholderName') && getMonth('FieldExpiryDateMonth') || validateISSUE('issuenum') ){
				cardValid = true;
			}
		}
		if(checkBox()){
			isvalid = isValid();
		}
		else
		{
			var countryValue = "${bookingInfo.bookingComponent.contactInfo.country}";
			if(countryValue != "")
			{
				document.getElementById('country').value = countryValue;
			}
		}
		if(!isvalid || !cardValid){
			document.getElementById('errorMessageToolTip').style.display = "block";
		}
		var paymentForm = $('CheckoutPaymentDetailsForm');
		var element = paymentForm.serialize();
		elementstring = element
				+ "&token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />";
		var url = "/cps/processPostPayment?token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}'/>";
		elementstring = elementstring.replace("&title=&", "&title=" + leadTitle + "&");
		elementstring = elementstring.replace("payment_0_type=&", "");
		elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
		elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
		if(isvalid && cardValid){
			document.getElementById("modal").style.visibility = "visible";
			var request = new Ajax.Request(url, {
				method : "POST",
				parameters : elementstring,
				onSuccess : showThreeDOverlay
			});
		}
	}

	function isValid(){
		var firstName =  checkFName("firstName"),
			title = checkTitle("styled-select"),
			surName = checkName("surName"),
			houseName = housenameCheck("houseDetails1"),
			street1 = streetline1Check("StreetLine1"),
			street2 = streetline2Check("Streetline2"),
			town = townorcityCheck("townorcity"),
			county = testCounty("county"),
			postCode = testPostCode("postalCode");

		 if(!firstName || !title || !surName || !houseName || !street1 || !street2 || !town || !county || !postCode){
			return false;
		 }
		 return true;
		}

	/**
	 ** This method handles diplay of the 3D secure overlay based on the AJAX response.
	 *	This method handles	1. Redirecting to Confirmation page
	 *						2. Displaying the 3Ds secure overlay
	 *						3. Displaying the error message
	**/
	function showThreeDOverlay(http_request) {
		if (http_request.readyState == 4) {
			if (http_request.status == 200) {
				document.getElementById("modal").style.visibility = "hidden";
				result = http_request.responseText;
				if (result.indexOf("ACSframe") == -1) {
					if (result.indexOf("ERROR_MESSAGE") == -1) {
						document.postPaymentForm.action = result;
						document.CheckoutPaymentDetailsForm.reset();
						var csrfTokenVal = "<c:out value="${bookingComponent.nonPaymentData['csrfToken']}"/>";
						if(csrfTokenVal != '')
						{
						                   var csrfParameter = document.createElement("input");
						                    csrfParameter.type = "hidden";
						                    csrfParameter.name = "CSRFToken";
						                    csrfParameter.value = csrfTokenVal;
						                    document.postPaymentForm.appendChild(csrfParameter);
						}
						document.postPaymentForm.submit();
					} else {
						var errorMsg = result.split(':');

						jQuery('.alert').html('<i class="caret warning"></i>'+errorMsg[1]);
						document.getElementById("commonError").style.display = "block";
						jQuery('#commonError').removeClass('hide');
						jQuery(".ctnbutton").removeClass("disable");



						try
						{
							document.getElementById("errorMsg").style.display = "block";
							document.getElementById('errorMsg').innerHTML = "<p><strong>"
								+ errorMsg[1] + "</p></strong>";
							document.getElementById('errorMsg').className = "error-notify info-section clear padding10px mb20px";
							clearCardEntryElements();
						}
						catch(err)
						{
							console.log(err);
						}

					}
				} else {
					el = document.getElementById("overlay");
					el.style.visibility = (el.style.visibility == "visible") ? "hidden"
							: "visible";
					document.getElementById("modal").style.visibility = "visible";
					document.getElementById('overlay').innerHTML = result;
					bankRedirect();
					var vernode = document.getElementById('thcardtypetext');
					if(vernode){
						vernode.innerHTML = window.thCardTypeText;
					}
				}
			} else {
				document.getElementById('errorMsg').innerHTML = "<h2>Payment failed. Please try again.</h2>";
			}
		}
		window.scrollTo(0, 0);
	}



</script>
<body onload="setDefaultDepositOption();">
	<input id="reloadValue" type="hidden" name="reloadValue" value="" />
	<%@include file="flightsOnlyConfigSettings.jspf"%>
	<div id="wrapper">
		<jsp:include page="sprocket/header.jsp" />
		<div>
			<style type="text/css">
#mboxImported-default-TH_PromoStrip-0 {
	clear: both;
}
</style>
		</div>
		<div class="mboxDefault">&nbsp;</div>
		<script type="text/javascript">
		mboxCreate("Flight_Promostrip");
		</script>
		<%@include file="breadCrumb.jspf"%>
		<form id="CheckoutPaymentDetailsForm"
			name="CheckoutPaymentDetailsForm" method="post"
			action="javascript:makePayment();" novalidate>











			<c:if
				test="${(not empty bookingComponent.nonPaymentData['soldoutalertheader']) && (not empty bookingComponent.nonPaymentData['soldoutalertmessage'])}">
				<div id="errorMsg">
					<div class="alert-infosection">
						<div class="info-section pax-name">
							<span class="fix_header"> <b><c:out
										value="${bookingComponent.nonPaymentData['soldoutalertheader']}" /><b><br>
										<c:out
											value="${bookingComponent.nonPaymentData['soldoutalertmessage']}" /></span><a
								class="closeLink fr" onclick="closeAlertMsg('errorMsg');">x</a>

						</div>
					</div>
					<br />
				</div>
			</c:if>



			<c:if test="${not empty bookingComponent.errorMessage}">

				<c:if
					test="${fn:containsIgnoreCase(bookingComponent.errorMessage,'Whilst')}">
					<div id="errorMsg">
						<div class="alert-infosection">
							<div class="info-section pax-name">
								<c:if
									test="${fn:containsIgnoreCase(bookingComponent.errorMessage,'decreased')}">
									<span class="fix_header">Your flights are now even
										cheaper! </span>
									<a class="closeLink fr" onclick="closeAlertMsg('errorMsg');">x</a>
									<br>
								</c:if>
								<c:if
									test="${!fn:containsIgnoreCase(bookingComponent.errorMessage,'decreased')}">
									<span class="fix_header">The price of your flight has
										changed </span>
									<a class="closeLink fr" onclick="closeAlertMsg('errorMsg');">x</a>
									<br>
								</c:if>
								<c:out value="${bookingComponent.errorMessage}"
									escapeXml="false" />
							</div>
						</div>
						<br />
					</div>
				</c:if>
			</c:if>
			<h1 class="fs32px pad-left" style="text-transform: none;">BOOK
				YOUR FLIGHT</h1>



			<div class="content-section">
				<c:if test="${not empty bookingComponent.errorMessage}">
					<c:if
						test="${!fn:containsIgnoreCase(bookingComponent.errorMessage,'Whilst')}">
						<div id="errorMsgBlock" class="newWarningBlock">
							<div class="newWarningBlockInner">
								<div class="error-notify info-section clear padding10px mb20px">
									<p>
										<strong><c:out
												value="${bookingComponent.errorMessage}" escapeXml="false" /></strong>
									</p>
								</div>
							</div>
						</div>
					</c:if>
				</c:if>
				<%-- <c:if test="${not empty bookingComponent.errorMessage}">
					<c:if
						test="${!fn:containsIgnoreCase(bookingComponent.errorMessage,'Whilst')}">
						<div id="commonError" class="commonErrorSummary">
							<div class="standard-layout">
								<p class="alert high-level generic">
									<i class="caret warning"></i>
									<c:out value="${bookingComponent.errorMessage}"
										escapeXml="false" />
								</p>
							</div>
						</div>
					</c:if>
				</c:if> --%>

				<%-- <div id="commonError"
					class="commonErrorSummary <c:if test="${(not empty errorMessageWhilstMessage) || (empty errorMessageFirstLine) || (not empty bookingInfo.errorFields && hideErrorMessage) || fn:contains(errorMessageFirstLine, 'Promotional') || fn:contains(errorMessageFirstLine, 'PROMOTIONAL') || fn:contains(errorMessageFirstLine, 'promotional')}">hide</c:if>">

					<p class="alert high-level generic">
						<i class="caret warning"></i>
						<c:out value="${errorMessageFirstLine}" escapeXml="false" />
					</p>
				</div> --%>
				<!--		<div id="errorMsg"></div>           -->
				<%@include file="timeoutInfo.jspf"%>

				<div class="mask-interactivity"></div>

				<%@include file="importantInformation.jspf"%>

				<!-- <%@include file="promotionCode.jspf"%>-->

				<%@include file="passengerDetails.jspf"%>

				<%@include file="paymentDetails.jspf"%>

				<%@include file="cardHolderAddress.jspf"%>

				<div class="clear"></div>

				<%@include file="termsAndCondition.jspf"%>
				<div class="clear"></div>
				<div class="final"></div>
				<div class="clear"></div>
				<span id="paypalspanText">Book with</span>
				<div class="bookHoliday" align="center">
					<div class="buttonToolTip tooltip fl  disNone"
						id="errorMessageToolTip">
						<h3 class="error-message-text">Hold on there...</h3>
						<h5 class="error-message-subText">some details still haven't
							been entered, or are currently invalid</h5>
						<span class="arrow"></span>
					</div>
					<c:if test="${bookingInfo.newHoliday == true}">
						<button class="button cta jumbo" id="paypalbutton"
							onClick="return validateRequiredField();"
							onmouseout="displayTooltipInfo_mouseout('errorMessageToolTip');">BOOK
							AND PAY</button>
					</c:if>
				</div>
			</div>
		</form>
		<%@include file="summaryPanel.jspf"%>

	</div>
	<jsp:include page="sprocket/footer.jsp" />
	<div id="overlay" class="posFix"></div>
	<form novalidate name="postPaymentForm" method="post">
		<input type="hidden" name="emptyForm" class="disNone"></input>
	</form>
	<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,chkTuiMarketingAllowed,chkThirdPartyMarketingAllowed,communicateByPost,communicateByPhone,communicateByEmail,selected_country_code,TUIHeaderSwitch</c:set>
	<script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
				 tui.analytics.page.pageUid = "paymentDetailsPage";
				 <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
					 <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
					 tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
				    </c:if>
					<c:if test= "${analyticsDataEntry.key == 'Party'}">
						tui.analytics.page.Party= "${analyticsDataEntry.value}";
					</c:if>
			    </c:forEach>
				</script>
</body>
</html>