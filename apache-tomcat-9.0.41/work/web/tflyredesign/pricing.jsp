<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />

<style>
	.pl10px
	{
	padding:0 10px;
	}
	.clear
	{
	clear:both;
	}
</style>
<div id="summaryPanelContainer">
  <div class="detailsPanel" id="summaryDetailsPanel">
    <div class="top">
      <div class="right"></div>
    </div>
    <div class="body">
      <div class="right">
        <div class="content">
          <div class="summarySection">

		    <c:choose>
			 <c:when test="${not empty bookingComponent.nonPaymentData['operatingCarrier']}">
			  <c:choose>
			   <c:when test="${fn:contains(bookingComponent.nonPaymentData['operatingCarrier'],'THOMSON AIRWAYS')}">
						             <h2 id="summary">Thomson Airways</h2>
			    </c:when>
				<c:otherwise>
				 <h2><c:out value="${bookingComponent.nonPaymentData['operatingCarrier']}"/></h2>
				 </c:otherwise>
				</c:choose>
				</c:when>
				   <c:otherwise>
				                        <c:choose>
									  <c:when test="${fn:contains(bookingComponent.nonPaymentData['outBoundFlight_operatingAirlineCode'],'TOM')}">
									      <h2 id="summary">Thomson Airways</h2>

										 </c:when>
										   <c:otherwise>

				   <h2 style="padding:1px;"></h2>
				     </c:otherwise>
					  </c:choose>
					   </c:otherwise>
			</c:choose>
            <div class="priceDetails">
              <ul>
               <c:forEach var="priceComponent" items="${bookingComponent.priceComponents}">

				 <c:if test="${priceComponent.itemDescription!='total Price' && priceComponent.itemDescription!='Outgoing' && priceComponent.itemDescription!='Extras' && priceComponent.itemDescription!='Round Trip'}">
				    <c:choose>
					   <c:when test="${fn:contains(priceComponent.itemDescription,'Taxes & charges')}">
		            	 </c:when>
						  <c:otherwise>
              					   <li class="pricesInDetail"><span class="costingDetails">
									 <c:choose>
					   <c:when test="${fn:contains(priceComponent.itemDescription,'World Care Fund')}">
                                     <c:out value="${priceComponent.itemDescription}" escapeXml="false"/></span>
		            	 </c:when>
						  <c:otherwise>
						  <c:if test="${priceComponent.quantity>0}">
					     <c:out value="${priceComponent.quantity}" escapeXml="false"/>
							      x
					                </c:if>
					  <c:out value="${priceComponent.itemDescription}" escapeXml="false"/></span>
							  </c:otherwise>
				      </c:choose>



								   	    <c:if test="${not empty priceComponent.amount.amount}">
								   <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
					                 minFractionDigits="2" pattern="#,##,###.##"/></span>
									 </c:if>
									 </li>
			              </c:otherwise>
				      </c:choose>
                       </c:if>
                    </c:forEach>
						   <li class="pricesInDetail costingDetails" id="creditcardcontent" style="display:none;" >
						  <span style="float:left;padding:3px 2px;text-align:left;padding-right:0px;font-weight:700;border-bottom:1px solid #FDE4B1;" id="creditCardSurcharge" >Credit card surcharge</span>

						  <span style="float:right;font-weight:700;padding:3px 0;text-align:right;" id="surcharge">
						  </span>
						  </li>
				   <c:set var="counter" value="${bookingComponent.nonPaymentData['tax_count']}"/>
						  <c:forEach var="priceComponent" items="${bookingComponent.priceComponents}">
			     <c:if test="${priceComponent.itemDescription!='total Price' && priceComponent.itemDescription!='Outgoing' && priceComponent.itemDescription!='Extras' && priceComponent.itemDescription!='Round Trip'}">
	    			<c:choose>
					   <c:when test="${fn:contains(priceComponent.itemDescription,'Taxes & charges')}">

						 <li class="taxesCharges"><span class="costingDetails"><a id="taxTerms" class="sticky stickyOwner" href="#" title="View taxes and charges">   <c:out value="${priceComponent.itemDescription}" escapeXml="false"/></a></span> <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}" escapeXml="false"/>  <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
							minFractionDigits="2" pattern="#,##,###.##"/></span></li>
					  </c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				 </c:if>
			   </c:forEach>

			  <c:if test="${not empty bookingComponent.nonPaymentData['pricing_saving_amount']}">
                 <li class="webSaving">
                    <span class="costingDetails">
				       <c:out value="${bookingComponent.nonPaymentData['pricing_saving_desc']}"/>
				    </span>
				    <span class="price">
				       <c:out value="${bookingComponent.nonPaymentData['pricing_saving_amount']} " escapeXml="false"/>
				    </span>
				 </li>
              </c:if>

				<li class="totalCost"><span class="costingDetails">Total price</span> <span class="price"><c:out value="${bookingComponent.totalAmount.symbol}" escapeXml="false"/><span id="calculatedTotalAmount"> <fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
              minFractionDigits="2" pattern="#,##,###.##"/></span></span></li>
              </ul>
            </div>
            <!--start overlay-->
            <div class="genericOverlay" id="taxTermsOverlay">
              <div class="top">
                <div class="right"> </div>
              </div>
              <div class="body">
                <div class="right">
                  <div class="content">
                    <div class="popupTitle">
                      <h3>Taxes &amp; charges</h3>
                      <a class="close" id="taxTermsClose" href="#" title="Close overlay">Close</a> </div>
                    <p>Here is a breakdown of the taxes and fees that apply to your purchase</p>
                    <h4>Fare tax and fees:</h4>

                    <ul>
					 <c:forEach begin="0" end="${counter - 1}" varStatus="taxDsc">
                 <c:set var="dscKey" value="tax_dsc_${taxDsc.index}"/>


                 <c:set var="taxDescription" value="${bookingComponent.nonPaymentData[dscKey]}"/>
                 <c:set var="amountKey" value="tax_amt_${taxDsc.index}"/>

                 <c:choose>
                    <c:when test="${fn:contains(taxDescription,'Total Tax & Fees')}">
                       <c:set var="taxDetails" value="${taxDetails} \n" />
                    </c:when>
                 </c:choose>

                	    <li><span class="costingDetails">${taxDescription}</span> <span class="price">${bookingComponent.totalAmount.symbol}${bookingComponent.nonPaymentData[amountKey]} </span></li>


              </c:forEach>


                    </ul>
                  </div>
                </div>
              </div>
              <div class="bottom">
                <div class="right"></div>
              </div>
            </div><!--end overlay-->
            <h3> Your Itinerary </h3>
            <ul class="itinerary">
              <li class="itinerarySummary">
			  <c:out value="${bookingComponent.nonPaymentData['outBoundFlight_departureAirportName']}"/>
			 to   <c:out value="${bookingComponent.nonPaymentData['outBoundFlight_arrivalAirportName']}"/>
			  <c:if test="${not empty bookingComponent.nonPaymentData['tripDuration']}">
			 for
			  </c:if>
			 <c:out value="${bookingComponent.nonPaymentData['tripDuration']}"/></li>

              <li class="outbound">Outbound</li>
              <li class="outboundDetails"><c:out value="${bookingComponent.nonPaymentData['outBoundFlight_departureDate']}"/></li>
              <li class="outboundDetails"><span><c:out value="${bookingComponent.nonPaymentData['outBoundFlight_departureTime']}"/></span><span class="arrivalTime"><c:out value="${bookingComponent.nonPaymentData['outBoundFlight_arrivalTime']}"/></span> <span class="flightNumber"><c:out value="${bookingComponent.nonPaymentData['outBoundFlight_operatingAirlineCode']}"/></span></li>

			<c:if test="${not empty bookingComponent.nonPaymentData['inBoundFlight_departureDate']}">
              <li class="inbound">Inbound</li>

              <li class="inboundDetails"><c:out value="${bookingComponent.nonPaymentData['inBoundFlight_departureDate']}"/></li>
              <li class="inboundDetails"><span><c:out value="${bookingComponent.nonPaymentData['inBoundFlight_departureTime']}"/></span><span class="arrivalTime"><c:out value="${bookingComponent.nonPaymentData['inBoundFlight_arrivalTime']}"/></span> <span class="flightNumber"><c:out value="${bookingComponent.nonPaymentData['inBoundFlight_operatingAirlineCode']}"/></span></li>

 </c:if>
       </ul>
				 <c:if test="${bookingComponent.nonPaymentData['longhaul_premium_service']=='true' && (bookingComponent.nonPaymentData['isInboundLegDreamliner'])== 'false'&& (bookingComponent.nonPaymentData['isOutboundLegDreamliner'])== 'false' }">
			<h3 id="longHaul"> Thomson Airlines Longhaul </h3>
            <ul class="airlineDetails">
              <li>33 inches pitch as standard</li>
              <li>Comfy leather seats</li>
              <li>Cutting-edge seatback entertainment</li>
            </ul>
				  </c:if>
            <!--summaryPanel-->
            <c:if test="${(bookingComponent.nonPaymentData['isInboundLegDreamliner'])== 'true'&& (bookingComponent.nonPaymentData['isOutboundLegDreamliner'])== 'false' }">
			<br>
                 <p class="pl10px"><strong>You'll fly on Thomson's 787 Dreamliner for your Inbound Flight</strong></p>
                 <br />
				 <p align="center">
                 <img src="/cms-cps/tflyredesign/images/dreamliner-logo-small.jpg"></img>
				 </p>
                 <br />
                 <p class="pl10px" style="padding:0px 0px 3px 0px"><strong>Fly on the Dreamliner - fly on the best...</strong></p>
           <ul class="airlineDetails">
                 <li>From 32" seat pitch in Economy Club or 38" in Premium Club.</li>
                 <li>Plug your MP3 player into your seatback TV</li>
                 <li>High-spec mood lighting to help your body clock</li>
                 <li>Spacious cabin over 6-foot high</li>
                 <li>Dim your window at the touch of a button</li>
            </ul>
            

          </c:if>

          <c:if test="${(bookingComponent.nonPaymentData['isOutboundLegDreamliner'])== 'true'&& (bookingComponent.nonPaymentData['isInboundLegDreamliner'])== 'false' }">
		        <br>

                 <p class="pl10px"><strong>You'll fly on Thomson's 787 Dreamliner for your Outbound Flight</strong></p>

				 </br>
				 <p align="center">
                 <img src="/cms-cps/tflyredesign/images/dreamliner-logo-small.jpg"></img>
				 </p>
                 </br>
                 <p class="pl10px" style="padding:0px 0px 3px 0px"><strong>Fly on the Dreamliner - fly on the best...</strong></p>
				 <div class="clear"></div>
           <ul class="airlineDetails">
                 <li>From 32" seat pitch in Economy Club or 38" in Premium Club.</li>
                 <li>Plug your MP3 player into your seatback TV</li>
                 <li>High-spec mood lighting to help your body clock</li>
                 <li>Spacious cabin over 6-foot high</li>
                 <li>Dim your window at the touch of a button</li>
            </ul>
            


          </c:if>

          <c:if test="${(bookingComponent.nonPaymentData['isInboundLegDreamliner'])== 'true'&& (bookingComponent.nonPaymentData['isOutboundLegDreamliner'])== 'true' }">
		         <br></br>
		         <p align="center">
                 <img src="/cms-cps/tflyredesign/images/dreamliner-logo-small.jpg"></img>
				 </p>
				 <br/>
                 <p class="pl10px" style="padding:0px 0px 3px 0px"><strong>Fly on the Dreamliner - fly on the best...</strong></p>
				 <div class="clear"></div>
           <ul class="airlineDetails">
                 <li>From 32" seat pitch in Economy Club or 38" in Premium Club.</li>
                 <li>Plug your MP3 player into your seatback TV</li>
                 <li>High-spec mood lighting to help your body clock</li>
                 <li>Spacious cabin over 6-foot high</li>
                 <li>Dim your window at the touch of a button</li>
            </ul>
            


          </c:if>

      <c:if test="${(bookingComponent.nonPaymentData['atol_date']) !=null }">
		<c:set var="atoldate" value="${(bookingComponent.nonPaymentData['atol_date'])}"/>
		<c:choose>
			   <c:when test="${!fn:contains(bookingComponent.nonPaymentData['outBoundFlight_operatingAirlineCode'],'TOM')}">


					<fmt:setTimeZone value="Europe/London" scope="session"/>
						<c:set var="nowOct" value="${atoldate} "/>
						<fmt:parseDate var="nowOct" value="${nowOct}" type="DATE" pattern="dd MMM yyyy"/>
							   <jsp:useBean id="now" class="java.util.Date" />
							   <fmt:formatDate value="${now}" var="now" pattern="dd MMM yyyy" />
							   <fmt:parseDate var="now" value="${now}" type="DATE" pattern="dd MMM yyyy"/>
							  	<c:if test="${ now lt nowOct}">
											<br/>
												<img src="/cms-cps/tflyredesign/images/atol.png"></img>
								</c:if>
											<c:if test="${ now ge nowOct}">
											</c:if>
				</c:when>
		</c:choose>
		</c:if>
        <c:if test="${(bookingComponent.nonPaymentData['atol_date']) == null && ! (fn:contains(bookingComponent.nonPaymentData['outBoundFlight_operatingAirlineCode'],'TOM'))}">
		<br/>
			<img src="/cms-cps/tflyredesign/images/atol.png"></img>
		</c:if>
	          </div>
        </div>
      </div>
    </div>
    <div class="bottom">
      <div class="right"></div>
    </div>
  </div>
</div>