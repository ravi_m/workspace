<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>


           <div class="detailsPanel" id="fareDetailsPanel">
             <div class="top">
               <div class="right"></div>
             </div>
             <div class="body">
               <div class="right">
                 <div class="content">
                   <div class="section">
                        <h2 id="leadDetails"><span>Cardholder address details</span></h2>
                  <div class="mandatoryKey">Please complete all fields marked with <span class="mandatory">*</span></div>

                     <div class="sectionContent">

				To help ensure your card details remains secure, Please confirm the address of the cardholder.
								   <br/>If this is same as the lead passenger address, you just need to check the box.
                      <ul class="controlGroups">



                      <li class="controlGroup">
                        <div>
                          <input type="checkbox"  style="margin-left:180px;margin-top:15px;"
                            name="autofill" id="autofill" onclick="autoCompleteAddress()"> <span style="">same address as lead passenger</span>

                        </div>
                      </li>

                      <li class="controlGroup">
                        <p class="fieldLabel"><label>Street address</label></p>
                        <div class="controls">

                          <input type="text"
                             name="payment_0_street_address1" id="payment_0_street_address1"
                             maxlength="25" class="inputWhole longField"
							  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/>"
              requiredError="Your street address" regex="^[A-Za-z0-9'\./()\-&amp; ]+$" required="true"
              alt="Your address is required to complete your booking. Please enter your address|Y|ADDRESS"
              regexerror="Please enter only English alphanumeric characters or './()-&amp; in the address lines."
							 > <span class="mandatory">*</span>

                        </div>
                      </li>

                      <li class="controlGroup">

                        <div class="controls" style="margin-left:180px;">

                          <input type="text"
                             name="payment_0_street_address2" id="payment_0_street_address2"
                             maxlength="25" class="inputWhole longField"
							  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>"	   >
                        </div>
                      </li>
                      <li class="controlGroup">
                        <p class="fieldLabel"><label>Town/city</label></p>
                        <div class="controls">
                        <input type="text"
                             name="payment_0_street_address3" id="payment_0_street_address3"
                              maxlength="25" class="inputWhole longField"
							  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>"
                 regexerror="Please enter only English alphabetic characters or in the Town/City field."
                regex="^[A-Za-z ]+$"
                requirederror="Your town/city"
                required="true">

                          <span class="mandatory">*</span>
                        </div>
                      </li>
					    <li class="controlGroup">
                        <p class="fieldLabel"><label>Postal code</label></p>
                        <div class="controls">
                          <input name="payment_0_postCode" id="payment_0_postCode" type="text" maxlength="8"
						     value="<c:out value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/>"
                           class="inputWhole" regexerror="Please enter alphanumeric characters into the field for the postal code only (e.g. 'W2 4DJ')." 	regex="^[A-Za-z0-9 ]+$" 	requirederror="Your postal code" required="true">
                           <span class="mandatory">*</span>
                        </div>


                          </li>

						   <li class="controlGroup">
                        <p class="fieldLabel"><label>Country</label></p>
                        <div class="controls">
						 <c:choose>
              <c:when test="${bookingInfo.paymentDataMap['payment_0_selectedCountryCode'] == null }">
                      <c:set var="selectedCountryCode" value="GB"/>
     	 		  </c:when>
               <c:otherwise>
			      <c:set var="selectedCountryCode" value="${bookingInfo.paymentDataMap['payment_0_selectedCountryCode']}"/>
			   </c:otherwise>
           </c:choose>
		   <c:set var="totalcountrycount" value="${bookingComponent.nonPaymentData['country_count']}"/>

           <select name="payment_0_selectedCountryCode" id="payment_0_selectedCountryCode" required="true" requiredError="Your country" class="inputWhole longField" onchange="document.getElementById('payment_0_selectedCountry').value=this.options[selectedIndex].value;">
		      <c:forEach begin="0" end="${totalcountrycount - 1}" varStatus="countrycount">
			     <c:set var="countryCode" value="country_code_${countrycount.index}" />
				 <c:set var="countryName" value="country_dsc_${countrycount.index}" />
				 <option value='<c:out value="${bookingComponent.nonPaymentData[countryCode]}"/>' <c:if test='${selectedCountryCode==bookingComponent.nonPaymentData[countryCode]}'>selected='selected'</c:if>>
				    <c:out value="${bookingComponent.nonPaymentData[countryName]}" />
				 </option>
			  </c:forEach>
		   </select>
		   <input type="hidden" id="payment_0_selectedCountry" name="payment_0_selectedCountry" value="${selectedCountryCode}" />
	       <span class="mandatory">*</span>
        </div>


                          </li>

                    </ul>


                     </div>
                   </div>
                 </div>
               </div>
             </div>
             <div class="bottom">
               <div class="right"></div>
             </div>
           </div>
