<div class="info-section timer">
	<div class="icon clock">30:00</div>
	<h5 style="font-size:1em">Please complete this page within 30 minutes.</h5>
	<p style="font-size:1em">After 30 minutes this page will timeout and you will have to re-enter any details.</p>
</div>