<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="/common/commonTagLibs.jspf"%>
<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>
<jsp:useBean id="now" class="java.util.Date" /><fmt:formatDate var ="currentyear" type="date" value="${now}" pattern="yyyy"/>
<%@include file="/common/intellitrackerSnippet.jspf"%>
<html>
<head>
<%@ page contentType="text/html; charset=UTF-8" %>
<version-tag:version/>
<title>Thomson Flights all the assurances and service you've come to expect from Thomson</title>
<META NAME="description" content="Book great value flights with Thomson. Book your cheap flights, hotel accommodation, car hire, airport parking, transfers &amp; travel insurance through us." xmlns:myXsltExtension="urn:XsltExtension">
<META NAME="keywords" content="Thomson Airways, Thomson flights, thomsonfly.com, thomsonfly, cheap flights, cheapflights, cheap, flights, flight, ticket, tickets, air fare, airfares, fly, low cost fare, low cost, air travel, airline, budget airlines, travel, holidays, hotels, tickets, bookings, book, booking, plane, airport, discount, air, europe, international, scheduled, charter, destinations, longhaul, carhire, car hire, car rental, insurance, thomson holidays, Thompson, tompsonfly, flythomson, tomson, thomsonflights, thompsonfly, thomsonflights, britannia airways, birmingham, bournemouth, coventry, cardiff, doncaster, edinburgh, east midlands, gatwick, glasgow, jersey, leeds bradford, liverpool, luton, manchester, newcastle, stansted, london, spain, majorca, alicante, barcelona, faro, grenoble, ibiza, jersey, lanzarote, malaga, minorca, menorca, naples, paris, paphos, palma, prague, pisa, marrakech, salzburg, tenerife, turkey, cancun, mexico, cuba, egypt, greece, cyprus, dominican republic, croatia, italy, morocco, canary islands, portugal" xmlns:myXsltExtension="urn:XsltExtension">

<%
	String getHomePageURL =(String)ConfReader.getConfEntry("TFly.homepage.url","");
	pageContext.setAttribute("getHomePageURL",getHomePageURL);
%>
<c:choose>
 	<c:when test="${not empty bookingInfo.bookingComponent.clientURLLinks.homePageURL}">
  		<c:set var='homePageURL' value="${bookingInfo.bookingComponent.clientURLLinks.homePageURL}" />
    </c:when>
    <c:otherwise>
 	  	<c:set var='homePageURL' value='${getHomePageURL}'/>
    </c:otherwise>
</c:choose>
<link rel="icon" href="/cms-cps/tflyredesign/images/favicon.ico" type="image/x-icon" xmlns:myXsltExtension="urn:XsltExtension">
<link rel="shortcut icon" href="/cms-cps/tflyredesign/images/favicon.ico" type="image/x-icon" xmlns:myXsltExtension="urn:XsltExtension">
<script src="/cms-cps/tflyredesign/js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
<script type="text/javascript" src="/cms-cps/tflyredesign/js/Common.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
<script type="text/javascript" src="/cms-cps/tflyredesign/js/basic.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
<!--Modified for ensighten to remove existing tag-->
<script type="text/javascript" src="/cms-cps/tflyredesign/js/tfly.js"></script>
<script type="text/javascript" src="/cms-cps/tflyredesign/js/popups.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>

<script src="/cms-cps/tflyredesign/js/events.js" type="text/javascript"></script>
<link href="/cms-cps/tflyredesign/css/cps.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/cms-cps/tflyredesign/css/headfoot.css" rel="stylesheet" type="text/css" media="screen" />

<link rel="stylesheet" type="text/css" href="/cms-cps/tflyredesign/css/flexi-box/core.css" title="Thomson" media="screen" />
<!--flexibox-->
<!--[if lt IE 7]>
            <link rel="stylesheet" type="text/css" href="/cms-cps/tflyredesign/css/flexi-box/core-ie6.css" title="Thomson" media="screen" />
            <link href="/cms-cps/tflyredesign/css/headfoot-ie6.css" rel="stylesheet" type="text/css" media="screen" />
      <![endif]-->
<!--end flexibox-->
<!--[if gte IE 8]>
<style>
   .visaContainer{ padding-top: 135px;}
   .mastercardContainer{ padding-top: 138px;}
</style>
<![endif]-->

<!--[if lte IE 6]>
       <%--  This script is added to remove image caching in ie 6 which was a main cause for me
       losing sleep for days :-) This solves the flickering images problem in IE6.--%>
       <script type="text/javascript">
         try
         {
	        document.execCommand("BackgroundImageCache", false, true);
         }
         catch(err) { }
       </script>
<![endif]-->

<!-- Modified for ensighten to remove the existing tag which includes intellitracker.js -->
<script type="text/javascript" language="javascript">
	//js variable to be used in the intellitracker pqry string.
    var previousPageVar ="${bookingComponent.nonPaymentData['prevPage' ]}";
</script>
<script type="text/javascript">
     var session_timeout= ${bookingInfo.bookingComponent.sessionTimeOut};
	 var IDLE_TIMEOUT = (${bookingInfo.bookingComponent.sessionTimeOut})-(${bookingInfo.bookingComponent.sessionTimeAlert});
	 var _idleSecondsCounter = 0;
	 var IDLE_TIMEOUT_millisecond = IDLE_TIMEOUT *1000;
	 var url="${homePageURL}";
	 var sessionTimeOutFlag =  false;

	 window.setInterval(CheckIdleTime, 1000);
     window.alertMsg();

	 function noback()
	 {
		  window.history.forward();
	 }

	 function alertMsg()
	 {
		  var myvar = setTimeout(function(){
		  document.getElementById("sessionTime").style.visibility = "visible";
		  document.getElementById("modal").style.visibility = "visible";
		  document.getElementById("sessionTimeTextbox").style.visibility = "visible";
		  var count = Math.round((session_timeout - _idleSecondsCounter)/60);
		  document.getElementById("sessiontimeDisplay").innerHTML = count +"mins";
		  },IDLE_TIMEOUT_millisecond);
	 }
	
	 function CheckIdleTime()
	 {
		 _idleSecondsCounter++;
	
	    if (  _idleSecondsCounter >= session_timeout)
	    {
		   	if (window.sessionTimeOutFlag == false)
		   	{
		   		document.getElementById("sessionTime").style.visibility = "hidden";
		   		document.getElementById("modal").style.visibility = "hidden";
		   		document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
		   		document.getElementById("sessionTimeout").style.visibility = "visible";
		   		document.getElementById("modal").style.visibility = "visible";
		   		document.getElementById("sessionTimeOutTextbox").style.visibility = "visible";
		   	}
		   	else
		   	{
		   		document.getElementById("sessionTimeout").style.visibility = "hidden";
		   		document.getElementById("modal").style.visibility = "visible";
		   		document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";
		   	}
		   //navigate to technical difficulty page
		}
	}
	 
	function closesessionTime()
	{
		document.getElementById("sessionTime").style.visibility = "hidden";
	    document.getElementById("modal").style.visibility = "hidden";
		document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
	}
	
	function reloadPage()
	{
		 window.sessionTimeOutFlag= true;
		 document.getElementById("sessionTimeout").style.visibility = "hidden";
		 document.getElementById("modal").style.visibility = "hidden";
		 document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";
		 window.noback();
		 window.location.replace(window.url);
		 return(false);
	}
	
	function homepage()
	{
		 url="${homePageURL}";
		 window.noback();
		 window.location.replace(url);
	}
	function activestate()
	{
		 document.getElementById("sessionTime").style.visibility = "hidden";
		 document.getElementById("modal").style.visibility = "hidden";
		 document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
		 window.ajaxForCounterReset();
		 window.alertMsg();
		 window._idleSecondsCounter = 0;
	}
	
	function ajaxForCounterReset()
	{
		 var token = "<c:out value='${param.token}' />";
		 var url = "/cps/SessionTimeOutServlet?tomcat=<c:out value='${param.tomcat}'/>";
		 var request = new Ajax.Request(url, {
			 method : "POST"
		 });
		 window._idleSecondsCounter = 0;
	}
	
	function newPopup(url)
	{
		 var win = window.open(url,"","width=700,height=600,scrollbars=yes,resizable=yes");
		 if (win && win.focus) win.focus();
	}
   
   
   var  newHoliday  = "<c:out value='${bookingInfo.newHoliday}'/>";
   var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
   var token = "<c:out value='${param.token}' />" ;
   var streetaddress1="${bookingComponent.nonPaymentData['street_address1']}";
   var streetaddress2="${bookingComponent.nonPaymentData['street_address2']}";
   var streetaddress3="${bookingComponent.nonPaymentData['street_address3']}";
   var cardBillingPostcode="${bookingComponent.nonPaymentData['cardBillingPostcode']}";
  	var selectedcountryforleadpassenger="${bookingComponent.nonPaymentData['selectedCountryCode']}";

   var PaymentInfo = new Object();

PaymentInfo.totalAmount =${bookingComponent.totalAmount.amount};
PaymentInfo.payableAmount = ${bookingComponent.payableAmount.amount};
PaymentInfo.calculatedPayableAmount = ${bookingInfo.calculatedPayableAmount.amount};
PaymentInfo.calculatedTotalAmount = ${bookingInfo.calculatedTotalAmount.amount};
PaymentInfo.currency = "${bookingComponent.totalAmount.symbol}";

<fmt:formatNumber var="ccAmount" value="${bookingInfo.calculatedCardCharge.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>

PaymentInfo.chargeamt =${ccAmount};
PaymentInfo.selectedCardType = null;

   var cardChargeMap = new Map();
   var pleaseSelect = new Array();

cardChargeMap.put('none', pleaseSelect);
  <c:forEach var="cardCharges" items="${bookingInfo.cardChargeMap}">
  var cardChargeInfo = new Array();
		     cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');
     </c:forEach>


var cardDetails = new Map();
 <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">
			    var cardCharge_Max_Min_Issue = new Array();
        	         	   cardCharge_Max_Min_Issue.push('${cardDetails.cardType.securityCodeLength}');
						    cardCharge_Max_Min_Issue.push('${cardDetails.cardType.minSecurityCodeLength}');
							 cardCharge_Max_Min_Issue.push('${cardDetails.cardType.isIssueNumberRequired}');

			  cardDetails.put('${cardDetails.paymentCode}',cardCharge_Max_Min_Issue);

         </c:forEach>

		window.onload= function(){ checkBookingButton("bookingcontinue");}
        //  The following map stores the pay button labels in case of 3D cards.
        var threeDCards = new Map();
        <c:forEach var="threeDCards" items="${bookingInfo.payDescription}">
            threeDCards.put("<c:out value="${threeDCards.key}"/>","<c:out value="${threeDCards.value}"/>");
        </c:forEach>

  </script>
  <%@include file="/tflyredesign/tag.jsp"%>
</head>
<body>
<c:set var='clientapp' value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName} '/>
<c:set var="visiturl" value="yes" scope="session" />

   <div class="pageContainer">
   <jsp:include page="/tflyredesign/cookielegislation.jsp"></jsp:include>
     <div id="headerSection">
     <jsp:include page="header.jsp" /></div>
     <!--end of HeaderSection-->

     <div id="contentSection">

         <div id="breadcrumb">
            <ul class="contentBox horizontal breadCrumb">
				  <c:set var="breadcrumb_counter" value="${bookingComponent.nonPaymentData['breadcrumb_count']}"/>
				 <c:forEach begin="0" end="${breadcrumb_counter - 1}" varStatus="breadcrumb">
				     <c:set var="breadcrumbitemKey" value="breadcrumb_item_${breadcrumb.index}"/>
							 <c:set var="breadcrumblinkKey" value="breadcrumb_link_${breadcrumb.index}"/>


					   <c:choose>
			              <c:when test="${breadcrumb.index==4||breadcrumb.index==5}">

									<c:choose>
													   <c:when test="${breadcrumb.index==4}">
													   <li class="visited">
													      <c:out value="${bookingComponent.nonPaymentData[breadcrumbitemKey]}"/>
													     </li>
													     </c:when>
                                                        <c:otherwise>
																		     <li class="last"><c:out value="${bookingComponent.nonPaymentData[breadcrumbitemKey]}"/></li>
										                </c:otherwise>
                     </c:choose>
						  </c:when>
                         <c:otherwise>
						  <li class="visited">
                					      <a href="${bookingComponent.nonPaymentData[breadcrumblinkKey]}">
						 <c:out value="${bookingComponent.nonPaymentData[breadcrumbitemKey]}"/>	</a></li>
                        </c:otherwise>
                     </c:choose>

          		 </c:forEach>
            </ul>
         </div>
          <div class="titleSection" id="pageTitle">
            <h1>Payment</h1>
          </div>
          <!--left panel-->
          <div id="contentCol1">
            <jsp:include page="pricing.jsp"></jsp:include>
          </div>
          <!--end of left panel-->
       <div id="contentCol2">

         <form  id="SkySales" name ="SkySales" method="post" action="/cps/processPayment?b=15000&token=<c:out value='${param.token}' />&tomcat=<c:out value='${param.tomcat}' />">


           <%-- This section was added to solve the problem caused at the sinnerschrader end --%>

           <input type="hidden" name="tomcatInstance" id="tomcatInstance" value="${param.tomcat}" />
           <input type="hidden" name="token" id="token" value="${param.token}" />
			  <input	type="hidden" name="payment_0_street_address4" value="" />
           <input	type="hidden" name="payment_0_cardHoldersemailAddress" value="" />
        	<input	type="hidden" name="payment_0_cardHoldersemailAddress1" value="" />
     		<input	type="hidden" name="payment_0_cardHoldersmobilePhone" value="" />
     		<input	type="hidden" name="payment_0_cardHoldersdayTimePhone" value="" />
     		<input	type="hidden" name="payment_0_cardHolderAddress2" value="" />
           
           <!-- Payment -->
           <jsp:include page="paymentDetails.jsp"></jsp:include>
		   <jsp:include page="passengerDetails.jsp"></jsp:include>

           <c:choose>
              <c:when test="${bookingComponent.accommodationSummary != null &&
                                   bookingComponent.accommodationSummary.accommodationSelected == 'true'}">
                 <jsp:include page="tandc.jsp"></jsp:include>
              </c:when>
              <c:otherwise>
                 <jsp:include page="fareRules.jsp"></jsp:include>
              </c:otherwise>
           </c:choose>
           
           <!--Session Time out popup-->
           <div id="sessionTime" class="posFix">
				<div id="sessionTimeTextbox" class="sessionTimeText">
					<div class="session_dialog_title fl">
								 <div class="sessionText fl">Are you still there?</div>
								 <a style="color:#73afdc" class="remove fr" href="#"
									onClick="closesessionTime()">close</a>
					</div>
					<div class="session_dialog_timeOut_content_container fl">
							<div class="SesTimeoutImg fl">
									<div class="sessionoutWarnClock fl"></div>
									<div class="clear"></div>
									<span class="sesDisTime">Timeout in</span>
									<span id="sessiontimeDisplay" class="sesDisTime"></span>
									<div class="clear"></div>
							</div>
							<div class="session_dialog_timeOut_content" style="line-height:20px">
								<p style="font-size:14.5px">This page will soon be timing out for security reasons. Let us know that <br/> you're still here , otherwise you'll have to re-enter your details.</p>
							</div>
							<div class="clear"></div>
							<div class="sessionTimeTextbox_border-divider"></div>
					</div>
					<div class="activestate fr">
								 <a class="button fr" href="#"
									onClick="activestate()" style="color:#000;text-decoration: none;">I'm still here</a>
					</div>
				</div>
		   </div>
				
			<div class="posFix" id="sessionTimeout">
				<div class="sessionTimeOutText" id="sessionTimeOutTextbox">
					<div class="session_dialog_title fl">
						 <div class="sessionText fl">Sorry, this page has timed out!</div>
						 <a style="color:#73afdc" onclick="reloadPage()" class="remove fr">close</a>
					</div>
					<div class="session_dialog_timeOut_content_container fl">
						<div class="sessiontimeout_content fl" style="line-height:20px">
						Unfortunately this page has timed out for security reasons. Don't worry, we're not going to make you start over again, but we do need you to re-enter your details on this page. If you don't want to continue you can go to the <a style="color:#73afdc;text-decoration:none" href="#" onclick="homepage()">homepage</a>.
						</div>
						<div class="clear"></div>
						<div class="border-divider_sessiontimeout"></div>
					</div>
					<div class="activestate fr">
						 <a onclick="reloadPage()"  class="button fr">ok</a>
					</div>
				</div>
			</div>
			<div id="modal" class="web_dialog_overlay"></div>
           
           <div class="pageControls">
              <c:set var="backurl" value="${bookingComponent.prePaymentUrl}"/>
              <a class="backPage" href="<c:out value='${backurl}'/>" title="Back to passenger details">Back to passenger details</a>
              <c:if test="${bookingInfo.newHoliday}">
                <div class="buttonGroup">
	                 <c:choose>
						<c:when test="${requestScope.disablePaymentButton == 'true'}">
	                        <span class="forwardButtonOuter" style="background-image: url(/cms-cps/tflyredesign/images/flights-sprite-disabled.gif);">
		                        <span class="forwardButtonInner" style="background-image: url(/cms-cps/tflyredesign/images/flights-sprite-disabled.gif);">
		                        	<input style="cursor:default;" type="submit" id="bookingcontinue" value="Pay" class="primary" title="pay" disabled="disabled"/>
				                </span>
			                </span>
			            </c:when>
						<c:otherwise>
							<span class="forwardButtonOuter">
								<span class="forwardButtonInner">
									<input type="submit" id="bookingcontinue" value="Pay" class="primary" title="pay" onclick="return validateRequiredField()  && preventDoubleClick() && formSubmit()"/>
						    	</span>
						    </span>
						</c:otherwise>
					 </c:choose>
                
                </div>
              </c:if>
           </div>
         </form>
       </div>
       <!--end of contentCol2-->
     </div>
     <!--end of ContentSection-->
     <div id="footerSection"><jsp:include page="footer.jsp" /></div>
     <!--end of FooterSection-->
   </div>
   <!--end of PageContainer-->

   <script>
     if(PaymentInfo.chargeamt !=0) { displayFieldsWithValuesForCard(); }
   </script>
</body>
</html>
