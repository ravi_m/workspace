<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />

<h2>
   <img id="hl_pricing" src="/cms-cps/tfly/images/head_pricing_dkblauhg.gif"
      alt="Pricing" border="0">
</h2>
<div class="shadedBoxNormal">


<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <c:forEach var="priceComponent" items="${bookingComponent.priceComponents}">
     <tr>
        <td colspan="5">
           <h3 class="topspace">
           <c:choose>
              <c:when test="${fn:contains(priceComponent.itemDescription,'Outgoing')}">
                 <img id="goingOut" src="/cms-cps/tfly/images/head_departing_weisshg.gif"
                    alt="Departing (one-way)" border="0">
              </c:when>
              <c:when test="${fn:contains(priceComponent.itemDescription,'Round Trip')}">
                <img id="goingOut" src="/cms-cps/tfly/images/head_roundtrip_weisshg.gif"
                    alt="Departing (one-way)" border="0">
              </c:when>
              <c:when test="${fn:contains(priceComponent.itemDescription,'Extras')}">
                 <img id="goingOut" src="/cms-cps/tfly/images/subhl_extras_en.gif"
                    alt="Departing (one-way)" border="0">
              </c:when>
           </c:choose>
           </h3>
        </td>
     </tr>
     <c:if test="${priceComponent.itemDescription!='total Price' && priceComponent.itemDescription!='Outgoing' && priceComponent.itemDescription!='Extras'}">
     <tr>
        <c:choose>
           <c:when test="${fn:contains(priceComponent.itemDescription,'Taxes & charges')}">
              <td nowrap="" colspan="2"><c:out value="${priceComponent.itemDescription}" escapeXml="false"/></td>
              <c:set var="taxDetails" value=""/>
              <c:set var="counter" value="${bookingComponent.nonPaymentData['tax_count']}"/>
              <c:set var="outGoingRoundTripCaption" value="${bookingComponent.nonPaymentData['pricing_caption_0']}"/>
              <c:choose>
                 <c:when test="${fn:contains(outGoingRoundTripCaption,'Outgoing')}">
                    <c:set var="taxDetails" value="${taxDetails} (one-way):\n" />
                 </c:when>
                 <c:otherwise>
                    <c:set var="taxDetails" value="${taxDetails} (return):\n" />
                 </c:otherwise>
              </c:choose>
              <c:set var="taxDetails" value="${taxDetails} Fare Tax and Fees\n" />
              <c:forEach begin="0" end="${counter - 1}" varStatus="taxDsc">
                 <c:set var="dscKey" value="tax_dsc_${taxDsc.index}"/>


                 <c:set var="taxDescription" value="${bookingComponent.nonPaymentData[dscKey]}"/>
                 <c:set var="amountKey" value="tax_amt_${taxDsc.index}"/>

                 <c:choose>
                    <c:when test="${fn:contains(taxDescription,'Total Tax & Fees')}">
                       <c:set var="taxDetails" value="${taxDetails} \n" />
                    </c:when>
                 </c:choose>

                 <c:set var="taxDetails" value="${taxDetails} ${bookingComponent.totalAmount.symbol}" />
                		<c:set var="taxDetails" value="${taxDetails} ${bookingComponent.nonPaymentData[amountKey]}   " />
				 <c:set var="taxDetails" value="${taxDetails} ${taxDescription} \n" />
              </c:forEach>
              <td nowrap>
                 (<a href="javascript:feesTaxes_breakDowns('${taxDetails}')">details</a>)
              </td>
           </c:when>
           <c:otherwise>
              <td nowrap="" colspan="3">

			  <c:out value="${priceComponent.quantity}" escapeXml="false"/>

			   <c:choose>
                 <c:when test="${fn:contains(priceComponent.itemDescription,'Round Trip')}">
                 </c:when>
                 <c:otherwise>
                   <c:out value="${priceComponent.itemDescription}" escapeXml="false"/>
                 </c:otherwise>
              </c:choose>
				   </td>
		   </c:otherwise>
        </c:choose>

        <c:if test="${priceComponent.amount.amount>0}">
           <td><c:out value="${bookingComponent.totalAmount.symbol}" escapeXml="false"/></td>
           <td nowrap="" align="right">
              <fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2"
                 minFractionDigits="2" pattern="#,##,###.##"/>
           </td>
        </c:if>
     </tr>
     </c:if>
   </c:forEach>
  </TR>
  <tr>
     <td>&nbsp;</td>
  </tr>
  <tr>
     <td nowrap colspan="3" id='creditCardSurcharge'></td>
     <td id='currencySymbol'></td>
     <td align="right" id='surcharge' nowrap></td>
  </tr>
  <tr>
     <td>&nbsp;</td>
  </tr>
  <tr>
     <td nowrap="" colspan="3" id='totalPrice'><b>Total Price</b></td>
     <td><b><c:out value="${bookingComponent.totalAmount.symbol}" escapeXml="false"/></b></td>
     <td nowrap="" align="right" id='calculatedTotalAmount'>
        <strong>
           <fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
              minFractionDigits="2" pattern="#,##,###.##"/>
        </strong>
     </td>
  </tr>
</table>
</div>

<div class="shadedBoxNormalBottom"></div>