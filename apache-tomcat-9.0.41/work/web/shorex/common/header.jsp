    <div id="toyHeaderSection" xmlns:myXsltExtension="urn:XsltExtension">
    <div id="toyHeaderPanel">
    <a href="http://www.thomson.co.uk/" title="Thomson logo" alt="Thomson logo">
       <div id="toyHeaderLogo"></div>
    </a>
    <div class="clearb"></div>
    <div id="toyHeaderNavPanelTop"></div>
    <div id="toyHeaderNavPanelSides">
       <div id="toyHeaderNavPanelSidesPadder">
          <ul id="toyHeaderNavPanel">
             <li id="toyNavHolidays">
                <a href="http://www.thomson.co.uk/package-holidays.html">Package holidays</a>
             </li>
             <li id="toyNavFlights" class="selected">
                <a href="http://flights.thomson.co.uk/en/index.html">Flights</a>
             </li>
             <li id="toyNavHotels">
                <a href="http://www.thomson.co.uk/hotels/hotels.html">Hotels</a>
             </li>
             <li id="toyNavCities">
                <a href="http://www.thomson.co.uk/city-breaks/city-breaks.html">City breaks</a>
             </li>
             <li id="toyNavCruises"><a href="http://www.thomson.co.uk/cruise/cruise.html">Cruises</a></li>
             <li id="toyNavVillas"><a href="http://www.thomson.co.uk/villas/villa-holidays.html">Villas</a></li>
             <li id="toyNavDeals"><a href="http://www.thomson.co.uk/editorial/deals/deals.html">Deals</a></li>
             <li id="toyNavExtras"><a href="http://www.thomson.co.uk/editorial/extras/extras.html">Extras</a></li>
             <li id="toyNavDests"><a href="http://www.thomson.co.uk/editorial/destinations.html">Destinations</a></li>
          </ul>
          <div class="clearb"></div>
       </div>
    </div>
    <div id="toyHeaderNavPanelBottom"></div>
 </div>
 </div>
 <div id="topcontainer">
    <div id="bookingSteps">
       <img id="booking_step5" src="/cms-cps/tfly/images/bookingsteps_05.gif">
    </div>
 </div>