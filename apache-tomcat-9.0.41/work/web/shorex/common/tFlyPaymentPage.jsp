<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>
<jsp:useBean id="now" class="java.util.Date" />
	 <fmt:formatDate var ="currentyear" type="date" value="${now}" pattern="yyyy"/>
<%@include file="/common/intellitrackerSnippet.jspf"%>
<html>
 <head>
  <%@ page contentType="text/html; charset=UTF-8" %>
    <version-tag:version/>
    <title>Thomson Flights all the assurances and service you've come to expect from Thomson</title>
    <META NAME="description" content="Book great value flights with Thomson. Book your cheap flights, hotel accommodation, car hire, airport parking, transfers &amp; travel insurance through us." xmlns:myXsltExtension="urn:XsltExtension">
    <META NAME="keywords" content="Thomson Airways, Thomson flights, thomsonfly.com, thomsonfly, cheap flights, cheapflights, cheap, flights, flight, ticket, tickets, air fare, airfares, fly, low cost fare, low cost, air travel, airline, budget airlines, travel, holidays, hotels, tickets, bookings, book, booking, plane, airport, discount, air, europe, international, scheduled, charter, destinations, longhaul, carhire, car hire, car rental, insurance, thomson holidays, Thompson, tompsonfly, flythomson, tomson, thomsonflights, thompsonfly, thomsonflights, britannia airways, birmingham, bournemouth, coventry, cardiff, doncaster, edinburgh, east midlands, gatwick, glasgow, jersey, leeds bradford, liverpool, luton, manchester, newcastle, stansted, london, spain, majorca, alicante, barcelona, faro, grenoble, ibiza, jersey, lanzarote, malaga, minorca, menorca, naples, paris, paphos, palma, prague, pisa, marrakech, salzburg, tenerife, turkey, cancun, mexico, cuba, egypt, greece, cyprus, dominican republic, croatia, italy, morocco, canary islands, portugal" xmlns:myXsltExtension="urn:XsltExtension">
    <link rel="icon" href="/cms-cps/tfly/images/favicon.ico" type="image/x-icon" xmlns:myXsltExtension="urn:XsltExtension">
    <link rel="shortcut icon" href="/cms-cps/tfly/images/favicon.ico" type="image/x-icon" xmlns:myXsltExtension="urn:XsltExtension">
    <link rel="stylesheet" type="text/css" href="/cms-cps/tfly/css/matrix.css" xmlns:myXsltExtension="urn:XsltExtension">
    <link rel="stylesheet" type="text/css" href="/cms-cps/tfly/css/seven.css" />
	<script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>

    <script type="text/javascript" src="/cms-cps/tfly/js/Common.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
    <script type="text/javascript" src="/cms-cps/tfly/js/basic.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
    <script type="text/javascript" src="/cms-cps/tfly/js/vs_thf.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
    <script type="text/javascript" src="/cms-cps/tfly/js/tfly.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/prototype.js">	 </script>
	<script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
    <script type="text/javascript" src="/cms-cps/tfly/js/intellitracker.js"></script>
    <script type="text/javascript" language="javascript">
	//js variable to be used in the intellitracker pqry string.
         var previousPageVar ="${bookingComponent.nonPaymentData['prevPage' ]}";
	</script>



	<script type="text/javascript">
var  newHoliday  = "<c:out value='${bookingInfo.newHoliday}'/>";


 var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
 var token = "<c:out value='${param.token}' />" ;

var PaymentInfo = new Object();
PaymentInfo.totalAmount =${bookingComponent.totalAmount.amount};

PaymentInfo.payableAmount = ${bookingComponent.payableAmount.amount};
PaymentInfo.calculatedPayableAmount = ${bookingInfo.calculatedPayableAmount.amount};
PaymentInfo.calculatedTotalAmount = ${bookingInfo.calculatedTotalAmount.amount};
PaymentInfo.currency = "${bookingComponent.totalAmount.symbol}";

<fmt:formatNumber var="ccAmount" value="${bookingInfo.calculatedCardCharge.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>

PaymentInfo.chargeamt =${ccAmount};

PaymentInfo.selectedCardType = null;

var cardChargeMap = new Map();

   var pleaseSelect = new Array();
   cardChargeMap.put('none', pleaseSelect);
  <c:forEach var="cardCharges" items="${bookingInfo.allowedCards}">
  var cardChargeInfo = new Array();
    	<c:forEach var="cardChargesRate" items="${cardCharges.value}"  varStatus="status">
				   <c:choose>
		               <c:when test="${status.count == 4}">
					         <c:set var="CardName" value="${cardChargesRate}"/>
		               </c:when>
				       <c:otherwise>
            			    cardChargeInfo.push(<c:out value="${cardChargesRate}"/>);
							<c:set var="CardName" value="${cardCharges.key}"/>
						</c:otherwise>
		            </c:choose>
  </c:forEach>
		     cardChargeMap.put('<c:out value="${CardName}"/>', cardChargeInfo);
     </c:forEach>


var cardDetails = new Map();
 <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">
			    var cardCharge_Max_Min_Issue = new Array();
        	         	   cardCharge_Max_Min_Issue.push('${cardDetails.cardType.securityCodeLength}');
						    cardCharge_Max_Min_Issue.push('${cardDetails.cardType.minSecurityCodeLength}');
							 cardCharge_Max_Min_Issue.push('${cardDetails.cardType.isIssueNumberRequired}');

			  cardDetails.put('${cardDetails.paymentCode}',cardCharge_Max_Min_Issue);

         </c:forEach>

		window.onload= function(){ checkBookingButton("bookingcontinue");}
  var BookingConstants = {PAY_CLASS:"bookingcontinue", PAY_DESCRIPTION:"Continue"};
  </script>




 </head>

 <body>
<c:set var='clientapp' value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName} '/>
    <%

         String clientApp = ((String)pageContext.getAttribute("clientapp")).trim();

         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");

         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);


    %>
 <div id="wrapper">
 <jsp:include page="header.jsp"></jsp:include>
 <br clear="all"><div id="content"><h1>
 <img id="head_booking_step5" src="/cms-cps/tfly/images/head_booking05_blauhg.gif" alt="EXTRAS"></h1>
 <div id="help">
 <p>Please enter your payment information, check it carefully and click Pay and complete booking.
 Your booking will then be confirmed and any changes will incur an amendment fee.</p>


    <div style="display:block;margin-top:0px;margin-bottom:0px;">

       Fields marked * are mandatory.<BR><BR>Please enter passenger names exactly as they appear on the
       Photo-ID. For important information on Photo-ID please click
       <A href="http://flights.thomson.co.uk/en/flywithus_2807.html" target=_blank>here</A>.
       <BR><BR>Please read our
       <A onclick="window.open('http://flights.thomson.co.uk/en/popup_booking_conditions.html','coc','width=570,height=600,scrollbars')"
          href="javascript:void(0)">Booking Conditions
       </A>
       before proceeding.<BR><BR>
	   <c:choose>
			<c:when test="${bookingInfo.bookingComponent.totalAmount.currency.currencyCode == 'EUR'}">
						Accepted forms of payment: Visa Credit, MasterCard and Laser.
		</c:when>
		 <c:otherwise>
								Accepted forms of payment: Mastercard, Maestro, Visa, Visa Debit, Electron.
	   </c:otherwise>
		</c:choose>
       <P></P>

       <c:choose>
	<c:when test="${bookingInfo.bookingComponent.totalAmount.currency.currencyCode == 'EUR'}">
					  <IMG src="/cms-cps/tfly/images/visa_card.gif"><IMG src="/cms-cps/tfly/images/mastercard_card.gif"><IMG src="/cms-cps/tfly/images/laser_card.gif">
   </c:when>
		 <c:otherwise>
				 <IMG src="/cms-cps/tfly/images/payment_cards_DPFO.gif">
		 </c:otherwise>
	</c:choose>
       <BR><BR>
       Please note the Handling Fee in our dropdown menu when making payment on this website. <BR><BR>
       Please make sure all of the cardholder 's details are entered accurately so that our payment
       authorisation process can be completed and your booking confirmed.
    </div>
    </div>
    <div id="copy">
    <div id="copy_head"></div>
    <div id="copy_center">
       <noscript xmlns:myXsltExtension="urn:XsltExtension">
          <font class="error">
             ERROR:
             This service makes use of Javascript, which appears to be turned off.
             Click <a href='activateJavascript.htm' class='error'>here</a> to learn how to activate it.
         </font>
       </noscript>

	   <form  id="SkySales" name ="SkySales" method="post" action="/cps/processPayment?b=15000&token=<c:out value='${param.token}' />&tomcat=<c:out value='${param.tomcat}' />"  onsubmit="return validateRequiredField()  && preventDoubleClick() && formSubmit()">

<%-- This section was added to solve the problem caused at the sinnerschrader end --%>
     <input type="hidden" name="payment_0_street_address1" value="" />
     <input	type="hidden" name="payment_0_street_address2" value="" />
     <input	type="hidden" name="payment_0_street_address3" value="" />
     <input	type="hidden" name="payment_0_street_address4" value="" />
     <input	type="hidden" name="payment_0_cardHoldersemailAddress" value="" />
        <input	type="hidden" name="payment_0_cardHoldersemailAddress1" value="" />
     <input	type="hidden" name="payment_0_cardHoldersmobilePhone" value="" />
     <input	type="hidden" name="payment_0_cardHoldersdayTimePhone" value="" />
     <input	type="hidden" name="payment_0_cardHolderAddress2" value="" />
      <input type="hidden" name="payment_0_postCode" value="" />



	   <input type="hidden"
	name="tomcatInstance" id="tomcatInstance" value="${param.tomcat}" /> <input
	type="hidden" name="token" id="token" value="${param.token}" />
	   <div>
    </div>

  <c:if test='${error != "" && error != null}'>
     <table cellspacing="10" cellpadding="0" border="0" width="520" style="background: rgb(255, 255, 153) none repeat scroll 0% 0%; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial;">
     <tbody>
        <tr>
           <td><img src="/cms-cps/tfly/images/ani-error.gif" id="ani-error"/></td>
           <td width="470">
              <span>
                 <b><c:out value="${bookingComponent.errorMessage}" escapeXml="false"/></b>
              </span>
           </td>
        </tr>
     </tbody>
     </table>
	 <br/>
  </c:if>



     <jsp:include page="pricing.jsp"></jsp:include>
    <jsp:include page="passengerNames.jsp"></jsp:include>
    <jsp:include page="leadPassengerDetails.jsp"></jsp:include>
    <jsp:include page="paymentDetails.jsp"></jsp:include>
						   <c:choose>
						 <c:when test="${bookingComponent.accommodationSummary != null &&
                                bookingComponent.accommodationSummary.accommodationSelected == 'true'}">
										<jsp:include page="tandc.jsp"></jsp:include>
	                 </c:when>
				       <c:otherwise>
            			   				  <jsp:include page="fareRules.jsp"></jsp:include>
						</c:otherwise>
		            </c:choose>


    <div class="fullwidth" style="margin:20px 0 0 0;height:30px;">
       <div class="leftfloat" style="width: 33%; text-align: left;">

		  <c:set var="backurl" value="${bookingComponent.prePaymentUrl}"/>

		<a href="<c:out value='${backurl}'/>"   title="Back">
             <img src="/cms-cps/tfly/images/btn_back_dkblauhg.gif" alt="Back" border="0">
          </a>
       </div>

       <c:if test="${bookingInfo.newHoliday}">
	     <div class="leftfloat" style="width: 66%; text-align: right;">
           <input type="image" id="bookingcontinue" class="bookingcontinue"  src="/cms-cps/tfly/images/btn_continue_dkblauhg.gif" alt="Continue" border="0"/>

       </div>
       </c:if>
    </div>
</form>
</div>
<script>
if(PaymentInfo.chargeamt !=0) { displayFieldsWithValuesForCard(); }
</script>
<jsp:include page="footer.jsp"></jsp:include>
</div>
</body></html>
