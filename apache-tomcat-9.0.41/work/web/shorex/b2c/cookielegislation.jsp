<%@include file="/common/commonTagLibs.jspf"%>
<%
    String tandcLink=com.tui.uk.config.ConfReader.getConfEntry("B2CShorex.TermsofUse", "#");
	String privacyandpolicyLink=com.tui.uk.config.ConfReader.getConfEntry("B2CShorex.Privacypolicy", "#");
	String stmtlink=com.tui.uk.config.ConfReader.getConfEntry("B2CShorex.Statement", "#");
	
	String tuiHeaderSwitch=request.getParameter("paramSwitch");
	if("ON".equals(tuiHeaderSwitch)){
	tandcLink="https://www.tui.co.uk/destinations/info/website-terms-and-conditions";
	privacyandpolicyLink="https://www.tui.co.uk/destinations/info/privacy-policy";
	stmtlink="https://www.tui.co.uk/editorial/legal/statement-on-cookies-popup.html";
	}
%>
<link rel="stylesheet" type="text/css" href="/cms-cps/shorex/b2c/css/cookielegislation.css"/>
<script type="text/javascript">
function Popup(popURL,popW,popH,attr){
	   if (!popH) { popH = 450 }
	   if (!popW) { popW = 600 }
	   var winLeft = (screen.width-popW)/2;
	   var winTop = (screen.height-popH-30)/2;
	   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
	   popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
	   popupWin.close()


	      popupWin=window.open(popURL,"popupWindow",winProp);


	   popupWin.window.focus()
	}

function pnlReadOurTerms(){
var tomcatInstance= "<c:out value='${param.tomcat}' />" ;
	jQuery(document).ready(function(){
			jQuery.ajax({
			  type: "POST",
			url: "<%=request.getContextPath()%>/CookieLegislationServlet?b=<c:out value='${param.b}' />&tomcat="+tomcatInstance,
				  success: function(response){
jQuery('.cookie_bg').slideUp('slow');
			  }
			});
		});



	}
</script>

<c:choose>
<c:when test="${requestScope.cookieswitch=='true'}">


<c:choose>
<c:when test="${requestScope.cookiepresent != 'true'}">

<div id="fhpiCookieLegislation">
<div class="cookie_bg" style="z-index:1000;">
		<div><strong>Please read our terms of use</strong></div>
		<div class="w98 fLeft txt90">
			<div class="txtSlideContent">
			By clicking OK or continuing to use our website, you consent to our use of cookies and our <a href="javascript:void(0);"  onclick="Popup('<%=tandcLink%>',465,450,'scrollbars=yes,resizable=yes');" class="lnkContent">Website Terms and Conditions</a>, <a href="javascript:void(0);"  onclick="Popup('<%=privacyandpolicyLink%>',465,450,'scrollbars=yes,resizable=yes');" class="lnkContent">Privacy Policy</a> and <a href="javascript:void(0);"  onclick="Popup('<%=stmtlink%>',465,450,'scrollbars=yes,resizable=yes');" class="lnkContent">Statement on Cookies</a>. Please leave our website if you don't agree.
			</div>
		<span class="ok_button" onclick="javascript:pnlReadOurTerms();"><img class="floatedRight "
	     src="/cms-cps/shorex/b2c/images/fhpi_ok.gif" alt="OK" title="OK"/></span>

		</div>

</div>

</div>
<br clear="all" />



</c:when>
<c:otherwise>

</c:otherwise>

 </c:choose>

</c:when>
<c:otherwise>

</c:otherwise>

</c:choose>

