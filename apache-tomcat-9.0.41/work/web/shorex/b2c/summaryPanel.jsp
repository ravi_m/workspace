<%@ page isELIgnored="false" %>
<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />
<c:set var="currencySymbol" value="&pound;" scope="request"/>

<div id="contentCol1">
<div class="basketPanel">
    <h3>Your Shore Excursion</h3>
    <div class="basketContent">
        <!-- Basket Itinerary starts-->
		<c:forEach var="priceComponent"	items="${bookingComponent.priceComponents}" varStatus="itemStatus">
        	<div class="basketItinerary <c:if test='${itemStatus.first}'> noBorderTop</c:if>">

				<c:choose>
					<c:when test="${(fn:containsIgnoreCase(priceComponent.itemDescription,'adults'))||(fn:containsIgnoreCase(priceComponent.itemDescription,'Adult'))}">
						<span class="basketItineraryPersons"><c:out value="${priceComponent.itemDescription}" escapeXml="false" /></span>
					<c:if test="${not empty priceComponent.amount.amount}">
						<span class="basketItineraryPrice">
							<c:out value="${currencySymbol}" escapeXml="false" /><fmt:formatNumber value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##"></fmt:formatNumber>
						</span>
					
					</c:if>
					</c:when>
					<c:otherwise>
						<div class="dash <c:if test='${itemStatus.first}'> noBorderTop</c:if>" ></div>
						<span class="basketItineraryExcursion"><c:out value="${priceComponent.itemDescription}" escapeXml="false" /></span>
						
					</c:otherwise>
				</c:choose>
					
			</div>
		</c:forEach>
        <!-- Basket Itinerary ends-->

        <div class="totalCostContainer">
            <span class="basketTotalCostText">Total cost:</span>
            <span id="calculatedTotalAmount" class="basketTotalCost"><fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
              minFractionDigits="2" pattern="#,##,###.##"/></span><span 

class="basketTotalCost"> <c:out value="${bookingComponent.totalAmount.symbol}" escapeXml="false"/></span>
	<c:if test="${applyCreditCardSurcharge eq 'true'}">
			<p class="disclaimer">Price includes all applicable taxes &amp; charges</p>
			</c:if>
        </div>
        <div class="clearb"></div>

    </div>
</div>

<div class="clearb"></div>
<!-- Excursions Basket ends--></div>
