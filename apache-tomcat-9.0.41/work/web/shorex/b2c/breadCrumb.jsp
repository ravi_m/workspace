              <%@include file="/common/commonTagLibs.jspf"%>
<%@ page isELIgnored="false" %>
<c:if test="${not empty bookingComponent.nonPaymentData['breadcrumb_count']}">
<c:set var="breadcrumb_counter"
	value="${bookingComponent.nonPaymentData['breadcrumb_count']}" />
	<ul id="bookingFlowBar">
<c:forEach begin="0" end="${breadcrumb_counter - 1}"
	varStatus="breadcrumb">
	<c:set var="breadcrumbitemKey"
		value="breadcrumb_item_${breadcrumb.index}" />
	<c:set var="breadcrumblinkKey"
		value="breadcrumb_link_${breadcrumb.index}" />






	<c:choose>
		<c:when test="${breadcrumb.index==2||breadcrumb.index==3}">

			<c:choose>
				<c:when test="${breadcrumb.index==2}">
					<li class="current">
					<span class="three"><c:out
						value="${bookingComponent.nonPaymentData[breadcrumbitemKey]}" /></span></li>
				</c:when>
				<c:otherwise>
					<li class="last"><c:out
						value="${bookingComponent.nonPaymentData[breadcrumbitemKey]}" /></li>
				</c:otherwise>
			</c:choose>
		</c:when>
		<c:otherwise>
			<li ><span class="one">
			
				<c:out value="${bookingComponent.nonPaymentData[breadcrumbitemKey]}" /> </span></li>
		</c:otherwise>
	</c:choose>

</c:forEach>
  </ul>
  </c:if>