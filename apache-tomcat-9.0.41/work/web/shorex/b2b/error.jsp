<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      <title>Payments - Error!!</title>

	  <link rel="stylesheet" type="text/css" href="/cms-cps/shorex/b2b/css/reset.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/shorex/b2b/css/global.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/shorex/b2b/css/flexibox.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/shorex/b2c/css/payment_details.css"/>
      <link rel="stylesheet" type="text/css" href="/cms-cps/shorex/b2b/css/footer.css"/>
      <!--[if IE 7]>
      <link rel="stylesheet" type="text/css" href="/cms-cps/thomson/byoredesign/css/styles-ie7.css"/>
      <![endif]-->
      <link rel="shortcut icon" href="/cms-cps/shorex/b2c/images/favicon.ico"/>
   </head>
   <body>
      <div class="pageContainer" id="payments">
	     <%@include file="header.jspf" %>
		 <div id="contentSection">
		    <%@include file="breadCrumb.jspf" %>
			<h1>Passenger &amp; payment details</h1>
            <p class="intro">Your holiday booking is nearly complete</p>
		    <div id="contentCol2">
			   <div class="flexiBox greyContainer">
			      <div class="top"><span class="border">&nbsp;</span></div>
				  <div class="middle">
				     <span class="shadow">&nbsp;</span>
					 <div class="body">
                        <c:choose>
                           <c:when test="${not empty bookingInfo.trackingData.sessionId}">
                              <%@include file="techDifficulties.jspf"%>
                           </c:when>
                           <c:otherwise>
                              <%@include file="sessionTimeOut.jspf"%>
                           </c:otherwise>
                        </c:choose>
			         </div>
			      </div>
				  <div class="bottom"><span class="border">&nbsp;</span></div>
			   </div>
		    </div>
            <div id="contentCol1">
			   <div class="flexiBox leftPanel">
			   </div>
	        </div>
			<div id="footerSection">
               <%@include file="footer.jsp"%>
            </div>
         </div>
      </div>
   </body>
</html>