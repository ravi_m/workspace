<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>
<h2>
   <img id="hl_farerules" src="/cms-cps/tfly/images/head_farerules_dkblauhg.gif"
      alt="Fare rules" border="0">
</h2>
<div class="control" id="fareRules">
    <ul>
       <li>Original Fares are valid for the original route and named passengers only.</li>
       <li>Any changes must be made via the Call Centre up to 3 hours before departure, subject to availability.</li>
       <li>Changes to date or time of travel will incur a charge of GBP15/EUR25 per passenger per segment and will be priced at the prevailing fare less a discount based on the number of days prior to departure that the change is made.(0% of original booking value upto 7 days, 10% if 8-21 days, 30% if 22-49 days, 50% if 50+ days).</li>
       <li>Changes to passenger names are permitted only if the change applies to all sectors of the relevant booking. Such changes will incur a GBP25/EUR40 fee per passenger per segment plus any increase between the original and prevailing fare.</li>
       <li>There are no refunds or credits for missed flights or for cancellations.</li>
       <li>There is no refund if the price of a changed flight is lower than the prevailing price.</li>
    </ul>
 </div>
 <div class="shadedBoxAttn">
    <table border="0" cellpadding="0" cellspacing="2">
       <tr>
          <td valign="top">
             <input id="termAndCondition"
                type="checkbox" name="termAndCondition">	    <input id="tourOperatorTermsAccepted"
                type="hidden" name="tourOperatorTermsAccepted">
          </td>
          <td valign="top">
             <p>
                I have read and accept Thomson's
                <a href="javascript:void(0);" onclick="window.open('http://flights.thomson.co.uk/en/popup_booking_conditions.html','conditions','width=570,height=550,scrollbars');">Booking Conditions</a>,
                <a href="javascript:void(0);" onclick="window.open('http://flights.thomson.co.uk/en/popup_coc.html','COC','width=570,height=550,scrollbars');">Conditions of Carriage</a>,
                <a href="javascript:void(0);" onclick="window.open(' ${bookingComponent.nonPaymentData['legal_info_url' ]} '
				,'legal','width=570,height=550,scrollbars');">Legal Information</a> &amp;
                <a href="javascript:void(0);" onclick="window.open('http://flights.thomson.co.uk/en/633.html','privacy','width=570,height=550,scrollbars');">Privacy Policy</a>.
             </p>
             <strong> Please tick this box and continue.</strong>
          </td>
       </tr>
    </table>
 </div>
 <div class="shadedBoxAttnBottom"></div>