<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
 <div class="headerContainer">
    <img class="tuiLogo" src="/cms-cps/brac/images/tui_logo.gif" alt="TUI" />
  <c:set var="url" value='${bookingComponent.clientDomainURL}'/>
            <div class="loginBox">
            <ul class="thin">
              <li>
                 <p><span>Welcome:&nbsp;</span> <c:out value="${bookingComponent.shopDetails.shopName}"/></p>
              </li>
                <c:if test="${bookingComponent.searchType == 'CSS Mode'}">
                <li id="css_mode">
                <p class="css_mode_warning">You are currently in CSS mode</p>
        </li>
        </c:if>
              <li>
                 <p><span>Shop ID:&nbsp;</span><c:out value="${bookingComponent.shopDetails.shopId}"/></p>
              </li>
              <li>
                   <form action="/page/login/logout.page" name="logoutForm">
                     <a href="<c:out value='${url}'/>/brac/page/login/logout.page"> Logout</a>
                   </form>
              </li>
                 <li>&nbsp;</li>
           <li>
                    <p><span>Retailer Name:&nbsp;</span><c:out value="${bookingComponent.shopDetails.retailBrand}"/></p>
                  </li>
                  <li>
                     <p><span>Shop / Reference:&nbsp;</span>
					 <c:choose>
					 <c:when test="${not empty bookingComponent.retailBookingReference}">
					 <c:out value="${bookingComponent.retailBookingReference}"/>
					 </c:when>
					 <c:otherwise>
					 <c:out value="${bookingComponent.bookingData.bookingReference}"/>
					 </c:otherwise>
					 </c:choose>
           </p>
                  </li>
                  <li>
                     <p><span>Booking Creation Date:&nbsp;</span> <fmt:formatDate value="${bookingComponent.bookingData.bookingDate}" pattern="dd-MMM-yyyy" /></p>
                  </li>
                  <li>
                     <p><span>Booking Status:</span>
                       <c:out value="${bookingComponent.bookingData.bookingStatus}"/>
                        </p>
                  </li>
            </ul>
        </div> <%--  end loginbox --%>

    <div class="clearb"></div>
 </div>  <%-- End headerContainer --%>
