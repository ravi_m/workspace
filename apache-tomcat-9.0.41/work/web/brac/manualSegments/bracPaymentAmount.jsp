<%@include file="/common/commonTagLibs.jspf"%>
<%@page import="java.util.Date"%>
<%
         pageContext.setAttribute("todayDate", new Date());
%>
<c:set var='clientapp' value='${bookingComponent.clientApplication.clientApplicationName}'/>
<%
	String clientApp = (String)pageContext.getAttribute("clientapp");
    String minimumPayableAmount = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".minimumPayableAmount" , "");
    pageContext.setAttribute("minimumPayableAmount", minimumPayableAmount, PageContext.REQUEST_SCOPE);
%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var = "currencysymbol" value="${bookingComponent.totalAmount.symbol}" scope="page"/>
<fmt:formatDate var="todayDateVal" pattern="dd-MMM-yyyy" value="${todayDate}" />

 <div class="paymentPanel">
       <h4>Payment Amount</h4>
 <c:forEach var="depositComponent" varStatus="status" items="${bookingComponent.depositComponents}">
  <c:if test="${depositComponent.depositType == 'fullCost'}">
     <script>
       var fullBalanceAmount = <c:out value="${depositComponent.depositAmount.amount}" />;
       maxPayable = <c:out value="${depositComponent.depositAmount.amount}" />;
     </script>
       <c:set var="newBalance" value ="${depositComponent.outstandingBalance.amount}"/>
       <fmt:formatDate var="balanceDueDateVal" pattern="dd-MMM-yyyy" value="${depositComponent.depositDueDate}" />
  </c:if>
</c:forEach>
         <%--***********************--%>
       <p>Your total holiday cost is
         <span class="highLight">
            <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${holidayCost}" />&nbsp;(Excluding Post Paid, charges and compensations)
        </span>
      </p>
       <c:if test="${totalCharges >= 0}">
          <p>Total charges payable <span class="highLight"><fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${totalCharges}" />&nbsp;(Compensation Included)
        </span>
        </p>
         </c:if>
         <c:if test="${totalCharges < 0}">
           <p>Compensation <span class="highLight"><fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${totalCharges}" />&nbsp;(Charges Included)</span></p>
         </c:if>

         <p>
            The outstanding balance of your holiday is&nbsp;
            <span class="highLight">
               <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${outstandingBalance}" />
               <c:if test="${outstandingBalance != newBalance}">
                 &nbsp;
                 (<fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${newBalance}" />&nbsp;Inclusive of charges & Compensations)
                 &nbsp;
               </c:if>
            </span>
         </p>
         <c:choose>
            <c:when test="${todayDateVal == balanceDueDateVal }">
               <p>The full balance is due today.</p>
            </c:when>
            <c:otherwise>
               <p>The full balance is due on <c:out  value="${balanceDueDateVal}" />.</p>
            </c:otherwise>
         </c:choose>
         <h5>&nbsp;Payment/Refund will include any outstanding deposits, charges, compensations and balance due.</h5>
         <table>
            <colgroup>
               <col width="80%" />
               <col width="20%" />
            </colgroup>
            <tbody>

              <c:forEach var="depositComponent" varStatus="status" items="${bookingComponent.depositComponents}">
				<c:set var="depositAmount" value="${depositComponent.depositAmount.amount}"/>
				<c:set var="depositType" value="${depositComponent.depositType}"/>
               <!-- Start Minimum Payment -->
               <c:if test="${depositType == 'MinimumAmount'}">
                  <tr class="first">
                     <td>
                        <label for="paymentAmountTypeMinimum">Minimum Payment: </label><c:out value="${currencysymbol}" /><c:out value="${depositAmount}" />
                     </td>
                     <td>
                        <input id="paymentAmountTypeMinimum" name="depositType" type="radio" value="<c:out value='${depositType}'/>" onclick="setSelectedOption('<c:out value='${depositType}'/>');"/>
                     </td>
                  </tr>
               </c:if>
               <!-- End Minimum Payment -->
               <!-- Start Pay in Part -->
               <c:if test="${depositType == 'partialDeposit'}">
					<c:choose>
						<c:when test="${depositAmount > 0.0}">
							<c:set var = "partialDepositAmount" value="${depositAmount}"/>
						</c:when>
						<c:otherwise>
							<c:set var = "partialDepositAmount" value="${minimumPayableAmount}"/>
						</c:otherwise>
					</c:choose>
                  <tr class="first">
                    <c:choose>
				    <c:when test="${partialDepositAmount > 0}">
                     <td>
                        <label for="paymentAmountTypePartialDeposit">Pay in Part (Your minimum payment is  <c:out value="${currencysymbol}" /><c:out value='${partialDepositAmount}'/> and includes any charges, deposits or balances due today)</label>
                     </td>
					 </c:when>
					 <c:otherwise>
					   <td>
                        <label for="paymentAmountTypePartialDeposit">Pay in Part</label>
                     </td>
					 </c:otherwise>
					 </c:choose>
                     <td>
                        <input id="paymentAmountTypePartialDeposit" name="depositType" type="radio" value="<c:out value='${depositType}'/>" onclick="setSelectedOption('<c:out value='${depositType}'/>');"/>
                     </td>
                  </tr>
                   <script>
					minPayable = <c:out value="${partialDepositAmount}" />;
              	   </script>
               </c:if>
               <!-- End Pay in Part -->

             <c:if test="${depositType == 'REFUND'}">
                  <tr class="first">
                     <td>
                        <label for="paymentAmountTypeRefund">Refund:</label>
                     </td>
                     <td>
                        <input id="paymentAmountTypeRefund" name="depositType" type="radio" value="Refund" onclick="setSelectedOption('<c:out value='${depositType}'/>');"/>
                     </td>
                  </tr>
               </c:if>

             <c:if test="${depositType == 'fullCost'}">
               <tr class="first">
                  <td>
                      <label for="paymentAmountTypeFull">Full Amount:</label>
                      <c:choose>
                       <c:when test ="${depositAmount < 0}">
					     <c:set var="amount" value="${depositAmount * -1}"/>
						 -<c:out value="${currencysymbol}" /><c:out value="${amount}" />
					   </c:when>
					   <c:otherwise>
                         <c:out value="${currencysymbol}" /><c:out value="${depositAmount}" />
			           </c:otherwise>
			        </c:choose>
                  </td>
                  <td>
                     <input id="paymentAmountTypeFull" name="depositType" type="radio" value="<c:out value='${depositType}'/>" onclick="setSelectedOption('<c:out value='${depositType}'/>');" <c:if test="${!depositPayable}">checked="checked"</c:if>/>
                  </td>
               </tr>
             </c:if>
              </c:forEach>
            </tbody>
         </table>
         <p> Every time you use your credit card, the credit card company charges us a fee. Unfortunately this means we have to charge you a credit card fee to cover the costs, which is added onto your booking at the payment stage. But if you want a way round this, then why not use your debit card? There are no handling fees on bookings made with Maestro (formerly Switch), Visa Electron, Solo or Delta.</p>
      </div>
       <c:if test="${bookingComponent.bookingData.reaccreditationAllowed}">
        <div class="paymentPanel">
         <h4>Change Consultant</h4>
          <div class="paymentPanelTotals">

              <ul>
              <li class="long">
           <label for="employeeNumber">Changed To</label>
                 <input type="text" name="userId"  id="consultantId" value="" onkeyup="If_showReaccreditationData()" />

              </li>
           <li class="long">
          <label for="loginUsername">Authorized By</label>
                <input type="text" name ="authenticationUserID" id ="authenticationUserID" value="" disabled/>

              </li>
          <li>
        <div class="clearb"></div>
                 <label for="employeeNumber">Password:</label>
                 <input type="password" name="authenticationPassword"  id="authenticationPassword" value="" disabled/>

              </li>
          </ul>

     </div>
     </c:if>
