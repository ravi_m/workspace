<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ page isELIgnored ="false" %>
<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<%--********search the request first ifnot there search the session***************--%>
<c:set var = "currencysymbol" value="${bookingComponent.totalAmount.symbol}" scope="page"/>
<div class="sidePanelContainer">
    <div class="mainMenuPanel">
       <ul>
       </ul>
  </div>
   <%--*************** displaying the pricecomponents**************** --%>
   <div class="costCustomerPanel">
       <h4>Cost to Customer </h4>
       <ul>
         <c:set var = "compensationAmount" value = "0"  scope ="request"/>
        <c:forEach var="depositComponent"  items="${bookingComponent.depositComponents}">
                         <c:if test ="${depositComponent.depositType == 'deposit'}">
                          <c:set var="dueDate" value="${depositComponent.depositDueDate}"/>
                         </c:if>
                        <c:if test ="${depositComponent.depositType == 'fullCost'}">
                          <c:set var="fullbaldueDate" value="${depositComponent.depositDueDate}"/>
                         </c:if>
            </c:forEach>
       <c:forEach var="costingLine" varStatus="status" items="${bookingComponent.priceComponents}">
             <c:if test = "${costingLine.itemDescription == 'Credit Card Charges'}">
        <c:set var = "cardCharges" value ="${costingLine.amount.amount}" scope= "page"/>
        </c:if>
            <c:if test = "${costingLine.itemDescription =='PostPayGBP'}">
        <c:set var = "accomcost" value ="${costingLine.amount.amount}" scope= "page"/>
        </c:if>

         <c:if test = "${costingLine.itemDescription =='HOPLA_EURO'}">
        <c:set var = "eurocost" value ="${costingLine.amount.amount}" scope= "page"/>
        </c:if>

           <c:if test = "${costingLine.itemDescription == 'CompensationChequeAmount'}">
                <c:set var = "compensationAmount" value = "${costingLine.amount.amount}"  scope ="request"/>
             </c:if>
        </c:forEach>

              <c:forEach var="costingLine" varStatus="status" items="${bookingComponent.priceComponents}">
      <c:if test="${costingLine.itemDescription != 'Total Charges' && costingLine.itemDescription != 'Outstanding Balance' && costingLine.itemDescription != 'TOTAL_HOLIDAY_COST' && costingLine.itemDescription !='CompensationChequeAmount' && costingLine.itemDescription !='HOPLA_EURO' }">
               <c:if test="${costingLine.itemDescription != 'Total' && costingLine.itemDescription != 'PostPayGBP' && costingLine.itemDescription != 'Balance'}" >
          <c:choose>
            <c:when test="${costingLine.itemDescription =='Flight Cost'|| costingLine.itemDescription == 'ACCOMMODATION Cost'
                 }">

          <c:if test="${costingLine.itemDescription == 'ACCOMMODATION Cost'}" >

           <li>
                      <span class="descaccom">
                         <c:out value="${costingLine.itemDescription}" /> :
                      </span>
            </li>

           <li>
                 <span>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           <c:if test ="${costingLine.amount.currency =='EUR'}"> <fmt:formatNumber type="currency" currencySymbol= "&euro;"
             value="${costingLine.amount.amount}" /></c:if>

           <c:if test ="${costingLine.amount.currency =='GBP'}"> <fmt:formatNumber type="currency" currencySymbol= "&pound;"
             value="${costingLine.amount.amount}" /></c:if>
               <c:if test="${not empty bookingComponent.accommodationSummary.prepay && !bookingComponent.accommodationSummary.prepay}">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${accomcost}" />
                                          )&nbsp;**   </c:if>
            </span>
                   </li>
          </c:if>
            <c:if test="${costingLine.itemDescription != 'ACCOMMODATION Cost'}" >
            <li>
                      <span class="descaccom">
                         <c:out value="${costingLine.itemDescription}" /> :
                      </span>
            </li>
            <li>
                 <span>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${costingLine.amount.amount}" />
            </span>
                   </li>
           </c:if>
           </c:when>
                   <c:when test="${costingLine.itemDescription =='HOPLA CHARGE'}">
             <li>
                       <span class="descsupplier">
                         Supplier Charge :
                      </span>
                      <span class="cost">
                  <fmt:formatNumber type="currency" currencySymbol= "${currencysymbol}" value="${costingLine.amount.amount}"/> <c:if test="${not empty bookingComponent.accommodationSummary.prepay && !bookingComponent.accommodationSummary.prepay}">
           &nbsp;(<fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${eurocost}" />
                                          )&nbsp;**   </c:if>
            </span>
                   </li>
           </c:when>
                  <c:when test="${costingLine.itemDescription =='Deposit Due'}">
             <li>
                       <span class="desc">
                         <c:out value="${costingLine.itemDescription}" />:
                      </span>
                      <span class="cost">
               <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${costingLine.amount.amount}" />
            </span>
                   </li>
                   <c:if test ="${dueDate!=null}">
                   <li><span class="desc">Deposit Due Date:</span>
               <span class="cost">
                      <fmt:formatDate pattern="dd-MMM-yyyy"  value="${dueDate}" />
                   </span>
           </c:if>
         </c:when>
   <c:when test="${costingLine.itemDescription =='Full Balance'}">
    <li>
                       <span class="desc">
                         <c:out value="${costingLine.itemDescription}" />:
                      </span>
                      <span class="cost">
               <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${costingLine.amount.amount}" />
            </span>
                   </li>
      <c:if test ="${fullbaldueDate!=null && costingLine.amount.amount >0}">
              <li><span class="desc">Full Bal Due Date:</span>
      <span class="cost">
        <fmt:formatDate pattern="dd-MMM-yyyy"  value="${fullbaldueDate}" />
        </span>
      </c:if>
        </li>
                 </c:when>
            <c:otherwise>
             <c:if test="${costingLine.itemDescription == 'Amount Payable'}">
             <li><span class="desc" style="width:110px;">
                         <c:out value="${costingLine.itemDescription}" />:
                      </span>
                       <span class="cost">
               <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${costingLine.amount.amount}" />
            </span>
                   </li>
                      </c:if>

                  <c:if test="${costingLine.itemDescription != 'Amount Payable'&& costingLine.itemDescription != 'INSURANCE Cost' && costingLine.itemDescription != 'Thomson Charge'  &&  costingLine.itemDescription != 'Compensation'}" >
               <li>

                      <span class="desc">
                         <c:out value="${costingLine.itemDescription}" />:
                      </span>
                      <span class="cost">
               <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${costingLine.amount.amount}" />
            </span>
                   </li>
           </c:if>
           <c:if test="${costingLine.itemDescription == 'Compensation' || costingLine.itemDescription == 'Thomson Charge'}">
             <li>
                      <span class="desc">
                         <c:out value="${costingLine.itemDescription}" />: </span>
                      <span class="cost">&nbsp;
          <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${costingLine.amount.amount}" />
            </span>
                   </li>
           </c:if>
                      <c:if test="${costingLine.itemDescription == 'INSURANCE Cost'}" >
             <li>
                      <span class="descaccom">
                         <c:out value="${costingLine.itemDescription}" /> :
                      </span>
            </li>
             <li>
                      <span >
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${costingLine.amount.amount}" />
            </span>
                   </li>
           </c:if>
                     </c:otherwise>
         </c:choose>
               </c:if>
               <c:if test="${costingLine.itemDescription == 'Total' || costingLine.itemDescription == 'Balance'}">
               <%--*************** The below logic is added for displaying holiday essentials.**************** --%>
                <c:if test="${costingLine.itemDescription == 'Total' && not empty bookingComponent.pricingDetails}">
                 <li>
                  <c:forEach var="pricingDetails" items="${bookingComponent.pricingDetails}">
                   <span class="pricingDetailsHeadingdesc"><c:out value="${pricingDetails.key}"/>: </span>
	                <c:forEach var="priceComponent" items="${pricingDetails.value}">
	                  <span class="pricingDetailsdesc" ><c:out value="${priceComponent.itemDescription}"/></span>
                      <span class="pricingDetailsCost"><fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${priceComponent.amount.amount}" /></span>
	                </c:forEach>
                  </c:forEach>
                </li>
               </c:if>
               <%--*************** End of the logic for displaying holiday essentials.**************** --%>
                   <li class="divider">
                       <span class="desc">
                           <c:out value="${costingLine.itemDescription}" /> :
                       </span>
                       <c:choose>
                         <c:when test ="${costingLine.amount.amount < 0}">
					       <c:set var="amount" value="${costingLine.amount.amount * -1}"/>
						   <span class="cost">
                           -<fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${amount}" />
                           </span>
					     </c:when>
					     <c:otherwise>
                          <span class="cost">
                            <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${costingLine.amount.amount}" />
                          </span>
			             </c:otherwise>
			          </c:choose>
                  </li>
        </c:if>
        </c:if>
           </c:forEach>
      <br clear="all"/>
       <br/>
             <c:if test="${cardCharges > 0}">
       <li>
               <span>* Excludes credit card charges</span>
             <span class="cost">&nbsp;</span>
           </li>
        </c:if>
      <c:if test="${not empty bookingComponent.accommodationSummary.prepay && !bookingComponent.accommodationSummary.prepay}">
           <li>
             <span>** Post Pay</span>
             <span class="cost">&nbsp;</span>
           </li>
       </c:if>
      <c:if test="${not empty bookingComponent.importantInformation.description['NOTE']}">
    <li>
      <span class="desc" style="width: 30px;color:red">Note:</span>
      <span style='color:red'> <c:out value ="${bookingComponent.importantInformation.description['NOTE']}"></c:out></span>
    </li>
  </c:if>
       </ul>
       <div class="clearb"></div>
    </div><%--****** End of costCustomerPanel***** --%>
 </div><%--******** End of sidePanelContainer******--%>

