<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<%@ page import="com.tui.uk.payment.domain.PaymentTransaction,
                 java.util.List,
                 com.tui.uk.payment.domain.DataCashPaymentTransaction"%>
<script type="text/javascript">
    var transamt="Transaction Amount";
    var minPayable;
    var maxPayable;
    var transamtalert="Please enter a valid Transaction Amount";
    var voucheralert="Please enter a valid Voucher Code";
    var tottransamt="Total amount charged in this transaction:";
    var cashaccept="Cash Accepted";
    var cheqaccept="Cheque Accepted";
    var vouchercode="Voucher Code";
    var vouchervalue="Voucher Value";
    var addanothervoc="Add another voucher";
    var voucheraccept="Voucher Accepted";
    var amountpaid="Amount Paid";
    var amountstilldue="Amount still due";
    var totalamountdue="Total Amount due";
    var cardaccept="Card Payment Accepted";
    var poundsymbol="�";
    var totalvouchervalue="Total Voucher Value";
    var vocamtalert="Please Enter Valid Voucher Amount"
    var exceedalert="Amount entered exceeds total amount due. Please re-enter";
    var lessalert = "Amount entered should be less then minimum amount. Please re-enter";
    var cardcode = "Auth code";
    var isHopla = false;
    var amtRec= "";
    var responseMsg = "";
	var maskedCardNumber = "";
	var maskedCvv = "";
  var paymenttypeoptions = new Array();
  var paymenttypeoptionsHopla = new Array();
  var payment_CP = new Array();
  var payment_CNP = new Array();
  var payment_CPNA = new Array();
  var payment_PPG = new Array();
  var refund_Card = new Array();
  var payment_startYear = new Array();
  var payment_expiryYear = new Array();
  var requiredFields_cp='';
  var requiredFields_cnp=''
  var requiredFields_cpna='';
  var requiredFields_ppg='';
  var requiredFields_rfndCard='';
  var requiredFields_startYear ='';
  var requiredFields_expiryYear ='';
    var searchType='<c:out value="${bookingComponent.searchType}"/>' ;
    var isReaccreditationallowed = <c:out value="${bookingComponent.bookingData.reaccreditationAllowed}"/> ;
    var callType='<c:out value="${bookingComponent.callType}"/>' ;
    var currencySymbol='';

    var PaymentInfo = new Object();
    //Let us give default values as null
    PaymentInfo.amtPaid = 0.0;
    PaymentInfo.amtDue = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
    PaymentInfo.payableAmount = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
    PaymentInfo.basePayableAmount = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
    PaymentInfo.payableAmountWithOutCardCharges = null;
    PaymentInfo.selectedCardType = null;
    PaymentInfo.pinpadCardType = null;
    PaymentInfo.chargePercent = null;
    PaymentInfo.accumulatedCardCharge = 0;
    PaymentInfo.selectedIndex = 1;
    PaymentInfo.currentIndex = 0;
    PaymentInfo.depositType = null;
    PaymentInfo.totalAmount = '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>';
    PaymentInfo.calculatedPayableAmount = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
    PaymentInfo.calculatedTotalAmount = '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>';
    PaymentInfo.calculatedDiscount = null;
    PaymentInfo.maxCardChargeAmount = null;
    PaymentInfo.calculatedDiscount = null;
    PaymentInfo.totalTransactions = 1;
    PaymentInfo.totalCardCharge = null;

    var cardChargeMap = new Map();
  cardChargeMap.put('PleaseSelect', 0);
  <c:forEach var="cardCharges" items="${bookingInfo.cardChargeMap}">
    cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');
  </c:forEach>
  var depositAmountsMap = new Map();
  <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}">
  	var depositValue = '<c:out value="${depositComponent.depositType}"/>';
   	depositAmountsMap.put(depositValue.toUpperCase(), '<c:out value="${depositComponent.depositAmount.amount}"/>');
  </c:forEach>
  depositAmountsMap.put('FULLCOST', '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>');
  var bookingConstants = {FULL_COST:"FULLCOST", TOTAL_CLASS:"total", PAYABLEAMOUNT_CLASS:""};
   var isHopla= false;
   var additionaltotamount ='<c:out value="${bookingComponent.totalAmount.amount}"/>';
 var calculatedpayableamount = <c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>;
  <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_PEG'}">
        isHopla = true;
    </c:if>
</script>
<div id="paymentFields_required"><input type='hidden' id='panelType' value='CP'/></div>
  <script type="text/javascript">
  <c:forEach var="paymentType" items="${bookingComponent.paymentType['CP']}">
    payment_CP.push("<option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/> '><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> <\/option>");
    requiredFields_cp+="<c:out value="<input type='hidden' value='${bookingComponent.cardChargesMap[paymentType.paymentCode]}'  name='payment_charge_${paymentType.paymentCode}' id='${paymentType.paymentCode}_charges' />" escapeXml="false"/>"+
    "<c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled'  name='payment_code_${paymentType.paymentCode}' />" escapeXml="false"/>"+
    " <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo'  />" escapeXml="false"/>"+
    " <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>"+
    " <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>";
  </c:forEach>
 <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
  <c:choose>
  <c:when test="${bookingInfo.calculatedTotalAmount.amount > 0.0}">
  payment_CNP.push("<option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> <\/option>");
  requiredFields_cnp+="<c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled' name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>"+
  " <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>"+
  " <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>"+
  " <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>";
 </c:when>
<c:otherwise>
  <c:if test="${paymentType.paymentMethod != 'Dcard'}">
  payment_CNP.push("<option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> <\/option>");
  requiredFields_cnp+="<c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled' name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>"+
  " <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>"+
  " <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>"+
  " <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>";
  </c:if>
</c:otherwise>
</c:choose>
</c:forEach>
    <c:forEach var="paymentType" items="${bookingComponent.paymentType['CPNA']}">
    <c:choose>
    <c:when test="${bookingInfo.calculatedTotalAmount.amount > 0.0}">
      payment_CPNA.push("<option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/> '><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> <\/option>");
        requiredFields_cpna+="<c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled'  name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>"+
        " <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>"+
        " <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>"+
        " <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>";
        </c:when>
        <c:otherwise>
          <c:if test="${paymentType.paymentMethod != 'Dcard'}">
          payment_CPNA.push("<option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> <\/option>");
          requiredFields_cnp+="<c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled' name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>"+
          " <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>"+
          " <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>"+
          " <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>";
          </c:if>
        </c:otherwise>
        </c:choose>
    </c:forEach>
    <c:forEach var="refundCard" varStatus="status" items="${bookingComponent.refundDetails}">
      refund_Card.push("<option value='<c:out value='${refundCard.datacashReference}' escapeXml='false'/>|<c:out value='${refundCard.transactionAmount}' escapeXml='false'/> '><c:out value="${refundCard.transactionDate}" escapeXml='false'/> <\/option>");
    </c:forEach>
    <c:forEach var="paymentType" items="${bookingComponent.paymentType['PPG']}">
      payment_PPG.push("<option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/> '><c:out value="${paymentType.paymentDescription}" escapeXml='false'/> <\/option>");
        requiredFields_ppg+="<c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled'  name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>"+
        " <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>"+
        " <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>"+
        " <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>";
    </c:forEach>
    requiredFields_startYear += "<option value=''>YY<\/option>";
    <c:forEach var="startYear" items="${bookingInfo.startYearList}">
      requiredFields_startYear += "<option value='<c:out value='${startYear}'/>'><c:out value='${startYear}'/><\/option>"
    </c:forEach>
    payment_startYear.push(requiredFields_startYear);
    requiredFields_expiryYear += "<option value=''>YY<\/option>";
    <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
      requiredFields_expiryYear += "<option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/><\/option>"
    </c:forEach>
    payment_expiryYear.push(requiredFields_expiryYear);
  </script>



<%--Setting the payment transaction details into script variables for refer transaction--%>
<script type="text/javascript">
<%
if (request.getAttribute("paymentTransactions") != null)
{
 for(PaymentTransaction paymentTransaction : (List<PaymentTransaction>)request.getAttribute("paymentTransactions"))
 {
 if (paymentTransaction instanceof DataCashPaymentTransaction)
 {
  pageContext.setAttribute("paymentTransaction",paymentTransaction);
 %>
    responseMsg = '<c:out value="${transactionResponseMsg}" />';
    var cardType = '<c:out value="${paymentTransaction.card.cardtype}" />';
	var cardNumber = '<c:out value="${pandata}" />';
	maskedCardNumber  = "";
	for (var i = 0; i < cardNumber.length; i++)
    {
       if (i <  cardNumber.length - 4)
        {
	      maskedCardNumber  = maskedCardNumber  + '*';
        }
       else
        {
	      maskedCardNumber  =  maskedCardNumber  + cardNumber.charAt(i);
        }
    }
    var name = '<c:out value="${paymentTransaction.card.nameOncard}" />';
    var postCode = '<c:out value="${paymentTransaction.card.postCode}" />';
    var expiryDate = '<c:out value="${paymentTransaction.card.expiryDate}" />';
    var securitycode = '<c:out value="${securitycodedata}" />';
	maskedCvv = '';
	for (var i = 0; i < securitycode.length; i++)
	{
	  maskedCvv  = maskedCvv  + '*';
	}
    var issueNumber = '<c:out value="${paymentTransaction.card.issueNumber}" />';
    <% }}}%>
    var typeOfCustomer = '<c:out value="${bookingComponent.nonPaymentData['customer_Type']}"/>';
    var selectedDepositType =
    	'<c:out value="${bookingComponent.nonPaymentData['depositType']}"/>';
</script>

<div class="paymentPanel">
         <h4>Payment Details</h4>
<%--*****************Refund related section**********************--%>
<c:if test="${bookingInfo.calculatedTotalAmount.amount <0.0 }">
   <script type="text/javascript">
       isRefund = true;
   </script>
</c:if>
<%--*****************End of refund related section****************--%>
        <input type="hidden" name="datacashEnabled" id="datacashEnabled" value="<c:out value='${bookingComponent.datacashEnabled}'/>"/>
   <c:choose>
    <c:when test="${bookingComponent.datacashEnabled == false }">
    <c:if test="${bookingComponent.callType == 'callCenter'}">
       <div id="payment_type_header" >
         <div id="c1" class="cust_present">
          <input type="radio" id="cp" name="customer_Type" value="payment_CP" onclick="javascript:setCustomerType(this.value)"/>
          <label  class="payment_type_header_label" > <span class="fontstyle">Customer present</span></label>
         </div>
         <div id="c2" class="cust_not_present">
          <input type="radio" id="cnp" name="customer_Type" value="payment_CNP" onclick="javascript:setCustomerType(this.value)" />
          <label class="payment_type_header_label"><span class="fontstyle"> Customer not present </span></label>
         </div>
         <div id="c3" class="chip_pin_not_avail">
          <input type="radio" id="cpna" name="customer_Type" value="payment_CPNA" onclick="javascript:setCustomerType(this.value)"/>
          <label class="payment_type_header_label"><span class="fontstyle gray_text"> Chip-and-pin not available</span></label>
         </div>
       </div>
  </c:if>
    <c:if test="${bookingComponent.callType != 'callCenter'}">
       <div id="payment_type_header" >
         <div id="c1" class="cust_present">
          <input type="radio" id="cp" name="customer_Type" value="payment_CP" onclick="javascript:setCustomerType(this.value)"/>
          <label  class="payment_type_header_label" > <span class="fontstyle gray_text">Customer present</span></label>
         </div>
         <div id="c2" class="cust_not_present">
          <input type="radio" id="cnp" name="customer_Type" value="payment_CNP" onclick="javascript:setCustomerType(this.value)" />
          <label class="payment_type_header_label"><span class="fontstyle gray_text"> Customer not present </span></label>
         </div>
         <div id="c3" class="chip_pin_not_avail">
          <input type="radio" id="cpna" name="customer_Type" value="payment_CPNA" onclick="javascript:setCustomerType(this.value)"/>
          <label class="payment_type_header_label"><span class="fontstyle gray_text"> Chip-and-pin not available</span></label>
         </div>
       </div>
       </c:if>
    </c:when>

    <c:otherwise>
       <div id="payment_type_header" >
         <div id="c1" class="cust_present">
          <input type="radio" id="cp" name="customer_Type" value="payment_CP" onclick="javascript:setCustomerType(this.value)"/>
          <label  class="payment_type_header_label" > <span class="fontstyle">Customer present</span></label>
         </div>
         <div id="c2" class="cust_not_present">
          <input type="radio" id="cnp" name="customer_Type" value="payment_CNP" onclick="javascript:setCustomerType(this.value)" />
          <label class="payment_type_header_label"><span class="fontstyle"> Customer not present </span></label>
         </div>
         <div id="c3" class="chip_pin_not_avail">
          <input type="radio" id="cpna" name="customer_Type" value="payment_CPNA" onclick="javascript:setCustomerType(this.value)"/>
          <label class="payment_type_header_label"><span class="fontstyle"> Chip-and-pin not available</span></label>
         </div>
       </div>
    </c:otherwise>
  </c:choose>
         <c:set var="iterCount"  value="${bookingComponent.noOfTransactions}"/>
         <div class="paymentPanelNumTrans">
            <label for="paymentNumTransactions">How many transactions do you wish to make?</label>
            <input id="preTrans" type="hidden" value="0" name="preTrans"/>
            <select onchange="javascript:noOfPaymentsHandler(this.value);" id="paymentTrans" name="payment_totalTrans">
               <c:forEach begin="1" end="${iterCount}" varStatus="status">
                    <c:out value="<option value=${status.count}>${status.count}</option>" escapeXml="false"/>
                </c:forEach>
            </select>
         </div>

    <div id="paymentSelections" class="payment_maincont"></div>
    <div id="hoplaDisplay"></div>

   <br clear="all"/>
    <div id="paymentAccumlation" class="paymentPanelTotals"></div>
</div>

       <script type="text/javascript">
       var newHoliday = "<c:out value='${bookingInfo.newHoliday}'/>";
       if (parseFloat(document.getElementById("totalAmountDue").value) == 0)
   {
      document.getElementById("totalAmountDue").value = <c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>;
   }
   amtRec = document.getElementById("totalAmountDue").value;
   window.onload=
   function()
   {
          onLoadPaymentHandler();
		  //This is for refer transaction CR
		  if(responseMsg != null && responseMsg != "")
          {
            setToPreviousSelection();
          }
		  else
		  {
         initializeDepositSelection();
		  }

   //checkBookingButton();
  }
  window.onunload=function()
  {
    transactionStopped = true;
    moATS.Reset;
    moATS.Close;
  }
</script>
<div id='refundCaption' style='display:none' class='refundCaption'>
<input type="hidden" id="refundValue"/>
              </div>
              <br/><br/><br/>
<%--********maximum card charge and applicable charge cap******--%>
<input  name="cardDetailsFormBean.cardCharge" id="cardCharge" value="0.0" type="hidden"/>
<input  name="cardDetailsFormBean.cardLevyPercentage" id="cardLevyPercentage" value="0.0" type="hidden"/>
 <%--*******Payment type header with radio buttons********--%>
   <input type="hidden" name="datacashEnabled" id="datacashEnabled" value="<c:out value='${bookingComponent.datacashEnabled}'/>"/>
<input  name="updateFlag" id="updateFlag" value="false" type="hidden"/>
<%--********Following fields contain maximum charge allowed******--%>
<input type="hidden" name="applicablecardcharge" id="applicablecardcharge" value='<c:out value="${bookingComponent.maxCardCharge}"/>'/>
<input type="hidden" name="applicablecapvalue" id="applicablecapvalue" value='<c:out value="${bookingComponent.maxCardChargePercent}"/>'/>
<%--********end of maximum card charge and applicable charge cap*******--%>

<input  type='hidden' id='totCalculatedAmount'  value='<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>'/>
<input  type='hidden' id='totamtpaidWithoutCardCharge'  name='payment_amtwithdisc' value='<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>'/>
<input type="hidden" name="total_transamt" id="total_transamt" value='' />
<input type="hidden" name="total_cardChargeAmt" id="total_cardChargeAmt" value='' />
<input type='hidden' id='clientApplication' value='<c:out value="${bookingComponent.clientApplication.clientApplicationName}"/>'/>
<input  type='hidden' id='payment_pinpadTransactionCount' name='payment_pinpadTransactionCount'/>
<input type='hidden' id='payment_pinpadTransactionAmount' name='payment_pinpadTransactionAmount'/>
<%-- Added for obtaining card type and processing it starts here --%>
<script type="text/vbscript">
    Dim trans_Index

    Sub moATS_CardInserted(CardType)
       Dim amountWithCardCharge
       Dim pinpadCardCharge
       prepayTextOne="Payment by this card type incurs additional administrative charge of "
       prepayTextTwo=" please ensure customer accepts this additional cost"
       document.getElementById("cardType"&trans_Index).value = CardType
       CardType = Replace(Trim(CardType)," ","_",1,1)
       transAmt = RmChr(document.getElementById("transamt"&trans_Index).value, "�")
       PaymentInfo.pinpadCardType = CardType
       PaymentInfo.chargePercent = cardChargeMap.get(CardType)
       pinpadCardCharge = ((transAmt * PaymentInfo.chargePercent)/100)
       if (1*pinpadCardCharge > 1*PaymentInfo.maxCardChargeAmount) then
          pinpadCardCharge = 1 * PaymentInfo.maxCardChargeAmount
       end if
       document.getElementById("pinpadCardNumber"&trans_Index).style.display = "block"
       document.getElementById("pinpadCardNumber"&trans_Index).innerHTML = "<input type='hidden' name='payment_"&trans_Index&"_cardNumber' id='payment_"&trans_Index&"_cardNumber'/>"
       document.getElementById("payment_"&trans_Index&"_cardNumber").value = moATS.CardNumber
       document.getElementById("pinpadMsgDetails"&trans_Index).style.display = "block"
       document.getElementById("pinpadCardDetails"&trans_Index).style.display = "block"
       document.getElementById("pinpadCardDetails"&trans_Index).innerHTML = "Card Type : "&CardType
       document.getElementById("pinpadMsgDetails"&trans_Index).innerHTML = prepayTextOne&currencySymbol&FormatNumber(1*pinpadCardCharge,2)&prepayTextTwo
       cardExpiryStatus=checkCardValidity(trans_Index)
       if cardExpiryStatus <> True then
          amountWithCardCharge = transAmt + pinpadCardCharge
          document.getElementById("payment_pinpadTransactionAmount").value = amountWithCardCharge
          moATS.AcceptCard(amountWithCardCharge)
          cardChargesDisplay = cardChargeSectionConfig(trans_Index, FormatNumber(1*pinpadCardCharge,2))
          document.getElementById("cardCharges"&trans_Index).innerHTML = cardChargesDisplay
          document.getElementById("tottransamt"&trans_Index).innerHTML = currencySymbol&FormatNumber(1*amountWithCardCharge,2)
          document.getElementById("payment_"&trans_Index&"_chargeAmount").value = FormatNumber(1*pinpadCardCharge,2)
       end if
    End Sub

    Public Function setIndex(byVal index)
        trans_Index = index
    End Function

    Public Function acceptCard(byVal calculatedAmount)
        amountWithCardCharge = calculatedAmount
    End Function

    Private Function RmChr(byVal string, byVal remove)
        Dim i, j, tmp, strOutput
        strOutput = ""
        for j = 1 to len(string)
            tmp = Mid(string, j, 1)
            for i = 1 to len(remove)
                tmp = replace( tmp, Mid(remove, i, 1), "")
                if len(tmp) = 0 then exit for
            next
            strOutput = strOutput & tmp
        next
        RmChr = strOutput
    End Function
</script>
