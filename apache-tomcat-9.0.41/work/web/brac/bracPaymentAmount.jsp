<%@include file="/common/commonTagLibs.jspf"%>
<%@page import="java.util.Date"%>
<%
         pageContext.setAttribute("todayDate", new Date());
%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
    <c:set var = "currencysymbol" value="${bookingComponent.totalAmount.symbol}" scope="page"/>

<fmt:formatDate var="todayDateVal" pattern="dd-MMM-yyyy" value="${todayDate}" />

 <div class="paymentPanel">
       <h4>Payment Amount</h4>
       <%--***********************--%>
       <script type="text/javascript">
       var maxTransactions = <c:out value="${bookingComponent.noOfTransactions}"/>;
       </script>
<c:forEach var="depositComponent" varStatus="status" items="${bookingComponent.depositComponents}">

 <c:if test="${depositComponent.depositType == 'ADDITIONAL'}">
 <script>
 var additionalPaymentOptionBalanceDue = <c:out value="${depositComponent.depositAmount.amount}" />;
 </script>
 </c:if>
      <c:if test="${depositComponent.depositType == 'REFUND'}">
         <script>
        var refundPaymentOptionBalanceDue = <c:out value="${depositComponent.depositAmount.amount}" />;
         </script>
       </c:if>
       <c:if test="${depositComponent.depositType == 'Deposit'}">
                <script>
               var depositOptionBalanceDue = <c:out value="${depositComponent.depositAmount.amount}" />;
                </script>

        </c:if>
         <c:if test="${depositComponent.depositType == 'New Deposit'}">
                <c:set var="depositOptionDue" value ="${depositComponent.depositAmount.amount}"/>
       <c:set var="newDepositAmount" value ="${depositComponent.outstandingBalance.amount}"/>
         </c:if>
        <c:if test="${depositComponent.depositType == 'Full Balance'}">
              <script>
              var fullBalanceAmount = <c:out value="${depositComponent.depositAmount.amount}" />;

              </script>
                <c:set var="newBalance" value ="${depositComponent.outstandingBalance.amount}"/>
             <fmt:formatDate var="balanceDueDateVal" pattern="dd-MMM-yyyy" value="${depositComponent.depositDueDate}" />
        </c:if>
</c:forEach>


         <%--***********************--%>
       <p>Your total holiday cost is
         <span class="highLight">
            <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${holidayCost}" />&nbsp;(Excluding Post Paid, charges and compensations)
        </span>
      </p>
       <c:if test="${totalCharges >= 0}">
          <p>Total charges payable <span class="highLight"><fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${totalCharges}" />&nbsp;(Compensation Included)
        </span>
        </p>
         </c:if>
         <c:if test="${totalCharges < 0}">
           <p>Compensation <span class="highLight"><fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${totalCharges}" />&nbsp;(Charges Included)</span></p>
         </c:if>

         <p>
            The outstanding balance of your holiday is&nbsp;
            <span class="highLight">
               <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${outstandingBalance}" />
               <c:if test="${outstandingBalance != newBalance}">
                 &nbsp;
                 (<fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${newBalance}" />&nbsp;Inclusive of charges & Compensations)
                 &nbsp;
               </c:if>
            </span>
         </p>
         <c:choose>
            <c:when test="${todayDateVal == balanceDueDateVal }">
               <p>The full balance is due today.</p>
            </c:when>
            <c:otherwise>
               <p>The full balance is due on <c:out  value="${balanceDueDateVal}" />.</p>
            </c:otherwise>
         </c:choose>
         <h5>&nbsp;Payment/Refund will include any outstanding deposits, charges, compensations and balance due.</h5>
         <table>
            <colgroup>
               <col width="80%" />
               <col width="20%" />
            </colgroup>
            <tbody>

              <c:forEach var="depositComponent" varStatus="status" items="${bookingComponent.depositComponents}">

                  <c:if test="${depositComponent.depositType == 'ADDITIONAL'}">
                  <tr class="first">
                     <td>
                        <label for="paymentAmountTypeAdditional">Additional Payment:</label>
                     </td>
                     <td>
                        <input id="paymentAmountTypeAdditional" name="depositType" type="radio" value="Additional" onclick="setSelectedOption('<c:out value='${depositComponent.depositType}'/>');"/>
                     </td>
                  </tr>
               </c:if>
                     <c:if test="${depositComponent.depositType == 'Low Deposit'}">
                         <input  name="payLowDepositOnly"  id ="payLowDepositOnly" value="true" type="hidden"/>
                        <tr class="first">
                     <td>
                        <label for="paymentAmountTypeLowDeposit">New Low Deposit(Optional):&nbsp; <fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${depositComponent.outstandingBalance.amount}" />
                       </label>
                     </td>
                     <td>
                        <input id="paymentAmountTypeLowDeposit" name="depositType" type="radio" value="Low Deposit" onclick="setSelectedOption('<c:out value='${depositComponent.depositType}'/>');"/>
                     </td>
                  </tr>
               </c:if>

             <c:if test="${depositComponent.depositType == 'REFUND'}">
                  <tr class="first">
                     <td>
                        <label for="paymentAmountTypeRefund">Refund:</label>
                     </td>
                     <td>
                        <input id="paymentAmountTypeRefund" name="depositType" type="radio" value="Refund" onclick="setSelectedOption('<c:out value='${depositComponent.depositType}'/>');"/>
                     </td>
                  </tr>
               </c:if>
               <c:if test="${depositComponent.depositType == 'Deposit'}">
                                    <input  name="payDepositOnly"  id ="payDepositOnly" value="true" type="hidden"/>
                <c:set var="depositPayable" value="true"/>
                  <tr class="first">
                     <td>
                        <label for="paymentAmountTypeDeposit">Deposit Due:&nbsp;<fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${depositOptionDue}" />&nbsp;(New Deposit&nbsp;<fmt:formatNumber type="currency" currencySymbol="${currencysymbol}" value="${newDepositAmount}" />)
                          </label><label>
                        &nbsp;Due Date &nbsp;<fmt:formatDate pattern="dd-MMM-yyyy"  value="${depositComponent.depositDueDate}" />
                        </label>
                     </td>
                     <td>
                        <input id="paymentAmountTypeDeposit" name="depositType" type="radio" value="Deposit" checked="checked" onclick="setSelectedOption('<c:out value='${depositComponent.depositType}'/>');"/>
                     </td>
                  </tr>
               </c:if>

             <c:if test="${depositComponent.depositType == 'Full Balance'}">
               <tr>
                  <td>
                      <label for="paymentAmountTypeFull">Full Balance:</label>
                      <c:if test="${depositComponent.depositAmount.amount >0}">
             <label>&nbsp;&nbsp; Due Date &nbsp;<fmt:formatDate pattern="dd-MMM-yyyy"  value="${depositComponent.depositDueDate}" /></label></c:if>
                  </td>
                  <td>
                     <input id="paymentAmountTypeFull" name="depositType" type="radio" value="Full Balance" onclick="setSelectedOption('Full Balance');" <c:if test="${!depositPayable}">checked="checked"</c:if>/>
                  </td>
               </tr>
             </c:if>
              </c:forEach>
            </tbody>
         </table>
         <p> Every time you use your credit card, the credit card company charges us a fee. Unfortunately this means we have to charge you a credit card fee to cover the costs, which is added onto your booking at the payment stage. But if you want a way round this, then why not use your debit card? There are no handling fees on bookings made with Maestro (formerly Switch), Visa Electron, Solo or Delta.</p>
      </div>
       <c:if test="${bookingComponent.bookingData.reaccreditationAllowed}">
        <div class="paymentPanel">
         <h4>Change Consultant</h4>
          <div class="paymentPanelTotals">

              <ul>
              <li class="long">
           <label for="employeeNumber">Changed To</label>
                 <input type="text" name="userId"  id="consultantId" value="" onkeyup="If_showReaccreditationData()" />

              </li>
           <li class="long">
          <label for="loginUsername">Authorized By</label>
                <input type="text" name ="authenticationUserID" id ="authenticationUserID" value="" disabled/>

              </li>
          <li>
        <div class="clearb"></div>
                 <label for="employeeNumber">Password:</label>
                 <input type="password" name="authenticationPassword"  id="authenticationPassword" value="" disabled/>

              </li>
          </ul>

     </div>
     </c:if>
