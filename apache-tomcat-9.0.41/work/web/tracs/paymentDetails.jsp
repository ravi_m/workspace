   <div class="section">
            <h2 id="paymentDetails"><span>Payment details</span></h2>

            <div class="sectionContent paymentDetails">
              <p class="intro"><strong>There is no addtional charge when paying by Visa Debit, Visa Electron, Maestro or Solo.</strong>
              <br />For American Express, MasterCard and Visa Credit, the credit card charge amount shown will be added to the total amount debited from your card.</p>

              <ul id="paymentOptionDetails" class="controlGroups">

<%-----------------------Amount payable section------------------------------------------------------%>
              <c:set var="fieldControlGroupDisplayStyle" value=""/>
              <c:set var="fieldDisplayStyle" value="hide"/>

              <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'totalAmountPayable')}">
                  <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
                  <c:set var="fieldDisplayStyle" value=""/>
              </c:if>
                <li id="amountPayableControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
                  <p class="fieldLabel"><label for="amountPayable">Total amount payable:</label></p>

                  <div class="controls">
                    <p class="totalAmount" id="totalAmountPayable"><c:out value="${currency}" escapeXml="false"/><fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##"/></p>
                    <p id="totalAmountPayableCaption" class="additionalNote hide">(include credit card charges)</p>
                  </div>
                </li>
             <%------------------*****END Amount payable section*****---------------------------------------------%>
             <%-----------------------Card type section----------------------------------------------------------%>
             <c:set var="fieldControlGroupDisplayStyle" value=""/>
             <c:set var="fieldDisplayStyle" value="hide"/>

             <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'CardType')}">
                     <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
                     <c:set var="fieldDisplayStyle" value=""/>
              </c:if>
                <li id="cardTypeControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
                  <p class="fieldLabel"><label for="cardType">Card type:</label></p>

                  <div class="controls">
                    <select name="payment_type_0" id="cardType">
                      <option value="">Please select</option>
                          <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
                <option value="<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>">
                  ${paymentType.paymentDescription}
                  </option>
                </c:forEach>
                    </select>


          <%-----Show different card type image based on brands -----%>
                    <c:set var="cardTypeStyle" value="GB"/>
                    <c:set var="cardTypeTitle" value="We accept Visa, Delta, MasterCard, Maestro, Solo and American Express."/>

                    <span class="cardType <c:out value='${cardTypeStyle}'/>" title="<c:out value='${cardTypeTitle}'/>"><c:out value='${cardTypeTitle}'/></span>
          <%---**END of showing card type images**---%>
                  </div>
                </li>
                <%------------------*****Card type section*****-----------------------------------------------------%>
        <%-----------------------Card number section--------------------------------------------------------%>
                <c:set var="fieldControlGroupDisplayStyle" value=""/>
                <c:set var="fieldDisplayStyle" value="hide"/>

                <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'CardNumber')}">
                    <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
                    <c:set var="fieldDisplayStyle" value=""/>
               </c:if>
                <li id="cardNumberControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
                <p class="fieldLabel"><label for="cardNumber">Card number:</label></p>

                   <div class="controls">
                     <input type="text" name="payment_0_cardNumber" id="cardNumber" class="longField" maxlength="20" onblur='removeSpaces(this)' autocomplete="off"/>
                   </div>
                </li>
        <%------------------*****END Card number section*****-----------------------------------------------%>

                <%-----------------------Expiry date section--------------------------------------------------------%>
                <c:set var="fieldControlGroupDisplayStyle" value=""/>
                <c:set var="fieldDisplayStyle" value="hide"/>

                <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'ExpiryDate')}">
                       <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
                       <c:set var="fieldDisplayStyle" value=""/>
                </c:if>

                <li id="expiryMonthControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
                <p class="fieldLabel"><label for="expiryDate">Expiry date:</label></p>

                  <div class="controls">
                    <select name="payment_0_expiryMonth" id="expiryMonth">
                      <c:forEach begin="1" end="12" varStatus="loopStatus">
                        <fmt:formatNumber var="expiryMonth" value="${loopStatus.count}" type="number" minIntegerDigits="2" pattern="##"/>
                        <option value="<c:out value="${expiryMonth}"/>"><c:out value="${expiryMonth}" /></option>
                      </c:forEach>
                   </select>

                    <select name="payment_0_expiryYear" id="expiryYear">
                      <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
                        <option value='<c:out value='${expiryYear}'/>'>20<c:out value='${expiryYear}'/></option>
                      </c:forEach>
                    </select>
                  </div>
                </li>
                <%------------------*****END Expiry date section*****-----------------------------------------------%>

        <%-----------------------Name on card section-------------------------------------------------------%>
                <c:set var="fieldControlGroupDisplayStyle" value=""/>
                <c:set var="fieldDisplayStyle" value="hide"/>

                <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'CardName')}">
                     <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
                     <c:set var="fieldDisplayStyle" value=""/>
                </c:if>

                <li id="nameOnCardControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
                <p class="fieldLabel"><label for="nameOnCard">Name on card:</label></p>

                  <div class="controls">
                    <input type="text" name="payment_0_nameOnCard" id="nameOnCard" class="longField" maxlength="80" onblur='removeSpaces(this)' autocomplete="off"/>
                  </div>
                </li>
                <%------------------*****END Name on card section*****----------------------------------------------%>

        <%-----------------------Issue number section-------------------------------------------------------%>
        <c:set var="fieldControlGroupDisplayStyle" value=""/>
                <c:set var="fieldDisplayStyle" value="hide"/>

                <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'IssueNumber')}">
                   <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
                   <c:set var="fieldDisplayStyle" value=""/>
               </c:if>

                <li id="issueNumberControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
                <p class="fieldLabel"><label for="issueNumber">Issue number:</label></p>

                  <div class="controls">
                    <input type="text" name="payment_0_issueNumber" id="issueNumber" maxlength="2" autocomplete="off" />
                    <p>(Leave blank if not on your card)</p>
                  </div>
                </li>
                <%------------------*****Issue number section*****--------------------------------------------------%>

        <%-----------------------Card security section------------------------------------------------------%>
                <c:set var="fieldControlGroupDisplayStyle" value=""/>
                <c:set var="fieldDisplayStyle" value="hide"/>

                <c:if test="${fn:containsIgnoreCase(bookingInfo.errorFields,'SecurityCode')}">
                   <c:set var="fieldControlGroupDisplayStyle" value="inError"/>
                   <c:set var="fieldDisplayStyle" value=""/>
                </c:if>

                <li id="cardSecurityCodeControlGroup" class="controlGroup <c:out value="${fieldControlGroupDisplayStyle}"/>">
                <p class="fieldLabel"><label for="cardSecurityCode">Card security code:</label></p>

                  <div class="controls">
                    <input type="text" name="payment_0_securityCode" id="cardSecurityCode" maxlength="4" onblur='removeSpaces(this)' autocomplete="off" />

                    <p><a id="cardSecurityCodeLink" href="#cardSecurityCodeLinkOverlay" class="popupLink blinkyOwner">The last 3 digits on the reverse of your card:</a></p>
                  </div>
                </li>
        <%------------------*****END Card security section*****---------------------------------------------%>
              </ul><%--END paymentOptionDetails --%>
            </div><!-- END sectionContent -->
          </div><!-- END section -->