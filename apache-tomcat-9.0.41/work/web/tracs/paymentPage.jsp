<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>Payment Details</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link href="/cms-cps/tracs/css/main.css" rel="stylesheet" type="text/css" />
  <!--[if lte IE 6]>
    <link href="/cms-cps/tracs/css/ie6.css" rel="stylesheet" type="text/css" />
  <![endif]-->

   <%--JS files --%>
  <script src="/cms-cps/common/js/prototype.js" type="text/javascript"></script>
  <script src="/cms-cps/common/js/utilFunctions.js" type="text/javascript"></script>
  <script src="/cms-cps/common/js/cardCharges.js" type="text/javascript"></script>
  <script src="/cms-cps/common/js/commonAjaxCalls.js" type="text/javascript"></script>

  <script src="/cms-cps/tracs/js/tracsPayment.js" type="text/javascript"></script>
  <script src="/cms-cps/tracs/js/formValidation.js" type="text/javascript"></script>
  <script src="/cms-cps/tracs/js/browserdetect_lite.js" type="text/javascript"></script>
  <script src="/cms-cps/tracs/js/prototype-tui-util.js" type="text/javascript"></script>
  <script src="/cms-cps/tracs/js/prototype-tui-overlay.js" type="text/javascript"></script>

  <link type="image/x-icon" href="/cms-cps/tracs/images/icons/favicon.ico" rel="icon" />
  <link type="image/x-icon" href="/cms-cps/tracs/images/icons/favicon.ico" rel="shortcut icon" />
</head>

<body>
<div class="wrapper">
  <%@include file="header.jspf"%>

  <div id="contentContainer">
    <div class="mainContent">
      <div id="pageTitle" class="titleSection">
        <h1>Make a payment</h1>
      </div>

      <div class="mainContentTop">
        <span class="outer">&nbsp;</span>
        <span class="inner">&nbsp;</span>
      </div>

      <div class="mainContentMiddle">
        <form action="/cps/processPayment?token=<c:out value='${param.token}'/>&b=22000&tomcat=<c:out value='${param.tomcat}' />" method="post" onsubmit="return payNowButtonClicked();">
          <%@include file="configSettings.jspf"%>
          <%@include file="pageErrors.jspf"%>
          <%@include file="paymentDetails.jsp"%>
          <%@include file="buttonGroup.jspf"%>
          <%@include file="cardSecurityCodeOverlay.jspf" %>
        </form>
      </div><!-- END mainContentMiddle -->

      <div class="mainContentBottom">
        <span class="outer">&nbsp;</span>
        <span class="inner">&nbsp;</span>
      </div>
    </div><!-- END mainContent -->
  </div><!-- END #contentContainer -->
</div><!-- END wrapper -->

<%--------------------------------Include Footer --------------------------------------%>
                        <%@include file="footer.jspf"%>
<%---------------------------*****End Footer*****--------------------------------------%>


<%--------------------------------Include onload functions --------------------%>
<script type="text/javascript">
   TuiUtil.addLoadEvent(initialisePaymentEvents);
   //TuiUtil.addLoadEvent(function(){processingErrors('<c:out value="${bookingInfo.errorFields}"/>','<c:out value="${bookingComponent.errorMessage}"/>')});
</script>
<%---------------------------*****End Scripts*****----------------------------------------------------%>
</body>
</html>
