<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>Payment Details - Errors</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link href="/cms-cps/tracs/css/main.css" rel="stylesheet" type="text/css" />
  <!--[if lte IE 6]>
    <link href="/cms-cps/tracs/css/ie6.css" rel="stylesheet" type="text/css" />
  <![endif]-->
  <script src="/cms-cps/common/js/utilFunctions.js" type="text/javascript"></script>
    
  <link type="image/x-icon" href="/cms-cps/tracs/images/icons/favicon.ico" rel="icon" />
  <link type="image/x-icon" href="/cms-cps/tracs/images/icons/favicon.ico" rel="shortcut icon" />
</head>
<body>
<div class="wrapper">
  <%@include file="header.jspf"%>

  <div id="contentContainer">
    <div class="mainContent">
      
     
      <div class="mainContentTop">
        <span class="outer">&nbsp;</span>
        <span class="inner">&nbsp;</span>
      </div>

      <div class="mainContentMiddle">
        <div id="pageOutcome" class="panelOuter2 pageError">
          <div class="panelInner2">
            <h2><span class="icon"></span>
            <span>
                 <c:choose>
                  <c:when test="${not empty bookingInfo.trackingData.sessionId}">
                    Sorry, we are facing technical difficulties 
                  </c:when>
                  <c:otherwise>
                    Sorry, due to inactivity your session has timed out.
                  </c:otherwise>
                 </c:choose>
                </span></h2>            
         
            <div class="panelContent2">           
              <p><a href="javascript:closeMainWindow()" title="Close this page">Close this page</a></p>
            </div>              
          </div>
        </div><!-- END #pageOutcome - Validation Errors -->
      </div><!-- END mainContentMiddle -->

      <div class="mainContentBottom">
        <span class="outer">&nbsp;</span>
        <span class="inner">&nbsp;</span>
      </div>
    </div><!-- END mainContent -->
  </div><!-- END #contentContainer -->
</div><!-- END wrapper -->

<%@include file="footer.jspf"%>
</body>
</html>