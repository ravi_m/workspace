<%@ page import="com.tui.uk.config.ConfReader;"%>
<%@include file="/common/commonTagLibs.jspf"%>
				<%
			 String getHomePageURL =(String)ConfReader.getConfEntry("hybrisfalcon.homepage.url","");
				  pageContext.setAttribute("getHomePageURL",getHomePageURL);
			%>
			<c:choose>
					 <c:when test="${not empty bookingComponent.clientURLLinks.homePageURL}">
					 <c:set var='homePageURL' value='${bookingComponent.clientURLLinks.homePageURL}' />
				      </c:when>
				     <c:otherwise>
				  	  <c:set var='homePageURL' value='${getHomePageURL}'/>
				     </c:otherwise>
			</c:choose>
			<c:choose>
			   <c:when test="${bookingComponent.nonPaymentData['TUIHeaderSwitch'] eq 'ON'}">
				  <c:set value="true" var="tuiLogo" />
			   </c:when>
			   <c:otherwise>
				  <c:set value="false" var="tuiLogo" />  
			   </c:otherwise>
			</c:choose>

			
			
				<div id="header">
					<!-- <div class="beta">
						<p>Welcome to our brand new website.</p>
						<p class="tell-us">
							<a href="http://www.falconholidays.ie/beta/send-us-an-email/provide-feedback.html" target="_blank" class="ensLinkTrack">Tell us what you think</a> or <a href="http://www.falconholidays.ie/beta/send-us-an-email/provide-feedback.html" target="_blank" class="ensLinkTrack" >Report a problem</a>
						</p>
						<a href="#" class="button">Switch to main site</a>
					</div> -->
					<div class="message-window" style="display:none" id="tellUsWhatYouThink">
						<a href="#" class="close ensLinkTrack"> <span class="text">Close</span></a>
						<div class="message-content small-pop beta-pop">
							<h3>Before you go...</h3>
							<p>We'd love to know what you made of the new site:</p>
							<ul>
								<li><a href="" target="_blank" class="ensLinkTrack" data-componentid="WF_COM_088-1">Tell us what you think</a></li>
								<li>or</li>
								<li><a href="" target="_blank" class="ensLinkTrack" data-componentid="WF_COM_088-1">Report a problem</a></li>
							</ul>
						</div>
					</div>
					<c:if test="${!tuiLogo}">
					<div id="utils">
						<ul>
						 
       <li><img width="58" height="77" src="/cms-cps/hybrisfalcon/images/WOT_Endorsement_V2_3C.png" id="header-wtui" alt="Welcome to the world of TUI"></li>
   </ul>
					</div>
					</c:if>
					<div id="logo<c:if test="${tuiLogo}">TUI</c:if>">
						<a target="_blank" class="ensLinkTrack" href="${bookingComponent.clientURLLinks.homePageURL}">Falcon</a>
					</div>
					
				</div>


