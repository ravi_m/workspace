		<%@ page import="com.tui.uk.config.ConfReader"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>



		<%
				String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.desktop.falconfo" , "");
				pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);

		%>
			<script>
			var ensLinkTrack = function(){};
			function displayHolidayTypes(ele){
				var holiday_type = new Array();
				holiday_type[0]="holidaytypes";
				holiday_type[1]="popDestinations";
				holiday_type[2]="shorthaul";
				var get_holiday_type_length=holiday_type.length;
				for(var i=0;i<get_holiday_type_length;i++){
					document.getElementById(holiday_type[i]).style.display="none";
					document.getElementById(holiday_type[i]+"_li").className="";
				}
				var split_type=ele.id.split("_");
				document.getElementById(split_type[0]).style.display="block";
				ele.className="active";
			}
			</script>
				<div id="inner-footer">
					<ul id="footer-utils">
					      <li id="safe-hands">
							<h3>You're in safe hands</h3>

							<p>Just so you know, Falcon Holidays is now called TUI Holidays, and we're part of TUI Group - the world's leading travel company. All of our holidays are designed to help you 'Discover your smile'.</p>


							<p class="authority">
							<!-- <img width="93" height="19" alt="World of TUI" src="/cms-cps/hybrisfalcon/images/WOT-logo.png" class="wot-logo"> -->
			                <a alt="Commission for Aviation Regulation" href="http://www.aviationreg.ie" target="_blank" class="ensLinkTrack rio">RIO</a>
							 <p class="commission">Licenced by the Commission for Aviation Regulation, TO 021</p>
		                    </p>


						</li>
						<li class="staySafeAbroad">
							<c:out value="${staySafeAbroad}" escapeXml="false"/>
						</li>
						<li>
							<h3>Find a local store</h3>
							<p><a data-componentid="WF_COM_200-2" class="ensLinkTrack" href="http://www.falconholidays.ie/about-us/travel-shops/">Shop Finder</a></p>
						</li>
						<li id="questions">
							<h3>Search For Anything</h3>
							<form action="https://www.falconholidays.ie/gsa/gsa.html" method="get">
								<div class="formrow">
									<textarea  name="q" class="textfield" placeholder="e.g. Where do I print my e-tickets?"></textarea>
									<!-- <input type="hidden" name="site" value="firstchoice_collection" />
									<input type="hidden" name="client" value="fc-hugo-main" />
									<input type="hidden" name="proxystylesheet" value="fc-hugo-main" />
									<input type="hidden" name="output" value="xml_no_dtd" />
									<input type="hidden" name="submit" value="Go" /> -->

									    <input type="hidden" value="default_collection" name="site">
							            <input type="hidden" value="production_frontend" name="client">
							            <input type="hidden" value="production_frontend" name="proxystylesheet">
							            <input type="hidden" value="xml_no_dtd" name="output">
							            <input type="hidden" value="Go" name="submit">


								</div>
								<div class="floater">
									<button class="button fr mt4 small">Search</button>
                                    <!-- <p class="help fl"><a target="_blank" href="http://www.falconholidays.ie/destinations/faqCategories" class="ensLinkTrack">Ask a question</a></p> -->
									<p class="contact-us fl"><a target="_blank" href="http://www.falconholidays.ie/editorial/legal/contact-us.html" class="ensLinkTrack">Contact us</a></p>
								</div>
							</form>
						</li>
					</ul>
					<!-- <script type="text/javascript">
					dojoConfig.addModuleName("tui/widget/Tabs");
					</script> -->

					<div id="footer-seo">
						<div class="tabs-container">
							<ul class="tabs">
								<li onclick="displayHolidayTypes(this);"  id="holidaytypes_li" class="active"><a enslinktrackattached="true"  class="ensLinkTrack" >Holiday Types</a></li>
								<li onclick="displayHolidayTypes(this);"  id="popDestinations_li"><a enslinktrackattached="true"  class="ensLinkTrack" >Popular Destinations</a></li>
								<li onclick="displayHolidayTypes(this);"  id="shorthaul_li"><a enslinktrackattached="true"  class="ensLinkTrack" >Short/Mid Haul</a></li>

							</ul>
							<div class="menu" id="holidaytypes" style="display: block;">
			<ul>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/sun-holidays/family-holidays/">Family Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/sun-holidays/adult-holidays/">Adult Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/sun-holidays/premier-holidays/">Premier Holidays</a></li>
			</ul>
			<ul>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/sun-holidays/all-inclusive-holidays/">All Inclusive Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/sun-holidays/holiday-villages/">Holiday Villages</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/sun-holidays/family-holidays/Splash.html">Splashworld</a></li>
			</ul>
			<ul>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/sun-holidays/adult-holidays/young-and-lively/">Young and Lively</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/sun-holidays/budget-holidays/">Budget Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/sun-holidays/">Sun Holidays</a></li>
			</ul>
			<ul>
				<!-- <li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/f/deals/summer-2016-deals">Summer 2016</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/f/deals/summer-2016-deals">Summer 2016</a></li> -->
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/arctic-circle-holidays/">Winter Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/last-minute-holidays/">Last Minute Holidays</a></li>
			</ul>

		</div>
			                <div class="menu" id="popDestinations" style="display: none;">
			<ul>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/spain/canary-islands/tenerife/">Tenerife holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/greece/ionian-islands/zante/">Zante</a></li>
            	<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/spain/canary-islands/">Canary Islands holidays</a></li>

			</ul>
			<ul>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/spain/canary-islands/lanzarote/">Lanzarote holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/africa/egypt/egypt-red-sea/sharm-el-sheikh/">Sharm El Sheikh Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/turkey/turkey-dalaman/">Dalaman</a></li>
			</ul>
			<ul>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/spain/balearic-islands/menorca/">Menorca Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/spain/balearic-islands/majorca/">Majorca Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/greece/crete/">Crete Holidays</a></li>
			</ul>
			<ul>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/spain/balearic-islands/ibiza/">Ibiza Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/spain/canary-islands/gran-canaria/">Gran Canaria Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/portugal/algarve/">Algarve Holidays</a></li>
			</ul>
		</div>
							<div class="menu" id="shorthaul" style="display: none;">
			<ul>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/africa/tunisia/">Tunisia Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/portugal/">Portugal Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/spain/">Spain Holidays</a></li>

			</ul>
			<ul>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/turkey/">Turkey Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/greece/">Greece Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/bulgaria/">Bulgaria Holidays</a></li>
			</ul>
			<ul>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/europe/finland/lapland/">Lapland Holidays</a></li>
				<li><a  class="ensLinkTrack" href="http://www.falconholidays.ie/destinations/africa/egypt/">Egpyt Holidays</a></li>
			</ul>
		</div>

						</div>
					</div>
				</div>

			<div id="footer">
				<ul >
	<li><a target="_blank" href="http://tuijobsuk.co.uk/">Travel Jobs</a></li>
	<li><a   target="_blank" href="https://blog.tuiholidays.ie/">TUI Blog</a></li>


</ul>
				<ul class="span half-bottom-margin">
	<li><jsp:useBean id='CurrentDate12' class='java.util.Date'/>
         	<fmt:formatDate var='currentYear' value='${CurrentDate12}' pattern='yyyy'/>
			&copy; <c:out value="${currentYear}" />  <a  target="_blank" href="http://www.tuigroup.com/en-en">TUI Group</a></li>

	<li><a  href="https://www.tuiholidays.ie/f/info/about-us">About TUI</a></li>
	<li><a  href="http://www.falconholidays.ie/our-policies/terms-of-use/">Terms &amp; Conditions</a></li>
	<li><a  href="https://www.falconholidays.ie/gsa/gsa.html?q=customer+welfare&site=Falcon-ie_collection&client=fj-hugo-main&proxystylesheet=fj-hugo-main&output=xml_no_dtd">Customer Welfare</a></li>
	<li><a  href="http://www.falconholidays.ie/our-policies/privacy-policy/">Privacy Policy</a></li>
	<li><a  href="http://www.falconholidays.ie/our-policies/statement-on-cookies/index.html">Statement on Cookies</a></li>
   <li>
   <c:choose>
		<c:when test="${applyCreditCardSurcharge eq 'true'}">
   <a  href="http://www.falconholidays.ie/information/credit-card-payments-and-charges/">Credit Card Fees</a>
   </c:when>
   <c:otherwise>
   <a  href="http://www.falconholidays.ie/information/credit-card-payments-and-charges/">Ways to Pay</a>
   </c:otherwise>
   </c:choose>
   </li>
</ul>
						<p>TUI is licenced by the Commission for Aviation Regulation under Licence T.O. 021, we have arranged an approved bond, therefore your money is secure with us. TUI is the part of the TUI Group of Companies.</p>
			
			</div>
