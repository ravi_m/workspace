<!-- <!doctype html> -->
<!--[if lt IE 7 ]> <html lang="en" id="firstchoice" class="ie6 "> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" id="firstchoice" class="ie7 "> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" id="firstchoice" class="ie8 "> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" id="firstchoice" class="ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="" id="firstchoice"> <!--<![endif]-->
<!-- <head>
	<title></title> -->
	<!--[if IE 7 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /><![endif]-->
	<!--[if IE 8 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /><![endif]-->
	<!--[if IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" /><![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><!--<![endif]-->
	<!-- <meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1"> -->


	<link rel="icon" type="image/png" href="/cms-cps/firstchoice/images/favicon.png" />

		<!--[if lte IE 9]>
			<link rel="shortcut icon" href="/cms-cps/firstchoice/images/favicon.ico" type="image/x-icon" />
			<link rel="icon" href="/cms-cps/firstchoice/images/favicon.ico" />
			<![endif]-->

			<!-- HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
         <%@ page import="com.tui.uk.config.ConfReader"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
			<!-- javascript -->
			<!-- <script type="text/javascript"  src="../js/TuiConfigProd.js"></script>
			<script type="text/javascript" src="../js/Bootstrap.js"></script>
			<link href="../css/main.css" rel="stylesheet" type="text/css" />
			<link href="../css/main_header.css" rel="stylesheet" type="text/css" />
			<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" /> -->
			<%
				String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.desktop.fc" , "");
				pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);

				%>
			<script>
			var ensLinkTrack = function(){};
			function displayHolidayTypes(ele){
				var holiday_type = new Array();
				holiday_type[0]="holidaytypes";
				holiday_type[1]="longhaul";
				holiday_type[2]="shorthaul";
				holiday_type[3]="allinclusive";
				var get_holiday_type_length=holiday_type.length;
				for(var i=0;i<get_holiday_type_length;i++){
					document.getElementById(holiday_type[i]).style.display="none";
					document.getElementById(holiday_type[i]+"_li").className="";
				}
				var split_type=ele.id.split("_");
				document.getElementById(split_type[0]).style.display="block";
				ele.className="active";
			}
			</script>
		<!-- </head>
		<body> -->
			<div id="wrapper">

				<div id="inner-footer">
					<ul id="footer-utils">
						<li id="safe-hands">
							<h3>You're in safe hands</h3>
							<p>We're part of TUI Group - one of the world's leading travel companies. And all of our holidays are designed to help you Discover Your Smile.</p>
							<p class="authority">
								<a target="_blank" class="abta ensLinkTrack" href="http://www.abta.com/find-a-holiday/member-search/5736">ABTA</a>
								<a target="_blank" class="atol ensLinkTrack" href="http://www.caa.co.uk/default.aspx?catid=27">ATOL</a>
							</p>
						</li>
						<li class="staySafeAbroad">
							<%=staySafeAbroad%>
						</li>
						<li>
							<h3>Find a local store</h3>
							<p><a target="_blank" href="http://www.firstchoice.co.uk/about-us/travel-shops/" class="ensLinkTrack">Shop Finder</a></p>
						</li>


						<li id="questions">
							<h3>Search for anything</h3>
							<form action="https://www.firstchoice.co.uk/gsa/search" method="get">
								<div class="formrow">
									<textarea  name="q" class="textfield" placeholder="e.g. Where do I print my e-tickets?"></textarea>
									<!-- <input type="hidden" name="site" value="firstchoice_collection" />
									<input type="hidden" name="client" value="fc-hugo-main" />
									<input type="hidden" name="proxystylesheet" value="fc-hugo-main" />
									<input type="hidden" name="output" value="xml_no_dtd" />
									<input type="hidden" name="submit" value="Go" /> -->

									    <input type="hidden" value="default_collection" name="site">
							            <input type="hidden" value="production_frontend" name="client">
							            <input type="hidden" value="production_frontend" name="proxystylesheet">
							            <input type="hidden" value="xml_no_dtd" name="output">
							            <input type="hidden" value="Go" name="submit">

								</div>
								<div class="floater">
									<button class="button fr mt4 small">Search</button>
                                  <p class="help fl"><a target="_blank" href="http://www.firstchoice.co.uk/holiday/faqCategories" class="ensLinkTrack">Ask a question</a></p>
									<p class="contact-us fl"><a target="_blank" href="http://www.firstchoice.co.uk/contact-us" class="ensLinkTrack">Contact us</a></p>
								</div>
							</form>
						</li>
					</ul>
					<!-- <script type="text/javascript">
					dojoConfig.addModuleName("tui/widget/Tabs");
					</script> -->

					<div id="footer-seo">
						<div class="tabs-container">
							<ul class="tabs">
								<li onclick="displayHolidayTypes(this);" class="active" id="holidaytypes_li"><a target="_blank"  class="ensLinkTrack">Holiday Types</a><span class="arrow"></span></li>
								<li onclick="displayHolidayTypes(this);" id="longhaul_li"><a  target="_blank" class="ensLinkTrack">Mid/Long haul</a><span class="arrow"></span></li>
								<li onclick="displayHolidayTypes(this);" id="shorthaul_li"><a target="_blank"  id="ensLinkTrack">Short haul</a><span class="arrow"></span></li>
								<li onclick="displayHolidayTypes(this);" id="allinclusive_li"><a target="_blank"  id="ensLinkTrack">All inclusive</a><span class="arrow"></span></li>
							</ul>
							<div id="holidaytypes" class="menu" style="display:block">
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/deals/summer-2017-deal" class="ensLinkTrack" data-componentId="WF_COM_200-3">Summer Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/deals/winter-2016-deal" class="ensLinkTrack" data-componentId="WF_COM_200-3">Winter Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/adult" class="ensLinkTrack" data-componentId="WF_COM_200-3">Adult Only Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/all-inclusive" class="ensLinkTrack" data-componentId="WF_COM_200-3">All inclusive Holidays</a></li>
								</ul>
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/family" class="ensLinkTrack" data-componentId="WF_COM_200-3">Family Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/deals/summer-2017-deal" class="ensLinkTrack" data-componentId="WF_COM_200-3">Sunshine Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/deals" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cheap Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/last-minute" class="ensLinkTrack" data-componentId="WF_COM_200-3">Last Minute Holidays</a></li>
								</ul>
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/beach" class="ensLinkTrack" data-componentId="WF_COM_200-3">Beach Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/deals/package-holidays" class="ensLinkTrack" data-componentId="WF_COM_200-3">Package Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/last-minute" class="ensLinkTrack" data-componentId="WF_COM_200-3">Late deals</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/luxury" class="ensLinkTrack" data-componentId="WF_COM_200-3">Luxury Holidays</a></li>
								</ul>
							</div>
							<div id="longhaul" class="menu" style="display:none">
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Egypt-EGY" class="ensLinkTrack" data-componentId="WF_COM_200-3">Egypt Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Goa-001915" class="ensLinkTrack" data-componentId="WF_COM_200-3">Goa Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Sri-Lanka-LKA" class="ensLinkTrack" data-componentId="WF_COM_200-3">Sri Lanka Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/The-Caribbean-CCC" class="ensLinkTrack" data-componentId="WF_COM_200-3">Caribbean Holidays</a></li>
									<li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/St-Lucia-LCA" class="ensLinkTrack" data-componentId="WF_COM_200-3">St Lucia Holidays</a></li>
								</ul>
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Dominican-Republic-DOM" class="ensLinkTrack" data-componentId="WF_COM_200-3">Dominican Republic Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Cape-Verde-CPV" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cape Verde Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Jamaica-JAM" class="ensLinkTrack" data-componentId="WF_COM_200-3">Jamaica Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Thailand-THA" class="ensLinkTrack" data-componentId="WF_COM_200-3">Thailand Holidays</a></li>
									<li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Vietnam-VNM" class="ensLinkTrack" data-componentId="WF_COM_200-3">Vietnam Holidays</a></li>
								</ul>
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Aruba-ABW" class="ensLinkTrack" data-componentId="WF_COM_200-3">Aruba Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Turkey-TUR" class="ensLinkTrack" data-componentId="WF_COM_200-3">Turkey Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Mexico-MEX" class="ensLinkTrack" data-componentId="WF_COM_200-3">Mexico Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Barbados-BRB" class="ensLinkTrack" data-componentId="WF_COM_200-3">Barbados Holidays</a></li>
									<li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Dubai-and-Emirates-004017" class="ensLinkTrack" data-componentId="WF_COM_200-3">Dubai Holidays</a></li>
								</ul>
							</div>
							<div id="shorthaul" class="menu" style="display:none">
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Portugal-PRT" class="ensLinkTrack" data-componentId="WF_COM_200-3">Portugal Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Cyprus-CYP" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cyprus Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Malta-MLT" class="ensLinkTrack" data-componentId="WF_COM_200-3">Malta Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Morocco-MAR" class="ensLinkTrack" data-componentId="WF_COM_200-3">Morocco Holidays</a></li>
									<li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Dubrovnik-&-Islands-002925" class="ensLinkTrack" data-componentId="WF_COM_200-3">Dubrovnik Holidays</a></li>
								</ul>
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Bulgaria-BGR" class="ensLinkTrack" data-componentId="WF_COM_200-3">Bulgaria Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Spain-ESP" class="ensLinkTrack" data-componentId="WF_COM_200-3">Spain Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Italy-ITA" class="ensLinkTrack" data-componentId="WF_COM_200-3">Italy Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Croatia-HRV" class="ensLinkTrack" data-componentId="WF_COM_200-3">Croatia Holidays</a></li>
								    <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Paphos-000799" class="ensLinkTrack" data-componentId="WF_COM_200-3">Paphos Holidays</a></li>
								</ul>
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Greece-GRC" class="ensLinkTrack" data-componentId="WF_COM_200-3">Greece Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Crete-000800" class="ensLinkTrack" data-componentId="WF_COM_200-3">Crete Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Rhodes-000923" class="ensLinkTrack" data-componentId="WF_COM_200-3">Rhodes Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Corfu-000691" class="ensLinkTrack" data-componentId="WF_COM_200-3">Corfu Holidays</a></li>
								<li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Fuerteventura-000312" class="ensLinkTrack" data-componentId="WF_COM_200-3">Fuerteventura Holidays</a></li>
								</ul>
							</div>
							<div id="allinclusive" class="menu" style="display:none">
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Tenerife-000335" class="ensLinkTrack" data-componentId="WF_COM_200-3">Tenerife Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Madeira-000157" class="ensLinkTrack" data-componentId="WF_COM_200-3">Madeira Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Ibiza-000242" class="ensLinkTrack" data-componentId="WF_COM_200-3">Ibiza Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Costa-del-Sol-000365" class="ensLinkTrack" data-componentId="WF_COM_200-3">Costa Del Sol Holidays</a></li>
								</ul>
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Zante-000952" class="ensLinkTrack" data-componentId="WF_COM_200-3">Zante Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Gran-Canaria-000318" class="ensLinkTrack" data-componentId="WF_COM_200-3">Gran Canaria Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Kefalonia-000864" class="ensLinkTrack" data-componentId="WF_COM_200-3">Kefalonia Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Benidorm-000349" class="ensLinkTrack" data-componentId="WF_COM_200-3">Benidorm Holidays</a></li>
								</ul>
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Lanzarote-000329" class="ensLinkTrack" data-componentId="WF_COM_200-3">Lanzarote Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Majorca-000122" class="ensLinkTrack" data-componentId="WF_COM_200-3">Majorca Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Cuba-CUB" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cuba Holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Algarve-000154" class="ensLinkTrack" data-componentId="WF_COM_200-3">Algarve Holidays</a></li>
								</ul>
							</div>
						</div>
					</div></div>
				</div>
<!-- 			</div> -->
			<div id="footer">
				<ul>
					<li>&copy; <script> document.write((new Date()).getFullYear()) </script> <a target="_blank" href="http://www.tuigroup.com/en-en" class="ensLinkTrack">TUI Group</a></li>
					<li><a target="_blank" href="http://www.firstchoice.co.uk/our-policies/terms-of-use/" class="ensLinkTrack">Terms &amp; Conditions</a></li>
					<li><a target="_blank" href="http://www.firstchoice.co.uk/our-policies/accessibility/" class="ensLinkTrack">Accessibility</a></li>
					<li><a target="_blank" href="http://www.firstchoice.co.uk/our-policies/privacy-policy/" class="ensLinkTrack">Privacy Policy</a></li>
					<li><a target="_blank" href="http://www.firstchoice.co.uk/our-policies/statement-on-cookies/" class="ensLinkTrack">Statement on Cookies</a></li>
					
					<li>
					<c:choose>
								<c:when test="${applyCreditCardSurcharge eq 'true'}">
					<a target="_blank" href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges/" class="ensLinkTrack" rel="author" target="_blank">Credit Card Fees</a>
					</c:when>
					<c:otherwise>
					<a target="_blank" href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges/" class="ensLinkTrack" rel="author" target="_blank">Ways to Pay</a>
					</c:otherwise>
					</c:choose>
					</li>
					
					<li><a target="_blank" href="http://www.firstchoice.co.uk/affiliates.html" class="ensLinkTrack">Affiliates</a></li>
				    <li><a target="_blank" href="http://press.firstchoice.co.uk/" class="ensLinkTrack">Press</a></li>
				    <li><a target="_blank" href="http://www.firstchoice.co.uk/blog/" class="ensLinkTrack">Blog</a></li>
				</ul>
				<p style="text-align: left;"><span style="font-size: 8.5pt; font-family: TUIType, sans-serif; color: rgb(140, 140, 140);">Many of the flights and flight-inclusive holidays on this website are financially protected by the ATOL scheme. But ATOL protection does not apply to all holiday and travel services listed on this website. Please ask us to confirm what protection may apply to your booking. If you do not receive an ATOL Certificate then the booking will not be ATOL protected. If you do receive an ATOL Certificate but all the parts of your trip are not listed on it, those parts will not be ATOL protected. Please see our booking conditions for information or for more information about financial protection and the ATOL Certificate go to:<span class="apple-converted-space">&nbsp;</span></span><span style="font-family: TUIType, sans-serif;"><a target="_blank" href="http://www.atol.org.uk/ATOLCertificate"><span style="font-size: 8.5pt; color: rgb(102, 102, 102);">www.atol.org.uk/ATOLCertificate</span></a></span>&nbsp;</p>

			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,tuiAnalyticsJson</c:set>
			<script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
				 tui.analytics.page.pageUid = "paymentDetailsPage";
				 <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
					 <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
					 tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
				    </c:if>				    
			    </c:forEach>
				</script>				
				<script>			
				window.dataLayer = window.dataLayer || [];
				window.dataLayer.push(${bookingInfo.bookingComponent.nonPaymentData['tuiAnalyticsJson']});			
			   </script>
			</div>
	<!-- 	</body>
		</html> -->