<%---------------------------Set the errata message ---------------------------------------------------------%>
<c:forEach var="errata" items="${bookingComponent.importantInformation.errata}">
  <c:if test="${errata != 'No errata'}">
    <c:set var="errataMessage" value= "${errata}"/>
  </c:if>
</c:forEach>
<%----------------------*****End Set the errata message *****------------------------------------------------%>

<%---------------------------Set showRedundancyTextValue-----------------------------------------------------%>
<c:forEach var="description" items="${bookingComponent.importantInformation.description}">
  <c:if test="${not empty description.value && description.key == 'redundntInsuranceTxt'}">
    <c:set var="showRedundancyTextValue" value="${description.value}"/>
  </c:if>
</c:forEach>
<%----------------------*****End Set showRedundancyTextValue*****--------------------------------------------%>

<%---------------------------Important information Display --------------------------------------------------%>
<c:if test="${not empty errataMessage || not empty showRedundancyText}">
  <div class="optionSection">
    <div id="container_importantinformation">

      <%-----------------------Important Information header -------------------------------------------------%>
      <div class="subheaderBar">
        <img src="/cms-cps/fcao/images/icons/header-information.gif" title="Important Information" alt="Important Information" width="34" height="27" />
        <h2>Important information</h2>
      </div>
      <%------------------*****End Important Information header *****----------------------------------------%>
     
      <div id="container_importantinformation_text1" class="optionDetails">
       
       <%---------------------Errata Block------------------------------------------------------------------%>
        <c:if test="${not empty errataMessage}">
         
          <div class="newInfoBlock" id="importantInfo">
            <c:if test="${not empty errataMessage}">
              <p><c:out value="${errataMessage}" escapeXml="false"/></p>
            </c:if>

            <c:if test="${bookingComponent.flightSummary != null && (bookingComponent.flightSummary.flightSupplierSystem) == 'Amadeus' && (bookingComponent.flightSummary.flightSelected) == 'false'}">
              <div class="amadeustermsandconditions">
                <p>Full terms and conditions of your flight product component supplier are available on request.  Any cancellation, amendments or changes may incur a charge of up to 100% of your booking price plus supplier&#146;s administrative expenses.</p>
                <p>If the cancellation fee is 100% and you wish to re-book, the price of your new flight will usually be based on the prices applicable on the day you ask to re-book, which may not be the same as when you first booked.</p>
                <p>International travel is subject to the liability rules of the Warsaw Convention and the Montreal Convention. To view the notice summarising the liability rules applied by EU community air carriers please read the <a class="inline-link" href="javascript:void(0);" onclick="Popup('http://www.firstchoice.co.uk/our-policies/air-passenger-notice/',795,595,'location=0,status=0,left=100,top=50,toolbar=0,menubar=0,titlebar=0,resizable=1,scrollbars=1')" title="Air Passenger Notice">Air Passenger Notice.</a></p>
              </div>
            </c:if>
          </div><%-- end importantInfo --%>
        </c:if>
       <%----------------*****END Errata Block***********---------------------------------------------------%>
      
       <%---------------------RedundancyText Block----------------------------------------------------------%>  
        <c:if test="${not empty showRedundancyTextValue}">
          <p><c:out value="${showRedundancyTextValue}" escapeXml="false"/></p>
        </c:if>
       <%----------------*****END RedundancyText Block***********-------------------------------------------%>

      </div><%--END container_importantinformation_text1 --%>
    </div><%--END  container_importantinformation--%>
  </div><%-- END optionSection--%>
</c:if>
<%----------------------*****END Important information Display *****-----------------------------------------%>