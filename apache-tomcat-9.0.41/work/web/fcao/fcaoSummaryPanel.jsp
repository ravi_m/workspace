<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div id="summaryPanel">
  <div class="genericSidesShadowRight">
    <div class="genericSidesShadowLeft">
      <jsp:include page="fcaoPricePanel.jsp"/>
      <c:if test="${not empty bookingComponent.flightSummary || bookingComponent.flightSummary != null}">
        <jsp:include page="fcaoFlightOptions.jsp"/>
      </c:if>
      <jsp:include page="fcaoAccomDetails.jsp"/>
    </div><!-- END genericSidesShadowLeft -->
  </div><!-- END genericSidesShadowRight -->
  <jsp:include page="fcaoGenericBottomshadow.jsp"/>
</div><!-- END summaryPanel -->