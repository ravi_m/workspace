<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="rating" value="${bookingComponent.accommodationSummary.accommodation.rating}"/>
<c:if test="${rating!=null}">
  <img src="/cms-cps/fcao/images/rating/<c:out value='${rating}'/>.gif" alt="<c:out value='${rating}'/>"/>
</c:if>
