<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="url" value='${bookingComponent.clientDomainURL}'/>
<c:set var="newHoliday" value="${bookingInfo.newHoliday}"/>
<c:set var="accomInv" value="${bookingComponent.accommodationSummary.accommodationInventorySystem}"/>
<c:set var="errataMessage" value= ""/>

<%--Set the errata message in the errataMessage variable so that it can be used in the display block--%>
<c:forEach var="errata" items="${bookingComponent.importantInformation.errata}">
  <c:if test="${errata != 'No errata'}">
    <c:set var="errataMessage" value= "${errataMessage} ${errata}"/>
  </c:if>
</c:forEach>
<%--End of the errataMessage setter block--%>

<%-- Set the redundancy text in the showRedundancyTextValue variable which is used in the display block. Also set the showRedundancyText flag accordingly.
     Depending on the flag, the redundancy text will be displayed --%>
<c:set var="showRedundancyText" value="false"/>
<c:set var="showRedundancyTextValue" value=""/>
 <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
   <c:set var="redundntInsTxt" value="${description.value}" />
   <c:if test="${not empty redundntInsTxt && description.key == 'redundntInsuranceTxt'}">
      <c:set var="showRedundancyText" value="true"/>
      <c:set var="showRedundancyTextValue" value="${description.value}"/>
   </c:if>
 </c:forEach>
<%--End of redundanctTxt setter block--%>


<%--There are two scenarios here. Case1: There is additionalInfo or errata to be displayed or Case2: No errata or additionalInfo to be displayed--%>
<c:choose>
<%--Case1: Either additionalInfo or errata messages are present --%>
  <c:when test="${bookingComponent.accommodationSummary.additionalInfo != null  || not empty errataMessage }">
  <div class="optionSection">
    <div id="container_importantinformation">
      <div class="subheaderBar">
        <img src="/cms-cps/fcao/images/icons/header-information.gif" title="Important Information" alt="Important Information" width="34" height="27" />
        <h2>Important information</h2>

      </div>

      <div id="container_importantinformation_text1" class="optionDetails">
        <p>Here is some additional information about the holiday you have chosen. If you are happy to proceed please click to accept or <a title="Return to your results" href="<c:out value='${url}'/>/accom/page/search/searchresults.page?pageNumber=0&ico=aOPayment_ReturnResults" class="inline-link">return to your results</a> to select an alternative.</p>

      <div class="newInfoBlock" id="importantInfo">
          <c:if test="${not empty errataMessage}">
            <p><c:out value="${errataMessage}" escapeXml="false"/></p>
          </c:if>

        <c:if test="${bookingComponent.flightSummary != null && (bookingComponent.flightSummary.flightSupplierSystem) == 'Amadeus' && (bookingComponent.flightSummary.flightSelected) == 'false'}">
          <div class="amadeustermsandconditions">
            <p>Full terms and conditions of your flight product component supplier are available on request.  Any cancellation, amendments or changes may incur a charge of up to 100% of your booking price plus supplier&#146;s administrative expenses.</p>
            <p>If the cancellation fee is 100% and you wish to re-book, the price of your new flight will usually be based on the prices applicable on the day you ask to re-book, which may not be the same as when you first booked.</p>
            <p>International travel is subject to the liability rules of the Warsaw Convention and the Montreal Convention. To view the notice summarising the liability rules applied by EU community air carriers please read the <a class="inline-link" href="javascript:void(0);" onclick="Popup('http://www.firstchoice.co.uk/our-policies/air-passenger-notice/',795,595,'location=0,status=0,left=100,top=50,toolbar=0,menubar=0,titlebar=0,resizable=1,scrollbars=1')" title="Air Passenger Notice">Air Passenger Notice.</a></p>
          </div>
        </c:if>

        <%--/*Section 1 :This section displays the decriptions in bold only if the description.key are [Cancellation policy:,cancellationcharges,Guarantee policy:]
		To be noted that the description.key are not displayed in this section
        <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
            <c:set var="extrainfo" value="${description.value}" />
            <c:if test ="${extrainfo != null && (description.key == 'Cancellation policy:' || description.key == 'cancellationcharges' || description.key == 'Guarantee policy:')}">
              <p><strong><c:out value="${extrainfo}" escapeXml="false"/></strong></p>
            </c:if>
        </c:forEach>
         End of Section 1*/ --%>

        <%--Section 2: This section displays the description.key (in bold) and the description.value(in normal text) only if the inventory system is Hopla Pegasus--%>
        <c:if test="${accomInv == 'HOPLA_PEG'}">
          <p><strong>Supplier's terms and conditions: -</strong></p>
          <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
            <c:set var="extrainfo" value="${description.value}" />
            <p><strong><c:out value="${description.key}"/></strong>
            <c:out value="${extrainfo}" escapeXml="false"/></p>
          </c:forEach>
        <%-- End of Section 2--%>
          <p>We aim to provide you with the most accurate rate information, but we urge you to check with the individual hotel for child policies and possible extra charges</p>
        </c:if>
      </div>
      <div class="termsAcceptance">
        <div class="checkbox">
          <input id="importantInformationChecked" name="importantInformationChecked" type="checkbox"  value="true" />
        </div>

        <label for="importantInformationChecked">
          <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem  == 'HOPLA_PEG'}">
            I have read and accept the suppliers terms and conditions.
          </c:if>
          <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem  != 'HOPLA_PEG'}">
            I have read and accept the important additional information.
          </c:if>
        </label>
     </div>
     <c:if test="${showRedundancyText}">
         <p>
            <c:out value="${showRedundancyTextValue}" escapeXml="false"/>
        </p>
    </c:if>
  </div>
  </div>
  </c:when>
<%--Case1 ends--%>

<%--Case2:No additionalInfo or errata --%>
  <c:otherwise>
    <c:if test="${showRedundancyText }">
      <div class="optionSection">
         <div class="subheaderBar">
            <img src="/cms-cps/fcao/images/icons/header-information.gif" title="Important Information" alt="Important Information" width="34" height="27" />
            <h2>Important information</h2>
          </div>
          <div class="optionDetails">
            <p><c:out value="${showRedundancyTextValue}" escapeXml="false"/></p>
          </div>
         </div>
    </c:if>
  </c:otherwise>
<%--Case2ends--%>
</c:choose>

