<%@include file="/common/commonTagLibs.jspf"%>

<c:set var='clientapp' value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName} '/>

<%
    String clientApp = ((String)pageContext.getAttribute("clientapp")).trim();
    String axaInsurance = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".axaInsuranceEnabled","false");
    pageContext.setAttribute("axaInsurance", axaInsurance, PageContext.REQUEST_SCOPE);

%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div class="subheaderBar">
  <img alt="" src="/cms-cps/fcao//images/icons/header-passengers.gif" />
  <h2>Personal details</h2>
</div>
<div class="optionDetails">
  <p>Please enter the passenger names to match those shown on your passports.</p>
  <div class="paxHeaders">
    <p class="titleHeader">Title</p>
    <p class="firstNameHeader">First Name <span>*</span></p>
    <p class="middleInitialHeader">Middle Initial</p>
    <p class="surnameHeader">Surname <span>*</span></p>
    <c:set var="over65count" value="0" />
    <c:set var="over76count" value="0" />

    <c:forEach var="passenger" items="${bookingComponent.passengerRoomSummary}">
      <c:forEach var="passengerRoom" items="${passenger.value}" varStatus="passen">
        <c:if test="${passengerRoom.ageClassification.code == 'SNR'}">
          <c:set var="over65count" value="${over65count+1}" />
        </c:if>
        <c:if test="${passengerRoom.ageClassification.code == 'SNR2'}">
          <c:set var="over76count" value="${over76count+1}" />
        </c:if>
      </c:forEach>
    </c:forEach>
    <c:if test="${over65count > 0}">
      <p class="seniorAge">Aged<br />65-74</p>
    </c:if>
    <c:if test="${over76count > 0}">
    <c:choose>
           <c:when test="${axaInsurance}">
            <p class="seniorAge">Aged<br />75-84</p>
           </c:when>
           <c:otherwise><p class="seniorAge">Aged<br />75+</p></c:otherwise>
          </c:choose>
      </c:if>
    <%-- <c:if test="${over76count > 0}">
      <p class="seniorAge">Aged<br />75+</p>
    </c:if> --%>
  </div>
  <!--Needed for no of passengers -->
  <c:set var="numberOfPassengers" />
  <c:set var="passengerCount" value="0" />
  <c:set var="totalNumberOfPassengersNew" value="0" />
  <c:set var="totalPassenCount" value="0" />
  <c:forEach var="passenger" items="${bookingComponent.passengerRoomSummary}" varStatus="status">
    <c:set var="noOfRooms" value="${status.count}"/>
    <c:set var="numberOfPassengers" value="${status.count}" />
    <c:forEach var="passengerRoom" items="${passenger.value}" varStatus="passen">
      <c:set var="numberOfPassengersNew" value="${passen.count}"/>
    </c:forEach>
    <c:set var="totalNumberOfPassengersNew" value="${totalNumberOfPassengersNew+numberOfPassengersNew}"/>
  </c:forEach>
  <input type="hidden" name="numberOfPassengers" value="<c:out value="${numberOfPassengers}"/>" />
  <div class="roomAllocation">
    <c:forEach var="passenger" items="${bookingComponent.passengerRoomSummary}" varStatus="status">
      <c:set var="noOfRooms" value="${status.count}" />
      <c:if test="${noOfRooms ge 1}">
        <p class="roomNumber">Room <c:out value="${noOfRooms}"/>:</p>
      </c:if>
      <ul class="roomConfig">
        <c:forEach var="passengerRoom" items="${passenger.value}" varStatus="passen">
          <c:set var="numberOfPassengers" value="${passen.index}"/>
          <li>
            <c:set var="numberOfPassengers" value="${passen.index}"/>
            <p class="paxNumber">
            <c:out value="${passengerRoom.label}"/>
            <c:set var="identifier" value="passenger_${passengerRoom.identifier}_identifier"/>
            <input type = "hidden" name="<c:out value="${identifier}"/>" value = "<c:out value = '${passengerRoom.identifier}'/>"/>
            <c:if test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR' || passengerRoom.ageClassification == 'SENIOR2'}">
              <c:if test = "${passengerCount == 0}">
                <input name="leadPassenger" type="hidden" value="<c:out value="${passengerRoom.identifier}"/>" />
              </c:if>
            </c:if>
            <c:if test="${passengerRoom.ageClassification == 'CHILD'}">
              <input id="childPassenger<c:out value='${passengerRoom.identifier}'/>" name="childPassenger<c:out value='${passengerRoom.identifier}'/>" type="hidden" value="<c:out value='${passengerRoom.identifier}'/>" />
            </c:if>
            <c:if test="${passengerRoom.ageClassification == 'INFANT'}">
              <input id="infantPassenger<c:out value='${passengerRoom.identifier}'/>" name="infantPassenger<c:out value='${passengerRoom.identifier}'/>" type="hidden" value="<c:out value='${passengerRoom.identifier}'/>" />
            </c:if>
            <c:if test="${passengerRoom.ageClassification == 'CHILD'}">
              <br />(age <c:out value="${passengerRoom.age}" />)
              <input type='hidden' id='passenger_<c:out value="${passengerRoom.identifier}"/>_age' value='<c:out value="${passengerRoom.age}" />'/>
              <input type='hidden' id='passenger_<c:out value="${passengerRoom.identifier}"/>_isInfant' value='false'/>
            </c:if>
            <c:if test="${passengerRoom.ageClassification == 'INFANT'}"><br />(age &lt; 2)<input type='hidden' id='passenger_<c:out value="${passengerRoom.identifier}"/>_isInfant' value='true'/></c:if>
            </p>
            <input type = "hidden" name="<c:out value="${identifier}"/>" value = "<c:out value = '${passengerRoom.identifier}'/>"/>
            <c:set var="titlekey" value="personaldetails_${passengerRoom.identifier}_title"/>
            <div class="title">
              <select name="<c:out value='${titlekey}' />">
                <c:choose>
                  <c:when test="${passengerRoom.ageClassification == 'INFANT' || passengerRoom.ageClassification == 'CHILD'}">
                    <option value="Mstr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mstr'}">selected</c:if>>Mstr</option>
                    <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
                  </c:when>
                  <c:otherwise>
                    <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mr'}">selected</c:if>>Mr</option>
                    <option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mrs'}">selected</c:if>>Mrs</option>
                    <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
                    <option value="Ms" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Ms'}">selected</c:if>>Ms</option>
                    <option value="Dr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Dr'}">selected</c:if>>Dr</option>
                    <option value="Rev" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Rev'}">selected</c:if>>Rev</option>
                    <option value="Prof" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Prof'}">selected</c:if>>Prof</option>
                  </c:otherwise>
                </c:choose>
              </select>
            </div>
            <div class="firstName">
              <c:set var="foreNameKey" value="personaldetails_${passengerRoom.identifier}_foreName"/>
              <input type="text" id="foreName_<c:out value='${passengerRoom.identifier}'/>" maxlength="15" name="<c:out value="${foreNameKey}"/>" value="<c:out value="${bookingComponent.nonPaymentData[foreNameKey]}"/>" />
            </div>
            <div class="middleInitial">
              <c:set var="middleNameKey" value="personaldetails_${passengerRoom.identifier}_middleInitial"/>
              <input type="text" id="middleName_<c:out value='${passengerRoom.identifier}'/>" maxlength="1" name="<c:out value="${middleNameKey}"/>" value="<c:out value="${bookingComponent.nonPaymentData[middleNameKey]}"/>" />
            </div>
            <div class="surname">
              <c:set var="lastNameKey" value="personaldetails_${passengerRoom.identifier}_surName"/>
              <input type="text" id="surName_<c:out value='${passengerRoom.identifier}'/>" name="<c:out value="${lastNameKey}"/>" onclick="javascript:unCheck()" value="<c:out value="${bookingComponent.nonPaymentData[lastNameKey]}"/>" id="lastname_<c:out value='${passengerCount}'/>" maxlength="15"/>
            </div>
              <c:if test="${(over65count >0) && (passengerRoom.ageClassification != 'CHILD' && passengerRoom.ageClassification != 'INFANT')}">
			  <div class="seniorAgeCheckbox">
                <c:set var="ageBetween65To75" value="personaldetails_${passengerRoom.identifier}_ageBetween65To75"/>
                <input name='<c:out value="${ageBetween65To75}"/>' id='<c:out value="${ageBetween65To75}"/>' type="checkbox" value="true"  onclick="validatePassenegersOver65('<c:out value='${passengerRoom.identifier}'/>');"/>

				</div>
              </c:if>
              <c:if test="${(over76count >0) && (passengerRoom.ageClassification != 'CHILD' && passengerRoom.ageClassification != 'INFANT')}">
			   <div class="seniorAgeCheckbox">
					 <c:set var="ageBetween76To84" value="personaldetails_${passengerRoom.identifier}_ageBetween76To84"/>
                    <c:if test="${over65count == 0}">
                     <input name='<c:out value="${ageBetween76To84}"/>' id='<c:out value="${ageBetween76To84}"/>' type="checkbox" value="true" onclick="validatePassenegersOver75('<c:out value='${passengerRoom.identifier}'/>');"/>
                    </c:if>
                    <c:if test="${over65count != 0}">
                     <input name='<c:out value="${ageBetween76To84}"/>' id='<c:out value="${ageBetween76To84}"/>' type="checkbox" value="true" onclick="validatePassenegersOver75('<c:out value='${passengerRoom.identifier}'/>');"/>
                   </c:if>
             	</div>
              </c:if>

            <c:if test="${passengerRoom.ageClassification == 'INFANT' || passengerRoom.ageClassification == 'CHILD'}">
              <p class="childDOB">
                <span id="spanDay<c:out value='${passengerRoom.identifier}'/>"></span>
                <span id="spanMonth<c:out value='${passengerRoom.identifier}'/>"></span>
                <span id="spanYear<c:out value='${passengerRoom.identifier}'/>"></span>
              </p>
              <script type="text/javascript">
                loadDates('<c:out value="${passengerRoom.identifier}"/>');
              </script>
            </c:if>
          </li>
          <c:if test="${passen.first && passengerCount == 0}">
            <li class="extraInfo">
              <p class="minAge">
                <c:if test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR' || passengerRoom.ageClassification == 'SENIOR2'}">
                  <input name="leadPassenger" type="hidden" value="<c:out value="${passengerRoom.identifier}"/>" />
                      Lead passenger must be 18 years or over
                </c:if>
              </p>
              <p class="sameSurname">
                <c:if test="${totalNumberOfPassengersNew > 1}">
                  <c:if test="${passengerCount+1 != numberOfPassengers }">
                    <input type="checkbox" id="autoCheck[<c:out value='${passengerCount+1}'/>]" name="autoCheck[<c:out value='${passengerCount+1}'/>]" onclick="javascript:autoCompletionSurname(<c:out value='${totalNumberOfPassengersNew}'/>,<c:out value='1'/>)"/>
                    Use same surname for whole party
                  </c:if>
               </c:if>
              </p>
            </li>
          </c:if>
          <c:set var="passengerCount" value="${passengerCount+1}" />
        </c:forEach>
      </ul>
    </c:forEach>
    <input type = "hidden" id="passengerCount" name = "passengerCount" value = "<c:out value = '${passengerCount}'/>"/>
  </div><!-- END roomAllocation -->
  <script type="text/javascript">
      var totalPassenCount=<c:out value='${passengerCount}'/>;
      var totalPassengerCount=<c:out value='${totalNumberOfPassengersNew}'/>;
      var noPerson65 = "<c:out value='${over65count}' />";
      var noPersons75 =  "<c:out value='${over76count}' />";
  </script>
  <jsp:include page="fcaoPostalAddress.jsp"/>
</div><!-- END optionDetails -->