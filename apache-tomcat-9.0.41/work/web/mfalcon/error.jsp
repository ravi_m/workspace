<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<c:choose>
        <c:when test="${not empty sessionScope=='true'}">
            <title>Falcon | Error</title>
        </c:when>
        <c:otherwise>
            <title>Falcon | Session Timed Out</title>
        </c:otherwise>
    </c:choose>
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">

	<link rel="stylesheet" href="/cms-cps/mfalcon/css/base.css" />
	<link rel="stylesheet" href="/cms-cps/mfalcon/css/bf.css" />
</head>
<body>
	<div class="structure">
	
		<div id="page">
			
			<jsp:include page="sprocket/header.jsp" />
			
			<div id="content" class="book-flow">
				<div class="content-width">
					
					<!-- Sorry - something has gone wrong... -->
					<div class="error-oh bg-light-grey marg-bottom-20">
						<div class="halfs">
							<div class="crop">
								<img src="/cms-cps/mfalcon/images/TOM_AIR_787_13_F0429_2.jpg" srcset="
									/cms-cps/mfalcon/images/TOM_AIR_787_13_F0429_3.jpg 658w,
									/cms-cps/mfalcon/images/TOM_AIR_787_13_F0429_2.jpg 488w,
									/cms-cps/mfalcon/images/TOM_AIR_787_13_F0429_1.jpg 232w" sizes="100vw" alt="" class="dis-block full" />
							</div>
						</div>					
						<div class="halfs copy">
							<c:choose>
                                <c:when test="${empty errorMessage}">
                                    <h1>Sorry - something has gone wrong with our website.</h1>
                                </c:when>
                                <c:otherwise>
                                    <h1>Sorry - <c:out value="${errorMessage}"/>.</h1>
                                </c:otherwise>
                            </c:choose>
							<p class="grey-med marg-bottom-10">The feature you were using may not have worked properly. Our engineers have been informed.</p>
					        <p class="grey-med marg-bottom-10">If you notice any odd effects try re-loading the page.</p>			
							
						</div>
					</div>
					
				</div>
			</div>
		
			<!--footer -->
			<jsp:include page="sprocket/footer.jsp" />
		
			<div class="page-mask"></div>
		</div>
		
	</div>
	<script src="/cms-cps/mfalcon/js/iscroll-lite.js" type="text/javascript"></script>

</body>
</html>
