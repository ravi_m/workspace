<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:formatNumber value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}" var="totalCostingLine" type="number" pattern="####" maxFractionDigits="2" minFractionDigits="2"/>
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page"/>
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:if test="${not empty bookingComponent.breadCrumbTrail}">
<c:set var="optionsUrl" value="${bookingComponent.breadCrumbTrail['HOTEL']}"/>
<c:set var="searchUrl" value="${bookingComponent.breadCrumbTrail['HUBSUMMARY']}"/>
<c:set var="passengersUrl" value="${bookingComponent.breadCrumbTrail['PASSENGERS']}"/>
</c:if>

<div id="book-flow-header" class="flight-only">
				<div class="content-width">
					<div class="logo thomsonX firstchoiceX falcon"><a href="${bookingComponent.clientURLLinks.homePageURL}"></a></div>
					<img alt="World Of TUI" class="nomobile  nominitablet " id="header-wtui" src="/cms-cps/mfalcon/images/WOT_Endorsement_V2_3C.png">
					<div class="summary-panel-trigger b abs bg-tui-sand black">
						<i class="caret fly-out"></i>&euro;<c:out value="${totalcost[0]}."/><span class="pennys"><c:out value="${totalcost[1]}"/></span>
						
					</div>
				</div>
			</div>
			<div id="book-flow-progress">
				<div class="content-width">
					<div class="scroll uppercase" id="breadcrumb" data-scroll-dw="true" data-scroll-options='{"scrollX": true, "scrollY": false, "keyBindings": true, "mouseWheel": true}'>
						<ul class="c">
							<li class="back"><a href="<c:out value='${optionsUrl}'/>"><span class="rel b">1</span> HOTEL</a></li>
							<li class="back"><a href="<c:out value='${searchUrl}'/>"><span class="rel b">2</span> PERSONALISE</a></li>
							<li class="back"><a href="<c:out value='${passengersUrl}'/>"><span class="rel b">3</span> PASSENGER</a></li>
							<li class="active"><span class="rel b">4</span> Payment</li>
						</ul>
					</div>
				</div>
			</div>