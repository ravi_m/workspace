<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<h2>
   <img id="hl_leadPax" src="/cms-cps/atcomres/images/head_detailslead_dkblauhg.gif" alt="" border="0">
</h2>

<div class="control">
     <table border="0" cellpadding="0" cellspacing="0" width="100%">
       <tr>
         <td width="165">
           <div class="rowseparator"></div>
         </td>
         <td width="190">
           <div class="rowseparator"></div>
         </td>
         <td width="150">
           <div class="rowseparator"></div>
         </td>
       </tr>
       <tr>
         <td>Street address*</td>
         <td colspan="2">
         <c:set var="passengerCount" value="0" />
         <c:set var="addressLine1" value="personaldetails_addressLine1"/>
           <input name="addressLine1" id="<c:out value="${addressLine1}"/>"
              value="<c:out value="${bookingComponent.nonPaymentData['addressLine1']}"/>"
              requiredError="Your street address" regex="^[A-Za-z0-9'\./()\-&amp; ]+$" required="true"
              alt="Your address is required to complete your booking. Please enter your address|Y|ADDRESS"
              regexerror="Please enter only English alphanumeric characters or './()-&amp; in the address lines." style="width:185px;"
              type="text" class="large" maxlength="25" />

         </td>
       </tr>
       <tr>
         <td colspan="3">
           <div class="rowseparator"></div>
         </td>
       </tr>
       <tr>
         <td></td>
         <td colspan="2">
         <c:set var="addressLine2" value="personaldetails_addressLine2"/>
         <input name="addressLine2" id="<c:out value="${addressLine2}"/>"
            value="<c:out value="${bookingComponent.nonPaymentData['addressLine2']}"/>"
              maxlength="25" regex="^[A-Za-z0-9'\./()\-&amp; ]+$"
              regexerror="Please enter only English alphanumeric characters or './()-&amp; in the address lines."
              style="width:185px;">
         </td>
       </tr>
       <tr>
         <td colspan="3">
           <div class="rowseparator"></div>
         </td>
       </tr>
       <tr>
         <td>Town/city*</td>
         <td colspan="2">
         <c:set var="city" value="personaldetails_city"/>
            <input name="city" id="<c:out value="${city}"/>" regex="^[A-Za-z ]+$" requiredError="Your city name"
               value="<c:out value="${bookingComponent.nonPaymentData['city']}"/>"
               alt="Your home town/city is required to complete your booking. Please enter your home town/city.|Y|ADDRESS"
               type="text" class="large"  maxlength="25" style="width:185px;" required="true"/>
         </td>
       </tr>
       <tr>
         <td colspan="3">
           <div class="rowseparator"></div>
         </td>
       </tr>
       <tr>
         <td>Postal code*</td>
         <td colspan="2">
         <c:set var="postCode" value="personaldetails_postCode"/>
            <input name="postCode" value="<c:out value="${bookingComponent.nonPaymentData['postCode']}"/>"
            alt="Please enter the Post Code|Y|POSTCODE" required="true" id="<c:out value="${postCode}"/>"
            type="text" class="small" maxlength="8" style="width:75px;" regex="^[A-Za-z0-9 ]+$" requiredError="Your postal code" />
         </td>
       </tr>
       <tr>
         <td colspan="3">
           <div class="rowseparator"></div>
         </td>
       </tr>
       <tr>
         <td>Country*</td>
         <td colspan="2">
         <c:set var="selectedCountryCode" value="${bookingComponent.nonPaymentData['selected_country_code']}"/>
         <select name="country" required="true" requiredError="Your country">
         <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
            <c:set var="countryCode" value="${CountriesList.key}" />
            <c:set var="countryName" value="${CountriesList.value}" />
            <option value='<c:out value="${countryCode}"/>'
              <c:if test='${selectedCountryCode==countryCode}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
            </option>
         </c:forEach>
         </select>
         </td>
       </tr>
       <tr>
         <td colspan="3">
           <div class="rowseparator"></div>
         </td>
       </tr>
       <tr>
         <td>Phone number*</td>
         <td>
         <c:set var="phoneNumber" value="personaldetails_phoneNumber"/>
            <input name='phoneNumber' value='<c:out value="${bookingComponent.nonPaymentData['phoneNumber']}"/>'
               alt ='Please enter a valid telephone number.|Y|PHONE' regex="^[0-9]+$" required="true"
               type='text' maxlength='15' regexerror="Please enter only numbers in the phone fields."
               requiredError="Your phone number"
               id="<c:out value="${phoneNumber}"/>" onchange="filterPhoneField(this);" style="width:185px;" />

			  <input type ="hidden" name='dayTimePhone'  id="dayTimePhone"  />
         </td>
         <td rowspan="8" width="30%" valign="top">
           <table cellpadding="0" cellpasing="0" border="0">
             <tr>
               <td width="10"></td>
               <td>
                 <span class="reddot">Enter your phone/fax number(s) without any gaps, hyphens or punctuation e.g.<br>004424761234567</span>
               </td>
             </tr>
           </table>
         </td>
       </tr>
       <tr>
         <td colspan="2">
           <div class="rowseparator"></div>
         </td>
       </tr>
       <tr>
         <td>Mobile/overseas contact number</td>
         <td>
            <c:set var="mobilePhone" value="personaldetails_mobilePhone"/>
            <input name="mobilePhone" value='<c:out value="${bookingComponent.nonPaymentData['mobilePhone']}"/>'
               alt="Please enter a valid telephone number.|N|PHONE"
               type="text"
               regex="^[0-9]+$" regexerror="Please enter only numbers in the phone fields."
               maxlength="15" onchange="filterPhoneField(this);" />
         </td>
       </tr>
       <tr>
         <td colspan="2">
           <div class="rowseparator"></div>
         </td>
       </tr>
       <tr>
         <td>Fax number</td>
         <td>
            <c:set var="faxNumber" value="personaldetails_faxNumber"/>
            <input name="faxNumber" value='<c:out value="${bookingComponent.nonPaymentData['faxNumber']}"/>'
               alt="Please enter a valid fax number.|N|PHONE"
               type="text"
               regex="^[0-9]+$" regexerror="Please enter only numbers in the phone fields."
               maxlength="15" onchange="filterPhoneField(this);" />
         </td>
       </tr>
       <tr>
         <td colspan="2">
           <div class="rowseparator"></div>
         </td>
       </tr>
       <tr>
         <td>Email address*</td>
         <td>
         <c:set var="emailAddress" value="personaldetails_emailAddress"/>
            <input name="emailAddress" value='<c:out value="${bookingComponent.nonPaymentData['emailAddress']}"/>'
               alt="Your email address is required to complete your booking. Please enter your email address.|N|EMAIL"
               type="text" required="true" maxlength="100" id="<c:out value="${emailAddress}"/>"
               requiredError="Your email address" validationType="Email" validationTypeError="Email address is not in the correct format" />
        </td>
       </tr>
     </table>
   </div>
   <div class="shadedBoxAttn">
     <p>
       <input id="newsLetter" type="checkbox" name="e_checked" value=""  onblur ="setnewsLetter();" checked />
		        <input id="newsLetter1" type="hidden" name="ecommunication_checked" value="true"   />
       <label for="subscribeNewsLetter">If you would <strong>not</strong> like to receive information on the latest offers, discounts, products and services by e-communications from Thomson and other holiday divisions within TUI Travel PLC, please untick this box.</label>
     </p>
</div>
<div class="shadedBoxAttnBottom"></div>