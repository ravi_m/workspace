<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />
<c:set var="tomcatInstance" value="${param.tomcat}" />
<c:set var="token" value="${param.token}" />
<c:set var="error" value="${bookingComponent.errorMessage}"/>


<!-- following code added by sreenivasulu k-->
<%
    String tuiHeaderSwitch = com.tui.uk.config.ConfReader.getConfEntry("atcomres.switch" , "");
    pageContext.setAttribute("tuiHeaderSwitch2", tuiHeaderSwitch, PageContext.REQUEST_SCOPE);

%>

<c:choose>

   <c:when test="${tuiHeaderSwitch2 eq 'ON'}">
      <c:set value="TUI" var="textValue"/>
   </c:when>
   <c:otherwise>
      <c:set value="Thomson" var="textValue"/>
   </c:otherwise>
</c:choose>

<!-- above code added by sreenivasulu k-->



<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">

<%
    String creditCardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.GBP.cardChargeDetails" , "");
    pageContext.setAttribute("creditCardChargeDetails", creditCardChargeDetails, PageContext.REQUEST_SCOPE);
    String isMasterCardGiftApplied = com.tui.uk.config.ConfReader.getConfEntry("isMasterCardGiftApplied" , "");
    pageContext.setAttribute("isMasterCardGiftApplied", isMasterCardGiftApplied, PageContext.REQUEST_SCOPE);

%>
</c:if>


<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='EUR'}">
        <%
        String creditCardChargeDetails = com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.EUR.cardChargeDetails" , "");
        pageContext.setAttribute("creditCardChargeDetails", creditCardChargeDetails, PageContext.REQUEST_SCOPE);
        %>
</c:if>

<c:set var="cardChargeDetails" value="${creditCardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<fmt:formatNumber value="${cardChargeArray[0]}" type="number" var="cardCharge"/>

<%
   String[] defaultValue=null;
    String debitCardChargeDetails[] = com.tui.uk.config.ConfReader.getStringValues("AtcomRes.GBP.debitCardChargeDetails" ,defaultValue,",");

    %>
    <script type="text/javascript" src="/cms-cps/fcx/js/vs.js"></script>
<%
String applyCreditCardSurchargeGBP=com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.GBP.applyCreditCardSurcharge", null);
pageContext.setAttribute("applyCreditCardSurchargeGBP", applyCreditCardSurchargeGBP, PageContext.REQUEST_SCOPE);
String applyCreditCardSurchargeEUR=com.tui.uk.config.ConfReader.getConfEntry("AtcomRes.EUR.applyCreditCardSurcharge", null);
pageContext.setAttribute("applyCreditCardSurchargeEUR", applyCreditCardSurchargeEUR, PageContext.REQUEST_SCOPE);
%>
<script>
applyCreditCardSurchargeGBP ="<%=applyCreditCardSurchargeGBP%>";
applyCreditCardSurchargeEUR ="<%=applyCreditCardSurchargeEUR%>";
</script>
<div class="detailsPanel" id="paymentDetailsPanel">
          <div class="top">
            <div class="right"></div>
          </div>
          <div class="body">
            <div class="right">
              <div class="content">
                <div class="section">
                  <%@include file="depositSection.jspf"%>
                  <h2 id="paymentDetails"><span>Payment details</span></h2>
                  <div class="mandatoryKey">Please complete all fields marked with <span class="mandatory">*</span></div>
                  <!-- errors -->
                <c:if test='${not empty error}'>
                    <div class="errorContainer">
                       <c:out value="${bookingComponent.errorMessage}" escapeXml="false"/>
                    </div>
                </c:if>
                  <div class="paymentSectionWrapper">
                  <div class="sectionContent widthController">



                    <ul class="controlGroups">
                      <li class="controlGroup">
                      <!-- <p class="fieldLabel">Total amount due </p>
                        <div id="totalAmtDue" class="amount"><c:out value="${bookingComponent.totalAmount.symbol}" escapeXml="false"/><span id="totalAmountDue"><fmt:formatNumber  value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
                  minFractionDigits="2" pattern="#,##,###.##"/></span></div> -->
                    <fmt:formatNumber var="transactionTotal"
        value="${bookingInfo.calculatedTotalAmount.amount}" type="number"
        maxFractionDigits="2" minFractionDigits="2" scope="request" pattern="######.##" />
    <fmt:formatNumber var="amountWithCardCharge"
        value="${transactionTotal+applicableCardChargeForSelectedAmount}"
        type="number" maxFractionDigits="2" minFractionDigits="2"
        scope="request" groupingUsed="false" />
    <fmt:formatNumber var="amountWithDebitCardCharge"
        value="${transactionTotal+applicableDebitCardChargeForSelectedAmount}"
        type="number" maxFractionDigits="2" minFractionDigits="2"
        scope="request" groupingUsed="false" />
    <ul class="paymentAmountRight">
        <li class="fontWeightNormal" id="amountWithoutCardCharge" style=" float: left;">
        <span id="spanAmountWithoutCardCharge" class="amount">
        <c:out value="${currencySymbol}" escapeXml="false" />
		<c:out value="${amountWithDebitCardCharge}" /></span>
<span>Payable today if paying by Debit Card</span></li>

	<li class="fontWeightNormal" id="thomsonCardCharge" style=" float: left; display :none">
        <span id="spanthomsonCardCharge" class="amount">
        <c:out value="${currencySymbol}" escapeXml="false" /><c:out
            value="${amountWithDebitCardCharge}" /></span>
            <span>Includes no extra charge for using <c:out value="${textValue}"/> Credit Card</span></li>

	<li class="fontWeightNormal" id="masterCardGiftCardCharge" style=" float: left; display :none">
        <span id="spanthomsonCardCharge" class="amount">
        <c:out value="${currencySymbol}" escapeXml="false" /><c:out
            value="${amountWithDebitCardCharge}" /></span>
            <span>Includes no extra charge for using Gift Card</span></li>

        <li class="fontWeightNormal" id="amountWithCardCharge" style="margin-left: 10px;">
        <span id="spanAmountWithCardCharge" class="amount">
        <c:out value="${currencySymbol}" escapeXml="false" /><c:out
            value="${amountWithCardCharge}" /></span> <span>Payable today if
        paying by Credit Card<br>
     
        <p class="note">
        	<c:choose>
   				<c:when test="${bookingComponent.totalAmount.currency.currencyCode=='EUR'}">
      				 <c:if test="${applyCreditCardSurchargeEUR eq 'true'}">
      					  Includes <c:out value="${cardCharge}" />%
       					  Credit Card surcharge
        			</c:if>
   				</c:when>
   				<c:when test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
    				  <c:if test="${applyCreditCardSurchargeGBP eq 'true'}">
       					 Includes <c:out value="${cardCharge}" />%
       					 Credit Card surcharge
       				  </c:if>
   				</c:when>
   				<c:otherwise>
  			 </c:otherwise>
			</c:choose>
        </p>
        </span></li>


    </ul>

                         <script type="text/javascript">
       window.onload=
        function()
        {
		document.getElementById("thomsonCardCharge").style.display = "none";
		document.getElementById("masterCardGiftCardCharge").style.display = "none";
           amountUpdationWithCardCharge.updateCardChargeForFCX()

           amountUpdationWithCardCharge.updateCardChargeForFCX()
        }
    </script>
                      </li>
                     <li class="controlGroup">
                        <p class="fieldLabel"><label for="payment_type_0">Payment method</label></p>
                        <div class="controls">
                          <select onchange="javascript:setIssueField(this),CardTypeChangeHandler.handleCardSelection(),amountUpdationWithCardCharge.updatePaymentInfoForFCX(),resetPaymentField();" name="cardType" id="payment_0_cardType" required="true"
                           requiredError="Payment method is required." requiredEmpty="none"  class="longField">
                              <option selected value="">Please select</option>
                               <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">

                               <c:choose>
						<c:when
							test="${cardDetails.paymentCode eq 'MASTERCARD_GIFT'}">
							<c:if test="${isMasterCardGiftApplied eq 'true'}">

                                 <option value='${cardDetails.paymentCode}' >
                                    ${cardDetails.paymentDescription}
                                 </option>

							 </c:if>
						</c:when>

						<c:otherwise>
							   <option value='${cardDetails.paymentCode}' >
                                           <!-- following code added by sreenivasulu k-->
							   <c:set var="type" value="${cardDetails.paymentDescription}"/>

							  <c:choose>
								<c:when test="${fn:containsIgnoreCase(cardDetails.paymentDescription, 'Thomson')}">
								  <c:if test="${tuiHeaderSwitch2 eq 'ON'}">
								 TUI CreditCard
								 </c:if>
								<c:if test="${tuiHeaderSwitch2 ne 'ON'}">
								    <c:out value="${cardDetails.paymentDescription}"
								escapeXml='false' />
								 </c:if>

								</c:when>
								<c:otherwise>
											<c:out value="${cardDetails.paymentDescription}"
								escapeXml='false' />
								</c:otherwise>
							</c:choose>

							   <!-- above code added by sreenivasulu k-->

                                 </option>
						</c:otherwise>
</c:choose>

                              </c:forEach>
                           </select><span class="mandatory">*</span>
                        </div>
                      </li>
                      <li class="controlGroup">
                        <p class="fieldLabel"><label for="payment_0_nameOnCard">Name of card holder</label></p>
                        <div class="controls">
                          <c:set var="cardHolderName" value="paymentDetails_cardHolderName"/>
                          <input type="text"
                             name="payment_0_nameOnCard" id="payment_0_nameOnCard"
                             maxlength="30" class="inputWhole longField"
                             required="true" requiredError="Name of card holder is required."
                             value="<c:out value="${bookingComponent.nonPaymentData[cardHolderName]}" />"
                             validationTypeError="Card holder name is invalid." validationType="Alphabet">
                             <span class="mandatory">*</span>

                        </div>
                      </li>
                      <li class="controlGroup">
                        <p class="fieldLabel"><label for="payment_cardNumber">Card number</label></p>
                        <div class="controls">
                          <input name="payment_0_cardNumber" type="text"
           id="payment_cardNumber" class="longField" class="inputWhole" required="true"
           requiredError="Card number is required." validationTypeError="Card number is invalid."
           maxLengthError="Card number is invalid. Too short?" maxlength="20"
           minLengthError="Card number is invalid. Too long?" validationType="Mod10" autocomplete="off">

                          <span class="mandatory">*</span>
                        </div>
                      </li>
                      <li class="controlGroup">
                        <p class="fieldLabel"><label for="payment_expiryDateMonth">Expiry date</label></p>
                        <div class="controls">
                          <jsp:useBean id="now" class="java.util.Date" />
                          <fmt:formatDate var ="expiryMonthSelected1" type="date" value="${now}" pattern="MM"/>

                          <select id="payment_expiryDateMonth" name="payment_0_expiryMonth" required="true" requiredError="Expiry date is required.">
                           <c:forEach begin="1" end="12" varStatus="loopStatus">
                              <fmt:formatNumber var="expiryMonth" value="${loopStatus.count}" type="number" minIntegerDigits="2" pattern="##"/>
                              <c:choose>
                                 <c:when test="${expiryMonthSelected1 ==  expiryMonth}">
                                    <option value="<c:out value="${expiryMonth}"/>" selected="selected"><c:out value="${expiryMonth}"/></option>
                                 </c:when>
                                 <c:otherwise>
                                    <option value="<c:out value="${expiryMonth}"/>"><c:out value="${expiryMonth}" /></option>
                                 </c:otherwise>
                              </c:choose>
                           </c:forEach>
                          </select>
                          <select id="payment_expiryDateYear" name="payment_0_expiryYear" required="true"
                          requiredError="Expiry Year is required." validationTypeError="Please select a valid year in the future.">
                           <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
                              <option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>
                           </c:forEach>
                           </select>
                           <span class="mandatory">*</span>
                        </div>
                      </li>
                      <li class="controlGroup">
                        <p class="fieldLabel"><label for="payment_0_securityCode">CVV/CID code</label></p>
                        <div class="controls">
                          <input name="payment_0_securityCode" id="payment_0_securityCode" type="text"  autocomplete="off"
                           maxlength="4" id="payment_securityCode"
                           class="inputWhole" required="true" requiredError="CVV/CID code is required."
                           validationtype="Numeric" validationTypeError="CVV/CID code is invalid." minlength="3"
                           maxlengtherror="CVV/CID code is invalid. Too Large?"  minLengthError="CVV/CID code is invalid. Too short?">

                           <span class="mandatory">*</span>
                           <p class="fieldInformation">More about <a id="cvvDetails" class="sticky stickyOwner" href="#" title="View CVV details" rel="external">CVV/CID code</a></p>
                        </div>


                          </li>

                        <li class="controlGroup">
                            <p class="fieldLabel"><label for="payment_0_securityCode">Payment Notes</label></p>
                            <div class="controls">
                                <textarea id="payment_notes" name="payment_notes" rows="3" cols="30" maxLength="100" validationType="PaymentNotes" validationTypeError="No special character allowed in payment notes."></textarea>
                            </div>
                          </li>

                          <li class="controlGroup" id="issuerequiredlabel" style="display:none">
                          <div>
                             <p><label>For Maestro  Card Users only - Please enter  the issue number of your card.</label></p>
                             </div>
                          </li>
                          <li class="controlGroup" id="issuerequiredfield" style="display:none">
                          <div>
                            <p class="fieldLabel"><label for="payment_0_issueNumber">Issue Number</label></p>
                              <div class="controls">
                                  <input name="issueNumber" type="text" id="issueNumber" validationtype="issueno" validationtypeerror="Valid Issue Number Required" maxlength="4" >
                              </div>
                              </div>
                         </li>

                      <li class="controlGroup">
                      
                      <c:choose>
   				<c:when test="${bookingComponent.totalAmount.currency.currencyCode=='EUR'}">
      				 <c:if test="${applyCreditCardSurchargeEUR eq 'true'}">
                      Bookings made by American Express, MasterCard Credit or Visa Credit cards will incur a fee of
                      <c:out value='${cardCharge}'/>% per transaction. If you are booking using Maestro, MasterCard Debit, Visa/Delta debit cards, Visa Electron or <c:out value="${textValue}"/> Credit Card there is no charge.
        			</c:if>
   				</c:when>
   				<c:when test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
    				  <c:if test="${applyCreditCardSurchargeGBP eq 'true'}">
       					 Bookings made by American Express, MasterCard Credit or Visa Credit cards will incur a fee of
                      <c:out value='${cardCharge}'/>% per transaction. If you are booking using Maestro, MasterCard Debit, Visa/Delta debit cards, Visa Electron or <c:out value="${textValue}"/> Credit Card there is no charge.
       				  </c:if>
   				</c:when>
   				<c:otherwise>
  			 </c:otherwise>
			</c:choose>
                      </li>

                    </ul>

                    <!--start overlay section-->
                    <!--start overlay visa-->
                    <div class="genericOverlay" id="VisaDetailsOverlay">
                      <div class="top">
                        <div class="right"> </div>
                      </div>
                      <div class="body">
                        <div class="right">
                          <div class="content">
                            <div class="popupTitle">
                              <h3>Verified by Visa</h3>
                              <a class="close" id="VisaDetailsClose" href="#" title="Close overlay">Close</a></div>
                             <p>We've introduced Verified by Visa - a security programme created by Visa to give you peace of mind when shopping online. All you need to do is register once whilst paying on our website. You'll be asked to set up a password that you can then use for any future payments you make with your card on participating websites.
                              </p>
                          </div>
                        </div>
                      </div>
                      <div class="bottom">
                        <div class="right"></div>
                      </div>
                    </div><!--end overlay-->


                  <!--start overlay mastercard-->
                <div class="genericOverlay" id="mastercardDetailsOverlay">
                  <div class="top">
                    <div class="right"> </div>
                  </div>
                  <div class="body">
                    <div class="right">
                      <div class="content">
                        <div class="popupTitle">
                          <h3>MasterCard SecureCode</h3>
                          <a class="close" id="mastercardDetailsClose" href="#" title="Close overlay">Close</a></div>
                         <p>We've introduced MasterCard SecureCode - a security programme created by MasterCard to give you peace of mind when shopping online. All you need to do is register once whilst paying on our website. You'll be asked to set up a password that you can then use for any future payments you make with your card on participating websites.
                          </p>
                      </div>
                    </div>
                  </div>
                  <div class="bottom">
                    <div class="right"></div>
                  </div>
                </div>
                <!--end overlay-->




                <!-- Start overlay cvv -->
                  <div class="genericOverlay" id="cvvDetailsOverlay">
                    <div class="top">
                      <div class="right"> </div>
                    </div>
                    <div class="body">
                      <div class="right">
                        <div class="content">
                          <div class="popupTitle">
                            <h3>CVV/CID Code</h3>
                            <a class="close" id="cvvDetailsClose" href="#" title="Close overlay">Close</a></div>
                          <p>Entering the CVV or CID code serves as an additional security measure for your credit card transactions.
                          </p><img id="cvvCard" src="/cms-cps/atcomres/images/cvv-credit-card.gif" alt="CVV" />
                          <p><strong>Mastercard and Visa Credit Card</strong></p>
                           <p>The Card Verification Value (CVV) is a three-digit security code.
                           You will find this code on the back of your credit card on the signature strip: it consists of the last three digits.
                            </p>
                        </div>
                      </div>
                    </div>
                    <div class="bottom">
                      <div class="right"></div>
                    </div>
                  </div><!--end overlay-->

                  </div>

                    <!--start logos section-->
                    <div class="logos">
                        <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
                           <c:if test="${threeDLogos == 'mastercardgroup'}">
                                    <div class="logoContainer mastercardContainer">
                                       <a href="javascript:void(0);" id="mastercardDetails" class="stickyOwner"><img
                                        src="/cms-cps/atcomres/images/mastercard-logo.gif" alt="View MasterCard SecureCode details"/>
                                    </a>
                                     <a href="javascript:void(0);" id="mastercardDetails" class="sticky stickyOwner">
                                     Learn more
                                    </a>

                                    </div>
                           </c:if>
                        </c:forEach>
                        <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
                           <c:if test="${threeDLogos == 'visagroup'}">
                              <div class="logoContainer visaContainer">
                                 <a href="javascript:void(0);" id="VisaDetails" class="stickyOwner"><img src="/cms-cps/atcomres/images/visa-logo.gif" alt="View Visa details" />        </a>
                                <a href="javascript:void(0);" id="VisaDetails" class="sticky stickyOwner">
                                Learn more     </a>

                    </div>
                           </c:if>
                        </c:forEach>
                   </div>
                   <!-- end logos section -->
                   </div><!-- end div paymentSectionWrapper-->

                </div>
              </div>
              </div>

          </div>
          <div class="bottom">
            <div class="right"></div>
          </div>
        </div>


<%-- *******Essential fields ********************  --%>
<input type="hidden" name="payment_0_chargeAmount" id="payment_0_chargeIdAmount"  value="0" />
<input type="hidden" name="total_transamt"  id="total_transamt"
   value="<fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
                  minFractionDigits="2" pattern="#####.##"/>" />

<input type="hidden" id="part_payment_min_amount" name="part_payment_min_amount" value="${bookingComponent.nonPaymentData.part_payment_min_amount}"/>

<input type="hidden" name="payment_totamtpaid"  id="payment_totamtpaid"
   value="<fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
   minFractionDigits="2" pattern="#####.##"/>" />
<input type="hidden" id="bookingCalAmt" name="bookingCalAmt" value="${bookingInfo.calculatedTotalAmount.amount}"/>

<%-- should carry payment method - Dcard etc  --%>
<input type="hidden" id="payment_0_paymentmethod" name="payment_0_paymentmethod" value=""/>

<%-- no of transaction is 1  --%>
<input type="hidden"  id="payment_0_totalTrans" value="1" name="payment_totalTrans"/>

<%-- should carry cardtype like AMERICAN_EXPRESS etc  --%>
<input type="hidden" id="payment_0_paymenttypecode" value="" name="payment_0_paymenttypecode"/>

<%-- *******End Essential fields ********************  --%>


