<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />


<h2>
    <img id="hl_paxnames" src="/cms-cps/atcomres/images/head_passnames_dkblauhg.gif" alt="" border="0">
</h2>
<div class="control">
    Passenger names must be entered exactly as stated on photo ID / passport.<br>
    Once your booking has been confirmed, any changes to passenger details will be subject to a charge.

    <table border="0" cellpadding="5" cellspacing="0">
       <c:set var="adultCount" value="${bookingComponent.nonPaymentData['passenger_ADT_count']}"/>
       <c:if test="${adultCount >= 1}">
       <c:set var="passengerCount" value="1" />
       <tr>
          <td colspan="6" style="padding: 15px 0 2px 0;">
             <img id="subhl_adults" src="/cms-cps/atcomres/images/head_adults_weisshg.gif" alt="" border="0">
          </td>
       </tr>
       <tr>
          <td></td>
          <td valign="top" nowrap style="padding: 0 10px 0 0;">Title *</td>
          <td valign="top" style="padding: 0 10px 0 0;">First name *</td>
          <td valign="top" style="padding: 0 10px 0 0;">Last name *</td>
          <td></td>
          <td></td>
       </tr>
       <c:forEach var="count" begin="1" end="${adultCount}">
       <tr>
          <td>
             <c:out value="${count}"/>
          </td>
          <td style="padding: 5px 10px 0 0;">
          <c:set var="initial" value="passenger_${passengerCount}_title"/>
             <select name="<c:out value="${initial}"/>" id="<c:out value="${initial}"/>" required="true"
                description="Adult ${passengerCount}" requiredError="{description} : Title is required." requiredEmpty="none"
                style="width:50px">
                     <option value="none"></option>
                     <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[initial] == 'Mr'}">selected</c:if>>Mr.</option>
                     <option value="Ms" <c:if test="${bookingComponent.nonPaymentData[initial] == 'Ms'}">selected</c:if>>Ms.</option>
                     <option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[initial] == 'Mrs'}">selected</c:if>>Mrs.</option>
                     <option value="Dr" <c:if test="${bookingComponent.nonPaymentData[initial] == 'Dr'}">selected</c:if>>Dr.</option>
                     <option value="Mstr" <c:if test="${bookingComponent.nonPaymentData[initial] == 'Mstr'}">selected</c:if>>Mstr.</option>
                     <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[initial] == 'Miss'}">selected</c:if>>Miss.</option>
             </select>
          </td>
          <td style="padding: 5px 10px 0 0;">
             <c:set var="foreName" value="passenger_${passengerCount}_foreName"/>
             <input type="text" maxlength="15" class="medium" style="width:100px" required="true"
                name="<c:out value="${foreName}"/>" id="<c:out value="${foreName}"/>"
                description="Adult ${passengerCount}"
                alt="PLEASE ENTER VALID FORE NAME.|Y|ALPHA" requiredError="{description} : Fore name is required."
                value="<c:out value="${bookingComponent.nonPaymentData[foreName]}"/>"
             />
          </td>
          <td style="padding: 5px 10px 0 0;">
             <c:set var="lastName" value="passenger_${passengerCount}_lastName"/>
             <input  type="text" maxlength="15" class="medium" style="width:100px" required="true"
                name="<c:out value="${lastName}"/>" id="<c:out value="${lastName}"/>"
                description="Adult ${passengerCount}"
                alt="PLEASE ENTER VALID LAST NAME.|Y|ALPHA" requiredError="{description} : Last name is required."
                value="<c:out value="${bookingComponent.nonPaymentData[lastName]}" />"
             />
          </td>
          <td style="padding: 5px 10px 0 0;">
             <c:if test="${count == 1}">
                (Lead Passenger)
             </c:if>
          </td>
       </tr>
       <c:set var="passengerCount" value="${passengerCount+1}" />
       </c:forEach>
       </c:if>

       <%-- children --%>
       <c:set var="childCount" value="${bookingComponent.nonPaymentData['passenger_CHD_count']}"/>
       <c:if test="${childCount > 0}">
       <c:set var="passengerCount" value="1" />
       <tr>
       <td colspan="6" style="padding: 15px 0 2px 0;">
          <img id="subhl_childen" src="/cms-cps/atcomres/images/head_children_weisshg.gif" alt="" border="0">
       </td>
       </tr>
       <tr>
       <td></td>
          <td valign="top" nowrap style="padding: 0 10px 0 0;">Title *</td>
          <td valign="top" style="padding: 0 10px 0 0;">First name *</td>
          <td valign="top" style="padding: 0 10px 0 0;">Last name *</td>
          <td>Age <br>(on date of return)</td>
          <td></td>
       </tr>
       <c:forEach var="count" begin="1" end="${childCount}">
       <tr>
          <td>
             <c:out value="${count}"/>
          </td>
          <td style="padding: 5px 10px 0 0;">
          <c:set var="initial" value="passenger_${passengerCount}_childTitle"/>
             <select name="<c:out value="${initial}"/>" id="<c:out value="${initial}"/>" required="true"
                description="Child ${passengerCount}"
                   requiredError="{description} : Title is required." requiredEmpty="none" style="width:50px">
                     <option value="none"></option>
                     <option value="Mstr" <c:if test="${bookingComponent.nonPaymentData[initial] == 'Mstr'}">selected</c:if>>Mstr.</option>
                     <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[initial] == 'Miss'}">selected</c:if>>Miss.</option>
             </select>
          </td>
          <td style="padding: 5px 10px 0 0;">
             <c:set var="childForeName" value="passenger_${passengerCount}_childForeName"/>
             <input type="text" maxlength="15" class="medium" style="width:100px" required="true"
                description="Child ${passengerCount}" requiredError="{description} : Fore name is required."
                name="<c:out value="${childForeName}"/>" id="<c:out value="${childForeName}"/>"
                value="<c:out value="${bookingComponent.nonPaymentData[childForeName]}" />"
                alt="PLEASE ENTER VALID FORE NAME.|Y|ALPHA"/>
          </td>
          <td style="padding: 5px 10px 0 0;">
             <c:set var="childLastName" value="passenger_${passengerCount}_childLastName"/>
             <input  type="text" maxlength="15" class="medium" style="width:100px" required="true"
                description="Child ${passengerCount}" requiredError="{description} : Last name is required."
                value="<c:out value="${bookingComponent.nonPaymentData[childLastName]}" />"
                name="<c:out value="${childLastName}"/>" id="<c:out value="${childLastName}"/>" alt="PLEASE ENTER VALID LAST NAME.|Y|ALPHA"/>
          </td>
          <td style="padding: 5px 10px 0 0;" nowrap>
          <c:set var="age" value="passenger_${passengerCount}_childAge"/>
             <select name="<c:out value="${age}"/>" id="<c:out value="${age}"/>" required="true"
                description="Child ${passengerCount}"
                requiredError="{description} : Age is required." requiredEmpty="none" style="width:80px">
                <c:forEach var="count" begin="2" end="15">
                   <c:set var="yearCount" value="${count}"/>
                   <option value="<c:out value="${count}"/>"
                   <c:if test="${bookingComponent.nonPaymentData[age] == yearCount}">selected</c:if>><c:out value="${count}"/> years</option>

                </c:forEach>
             </select>
          </td>
       </tr>
       <c:set var="passengerCount" value="${passengerCount+1}" />
       </c:forEach>
       </c:if>
       </table>

       <table border="0" cellpadding="5" cellspacing="0">
       <%-- Infant --%>
       <c:set var="infantCount" value="${bookingComponent.nonPaymentData['passenger_INF_count']}"/>
       <c:if test="${infantCount > 0}">
       <c:set var="passengerCount" value="1" />
       <tr>
       <td colspan="6" style="padding: 15px 0 2px 0;">
          <img id="subhl_infants" src="/cms-cps/atcomres/images/head_infants_weisshg.gif" alt="" border="0">
       </td>
       </tr>
       <tr>
       <td></td>
          <td valign="top" nowrap style="padding: 0 10px 0 0;">First name *</td>
          <td valign="top" style="padding: 0 10px 0 0;">Last name *</td>
          <td valign="top" style="padding: 0 10px 0 0;">Age <br>(on date of return)</td>
          <td>Traveling with</td>
          <td></td>
       </tr>
       <c:forEach var="count" begin="1" end="${infantCount}">
       <tr>
          <td>
             <c:out value="${count}"/>
          </td>
          <td style="padding: 5px 10px 0 0;">
             <c:set var="infantForeName" value="passenger_${passengerCount}_infantForeName"/>
             <input type="text" maxlength="15" class="medium" style="width:100px" required="true"
                description="Infant ${passengerCount}" requiredError="{description} : Fore name is required."
                value="<c:out value="${bookingComponent.nonPaymentData[infantForeName]}" />"
                name="<c:out value="${infantForeName}"/>" id="<c:out value="${infantForeName}"/>" alt="PLEASE ENTER VALID FORE NAME.|Y|ALPHA"/>
          </td>
          <td style="padding: 5px 10px 0 0;">
             <c:set var="infantLastName" value="passenger_${passengerCount}_infantLastName"/>
             <input  type="text" maxlength="15" class="medium" style="width:100px" required="true"
                description="Infant ${passengerCount}" requiredError="{description} : Last name is required."
                value="<c:out value="${bookingComponent.nonPaymentData[infantLastName]}" />"
                name="<c:out value="${infantLastName}"/>" id="<c:out value="${infantLastName}"/>" alt="PLEASE ENTER VALID LAST NAME.|Y|ALPHA"/>
          </td>
          <td style="padding: 5px 10px 0 0;" nowrap>
          <c:set var="age" value="passenger_${passengerCount}_infantAge"/>
             <select name="<c:out value="${age}"/>" id="<c:out value="${age}"/>" required="true"
                description="Infant ${passengerCount}"
                requiredError="{description} : Age is required." requiredEmpty="none" style="width:90px">
                <c:forEach var="count" begin="0" end="23">
                   <c:set var="ageCount" value="${count}"/>
                   <option value="<c:out value="${count}"/>"
                   <c:if test="${bookingComponent.nonPaymentData[age] == ageCount}">selected</c:if>><c:out value="${count}"/> months</option>
                </c:forEach>
             </select>
          </td>
          <c:set var="adultCount" value="${bookingComponent.nonPaymentData.passenger_ADT_count}"/>
          <td style="padding: 5px 10px 0 0;">
          <c:set var="travelWith" value="passenger_${passengerCount}_travelWith"/>


             <select name="<c:out value="${travelWith}"/>" id="<c:out value="${travelWith}"/>" required="true"
                description="Infant ${passengerCount}"
                requiredError="{description} : Traveling with is required." requiredEmpty="none" style="width:70px">
             <c:forEach var="count" begin="1" end="${adultCount}">
                <c:set var="travellingWith" value="Adult ${count}"/>
                   <option value="Adult <c:out value="${count}"/>"
                 <c:if test="${count == passengerCount}">selected</c:if>>Adult <c:out value="${count}"/></option>Adult <c:out value="${count}"/></option>
                </c:forEach>
             </select>
          </td>
       </tr>
       <c:set var="passengerCount" value="${passengerCount+1}" />
       </c:forEach>
       </c:if>
    </table>
    <table border="0" cellpadding="5" cellspacing="0"></table>
    <table border="0" cellpadding="5" cellspacing="0"></table>

 </div>
