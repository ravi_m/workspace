<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:formatNumber
	value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}"
	var="totalCostingLine" type="number" pattern="####"
	maxFractionDigits="2" minFractionDigits="2" />
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page" />
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:set var="multiCentre" value="${bookingComponent.multiCentre}" />
<c:set var="zeroDepositDDExist" value="false" />
		<c:forEach var="depositComponent"
					items="${bookingComponent.depositComponents}"
					varStatus="count">
					 <c:set var="zeroDepositDDExist" value="${depositComponent.depositAmount.amount gt 0 ? false : true}"
			scope="page" /> 
		</c:forEach>
<fmt:formatNumber
	value="${bookingComponent.lockYourPriceSummary.outstandingAmount.amount}"
	var="outStandingAmount" type="number" pattern="####"
	maxFractionDigits="2" minFractionDigits="2" />
<c:set var="outStandingcost" value="${fn:split(outStandingAmount, '.')}" />

<c:if test="${not empty bookingComponent.breadCrumbTrail}">
	<c:set var="searchUrl"
		value="${bookingComponent.breadCrumbTrail['HUBSUMMARY']}" />
	<c:set var="passengersUrl"
		value="${bookingComponent.breadCrumbTrail['PASSENGERS']}" />
	<c:if test="${not empty  bookingComponent.depositComponents}">
		<c:set var="depositDetails"
			value="${bookingComponent.depositComponents }"></c:set>
		<c:set var="CONST_LOW_DEPOSIT">lowDeposit</c:set>
		<c:set var="CONST_DEPOSIT">deposit</c:set>
	</c:if>
	<c:choose>
		<c:when test="${multiCentre eq true}">
			<c:set var="optionsUrl"
				value="${bookingComponent.breadCrumbTrail['CENTRESELECTION']}" />
		</c:when>
		<c:otherwise>
			<c:set var="optionsUrl"
				value="${bookingComponent.breadCrumbTrail['HOTEL']}" />
		</c:otherwise>
	</c:choose>
</c:if>

<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page" />
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:set var="discountFlag" value="false" />
<c:set var="priceBreakDown"
	value="${bookingComponent.pricingDetails['priceBreakDown']}" />
<c:choose>
   <c:when test="${bookingComponent.nonPaymentData['TUIHeaderSwitch'] eq 'ON'}">
      <c:set value="true" var="tuiLogo" />
   </c:when>
   <c:otherwise>
      <c:set value="false" var="tuiLogo" />
   </c:otherwise>
</c:choose>

<c:set var="discountAmount" value='${0.00}' />
<c:forEach var="priceComponent" items="${priceBreakDown}"
	varStatus="count">

	<c:if test="${not empty priceComponent.onlineDiscountData}">
	<c:set var="discountAmount" value="${discountAmount+priceComponent.amount.amount}" />
		<c:set var="discountFlag" value="true" />
	</c:if>
	<c:if test="${(priceComponent.offerType == 'ANCILLARY_DISCOUNTS')}">
		<c:set var="discountFlag" value="true" />
		<c:set var="discountAmount" value="${discountAmount+priceComponent.amount.amount}" />
	

	</c:if>	
</c:forEach>

<c:set var="depositComponentval" value="false" />
<c:forEach var="eachDepositComponentAcc" items="${depositDetails}"
	varStatus="count">
	<c:if test="${not empty eachDepositComponentAcc.depositDataPP}">
		<c:set var="depositComponentval" value="true" />
	</c:if>
</c:forEach>

<c:set var="perPersonPrice" value="${bookingComponent.pricingDetails['PER_PERSON_PRICE']}"/>
<c:if test="${not empty perPersonPrice}">
	<c:forEach var="price" items="${perPersonPrice}">
<fmt:formatNumber value="${price.amount.amount}" var="perPersonPriceCostingLine" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####" />
		<c:set var="perpersonpricecost" value="${fn:split(perPersonPriceCostingLine, '.')}" />
	</c:forEach>
</c:if>

<c:set var="outstandingPerPersonPrice" value="${bookingComponent.lockYourPriceSummary.outstandingAmountPP.amount}"/>
<c:if test="${not empty outstandingPerPersonPrice}">
	
<fmt:formatNumber value="${outstandingPerPersonPrice}" var="mmbPerPersonPriceCostingLine" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####" />
		<c:set var="outstandingPerpersonpricecost" value="${fn:split(mmbPerPersonPriceCostingLine, '.')}" />
	
</c:if>

<fmt:parseNumber var="fcpCount" integerOnly="true" type="number" value="${bookingComponent.nonPaymentData['freeChildPlaceCount']}" />

<div id="book-flow-header" class="tuiglobalHeaderCnt ski">
  <div class="content-width">
    <div id="crystal-ski">
      <div id="logo-section">
        <a href="https://www.crystalski.ie/" class="ski-link"
          ><div id="logo" class="skilogo"></div
        ></a>
        <div class="right-section ski">
          <div class="nomobile nominitablet call-center-info tooltip-block">
            <a
              class="telDiv centerAlign"
              href="tel:01 653 3504"
              aria-label="tel:01 653 3504"
              rel="noopener"
            >
              <svg viewBox="0 0 15 24" id="icon-phone">
                <path
                  d="M.71631 4.259313c.245396-.417005.43626-.86181.708922-1.251016C2.24322 1.785082 3.388401.951072 4.778978.478466c.381728-.111202.736189-.278004 1.09065-.389205.817987-.250203 1.472376.0278 1.826837.80621.163598.361404.327195.75061.408994 1.167614.163597.83401.327194 1.668021.43626 2.502032.054532.389205 0 .80621-.0818 1.195415-.163597.75061-.681655 1.223215-1.390577 1.445618l-1.472377.500406c-.736188.222403-1.117915.695009-1.22698 1.445618-.136331.80621 0 1.58462.190864 2.36303.35446 1.473418.87252 2.891236 1.581441 4.197852.327195.611608.708922 1.167615 1.281513 1.55682.490792.361404 1.036117.389205 1.581441.139001.518059-.222402 1.036117-.444805 1.554175-.639408.954319-.333604 1.74504-.111201 2.399429.667209.299928.361404.545324.722809.763454 1.112014.381727.695008.708922 1.390017 1.036117 2.112826.109065.250203.163597.500406.21813.77841.109065.722809-.163598 1.278816-.79072 1.55682-.87252.361404-1.74504.778409-2.672091.91741-1.172448.194603-2.31763.055601-3.435546-.389204-1.554175-.639408-2.781155-1.723622-3.790006-3.058038-.899785-1.167615-1.581441-2.44643-2.181298-3.808648-.927052-2.140627-1.635974-4.336854-1.963169-6.644283-.190863-1.417818-.21813-2.835636.081799-4.225653.054532-.305804.163597-.611608.21813-.889611.109065-.250203.190863-.444806.272662-.639408z"
                ></path>
              </svg>

              <span class="telNumber">01 653 3504</span>
              <svg viewBox="0 0 15 9" id="icon-arrow-down">
                <path
                  d="M1.907.342a1.084 1.084 0 0 0-1.58 0 1.204 1.204 0 0 0 0 1.65L6.71 8.658a1.084 1.084 0 0 0 1.58 0l6.383-6.666a1.204 1.204 0 0 0 0-1.65 1.084 1.084 0 0 0-1.58 0L7.497 6.187 1.907.342z"
                ></path>
              </svg>
            </a>
            <div class="time-info nomobile nominitablet tooltiptext">
				<span class="contactus_header">Contact us</span>
				<p>
					See our call centre opening times and the different ways you can <a class="contactUs" href="https://www.crystalski.ie/contact-us/" aria-label="/contact-us/" target="_blank">contact us</a>
				</p>
            </div>
          </div>
          <div class="nodesktop notablet call-center-info">
            <a id="modal-trigger" class="centerAlign">
              <svg viewBox="0 0 15 24" id="icon-phone">
                <path
                  d="M.71631 4.259313c.245396-.417005.43626-.86181.708922-1.251016C2.24322 1.785082 3.388401.951072 4.778978.478466c.381728-.111202.736189-.278004 1.09065-.389205.817987-.250203 1.472376.0278 1.826837.80621.163598.361404.327195.75061.408994 1.167614.163597.83401.327194 1.668021.43626 2.502032.054532.389205 0 .80621-.0818 1.195415-.163597.75061-.681655 1.223215-1.390577 1.445618l-1.472377.500406c-.736188.222403-1.117915.695009-1.22698 1.445618-.136331.80621 0 1.58462.190864 2.36303.35446 1.473418.87252 2.891236 1.581441 4.197852.327195.611608.708922 1.167615 1.281513 1.55682.490792.361404 1.036117.389205 1.581441.139001.518059-.222402 1.036117-.444805 1.554175-.639408.954319-.333604 1.74504-.111201 2.399429.667209.299928.361404.545324.722809.763454 1.112014.381727.695008.708922 1.390017 1.036117 2.112826.109065.250203.163597.500406.21813.77841.109065.722809-.163598 1.278816-.79072 1.55682-.87252.361404-1.74504.778409-2.672091.91741-1.172448.194603-2.31763.055601-3.435546-.389204-1.554175-.639408-2.781155-1.723622-3.790006-3.058038-.899785-1.167615-1.581441-2.44643-2.181298-3.808648-.927052-2.140627-1.635974-4.336854-1.963169-6.644283-.190863-1.417818-.21813-2.835636.081799-4.225653.054532-.305804.163597-.611608.21813-.889611.109065-.250203.190863-.444806.272662-.639408z"
                ></path>
              </svg>
            </a>
          </div>

          <!-- Tertiary navigation elements for desktop-->
        </div>
      </div>
    </div>
</div>


<!-- The Modal -->
<div id="modal-contactus" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
    <section class="modalBody">
      <header>
        <h4 aria-label="title">CALL US</h4>
        <span aria-label="close" class="close">
          <svg
            width="16px"
            height="16px"
            class="closeIcon"
            viewBox="0 0 1024 1024"
            data-di-res-id="a6a4a90c-5964ace5"
            data-di-rand="1594036926335"
          >
            <path
              d="M511.719 445.381l-260.452-259.758c-43.788-44.335-110.322 22.168-65.966 65.934l259.883 260.326-259.883 260.326c-44.356 44.335 22.178 110.269 66.535 66.503l259.883-260.326 260.452 260.326c44.356 44.335 110.322-22.168 66.535-66.503l-260.452-260.326 260.452-260.326c44.356-44.335-22.178-110.269-66.535-66.503l-260.452 260.326z"
            ></path>
          </svg>
        </span>
      </header>
      <section aria-label="overlay content">
        <div class="contentBody">
          <a
            class="tele-link"
            href="tel:01 653 3504"
            aria-label="tel:01 653 3504"
            rel="noopener"
          >
            <svg viewBox="0 0 15 24" id="icon-phone">
              <path
                d="M.71631 4.259313c.245396-.417005.43626-.86181.708922-1.251016C2.24322 1.785082 3.388401.951072 4.778978.478466c.381728-.111202.736189-.278004 1.09065-.389205.817987-.250203 1.472376.0278 1.826837.80621.163598.361404.327195.75061.408994 1.167614.163597.83401.327194 1.668021.43626 2.502032.054532.389205 0 .80621-.0818 1.195415-.163597.75061-.681655 1.223215-1.390577 1.445618l-1.472377.500406c-.736188.222403-1.117915.695009-1.22698 1.445618-.136331.80621 0 1.58462.190864 2.36303.35446 1.473418.87252 2.891236 1.581441 4.197852.327195.611608.708922 1.167615 1.281513 1.55682.490792.361404 1.036117.389205 1.581441.139001.518059-.222402 1.036117-.444805 1.554175-.639408.954319-.333604 1.74504-.111201 2.399429.667209.299928.361404.545324.722809.763454 1.112014.381727.695008.708922 1.390017 1.036117 2.112826.109065.250203.163597.500406.21813.77841.109065.722809-.163598 1.278816-.79072 1.55682-.87252.361404-1.74504.778409-2.672091.91741-1.172448.194603-2.31763.055601-3.435546-.389204-1.554175-.639408-2.781155-1.723622-3.790006-3.058038-.899785-1.167615-1.581441-2.44643-2.181298-3.808648-.927052-2.140627-1.635974-4.336854-1.963169-6.644283-.190863-1.417818-.21813-2.835636.081799-4.225653.054532-.305804.163597-.611608.21813-.889611.109065-.250203.190863-.444806.272662-.639408z"
              ></path>
            </svg>
            <span>01 653 3504</span>
          </a>
          <div class="time-info">
			<span class="contactus_header">Contact us</span>
            <p>
				See our call centre opening times and the different ways you can <a class="contactUs" href="https://www.crystalski.ie/contact-us/" aria-label="/contact-us/" target="_blank">contact us</a>
			</p>
          </div>
        </div>
      </section>
    </section>
  </div>
</div>
<script>
  // Get the modal
  var modal = document.getElementById("modal-contactus");

  // Get the button that opens the modal
  var trigger = document.getElementById("modal-trigger");

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  // When the user clicks on the button, open the modal
  trigger.onclick = function () {
    modal.style.display = "block";
    document.body.style.position = "fixed";
  };

  // When the user clicks on <span> (x), close the modal
  span.onclick = function () {
    modal.style.display = "none";
    document.body.style.position = "";
  };

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function (event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  };
</script>
	

</div>
<c:choose>
			<c:when test="${bookingComponent.nonPaymentData['isResponsiveHubAndSpoke'] == 'Y' or bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow == true}">
				<c:set var="numOfSteps" value="${fn:length(bookingComponent.breadCrumbTrail)}" />
				<c:set var="stepWidth" value="${100 / numOfSteps}" />
				<c:set var="stepWidth" value="${fn:substringBefore(stepWidth, '.')}" />
<c:if test="${ bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne true}">

				<div class="span no-bottom-margin header-spacing breadcrumb">
					<div class="content-width">
						<div class="scroll" id="breadcrumb" data-scroll-dw="true"
							data-scroll-options='{"scrollX": true, "scrollY": false, "keyBindings": true, "mouseWheel": true}'>
							<ul id="indicators-sprite-${numOfSteps}"
								class="breadcrumbs-spacing">
								<c:forEach var="indicator"
									items="${bookingComponent.breadCrumbTrail}" varStatus="counter">
									<c:choose>

										<c:when test="${!counter.last}">
											<c:set var="indicatorHref" value="${indicator.value}" />

										</c:when>
										<c:otherwise>
											<c:set var="indicatorHref" value="#" />

										</c:otherwise>
									</c:choose>

									<li
										class="indicator  uc  sprite-${numOfSteps} ${counter.last ? '' : 'completed'} ${indicator.key=='Payment'? 'active':''}">

										<i class="arrow in ">${counter.count}</i> <c:if
											test="${!counter.last}">
											<a <c:if test="${indicator.key ne 'Payment'}">href="<c:out value='${indicatorHref}'/>"</c:if> class="ensLinkTrack" data-componentId="bookflowStepIndicator_comp">
												<span class="">${indicator.key}</span>
											</a>
										</c:if> <c:if test="${counter.last}">
											<a class="last"><span class="">${indicator.key}</span></a>
										</c:if> <i class="arrow out "></i>
									</li>

								</c:forEach>
							</ul>
						</div>
					</div>
					<div class="clear-both"></div>
				</div>
</c:if>


				<div class="component summary total-price  <c:if test='${errorPage}'> hide </c:if>">
					<div class="book-navigation content-width">
					<c:choose>
					<c:when test="${bookingComponent.lockYourPriceSummary.selected and bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow ne true}">
							
							<div class="price-panel lockThePrice fr">
							<div class="price-block dis-inblock vertal-m">
								<div class="uppercase"> Lock this price for ${bookingComponent.lockYourPriceSummary.cancellationPeriodInHours} hours </div>
								<div class="per-person-price">
									<c:if test="${not empty bookingComponent.lockYourPriceSummary.totalPrice}">
																		
										<div class="per-person-price"}>
										<span>
											<span class="lockPricePart1" >&euro;</span>
											<span class="lockPricePart2">${bookingComponent.lockYourPriceSummary.totalPrice}</span>
										</span>
										<span class="lockPricePP">(&euro;${bookingComponent.lockYourPriceSummary.ppPrice}pp)</span>
										</div>
									</c:if>
								</div>
								<div class="total-price"><span class="lockPriceTotal"> Total holiday price<span> &euro;<fmt:formatNumber  value="${totalcost[0]}" />.<c:out value="${totalcost[1]}"/></span></span></div>
							</div>
							</div>
							
					</c:when>
					<c:when test="${bookingComponent.lockYourPriceSummary.selected and bookingComponent.lockYourPriceSummary.mmbLockYourPriceFlow eq true}">
						<div class="price-panel fr">
							<div class="price-block dis-inblock vertal-m lypmmb">
								<div class="per-person-price">
									<span class="currency">&euro;</span>
									<c:if test="${not empty outstandingPerPersonPrice}">
										<span>
											<span class="part1"><fmt:formatNumber  value="${outstandingPerpersonpricecost[0]}" /></span>
											<span class="part2">.<c:out value="${outstandingPerpersonpricecost[1]}"/></span>pp
										</span>
									</c:if>
								</div>
								<div class="total-price">Outstanding Amount<span> &euro;<fmt:formatNumber  value="${outStandingcost[0]}" />.<c:out value="${outStandingcost[1]}"/></span></div>
							</div>

							<c:if test="${not empty priceBreakDown}">
								<div class="dis-block discount-block">
									<ul>
										<li class="dis-inblock" >
											 <c:forEach var="priceComponent" items="${priceBreakDown}" varStatus="count">
												<c:choose>
												   <c:when test="${not empty priceComponent.itemDescription and count.index==0}">
															Inc &euro;<fmt:formatNumber  value="${priceComponent.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##" /> discount
													</c:when>
													<c:otherwise>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</li>
										<c:if test="${not empty depositDetails}">
											<li class="dis-inblock">
											<c:choose>
											    <c:when test="${zeroDepositDDExist}">
													Book now with no deposit
											    </c:when>
												<c:otherwise>
												<c:forEach var="eachDepositComponent" items="${depositDetails}" varStatus="deposits">
													<c:choose>
														<c:when test="${eachDepositComponent.depositType == CONST_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
															<c:out value="${eachDepositComponent.depositDataPP}" />
														</c:when>
														<c:otherwise>
															<c:if test="${eachDepositComponent.depositType == CONST_LOW_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
																 <c:out value="${eachDepositComponent.depositDataPP}"/>
													        </c:if>
														</c:otherwise>
													</c:choose>   
												</c:forEach>
												</c:otherwise>
											</c:choose>	
										</li>									
										</c:if>
									</ul>
								</div>
							</c:if>
							<c:if test="${fcpCount gt 0}">
								<div class="fcp-alert"><p>${fcpCount} X FREE CHILD PLACE INCLUDED</p></div>
							</c:if>
						</div>
						</c:when>
					<c:otherwise>
						<div class="price-panel fr">
							<div class="price-block dis-inblock vertal-m">
								<div class="per-person-price">
									<span class="currency">&euro;</span>
									<c:if test="${not empty perPersonPrice}">
										<span>
											<span class="part1"><fmt:formatNumber  value="${perpersonpricecost[0]}" /></span>
											<span class="part2">.<c:out value="${perpersonpricecost[1]}"/></span>pp
										</span>
									</c:if>
								</div>
								<div class="total-price">Total price<span> &euro;<fmt:formatNumber  value="${totalcost[0]}" />.<c:out value="${totalcost[1]}"/></span></div>
							</div>
							<c:if test="${not empty priceBreakDown or discountFlag}">
								<div class="dis-block discount-block">
									<ul>
										<li class="dis-inblock" >
											<c:if test="${discountFlag}">

												Inc &euro;<fmt:formatNumber  value="${discountAmount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##" /> discount
													</c:if>
										</li>
										<c:if test="${not empty depositDetails}">
											<li class="dis-inblock">
											<c:choose>
											    <c:when test="${zeroDepositDDExist}">
													Book now with no deposit
											    </c:when>
												<c:otherwise>
												<c:forEach var="eachDepositComponent" items="${depositDetails}" varStatus="deposits">
													<c:choose>
														<c:when test="${eachDepositComponent.depositType == CONST_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
															<c:out value="${eachDepositComponent.depositDataPP}" />
														</c:when>
														<c:otherwise>
															<c:if test="${eachDepositComponent.depositType == CONST_LOW_DEPOSIT and not empty eachDepositComponent.depositDataPP and deposits.index==0}">
																 <c:out value="${eachDepositComponent.depositDataPP}"/>
													        </c:if>
														</c:otherwise>
													</c:choose>   
												</c:forEach>
												</c:otherwise>
											</c:choose>
											</li>
										</c:if>
									</ul>
								</div>
							</c:if>
							<c:if test="${fcpCount gt 0}">
								<div class="fcp-alert"><p>${fcpCount} X FREE CHILD PLACE INCLUDED</p></div>
							</c:if>
						</div>
						</c:otherwise>
						
					</c:choose>
					</div>
				</div>
			</c:when>
			<c:otherwise>
		<div class="span no-bottom-margin header-spacing breadcrumb">

			<div class="content-width">

				<div class="scroll" id="breadcrumb" data-scroll-dw="true"
					data-scroll-options="{&quot;scrollX&quot;: true, &quot;scrollY&quot;: false, &quot;keyBindings&quot;: true, &quot;mouseWheel&quot;: true}">

					<ul id="indicators-sprite-5" class="breadcrumbs-spacing">

						<li class="indicator  uc  sprite-5 completed "><i
							class="arrow in ">1</i> <a href="Javascript:void(0)"
							class="ensLinkTrack"
							data-componentid="bookflowStepIndicator_comp"> <span class="">Holiday
									Package</span>

						</a> <i class="arrow out "></i></li>


						<li class="indicator  uc  sprite-5 completed "><i
							class="arrow in ">2</i> <a href="Javascript:void(0)"
							class="ensLinkTrack"
							data-componentid="bookflowStepIndicator_comp"> <span class="">Holiday
									Summary</span>

						</a> <i class="arrow out "></i></li>

						<li class="indicator  uc  sprite-5 completed "><i
							class="arrow in ">3</i> <a href="Javascript:void(0)"
							class="ensLinkTrack"
							data-componentid="bookflowStepIndicator_comp"> <span class="">Passenger
									Details</span>

						</a> <i class="arrow out "></i></li>


						<li class="indicator  uc  sprite-5 completed active"><i
							class="arrow in ">4</i> <a href="Javascript:void(0)"
							class="ensLinkTrack"
							data-componentid="bookflowStepIndicator_comp"> <span class="">Payment</span>

						</a> <i class="arrow out "></i></li>


						<li class="indicator  uc  sprite-5  "><i class="arrow in ">5</i>

							<a href="Javascript:void(0)" class="last"><span class="">Confirmation</span></a>

							<i class="arrow out "></i></li>





					</ul>

				</div>

			</div>

			<div class="clear-both"></div>

		</div>
	</c:otherwise>
</c:choose>