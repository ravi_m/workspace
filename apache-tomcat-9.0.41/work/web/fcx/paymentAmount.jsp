<div id="payment_amount">
	<h2 class="underline" style="width:750px;">PAYMENT AMOUNT</h2>

<fmt:formatNumber var="transactionTotal" value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" scope="request" pattern="######.##"/>
<fmt:formatNumber var="amountWithCardCharge" value="${transactionTotal+applicableCardChargeForSelectedAmount}" type="number" maxFractionDigits="2" minFractionDigits="2"  scope="request" groupingUsed="false"/>
<fmt:formatNumber var="amountWithDebitCardCharge" value="${transactionTotal+applicableDebitCardChargeForSelectedAmount}" type="number" maxFractionDigits="2" minFractionDigits="2"  scope="request" groupingUsed="false"/>

<ul class="paymentAmount">
   <li id="amountWithoutCardCharges" class="fontWeightNormal"><!-- for debitcard -->
      <span class="amount" id="spanAmountWithoutCardCharges">
      Your total excursion cost is <b id="totalAmt">
      <c:out value="${currencySymbol}" escapeXml="false"/><c:out value="${amountWithDebitCardCharge}"/></b></span> and full payment is
      due today.<br /> <br /> All fields marked * must be completed.
      </li>

   <li id="amountWithCardCharges" class="fontWeightNormal">
       Your total excursion cost is
       <span class="amount" id="spanAmountWithCardCharges">
	      <b id="totalAmt"><c:out value="${currencySymbol}" escapeXml="false"/><c:out value="${amountWithCardCharge}"/></b>
	   </span> and full payment is
      due today.<br /> <br /> All fields marked * must be completed.
      </li>



</ul>

<%-- *******Essential fields ********************  --%>
<input type="hidden" name="total_transamt"  id="total_transamt"  value='NA' />
<%-- *******End Essential fields ********************  --%>

</div>
