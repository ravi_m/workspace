<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%
 String headerlink = com.tui.uk.config.ConfReader.getConfEntry("fcx.headerlink" , "");
 pageContext.setAttribute("headerlink", headerlink, PageContext.REQUEST_SCOPE);
%>        
<c:if test="${headerlink== 'true'}">
<c:if test="${not empty bookingComponent.clientURLLinks.homePageURL}">

<div>
 <header>
                     <div id="navigation-bar" >
                <a href="<c:out value='${bookingComponent.clientURLLinks.homePageURL}'/>" id="logo" target="_blank">
                    <img src="http://www.firstchoice.co.uk/holiday/images/logo-first-choice-print.gif"  class="print-only removeBorder">
                </a>
                    </div>
                </header>
				</c:if>	
				<c:if test="${empty bookingComponent.clientURLLinks.homePageURL}">
				<div>
 <header>
                     <div id="navigation-bar" >
                <div href="<c:out value='${bookingComponent.clientURLLinks.homePageURL}'/>" id="logo" target="_blank">
                    <img src="http://www.firstchoice.co.uk/holiday/images/logo-first-choice-print.gif"  class="print-only removeBorder">
                </div>
                    </div>
                </header>
				
				
				</c:if>	
				
	</c:if>				
<div id="header_main_menu">

</div>
<%@include file="breadCrumb.jspf"%>