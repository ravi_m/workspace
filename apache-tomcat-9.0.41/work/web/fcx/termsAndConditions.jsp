<div id="terms_conditions">
	<h2 class="underline">TERMS AND CONDITIONS</h2>
<p>	By clicking "Pay Now" you are confirming your booking and your card will be debited with the due amount. All transactions are deducted over our secure server.
Please note that cancellation/amendment charges are applicable once your excursion has been confirmed.</p>
</div>