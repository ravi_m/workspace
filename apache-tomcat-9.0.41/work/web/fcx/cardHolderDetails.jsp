<li>
									<label for="checkbox">Check box if the same as personal details</label>
                                    <input type="checkbox" name="autoCheckCardAddress" value="det"
	id="useAddress" onclick="AddressPopulationHandler.handle()" />
								</li>

								<li>
                                    <label for="housename">House Name/No.*</label>
									<input type="alphanumeric" name="payment_0_street_address1" id="cardHouseName"
		class="textfield "
		value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/>"
		gfv_required="required" maxlength="20"  alt="House Name/No" data-validation-engine="validate[required, maxSize[20]]"/>
                                </li>

                                <li>
                                    <label for="address">Address *</label>
									
                                    <input type="nonblank" name="payment_0_street_address2"
		id="cardAddress1" class="textfield  xxlarge mandatory"
		value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>"
		gfv_required="required" maxlength="25" alt="Address" data-validation-engine="validate[required, maxSize[25]], custom[address]]"/>
									 </li>
								<li>
									<label for="address"> </label>
									<input
		type="nonblank" name="payment_0_cardHolderAddress2" id="cardAddress2"
		class="textfield  xxlarge mandatory"
		value="<c:out value="${bookingInfo.paymentDataMap['payment_0_cardHolderAddress2']}"/>"
		maxlength="25" alt="Address" data-validation-engine="validate[maxSize[25]],custom[address]]"/>
                                </li>

                                <li>
                                    <label for="town">Town / City *</label>
									
                                    <input type="alpha" name="payment_0_street_address3" id="cardTownCity"
		class="textfield  mandatory"
		value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>"
		gfv_required="required" maxlength="25" alt="Town/City" data-validation-engine="validate[required, maxSize[25], custom[town]]"/>
                                </li>
                                <li>
                                    <label for="county">Country *</label>
									<input
		type="alpha" name="payment_0_selectedCountry" id="cardCounty"
		class="textfield "
		value="<c:out value="${bookingInfo.paymentDataMap['payment_0_selectedCountry']}"/>"
		 maxlength="50" alt="Country" data-validation-engine="validate[required, maxSize[50], custom[county]]"/>
                                </li>
                                <li>
                                    <label for="postcode">Postcode *</label>
									
                                    <input
		type="postcode" name="payment_0_postCode" id="cardPostCode"
		class="textfield  mandatory"
		value="<c:out value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/>"
		gfv_required="required" maxlength="8" alt="Postcode" data-validation-engine="validate[required,maxSize[8], custom[postcode]]" />
                                </li>

							<br/>

                            </ul>

                        </div>
			</div>