 <%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="currency" value="${bookingComponent.totalAmount.symbol}" scope="request"/>
<c:set var="calculatedTotalAmount" value="${bookingInfo.calculatedTotalAmount.amount}" scope="request" />
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
  <version-tag:version/>
   <title>Portland Booking - Booking Payment</title>

<script type="text/javascript">
 var leftOffset = 215;
 var topOffset = 30;
</script>
<script type="text/javascript" src="/cms-cps/portland/js/processPayment.js"></script>
	<script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="/cms-cps/portland/js/paymentPanelValidation.js"></script>
<script type="text/javascript" src="/cms-cps/portland/js/formValidation.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/prototype.js">	 </script>
		<script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
		<script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
  <link rel="stylesheet" href="/cms-cps/portland/css/popup.css" type="text/css" />
 <link rel="shortcut icon" href="/cms-cps/portland/images/favicon.ico"/>
<link rel="stylesheet" href="/cms-cps/portland/css/global.css" type="text/css" />

        <!--[if lt IE 8]>
          <script type="text/javascript">
           var leftOffset = 220;
           var topOffset = 45;
          </script>
			<![endif]-->

			<!--[if lt IE 7]>
          <script type="text/javascript">
           var leftOffset = 220;
           var topOffset = 45;
          </script>
			<![endif]-->


 <%@ page contentType="text/html; charset=UTF-8" %>

 <script type="text/javascript">

var  newHoliday  = "<c:out value='${bookingInfo.newHoliday}'/>";


 var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
 var token = "<c:out value='${param.token}' />" ;

var PaymentInfo = new Object();
PaymentInfo.totalAmount =${bookingComponent.totalAmount.amount};

PaymentInfo.payableAmount = ${bookingComponent.payableAmount.amount};
PaymentInfo.calculatedPayableAmount = ${bookingInfo.calculatedPayableAmount.amount};
PaymentInfo.calculatedTotalAmount = ${bookingInfo.calculatedTotalAmount.amount};
PaymentInfo.currency = "${bookingComponent.totalAmount.symbol}";

<fmt:formatNumber var="ccAmount" value="${bookingInfo.calculatedCardCharge.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>

PaymentInfo.chargeamt =${ccAmount};

PaymentInfo.selectedCardType = null;

var BookingConstants = { PAY_CLASS:"pay", PAY_DESCRIPTION:"Confirm"};

var cardChargeMap = new Map();

   var pleaseSelect = new Array();
   cardChargeMap.put('none', pleaseSelect);
  <c:forEach var="cardCharges" items="${bookingInfo.cardChargeMap}">
		     cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}" />');
     </c:forEach>


var cardDetails = new Map();
 <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">
			    var cardCharge_Max_Min_Issue = new Array();
        	         	   cardCharge_Max_Min_Issue.push('${cardDetails.cardType.securityCodeLength}');
						    cardCharge_Max_Min_Issue.push('${cardDetails.cardType.minSecurityCodeLength}');
							 cardCharge_Max_Min_Issue.push('${cardDetails.cardType.isIssueNumberRequired}');

			  cardDetails.put('${cardDetails.paymentCode}',cardCharge_Max_Min_Issue);

         </c:forEach>

		window.onload= function(){ checkBookingButton("bookingcontinue");}

               //  The following map stores the pay button labels in case of 3D cards.
         var threeDCards = new Map();
         <c:forEach var="threeDCards" items="${bookingInfo.payDescription}">
             threeDCards.put("<c:out value="${threeDCards.key}"/>","<c:out value="${threeDCards.value}"/>");
         </c:forEach>

  jQuery(document).ready(function()
  {
	  toggleOverlayPortland();
  })

//These four variables are responsible for autopopulating the address fields on check of the checkbox.
var street_address1 = '<c:out value="${bookingComponent.nonPaymentData['street_address1']}"/>';
var street_address2 = '<c:out value="${bookingComponent.nonPaymentData['street_address2']}"/>';
var street_address3 = '<c:out value="${bookingComponent.nonPaymentData['street_address3']}"/>';
var street_address4 = '<c:out value="${bookingComponent.nonPaymentData['street_address4']}"/>';
var postCode = '<c:out value="${bookingComponent.nonPaymentData['cardBillingPostcode']}"/>';


  </script>
</head>
<body leftmargin="5" style="width:784px;">
<c:set var="visiturl" value="yes" scope="session" />
<c:set var='clientapp' value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName} '/>
<form name="paymentdetails" action="" method="post" >
 <input type="hidden" name="tomcatInstance" id="tomcatInstance" value="<c:out value='${param.tomcat}' />"/>

<jsp:include page="header.jsp"></jsp:include>
<div class="clear"></div>
<c:if test="${not empty bookingInfo.bookingComponent.errorMessage}">
    <div id="pricediff1">
           <p><b><c:out value="${fn:toUpperCase(bookingInfo.bookingComponent.errorMessage)}"/></b></p>
       </div>
</c:if>
<p>Please complete your payment details below and click 'Confirm' to book your holiday.</p>

  <h2>&nbsp;&nbsp;&nbsp;Your Payment Details</h2>
<div id="payment">

<div  style="width:400px;float:left">
   <table border="0" cellpadding="5" cellspacing="1" class="details">
               <b>Amount to be debited: <c:out value="${currency}" escapeXml="false"/>
               <span id="updatecardcharge"><fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
                  minFractionDigits="2" pattern="#####.##"/></span>
               </b>

   <tr>
      <td class="label">Card type</td>
      <td class="data">
        <select name="payment_0_type"
           id="payment_type_0" required="true"	 onchange="javascript:updateCardChargeForportland(),setSecurityCode(this);"
            requiredError="Please select payment method." requiredEmpty="none" style="width:145px;">
            <option selected value="none">Please select</option>
            <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">

            <option value='${cardDetails.paymentCode}'>
                          ${cardDetails.paymentDescription}
            </option>
         </c:forEach>
         </select>
      </td>
   </tr>

      <tr>
      <td class="label">Card number</td>
      <td class="data"><input type="text" id='payment_0_cardNumberId' name='payment_0_cardNumber' autocomplete="off"  onblur='validatePaymentFields(this) '/></td>
   </tr>
   <tr>
      <td class="label">Issue number<br /><span class="small">if applicable</span></td>
      <td class="data"><input type="text" id='IssueNumberInput' name='payment_0_issueNumber' autocomplete="off"  maxlength="2" size="2" onblur='checkIssueNumber(this)' /><br />&nbsp;</td>
   </tr>
      <tr>
      <td class="label">Expires</td>
      <td class="data">
         <select id='payment_0_expiryMonthId'  name='payment_0_expiryMonth' onchange="setExpiryMonth(this);">
            <option value="MM">MM</option>
            <option value="01">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
         </select>

          <select class='date_field uniform' id='ExpiryYear' id='payment_0_expiryYear' name='payment_0_expiryYear' onchange="setExpiryYear(this);">
                      <option value="YY">YY</option>
                      <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
                         <option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>
                      </c:forEach>
         </select>
      </td>
   </tr>


   <tr>
      <td class="label">Security Code</td>

      <td class="data"><input type="text" id="payment_0_securityCode" autocomplete="off" name ="payment_0_securityCode" maxlength='4'	size="4" onblur='validateSecurityCode(this) '/></td>
   </tr>

<tr>
      <td class="label">Name of card holder</td>

      <td class="data"><input type="text" id='payment_0_nameOnCardId' autocomplete="off"  name="payment_0_nameOnCard" maxLength="40" size=" 30" onblur='validatePaymentFields(this) ' /></td>
   </tr>
      </table>
      </div>
       <div id="threeDsecureimages">
          <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
             <c:if test="${threeDLogos == 'mastercardgroup'}">
                <div style="float:left;width:100px;padding-top:3px">
                   <a href="javascript:void(0);" id="masterCardDetails" class="sticky stickyOwner clearStyles">
                      <img style="border:none"  src="/cms-cps/portland/images/th-mastercard-secure.gif" alt="Mastercard SecureCode"/>
                   <b><u>Learn more</u></b></a>
			<%@ include file="/common/mastercardLearnMoreSticky.jspf"%>
                </div>
             </c:if>
             </c:forEach>
             <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
                <c:if test="${threeDLogos == 'visagroup'}">
                   <div style="float:left;width:80px;">
	                   <a href="javascript:void(0);" id="visaDetails" class="sticky stickyOwner clearStyles">
                         <img style="border:none"  src="/cms-cps/portland/images/th-visa.gif" alt="Verified by Visa"/>
                      <b><u>Learn more</u></b></a>
			   <%@ include file="/common/visaLearnMoreSticky.jspf"%>
                   </div>
             </c:if>
          </c:forEach>
      </div>
      <br clear="all"/>
     <input type="hidden" name="tomcatInstance" id="tomcatInstance" value="<c:out value='${param.tomcat}' />"/>
     <input type="hidden" name="token" id="token" value="<c:out value='${param.token}' />"/>
	  <input type="hidden" id="payment_0_paymenttypecode" value="" name="payment_0_paymenttypecode"/>
	  <input type="hidden" name="total_transamt"  id="total_transamt"
   value="<fmt:formatNumber value="${calculatedTotalAmount}" type="number" maxFractionDigits="2"
                  minFractionDigits="2" pattern="#####.##"/>" />
</div>

<div style="float:right;padding:5px 10px 5px 5px;"><b><font color="#808080">All fields marked * must be completed.</font></b></div>
<br clear="all"/>
<h2> &nbsp;&nbsp;&nbsp;Cardholder address</h2>
<div id="address">
 To help ensure your card details remain secure, please confirm the address of the card holder. If this is the same as lead passenger address, you just need to check the box.
<table border="0" cellpadding="5" cellspacing="1" class="details" width="450px">
   <tr>
      <td class="addresslabel"></td>
      <td class="data"><input type="checkbox" name="autofill" onclick="autoCompleteAddress()"/> Use same address as lead passenger</td>
   </tr>
   <tr>
      <td class="addresslabel"><b>Address*</b></td>
      <td class="data"><input type="textfield" maxlength='24' name="payment_0_street_address1" id="payment_0_streetAddress1" onblur='validatePaymentFields(this)' value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/></td>
   </tr>
   <tr>
      <td class="addresslabel"><b>* </b></td>
      <td class="data"><input type="textfield" maxlength='24' name="payment_0_street_address3" id="payment_0_streetAddress2" onblur='validatePaymentFields(this)' value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/></td>
   </tr>
   <tr>
      <td class="addresslabel"></td>
      <td class="data"><input type="textfield" maxlength='24' name="payment_0_street_address2" id="payment_0_streetAddress3" onblur='validatePaymentFields(this)' value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/></td>
   </tr>
   <tr>
      <td class="addresslabel"></td>
      <td class="data"><input type="textfield" maxlength='24' name="payment_0_street_address4" id="payment_0_streetAddress4" onblur='validatePaymentFields(this)' value="${bookingInfo.paymentDataMap['payment_0_street_address4']}"/> </td>
   </tr>
   <tr>
      <td class="addresslabel"></td>
      <td class="data"><input type="textfield" name="selectedCountry" value="United Kingdom" id="selectedCounty" disabled="disabled" /></td>
      <input type="hidden" name="selectedCountry" value="United Kingdom"/>
   </tr>
   <tr>
   <td class="label"><b>Postcode*</b></td>
      <td class="data"><input type="text" id='payment_0_postCodeId' name ="payment_0_postCode" onblur='validatePaymentFields(this)' value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/></td>

   </tr>
</table>

</div>


   <div id="note">
   <h3>Please Note:</h3>
   <p>Once you have entered your card details and confirmed your booking, the amount shown above will be debited from your account. Due to the fee levied by credit card companies, there is a <%= com.tui.uk.config.ConfReader.getConfEntry("cardChargePercent", "")%>% surcharge for credit card payments. This will be added automatically if you select this method of payment. (This charge does not apply to Delta, Visa Debit or Maestro card payments.)</p>
   <p>Cancellation / amendment charges are applicable once your booking has been confirmed.</p>
    <p>Payment method <img src="/cms-cps/portland/images/cards_01.gif" alt="Delta, Visa Debit, Maestro, Visa, Mastercard"/>


	</p>
</div>


<p class="confirm">Please only press 'Confirm' once to make your reservation. It may take some time to confirm.</p>

<br clear="all"/>
    <div id="buttons">
    <c:set var="backurl" value="${bookingInfo.bookingComponent.prePaymentUrl}"/>
     <div class="cancel">
   <a class="clearStyles" href="<c:out value='${backurl}'/>"   title="Back"><img src="/cms-cps/portland/images/cancel.gif" alt="Cancel"  id="BackText" width="65" height="17" border="0" /></a></div>
   <c:if test="${bookingInfo.newHoliday}">
   <div class="continue" id="continue"><a class="clearStyles Pay" href="javascript:GoToNextPage()" id="bookingcontinue" onmouseover="status='Continue'; return true;" onmouseOut="status=''; return true;"><img src="/cms-cps/portland/images/confirm.gif" alt="Confirm" id="ContinueText" class = "pay" width="71" height="17" border="0" /></a></div>
     </c:if>
</div>


<div class="clear"></div>
<%--Intellitracker code --%>
<script type="text/javascript" src="/cms-cps/portland/js/intellitracker.js"></script>
<%--End of intellitracker code --%>
<br/>
<input type="hidden" name="payment_0_selectedCountryCode" value="GB"/>
</form>
</body>
</html>
