<div class="pageContainer">
  <div id="headerSection">
  <img src="/cms-cps/portland/images/masthead.jpg" alt="Portland Direct" border="0" class="left" />
  <br clear="all"/>
  </div>
  <!--end of HeaderSection-->
  <!--breadcrumbs here-->
  <div id="contentSection">
    <div class="titleSection">
      <h1>Nearly done...</h1>
    </div>
    <div id="secureForm">
      <div class="panel" id="iframeInclude">
        <div class="top">
          <div class="right"></div>
        </div>
        <div class="body">
          <div class="right">
            <div class="content">
              <div class="section">
                <iframe name="ACSframe" class="secureframe" frameborder="0" style="overflow:auto;"  src="common/bank3dwaiting.jsp">&nbsp;</iframe>
              </div>
            </div>
          </div>
        </div>
        <div class="bottom">
          <div class="right"></div>
        </div>
      </div>
    </div>
    <!--end of secure form-->
    <div id="secureDetails">
      <h2>To complete your booking please verify your card</h2>
      <p class="sectionContent">We'd like you to book online with us knowing your details are 100% secure. That's why we've introduced Verified by Visa and MasterCard SecureCode - security programmes from your card provider designed to give you even more peace of mind when paying online.</p>
      <h2>First time you've seen this?</h2>
      <p class="sectionContent">Registering for Verified by Visa or MasterCard SecureCode is simple. Complete your card details in the box opposite. You can then set up a personal password to use for any future purchases you make with your card on participating websites - you only need to register once. We don't store any of your details - they're provided to your card issuer securely.</p>
      <h2>Already registered?</h2>
      <p class="sectionContent">If you've already used Verified by Visa or MasterCard SecureCode, simply enter your personal password in the box on the left to continue your payment.</p>
      <p class="callInfo">If you need any help, please call us on <br /> <span class="number">0871 231 4889</span> (Calls cost 10p per minute plus network extras) or contact your card provider.</p>
    </div>
    <!--end secure details-->
    <div class="pageControls"><a class="backPage" href="<%=request.getAttribute("postPaymentUrl")%>" title="Back to payment page">Back to payment page</a></div>
  </div>
  <!--end of ContentSection-->
  <div id="footerSection"> </div>
  <!--end of FooterSection-->
</div>
<%--Intellitracker code --%>
<script type="text/javascript" src="/cms-cps/portland/js/intellitracker.js"></script>
<%--End of intellitracker code --%>
<!--end of PageContainer-->
