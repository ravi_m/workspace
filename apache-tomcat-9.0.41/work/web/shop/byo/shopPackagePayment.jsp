<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<%@include file="/common/commonTagLibs.jspf"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>

        <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1;" />
        <version-tag:version/>
        <title>Build Your Own Holidays - Flight and Hotels - Payment </title>
        <c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
        <c:set var="clientapp" value="${bookingComponent.clientApplication.clientApplicationName}"/>
        <c:set var="searchtype" value="${bookingComponent.searchType}" scope="request"/>

        <%@include file="resources.jspf" %>

    </head>

    <body>
    <c:set var="customerType" value="${bookingComponent.nonPaymentData['customer_Type']}" scope="request"/>
	<c:set var="selectedDeposit" value="${bookingComponent.nonPaymentData['depositType']}" scope="request"/>
	<%--Since pan and security code are in form of character array we are converting them to String--%>
    <c:forEach var="paymentTransaction" items="${paymentTransactions}">
       <c:forEach var="pan" items="${paymentTransaction.card.pan}">
          <c:set var="pandata" value="${pandata}${pan}" />
       </c:forEach>
       <c:forEach var="securitycode" items="${paymentTransaction.card.cv2}">
          <c:set var="securitycodedata" value="${securitycodedata}${securitycode}" />
       </c:forEach>
       <c:set var="responseMsg" value="${transactionResponseMsg}" scope="request"/>
    </c:forEach>
    <%@ include file="/shop/common/scriptConfigSettings.jspf" %>
    <%@include file="pinpadScripts.jspf" %>
    <%-- TBP section starts here --%>
    <%
         String brandName = (String) request.getParameter("brand");
         String tbpValue = com.tui.uk.config.ConfReader.getConfEntry(brandName+".isTbpDisplay", "");
         pageContext.setAttribute("tbpValue", tbpValue, PageContext.REQUEST_SCOPE);
    %>
    <%-- TBP section ends here --%>

    <form name="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}' />&amp;brand=<c:out value='${param.brand}'/>&amp;tomcat=<c:out value='${param.tomcat}' />" onsubmit = "return FormHandler.handleSubmission(this)">
    <%@include file="shopPackageHeaderPartHtml.jsp" %>
    <%@include file="/shop/common/logoff.jsp" %>
    <br clear="all"/>

    <div class="leftPanelWrapper">
        <div id="leftpanel">
            <%@include file="shopPackageSummaryPanel.jsp"%>
        </div>
        <div align="center">
                 <a href="javascript:Popup('<c:out value="${bookingComponent.clientDomainURL}/thomson/page/shop/common/atoz/atoztandc.page"/>',755,584,'scrollbars=yes');">A-Z Booking Information
                 </a>
          </div>
    </div>

   <div id="bodysection">
        <div id="content">
           <%@include file="breadCrumb.jspf" %>
           <div class="contenthead filterpage"><%-- add "filterpage" class if this page is a filtered results page --%>
                <h1>Payment Details</h1>
           </div>

           <%@include file="pageErrors.jspf" %>


           <c:if test="${not empty bookingComponent.depositComponents}">
                <div><jsp:include page="shopPackageDepositOptions.jsp" /></div>
           </c:if>



           <div><jsp:include page="shopPackagePaymentDetails.jsp" /><br/></div>

           <c:if test="${not empty bookingComponent.termsAndCondition.relativeTAndCUrl}">
                 <div><jsp:include page="shopPackageTermsAndConditions.jsp" /></div>
           </c:if>

           <div><jsp:include page="shopPackageNavigation.jsp" /></div>

        </div>

   </div>


    <div id="footer"></div>
          </form>
    </body>
</html>

<%------------------------------TODO following should be properly placed------------------------------------------------%>
<input type="hidden" value="0" name="totalAmountDue" id="totalAmountDue" />
<script type="text/javascript">
    var numberOfSeniorPassengers =0// "<c:out value='${TravelOptionsFormBean.passengersOver65}'/>";
    var NumberOfAdults =2;//"<c:out value='${fhBookingContext.searchCriteria.partyComposition.partySize}'/>";
    var lessSeniorPassengersMessage="Please select the appropriate number of senior citizens as chosen in previous page";
    var moreSeniorPassengersMessage="Please select the appropriate number of senior citizens, navigate to previous page if required.";
    var surnameValidationError="We're sorry but we cannot book you onto your chosen flight.  Would you like to select another flight and try again.";
    var supplierSystem="<c:out value='${bookingComponent.flightSummary.flightSupplierSystem}'/>";
    var supplierAmadeus="Amadeus";
</script>

<c:set var="hoplaBonded" value="${bookingComponent.accommodationSummary.hoplaBonded}" scope="request"/>
<%-- /* In all the cases script variable isHopla is set to false since no PPG section should be displayed. */ --%>
<c:choose>
    <c:when test="${empty hoplaBonded}">
      <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_PEG'}">
        <script type="text/javascript">
          isHopla = true;
        </script>
      </c:if>
    </c:when>
    <c:otherwise>
        <c:choose>
            <c:when test="${hoplaBonded}">
                <script type="text/javascript">
                  isHopla = false;
                </script>
            </c:when>
            <c:otherwise>
                <c:if test="${bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_PEG'}">
                    <script type="text/javascript">
                       isHopla = false;
                    </script>
                </c:if>
            </c:otherwise>
        </c:choose>
    </c:otherwise>
</c:choose>