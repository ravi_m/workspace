<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="hoplaBonded" value="${bookingComponent.accommodationSummary.hoplaBonded}" scope="request"/>
<div>
    <%-- price section start --%>
    <%-- Start of the code for the postPay accommodation price panel --%>
    <div id="price">
        <c:choose>
            <c:when test="${hoplaBonded && bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
                <c:if test="${bookingComponent.flightSummary.flightSupplierSystem == 'Amadeus'}">
                    <div class="thirdpartyindicator thirdparty_flighthotel">&nbsp;</div>
                    <br clear="all"/>
                    <br clear="all"/>
                </c:if>
            </c:when>
            <c:otherwise>
                <div class="logo_img">
                    <c:set var="callType" value="${bookingComponent.callType}"/>
                    <c:choose>
                        <c:when test="${callType=='PORTDIR'|| callType =='PORTOFFL'}">
                            <img width="60" height="20" border="0" title="Portland" alt="Portland" src="/cms-cps/shop/byo/images/logos/portlandlogo1.gif" class="portland"/>
                        </c:when>
                        <c:when test="${bookingComponent.companyCode == 'FC'}">
                            <img src="/cms-cps/shop/byo/images/fc-logo-180x23.gif" alt="First Choice"/>
                        </c:when>
                        <c:when test="${bookingComponent.companyCode == 'FN'}">
                            <img src="/cms-cps/shop/byo/images/logos/falcon-logo-180x23.gif" alt="Falcon"/>
                        </c:when>
                        <c:otherwise>
                            <img src="/cms-cps/shop/common/images/thomson_logo1.gif" alt="Thomson" title="Thomson" border="0"/>
                        </c:otherwise>
                    </c:choose>
                </div>
            </c:otherwise>
        </c:choose>
        <%-- Retrieve the Costlinenames article --%>
        <dl class="price_summary">
            <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
                <c:if test="${costingLine.itemDescription!='TBP' && costingLine.amount.amount != 0.0}">
                    <c:set var="description" value='${costingLine.itemDescription}'/>
                    <c:set var="lineAmount" value='${costingLine.amount.amount}'/>
                    <c:set var="lineItemQuantity" value='${costingLine.quantity}'/>
                    <%-- Style settings starts here --%>
                    <c:choose>
                        <c:when test="${description =='Flight' || description == 'Flight:' || description =='Hotel (payable on checkout)' || description =='Flight + Hotel' || description == 'Hotel' || description == 'Hotel:'}">
                            <c:set var="boldMeClass" value='class=boldme'/>
                        </c:when>
                        <c:otherwise><c:set var="boldMeClass" value=''/></c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${(description == 'Flight' || description == 'Flight:') && bookingComponent.flightSummary.flightSelected == 'false'}">
                            <c:set var="inactiveClass" value='class=inactive'/>
                        </c:when>
                        <c:when test="${(description == 'Hotel:' || description == 'Hotel') && bookingComponent.accommodationSummary.accommodationSelected == 'false'}">
                            <c:set var="inactiveClass" value='class=inactive'/>
                        </c:when>
                        <c:otherwise><c:set var="inactiveClass" value=''/></c:otherwise>
                    </c:choose>
                    <c:if test="${costingLine.discount=='false'}">
                        <c:choose>
                            <c:when test="${description == 'Flight' || description == 'Flight:'}">
                                <c:choose>
                                    <c:when test="${bookingComponent.flightSummary.flightSelected == 'false'}">
                                        <c:set var="inactiveandStrike" value="class=inactive inactive_strike"/>
                                    </c:when>
                                    <c:otherwise><c:set var="inactiveandStrike" value=""/></c:otherwise>
                                </c:choose>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test="${description == 'Hotel:' || description == 'Hotel'}">
                                <c:choose>
                                    <c:when test="${bookingComponent.accommodationSummary.accommodationSelected == 'false'}">
                                        <c:set var="inactiveandStrike" value="class=inactive inactive_strike"/>
                                    </c:when>
                                    <c:otherwise><c:set var="inactiveandStrike" value=""/></c:otherwise>
                                </c:choose>
                            </c:when>
                        </c:choose>
                    </c:if>
                    <%-- Style settings ends here --%>
                    <c:choose>
                        <c:when test="${bookingComponent.accommodationSummary.caption == 'FLY_DRIVE' && description == 'Flight + Hotel'}">
                            <dt>Fly Drive</dt>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
                                    <c:choose>
                                        <c:when test="${(description == 'Flight' || description == 'Flight:') || description == 'Hotel (payable on checkout)'}">
                                            <dt <c:out value="${boldMeClass}"/>>
                                                <span <c:out value="${inactiveClass}"/>>
                                                    <c:out value="${description}"/>
                                                </span>
                                            </dt>
                                        </c:when>
                                        <c:otherwise>
                                            <dt>
                                                <c:if test="${lineItemQuantity != null}"><c:out value="${lineItemQuantity}"/> x</c:if>
                                                <c:out value="${description}"/>
                                            </dt>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise><%-- For PDP( and other non HOPLA packages)  --%>
                                    <dt <c:out value="${boldMeClass}"/>>
                                        <span <c:out value="${inactiveClass}"/>>
                                            <c:if test="${lineItemQuantity != null}"><c:out value="${lineItemQuantity}"/> x</c:if>
                                            <%-- For First Choice  --%>
                                            <c:choose>
                                                <c:when test="${description!= 'Thomson Saving'}"><c:out value="${description}"/></c:when>
                                                <c:otherwise>
                                                    <c:choose>
                                                        <c:when test="${bookingComponent.companyCode == 'FC'}">FirstChoice Saving</c:when>
                                                        <c:when test="${bookingComponent.companyCode == 'FN'}">Falcon Saving</c:when>
                                                        <c:otherwise><c:out value="${description}"/></c:otherwise>
                                                    </c:choose>
                                                </c:otherwise>
                                            </c:choose>
                                        </span>
                                    </dt>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${costingLine.discount=='true'&& (description == 'Thomson Saving' || description == 'Portland Saving')}">
                        <dd>
                            <c:if test="${lineAmount > 0}">-</c:if>
                            &pound;<fmt:formatNumber value="${lineAmount}" var="linetotal" type="number" maxFractionDigits="2" minFractionDigits="2"/>
                            <c:out value="${linetotal}"/>
                        </dd>
                    </c:if>
                    <c:if test="${costingLine.discount=='false'}">
                        <fmt:formatNumber value="${lineAmount}" var="linetotal" type="number" maxFractionDigits="2" minFractionDigits="2"/>
                        <c:choose>
                            <c:when test="${description == 'Flight' || description == 'Flight:'}">
                                <dd><span <c:out value="${inactiveandStrike}"/>>&pound;<c:out value="${linetotal}"/></span></dd>
                            </c:when>
                            <c:when test="${description == 'Hotel:' || description == 'Hotel'}">
                                <dd><span <c:out value="${inactiveandStrike}"/>>&pound;<c:out value="${linetotal}"/></span></dd>
                            </c:when>
                            <c:otherwise>
                                <dd <c:out value="${boldMeClass}"/>>&pound; <c:out value="${linetotal}"/></dd>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                </c:if>
            </c:forEach>
            <c:set var="ccAmount"><fmt:formatNumber value="${bookingInfo.calculatedCardCharge.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/></c:set>
            <c:if test="${bookingInfo.newHoliday == 'false' && (ccAmount != null && ccAmount != '' && ccAmount != 0.0)}">
                <dt>Card Charges:</dt>
                <dd>&pound;<c:out value="${bookingInfo.calculatedCardCharge.amount}"/></dd>
            </c:if>



            <dt id='cardChargeText'  class = "cardchargeBlock">Card Charges</dt>
            <dd id='cardChargeAmount' class="calculatedCardChargeSummaryPanel cardchargeBlock"> </dd>
            <dt id='totalText' class="totalText" onmouseover="openTitlePopup('tbpDisplay');" onmouseout="closeTitlePopup('tbpDisplay');">Total&#58;</dt>
            <dd id='totalAmount' class="total totalholidaycostSummaryPanel" onmouseover="openTitlePopup('tbpDisplay');" onmouseout="closeTitlePopup('tbpDisplay');">&pound;
                <fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" var="totalCostingLineWithoutDiscount" maxFractionDigits="2" minFractionDigits="2"/>
                <c:out value="${totalCostingLineWithoutDiscount}"/>
            </dd>
            <%-- /* Display area for Discount components starts here*/--%>
            <c:forEach var="discountComponent" items="${bookingComponent.discountComponents}">
                <c:if test="${!fn:containsIgnoreCase(discountComponent.discountType,'Total discount %')}">
                    <dt><c:out value="${discountComponent.discountType}"/>&nbsp;</dt>
                    <dd>-&pound;<fmt:formatNumber type="number" value="${discountComponent.maxDiscountAmount.amount}" maxFractionDigits="2" minFractionDigits="2"/></dd>
                </c:if>
                <c:if test="${fn:containsIgnoreCase(discountComponent.discountType,'Total discount %')}">
                    <dt><c:out value="${discountComponent.discountType}"/>&nbsp;</dt>
                    <dd><fmt:formatNumber type="number" value="${discountComponent.maxDiscountAmount.amount}" maxFractionDigits="2" minFractionDigits="2"/></dd>
                </c:if>
            </c:forEach>
            <%-- /* Display area for Discount components ends here*/--%>
            <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
                <c:if test="${tbpValue && costingLine.itemDescription=='TBP' && costingLine.amount.amount != '0.00'}">
                    <dt><c:out value="${costingLine.itemDescription}"/>&nbsp;&#61;</dt>
                    <dd>&nbsp;&nbsp;<c:out value="${costingLine.amount.amount}"/></dd>
                </c:if>
            </c:forEach>
        </dl>
        <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
            <c:if test="${!tbpValue && costingLine.itemDescription=='TBP' && costingLine.amount.amount != '0.00'}">
                <span id='tbpDisplay' class="tbpDesc"><c:out value="${costingLine.itemDescription}"/>&nbsp;&#61; <fmt:formatNumber type="number" value="${costingLine.amount.amount}" maxFractionDigits="2" minFractionDigits="2"/></span>
            </c:if>
        </c:forEach>
    </div><%--End of price div --%>
</div>