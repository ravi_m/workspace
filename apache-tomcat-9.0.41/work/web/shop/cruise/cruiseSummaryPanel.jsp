<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div class="blueBorder">
    <div id="price">
        <h3>
            <c:set var="callType" value="${bookingComponent.callType}"/>
            <span class="whiteBg">
                <c:choose>
                    <c:when test="${callType=='PORTDIR'|| callType =='PORTOFFL'}">
                        <img width="60" height="20" border="0" title="Portland" alt="Portland" src="/cms-cps/shop/cruise/images/logos/portlandlogo1.gif" class="portland"/>
                    </c:when>
                    <c:otherwise>
                        <img src="/cms-cps/shop/common/images/logo-summary-panel-thomson.gif" width="90" height="12" border="0" alt=""/>
                    </c:otherwise>
                </c:choose>
            </span>
        </h3>
        <dl class="price_summary">
            <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
                <c:if test="${costingLine.itemDescription!='TBP'}">
                    <c:if test="${costingLine.itemDescription != 'Total' && costingLine.itemDescription != 'Thomson Saving' && costingLine.itemDescription != 'Portland Saving'}">
                        <dt>
                            <c:if test="${costingLine.quantity != null}"><c:out value="${costingLine.quantity}"/> x</c:if>
                            <c:out value="${costingLine.itemDescription}"/>&#58;
                        </dt>
                        <dd>&nbsp;&nbsp;&pound;
                        <fmt:formatNumber value="${costingLine.amount.amount}" type="number" var="totalCostingLineFuelSuplement" maxFractionDigits="2" minFractionDigits="2"/>



						<c:out value="${totalCostingLineFuelSuplement}"/></dd>
                    </c:if>
                    <c:if test="${costingLine.itemDescription == 'Thomson Saving' || costingLine.itemDescription == 'Portland Saving' && costingLine.amount.amount gt 0}">
                        <dt><c:out value="${costingLine.itemDescription}"/>&#58;</dt>
                        <dd>- &pound;<c:out value="${costingLine.amount.amount}"/></dd>
                    </c:if>
                </c:if>
            </c:forEach>
            <c:if test="${bookingInfo.newHoliday == 'false' && (bookingInfo.calculatedCardCharge.amount != null && bookingInfo.calculatedCardCharge.amount != '0.00')}">
                <dt>Card Charges:</dt>
                <dd>&pound;<c:out value="${bookingInfo.calculatedCardCharge.amount}"/></dd>
            </c:if>

               <dt id='cardChargeText' class="cardchargeBlock">Card Charges</dt>
               <dd id='cardChargeAmount' class="calculatedCardChargeSummaryPanel cardchargeBlock"></dd>
            <dt id='totalText' class="totalText" onmouseover="openTitlePopup('tbpDisplay');" onmouseout="closeTitlePopup('tbpDisplay');">Total&#58;</dt>
            <dd id='totalAmount' class="total totalholidaycostSummaryPanel" onmouseover="openTitlePopup('tbpDisplay');" onmouseout="closeTitlePopup('tbpDisplay');">
                &pound;<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" var="totalCostingLineWithoutDiscount" maxFractionDigits="2" minFractionDigits="2"/>
                <c:out value="${totalCostingLineWithoutDiscount}"/>
            </dd>
            <c:forEach var="discountComponent" items="${bookingComponent.discountComponents}">
                <c:if test="${!fn:containsIgnoreCase(discountComponent.discountType,'Total discount %')}">
                    <dt>
                        <c:out value="${discountComponent.discountType}"/>&nbsp;
                    </dt>
                    <dd>-&pound;<fmt:formatNumber type="number" value="${discountComponent.maxDiscountAmount.amount}" maxFractionDigits="2" minFractionDigits="2"/></dd>
                </c:if>
                <c:if test="${fn:containsIgnoreCase(discountComponent.discountType,'Total discount %')}">
                    <dt><c:out value="${discountComponent.discountType}"/>&nbsp;</dt>
                    <dd><fmt:formatNumber type="number" value="${discountComponent.maxDiscountAmount.amount}" maxFractionDigits="2" minFractionDigits="2"/></dd>
                </c:if>
            </c:forEach>
            <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
                <c:if test="${tbpValue && costingLine.itemDescription=='TBP' && costingLine.amount.amount != '0.00'}">
                    <dt><c:out value="${costingLine.itemDescription}"/>&nbsp;&#61;</dt>
                    <dd>&nbsp;&nbsp;<c:out value="${costingLine.amount.amount}"/></dd>
                </c:if>
            </c:forEach>
        </dl>
        <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
            <c:if test="${!tbpValue && costingLine.itemDescription=='TBP' && costingLine.amount.amount != '0.00'}">
                <span class="tbpDesc" id='tbpDisplay'><c:out value="${costingLine.itemDescription}"/>&nbsp;&#61; <fmt:formatNumber type="number" value="${costingLine.amount.amount}" maxFractionDigits="2" minFractionDigits="2"/></span>
            </c:if>
        </c:forEach>
    </div>
    <div id="flightandhotelsection">
        <c:if test='${bookingComponent.cruiseSummary.ship.accommodationType == "CRUISE_AND_STAY" || bookingComponent.cruiseSummary.ship.accommodationType == "STAY_AND_CRUISE" || bookingComponent.cruiseSummary.ship.accommodationType == "FLY_CRUISE"}'>
            <div>
                <h3>Flights</h3>
                <h4>Leaving&#58; </h4>
                <c:forEach var="outboundFlight" items="${bookingComponent.flightSummary.outboundFlight}">
                    <c:choose>
                        <c:when test="${outboundFlight.departureAirportName == 'XUK'}">
                            <div id="portandcruisesection">
                                <dl class="portinfo">
                                    <dt>Embarkation Port&#58; </dt>
                                    <dd>
                                        <c:out value="${bookingComponent.cruiseSummary.startPort.description}"/>
                                        <fmt:formatDate value="${bookingComponent.cruiseSummary.startDate}" pattern="dd/MM/yy" />
                                    </dd>
                                </dl>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${not empty outboundFlight}">
                                <ul class="flight_info">
                                    <li>Depart&#58;
                                        <ul>
                                            <li><c:out value="${outboundFlight.departureAirportName}"/></li>
                                            <li class="date"><fmt:formatDate value="${outboundFlight.departureDateTime}" pattern="dd-MMM-yy"/></li>
                                            <li class="time"><fmt:formatDate value="${outboundFlight.departureDateTime}" type="TIME" pattern="HH:mm"/></li>
                                        </ul>
                                    </li>
                                    <li>Arrive&#58;
                                        <ul>
                                            <li><c:out value="${outboundFlight.arrivalAirportName}"/></li>
                                            <li class="date"><fmt:formatDate value="${outboundFlight.arrivalDateTime}" pattern="dd-MMM-yy"/></li>
                                            <li class="time"><fmt:formatDate value="${outboundFlight.arrivalDateTime}" type="TIME" pattern="HH:mm"/></li>
                                        </ul>
                                    </li>
                                    <li>Carrier&#58;
                                        <ul>
                                            <li>
                                                <c:out value="${outboundFlight.carrier}" escapeXml="false"/><br/>
                                                <c:out value="${outboundFlight.operatingAirlineCode}${outboundFlight.flightNumber}"/>
                                            </li>
                                             <li>
                                            	<c:if test="${(bookingComponent.nonPaymentData['isOutboundLegDreamliner'])== 'true'}">
                 									<div class="wishCruise_dreamliner"><img src="/cms-cps/shop/cruise/images/Flight.png"></img></div>
	 				    						</c:if>
	 				    					</li>
                                        </ul>
                                        <c:set var="marketingAirlineCode" value="${outboundFlight.marketingAirlineCode }"/>
                                        <c:set var="operatingAirlineCode" value="${outboundFlight.operatingAirlineCode}"/><br/>
                                        <c:if test="${marketingAirlineCode != null && operatingAirlineCode != null && operatingAirlineCode != marketingAirlineCode}">
                                            <li>Operated
                                                <ul>
                                                    <li><c:out value="${outboundFlight.operatingAirlineShortName}" escapeXml="false"/></li>
                                                </ul>
                                                <br />by&#58;
                                            </li>
                                        </c:if>
                                    </li>
                                </ul>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
                <h4>Returning&#58;</h4>
                <c:forEach var="inboundFlight" items="${bookingComponent.flightSummary.inboundFlight}">
                    <c:choose>
                        <c:when test="${inboundFlight.arrivalAirportName == 'XUK'}">
                            <div id="portandcruisesection">
                                <dl class="portinfo">
                                    <dt>Disembarkation Port&#58; </dt>
                                    <dd>
                                        <c:out value="${bookingComponent.cruiseSummary.endPort}"/>
                                        <fmt:formatDate value="${bookingComponent.cruiseSummary.endDate}" pattern="dd/MM/yy" />
                                    </dd>
                                </dl>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div>
                                <ul class="flight_info">
                                    <li>Depart&#58;
                                        <ul>
                                            <li><c:out value="${inboundFlight.departureAirportName}"/></li>
                                            <li class="date"><fmt:formatDate value="${inboundFlight.departureDateTime}" pattern="dd-MMM-yy"/></li>
                                            <li class="time"><fmt:formatDate value="${inboundFlight.departureDateTime}" type="TIME" pattern="HH:mm"/></li>
                                        </ul>
                                    </li>
                                    <li>Arrive&#58;
                                        <ul>
                                            <li><c:out value="${inboundFlight.arrivalAirportName}" /></li>
                                            <li class="date"><fmt:formatDate value="${inboundFlight.arrivalDateTime}" pattern="dd-MMM-yy"/></li>
                                            <li class="time"><fmt:formatDate value="${inboundFlight.arrivalDateTime}" type="TIME" pattern="HH:mm"/></li>
                                        </ul>
                                    </li>
                                    <li>Carrier&#58;
                                        <ul>
                                            <li>
                                                <c:out value="${inboundFlight.carrier}" escapeXml="false"/><br/>
                                                <c:out value="${inboundFlight.operatingAirlineCode}${inboundFlight.flightNumber}"/>
                                            </li>
                                            <li>
                                    			<c:if test="${(bookingComponent.nonPaymentData['isInboundLegDreamliner'])== 'true'}">
                 									<div class="wishCruise_dreamliner"><img src="/cms-cps/shop/cruise/images/Flight.png"></img></div>
	 				   							</c:if>
	 				   						</li>
                                        </ul>
                                    </li>
                                    <c:set var="marketingAirlineCode" value="${inboundFlight.marketingAirlineCode }"/>
                                    <c:set var="operatingAirlineCode" value="${inboundFlight.operatingAirlineCode}"/>
                                    <c:if test="${marketingAirlineCode != null && operatingAirlineCode != null && operatingAirlineCode != marketingAirlineCode}">
                                        <li>Operated
                                            <ul>
                                                <li><c:out value="${inboundFlight.operatingAirlineShortName}" escapeXml="false"/></li>
                                            </ul>
                                            <br />by&#58;
                                        </li>
                                    </c:if>
                                </ul>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </c:if>
        </div>
        <%-- Start of ports information cabin_only and ex-uk only --%>
        <c:if test='${bookingComponent.cruiseSummary.ship.accommodationType == "CRUISE_EX_UK" || bookingComponent.cruiseSummary.ship.accommodationType == "CRUISE"}'>
            <div id="portandcruisesection">
                <h3>Port Information</h3>
                <dl class="portinfo">
                    <dt>Embarkation Port&#58; </dt>
                    <dd>
                        <c:out value="${bookingComponent.cruiseSummary.startPort}"/>
                        <fmt:formatDate value="${bookingComponent.cruiseSummary.startDate}" pattern="dd/MM/yy" />
                    </dd>
                    <dt>Disembarkation Port&#58; </dt>
                    <dd>
                        <c:out value="${bookingComponent.cruiseSummary.endPort}"/>
                        <fmt:formatDate value="${bookingComponent.cruiseSummary.endDate}" pattern="dd/MM/yy" />
                    </dd>
                </dl>
            </div>
        </c:if>
        <%-- End of ports information --%>
        <%-- start of Accomodation --%>
            <c:if test='${(bookingComponent.cruiseSummary.ship.accommodationType == "CRUISE_AND_STAY" || bookingComponent.cruiseSummary.ship.accommodationType == "STAY_AND_CRUISE") && (bookingComponent.cruiseSummary.accommodation != null)}'>
                <div>
                    <h3>Accommodation</h3>
                    <h4><c:out value="${bookingComponent.cruiseSummary.accommodation.hotel}"/>
                    <c:set var="trating" value="${bookingComponent.cruiseSummary.accommodation.rating}"/>
                    <c:if test="${trating != 'trating0'}">
                        <img src="/cms-cps/shop/cruise/images/<c:out value='${trating}'/>.gif" border="0"/>
                    </c:if></h4><br/>
                    <dl class="accom_details">
                        <dt>Country&#58; </dt>
                        <dd><c:out value="${bookingComponent.cruiseSummary.accommodation.country}"/></dd>
                        <dt>Destination&#58; </dt>
                        <dd><c:out value="${bookingComponent.cruiseSummary.accommodation.destination}"/></dd>
                        <dt>Resort&#58; </dt>
                        <dd><c:out value="${bookingComponent.cruiseSummary.accommodation.resort}"/></dd>
                        <dt>Duration&#58; </dt>

                        <c:choose>
                          <c:when test="${bookingComponent.cruiseSummary.stayDuration gt 0 }">
                            <c:set var="stayDuration" value="${bookingComponent.cruiseSummary.stayDuration}" />
                          </c:when>
                        <c:otherwise>
                           <c:set var="stayDuration" value="${bookingComponent.cruiseSummary.duration}" />
                        </c:otherwise>
                        </c:choose>

                          <c:choose>
                            <c:when test="${stayDuration =='1'}">
                                <dd><c:out value="${stayDuration}" />  night</dd>
                            </c:when>
                            <c:otherwise>
                                <dd><c:out value="${stayDuration}" />  nights</dd>
                            </c:otherwise>
                        </c:choose>
                        <dt>Board Basis&#58;</dt>
                        <dd>
                            <span id="boardBasisSummary">
                                <c:out value="${bookingComponent.cruiseSummary.accommodation.boardBasis}"/>
                            </span>
                        </dd>
                    </dl>
                </div>
            </c:if>
            <%-- End of Accomodation --%>
            <%-- Cruise details --%>
            <div>
                <h3>Cruise</h3>
                <div class="cruisedetails">
                    <dl>
                        <dt> Itinerary&#58; </dt>
                        <dd><c:out value="${bookingComponent.cruiseSummary.ship.resort}"/></dd>
                        <dt> Ship&#58; </dt>
                        <dd><c:out value="${bookingComponent.cruiseSummary.ship.hotel}"/></dd>
                        <dt>Duration&#58; </dt>
                        <c:choose>
                            <c:when test="${bookingComponent.cruiseSummary.duration == 1}">
                                <dd><c:out value="${bookingComponent.cruiseSummary.duration}" /> night</dd>
                            </c:when>
                            <c:otherwise>
                                <dd><c:out value="${bookingComponent.cruiseSummary.duration}" /> nights</dd>
                            </c:otherwise>
                        </c:choose>
                        <dt>Board Basis&#58;</dt>
                        <dd>
                            <span id="boardBasisSummary">
                                <c:out value="${bookingComponent.cruiseSummary.ship.boardBasis}"/>
                            </span>
                        </dd>
                    </dl>
                </div>
            </div>
            <%-- Cruise details ends here --%>
        </div>
        <c:if test='${bookingComponent.cruiseSummary.ship.accommodationType == "CRUISE"}'>
            <div align="center">
                <a href="javascript:Popup('<c:out value="${bookingComponent.clientDomainURL}/thomson/page/shop/common/atoz/atoztandc.page"/>',755,584,'scrollbars=yes');">A-Z Booking Information</a>
            </div>
        </c:if>
    </div>
