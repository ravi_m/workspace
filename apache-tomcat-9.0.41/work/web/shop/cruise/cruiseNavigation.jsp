<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div class="width_add">
    <span class="backlink">
        <a href="<c:out value='${bookingComponent.clientDomainURL}'/>/thomson/page/shop/cruise/booking/prepayment.page" title="Back" class="backlink">
            <img src="/cms-cps/shop/common/images/img_dp_button_back.gif" width="17" height="16" border="0" alt="Back"/>Back
        </a>
    </span>
    <c:if test="${not empty bookingComponent.paymentType and bookingInfo.newHoliday}">
        <span id="confirmButton"> <%-- The pay button will be enabled only if the param newHoliday is set to true. --%>
            <a href="javascript:void(0);" onclick="return FormHandler.handleSubmission(document.forms[0]);">
                <img src="/cms-cps/shop/common/images/btn_confirm_booking.gif" width="119" height="23" alt="Confirm booking" title="Confirm booking" border="0" id="confirmbooking" />
            </a>
        </span>
    </c:if>
</div>
<br clear="all" />
