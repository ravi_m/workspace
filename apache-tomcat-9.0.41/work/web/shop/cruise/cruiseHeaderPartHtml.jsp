<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="url" value='${bookingComponent.clientDomainURL}'/>
<c:choose>
	<c:when test="${bookingComponent.shopDetails.retailBrand == 'FIRS'}">
		<ul id="p_thead_menufc">
			<li class="firstChild"><a href="<c:out value='${url}'/>/thomson/page/shop/byo/home/home.page" target="_top">Home</a></li>
			<li><a href="<c:out value='${url}'/>/thomson/page/shop/byo/home/lateshome.page" target="_top">Late Offers</a></li>
			<li><a href="http://tui-retail-braclive/">Manage Booking</a></li>
			<%--/*<li id="hideBalancePayment"><a href="<c:out value='${url}'/>/thomson/page/shop/postbooking/search/shopbookingssearchpanel.page" target="_top">Balance Payment</a></li>*/--%>
			<li><a onclick="LaunchExe();" href="file:///C:/Program%20Files/TTG/Rapid/Vision.exe" target="_blank">Rapid</a></li>
			<li><a href="http://click" target="_blank">InsideClick Retail</a></li>
			<li><a href="http://click/aw/Retail/On_the_web/~bgwn/On_the_web/" target="_blank">On the Web</a></li>
			<li><a target="_blank" href="http://www.specialistholidays.com/agents">Specialist Holidays Group</a></li>
		</ul>
		<div class="homefcLogo"><img src="/cms-cps/shop/byo/images/logos/first_choice_logo.gif" alt="First Choice"/></div>
	<!-- </div> -->
	</c:when>
	<c:when test="${bookingComponent.shopDetails.retailBrand == 'THOM'}">
		<div id="thheadcolor">
			<ul id="p_thead_menu">
				<li class="firstChild"><a href="<c:out value='${url}'/>/thomson/page/shop/byo/home/home.page" target="_top">Home</a></li>
				<li><a href="<c:out value='${url}'/>/thomson/page/shop/byo/home/lateshome.page" target="_top">Late Offers</a></li>
				<li><a href="http://tui-retail-braclive/">Manage Booking</a></li>
				<%--/*<li id="hideBalancePayment"><a href="<c:out value='${url}'/>/thomson/page/shop/postbooking/search/shopbookingssearchpanel.page" target="_top">Balance Payment</a></li>*/--%>
				<li><a onclick="LaunchExe();" href="file:///C:/Program%20Files/TTG/Rapid/Vision.exe" target="_blank">Rapid</a></li>
				<li><a href="http://click" target="_blank">InsideClick Retail</a></li>
				<li><a href="http://click/aw/Retail/On_the_web/~bgwn/On_the_web/" target="_blank">On the Web</a></li>
				<li><a target="_blank" href="http://www.specialistholidays.com/agents">Specialist Holidays Group</a></li>
			</ul>
			<a href="<c:out value='${url}'/>/thomson/page/shop/byo/home/home.page">
				<img src="/cms-cps/shop/common/images/header.gif" width="800" height="110" border="0" alt="Thomson.co.uk" name="header_thomsonlogo" style="margin-top:0px;"/>
			</a>
		</div>
	</c:when>
	<c:otherwise>
		<ul id="p_thead_menufc">
			<li class="firstChild"><a href="<c:out value='${url}'/>/thomson/page/shop/byo/home/home.page" target="_top">Home</a></li>
			<li><a href="<c:out value='${url}'/>/thomson/page/shop/byo/home/lateshome.page" target="_top">Late Offers</a></li>
			<li><a href="http://tui-retail-braclive/">Manage Booking</a></li>
			<%--/*<li id="hideBalancePayment"><a href="<c:out value='${url}'/>/thomson/page/shop/postbooking/search/shopbookingssearchpanel.page" target="_top">Balance Payment</a></li>*/--%>
			<li><a onclick="LaunchExe();" href="file:///C:/Program%20Files/TTG/Rapid/Vision.exe" target="_blank">Rapid</a></li>
			<li><a href="http://click" target="_blank">InsideClick Retail</a></li>
			<li><a href="http://click/aw/Retail/On_the_web/~bgwn/On_the_web/" target="_blank">On the Web</a></li>
			<li><a target="_blank" href="http://www.specialistholidays.com/agents">Specialist Holidays Group</a></li>
		</ul>
		<a href="<c:out value='${url}'/>/thomson/page/shop/byo/home/home.page">
			<img src="/cms-cps/shop/byo/images/logos/Main_logo.jpg" alt="Falcon"/>
		</a>
	</c:otherwise>
</c:choose>
<script type="text/javascript">
	function LaunchExe()
	{
		if(window.XMLHttpRequest)
		{
			if (window.ActiveXObject)
			{
				var WshShell = new ActiveXObject("WScript.Shell");
				var dirfile="C:\\Program%20Files\\TTG\\Rapid\\Vision.exe";
				var oExec = WshShell.Exec(dirfile);
			}
			else
			{
				window.open('file:///C:/Program%20Files/TTG/Rapid/Vision.exe');
			}
		}
		else
		{
			window.open('file:///C:/Program%20Files/TTG/Rapid/Vision.exe');
		}
	}
</script>
<%-- /*following is brand specific Impl*/ --%>
<script type="text/javascript">
        var rqry = "iREGQry";
        var isl = "http"+(window.location.href.indexOf('https:')==0?'s':'')+"://";
        var pqry = "iAddPAR";
        var sqry = "iSale";
        var idl = window.location.href;
        <c:if test="${not empty bookingComponent.intelliTrackSnippet}">
            pqry = '<c:out value="${bookingComponent.intelliTrackSnippet.intelliTrackerString}"/>';
            var previousPageVar;
            var prvPageFromSession ="<c:out value='${bookingComponent.intelliTrackSnippet.previousPage}'/>";
            if(prvPageFromSession!="")
            {
               previousPageVar = prvPageFromSession.substr(prvPageFromSession.lastIndexOf("/")+1);
            }
            pqry += "PrevPage="+previousPageVar;
        </c:if>
</script>
<script type='text/javascript'>isl=isl +"tui.intelli-direct.com/e/t3.dll?249&";</script>
<script src="/cms-cps/shop/common/js/intellitracker.js" type='text/javascript'></script>