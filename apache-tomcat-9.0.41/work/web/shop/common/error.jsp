<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
       <version-tag:version/>
       <title>Build Your Own Holidays - Error!! </title>
       <%-- Include  CSS  here --%>
       <link rel="stylesheet" type="text/css" href="/cms-cps/shop/common/css/headfoot.css" />
       <link rel="stylesheet"  type="text/css" href="/cms-cps/shop/common/css/logoff.css" />
       <link rel="stylesheet" type="text/css" href="/cms-cps/shop/common/css/fc_headfoot.css" />
       <link rel="stylesheet"  type="text/css" href="/cms-cps/shop/common/css/404.css" />
      <%-- End of css files --%>
       <%-- Include  JS Here --%>
        <script type="text/javascript" src="/cms-cps/shop/common/js/login.js"></script>
        <%-- End of  JS files --%>
   </head>
   <body>
            <c:choose>
		<c:when test="${bookingInfo.bookingComponent.shopDetails.retailBrand == 'FIRS'}">
     			  <div id="fcheader">
		 </c:when>
		<c:when test="${bookingInfo.bookingComponent.shopDetails.retailBrand == 'THOM'}">
     			  <div id="header">
		 </c:when>
		 <c:otherwise>
      <div id="header">
	  </c:otherwise>
	   </c:choose>
          <jsp:include page="headerError.jsp" />
      </div>
      <div id="logoff">
	  <c:choose>
		<c:when test="${not empty sessionScope}" >
	         <jsp:include page="/shop/common/logoff.jsp" />
		</c:when>
		<c:otherwise>
	         <jsp:include page="/shop/common/logoffError.jsp" />
		</c:otherwise>
	  </c:choose>
      </div>

      <%-- Body section --%>
      <div id="bodysection">
         <div id="content">   <%--Right panel   --%>
             <h1>We're sorry...</h1>
             <div class="warning">
                  <c:if test="${param.clientApplication=='WiSHBYO' }">
                          <%--****Warning messages will be displayed here **** --%>
                          <h2></h2>
                           <p>
                               We are having trouble processing your booking, Please try again to search and process your booking. If the problem persists please contact the helpdesk and report the issue.
                            </p>
                            <%--****End of Warning messages **** --%>
                  </c:if>
                  <c:if test="${param.clientApplication=='WiSHAO'}">
                          <%--**Warning messages will be displayed here **** --%>
                           <h2></h2>
                           <p>
                              If you continue to experience problems, please contact our travel experts on <strong> 0800 032 5473</strong>.
                            </p>
                           <%--***End of Warning messages **** --%>
                  </c:if>
                  <c:if test="${param.clientApplication=='Cruise'}">
                         <h2></h2>
                           <p>Sorry, we are unable to process your request at this moment.
                               Please try again later or contact our Holiday Experts on<br/><b>0870 167 6533</b>
                            </p>
                  </c:if>


             </div>

             <div id="middlecontainer">
                 <div id="picturebar">

                 </div>
                 <div id="extraoptions">

                 </div>
             </div>

         </div><%--End of Right panel  --%>

         <div id="leftpanel">  </div>
      <%-- End of body section --%>

   </body>
</html>
