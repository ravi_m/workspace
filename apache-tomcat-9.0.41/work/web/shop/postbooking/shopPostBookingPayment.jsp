<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<%@include file="/common/commonTagLibs.jspf"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
       <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1;" />
       <version-tag:version/>
       <title>Build Your Own Holidays - Flight and Hotels - Payment </title>

       <%-- Include Payment related CSS  here --%>
       <link rel="stylesheet" type="text/css" href="/cms-cps/shop/common/css/headfoot.css" />
       <link rel="stylesheet"  type="text/css" href="/cms-cps/shop/common/css/logoff.css" />

        <link rel="stylesheet" type="text/css"  href="/cms-cps/shop/postbooking/css/payment.css"  />
        <link rel="stylesheet"  type="text/css" href="/cms-cps/shop/postbooking/css/travel_opts.css" />
        <link rel="stylesheet"  type="text/css" href="/cms-cps/shop/postbooking/css/integrated_payment.css" />
       <%-- End of css files --%>

       <%-- Include Payment related JS Here --%>
      <script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
        <script type="text/javascript" src="/cms-cps/shop/common/js/paymentdetails.js"></script>
        <script type="text/javascript" src="/cms-cps/shop/common/js/login.js"></script>
        <script type="text/javascript" src="/cms-cps/common/js/paymentUpdation.js"></script>
        <script type="text/javascript" src="/cms-cps/common/js/paymentPanelContent.js"></script>
        <script type="text/javascript" src="/cms-cps/common/js/paymentPanelValidation.js"></script>
        <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>


        <script type="text/javascript" src="/cms-cps/shop/common/js/intellitracker.js"></script>
        <script type="text/javascript" src="/cms-cps/shop/common/js/popup.js"></script>
        <script type="text/javascript" src="/cms-cps/shop/common/js/addressfinder.js"></script>
        <script type="text/javascript" src="/cms-cps/common/js/pinpadProcessor.js"></script>

        <script type="text/javascript" src="/cms-cps/shop/postbooking/js/formvalidation.js"></script>

       <%-- End of JS files --%>

   </head>

   <body>
  <c:if test="${not empty bookingInfo.bookingComponent.paymentType}">
   <%-- Pinpad DLL initialization starts here --%>
      <object name="moATS" classid="clsid:FB6D3933-5128-4B13-AD9D-06B6B18C29F1" declare="declare" style="display:none;"></object>
      <script language="vbscript">
        On Error Resume Next
      Err.Clear
          moATS.Connect("C:\Program Files\TTG\Rapid\TUICNP.DAT")
        On Error Resume Next
      Err.Clear
      </script>
   <%-- Pinpad DLL initialization ends here --%>
    </c:if>
      <div id="header">
          <jsp:include page="shopPostBookingHeaderPartHtml.jsp" />
      </div>
      <div id="logoff">
         <jsp:include page="/shop/common/logoff.jsp" />
      </div>
      <%-- Body section --%>
      <form name="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}' />&tomcat=<c:out value='${param.tomcat}' />"/>
    <input type="hidden" name="tomcatInstance" id="tomcatInstance" value="<c:out value='${param.tomcat}' />"/>
      <div id="bodysection">
         <div id="content">   <%--Right panel   --%>
                <jsp:include page="shopPostBookingPaymentContent.jsp" />
         </div>                   <%--End of Right panel  --%>
         <div id="leftpanel">  <%--Left panel   --%>
               <jsp:include page="shopPostBookingSummaryPanel.jsp"/>
         </div>                     <%--End of Left panel  --%>
      </div>
      </form>
      <%-- End of body section --%>
      <div id="footer">  <%-- Footer --%>
      </div>     <%-- End of  Footer --%>
   </body>
</html>
