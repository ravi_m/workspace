<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div id="payment_amount" class="borderone">
 <h2>Payment Amount</h2>
 <p>
 <input type="hidden" id="totalHolidayCostH" name="totalHolidayCostH" value='<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>'/>
  Your total holiday cost is <strong>&pound;<span><fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>*</span ></strong>
  <br/><br/>
  <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}" varStatus="vardepost">
   <c:if test="${depositComponent.depositType == 'olbpFullBalance'}">
	 <c:set value="${depositComponent.depositAmount.amount}" var="fullbalance"/>
	 <c:set value="${depositComponent.depositDueDate}" var="outstandingDueDate" />
   </c:if>
   <c:if test="${depositComponent.depositType == 'alreadyPaid'}">
     <c:set value="${depositComponent.depositAmount.amount}" var="alreadyPaid"/>
	 <c:set value="${depositComponent.depositDueDate}" var="depositDueDate" />
   </c:if>
  </c:forEach>
  <c:set var="depositDue" value="${bookingInfo.calculatedTotalAmount.amount - alreadyPaid}"/>
  <c:choose>
  <c:when test="${depositDue == 0}">
   <div class="highlight">
   Full payment was made on&nbsp;<fmt:formatDate value="${depositDueDate}"pattern="dd/MM/yy"/>
   &nbsp;of&nbsp;&pound;<fmt:formatNumber value="${alreadyPaid}" type="number" maxFractionDigits="2"/>
   </div>
  </c:when>
  <c:otherwise>
   A deposit of &pound;<span id="lowoutstandingbalance" ><fmt:formatNumber value="${alreadyPaid}" type="number" maxFractionDigits="2"/></span> was paid on <fmt:formatDate value="${depositDueDate}" pattern="dd/MM/yy"/>. please select your preferred payment amount due now:
 </p>
 <div class="highlight">
  <p class="radio">
   <label>
    <strong>
      Part Payment:&nbsp;&pound;&nbsp;
    </strong>
   </label>
   <input type="text" value="0.0" name="partPayment" id="partPayment" class="medium" onChange="javascript:validatePartPayment()"/>
   <input type="hidden" value="<c:out value='${fullbalance}'/>" name="maxFullBalance" id="maxFullBalance"/>
  </p>
  <div style="margin-left:580px;">
    <input type="radio" name="depositType" id="lowDepositR" checked="checked" value="lowdeposit" onclick="setSelectedOption('partialPayment');"/>
  </div>
  <p class="radio"><hr width="690" size="1" color="lightblue"/></p>
  <div>
    <p>
      The outstanding balance of your deposit is <strong>&pound;<span id="lowoutstandingbalance" ><fmt:formatNumber value="${fullbalance}" type="number" maxFractionDigits="2"/></span>**</strong> and is due on
      <strong>
	    <fmt:formatDate value="${outstandingDueDate}" pattern="dd/MM/yy"/>
      </strong>
    </p>
    <p class="radio"><hr width="690" size="1" color="lightblue"/></p>
  </div>
  <div>
	<p class="radio">
	  <strong> Full Balance:&nbsp;&pound;
	    <span id="olbpFullBalance" ><fmt:formatNumber value="${fullbalance}" type="number" maxFractionDigits="2"/></span >*
      </strong>
	</p>
	<div style="margin-left:580px;">
	  <input type="radio" name="depositType" id="fullBalance" checked="checked" value="lowdeposit" onclick="setSelectedOption('olbpFullBalance');"/>
	</div>
  </div>
 </div>
 <p>* If you pay by credit card the charges will be updated in the price panel once you have selected your method of payment.</p>
 <p>** There are no additional charges when paying by Maestro ,MasterCard Debit or Visa/Delta debit cards.A fee of 2.0% applies to credit card payments,which is capped at �95.00 per transaction when using American Express,MasterCard Credit or Visa Credit Cards.</p>
   </c:otherwise>
 </c:choose>

</div>