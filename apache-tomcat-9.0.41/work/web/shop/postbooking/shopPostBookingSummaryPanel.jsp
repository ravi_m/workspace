<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />

<c:if test="${not empty bookingComponent.flightSummary}">
<jsp:include page="shopPostBookingFlightPanel.jsp" /><br/>
</c:if>
<c:if test="${not empty bookingComponent.accommodationSummary}">
<jsp:include page="shopPostBookingAccommodationPanel.jsp" /><br/>
</c:if>

