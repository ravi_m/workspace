<c:set var="clientUrlLinks" value="${bookingComponent.clientURLLinks}" scope="request"/>
<div class="altpanelproductnav">
  <div id="portalnavigation" class="navtable header_image_general">
	 <div id="tabSearch" class="selectedlink">
		<a href="<c:out value='${url}'/><c:out value='${clientUrlLinks.homePageURL}'/>">Search</a>
	 </div>
	 <div id="tabDestinations" class="noselectedlink">
		<a href="<c:out value='${url}'/><c:out value='${clientUrlLinks.accomDetailsURL}'/>">Destinations</a>
	 </div>
	 <div id="emptylink" class="noselectedlink">&nbsp;</div>
	 <br clear="all"/>
	 <div class="container_titletext"><h1 class="homepage_topbanner">Hotels, Villas &amp; Apartments</h1></div>
  </div>
</div>