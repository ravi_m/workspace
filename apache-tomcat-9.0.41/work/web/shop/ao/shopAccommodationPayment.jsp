<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
	 <meta http-equiv="Content-Type" content="text/html;" />
	 <version-tag:version/>
	 <title>Thomson.co.uk - Great value accommodation from the UK's no.1 tour operator</title>

    <%------------------------------------------------------------------------------------------------%>
	<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
    <c:set var="url" value="${bookingComponent.clientDomainURL}" scope="request"/>
    <c:set var="clientapp" value="${bookingComponent.clientApplication.clientApplicationName}" scope="request" />
	 <%------------------------------------------------------------------------------------------------%>
	 <%-- All css files and js files --%>
	 <%@include file="resources.jspf" %>

  </head>

  <body>
  	<c:set var="customerType" value="${bookingComponent.nonPaymentData['customerType']}" scope="request"/>
	<c:set var="selectedDeposit" value="${bookingComponent.nonPaymentData['depositType']}" scope="request"/>
	<%--Since pan and security code are in form of character array we are converting them to String--%>
    <c:forEach var="paymentTransaction" items="${paymentTransactions}">
       <c:forEach var="pan" items="${paymentTransaction.card.pan}">
          <c:set var="pandata" value="${pandata}${pan}" />
       </c:forEach>
       <c:forEach var="securitycode" items="${paymentTransaction.card.cv2}">
          <c:set var="securitycodedata" value="${securitycodedata}${securitycode}" />
       </c:forEach>
       <c:set var="responseMsg" value="${transactionResponseMsg}" scope="request"/>
    </c:forEach>
	 <%@ include file="/shop/common/scriptConfigSettings.jspf" %>
	 <%@include file="pinpadScripts.jspf" %>


	 <div id="dp_homepage_maincontainer" class="bgcolor_maincontainer">
	 <%------------------------------------------------------------------------------------------------%>
		<%@include file="shopAccommodationHeaderPartHtml.jsp" %>
		<%@include file="/shop/common/logoff.jsp" %>
		<%@include file="headerImage.jspf"%>
	 <%------------------------------------------------------------------------------------------------%>

		<div id="dp_homepage_bodysection_maincontainer">
		  <div class="globalNavigationMargin" style="clear:both;"></div>
		  <%@include file="shopAccommodationProductNavigation.jsp" %>

 		  <form name="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}' />&amp;brand=<c:out value='${param.brand}'/>&amp;tomcat=<c:out value='${param.tomcat}'/>">
 			<%-----------------------------Summary Panel-------------------------------------------------%>
			 <div id="leftpanel_container" >
			    <%@include file="shopAccommodationSummaryPanel.jsp" %>
		    </div>
		   <%------------------------------------------------------------------------------------------%>

         <%------------------------------Payment Panel------------------------------------------------%>
		 			    <%@include file="pageErrors.jspf" %>
             <div id="fh7paymentdetails_container_main">
                       <c:if test="${not empty bookingComponent.depositComponents}">
             <%@include file="shopAccommodationDepositOptions.jsp" %>
          </c:if>
				 <%@include file="shopAccommodationPaymentdetails.jsp" %>
             <c:if test ="${not empty bookingComponent.termsAndCondition.relativeTAndCUrl}">
                <%@include file="shopAccommodationTermsAndCondition.jsp" %>
             </c:if>
             <%@include file="shopAccommodationNavigation.jsp" %>
             </div><%--END fh7paymentdetails_container_main --%>
         <%------------------------------------------------------------------------------------------%>
	     </form>
		  <br clear="all" />
	   </div><%--END dp_homepage_bodysection_maincontainer--%>
	 </div><%--END  dp_homepage_maincontainer--%>
  </body>
</html>

<div id="paymentFields_required" style="display:none"><input type='hidden'  id='panelType' value='CP'/></div>

<script type="text/javascript">
  var tottransamt="Total amount charged <br/>in this transaction:";
</script>

<script type="text/javascript">
  var newHoliday = "<c:out value='${bookingInfo.newHoliday}'/>";
  window.onunload=function()
  {
    transactionStopped = true;
    moATS.Reset;
    moATS.Close;
  }
</script>