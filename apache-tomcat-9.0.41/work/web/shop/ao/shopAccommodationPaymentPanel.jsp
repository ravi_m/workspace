<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<%-------------------- Settings to be used through out this file ----------------------------------------%>
<fmt:setLocale value="en_GB"/>
<c:set var="payableOnCheckout" value="false" scope="page"/>
<c:set var="invSystem" value="${bookingComponent.accommodationSummary.accommodationInventorySystem}" scope="page"/>
<c:set var="isHoplaPrePay" value="${bookingComponent.accommodationSummary.prepay}" scope="page"/>
<%--card charge special  --%>
<c:set var="insertCardChargeSection" value="true" scope="page"/>
<c:set var="styleCardCharge"  value="display:none"/>
<c:if test="${bookingInfo.newHoliday == 'false' && (bookingInfo.calculatedCardCharge.amount != null && bookingInfo.calculatedCardCharge.amount != 0)}">
    <c:set var="styleCardCharge"  value="display:block"/>
</c:if>
<%--End of card charge special  --%>

<%------------------- End of Settings to be used through out this file ---------------------------------%>
<%--*** Start of Header ***--%>
<c:choose>
    <c:when test="${hoplaBonded && invSystem == 'HOPLA_THM' || invSystem == 'HOPLA_PEG'}">
        <div class="container_summarypanel_head">Price</div>
    </c:when>
    <c:otherwise>
        <div class="container_summarypanel_head">
            <div class="container_summarypanel_imgHeader">
                <c:choose>
                    <c:when test="${bookingComponent.companyCode == 'FC'}">
                        <span><img src="/cms-cps/shop/ao/images/fc-logo-180x23.gif" height="23" border="0" alt="First Choice"/></span>
                    </c:when>
                    <c:otherwise>
                        <span><img src="/cms-cps/shop/common/images/logo-summary-panel-thomson.gif" width="90" height="12" border="0" alt=""/></span>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </c:otherwise>
</c:choose>
<%--***End of Header ***--%>
<br clear="all"/>
<div class="container_price" id="price">
    <c:forEach var="costingLine" items="${bookingComponent.priceComponents}" varStatus="iterationStatus">
        <%---------------------------- Logic  for currency to be displayed on pricepanel --------------------------------------%>
            <c:set var="pricePanelCurrency">
                <c:choose>
                    <c:when test='${costingLine.amount.currency == "EUR"}'>&euro;</c:when>
                    <c:when test='${costingLine.amount.currency == "GBP"}'>&pound;</c:when>
                    <c:when test='${costingLine.amount.currency == "USD"}'>&#36;</c:when>
                    <c:otherwise><c:out value="${costingLine.amount.currency}"/></c:otherwise>
                </c:choose>
            </c:set>
        <%-----------------------** End of  Logic for currency description **------------------------------------------------%>
        <%---------------------------- Logic to show costingLine description --------------------------------------------------%>
        <c:set var="description">
            <c:choose>
                <c:when test="${fn:containsIgnoreCase(costingLine.itemDescription, 'Total Base Price') }"><c:out value="Payable on checkout"/>:</c:when>

                <c:when test="${fn:containsIgnoreCase(costingLine.itemDescription, 'Payable on checkout') }"><c:out value=""/></c:when>

                <c:when test="${not empty costingLine.quantity}">
                    <c:out value="${costingLine.itemDescription}"/><c:if test="${not empty costingLine.quantity}">
                    (x<c:out value="${costingLine.quantity}" escapeXml="false"/>)</c:if>:
                </c:when>

                <c:when test="${invSystem == 'HOPLA_PEG' || invSystem == 'HOPLA_THM'}">
                    <c:if test="${!isHoplaPrePay}"><c:out value="Total payable <br/>now :" escapeXml="false"/></c:if>
                </c:when>
                <c:otherwise><c:out value="${costingLine.itemDescription}"/>:</c:otherwise>
            </c:choose>
        </c:set>
        <%--------------------------------*** End of Logic to show costingLine description ***---------------------------------------------------%>
        <%---------------------------- Logic to show costingLine header --------------------------------------------------%>
        <c:if test="${invSystem == 'HOPLA_PEG' || invSystem == 'HOPLA_THM'}">
            <c:set var="descriptionHeader"><c:if test="${fn:containsIgnoreCase(costingLine.itemDescription, 'Total Base Price') }"><c:out value="Hotel"/></c:if></c:set>
        </c:if>
        <%--------------------------------*** End of Logic to show costingLine description ***---------------------------------------------------%>
        <%---------------------------------  Logic to show costingLine amount --------------------------------------------------------------------%>
        <c:set var="amount">
            <c:choose>
                <c:when test="${fn:containsIgnoreCase(costingLine.itemDescription, 'Payable on checkout') }">(&pound;<c:out value="${costingLine.amount.amount}"/>&dagger;)</c:when>
                <c:when test="${fn:containsIgnoreCase(costingLine.itemDescription, 'Thomson Savings')}">-<c:out value="${pricePanelCurrency}" escapeXml="false"/><c:out value="${costingLine.amount.amount}"/></c:when>
                <c:otherwise><c:out value="${pricePanelCurrency}" escapeXml="false"/><c:out value="${costingLine.amount.amount}"/></c:otherwise>
            </c:choose>
        </c:set>
        <%--------------------------------**  End of Logic to show costingLine amount **--------------------------------------------------------------------%>
        <%------------------------------- Logic to apply Styles for price panel items----------------------------------------------------%>
        <%--  Add span class as below
            <li <c:out value='${listStyle}
                span class="<c:out value='${styleDescriptionClasses}......Hotel
                span class="<c:out value='${styleAmountClasses}......
        --%>
        <%-- item  styles  --%>
        <c:choose>
            <c:when test="${costingLine.amount.amount != '0.00'}"><c:set var="styleDescriptionClasses" value="price_title_marginbottom"/></c:when>
            <c:otherwise><c:set var="styleDescriptionClasses" value=""/></c:otherwise>
        </c:choose>
        <c:set var="styleAmountClasses" value="price_amount_marginbottom" />
        <c:set var="costingLineAmountId" value="" />
        <c:set var="styleHeaderClasses" value="" />
        <c:choose>
            <%--Following are for normal bookings --%>
            <c:when test="${fn:contains(costingLine.itemDescription,'Travel Extras') || fn:contains(costingLine.itemDescription,'Hotel') || fn:contains(costingLine.itemDescription,'Flight') || fn:contains(costingLine.itemDescription,'Flight + Hotel')}">
                <c:choose>
                    <c:when test="${invSystem == 'HOPLA_PEG' || invSystem == 'HOPLA_THM'}">
                        <c:if test="${isHoplaPrePay}"><c:set var="styleDescriptionClasses" value="price_title_marginbottom"/></c:if>
                        <c:if test="${!isHoplaPrePay}"><c:set var="styleDescriptionClasses" value="price_title_marginbottom boldme" /></c:if>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${!fn:contains(costingLine.itemDescription,'Flight Meals')}"><c:set var="styleDescriptionClasses" value="price_title_marginbottom boldme"/></c:when>
                            <c:otherwise><c:set var="styleDescriptionClasses" value="price_title_marginbottom"/></c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
                <c:set var="styleAmountClasses" value="price_amount_marginbottom" />
            </c:when>
            <c:when test="${costingLine.itemDescription =='Total' }">
                <c:set var="costingLineAmountId" >id = totalText</c:set>
                <c:set var="styleAmountClasses" value="price_amount total" />
                <c:set var="styleDescriptionClasses" value="price_title priceandtitle" />
            </c:when>
            <%-- Following are for hopla bookings --%>
            <c:when test="${fn:contains(description,'Total payable <br/>now :')}">
                <c:set var="costingLineAmountId" >id = totalText</c:set>
                <c:set var="styleAmountClasses" value="total price_amount" />
                <c:set var="styleDescriptionClasses" value="price_title priceandtitle" />
            </c:when>
            <c:when test="${fn:contains(costingLine.itemDescription,'Total Base Price')}">
                <c:set var="styleHeaderClasses" value="accommodation_titles_hopla" />
                <c:set var="styleDescriptionClasses" value="price_title_marginbottom" />
                <c:set var="styleAmountClasses" value="price_amount_marginbottom" />
            </c:when>
            <c:when test="${fn:contains(costingLine.itemDescription,'Payable on checkout')}">
                <c:set var="styleDescriptionClasses" value="price_title_marginbottom" />
                <c:set var="styleAmountClasses" value="price_amount_marginbottom" />
            </c:when>
        </c:choose>
        <%--end item styles  --%>
        <%----------------------******End of Logic to apply Styles for price panel elements*****----------------------------------------------------%>
        <%--------------------------Now display costingline/price items in summary panel -------------------------------------------------------------%>
        <%--  Insert card charge --%>
        <c:if test="${(fn:containsIgnoreCase(description,'Total payable <br/>now :') || costingLine.itemDescription =='Total') && insertCardChargeSection }">
            <span id="cardChargeText" class="cardchargeBlock hide">
                <span class="price_title_marginbottom">Card charges:</span>
                <span id='cardChargeAmount' class='calculatedCardChargeSummaryPanel'>&pound;<c:out value="${bookingInfo.calculatedCardCharge.amount}"/></span>
            </span>
            <c:set var="insertCardChargeSection" value="false" scope="page"/>
        </c:if>
        <c:if test="${!fn:containsIgnoreCase(costingLine.itemDescription,'TBP') && (styleHeaderClasses != '' || styleDescriptionClasses != '')}">
            <span class="<c:out value='${styleHeaderClasses}'/>"><c:out value="${descriptionHeader}" escapeXml="false"/></span>
                        <c:choose>
                <c:when test="${costingLine.itemDescription=='Total'}">
                    <span onmouseover="openTitlePopup('tbpDisplay');" onmouseout="closeTitlePopup('tbpDisplay');" class="<c:out value='${styleDescriptionClasses}'/>"><c:out value="${description}" escapeXml="false"/></span>
                    <span onmouseover="openTitlePopup('tbpDisplay');" onmouseout="closeTitlePopup('tbpDisplay');" <c:out value="${costingLineAmountId}"/> class="totalholidaycostSummaryPanel"><c:out value="${amount}" escapeXml="false"/></span>
                </c:when>
                <c:otherwise>
                    <span class="<c:out value='${styleDescriptionClasses}'/>"><c:out value="${description}" escapeXml="false"/></span>
                    <span <c:out value="${costingLineAmountId}"/> class="<c:out value='${styleAmountClasses}'/>"><c:out value="${amount}" escapeXml="false"/></span>
                </c:otherwise>
            </c:choose>
        </c:if>
        <%-------------------------***End  display costingline/price items in summary panel***----------------------------------------------%>
    </c:forEach>
    <c:forEach var="discountComponent" items="${bookingComponent.discountComponents}">
        <c:if test="${!fn:containsIgnoreCase(discountComponent.discountType,'Total discount %')}">
            <span class="price_title"><c:out value="${discountComponent.discountType}"/>&nbsp;</span>
            <span class="price_amount_marginbottom">-&pound;<fmt:formatNumber type="number" value="${discountComponent.maxDiscountAmount.amount}" maxFractionDigits="2" minFractionDigits="2"/></span>
        </c:if>
        <c:if test="${fn:containsIgnoreCase(discountComponent.discountType,'Total discount %')}">
            <span class="price_title"><c:out value="${discountComponent.discountType}"/>&nbsp;</span>
            <span class="price_amount_marginbottom"><fmt:formatNumber type="number" value="${discountComponent.maxDiscountAmount.amount}" maxFractionDigits="2" minFractionDigits="2"/></span>
        </c:if>
    </c:forEach>
    <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
        <c:if test="${tbpValue && costingLine.itemDescription=='TBP' && costingLine.amount.amount != '0.00'}">
            <span class="price_title"><c:out value="${costingLine.itemDescription}"/>&nbsp;&#61;</span>
            <span class="price_amount_marginbottom"><fmt:formatNumber type="number" value="${costingLine.amount.amount}" maxFractionDigits="2" minFractionDigits="2"/></span>
        </c:if>
        <c:if test="${!tbpValue && costingLine.itemDescription=='TBP' && costingLine.amount.amount != '0.00'}">
            <span class="tbpDesc" id='tbpDisplay'><c:out value="${costingLine.itemDescription}"/>&nbsp;&#61; <fmt:formatNumber type="number" value="${costingLine.amount.amount}" maxFractionDigits="2" minFractionDigits="2"/></span>
        </c:if>
    </c:forEach>
    <%--------------------------- Hopla information text to be displayed  -------------------------%>
    <c:if test="${!isHoplaPrePay}">
        <c:if test="${invSystem == 'HOPLA_PEG' || invSystem == 'HOPLA_THM'}">
            <span class="price_text_marginbottom"><br>&dagger;The pound sterling equivalent may vary depending on the currency conversion exchange rate applicable on the date of payment.</span>
        </c:if>
    </c:if>
    <c:if test="${isHoplaPrePay}">
        <span>
            <c:if test="${invSystem == 'HOPLA_PEG'}">
                <br clear="all"/>
                <span class="price_text_marginbottom">
                    *Some travel suppliers may request full or part payment of balance at time of booking. Local taxes and charges may apply, please check 'Rate details' link or supplier's 'Terms and Conditions' for more information.
                </span>
            </c:if>
        </span>
    </c:if>
    <%--------------------------- End Hopla information text to be displayed  ----------------------%>
</div>
