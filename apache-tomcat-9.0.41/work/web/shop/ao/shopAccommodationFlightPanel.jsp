<%--
 Flight Details jsp  with following Sections

+OutBound Flight Details
  *Departure Airport, Date and Time
  *Arrival Airport, Date and Time
  *Carrier
  *Operated by

+InBound Flight Details
  *Departure Airport, Date and Time
  *Arrival Airport, Date and Time
  *Carrier
  *Operated by

+Flight Itinerary Link

--%>

<div class="container_head_summarypanel_flights hide_in_print">
  <font> Flights</font>
</div>

<div id="flightandhotelsection" >

  <!------------------------------OutBound Flight----------------------------------------------------->
  <div class="flights_head_blue hide_in_print">Leaving&#58;</div>
  <c:forEach var="outboundFlight" items="${bookingComponent.flightSummary.outboundFlight}">

     <!--------------------------Departure Airport, Date and Time------------------------->
      <div class="container_flights hide_in_print">

        <span class="flights_title"> Depart&#58;</span>
        <span class="flights_country"><c:out value="${outboundFlight.departureAirportName}"/>&nbsp;</span>

        <span class="flights_title">&nbsp;</span>
        <span class="flights_date">
            <fmt:formatDate value = "${outboundFlight.departureDateTime}" type="time" pattern="dd/MM/yy"/>
        </span>

        <span class="flights_time">
            <fmt:formatDate value = "${outboundFlight.departureDateTime}" type="time" pattern="HH:mm"/>
        </span>

      </div>
      <!----------------------End Departure Airport, Date and Time------------------------>

      <!----------------------Arrival Airport, Date and Time-------------------------------->
      <div class="container_flights hide_in_print">

        <span class="flights_title"> Arrive&#58; </span>
        <span class="flights_country">
          <c:out value="${outboundFlight.arrivalAirportName}"/>
        </span>

        <span class="flights_time">&nbsp;</span>
        <span class="flights_title">&nbsp;</span>
        <span class="flights_date">
            <fmt:formatDate value="${outboundFlight.arrivalDateTime}" type="time" pattern="dd/MM/yy"/>
        </span>

        <span class="flights_time">
          <fmt:formatDate value="${outboundFlight.arrivalDateTime}" type="time" pattern="HH:mm"/>
        </span>

      </div>
      <!--------------------End Arrival Airport, Date and Time------------------------------->

     <!----------------------Carrier---------------------------------------->
      <div class="container_flights hide_in_print">
        <span class="flights_title">Carrier&#58;</span>
        <span class="flights_country">
          <c:out value="${outboundFlight.carrier}" escapeXml="false"/><br/>
          <c:out value="${outboundFlight.operatingAirlineCode}${outboundFlight.flightNumber}"/>

        </span>
      </div>
     <!----------------------End Carrier------------------------------------>

     <!----------------------Operated by----------------------------------->
      <c:set var="marketingAirlineCode" value="${outboundFlight.marketingAirlineCode }"/>
      <c:set var="operatingAirlineCode" value="${outboundFlight.operatingAirlineCode}"/>
      <c:if test="${marketingAirlineCode != null && operatingAirlineCode != null &&
                    operatingAirlineCode != marketingAirlineCode}">
        <span class="flights_title">Operated by&#58;</span>
        <span class="flights_country">
           <c:out value="${outboundFlight.operatingAirlineShortName}" escapeXml="false"/>
        </span>
      </c:if>
     <!----------------------End Operated by--------------------------------->

      <br/><br/>
      <div><img src="/cms-cps/shop/ao/images/blue_line.gif" width="205" height="1" border="0" alt=""/></div>
    </c:forEach>
    <!----------------------END OutBound Flight-------------------------------------------------------------->

    <!----------------------InBound Flight Details------------------------------------------------------------------->
    <div class="flights_head_blue hide_in_print">Returning&#58;</div>
    <c:forEach var="inboundFlight" items="${bookingComponent.flightSummary.inboundFlight}">

        <div class="container_flights">
          <span class="flights_title">Depart&#58;</span>
          <span class="flights_country">
             <c:out value="${inboundFlight.departureAirportName}"/>
          </span>
          <span class="flights_time" >&nbsp;</span>
          <span class="flights_title">&nbsp;</span>
          <span class="flights_date">
            <fmt:formatDate value="${inboundFlight.departureDateTime}" type="time" pattern="dd/MM/yy"/>
          </span>
          <span class="flights_time">
            <fmt:formatDate value="${inboundFlight.departureDateTime}" type="TIME" pattern="HH:mm"/>
          </span>
        </div>

        <div class="container_flights hide_in_print">
          <span class="flights_title">Arrive&#58;</span>
          <span class="flights_country">
              <c:out value="${inboundFlight.arrivalAirportName}" />
          </span>
          <span class="flights_title">&nbsp;</span>
          <span class="flights_date">
            <fmt:formatDate value="${inboundFlight.arrivalDateTime}" type="time" pattern="dd/MM/yy"/>
          </span>
          <span class="flights_time">
            <fmt:formatDate value="${inboundFlight.arrivalDateTime}" type="TIME" pattern="HH:mm"/>
          </span>
        </div>


      <div class="container_flights hide_in_print">
        <span class="flights_title">Carrier&#58;</span>
        <span class="flights_country">
          <c:out value="${inboundFlight.carrier}" escapeXml="false"/><br/>
          <c:out value="${inboundFlight.operatingAirlineCode}${inboundFlight.flightNumber}"/> <br/>
        </span>
      </div>

      <c:set var="marketingAirlineCode" value="${inboundFlight.marketingAirlineCode }"/>
      <c:set var="operatingAirlineCode" value="${inboundFlight.operatingAirlineCode}"/>
      <c:if test="${marketingAirlineCode != null && operatingAirlineCode != null && operatingAirlineCode != marketingAirlineCode}">
        <span class="flights_title">Operated by&#58;</span>
        <span class="flights_country">
          <c:out value="${inboundFlight.operatingAirlineShortName}" escapeXml="false"/>
        </span>
      </c:if>

      <br/><br/>
      <div><img src="/cms-cps/shop/ao/images/blue_line.gif" width="205" height="1" border="0" alt=""/></div>
    </c:forEach>
    <!--------------------------End InBound Flight Details-------------------------------------------------->

</div>

<!------------------------------Flight Itinerary Link------------------------------------------------------------->
<div class="container_accommodation hide_in_print">
  <span class="accommodation_viewaccommodation">
    <a href='javascript:Popup("<c:out value="${bookingComponent.clientDomainURL}"/>/thomson/page/shop/ao/details/flightitinerarydetails.page",440,400,"scrollbars");'>View Flight Itinerary</a>
  </span>
</div>
<!------------------------------End Flight Itinerary Link---------------------------------------------------------->

<br clear="all"/>
<%--*** End of flight details panel ***--%>
