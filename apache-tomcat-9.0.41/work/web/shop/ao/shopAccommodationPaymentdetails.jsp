
   <div class="fh4_payment_head_tac">Payment Details</div>
   <br clear="all"/>
   <div class="container_paymentandcontainer" id="paymentDetails">
      <%----------------------------------- Payment type radio buttons -------------------------------------%>
      <c:set var="grayText" value=""/>
      <c:if test="${bookingComponent.datacashEnabled == false}">
         <c:set var="grayText" value="gray_text"/>
      </c:if>
      <div id="payment_type_header" >
         <div id="c1" class="cust_present">
            <input type="radio" id="cp" name="customerType" value="CP" onclick="CustomerTypeChangeHandler.handle(this.value);"/>
            <label  class="payment_type_header_label" for="cp"> <span class="fontstyle <c:out value='${grayText}'/>">Customer present</span></label>
         </div>
         <div id="c2" class="cust_not_present">
            <input type="radio" id="cnp" name="customerType" value="CNP" onclick="CustomerTypeChangeHandler.handle(this.value);"/>
            <label class="payment_type_header_label" for="cnp"><span class="fontstyle <c:out value='${grayText}'/>"> Customer not present </span></label>
         </div>
         <div id="c3" class="chip_pin_not_avail">
            <input type="radio" id="cpna" name="customerType" value="CPNA" onclick="CustomerTypeChangeHandler.handle(this.value);"/>
            <label class="payment_type_header_label" for="cpna"><span class="fontstyle <c:out value='${grayText}'/>"> Chip-and-pin not available</span></label>
         </div>
      </div>
      <%--------------------------------- Payment type End radio buttons -----------------------------------%>

      <%------------------------------------- No Of Transactions -------------------------------------------%>
      <div id="payment_trans_txt" class="howmanypays">How many payments would you like to make?</div>
      <div id="noOfPaymentsBlock" class="movepaymentrans">
         <select class="num_paymenttypes" id="noOfPayments" name="payment_totalTrans" onchange="NoOfPaymentsChangeHandler.handle(this.value);">
            <c:forEach begin="1" end="${bookingComponent.noOfTransactions}" varStatus="status">
               <c:out value="<option value=${status.count}>${status.count}</option>" escapeXml="false"/>
            </c:forEach>
         </select>
      </div>
      <%------------------------------------- End No Of Transactions ---------------------------------------%>
<!----------------------------------------------- Payment Contents  --------------------------------------------------------------->
   <!--Payment Contents will be added dynamically -->

   <%---------------------------------------------END Payment Section  -------------------------------------------------------- ---%>

   <%-----------------------------------------------Payment Accumulation------------------------------------------------------------%>
   <ul id="paymentAccumlation" >
      <li>
            <span >Amount Paid</span>
            <input type="text" id="amountPaid" class="paidAmount medium"/>
       </li>
      <li>
           <span >Amount still due</span>
           <input type="text" id="amountStillDue" class="amountStillDue medium"/>
       </li>
      <li>
            <span >Total Amount due</span>
            <input type="text" id="totalAmountDue" class="calculatedPayableAmount medium"/>
       </li>
   </ul>
   <%-----------------------------------------------END Payment Accumulation------------------------------------------------------------%>

      <%----------------------------------------------------------------------------------------------------%>
      <div id="paymentSelections" class="payment_maincont"></div>
      <div id="hoplaDisplay"></div>
      <div id="hoplaTransCount"></div>
      <%----------------------------------------------------------------------------------------------------%>
      <br clear="all"/>
      <%---------------------------------------------------------------------------------------------------%>

      <%---------------------------------------------------------------------------------------------------%>
   </div><%--END paymentDetails --%>
<input type="hidden" id="preTrans" name="preTrans" value="0"/>
<input  name="updateFlag" id="updateFlag" value="false" type="hidden"/>
<input type="hidden" name="total_transamt" id="total_transamt" value='' />
<input type='hidden' id='payment_pinpadTransactionCount' name='payment_pinpadTransactionCount'/>
<input type='hidden' id='payment_pinpadTransactionAmount' name='payment_pinpadTransactionAmount'/>
<input type="hidden" name="datacashEnabled" id="datacashEnabled" value="<c:out value='${bookingComponent.datacashEnabled}'/>"/>

<%-------------------------------------------------------------------------------------------------------%>