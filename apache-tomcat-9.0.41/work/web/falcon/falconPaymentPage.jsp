<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <%@ page contentType="text/html; charset=UTF-8" %>
  <%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
  <version-tag:version/>
  <title>Falcon | Payment Details</title>
 <c:set var='clientapp' value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName} '/>
 <%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         clientApp = (clientApp!=null)?clientApp.trim():clientApp;
         String lowDepositValue = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".lowDepositFeatureAvailable", "");
         pageContext.setAttribute("lowDepositFeatureAvailable", lowDepositValue, PageContext.REQUEST_SCOPE) ;
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
         String cvvEnabled = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".CV2AVS.Enabled", "false");
         pageContext.setAttribute("cvvEnabled", cvvEnabled, PageContext.REQUEST_SCOPE);
 %>
<c:choose>
<c:when test="${!lowDepositFeatureAvailable}">
<link media="screen, print" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/preLdDepositOptions_css/style.css"/>
<link media="screen, print" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/preLdDepositOptions_css/style_media.css"/>
<link media="screen" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/preLdDepositOptions_css/fcaccom.css"/>
<link media="print" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/preLdDepositOptions_css/style_print.css"/>
<link media="screen" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/preLdDepositOptions_css/integrated_payments.css"/>
<!--[if lte IE 6]><link type="text/css" rel="stylesheet" href="/cms-cps/firstchoice/falcon/preLdDepositOptions_css/style_ie.css" /><![endif]-->
</c:when>
<c:otherwise>
<link media="screen, print" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/style.css"/>
<link media="print" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/style_print.css"/>
<link media="screen" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/integrated_payments.css"/>
<!--[if lte IE 6]><link type="text/css" rel="stylesheet" href="/cms-cps/firstchoice/falcon/css/style_ie.css" /><![endif]-->
</c:otherwise>
</c:choose>

 <script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
<c:choose>
<c:when test="${!lowDepositFeatureAvailable}">
<%-- Include Payment related JS Here --%>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/prototype.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/rico.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/functions.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/preLdDepositOptions_js/functions_checkout_payment_details.js"></script>

<script type="text/javascript" src="/cms-cps/common/js/paymentPanelContent.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/paymentPanelValidation.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>

<script type="text/javascript" src="/cms-cps/firstchoice/falcon/preLdDepositOptions_js/paymentUpdation.js"></script>

<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/popup.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/messages.js"></script>
</c:when>
<c:otherwise>
<%-- Include Payment related JS Here --%>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/prototype.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/rico.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/functions.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/functions_toggle.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/functions_checkout_payment_details.js"></script>

<script type="text/javascript" src="/cms-cps/common/js/paymentPanelContent.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/paymentPanelValidation.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>

<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/paymentUpdation.js"></script>

<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/popup.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/messages.js"></script>
</c:otherwise>
</c:choose>

<link type="image/x-icon" href="/cms-cps/firstchoice/falcon/images/favicon/favicon.ico" rel="icon"/>
<link type="image/x-icon" href="/cms-cps/firstchoice/falcon/images/favicon/favicon.ico" rel="shortcut icon"/>
<%-- Modified to add ensighten tag in head section --%>
 <jsp:include page="tag.jsp"/>
</head>

  <body class="">
  <c:choose>
<c:when test="${requestScope.cookieswitch=='true'}">


<c:choose>
<c:when test="${requestScope.cookiepresent != 'true'}">
<jsp:include page="cookielegislation.jsp"/>


</c:when>
<c:otherwise>

</c:otherwise>

 </c:choose>

</c:when>
<c:otherwise>

</c:otherwise>

</c:choose>
    <%--Contains Essential configuration settings used thru out the page --%>
   <%@include file="falconConfigSettings.jspf"%>
    <div id="PageContainer">
      <div id="Page">
        <div id="PageColumn1">

        <%-- Booking Process Header --%>
        <div class="accessibility">
          <ul>
            <li><a href="#main_content">Jump to main Content</a></li>
            <li><a href="#SearchPanel">Jump to Search</a></li>
            <li><a href="#masthead">Jump to Main Menu</a></li>
            <li><a href="#Footer">Jump to Secondary Menu</a></li>
            <li><a href="#">Jump to Accessibility Statement</a></li>
          </ul>
          <p>If you are using a screen reader, we recommend that you&nbsp;<a href="#">disable JavaScript</a>.</p>
        </div><%--End of accessibility --%>

        <jsp:include page="sprocket/bookingHeader.jsp"/>

        <div id="MainContent">
          <div id="BodyWide">
            <div class="bodyPadder">

              <jsp:include page="breadCrumb.jsp"/>
              <jsp:include page="mbox.jsp"/>
              <jsp:include page="summaryPanel.jsp"/>
<c:choose>
<c:when test="${!lowDepositFeatureAvailable}">
                 <jsp:include page="preLdDepositOptions/payment.jsp"/>
</c:when>
<c:otherwise>
              <jsp:include page="payment.jsp"/>
</c:otherwise>
</c:choose>
            </div><%--End bodyPadder--%>
          </div><%--End BodyWide--%>
        </div> <%--End MainContent--%>
      </div><%--**** End of PageColumn1 div ***--%>

    </div>
  </div>
  </body>
</html>
