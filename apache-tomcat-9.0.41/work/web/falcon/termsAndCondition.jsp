<%@include file="/common/commonTagLibs.jspf"%>
<%-- TERMS AND CONDITIONS HEADER --%>
<div class="subheaderBar">
   <div class="subheaderBarSidesShadow">
       <img src="/cms-cps/firstchoice/falcon/images/icons/header-terms-and-conditions.gif" alt=""/>
       <h2>Terms and conditions</h2>
   </div>
</div>
<div class="clearer"></div>
<div class="firstColSidesShadow">
  <div class="checkoutPaymentTermsConditions">
      <input id="agreeTermAndCondition" type="checkbox" onclick="this.blur();" name="agreeTermAndCondition"/>
           <p>
                    <label for="agreeTermAndCondition">I agree to Falcon's</label>
              <img width="10" height="10" src="/cms-cps/firstchoice/falcon/images/icons/link_new_window.gif" alt="The following link opens in a new window"/>
                 <a href="JavaScript:void(popUpBookingConditionsFalcon('<c:out value="${clientUrl}"/>','<c:out
            value="${bookingComponent.accommodationSummary.accommodationInventorySystem}"/>','<c:out
            value="${bookingComponent.flightSummary.flightSupplierSystem}"/>','<c:catch><fmt:formatDate
            value="${bookingComponent.bookingData.bookingDate}" pattern="yyyy-MM-dd"/></c:catch>'));"
            class="arrow-link">
                 Terms and conditions</a>
                 <c:if test="${Irlandnorth!='true'}">
                 <label for="agreeTermAndCondition"> and I have read and understood the Arbitration Clause</label>
                 </c:if>
           </p>
        </div>

        <div class="checkoutPaymentSubmit">
           <p>
            You are about to submit your card details to make your holiday booking.
            It may take up to 30 seconds to complete your payment.
           </p>
           <c:choose>
	  	  <c:when test="${( bookingComponent.nonPaymentData['atol_flag'] != null ) &&( bookingComponent.nonPaymentData['atol_flag'] )}">
				  <c:if test="${( bookingComponent.nonPaymentData['atol_protected'] !=null ) &&( bookingComponent.nonPaymentData['atol_protected'] )}">
			  		<p>This booking is authorised under the ATOL licence of TUI UK Limited, ATOL 2524, and is protected under the ATOL scheme, as set out in the ATOL certificate to be supplied.</p>
			          </c:if>
		  </c:when>
	  </c:choose>
           
           
           <c:if test="${bookingInfo.newHoliday}">
             <img id="CheckoutPaymentDetailsBookNowButtonDisabled" src="/cms-cps/firstchoice/falcon/images/buttons/form/book-holiday-now-greyed.gif" class="checkoutPaymentSubmitButtonDisabled" alt="" />
             <div id="confirmBookNowButton">
	             <c:choose>
					<c:when test="${requestScope.disablePaymentButton == 'true'}">
	                    <input style="cursor:default;" disabled="disabled" type="image" id="CheckoutPaymentDetailsBookNowButton" src="/cms-cps/firstchoice/falcon/images/buttons/form/book-holiday-now-greyed.gif"  alt="Book this holiday now" title="Book this holiday now"/>
		            </c:when>
					<c:otherwise>
						<input type="image" id="CheckoutPaymentDetailsBookNowButton" src="/cms-cps/firstchoice/falcon/images/buttons/form/book-holiday-now.gif"  alt="Book this holiday now" title="Book this holiday now"/>
					</c:otherwise>
				 </c:choose> 
             </div>
           </c:if>
           <div class="clearer"></div>
        </div>
           </div><%-- END firstColSidesShadow --%>
        <div class="firstColBottomShadow"></div>