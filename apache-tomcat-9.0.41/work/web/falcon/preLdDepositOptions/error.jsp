<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
   
 <c:choose>
    <c:when test="${not empty sessionScope=='true'}">
    <title>Falcon | Error</title>
   </c:when>
   <c:otherwise>
   <title>Falcon | Session Timed Out</title>
   </c:otherwise>
   </c:choose>
   

<link media="screen, print" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/style.css"/>
<link media="screen, print" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/style_media.css"/>
<link media="screen" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/fcaccom.css"/>
<link media="print" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/style_print.css"/>
<link media="screen" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/integrated_payments.css"/>
<!--[if lte IE 6]><link type="text/css" rel="stylesheet" href="/cms-cps/firstchoice/falcon/css/style_ie.css" /><![endif]-->

<meta name="errorPageText" content="session_timeout" />


<link type="image/x-icon" href="/cms-cps/firstchoice/falcon/images/favicon/favicon.ico" rel="icon"/>
<link type="image/x-icon" href="/cms-cps/firstchoice/falcon/images/favicon/favicon.ico" rel="shortcut icon"/>
</head>

<body class="systemDown">
   <div id="PageContainer">
      <div id="Page">
         <div id="PageColumn1">
            <div id="Header">
              <!-- #Header -->
                <div id="headerContentWrapper">
                <div id="masthead">
                     <a href="https://www.falconholidays.ie"><img src="/cms-cps/firstchoice/falcon/images/header/falcon-header-logo.gif" alt="Falcon" width="139" height="34" /></a>

                  </div>
              </div>
             </div>
            <div id="MainContent">
               <div id="BodyWide">
                  <div class="bodyPadder">
                     <div id="PrimaryColumn">
                     </div>


<div class="accessibility">
   <ul>
      <li><a href="#main_content">Jump to main Content</a></li>
      <li><a href="#SearchPanel">Jump to Search</a></li>
      <li><a href="#masthead">Jump to Main Menu</a></li>
      <li><a href="#Footer">Jump to Secondary Menu</a></li>
      <li><a href="#">Jump to Accessibility Statement</a></li>

   </ul>
   <p>If you are using a screen reader, we recommend that you&nbsp;<a href="#">disable JavaScript</a>.</p>
</div>

<div id="BodyWide">
   <div class="bodyPadder">
     <c:choose>
   
    <%--Tech Difficulties Error Message --%>
    <c:when test="${not empty sessionScope=='true'}">
      <h1>Sorry, we are currently experiencing technical difficulties</h1>
      <div class="standardContent ">
        <div class="imgBlock">
            <img src="/cms-cps/firstchoice/falcon/images/error/content-pic.jpg" class="left" alt="" />
        </div>
        <div class="contentBlock">
            <p>For further assistance please call us on 1850 94 61 64 (Republic of Ireland) or 028 90 389 387 (Northern Ireland). Mon to Fri (8am - 10pm), Sat (9am - 5.30pm) and Sun (10am - 6pm). Alternatively, please search again later.</p>
            <p><a href="https://www.falconholidays.ie" class="arrow-link">Return to homepage</a></p>
        </div>
        <div class="clearer"></div>
      </div>
     </c:when>
   <%--End Tech Difficulties Error Message --%>
   
   <%--Session Time out Error Message --%>
   <c:otherwise>
      <h1>Sorry, your session has timed out</h1>
      <div class="standardContent">
        <div class="imgBlock">
          <img src="/cms-cps/firstchoice/falcon/images/error/content-pic.jpg" class="left" alt="" />
        </div>
        <div class="contentBlock">
           <p>Due to inactivity your session has timed out, please click homepage to begin another search.</p>
           <p><a href="https://www.falconholidays.ie" class="arrow-link">Return to homepage</a>.</p>
        </div>
        <div class="clearer"></div>
    </div>
   </c:otherwise>
   <%--End of Session Time out Error Message --%>
 
  </c:choose>    
  </div> <!--end  bodyPadder-->
</div> <!--end  BodyWide-->
<script type="text/javascript">
                var sitestatpagename = "tech-difficulties.page";
  var fch_location = "";
  var fch_resort = "";
  var fch_accommodation = "";
</script>
                  </div>
               </div>
            </div>
                  <div id="Footer">
         <a name="footer" id="footer"></a>
         <div id="atol_abta" class="systemDown"><a href="#" title="ABTA"><img src="/cms-cps/firstchoice/falcon/images/footer/abta_logo.gif" width="32" height="41" alt ="ABTA logo" class="foot_img_1" /></a>
            <a href="#" title="ATOL"><img src="/cms-cps/firstchoice/falcon/images/footer/atol_logo.gif" width="41" height="41" alt ="ATOL logo" class="foot_img_2"  /></a>            
            
            </div> <!-- end atol_abta-->
      </div> <!-- end footer-->

         </div>
         <div id="PageColumn2">
       </div>
      </div>
   </div>
   <jsp:include page="/falcon/tag.jsp"/>
 </body>

</html>

