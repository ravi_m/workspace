<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
 <%@ page contentType="text/html; charset=UTF-8" %>
  <%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
  <version-tag:version/>

  <title>Falcon | Payment Details</title>
<link media="screen, print" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/style.css"/>
<link media="screen, print" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/style_media.css"/>
<link media="screen" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/fcaccom.css"/>
<link media="print" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/style_print.css"/>
<link media="screen" type="text/css" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/falcon/css/integrated_payments.css"/>
<!--[if lte IE 6]><link type="text/css" rel="stylesheet" href="/cms-cps/firstchoice/falcon/css/style_ie.css" /><![endif]-->


<!-- Include Payment related JS Here -->
<script type="text/javascript" src="/cms-cps/common/js/paymentPanelContent.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/paymentPanelValidation.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>

<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/paymentUpdation.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/popup.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/functions_checkout_payment_details.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/functions.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/messages.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/prototype.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/rico.js"></script>
<script type="text/javascript" src="/cms-cps/firstchoice/falcon/js/falcon_functions.js"></script>


<link type="image/x-icon" href="/cms-cps/firstchoice/falcon/images/favicon/favicon.ico" rel="icon"/>
<link type="image/x-icon" href="/cms-cps/firstchoice/falcon/images/favicon/favicon.ico" rel="shortcut icon"/>

<%--***********Essential JSP settings. to be used through out the page ***********--%>
<c:set target="${bookingInfo}" property="cardChargePercent" value="${null}" />
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />

 <c:set var="clientUrl" value='${bookingComponent.clientDomainURL}' scope="request"/>

<c:choose>
 <c:when test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
      <c:set var="currency" value="&pound;" scope="request"/>
     <c:set var="Irlandnorth" value="true" scope="request" />
 </c:when>
 <c:otherwise>
     <c:set var="currency" value="&euro;" scope="request"/>
     <c:set var="Irlandnorth" value="false" scope="request"/>
 </c:otherwise>
 </c:choose>

<c:if test="${fn:contains(header['referer'], 'falcon') || fn:contains(header['host'], ' falcon')}">
    <c:set var="falconSession"  value="true" scope="request"/>
</c:if>

<script type="text/javascript">
 var clientApp = "<c:out value='${bookingComponent.clientApplication.clientApplicationName} '/>";
 var Irlandnorth = "<c:out value='${Irlandnorth}'/>";
 var clientDomainURL = "<c:out value='${clientUrl}'/>";
 var isPromotionalcodeApplied = "<c:out value="${bookingComponent.nonPaymentData['isPromoCodeApplied']}"/>";
 var themePath = '';
 var promoCodeValidated = false;
 var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
 var token = "<c:out value='${param.token}' />" ;
 var isNewHoliday = '<c:out value="${bookingInfo.newHoliday}"/>';
 var errorFields = "<c:out value="${bookingInfo.errorFields}"/>";
 var paymenttypeoptions = new Array();
</script>
</head>

<body class="">
<input type='hidden' id='currencyText'  value='<c:out value="${currency}" escapeXml="false"/>'/>
<%--*********Essential JSP settings. to be used through out the page **************--%>
   <div id="PageContainer">
      <div id="Page">
         <div id="PageColumn1">

            <!-- Booking Process Header -->
            <div class="accessibility">
             <ul>
               <li><a href="#main_content">Jump to main Content</a></li>
               <li><a href="#SearchPanel">Jump to Search</a></li>
               <li><a href="#masthead">Jump to Main Menu</a></li>
               <li><a href="#Footer">Jump to Secondary Menu</a></li>
              <li><a href="#">Jump to Accessibility Statement</a></li>
           </ul>
          <p>If you are using a screen reader, we recommend that you&nbsp;<a href="#">disable JavaScript</a>.</p>
        </div><!--End of accessibility -->

        <jsp:include page="sprocket/bookingHeader.jsp"/>


            <div id="MainContent">
               <div id="BodyWide">
                  <div class="bodyPadder">
                       <jsp:include page="breadCrumb.jsp"/>
                       <!--Summary panel -->
                          <jsp:include page="summaryPanel.jsp"/>
                       <!---End of summary panel -->
                       <!--Payment panel -->
                           <jsp:include page="payment.jsp"/>
                      <!--End of payment panel -->
         </div><!--End bodyPadder-->
      </div><!--End BodyWide-->
   </div> <!--End MainContent-->

</body>
</html>
