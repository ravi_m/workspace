<div class="overlayWrapper">
  <div class="headerBlock">
    <h4><span class="padder"><%-- InstanceBeginEditable name="overlay-heading" --%>Card Security Code<%-- InstanceEndEditable --%></span></h4>
  </div>
  <div class="overlayPadder">
    <div class="contentBlock">

	  	<%-- InstanceBeginEditable name="overlay-content" --%><p class="marginBefore">On the reverse side of your card, at the top of the signature strip, there should be either your full card number or just the last four digits, followed by a unique three-digit number &ndash; this is your card security code. As an added protection against card fraud, we ask for this number when you book one of our holidays online</p>
	  	<p class="marginBefore"><img src="/cms-cps/firstchoice/falcon/images/bookings/credit-card-cvc-illustration.jpg" width="250" height="158" /></p>
  	  <%-- InstanceEndEditable --%>

      <div class="clearer"></div>
    </div>
    <%--end contentBlock--%>
  </div>
  <%--end overlayPadder--%>
</div>
<%--end overlayWrapper--%>

 