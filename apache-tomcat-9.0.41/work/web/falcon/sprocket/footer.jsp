<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />

<c:choose>
 <c:when test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
      <c:set var="currency" value="&pound;" scope="request"/>
     <c:set var="Irlandnorth" value="true" scope="request" />
 </c:when>
 <c:otherwise>
     <c:set var="currency" value="&euro;" scope="request"/>
     <c:set var="Irlandnorth" value="false" scope="request"/>
 </c:otherwise>
</c:choose>

<%--****************Footer*********************--%>
<c:choose>
 <c:when test="${Irlandnorth=='true'}">
     <jsp:include page="/falcon/sprocket/bookingFooterNorth.jsp"/>
 </c:when>
 <c:otherwise>
    <jsp:include page="/falcon/sprocket/bookingFooterSouth.jsp"/>
 </c:otherwise>
 </c:choose>
<%--****************End Footer*********************--%>