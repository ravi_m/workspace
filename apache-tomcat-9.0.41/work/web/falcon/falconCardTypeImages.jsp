<%@include file="/common/commonTagLibs.jspf"%>

 <c:choose>
      <c:when test="${Irlandnorth=='true'}">
         <img id="imgVisaCredit" src="/cms-cps/firstchoice/falcon/images/checkout/visa_card.gif" alt="We accept Visa" title="We accept Visa" />
         <img id="imgVisaDebit" src="/cms-cps/firstchoice/falcon/images/checkout/delta_card.gif" alt="We accept Delta" title="We accept Delta" />
         <img id="imgVisaElectron" src="/cms-cps/firstchoice/falcon/images/checkout/visaElectron_card.gif" alt="We accept Visa Electron" title="We accept Visa Electron"/>
         <img id="imgMasterCard" src="/cms-cps/firstchoice/falcon/images/checkout/mastercard_card.gif" alt="We accept MasterCard" title="We accept MasterCard"/>
         <img id="imgMaestro" src="/cms-cps/firstchoice/falcon/images/checkout/maestro_card.gif" alt="We accept Maestro" title="We accept Maestro"/>
      </c:when>
       <c:otherwise>
         <img id="imgVisaCredit" src="/cms-cps/firstchoice/falcon/images/checkout/visa_card.gif" alt="We accept Visa" title="We accept Visa"/>
         <img id="imgMasterCard" src="/cms-cps/firstchoice/falcon/images/checkout/mastercard_card.gif" alt="We accept MasterCard" title="We accept MasterCard"/>
         <img id="imgLaser" src="/cms-cps/firstchoice/falcon/images/checkout/laser_card.gif" alt="We accept Laser" title="We accept Laser"/>
       </c:otherwise>
    </c:choose>
