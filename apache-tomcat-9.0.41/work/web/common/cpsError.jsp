<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
       <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
       <version-tag:version/>
       <title>TUI Holidays | Discover Your Smile</title>
       <%-- Include  CSS  here --%>
       <link rel="stylesheet" type="text/css" href="/cms-cps/common/css/headfoot.css" />
       <link rel="stylesheet"  type="text/css" href="/cms-cps/common/css/404.css" />
	   <link rel="stylesheet" href="/cms-cps/tuith/css/base.css">
	   <link rel="stylesheet" href="/cms-cps/tuith/css/bf.css">
      <%-- End of css files --%>
	  <link rel="shortcut icon" type="image/x-icon" media="all" href="https://www.tui.co.uk/favicon.png" />
		<link href="https://www.tui.co.uk/favicon.png" rel="icon" type="image/png">
		<!-- IE10 support, favicon.ico added -->
		<link href="https://www.tui.co.uk/favicon.ico" rel="shortcut icon" type="image/x-icon">

   </head>
	<div style="
		width: 1048px;
		margin: 0 auto;
		background: #fff;
		-webkit-box-shadow: 0 0 15px rgba(0,0,0,.35);
		box-shadow: 0 0 15px rgba(0,0,0,.35);
	">
	<div id="header" style="height: 120px;margin:0 auto;min-width: 283px;max-width: 1140px;height: 100%;background-color: #70cbf4 !important;">
	<a href="http://www.tui.co.uk"> <img src="/cms-cps/tuith/images/logo/TUI-Logo.svg" alt="TUI.co.uk" name="header_thomsonlogo" style="width: 116px; height: 88px; margin-left: 0px;padding: 0 0 9px 19px;" border="0"></a>
	</div>		
	<div id="bodysection" style="overflow: hidden;">
		   <div id="">
				<div class="span error-page" style=" padding: 0 0 24px 24px;">
				<h1>We're really sorry, we're having some technical problems.</h1>
				</div>
				<div class="fl message" style="float: left;padding: 0 0 24px 24px;">

					<p>
						You can go back to viewing<a href="https://www.tui.co.uk" class="history-reload" data-di-id="di-id-c3c0b4d7-ebbae1c6">&nbsp;holidays matching your search</a>
					</p>

					<p>or give us a call on&nbsp;0203 636 1931</p>
				</div>
			<div class="span clearfix error-page">
				
				<img class="fr error-image" style="float: right;" src="/cms-cps/common/images/error-tech-diff.png" alt="Technical Difficulties" width="460" height="276">
				</div>

		   </div>

	   </div>
<div id="footer" style="width:100%;">
				<div id="call-us" class="hide">
					<div class="content-width">
						<i class="caret call white b"></i><h2 class="d-blue">BOOK NOW! <span>Call us on: <a href="tel:0203 636 1931">0203 636 1931</a></span></h2>
					</div>
				</div>
				<div id="search" class="b">
					<div class="content-width">
						
					</div>
				</div>
				<div id="group" class="b thomson">
					<div class="content-width">
						<div class="copy">
							<!-- <span id="world-of-tui"><img alt="World of TUI" src="/cms-cps/tuith/images/logo/wtui.png"></span> -->
							<!-- <p>We're part of TUI Group - one of the world's leading travel companies. And all of our holidays are designed to help you Discover Your Smile.</p> -->
						<p>Just so you know, Thomson is now called TUI and we're part of TUI Group - the world's leading travel company. All of our holidays are designed to help you 'Discover your smile'.</p>
						</div>
						<div class="logos">
							<a href="http://abta.com/go-travel/before-you-travel/find-a-member" id="logo-abta" title="ABTA - The Travel Association" target="_blank"></a>
							<a href="http://www.caa.co.uk/application.aspx?catid=490&amp;pagetype=65&amp;appid=2&amp;mode=detailnosummary&amp;atolnumber=2524" id="logo-atol" title="ATOL Protected"></a>
						</div>
					</div>
				</div>
				<div id="terms">

					<div class="content-width">
					<p class="title"><a href=" https://www.tui.co.uk/destinations/info/about-us">More from TUI</a></p>
						<p>
							
							<a target="_blank" href="https://www.tui.co.uk/destinations/info/my-tui-app">About TUI</a>
							<a target="_blank" href="https://www.tui.co.uk/destinations/info/my-tui-app">MyTUI app</a>
							<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/statement-on-cookies.html">Cookies policy</a>

							<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html">Privacy Policy</a>
							<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html">Terms &amp; conditions</a>
							
								
								
									<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">Ways to Pay</a>
								
							
							<a target="_blank" href="http://communicationcentre.thomson.co.uk/">Media Centre</a>
							<a target="_blank" href="http://www.tuijobsuk.co.uk/">Travel Jobs</a>
							<a target="_blank" href="http://www.thomson.co.uk/affiliates.html">Affiliates</a>

							
							<a target="_blank" href=" https://blog.tui.co.uk/">TUI Blog</a>
							<a target="_blank" href="https://play.google.com/store/apps/details?id=com.thomson.mythomson">Google Play Store</a>
							<a target="_blank" href="https://appsto.re/gb/NubyM.i">App Store for Ios</a>
                       
                        <a target="_blank" href="https://www.tui.co.uk/destinations/info/tui-credit-card">TUI Credit Card</a>
						<a target="_blank" href="http://www.firstchoice.co.uk">First Choice</a>
						<a target="_blank" href="http://www.tui.co.uk/brochures">Holiday Brochures
						</a>
						</p>
					</div>
				</div>
				<div class="know-before"><div class="content-width"><div class="know-header-section"><img class="know-before-logo" src="https://digital.thomson.co.uk/fco.png" alt="World of TUI"><h3 class="know-before-heading">Travel Aware – Staying Safe and Healthy Abroad</h3></div><p>The Foreign &amp; Commonwealth Office and National Travel Health Network and Centre have up-to-date advice on staying safe and healthy abroad.</p><p>For the latest travel advice from the Foreign &amp; Commonwealth Office including security and local laws, plus passport and visa information check <a href="http://www.gov.uk/travelaware" target="_blank" class="stay-safe">www.gov.uk/travelaware</a> and follow <a href="https://twitter.com/FCOtravel" target="_blank" class="stay-safe">@FCOtravel</a> and <a href="https://www.facebook.com/fcotravel/" target="_blank" class="stay-safe">Facebook.com/FCOtravel</a></p><p>More information is available by checking <a href="https://www.tui.co.uk/destinations/info/travel-aware" target="_blank" class="stay-safe">/destinations/info/travel-aware</a></p><p> Keep informed of current travel health news by visiting  <a href="http://www.travelhealthpro.org.uk" target="_blank" class="stay-safe">www.travelhealthpro.org.uk</a></p><p> The advice can change so check regularly for updates.</p></div></div>
			</div>
    <script>
	  var tui = {};
	  tui.analytics = {};
	  tui.analytics.page = {};
	  tui.analytics.page.pageUid = "technicaldifficultiespage";
	 </script>
   </body>
   </html>