<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>EMV Error</title>
<style>
.tech-error {
	background: #d40e14;
	color: white;
	font-size: 18px;
	font-family: arial;
	margin: 1rem;
	padding: 2rem;
}
</style>
</head>
<body>
	<c:choose>
		<c:when test="${not empty errorMessage}">
			<div class='tech-error'>${errorMessage}<p style='display: none'>ERROR_MESSAGE</p>
			</div>
		</c:when>
	</c:choose>
</body>
</html>