<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%
String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.mobile.fc" , "");
pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);
String registeredAddress = ConfReader.getConfEntry("tuiuk.registeredAddress" , "");
pageContext.setAttribute("registeredAddress", registeredAddress, PageContext.REQUEST_SCOPE);
String firstchoiceBrochure = ConfReader.getConfEntry("firstchoice.brochure.url" , "");
pageContext.setAttribute("firstchoiceBrochure", firstchoiceBrochure, PageContext.REQUEST_SCOPE);

%>

<div id="footer">
				<div id="call-us" class="hide">
					<div class="content-width">
						<i class="caret call white b"></i><h2 class="d-blue">BOOK NOW! <span>Call us on: <a href="tel:0203 636 1931">0203 636 1931</a></span></h2>
					</div>
				</div>
				<div id="search" class="b">
					<div class="content-width">
						<a href="#page" id="backtotop"><span>To top </span><i class="caret back-to-top white"></i></a>
					</div>
				</div>
				<div id="group" class="b firstchoice">
					<div class="content-width">
						<div class="copy">
							<p>We're part of TUI Group - one of the world's leading travel companies. And all of our holidays are designed to help you Discover Your Smile.</p>
						    <p><%= registeredAddress %></p>
						</div>
						<div class="logos">

							<a href="http://abta.com/go-travel/before-you-travel/find-a-member" id="logo-abta" title="ABTA - The Travel Association"></a>
							<a href="http://www.caa.co.uk/application.aspx?catid=490&pagetype=65&appid=2&mode=detailnosummary&atolnumber=2524" id="logo-atol" title="ATOL Protected"></a>
						</div>
					</div>
				</div>
				<div id="terms">
					<div class="content-width">
						<p>
							<a href="http://www.firstchoice.co.uk/about-us">About First Choice</a>
							<a href="http://www.firstchoice.co.uk/myapp/">MyFirstChoice app</a>
							<a href="http://www.firstchoice.co.uk/our-policies/statement-on-cookies">Cookies policy</a>

							<a href="http://www.firstchoice.co.uk/our-policies/privacy-policy">Privacy Policy</a>
							<a href="http://www.firstchoice.co.uk/our-policies/terms-of-use">Terms &amp; conditions</a>
							<c:choose>
								<c:when test="${applyCreditCardSurcharge eq 'true'}">
							<a href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges">Credit card fees</a>
</c:when>
<c:otherwise>
<a href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges">Ways to Pay</a>
</c:otherwise>
</c:choose>
<!-- 							<a href="http://www.thomson.co.uk/">Accessibility</a> -->
							<a class="desktopLinkHide" href="http://www.firstchoice.co.uk/">Desktop site</a>
							<a href="http://communicationcentre.firstchoice.co.uk/">Media Centre</a>

							<a href="http://www.tuitraveljobs.co.uk/">Travel Jobs</a>
							<a href="http://www.firstchoice.co.uk/affiliates.html">Affiliates</a>
							<a href="http://www.firstchoice.co.uk/blog/">First Choice Blog</a>

							<a target="_blank" href="http://www.tuigroup.com/en-en"><jsp:useBean id='CurrentDate12' class='java.util.Date'/>
         	<fmt:formatDate var='currentYear' value='${CurrentDate12}' pattern='yyyy'/>
			&copy; <c:out value="${currentYear}" />  TUI Group</a>
			<a target="_blank"  href="${firstchoiceBrochure}">Holiday Brochures
						</a>
						</p>
					</div>
				</div>
				<%=staySafeAbroad %>
			</div>
			<div id="disclaimer">
				<div class="content-width disclaim two-columns">

					<p>Some of the flights and flight-inclusive holidays on this website are financially protected by the ATOL scheme.  But ATOL protection does not apply to all holiday and travel services listed on this website. This website will provide you with information on the protection that applies in the case of each holiday and travel service offered before you make your booking.  If you do not receive an ATOL Certificate then the booking will not be ATOL protected. If you do receive an ATOL Certificate but all the parts of your trip are not listed on it, those parts will not be ATOL protected. Please see our booking conditions for information, or for more information about financial protection and the ATOL Certificate go to:<a href="http://www.caa.co.uk/home/" target="_blank">www.caa.co.uk</a></p>

				</div>
			</div>
			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip,freeChildPlaceCount,ensightenUrl,bootstrapUrl,TUIHeaderSwitch,tuiAnalyticsJson,csrfToken</c:set>
			<script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
				 tui.analytics.page.pageUid = "mobilePaymentDetailsPage";
				 <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
					 <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
					 tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
				    </c:if>
			    </c:forEach>
				</script>				<script>
			
				window.dataLayer = window.dataLayer || [];
				window.dataLayer.push(${bookingInfo.bookingComponent.nonPaymentData['tuiAnalyticsJson']});

			
				</script>