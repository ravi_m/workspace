<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:formatNumber value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}" var="totalCostingLine" type="number" pattern="####" maxFractionDigits="2" minFractionDigits="2"/>
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page"/>
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:set var="multiCentre" value="${bookingComponent.multiCentre}"/>
<c:if test="${not empty bookingComponent.breadCrumbTrail}">
<c:set var="searchUrl" value="${bookingComponent.breadCrumbTrail['HUBSUMMARY']}"/>
<c:set var="passengersUrl" value="${bookingComponent.breadCrumbTrail['PASSENGERS']}"/>
<c:choose>
		<c:when test="${multiCentre eq true}">
		  <c:set var="optionsUrl" value="${bookingComponent.breadCrumbTrail['CENTRESELECTION']}"/>
		</c:when>
		   <c:otherwise>
	     	<c:set var="optionsUrl" value="${bookingComponent.breadCrumbTrail['HOTEL']}"/>
		</c:otherwise>
</c:choose>
</c:if>

<c:choose>
   <c:when test="${bookingComponent.nonPaymentData['TUIHeaderSwitch'] eq 'ON'}">
      <c:set value="true" var="tuiLogo" />
   </c:when>
   <c:otherwise>
      <c:set value="false" var="tuiLogo" />
   </c:otherwise>
</c:choose>

<div id="book-flow-header">
				<div class="content-width">
					<div class="logo firstchoice thomsonX falconX"><a href="${bookingComponent.clientURLLinks.homePageURL}"></a></div>

					<div class="summary-panel-trigger b abs bg-tui-sand black">
						<i class="caret fly-out"></i>&pound;<c:out value="${totalcost[0]}."/><span class="pennys"><c:out value="${totalcost[1]}"/></span>

					</div>
				</div>
			</div>
			<div id="book-flow-progress">
				<div class="content-width">
					<div class="scroll uppercase" id="breadcrumb" data-scroll-dw="true" data-scroll-options='{"scrollX": true, "scrollY": false, "keyBindings": true, "mouseWheel": true}'>
						<ul class="c">
		<c:choose>
			<c:when test="${multiCentre eq true}">
					<li class="back"><a href="<c:out value='${optionsUrl}'/>"><span class="rel b">1</span> HOLIDAY</a></li>
					<li class="back"><a href="<c:out value='${searchUrl}'/>"><span class="rel b">2</span> PERSONALISE</a></li>
			</c:when>
			<c:otherwise>
					<li class="back"><a href="<c:out value='${optionsUrl}'/>"><span class="rel b">1</span> HOTEL</a></li>
					<li class="back"><a href="<c:out value='${searchUrl}'/>"><span class="rel b">2</span> PERSONALISE</a></li>
				</c:otherwise>
		</c:choose>

							<li class="back"><a href="<c:out value='${passengersUrl}'/>"><span class="rel b">3</span> PASSENGER</a></li>
							<li class="active"><span class="rel b">4</span> Payment</li>
						</ul>
					</div>
				</div>
			</div>