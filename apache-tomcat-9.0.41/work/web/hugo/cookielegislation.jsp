	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%
    String tandcLink=com.tui.uk.config.ConfReader.getConfEntry("HUGOBYO.TermsofUse", "#");
String privacyandpolicyLink=com.tui.uk.config.ConfReader.getConfEntry("HUGOBYO.Privacypolicy", "#");
String stmtlink=com.tui.uk.config.ConfReader.getConfEntry("HUGOBYO.Statement", "#");
%>
   <link rel="stylesheet" href="/cms-cps/firstchoice/hugo/css/cookielegislation.css" type="text/css"   />


<script type="text/javascript">


function cookiePopup(popURL,popW,popH,attr){
  if (!popH) { popH = 450 }
  if (!popW) { popW = 600 }
  var winLeft = (screen.width-popW)/2;
  var winTop = (screen.height-popH-30)/2;
  var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
  popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
  popupWin.close()


     popupWin=window.open(popURL,"popupWindow",winProp);


  popupWin.window.focus()
}


function pnlReadOurTerms(){
var tomcatInstance= "<c:out value='${param.tomcat}' />";
	jQuery(document).ready(function(){
			jQuery.ajax({
			  type: "POST",
			 url: "<%=request.getContextPath()%>/CookieLegislationServlet?b=<c:out value='${param.b}' />&tomcat="+tomcatInstance,
				  success: function(response){
jQuery('.cookie_bg').slideUp('slow');
			  }
			});
		});



	}

function lnkOk(){
	document.write('<a href="javascript:pnlReadOurTerms();"><div class="ok_button"><span>OK</span></div></a>');
}
</script>



<div id="hugoCookieLegislation" class="cookiecontainer">
	<div class="cookie_bg" style="z-index:1000;">
	<div class="cookie_bg_inner">
		<form action="<%=request.getContextPath()%>/CookieLegislationServlet?brand=HUGOBYO&b=<c:out value='${param.b}' />&tomcat=<c:out value='${param.tomcat}' />" method="post">
			<div><strong>Please read our terms of use</strong></div>
			<div class="w98 fLeft txt90">
				<div class="txtSlideContent">
			By clicking OK or continuing to use our website, you consent to our use of cookies and our <a id="pnlWebsiteTermsLnk" href="<%=tandcLink%>"  target="_blank" onclick="cookiePopup('<%=tandcLink%>',465,450,'scrollbars=yes,resizable=yes'); return false;" class="lnkContent" >Website Terms and Conditions</a>, <a href="<%=privacyandpolicyLink%>"  target="_blank"onclick="cookiePopup('<%=privacyandpolicyLink%>',465,450,'scrollbars=yes,resizable=yes');  return false;" class="lnkContent">Privacy Policy</a> and <a href="<%=stmtlink%>"    target="_blank" onclick="cookiePopup('<%=stmtlink%>',465,450,'scrollbars=yes,resizable=yes');  return false;" class="lnkContent">Statement on Cookies</a>. Please leave our website if you don't agree.

			<script>
				 lnkOk()
			</script>
				</div>
			<noscript>
				<button class="ok_button">
					<span>OK</span>
				</button>
				<input type="hidden" value="true" name="hiddennonjsflag"/>
			</noscript>
			</div>

		</form>
	</div>
</div>



	<div class="popupBottomShadow">
    	<div class="popupBottomDropBlock4"></div>
  	</div>
</div>


