<%@include file="/common/commonTagLibs.jspf"%>
<!doctype html>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request"/>
<!--[if lt IE 7 ]> <html lang="en" id="firstchoice" class="ie6 "> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" id="firstchoice" class="ie7 "> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" id="firstchoice" class="ie8 "> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" id="firstchoice" class="ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="" id="firstchoice"> <!--<![endif]-->
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
      <version-tag:version/>
      <title>First Choice | Payment Details</title>
		<!--[if IE 7 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /><![endif]-->
		<!--[if IE 8 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /><![endif]-->
		<!--[if IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" /><![endif]-->
		<!--[if (gt IE 9)|!(IE)]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><!--<![endif]-->

		
			<!-- HTML5 shim, for IE6-8 support of HTML elements -->
			<!--[if lt IE 9]>
				<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->

      <c:set var='clientapp' value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName} '/>
      <%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         clientApp = (clientApp!=null)?clientApp.trim():clientApp;
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
         String cvvEnabled = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".CV2AVS.Enabled", "false");
         pageContext.setAttribute("cvvEnabled", cvvEnabled, PageContext.REQUEST_SCOPE);
         String isExcursion =
            com.tui.uk.config.ConfReader.getConfEntry(clientApp+".summaryPanelWithExcursionTickets", "false");
         pageContext.setAttribute("isExcursion", isExcursion, PageContext.REQUEST_SCOPE);
      %>
      <link rel="stylesheet" type="text/css" href="/cms-cps/firstchoice/hugo/css/style.css"/>
	  <link rel="stylesheet" type="text/css" href="/cms-cps/firstchoice/hugo/css/common/crossover.css"/>
      <link rel="stylesheet" href="/cms-cps/firstchoice/hugo/css/integrated_payments.css" type="text/css"   />
	  <link media="screen, print" rel="stylesheet" title="Style" href="/cms-cps/firstchoice/hugo/css/iScape.css"/>

      <link type="image/x-icon" href="/cms-cps/firstchoice/hugo/images/fevicon/favicon.ico" rel="icon"/>
      <link type="image/x-icon" href="/cms-cps/firstchoice/hugo/images/fevicon/favicon.ico" rel="shortcut icon"/>
      <%-- Include Payment related JS Here --%>
      <script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/functions_toggle.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/paymentPanelContent.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/paymentPanelValidation.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
      <script type="text/javascript" src="/cms-cps/firstchoice/hugo/js/prototype.js"></script>
      <script type="text/javascript" src="/cms-cps/firstchoice/hugo/js/rico.js"></script>
      <script type="text/javascript" src="/cms-cps/firstchoice/hugo/js/functions.js"></script>
      <script type="text/javascript" src="/cms-cps/firstchoice/hugo/js/functions_checkout_payment_details.js"></script>
      <script type="text/javascript" src="/cms-cps/firstchoice/hugo/js/paymentUpdation.js"></script>
      <script type="text/javascript" src="/cms-cps/firstchoice/hugo/js/popup.js"></script>
      <script type="text/javascript" src="/cms-cps/firstchoice/hugo/js/messages.js"></script>

      <%-- End of JS files --%>
	  <%-- ensighten starts --%>
      <%@include file="tag.jsp"%>
	  <%-- ensighten ends --%>
   </head>
   <body>
	  <!-- Start of DoubleClick Floodlight Tag: Please do not remove
           Activity name of this tag: First Choice Payment Page
           URL of the webpage where the tag is expected to be placed: http://www.firstchoice.co.uk
           This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
           Creation Date: 05/05/2010 -->
      <script type="text/javascript">
         var axel = Math.random() + "";
         var a = axel * 10000000000000;
         document.write('<iframe src="https://fls.doubleclick.net/activityi;src=2205258;type=first133;cat=first492;ord=1;num=' + a + '?" width="1" height="1" frameborder="0"></iframe>');
      </script>
      <noscript>
         <iframe src="https://fls.doubleclick.net/activityi;src=2205258;type=first133;cat=first492;ord=1;num=1?" width="1" height="1" frameborder="0"></iframe>
      </noscript>

      <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
      <%--Contains Essential configuration settings used thru out the page --%>
      <%@include file="hugoConfigSettings.jspf"%>


		<div id="wrapperShadow">
			<div id="wrapper">


				  <div id="PageContainer">
					 <div id="Page">
						<div id="PageColumn1">
						   <div class="accessibility"></div>
						   <%@include file="bookingHeader.jspf"%>
						   <div class="clearer"></div>
						   <div class="mboxDefault">
							</div>
						   <script type="text/javascript">
							 mboxCreate("F_PromoStrip");
						  </script>


						   <div id="MainContent">

							  <div id="BodyWide">
								 <div class="bodyPadder">
									<%@include file="breadCrumb.jspf"%>
									<jsp:include  page="summaryPanel.jsp"/>
									<jsp:include page="payment.jsp"/>
								 </div><%--**** End of bodyPadder div ***--%>
							  </div><%--**** End of BodyWide div ***--%>
						   </div><%--**** End of MainContent div ***--%>
						</div><%--**** End of PageColumn1 div ***--%>
					   <div id="PageColumn2"></div>
					 </div>
				  </div>


			</div> <!-- #wrapper -->
		</div><!-- #wrapperShadow -->
		<jsp:include page="sprocket/bookingFooterHTML5.jsp"/>

   </body>
</html>