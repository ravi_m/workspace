<%@include file="/common/commonTagLibs.jspf"%>

<div id="CheckoutPaymentDetails" class="layoutEBodyColumn">
  <a name="main_content" id="main_content"></a>
    <h1>Payment Details</h1>

    <%-- PRINT-FRIENDLY ICON
     <div id="pageTools">
         <%@include file="printerFriendlyLink.jspf" %>
         <div class="clearer"></div>
        </div> --%>

<%--****************************************************************************************************************************--%>
    <form id="CheckoutPaymentDetailsForm" method="post"
       action="/cps/processPayment?token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />"
           onsubmit="updateEssentialFields();return bookNowButtonClicked();" >

       <%@include file="paymentDetails.jsp"%>
       <%@include file="termsAndCondition.jsp" %>

    </form>
    <c:set var="blinkyId" scope="page" value="blinky3digits"/>
    <c:set var="sprocketUrlInclude" scope="page">
          sprocket/payment-card-security-standard.jsp
    </c:set>
    <c:set var="sprocketCacheContent" scope="page" value="true"/>
    <%@ include file="genericSprocketBlinky.jspf"%>
    <c:set var="blinkyId" scope="page" value="blinky4digits"/>
    <c:set var="sprocketUrlInclude" scope="page">
       sprocket/payment-card-security-amex.jsp
    </c:set>
    <c:set var="sprocketCacheContent" scope="page" value="true"/>
       <%@ include file="genericSprocketBlinky.jspf"%>
</div>

<%-- Sitestat tracking DO NOT REMOVE --%>
<%--@ include file="/com/tui/uk/view/common/sitestat/ordinaryTracking.jspf"--%>
<%-- / Sitestat tracking --%>