<%@include file="/common/commonTagLibs.jspf"%>
<%-- TERMS AND CONDITIONS HEADER --%>
        <div class="subheaderBar">
           <img src="/cms-cps/firstchoice/hugo/images/icons/header_terms.png" alt=""/>
           <h2>Terms and conditions</h2>
        </div>

        <div class="clearer"></div>

      <%-- ERRATA - OUT OF SCOPE FOR INITIAL HUGO RELEASE --%>
        <%-- <div id="PaymentDetailsLatestHolidayInformation" class="errata">
           <h3>More information</h3>
           <p>The adult-only swimming pool will be closed for essential maintenance between 4th September and 22nd September 2007.</p>
        </div> --%>

         <%-- TERMS STATEMENT --%>
        <div class="checkoutPaymentTermsConditions">
           <input type="checkbox" id="agreeTermAndCondition" name="agreeTermAndCondition"  onclick="this.blur();"/>
               <label for="agreeTermAndCondition">I accept the </label>
            <%--*************TERMS AND CONDITIONS LINK ************************************--%>
            <a href="JavaScript:void(popUpBookingConditionsHugo('<c:out value="${clientUrl}"/>','<c:out
            value="${bookingComponent.accommodationSummary.accommodationInventorySystem}"/>','<c:out
            value="${bookingComponent.flightSummary.flightSupplierSystem}"/>','<c:catch><fmt:formatDate
            value="${bookingComponent.bookingData.bookingDate}" pattern="yyyy-MM-dd"/></c:catch>'));"
            class="arrow-link"><img width="10" height="10" alt="The following link opens in a new window"  src="/cms-cps/firstchoice/hugo/images/icons/link_new_window.gif" />Terms and conditions
            </a>
            <c:if test="${crmOptIn==true}">
            , Privacy Policy and Data Protection Notice.
            </c:if>
            <%--*************END TERMS AND CONDITIONS LINK ************************************--%>
        </div>


      <%-- SUBMIT SECTION --%>
        <div class="checkoutPaymentSubmit">
		<c:choose>
	  	  <c:when test="${( bookingComponent.nonPaymentData['atol_flag'] ) &&( bookingComponent.nonPaymentData['atol_flag'] !=null )}">
				  <c:if test="${(bookingComponent.nonPaymentData['atol_protected']) &&(bookingComponent.nonPaymentData['atol_protected'] !=null)}">
		<p>
            This booking is authorised under the ATOL licence of TUI UK Limited, ATOL 2524,
            and is protected under the ATOL scheme, as set out in the ATOL certificate to be
            supplied.
           </p>
		   </c:if>
		</c:when>
		</c:choose>
           <p>
            You are about to submit your card details to make your holiday booking.
            It may take up to 30 seconds to complete your payment.
           </p>


        <%/*  <c:if test="${!fhBookingContext.holidayBooked}">*/%>
           <c:if test="${bookingInfo.newHoliday}">
             <!-- <img id="CheckoutPaymentDetailsBookNowButtonDisabledButton" src='/cms-cps/firstchoice/hugo/images/buttons/book_holiday_now_greyed.gif' class="checkoutPaymentSubmitButtonDisabled" alt="" />
             <input type="image" id="CheckoutPaymentDetailsBookNowButton" src="/cms-cps/firstchoice/hugo/images/buttons/book_holiday_now.gif" alt="Book this holiday now" /> -->
			 <input type="submit" class="pay checkoutPaymentSubmitButtonDisabled" id="CheckoutPaymentDetailsBookNowButtonDisabledButton" value="Pay"/>
             <div id="confirmBookNowButton">
             <input type="image" id="CheckoutPaymentDetailsBookNowButton" src="/cms-cps/firstchoice/hugo/images/buttons/book_holiday_now.png" alt="Book this holiday now" />
             </div>
         </c:if>
          <%/*  </c:if> */%>
           <div class="clearer"></div>
        </div>