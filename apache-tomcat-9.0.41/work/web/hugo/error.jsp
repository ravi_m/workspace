 <%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request"/>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <c:choose>
    <c:when test="${not empty sessionScope=='true'}">
    <title>First Choice | Error</title>
   </c:when>
   <c:otherwise>
   <title>First Choice | Session Timed Out</title>
   </c:otherwise>
   </c:choose>


   <script type="text/javascript">
   // Global variable to store theme path. JavaScripts use
   // this variable to build full path for images, css, etc.
   var themePath = "https://www.firstchoice.co.uk/cmsmedia/cmsdev/firstchoice.co.uk/themes/firstchoice.co.uk/2008";
</script>

<link rel="stylesheet" type="text/css" href="/cms-cps/firstchoice/hugo/css/style.css"/>
<link rel="stylesheet" href="/cms-cps/firstchoice/hugo/css/integrated_payments.css" type="text/css"   />
<link rel="stylesheet" type="text/css" href="/cms-cps/firstchoice/hugo/css/content_modules.css"/>
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="/cms-cps/firstchoice/hugo/css/integrated_payments_ie7.css"/>
<link rel="stylesheet" type="text/css" href="/cms-cps/firstchoice/hugo/css/style_js.css"/>
<![endif]-->
<!--[if IE 6]>
<link rel="stylesheet" type="text/css" href="/cms-cps/firstchoice/hugo/css/summaryPrice_ie6.css"/>
<![endif]-->
<script type="text/javascript" src="/cms-cps/firstchoice/hugo/js/popup.js"></script>
<meta name="errorPageText" content="session_timeout" />
<link type="image/x-icon" href="/cms-cps/firstchoice/hugo/images/fevicon/favicon.ico" rel="icon"/>
<link type="image/x-icon" href="/cms-cps/firstchoice/hugo/images/fevicon/favicon.ico" rel="shortcut icon"/>
<script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
<%-- ensighten starts --%>
      <%@include file="tag.jsp"%>
<%-- ensighten ends --%>
</head>

<body class="withoutBackground systemDown">
   <div id="PageContainer">
  <c:choose>
				<c:when test="${requestScope.cookieswitch=='true'}">
				  <c:choose>
				<c:when test="${requestScope.cookiepresent != 'true'}">
					<jsp:include page="/hugo/cookielegislation.jsp"/>
					</c:when>
			  </c:choose>
 			</c:when>
			</c:choose>
      <div id="Page">

         <div id="PageColumn1">

             <div id="Header">
              <%-- #Header --%>
                <div id="headerContentWrapper">
			      <div id="masthead">
                     <a href="http://www.firstchoice.co.uk"><img src="/cms-cps/firstchoice/hugo/images/header/first_choice_logo.gif" alt="First Choice" width="151" height="36" /></a>
                  </div>
              </div>

             </div>
           <div id="MainContent">
               <div id="BodyWide">
                  <div class="bodyPadder">
                     <div id="PrimaryColumn">

                     </div>


<div class="accessibility">
   <ul>

      <li><a href="#main_content">Jump to main Content</a></li>
      <li><a href="#SearchPanel">Jump to Search</a></li>
      <li><a href="#masthead">Jump to Main Menu</a></li>
      <li><a href="#Footer">Jump to Secondary Menu</a></li>
      <li><a href="#">Jump to Accessibility Statement</a></li>
   </ul>

   <p>If you are using a screen reader, we recommend that you&nbsp;<a href="#">disable JavaScript</a>.</p>
</div>

<div id="BodyWide">
   <div class="bodyPadder">
   <c:choose>
    <%--Tech Difficulties Error Message --%>
    <c:when test="${not empty sessionScope=='true'}">
      <h1>Sorry, we are currently experiencing technical difficulties</h1>
      <div class="standardContent size7">
        <img src="/cms-cps/firstchoice/hugo/images/error/content_pic.jpg" class="left" alt=""/>
        <div class="contentBlock">
           <p>For further assistance please call us on 0871 200 7799 (Mon to Fri 8am - 10pm, Sat and Sun 9am - 8pm). Alternatively, please search again later.</p>
           <a href="http://www.firstchoice.co.uk" class="arrow-link">Return to homepage</a>
        </div>
        <div class="clearer"></div>
     </div>
   </c:when>
   <%--End Tech Difficulties Error Message --%>

   <%--Session Time out Error Message --%>
   <c:otherwise>
      <h1>Sorry, your session has timed out</h1>
      <div class="standardContent size7">
        <img src="/cms-cps/firstchoice/hugo/images/error/content_pic.jpg" class="left" alt=""/>
        <div class="contentBlock">
          Due to inactivity you session has timed out, please click homepage to begin another search.
          <br />
          <a href="http://www.firstchoice.co.uk" class="arrow-link">Click here to begin another search</a>.
        </div>
        <div class="clearer"></div>
    </div>
   </c:otherwise>
   <%--End of Session Time out Error Message --%>
   </c:choose>
  </div> <%--end  bodyPadder--%>

</div> <%--end  BodyWide--%>
<script type="text/javascript">
                  var sitestatpagename = "tech-difficulties.page";
</script>
                  </div>
               </div>
            </div>


<%-- 200508 --%>


<%-- The QUERY_STRING is booking=true --%>
<div id="Footer">
<div id="atol_abta">

     <div id="copyrightUtilityMenu" style="border:none;width: 690px;">
		    <a href="http://www.abta.com/" title="The following link opens in a new window" rel="external" target="_blank"><img src="/cms-cps/firstchoice/hugo/images/footer/logo-abta_2009.gif" alt="ABTA logo" class="foot_img_1" width="65" height="29"/></a>
  <a href="http://www.atol.org.uk/" title="The following link opens in a new window" rel="external" target="_blank"><img src="/cms-cps/firstchoice/hugo/images/footer/atol_logo.gif" alt="ATOL logo"
  class="foot_img_2" width="41" height="41"/></a>
             <ul>
               <li><a href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/accessibility/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Accessibility</a></li>
               <li><a href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/terms-of-use/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Terms of use</a></li>
               <li><a href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/security-policy/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Security policy</a></li>
               <li class="last"><a href="javascript:Popup('http://www.firstchoice.co.uk/our-policies/privacy-policy/?popup=true',630,600,'scrollbars=yes');" rel="nofollow">Privacy policy</a></li>
             </ul>
           </div>
  <%-- end copyright and utilities--%>
</div>
<%--include sitestat.js file--%>
          <script language='JavaScript1.1' type='text/javascript' src='/cms-cps/firstchoice/hugo/js/js-sitestat/sitestat.js'></script>
<%--include sitestat.js file //--%>

<script>
/*******************************************************************************
* variables
********************************************************************************/

//pageviews variables

//test url inspection variable
var url = location.href;

// Get the relevant part of the url
var url=url.split('co.uk/');
/*******************************************************************************
* variables
********************************************************************************/

/*******************************************************************************
* pageviews - sitestat variable
********************************************************************************/
//check to see if the page is a 'generated' page, if not then build the sitestat
//string by manipulating the url
if (typeof( window[ 'sitestatpagename' ] ) == 'undefined')
 {
      url[1] = url[1].replace(/\//g,".");
      url[1]+='page';
      sitestatpagename=url[1];
   }
/*******************************************************************************
* pageviews - sitestat variable
********************************************************************************/



/*******************************************************************************
* SiteStat forms code - disabled for now
********************************************************************************/
//if (document.getElementById('SearchPanelForm'))
// {
// IncludeJavaScript('/js-sitestat/sitestatforms.js');
// IncludeJavaScript('/js-sitestat/form.js');
// }
/*******************************************************************************
* Page containing search panel end
********************************************************************************/

/*******************************************************************************
* Transactional pages begin
********************************************************************************/
//sitestat string js variable = sun.main.confirmation (or whatever the conf page id is
if (sitestatpagename == 'sun.main.confirmation')
{
   // (c) 2004 Copyright Nedstat BV Netherlands.
   // ALL RIGHTS RESERVED
   // version v4.6 2004-02-17 TP added resend check with session cookie check
   // version v4.7 2004-02-22 TP added ns_ prefix and ns_undefined validation

   function ns_order(ns_customerID,ns_client_id,ns_order_id)
   {
      this.id = ns_customerID;
      this.ns_client_id = ns_client_id;
      this.ns_order_id = ns_order_id;
      this.orders = new Array();
      this.addLine = ns_addLine;
      this.sendOrder = ns_sendOrder;
   }

   function ns_addLine(prod_id,brand,prod_grp,shop,qty,prod_price)
   {
      this.orders[this.orders.length] = new Array(prod_id,brand,prod_grp,shop,qty,prod_price);
   }

   function ns_cookieVal(cookieName)
   {
      var thisCookie = document.cookie.split("; ");
      for (var i=0; i<thisCookie.length; i++)
     {
        if (cookieName == thisCookie[i].split("=")[0])
        {
          return thisCookie[i].split("=")[1];
       }
     }
   }

   function ns_isEmpty(s)
   {
      return ((s == null) || (s.length == 0))
   }

   function ns_isSignedFloat (s)
   {
      var s = "" + s + "";
     var i=0;
      var seenDecimalPoint = false;
      if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") ) i++;
     if (s.charAt(i) == ".") return false;
      for (i ; i < s.length; i++)
      {
         var c = s.charAt(i);
       if ((c == ".") && !seenDecimalPoint) seenDecimalPoint = true;
       else if ( (c > "9") || (c < "0") ) return false;
     }
     if (s.charAt(i-1)==".") return false;
     return true;
   }

   function ns_sendOrder()
   {
      if (ns_cookieVal("ns_order_id_"+this.ns_order_id)!="true")
      {
        document.cookie = "ns_order_id_"+this.ns_order_id+"=true";
       var keys = new Array('ns_prod_id','ns_brand','ns_prod_grp','ns_shop','ns_qty','ns_prod_price');
       var ns__t="&ns__t="+(new Date()).getTime();
       for (var i=0; i < this.orders.length; i++)
       {
          var start = this.id;
         start += ns__t + '&ns_commerce=true&ns_type=hidden&ns_client_id=' + this.ns_client_id + '&ns_order_id=' + this.ns_order_id ;
         start += '&ns_orderlines=' + this.orders.length;
         start += '&ns_orderline_id=' + ((i*1)+1);
         for (var t=0; t < keys.length; t++)
         {
            if (ns_isEmpty(this.orders[i][t])) this.orders[i][t]="ns_undefined";
            if ( (keys[t]=="ns_qty") || (keys[t]=="ns_prod_price") )
            {
               if (!ns_isSignedFloat(this.orders[i][t])) this.orders[i][t]="ns_undefined";
            }
            eval("start += '&"+keys[t]+"="+escape(this.orders[i][t])+"';");
         }
         eval("nedstat"+i+" = new Image();");
         eval("nedstat"+i+".src = start;");
       }
     }
   }

   ns_prod_price = ns_prod_price.replace(/,/g,"");

   ns_myOrder = new ns_order('http://uk.sitestat.com/firstchoice/firstchoice/s?sun.main.comfirmation&order_id='+ ns_order_id,ns_client_id,ns_order_id);
   ns_myOrder.addLine(ns_prod_id,ns_brand,ns_prod_grp,ns_shop,ns_qty,ns_prod_price);
   ns_myOrder.sendOrder();
}
/*******************************************************************************
* Transactional pages end
********************************************************************************/


/*******************************************************************************
* Include the relvant js files
********************************************************************************/
function IncludeJavaScript(jsFile)
  {
    document.write('<script type=\"text/javascript\" src=\"'+ jsFile + '\"><\/script>');
  }
/*******************************************************************************
* Include the relvant js files
********************************************************************************/



/*******************************************************************************
* Code from sitestat
********************************************************************************/
function sitestat(ns_l){ns_l+="&amp;ns__t="+(new Date()).getTime();ns_pixelUrl=ns_l;
ns_0=document.referrer;
ns_0=(ns_0.lastIndexOf("/")==ns_0.length-1)?ns_0.substring(ns_0.lastIndexOf("/"),0):ns_0;
if(ns_0.length>0)ns_l+="&amp;ns_referrer="+escape(ns_0);
if(document.images){ns_1=new Image();ns_1.src=ns_l;}else
document.write('<img src="'+ns_l+'" width="1" height="1" />');}

var secureModeCheck=window.location.href;
sitestatPageViewString="http"+(secureModeCheck.indexOf('https:')==0?'s':'')+"://uk.sitestat.com/firstchoice/firstchoice/s?"+sitestatpagename;

sitestat(sitestatPageViewString);
/*******************************************************************************
* Code from sitestat
********************************************************************************/

</script>
<%--<noscript><img src="http://uk.sitestat.com/firstchoice/firstchoice/s?non-js-user" width="1" height="1" alt="" /></noscript>--%>
<!--Deleted script tag for INC000012246207 -->
         </div>
        <div id="PageColumn2"></div>
      </div>

   </div>
 </body>
</html>

