
<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}"
  scope="session" />
<!--Total cost  -->
<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}"
  var="totalCostingLine" type="number" pattern="##.##"
  maxFractionDigits="2" minFractionDigits="2" />
<!--End of Total cost -->

<div id="price">
<table class="pricepanel" cellpadding="0" cellspacing="0" border="0">
 <tr>
    <th>
    <h2>Price</h2>
    </th>
 </tr>
 <tr>
   <td>
    <div id="divPricePanel">
   <table class="costing" border="0" cellpadding="0" cellspacing="0">
          <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
        <c:if test="${costingLine.amount.amount != 0.0}">
               <c:set var="description" value='${costingLine.itemDescription}' />
            <tr>
            <td>
      <span class="pricepanel_rightpadding" style="float: right">
      <c:choose>
      <c:when test="${fn:contains(description, 'Promotional')}" >
      <fmt:formatNumber value="${costingLine.amount.amount}"
             var="promotionalDiscount" type="number" pattern="##.##"
             maxFractionDigits="2" minFractionDigits="2" />
          <c:out escapeXml="false" value="${currency}" />-<c:out escapeXml="false" value="${promotionalDiscount}" />
      </c:when>
      <c:otherwise>
       <c:out escapeXml="false" value="${currency}" /><fmt:formatNumber value="${costingLine.amount.amount}"
                  type="number" pattern="#,###" minIntegerDigits="1" maxFractionDigits="2" minFractionDigits="2"/>
       </c:otherwise>
       </c:choose>
      </span>
            <c:out value="${description}" />
              <c:if test="${costingLine.quantity != null}">
              (x<c:out value="${costingLine.quantity}"/>)
              </c:if>
             </td>
             </tr>
             <c:if test="${fn:contains(description, 'Saving')}">
               <tr>
                 <td>
                   <hr color="#6699ff" width="100%" size="1"/>
                 </td>
               </tr>
             </c:if>
          </c:if>

    </c:forEach>
    <c:if test="${bookingInfo.newHoliday == 'false' && (bookingInfo.calculatedCardCharge.amount != null && bookingInfo.calculatedCardCharge.amount != 0)}">
         <tr>
           <td>
        <span class= "pricepanel_rightpadding" style="float: right">
        &pound;<c:out value="${bookingInfo.calculatedCardCharge.amount}"/>
        </span>
        <span class="pricepanel_rightpadding">Credit card charge</span>
           </td>
        </tr>
        </c:if>
      <tr id="cardChargeDiv" style="display:none;">
          <td>
          <span id="cardChargeAmount" style="float:right;margin-left: 53px;_margin-left: 0px;">
            <c:out escapeXml="false" value="${currency}"/>
          </span>
          <span>Credit card charge</span>
          </td>
        </tr>

    <tr id="promoDiscountDiv" style="display:none;">
        <td>
          <span id="promoCodeDiscount" style="float:right;margin-right: -62px;_margin-right: 0px">
      </span>
          <span id ="PromoDiscountText" style="display:none">Promotional Discount</span>
        </td>
      </tr>

    <tr>
        <td style="padding-top: 5px" class="price">
          <span id="totalAmountText">Total Web Price</span>
       <fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}"
            type="number" var="totalCostingLineWithoutDiscount" maxFractionDigits="2" minFractionDigits="2"/>
          &pound;<span id="totalAmount" class="total"><c:out value="${totalCostingLineWithoutDiscount}" />
          </span>
        </td>
      </tr>
   </table>
  </div>
   </td>
 </tr>
</table>
</div>