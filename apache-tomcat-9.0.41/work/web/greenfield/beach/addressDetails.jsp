<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="tabIndex" value="1" scope="page"/>

<!-- Passenger Details -->
</p>
<h5>Please provide card holder address details</h5>
<p>To help ensure your card details remain secure, please confirm the address of the cardholder. If this is the same as the lead passenger address, you just need to check the box.</p>

<!-- Passenger details -->



<input type="hidden" value="true" name="deliveredToLeadPassenger" />
<table cellspacing="0" cellpadding="0" border="0">
  <tbody>
    <tr>
      <td nowrap="nowrap"></td>
      <td>
        <input name = "autoCheckCardAddress" type="checkbox" id="autoCheckCardAddress" onclick="javascript:autoCompleteCardAddress()"/> Same address as lead passenger
      </td>
      <td nowrap="nowrap"></td>
      <td>    
      </td>
   </tr>
    <tr>
      <td nowrap="nowrap">House Name</td>
      <td>
        <input name = "payment_0_street_address1" type="text" alt="House Name|Y|ADDRESS" id="payment_0_street_address1" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/>" size="20" maxlength="20"/>
      </td>
      <td nowrap="nowrap"></td>
      <td>    
      </td>
    </tr>
    <tr>
      <td>House No</td>
            <td><input name="cardHolderHouseNumber" type="text" alt="House No|N|ADDRESS" id="cardHolderHouseNumber" value="<c:out value="${bookingComponent.nonPaymentData['cardHolderHouseNumber']}" />" size="4" maxlength="4"/></td>
            <td nowrap="nowrap"></td>
            <td></td>            
    </tr>
    <tr>
      <td>Address</td>
            <td nowrap="nowrap"><input name = "payment_0_street_address2" id="payment_0_street_address2" type="text" alt="all required address fields|Y|ADDRESS"
            value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>" size="25" maxlength="25" /> <span class="mandatory">*</span> </td>
            <td> </td>
            <td> </td>
    </tr>
    <tr>
      <td> </td>
             <td><input type="text" name = "cardHolderAddressLine2" id="cardHolderAddressLine2" alt="Address Line 2|N|ADDRESS" value="<c:out value="${bookingComponent.nonPaymentData['cardHolderAddressLine2']}"/>" size="25" maxlength="25" /></td>
             <td></td>
             <td nowrap="nowrap"> </td>
    </tr>
    <tr>
      <td>Town/City </td>
            <td><input  name="payment_0_street_address3" id="payment_0_street_address3" type="text" alt="all required address fields|Y|ADDRESS" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>" size="25" maxlength="25"/> <span class="mandatory">*</span></td>
            <td valign="top"></td>
            <td valign="top"></td>
    </tr>
        <tr>
          <td>County</td>
            <td>
            <input name="payment_0_street_address4" id="payment_0_street_address4" type="text" alt="all required address fields|N|ADDRESS" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address4']}"/>" size="16" maxlength="16"/>
            </td>
          <td valign="top"> </td>
          <td> </td>
        </tr>
        <tr>
          <td>Post Code</td>
            <td><input name="payment_0_postCode" id="payment_0_postCode" type="text" alt="Post Code|Y|POSTCODE" value="<c:out value="${bookingInfo.paymentDataMap['postCode']}"/>" size="8" maxlength="8"/> <span class="mandatory">*</span></td>
          <td valign="middle" align="right" colspan="2">All fields marked <span class="price">*</span> must be completed</td>
        </tr>
  </tbody>
</table>