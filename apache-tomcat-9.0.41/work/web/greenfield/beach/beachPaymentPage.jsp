<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />

<%--***********Essential JSP settings. to be used through out the page ***********--%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="clientUrl" value='${bookingComponent.clientDomainURL}' scope="request"/>
<c:set var="currency" scope="request">
  <c:choose>
    <c:when test='${bookingComponent.totalAmount.currency.currencyCode == "GBP"}'>&pound;</c:when>
    <c:when test='${bookingComponent.totalAmount.currency.currencyCode == "EUR"}'>&euro;</c:when>
    <c:when test='${bookingComponent.totalAmount.currency.currencyCode == "USD"}'>&#36;</c:when>
    <c:otherwise><c:out value="${bookingComponent.totalAmount.currency.currencyCode}"/></c:otherwise>
  </c:choose>
</c:set>
 <c:set var="isHolidayNew" value="${bookingInfo.newHoliday}" scope="session" />
 <c:set var='clientapp' value='${bookingComponent.clientApplication.clientApplicationName}'/>

<%--*********Essential JSP settings. to be used through out the page **************--%>

<%-- Declare Variables --%>
<head>
   <title>Thomson.co.uk - Booking Step 3 - Personal Details and Payment</title>

   <link rel="stylesheet" type="text/css" href="/cms-cps/greenfield/beach/css/thomson_global.css" />
   <link rel="stylesheet" type="text/css" href="/cms-cps/greenfield/beach/css/footer_styles.css"  />
   <link rel="stylesheet" type="text/css" href="/cms-cps/greenfield/beach/css/thomson_booking.css" />
   <link rel="stylesheet" type="text/css" href="/cms-cps/greenfield/beach/css/print.css" media="print" />

   <link type="image/x-icon" href="/cms-cps/greenfield/common/images/favicon.ico" rel="icon"/>
   <link type="image/x-icon" href="/cms-cps/greenfield/common/images/favicon.ico" rel="shortcut icon"/>
   <script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/print.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/dynamic_core.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/dynamic_price.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/formvalidation.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/personaldetailsandpayment.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/general.js"></script>
   <script type="text/javascript" src="/cms-cps/greenfield/beach/js/popups.js"></script>


   <script type="text/javascript" src="/cms-cps/greenfield/common/js/paymentUpdate.js"></script>

   <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
   <script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
   <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
<script>
  var threeDCards = new Map();
 <c:forEach var="threeDCards" items="${bookingInfo.payDescription}">
       threeDCards.put("<c:out value="${threeDCards.key}"/>","<c:out value="${threeDCards.value}"/>");
 </c:forEach>

  var clientDomainURL = "<c:out value='${clientUrl}'/>";
 var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
 var token = "<c:out value='${param.token}' />";

 function autoCompleteCardAddress()
{
	if ($("autoCheckCardAddress").checked)
	{
    	$("payment_0_street_address1").value=$("HouseName").value;
    	$("cardHolderHouseNumber").value=$("houseNo").value;
    	$("payment_0_street_address2").value=$("addressLine1").value;
    	$("cardHolderAddressLine2").value=$("addressLine2").value;
        $("payment_0_street_address3").value=$("city").value;
        $("payment_0_street_address4").value=$("county").value;
        $("payment_0_postCode").value=$("postCode").value;
	}
}
     </script>

      <!--[if IE]>
         <style type="text/css">
         .securityCode{margin-left:13px;}
         .iAgree{margin: 8px 0px; float: left;}

         #visaDetailsOverlay{width: 297px;left:485px;top:200px;}
         #masterCardDetailsOverlay{width: 297px;left:485px;top:50px;}

        .genericOverlay .close{color:#3257C9 !important;margin: -20px 0px 0 270px !important;position:absolute;}


        .genericOverlay .body .content{margin:0px 5px 0 2px;padding-left:-2px;}
         div .content p{margin-left: 5px;}
         </style>
     <![endif]-->

</head>
<jsp:include page="/greenfield/beach/cookielegislation.jsp"></jsp:include>
<body>
 <%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure" , "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
 %>

<div class="pagediv">
<style type="text/css">
  #poh_header_container { padding: 0px 5px 0px 5px; height: 62px; margin: 0px; } /* height should not exceed 62px due to menu sizing restrictions */
  #poh_header_table p { margin: 0px; padding: 0px; }

  a.poh_headlink, a.poh_headlink:link, a.poh_headlink:active, a.poh_headlink:visited, a.poh_headlink:hover { font: normal 10px verdana,arial,sans-serif; color: #fff; }
  a.poh_headlink:hover { color: #ef0000; }

  .po_header_logo { padding-top: 10px; padding-bottom: 10px; }
  .po_header_logo img { display: block; }
  #po_header_book_section { height: 15px; border-right: 1px solid #3366cc; margin-top: 32px; background-image: url(/cms-cps/greenfield/beach/images/icon_phone_17x13.gif); background-repeat: no-repeat; background-position: 0px 0px; }
  #po_header_book_section p { padding-left: 23px; font: bold 10px verdana,arial,sans-serif; color: #3366cc; }
  #po_header_register_section { margin-top: 32px; height: 15px; }
  #po_header_register_section p { padding-left: 15px; font: normal 10px verdana,arial,sans-serif; color: #3366cc; }

</style>


<%@include file="header.jsp"%>
<!-- <form method="post" action=""> -->
<form name="paymentdetails" method="post" action="/cps/processPayment?b=23000&token=<c:out value='${param.token}'/>&tomcat=<c:out value='${param.tomcat}' />">
<input type="hidden" name="isHolidayNew" value="<c:out value="${isHolidayNew}"/>">
<input type="hidden" name="payment_0_selectedCountryCode" value="GB">
<input type="hidden" name="payment_0_selectedCountry" value="United Kingdom" />

<c:set var="visiturl" value="yes" scope="session" />
<div id="logo">
<table cellspacing="0" cellpadding="0" border="0" bgcolor="#99cbfe" width="547">
   <tbody><tr>
      <td height="50" width="158">
         <div class="margintopfive">
            <img src="/cms-cps/greenfield/beach/images/thomson_print_logo_black.gif"/>
         </div>
      </td>
      <td valign="bottom" class="txtalignr paddingbottomfive">
         <div class="margintopfive floatr">
            <h1>Personal Details & Payment</h1>
         </div>
      </td>
   </tr>
</tbody></table>
</div>
<div class="pagebr"></div>
<table cellspacing="0" cellpadding="0" border="0" class="maintable">
  <tbody>
  <tr>
    <td valign="top" style="width: 209px;" rowspan="2">
       <jsp:include page="summaryPanel.jsp"/>
    </td>
    <td valign="top" colspan="2">
      <jsp:include page="navigation.jsp"></jsp:include>
    </td>
  </tr>
  <tr>
    <td width="5"> </td>
    <td valign="top">
     <jsp:include page="paymentPanel.jsp"/>
    </td>
  </tr>
  </tbody></table>
  </td>
 </tr>
</tbody></table>
</form>
<jsp:include page="footer.jsp"/>
</div>
</body>
</html>