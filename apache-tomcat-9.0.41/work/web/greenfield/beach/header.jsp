<!-- Header -->

<div class="pagediv">
<div class="screenonly" id="poh_header_container">
  <table cellspacing="0" cellpadding="0" border="0" width="758" id="poh_header_table">
    <tbody><tr>
    <td valign="top">
      <div class="po_header_logo"><a href="<c:out value='${clientUrl}'/>/th/beach/getHomePageBeach.do"><img height="35" border="0" width="191" title="Thomson" alt="Thomson logo" src="/cms-cps/greenfield/beach/images/tuithomson.gif"/></a></div>
    </td>

    <td width="200" valign="top">
      <div title="Book online or call 0871 231 4691" id="po_header_book_section">
        <!--<p>UK Call Centre: 0871 231 4691</p> -->
        <span style="padding-left: 23px; font: bold 10px verdana,arial,sans-serif; color: #3366cc;">
            UK Call Centre: 0871 231 4691
            </span>
            <br clear="all"/>
            <span style="font: 8px verdana,arial,sans-serif;font-weight:normal;color: #3366cc;">Calls Cost 10p per minute plus network extras.</span>
        
      </div>
    </td>

    <td width="125" valign="top">
      <div title="Register to receive offers" id="po_header_register_section">
        <p><a class="poh_headlink" href="http://www.thomson.co.uk/editorial/ecrm/register-for-offers-full-page.html?content=register.htm">Register for Offers</a></p>
      </div>
    </td>
    </tr>
  </tbody></table>
</div>
<!-- / Header -->
