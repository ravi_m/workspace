<%@include file="/common/commonTagLibs.jspf"%>

<div class="border"><!-- Promotional Code -->
<h6>Discount Code</h6>
<p>If you have a discount code, please enter the code below and
click 'Validate Code'</p>
<table cellspacing="0" cellpadding="5" border="0">
  <tbody>
    <tr>
      <td>Please enter your Promotional Code
        <input type="text" id="promotionalCode" alt="a valid Promotional Code|N|ALPHANUMHYPHEN"  tabindex="109" size="20" maxlength="20" name="promotionalCodeDetails.promotionalCode" value="<c:out value="${bookingComponent.nonPaymentData['promotionalCode']}"/>" />
        <input type="hidden" id="isPromoCodeApplied" name="isPromoCodeApplied" value="<c:out value="${bookingComponent.nonPaymentData['isPromoCodeApplied']}"/>" />
        <input type="hidden" id="promoDiscount" name="promoDiscount" value="<c:out value="${bookingComponent.nonPaymentData['promoDiscount']}"/>" />
             </td>
       <td>
      <a href="javascript:updatePromotionalCode();"> <img
        border="0" title="Validate Code" alt="Validate Code"
        src="/cms-cps/greenfield/cruise/images/validate_code.gif" /> </a>
        </td>

</tr>
<tr> <td><span  id="promoMainContainer">
     <div id="promoText1" style='display:none' ></div>
        </span></td>
    <td></td>
    </tr>
            </tbody>
</table>
<!-- /Promotional Code --></div>