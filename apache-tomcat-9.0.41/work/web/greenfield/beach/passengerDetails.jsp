<%@include file="/common/commonTagLibs.jspf"%>

<c:set var="tabIndex" value="1" scope="page"/>

<!-- Passenger Details -->
<div class="border">
<h6>Passenger Details</h6>

<div class="elementstohide">
    <c:if test="${bookingInfo.newHoliday}">
        <span id="confirmButton1" style="float: right ! important; margin-right: 3px; margin-bottom: 5px;">
            <a class="Pay" target="" href="javascript:updateEssentialFields();javascript:ValidateForm(document.forms.paymentdetails);"><img border="0" title="Pay" alt="Pay" src="/cms-cps/greenfield/beach/images/pay.gif"/></a>
  </span>
    </c:if>
</div>



<p>Please enter passenger names to match those shown on your passports.</p>

<!-- Passenger details -->


<table cellspacing="0" cellpadding="0" border="0">
  <tbody>
    <tr>
      <td width="70" />
      <td width="55">Title</td>
      <td width="100">First Name</td>
      <td align="center" width="50">Initial</td>
      <td width="100">Surname</td>
      <td>&nbsp;</td>
    </tr>

    <c:set var="numberOfPassengers" />
    <c:set var="passengerCount" value="0" />
    <c:forEach var="passenger" items="${bookingComponent.passengerRoomSummary}" varStatus="status">
    <c:set var="noOfRooms" value="${status.count}" />

    <c:forEach var="passengerRoom" items="${passenger.value}" varStatus="passen">
      <tr>
        <c:set var="numberOfPassengers" value="${passen.index}"/>
        <td>
          <c:out value="${passengerRoom.label}"/>

          <c:set var="identifier" value="passenger_${passengerCount}_identifier"/>
          <input type = "hidden" name="<c:out value='${identifier}'/>" value="<c:out value='${passengerRoom.identifier}'/>"/>
          <c:if test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR'}">
            <c:if test = "${passengerCount == 0}">
              <input name="leadPassenger" type="hidden"
              value="<c:out value="${passengerRoom.identifier}"/>" />

            </c:if>
          </c:if>
        </td>
        <td>
          <c:set var="titlekey" value="personaldetails_${passengerCount}_title"/>
          <select name="${titlekey}" tabindex="${tabIndex}"><c:set var="tabIndex" value="${tabIndex+1}" scope="page"/>
            <c:choose>
                       <c:when test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR' || passengerRoom.ageClassification == 'SENIOR2'}">
                          <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mr'}">selected</c:if>>Mr</option>
                          <option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mrs'}">selected</c:if>>Mrs</option>
                          <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
                          <option value="Ms" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Ms'}">selected</c:if>>Ms</option>
                          <option value="Dr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Dr'}">selected</c:if>>Dr</option>
                         <option value="Rev" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Rev'}">selected</c:if>>Rev</option>
                         <option value="Prof" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Prof'}">selected</c:if>>Prof</option>
                      </c:when>
                   <c:otherwise>
                        <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mr'}">selected</c:if>>Mr</option>
                        <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
                        <option value="Mstr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mstr'}">selected</c:if>>Mstr</option>
                        <option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mrs'}">selected</c:if>>Mrs</option>
                  </c:otherwise>
                 </c:choose>
          </select>
          <input type="hidden" name="country" value="United Kingdom" />
        </td>
        <!-- TODO: class for all the text boxes -->
          <td nowrap="nowrap">
          <c:set var="foreNameKey" value="personaldetails_${passengerCount}_foreName"/>
          <input type="text" tabindex="${tabIndex}" class="medium"
          maxlength="15" value="<c:out value="${bookingComponent.nonPaymentData[foreNameKey]}"/>" name="<c:out value="${foreNameKey}"/>" alt="name details as shown on your passport.|Y|ALPHA"/>
          <span class="price">*</span>
          <c:set var="tabIndex" value="${tabIndex+1}" scope="page"/>
        </td>

          <td align="center">
          <c:set var="middleNameKey" value="personaldetails_${passengerCount}_middleInitial"/>
          <input  type="text" maxlength="1" class="vsmall"  tabindex="${tabIndex}" maxlength="1"
          value="<c:out value="${bookingComponent.nonPaymentData[middleNameKey]}"/>" name="<c:out value="${middleNameKey}"/>" alt="Initial|N|ALPHA"/>
          <c:set var="tabIndex" value="${tabIndex+1}" scope="page"/>
        </td>
          <td nowrap="nowrap">
          <c:set var="lastNameKey" value="personaldetails_${passengerCount}_surName"/>
          <input  type="text" maxlength="15" class="medium" tabindex="${tabIndex}"
          id="lastname_<c:out value='${passengerCount}'/>" value="<c:out value="${bookingComponent.nonPaymentData[lastNameKey]}"/>" name="<c:out value="${lastNameKey}"/>" alt="name details as shown on your passport.|Y|ALPHA"/>
          <span class="price">*</span>
          <c:set var="tabIndex" value="${tabIndex+1}" scope="page"/>
      </td>

           <td width="110" style="padding-top: 15px;">
      <c:if test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR'}">
            <c:if test = "${passengerCount == 0}">
    <strong>Lead Passenger</strong>
    <strong>(Must be 18 years or over)</strong>
            </c:if>

      </c:if>
      </td>
      </tr>
      <c:set var="passengerCount" value="${passengerCount+1}" />
      </c:forEach>
    </c:forEach>

  </tbody>
</table>
<h5>Please provide Lead Passenger Details</h5>
<input type="hidden" value="true" name="deliveredToLeadPassenger" />
<table cellspacing="0" cellpadding="0" border="0">
  <tbody>
    <tr>
      <td nowrap="nowrap">House Name</td>
      <td>
        <input name = "houseName" type="text" alt="House Name|N|ADDRESS" id="HouseName" value="<c:out value="${bookingComponent.nonPaymentData['houseName']}"/>" tabindex="98" class="houseName" maxlength="20" />
      </td>
      <td nowrap="nowrap">Daytime Phone No.</td>
      <td>
      <input name="dayTimePhone" type="text" alt="valid phone details|Y|PHONE" value="<c:out value="${bookingComponent.nonPaymentData['dayTimePhone']}"/>" tabindex="105" class="medium" maxlength="23"/>
                 <span class="mandatory">*</span>
      </td>
    </tr>
    <tr>
      <td>House No</td>
            <td><input name="houseNumber" type="text" alt="House No|N|ADDRESS" id="houseNo" value="<c:out value="${bookingComponent.nonPaymentData['houseNumber']}" />" tabindex="99" class="vsmall" maxlength="4"/></td>
            <td nowrap="nowrap">Mobile Phone No.</td>
            <td><input name="mobilePhone" type="text" alt="valid phone details|N|PHONE" value="<c:out value="${bookingComponent.nonPaymentData['mobilePhone']}"/>" tabindex="106" class="medium" maxlength="23"/> </td>
            <input type="hidden" alt="Please enter a house name or number|Y|HOUSE_EITHER_OR|HouseName|houseNo"/>
    </tr>
    <tr>
      <td>Address</td>
            <td nowrap="nowrap"><input name = "addressLine1" id="addressLine1" type="text" alt="all required address fields|Y|ADDRESS"
            value="<c:out value="${bookingComponent.nonPaymentData['addressLine1']}"/>" tabindex="100" class="large" maxlength="25" /> <span class="mandatory">*</span> </td>
            <td> </td>
            <td> </td>
    </tr>
    <tr>
      <td> </td>
             <td><input type="text" name = "addressLine2" id="addressLine2" alt="Address Line 2|N|ADDRESS" value="<c:out value="${bookingComponent.nonPaymentData['addressLine2']}"/>" tabindex="101" class="large" maxlength="25" /></td>
             <td>Email Address</td>
             <td nowrap="nowrap"><input name="emailAddress" type="text" alt="email address|Y|EMAIL" id="EmailAddress" value="<c:out value="${bookingComponent.nonPaymentData['emailAddress']}"/>" tabindex="107" class="large" maxlength="100"/> <span class="mandatory">*</span> </td>
    </tr>
    <tr>
      <td>Town/City </td>
            <td><input  name="city" id="city" type="text" alt="all required address fields|Y|ADDRESS" value="<c:out value="${bookingComponent.nonPaymentData['city']}"/>" tabindex="102" class="large" maxlength="25"/> <span class="mandatory">*</span></td>
            <td nowrap="nowrap">Re-enter Email Address</td>
            <td valign="top"><input name="emailAddress1" type="text" alt="your email address again|Y|EMAIL" id="EmailAddress2" value="<c:out value="${bookingComponent.nonPaymentData['emailAddress1']}"/>" tabindex="108" class="large" maxlength="100"/> <span class="mandatory">*</span></td>
    </tr>
        <tr>
          <td>County</td>
            <td>
            <input name="county" id="county" type="text" alt="all required address fields|N|ADDRESS" value="<c:out value="${bookingComponent.nonPaymentData['county']}"/>" tabindex="103" class="medium" maxlength="16"/>
            </td>
          <td valign="top"> </td>
          <td> </td>
        </tr>
        <tr>
          <td>Post Code</td>
            <td><input name="postCode" id="postCode" type="text" alt="Post Code|Y|POSTCODE" value="<c:out value="${bookingComponent.nonPaymentData['postCode']}"/>" tabindex="104" class="small" maxlength="8"/> <span class="mandatory">*</span></td>
          <td valign="middle" align="right" colspan="2">All fields marked <span class="price">*</span> must be completed</td>
        </tr>
  </tbody>
</table>
<input type="hidden"
  alt="The email addresses entered do not match. Please re-enter your details|-|MATCH|EmailAddress|EmailAddress2" />
<!-- / Passenger details --></div>