<!-- Header -->

<div class="pagediv">
<div class="screenonly" id="poh_header_container">

      <div class="po_header_logo"><a href="http://10.145.35.216/th/beach/getHomePageBeach.do"><img height="35" border="0" width="191" title="Thomson" alt="Thomson logo" src="/cms-cps/greenfield/beach/images/tuithomson.gif"/></a></div>
    
</div>
<!-- / Header -->


<div class="elementstohide threeDSecNav">

            <!-- Navigation -->
            <div class="sub_nav_book">
               <!-- Navigation -->

<div class="global_nav">
      <style type="text/css">
      .portalmenu { width:551px; margin:-2px 0px 14px 0px; }
      .portalsubmenu { top:85px !important; }

    #glnav_menu { float: right; width: 547px; height: 26px; margin: 0px; padding: 0px; }
    #glnav_menu li { clear: none; z-index: 900; margin-left: 2px; height: 26px; float: left; position: relative; display: block; }
    #glnav_menu #glnav_home { margin-left: 0px; }

    /* Set up link behaviours */
    .glnav_mainlink, .glnav_mainlink_hover { color: #ffff00; background-color: #3366cc; }

    .glnav_mainlink a:link span, .glnav_mainlink a:active span, .glnav_mainlink a:visited span, .glnav_mainlink a:hover span, .glnav_mainlink_hover a:link span,
    .glnav_mainlink_hover a:active span, .glnav_mainlink_hover a:visited span, .glnav_mainlink_hover a:hover span {
      overflow: hidden; cursor: hand; display: block; width: 100%; padding: 3px 0px 3px 0px; text-align: center;
    }

    .glnav_mainlink a:link, .glnav_mainlink a:active, .glnav_mainlink a:visited, .glnav_mainlink a:hover, .glnav_mainlink_hover a:link, .glnav_mainlink_hover a:active, .glnav_mainlink_hover a:visited, .glnav_mainlink_hover a:hover {
      overflow: hidden; background-color: #3366cc; text-decoration: none; color: #fff; font: bold 11px arial,verdana,sans-serif;
    }

    .glnav_mainlink a:hover, .glnav_mainlink_hover a:link, .glnav_mainlink_hover a:active, .glnav_mainlink_hover a:visited, .glnav_mainlink_hover a:hover {
      overflow: hidden; background-color: #3366cc; color: #ffff00;
    }

    .nav_submenu {
      /* Width 306 for dual column and 153 for single solumn menu */
      /* Note, although the columns only need to be 150 and 300 px, the extra 3px width fixes a bug in IE 6 that causes duplicate content to appear */
      width: 153px; text-align: left; z-index: 900; margin: 0px; padding: 0px; position: absolute; top: -10000px; left: 0px;
    }

    .nav_submenu a:link, .nav_submenu a:active, .nav_submenu a:visited, .nav_submenu a:hover {
      float: left; text-decoration: none; color: #fff; font: bold 11px arial,verdana,sans-serif; z-index: 900;  background-color: #6699ff;  display: block; border: 1px solid #3366cc; width: 150px; overflow: hidden;
    }

    .nav_submenu a:hover {
      float: left; background-color: #3366cc; color: #ffff00;
    }

    .nav_submenu a:link span, .nav_submenu a:active span, .nav_submenu a:visited span, .nav_submenu a:hover span {
      z-index: 900; display: block; padding: 3px 3px 3px 6px;
    }

    /* set main button widths */
    #glnav_home { margin-left: -2px; padding-left: 0px; width: 54px; }
    #glnav_holidays { width: 68px; }
    #glnav_byh { width: 148px; }
    #glnav_flights { width: 62px; }
    #glnav_hotels { width: 60px; }
    #glnav_extras { width: 58px; }
    #glnav_offers { width: 52px; }
    #glnav_late { width: 46px; }
    #glnav_tv { width: 29px; }

    /* Set up padding for the popup menus - to position the menu further left use a negative "left:" value */
    #glnav_extras_sub { left: -92px; }
    #glnav_offers_sub { left: -98px; }
    #glnav_late_sub { left: -191px; }

</style>


<script type="text/javascript">


/* ---------------------------------------------------------------------------------------- *\
  Function name:    showGlobalNav()
  Function purpose: Takes an element id for the global nav and hides all other nav menus
                    except the input element off the page edge. Displays the input menu
            and highlights the button text colour
\* ---------------------------------------------------------------------------------------- */

function showGlobalNav(elem) {
  if(document.getElementById(elem+'_sub').style.display!='block') {
    // hide all other global nav lists
    elemArray = document.getElementById('glnav_menu').getElementsByTagName('li');
    for(i=0; i < elemArray.length; i++) {
      if(document.getElementById(elemArray[i].id+'_sub') && (document.getElementById(elemArray[i].id+'_sub') != elem+'_sub')) {
        document.getElementById(elemArray[i].id+'_sub').style.top = '-10000px';
      }
    }

    // show this nav list
    document.getElementById(elem+'_sub').style.top = '22px';

    // set main button colour
    mainlinksElem = document.getElementById(elem).getElementsByTagName('div');
    for(i=0; i < mainlinksElem.length; i++) {
      if(mainlinksElem[i].className == 'glnav_mainlink') {
        mainlinksElem[i].className = 'glnav_mainlink_hover';
      }
    }
  }
}


/* ---------------------------------------------------------------------------------------- *\
  Function name:    hideGlobalNav()
  Function purpose: Hides the input element id's associated menu off the screen edge and
                    turns off button highlighting for the element
\* ---------------------------------------------------------------------------------------- */

function hideGlobalNav(elem) {
  // hide popup off screen edge
  document.getElementById(elem+'_sub').style.top = '-10000px';

  // set main button colour
  mainlinksElem = document.getElementById(elem).getElementsByTagName('div');
  for(i=0; i < mainlinksElem.length; i++) {
    if(mainlinksElem[i].className == 'glnav_mainlink_hover') {
      mainlinksElem[i].className = 'glnav_mainlink';
    }
  }
}


/* ---------------------------------------------------------------------------------------- *\
  Function name:    winPopLink()
  Function purpose: Open the link in a new window
\* ---------------------------------------------------------------------------------------- */

function winPopLink(elem) {
  window.open(elem);
}


</script>

  <ul id="glnav_menu">

    <li id="glnav_home">
      <div class="glnav_mainlink"><a href="http://10.145.35.216/th/beach/getHomePageBeach.do"><span>Home</span></a></div>
    </li>

    <li onMouseOut="hideGlobalNav(this.id);" onMouseOver="showGlobalNav(this.id);" id="glnav_holidays">
      <div class="glnav_mainlink">
      <a href="http://10.145.35.216/th/beach/book/SubmitExtraOptionsInsurance.do;jsessionId="><span>Holidays</span></a>
      </div>


      <div class="nav_submenu" id="glnav_holidays_sub" style="top: -10000px;">
        <a href="http://10.145.35.216/th/beach/getHomePageBeach.do"><span>Sun Holidays</span></a>
        <a href="http://www.thomsonbeach.co.uk/th/cruise/getHomePageCruise.do"><span>Cruise</span></a>
        <a href="http://www.thomsonski.co.uk"><span>Ski & Snowboarding</span></a>
        <a href="http://www.thomsoncities.co.uk"><span>City Breaks</span></a>
        <a href="http://www.thomsonauctions.co.uk"><span>Auctions</span></a>
        <a onClick="winPopLink(this.href); return false;" href="http://www.thomsonlakesandmountains.co.uk"><span>Lakes & Mountains</span></a>
        <a href="http://www.thomson.co.uk/po/showVillaHomePage.do"><span>Villas</span></a>
        <a href="http://10.145.35.216/th/beach/viewSelectedBrochure.do?brochureCode=TH07&amp;ico=POH_faraway"><span>Faraway Shores</span></a>
        <a href="http://www.thomsonworldwide.co.uk/"><span>Worldwide</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=uk_holidays.htm"><span>UK Holidays</span></a>
        <a href="http://www.thomson.co.uk/po/showSpecialistHolidays.do"><span>Specialist Holidays</span></a>
        <a href="http://www.thomsonalfresco.co.uk"><span>Mobile Homes</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=weddings.html"><span>Weddings in Paradise</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=disney.html"><span>Disneyland© Resort Paris</span></a>
        <a href="http://10.145.35.216/th/beach/viewSelectedBrochure.do?brochureCode=TH80&amp;ico=POH_stagHen"><span>Stags & Hens</span></a>
      </div>
    </li>

    <li id="glnav_byh">
      <div class="glnav_mainlink"><a href="http://www.thomson.co.uk/buildyourown/search.do"><span>Build Your Own Holiday</span></a></div>
    </li>

    <li id="glnav_flights">
      <div class="glnav_mainlink"><a href="http://www.thomsonfly.com"><span>Flights</span></a></div>
    </li>

    <li id="glnav_hotels">
      <div class="glnav_mainlink"><a href="http://www.thomson.co.uk/thomson/page/ao/home/home.page"><span>Hotels</span></a></div>
    </li>

    <li onMouseOut="hideGlobalNav(this.id);" onMouseOver="showGlobalNav(this.id);" id="glnav_extras">
      <div class="glnav_mainlink"><a href="http://10.145.35.216/th/beach/book/SubmitExtraOptionsInsurance.do;jsessionId="><span>Extras</span></a>
      </div>
      <div class="nav_submenu" id="glnav_extras_sub">
        <a href="http://tui.intelli-direct.com/e/PExit.dll?m=609&amp;p=1&amp;url=http%3a%2f%2fwww%2Ethomson%2Eco%2Euk%2fexcursions"><span>Excursions</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=essential_car_hire.htm"><span>Car Hire</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=essential_insurance.htm"><span>Insurance</span></a>
        <a href="http://www.thomson.co.uk/travelbuddy"><span>Travel Buddy</span></a>
        <a href="http://www.thomson.co.uk/po/showEssentials.do?essential=parking"><span>Airport Parking</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=attractions.html&amp;ICO=POMenu_attractions"><span>Attraction Tickets</span></a>
        <a href="http://www.mythomsonphotos.co.uk/"><span>My Photos</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=essential_airport_hotel.htm"><span>Airport Hotels</span></a>
        <a href="http://www.parkbcp.co.uk/index.aspx?ID1=WEB1604&amp;ID2=11111&amp;User=HYP&amp;DIATA=&amp;DPRD=Lounge"><span>Airport Lounges</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=essential_airtext_updates.htm"><span>Flights Text Updates</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=essential_coach_transfers.htm"><span>Coach & Taxi Transfers</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=essential_transport_uk.htm"><span>Transfer to UK Airport</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=essential_foreign_exchange.htm"><span>Foreign Exchange</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=essential_travel_options.htm"><span>Travel Options</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=essential_holiday_vouchers.htm"><span>Holiday Vouchers</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=essential_buying_property.htm"><span>Buying Property Abroad</span></a>
      </div>
    </li>

    <li onMouseOut="hideGlobalNav(this.id);" onMouseOver="showGlobalNav(this.id);" id="glnav_offers">
      <div class="glnav_mainlink"><a href="http://10.145.35.216/th/beach/book/SubmitExtraOptionsInsurance.do;jsessionId="><span>Deals</span></a></div>
      <div class="nav_submenu" id="glnav_offers_sub">
        <a href="http://www.thomsonbeach.co.uk/th/showSpecialOffersBeach.do"><span>Sun Holidays</span></a>
        <a href="http://www.thomsonbeach.co.uk/th/cruise/showCruiseSpecialOffers.do"><span>Cruise</span></a>
        <a href="http://www.thomsonfly.com/en/index.html?ito=1715"><span>Flights</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=buildyourowndeals.html"><span>Build Your Own</span></a>
        <a href="http://www.thomsonhotelsandapartments.co.uk/ao/home?formAction=displayOffers&amp;type=special&amp;searchForm=false&amp;site=ao&amp;content=ao/special/accommodationonly.html"><span>Hotels</span></a>
        <a href="http://www.thomsonski.co.uk/specialoffers/t_specialoffers.html"><span>Ski & Snowboard</span></a>
        <a href="http://www.thomsoncities.co.uk/city_offers.asp"><span>City Breaks</span></a>
        <a href="http://www.thomsonlakesandmountains.co.uk/offers_menu.asp"><span>Lakes & Mountains</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=tvp_offers.html"><span>Villas</span></a>
        <a href="http://www.thomsonworldwide.co.uk/kenilworth/Offers.jsp"><span>Worldwide</span></a>
        <a href="http://www.thomsonalfresco.co.uk/late_deals.asp"><span>Mobile Homes</span></a>
      </div>
    </li>

    <li id="glnav_tv">
      <div class="glnav_mainlink"><a href="http://10.145.35.216/th/LeakToAffiliate.do?target=http://www.thomsontv.com&amp;XA=BB"><span>TV</span></a></div>
    </li>

  </ul>


</div>

<!-- / Navigation -->

            </div>
            <!-- / Navigation -->
  </div>


<div class="elementstohide threeDSecBookingstep">
      
      <div class="bookingstep">         
	<p>&nbsp;1. Room Upgrade</p>&nbsp;&nbsp;<img height="19" width="12" title="" alt="" src="/cms-cps/greenfield/beach/images/booking_step_arrow.gif" />&nbsp;&nbsp;
    <p>&nbsp;2. Holiday Options</p>&nbsp;&nbsp;<img height="19" width="12" title="" alt="" src="/cms-cps/greenfield/beach/images/booking_step_arrow.gif"/>&nbsp;&nbsp;
    <h6>&nbsp;3. Payment</h6>&nbsp;&nbsp;<img height="19" width="12" title="" alt="" src="/cms-cps/greenfield/beach/images/booking_step_arrow.gif"/>&nbsp;&nbsp;
    <p>&nbsp;4. Confirmation</p>&nbsp;&nbsp;
      </div>
  </div>