<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<h1>Personal Details & Payment</h1>
<div class="bookingstep outertable">
            <p>1. Cabin Selection</p>
              <img height="19" width="12" title="" alt="" src="/cms-cps/greenfield/cruise/images/booking_step_arrow.gif"/>  
            <p>2. Holiday Options</p>
              <img height="19" width="12" title="" alt="" src="/cms-cps/greenfield/cruise/images/booking_step_arrow.gif"/>  
            <h6>3. Payment</h6>
              <img height="19" width="12" title="" alt="" src="/cms-cps/greenfield/cruise/images/booking_step_arrow.gif"/>  
            <p>4. Confirmation</p>
</div>

<table class="bookingpanel bordertopnone outertable" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td>
<c:if test="${not empty bookingComponent.errorMessage}">
<div id="pricediff1" class="pageinfotext">
       <c:out value="${bookingComponent.errorMessage}"/>
</div>
</c:if>
</td>
</tr>
<tr>
<td>
  <jsp:include page="passengerDetails.jsp"></jsp:include>
  <jsp:include page="discountCode.jsp"></jsp:include>
  <jsp:include page="cardDetails.jsp"></jsp:include>
  <jsp:include page="paymentAmount.jsp"></jsp:include>
  <jsp:include page="termsAndConditions.jsp"></jsp:include>
</td>
</tr>
</tbody>
</table>