<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:if test="${not empty bookingComponent.flightSummary.outboundFlight}">
	<c:forEach var="outboundFlight" items="${bookingComponent.flightSummary.outboundFlight}">
		<c:if test="${not empty outboundFlight}">
			<c:set var="departureAirportName" value="${outboundFlight.departureAirportName}" />
			<c:set var="departureDateTime" value="${outboundFlight.departureDateTime}" />
			<c:set var="arrivalAirportName" value="${outboundFlight.arrivalAirportName}" />
			<c:set var="arrivalDateTime" value="${outboundFlight.arrivalDateTime}" />
		</c:if>
	</c:forEach>
</c:if>

<table cellspacing="0" cellpadding="0" border="0" class="pricepanel">
	<tbody>
		<tr>
			<th>
			<h2>Port Information</h2>
			</th>
		</tr>
		<tr>
			<td>
			<table cellspacing="2" cellpadding="0" border="0">
				<tbody>
					<tr>
						<th colspan="2">Embarkation</th>
					</tr>
					<tr>
						<td width="70"><strong>Port</strong></td>
						<td>
							<c:out value="${departureAirportName}" /><br />
							<fmt:formatDate value="${departureDateTime}" pattern="dd/MM/yy" />
						</td>
					</tr>
					<tr>
						<td colspan="3" height="1">
							<hr width="100%" color="#6699ff" size="1" />
						</td>
					</tr>
					<tr>
						<th colspan="2">Disembarkation</th>
					</tr>
					<tr>
						<td width="80"><strong>Port</strong></td>
						<td>
							<c:out value="${arrivalAirportName}" /><br />
							<fmt:formatDate value="${arrivalDateTime}" pattern="dd/MM/yy" />
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
