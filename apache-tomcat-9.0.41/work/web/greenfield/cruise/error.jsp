<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />

<%--***********Essential JSP settings. to be used through out the page ***********--%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="clientUrl" value='${bookingComponent.clientDomainURL}' scope="request"/>
<c:set var="currency" value="&pound;" scope="request"/>
<c:set var="url" value='${bookingComponent.clientDomainURL}'/>

<script>
 var clientDomainURL = "<c:out value='${clientUrl}'/>";
 var tomcatInstance= "<c:out value='${param.tomcat}' />";
 var token = "<c:out value='${param.token}' />";
</script>


<%--*********Essential JSP settings. to be used through out the page **************--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<version-tag:version/>
<title>Thomson Cruises - System Error </title>


<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/thomson_global.css" type="text/css"/>
<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/footer_styles.css" type="text/css"/>
<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/thomson_booking.css" type="text/css"/>
<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/homepage.css" type="text/css"/>
<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/book_a_cruise_tab.css" type="text/css"/>
<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/content.css" type="text/css"/>

<script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>

<script type="text/javascript" src="/cms-cps/greenfield/common/js/paymentUpdate.js"></script>

<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/dynamic_core.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/dynamic_price.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/formvalidation.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/general.js"></script>

<!--this js needs to be included later-->
<!--<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/intellitracker.js"></script>-->

<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/menu.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/personaldetailsandpayment.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/popups.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/print.js"></script>
<script type="text/javascript">


/* ---------------------------------------------------------------------------------------- *\
  Function name:    showGlobalNav()
  Function purpose: Takes an element id for the global nav and hides all other nav menus
                    except the input element off the page edge. Displays the input menu
            and highlights the button text colour
\* ---------------------------------------------------------------------------------------- */

function showGlobalNav(elem) {
  if(document.getElementById(elem+'_sub').style.display!='block') {
    // hide all other global nav lists
    elemArray = document.getElementById('glnav_menu').getElementsByTagName('li');
    for(i=0; i < elemArray.length; i++) {
      if(document.getElementById(elemArray[i].id+'_sub') && (document.getElementById(elemArray[i].id+'_sub') != elem+'_sub')) {
        document.getElementById(elemArray[i].id+'_sub').style.top = '-10000px';
      }
    }

    // show this nav list
    document.getElementById(elem+'_sub').style.top = '22px';

    // set main button colour
    mainlinksElem = document.getElementById(elem).getElementsByTagName('div');
    for(i=0; i < mainlinksElem.length; i++) {
      if(mainlinksElem[i].className == 'glnav_mainlink') {
        mainlinksElem[i].className = 'glnav_mainlink_hover';
      }
    }
  }
}


/* ---------------------------------------------------------------------------------------- *\
  Function name:    hideGlobalNav()
  Function purpose: Hides the input element id's associated menu off the screen edge and
                    turns off button highlighting for the element
\* ---------------------------------------------------------------------------------------- */

function hideGlobalNav(elem) {
  // hide popup off screen edge
  document.getElementById(elem+'_sub').style.top = '-10000px';

  // set main button colour
  mainlinksElem = document.getElementById(elem).getElementsByTagName('div');
  for(i=0; i < mainlinksElem.length; i++) {
    if(mainlinksElem[i].className == 'glnav_mainlink_hover') {
      mainlinksElem[i].className = 'glnav_mainlink';
    }
  }
}


/* ---------------------------------------------------------------------------------------- *\
  Function name:    winPopLink()
  Function purpose: Open the link in a new window
\* ---------------------------------------------------------------------------------------- */

function winPopLink(elem) {
  window.open(elem);
}
</script>

</head>
<body>
<div class="pagediv">
<jsp:include page="header.jsp"></jsp:include>
<style type="text/css">
  #poh_header_container { padding: 0px 5px 0px 5px; height: 66px; margin: 0px; } /* height should not exceed 66px due to menu sizing restrictions */
  #poh_header_table p { margin: 0px; padding: 0px; }

  a.poh_headlink, a.poh_headlink:link, a.poh_headlink:active, a.poh_headlink:visited, a.poh_headlink:hover { font: normal 10px verdana,arial,sans-serif; color: #fff; }
  a.poh_headlink:hover { color: #ef0000; }

  .po_header_logo { padding-top: 10px; padding-bottom: 10px; }
  .po_header_logo img { display: block; }
  #po_header_book_section { height: 15px; border-right: 1px solid #3366cc; margin-top: 32px; background-image: url(/cms-cps/greenfield/cruise/images/icon_phone_17x13.gif); background-repeat: no-repeat; background-position: 0px 0px; }
  #po_header_book_section p { padding-left: 23px; font: bold 10px verdana,arial,sans-serif; color: #3366cc; }
  #po_header_register_section { margin-top: 32px; height: 15px; }
  #po_header_register_section p { padding-left: 15px; font: normal 10px verdana,arial,sans-serif; color: #3366cc; }
</style>
<style type="text/css">
.portalmenu { width:547px; margin-top:-2px; margin-bottom: 14px; margin-left: 0px; }
.portalsubmenu { top:89px !important; }

    #glnav_menu { float: right; width: 547px; height: 26px; margin: 0px; padding: 0px; }
    #glnav_menu li { clear: none; z-index: 900; margin-left: 2px; height: 26px; float: left; position: relative; display: block; }
    #glnav_menu #glnav_home { margin-left: 0px; }

    /* Set up link behaviours */
    .glnav_mainlink, .glnav_mainlink_hover { color: #ffff00; background-color: #3366cc; }

    .glnav_mainlink a:link span, .glnav_mainlink a:active span, .glnav_mainlink a:visited span, .glnav_mainlink a:hover span, .glnav_mainlink_hover a:link span,
    .glnav_mainlink_hover a:active span, .glnav_mainlink_hover a:visited span, .glnav_mainlink_hover a:hover span {
      overflow: hidden; cursor: hand; display: block; width: 100%; padding: 3px 0px 3px 0px; text-align: center;
    }

    .glnav_mainlink a:link, .glnav_mainlink a:active, .glnav_mainlink a:visited, .glnav_mainlink a:hover, .glnav_mainlink_hover a:link, .glnav_mainlink_hover a:active, .glnav_mainlink_hover a:visited, .glnav_mainlink_hover a:hover {
      overflow: hidden; background-color: #3366cc; text-decoration: none; color: #fff; font: bold 11px arial,verdana,sans-serif;
    }

    .glnav_mainlink a:hover, .glnav_mainlink_hover a:link, .glnav_mainlink_hover a:active, .glnav_mainlink_hover a:visited, .glnav_mainlink_hover a:hover {
      overflow: hidden; background-color: #3366cc; color: #ffff00;
    }

    .nav_submenu {
      /* Width 306 for dual column and 153 for single solumn menu */
      /* Note, although the columns only need to be 150 and 300 px, the extra 3px width fixes a bug in IE 6 that causes duplicate content to appear */
      width: 153px; text-align: left; z-index: 900; margin: 0px; padding: 0px; position: absolute; top: -10000px; left: 0px;
    }

    .nav_submenu a:link, .nav_submenu a:active, .nav_submenu a:visited, .nav_submenu a:hover {
      float: left; text-decoration: none; color: #fff; font: bold 11px arial,verdana,sans-serif; z-index: 900;  background-color: #6699ff;  display: block; border: 1px solid #3366cc; width: 150px; overflow: hidden;
    }

    .nav_submenu a:hover {
      float: left; background-color: #3366cc; color: #ffff00;
    }

    .nav_submenu a:link span, .nav_submenu a:active span, .nav_submenu a:visited span, .nav_submenu a:hover span {
      z-index: 900; display: block; padding: 3px 3px 3px 6px;
    }

    /* set main button widths */
    #glnav_home { padding-left: 2px; width: 54px; }
    #glnav_holidays { width: 68px; }
    #glnav_byh { width: 148px; }
    #glnav_flights { width: 62px; }
    #glnav_hotels { width: 60px; }
    #glnav_extras { width: 58px; }
    #glnav_offers { width: 52px; }
    #glnav_late { width: 46px; }
    #glnav_tv { width: 29px; }

    /* Set up padding for the popup menus - to position the menu further left use a negative "left:" value */
    #glnav_extras_sub { left: -92px; }
    #glnav_offers_sub { left: -98px; }
    #glnav_late_sub { left: -191px; }

</style>

<!-- Main Content-->
   <table class="maintable" cellpadding="0" cellspacing="0" border="0" style="margin-right: 3px !important;">
      <tr>
         <td valign="top">
            <div id="globalNav">
                <!-- Global Navigation -->
        <script type="text/javascript">
          var productType = "CRUISE";
        </script>
        <!-- / Global Navigation -->
        <!-- Global Navigation -->

  <ul id="glnav_menu">

    <li id="glnav_home">
      <div class="glnav_mainlink"><a href="<c:out value='${url}'/>/po/getHomePage.do"><span>Home</span></a></div>
    </li>

    <li onmouseout="hideGlobalNav(this.id);" onmouseover="showGlobalNav(this.id);" id="glnav_holidays">
      <div class="glnav_mainlink"><a href="<c:out value='${url}'/>/th/cruise/book/SubmitExtraOptionsInsurance.do;"><span>Holidays</span></a></div>

      <div class="nav_submenu" id="glnav_holidays_sub" style="top: -10000px;">
        <a onclick="menu_select('<c:out value='${url}'/>'+'/th/beach/getHomePageBeach.do'); return false;" href="<c:out value='${url}'/>/th/beach/getHomePageBeach.do"><span>Sun Holidays</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/th/cruise/getHomePageCruise.do'); return false;" href="<c:out value='${url}'/>/th/cruise/getHomePageCruise.do"><span>Cruise</span></a>
        <a onclick="tc_redirect('Ski','http://www.thomsonski.co.uk'); return false;" href="http://www.thomsonski.co.uk"><span>Ski & Snowboarding</span></a>
        <a onclick="tc_redirect('','http://www.thomsoncities.co.uk');" href="http://www.thomsoncities.co.uk"><span>City Breaks</span></a>
        <a href="http://www.thomsonauctions.co.uk"><span>Auctions</span></a>
        <a onclick="winPopLink(this.href); return false;" href="http://www.thomsonlakesandmountains.co.uk"><span>Lakes & Mountains</span></a>
        <a href="<c:out value='${url}'/>/po/showVillaHomePage.do"><span>Villas</span></a>
        <a onclick="tc_redirect('','<c:out value='${url}'/>/th/beach/viewSelectedBrochure.do?brochureCode=TH07&amp;ico=POH_faraway');" href="<c:out value='${url}'/>/th/beach/viewSelectedBrochure.do?brochureCode=TH07&amp;ico=POH_faraway"><span>Faraway Shores</span></a>
        <a onclick="tc_redirect('','http://www.thomsonworldwide.co.uk');" href="http://www.thomsonworldwide.co.uk/"><span>Worldwide</span></a>
        <a onclick="menu_select('http://www.thomson.co.uk/po/showContent.do?content=uk_holidays.htm'); return false;" href="http://www.thomson.co.uk/po/showContent.do?content=uk_holidays.htm"><span>UK Holidays</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showSpecialistHolidays.do'); return false;" href="<c:out value='${url}'/>/po/showSpecialistHolidays.do"><span>Specialist Holidays</span></a>
        <a onclick="tc_redirect('','http://www.thomsonski.co.uk');" href="http://www.thomsonalfresco.co.uk"><span>Mobile Homes</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=weddings.html'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=weddings.html"><span>Weddings in Paradise</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=disney.html'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=disney.html"><span>Disneylandİ Resort Paris</span></a>
        <a onclick="tc_redirect('','<c:out value='${url}'/>/th/beach/viewSelectedBrochure.do?brochureCode=TH80&amp;ico=POH_stagHen');" href="<c:out value='${url}'/>/th/beach/viewSelectedBrochure.do?brochureCode=TH80&amp;ico=POH_stagHen"><span>Stags & Hens</span></a>
      </div>
    </li>

    <li id="glnav_byh">
      <div class="glnav_mainlink"><a onclick="tc_redirect('','http://www.thomson.co.uk/buildyourown/search.do');" href="http://www.thomson.co.uk/buildyourown/search.do"><span>Build Your Own Holiday</span></a></div>
    </li>

    <li id="glnav_flights">
      <div class="glnav_mainlink"><a onclick="tc_redirect('','http://www.thomsonfly.com');" href="http://www.thomsonfly.com"><span>Flights</span></a></div>
    </li>

    <li id="glnav_hotels">
      <div class="glnav_mainlink"><a href="http://www.thomson.co.uk/thomson/page/ao/home/home.page"><span>Hotels</span></a></div>
    </li>

    <li onmouseout="hideGlobalNav(this.id);" onmouseover="showGlobalNav(this.id);" id="glnav_extras">
      <div class="glnav_mainlink"><a href ="<c:out value='${url}'/>/th/cruise/book/SubmitExtraOptionsInsurance.do;"><span>Extras</span></a></div>
      <div class="nav_submenu" id="glnav_extras_sub" style="top: -10000px;">
        <a onclick="menu_select('http://tui.intelli-direct.com/e/PExit.dll?m=609&amp;p=1&amp;url=http%3a%2f%2fwww%2Ethomson%2Eco%2Euk%2fexcursions'); return false;" href="http://tui.intelli-direct.com/e/PExit.dll?m=609&amp;p=1&amp;url=http%3a%2f%2fwww%2Ethomson%2Eco%2Euk%2fexcursions"><span>Excursions</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=essential_car_hire.htm'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=essential_car_hire.htm"><span>Car Hire</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=essential_insurance.htm'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=essential_insurance.htm"><span>Insurance</span></a>
        <a href="<c:out value='${url}'/>/travelbuddy"><span>Travel Buddy</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>/po/showEssentials.do?essential=parking'); return false" href="<c:out value='${url}'/>/po/showEssentials.do?essential=parking"><span>Airport Parking</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=attractions.html&amp;ICO=THMenu_attractions'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=attractions.html&amp;ICO=POMenu_attractions"><span>Attraction Tickets</span></a>

        <a onclick="tc_redirect('','http://www.mythomsonphotos.co.uk/');" href="http://www.mythomsonphotos.co.uk/"><span>My Photos</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'http://www.thomson.co.uk/po/showContent.do?content=essential_airport_hotel.htm'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=essential_airport_hotel.htm"><span>Airport Hotels</span></a>
        <a onclick="tc_redirect('','<c:out value='${url}'/>'+'/index.asp?ID1=WEB1604&amp;ID2=11111&amp;User=HYP&amp;DIATA=&amp;DPRD=Lounge'); return false" href="<c:out value='${url}'/>/index.aspx?ID1=WEB1604&amp;ID2=11111&amp;User=HYP&amp;DIATA=&amp;DPRD=Lounge"><span>Airport Lounges</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=essential_airtext_updates.htm'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=essential_airtext_updates.htm"><span>Flights Text Updates</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=essential_coach_transfers.htm'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=essential_coach_transfers.htm"><span>Coach & Taxi Transfers</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=essential_transport_uk.htm'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=essential_transport_uk.htm"><span>Transfer to UK Airport</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=essential_foreign_exchange.htm'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=essential_foreign_exchange.htm"><span>Foreign Exchange</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=essential_travel_options.htm'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=essential_travel_options.htm"><span>Travel Options</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=essential_holiday_vouchers.htm'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=essential_holiday_vouchers.htm"><span>Holiday Vouchers</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>'+'/po/showContent.do?content=essential_buying_property.htm'); return false;" href="<c:out value='${url}'/>/po/showContent.do?content=essential_buying_property.htm"><span>Buying Property Abroad</span></a>
      </div>
    </li>

    <li onmouseout="hideGlobalNav(this.id);" onmouseover="showGlobalNav(this.id);" id="glnav_offers">
      <div class="glnav_mainlink"><a href="<c:out value='${url}'/>/th/cruise/book/SubmitExtraOptionsInsurance.do;"><span>Deals</span></a></div>
      <div class="nav_submenu" id="glnav_offers_sub" style="top: -10000px;">
        <a onclick="menu_select('<c:out value='${url}'/>/th/showSpecialOffersBeach.do'); return false;" href="<c:out value='${url}'/>/th/showSpecialOffersBeach.do"><span>Sun Holidays</span></a>
        <a onclick="menu_select('<c:out value='${url}'/>/th/cruise/showCruiseSpecialOffers.do'); return false;" href="<c:out value='${url}'/>/th/cruise/showCruiseSpecialOffers.do"><span>Cruise</span></a>
        <a onclick="tc_redirect('','http://www.thomsonfly.com/en/index.html?ito=1715');" href="http://www.thomsonfly.com/en/index.html?ito=1715"><span>Flights</span></a>
        <a onclick="tc_redirect('','http://www.thomson.co.uk/po/showContent.do?content=buildyourowndeals.html');" href="http://www.thomson.co.uk/po/showContent.do?content=buildyourowndeals.html"><span>Build Your Own</span></a>
        <a onclick="tc_redirect('','http://www.thomsonhotelsandapartments.co.uk/ao/home?formAction=displayOffers&amp;type=special&amp;searchForm=false&amp;site=ao&amp;content=ao/special/accommodationonly.html');" href="http://www.thomsonhotelsandapartments.co.uk/ao/home?formAction=displayOffers&amp;type=special&amp;searchForm=false&amp;site=ao&amp;content=ao/special/accommodationonly.html"><span>Hotels</span></a>
        <a onclick="tc_redirect('','http://www.thomsonski.co.uk/specialoffers/t_specialoffers.html'); return false;" href="http://www.thomsonski.co.uk/specialoffers/t_specialoffers.html"><span>Ski & Snowboard</span></a>
        <a onclick="tc_redirect('','http://www.thomsoncities.co.uk/city_offers.asp');" href="http://www.thomsoncities.co.uk/city_offers.asp"><span>City Breaks</span></a>
        <a onclick="tc_redirect('Lakes','http://www.thomsonlakesandmountains.co.uk/offers_menu.asp'); return false;" href="http://www.thomsonlakesandmountains.co.uk/offers_menu.asp"><span>Lakes & Mountains</span></a>
        <a href="http://www.thomson.co.uk/po/showContent.do?content=tvp_offers.html"><span>Villas</span></a>
        <a onclick="tc_redirect('','http://www.thomsonworldwide.co.uk/kenilworth/Offers.jsp');" href="http://www.thomsonworldwide.co.uk/kenilworth/Offers.jsp"><span>Worldwide</span></a>
        <a onclick="tc_redirect('','http://www.thomsonalfresco.co.uk/late_deals.asp');" href="http://www.thomsonalfresco.co.uk/late_deals.asp"><span>Mobile Homes</span></a>
      </div>
    </li>

    <li id="glnav_tv">
      <div class="glnav_mainlink"><a onclick="tc_redirect('','http://www.thomsonbeach.co.uk/th/LeakToAffiliate.do?target=http://www.thomsontv.com&amp;XA=BB');" href="<c:out value='${url}'/>/th/LeakToAffiliate.do?target=http://www.thomsontv.com&amp;XA=BB"><span>TV</span></a></div>
    </li>

  </ul>

  <div style="clear: both; height: 1px; font-size: 1px;"> </div>

  <!-- / Global Navigation -->

            </div>
         </td>
      </tr>
      <tr>
         <td valign="top">
            <table border="0" cellspacing="0" cellpadding="0" class="waitingpanel">
               <tr>
                  <td>
                     <img src="/cms-cps/greenfield/cruise/images/cruise_waiting.jpg" width="207" alt="" class="marginfive" />
                  </td>
                  <td valign="top">
                     <div>
                       Sorry, we are unable to process your request at this moment. Please try again later
                       or contact our booking team on 0870 060 2277.
                     <%--
                        <logic:present name="<%= org.apache.struts.Globals.ERROR_KEY %>" >
                           <html:errors bundle="errorwarningsCruise" />
                        </logic:present>
                        <logic:notPresent name="<%= org.apache.struts.Globals.ERROR_KEY %>">
                           <bean:message bundle="errorwarningsCruise" key="error.general" />
                        </logic:notPresent>
                        --%>
                     </div>
                     <p align = "right">
                        <a href="<c:out value='${url}'/>/th/cruise/searchAgain.do"><img src="/cms-cps/greenfield/cruise/images/home.gif" alt="" border="0" class="action" /></a>
                     </p>
                  </td>
                  <!-- Navigation -->
               </tr>
            </table>
         </td>
      </tr>
   </table>
<!-- End Main Content-->
<jsp:include page="footer.jsp"></jsp:include>
</div><!-- End of pagediv -->
</body>
</html>
