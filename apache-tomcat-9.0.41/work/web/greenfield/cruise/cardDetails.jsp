<%@include file="/common/commonTagLibs.jspf"%>
<script language="JavaScript" type="text/javascript">
var currencySymbol="�";
var newHoliday = "<c:out value='${bookingInfo.newHoliday}'/>";
var errorMesage = "<c:out value='${bookingComponent.errorMessage}'/>";

var PaymentInfo = new Object();
PaymentInfo.payableAmount  = null;
PaymentInfo.partialPaymentAmount = null;
<c:if test="${(bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_PEG') && bookingComponent.accommodationSummary.prepay != 'true'}">
PaymentInfo.payableAmount  = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
PaymentInfo.partialPaymentAmount = 'true';
</c:if>
PaymentInfo.totalAmount = '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>';

PaymentInfo.calculatedPayableAmount = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
PaymentInfo.calculatedTotalAmount = '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>';

PaymentInfo.maxCardChargeAmount = null;

//Let us give default values as null
PaymentInfo.calculatedDiscount = null;
PaymentInfo.chargePercent = null;

PaymentInfo.totalCardCharge = null;

var depositAmountsMap = new Map();
 <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}">
   depositAmountsMap.put('<c:out value="${depositComponent.depositType}"/>', '<c:out value="${depositComponent.depositAmount.amount}"/>');
 </c:forEach>
 depositAmountsMap.put('fullCost', '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>');


var cardChargeMap = new Map();
cardChargeMap.put('PleaseSelect', 0);
 <c:forEach var="cardCharges" items="${bookingComponent.cardChargesMap}">
   cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');
 </c:forEach>

var bookingConstants = {FULL_COST:"fullCost", TOTAL_CLASS:"total", PAYABLEAMOUNT_CLASS:""};

</script>


<div id="paymentFields_required">
     <input type='hidden'  id='panelType' value='CNP'/>
     <c:forEach var="cardCharge" items="${bookingComponent.cardChargesMap}">
           <c:out value='<input type="hidden" id="${cardCharge.key}_charge" value="${cardCharge.value} "/>' escapeXml='false'/>
     </c:forEach>
     <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
        <c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled' name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>
        <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>
        <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>
        <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>
     </c:forEach>
</div>
<!----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<!-- Card Details -->
<div class="border">
<h6>Card Details</h6>
<p>The 'Lead Passenger' must be the passenger making the booking and
providing credit card details.</p>
<table cellpadding="0" cellspacing="0" border="0">
   <tr>
                  <td>Card Type</td>
      <td><select id="payment_type_0" onchange='handleCardSelection(0);' tabindex="199"
         name="payment_0_type" alt="a card type|Y">
         <option id="pleaseSelect" value="PleaseSelect" selected="selected">Card Type</option>
         <c:forEach var="paymentType"
            items="${bookingComponent.paymentType['CNP']}">
               <option
                  value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out
               value="${paymentType.paymentDescription}" escapeXml='false' /></option>
            </c:forEach>
         </select>&nbsp;<span class="mandatory">*</span>
       </td>
                  <td nowrap="nowrap">Valid from</td>
                  <td nowrap="nowrap"><c:set var="startMonthSelected" value="" /> <c:set
         var="startYearSelected" value="" /> <%-- VALID FROM MONTH --%> <select
         tabindex="203" id="StartMonth" name="payment_0_startMonth" alt="Valid from date|N|CARDDATE|START|StartYear">
         <%-- DEFAULT IS A BLANK FIELD. --%>
         <c:choose>
            <c:when test="${startMonthSelected == '' }">
               <option value="MM" selected="selected">MM</option>
            </c:when>
            <c:otherwise>
               <option value="">MM</option>
            </c:otherwise>
         </c:choose>
         <%-- DYNAMICALLY GENERATE MONTHS --%>
         <c:set var="validFromMonth" value="01" />

         <c:forEach begin="1" end="12" varStatus="loopStatus">
            <fmt:formatNumber var="validFromMonth" value="${loopStatus.count}"
               type="number" minIntegerDigits="2" pattern="##" />
            <c:choose>
               <c:when test="${startMonthSelected ==  validFromMonth}">
                  <option value="<c:out value="${validFromMonth}"/>"
                     selected="selected"><c:out value="${validFromMonth}" />
                  </option>
               </c:when>
               <c:otherwise>
                  <option value="<c:out value="${validFromMonth}"/>"><c:out
                     value="${validFromMonth}" /></option>
               </c:otherwise>
            </c:choose>
         </c:forEach>
      </select>&nbsp; <%-- VALID FROM YEAR --%> <c:set var="validFromYear" value="01" />
      <c:set var="validFromYearDisp" value="2001" /> <select tabindex="204"
         id="StartYear" name="payment_0_startYear">
         <%-- DEFAULT IS A BLANK FIELD. --%>
         <c:choose>
            <c:when test="${startYearSelected == '' }">
               <option value="YYYY" selected="selected">YYYY</option>
            </c:when>
            <c:otherwise>
               <option value="YYYY">YYYY</option>
            </c:otherwise>
         </c:choose>
         <%-- IF FORM BEAN HAS VALUE FOR THIS, MAKE THIS SELECTED. OTHERWISE, MAKE CURRENT YEAR SELECTED --%>
         <c:forEach var="startYear" items="${bookingInfo.startYearList}">
            <option value='<c:out value='${startYear}'/>'>20<c:out
               value='${startYear}' /></option>
         </c:forEach>
      </select></td>
   </tr>
               <tr>
                  <td nowrap="nowrap">Card Number</td>
      <td nowrap="nowrap"><input type="text" alt="card number|Y|LUHN"
      tabindex="200" id="payment_0_cardNumberId" name='payment_0_cardNumber' size="26" maxlength="20">&nbsp;<span
         class="mandatory">*</span></td>
      <td>Expiry Date</td>
      <td nowrap="nowrap"><c:set var="expiryMonthSelected" value="" />
      <c:set var="expiryYearSelected" value="" /> <%-- EXPIRY MONTH --%> <select
         tabindex="205" name="payment_0_expiryMonth" alt="expiry date for your card|Y|CARDDATE|EXPIRY|ExpiryYear|????">
         <%-- DEFAULT IS A BLANK FIELD. --%>
         <c:choose>
            <c:when test="${expiryMonthSelected == '' }">
               <option value="MM" selected="selected">MM</option>
            </c:when>
            <c:otherwise>
               <option value="MM">MM</option>
            </c:otherwise>
         </c:choose>

         <c:set var="expiryMonth" value="01" />
         <c:forEach begin="1" end="12" varStatus="loopStatus">
            <fmt:formatNumber var="expiryMonth" value="${loopStatus.count}"
               type="number" minIntegerDigits="2" pattern="##" />
            <c:choose>
               <c:when test="${expiryMonthSelected ==  expiryMonth}">
                  <option value="<c:out value="${expiryMonth}"/>"
                     selected="selected"><c:out value="${expiryMonth}" /></option>
               </c:when>
               <c:otherwise>

                  <option value="<c:out value="${expiryMonth}"/>"><c:out
                     value="${expiryMonth}" /></option>
               </c:otherwise>
            </c:choose>
         </c:forEach>
      </select>&nbsp;
         <select class='date_field_expiry_year uniform' id='ExpiryYear'
            tabindex="206" name='payment_0_expiryYear'>
                 <option value=''>YYYY</option>
                 <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
                     <option value='<c:out value='${expiryYear}'/>'>20<c:out value='${expiryYear}'/></option>
                  </c:forEach>
                  </select>&nbsp;<span class="mandatory">*</span></td>
   </tr>
               <tr>
                <td>Name on Card</td>
                  <td><input type="text" id='payment_0_nameOnCardId' name='payment_0_nameOnCard' alt="name on your card|Y|ALPHA"
                     tabindex="201" size="20" maxlength="25">&nbsp;<span
                     class="mandatory">*</span></td>
                  <td>
                  <div id="IssueNumberTitle" name='payment_0_issueNumber'style="display:none">Issue Number</div>
                  </td>
               <td>
               <div id="IssueNumber" style="display:none"><input type="text" tabindex="207"
                  alt="Issue Number|N|NUMERIC" id="IssueNumberInput" name="payment_0_issueNumber" size="3"
                  maxlength="2">
                  <input id="IssueNumberCaption"  type="hidden" alt="Please enter an issue number for your card. If you don't have an issue number, please enter the start date|-|EITHER_OR|StartMonth|IssueNumberInput" value=""/>

               </div>

               </td>
               </tr>
               <tr>
                  <td colspan="4">�</td>
               </tr>
               <tr>
                  <td colspan="3">Postcode of billing address for this card (for security purposes)</td>
                  <td>

                     <input type="text" alt="postcode for the billing address of your card|Y|POSTCODE" value="" tabindex="208" size="8" maxlength="8" id='payment_0_postCodeId' name='payment_0_postCode'/>�<span class="mandatory">*</span>


                  </td>
               </tr>
               <tr>
                  <td colspan="3">
              <span id="securityNumberText">3 digit security number (last 3 digits in the signature strip on the reverse of your card)</span></td>
                  <td>
              <input type="text" tabindex="209"
         alt="3 digit security number (the last 3 digits in the signature strip on the reverse of your card)|Y|SECURITY"
         size="3" maxlength="3" id='payment_0_securityCodeId'  name='payment_0_securityCode'>&nbsp;<span class="mandatory">*</span>
      </td>
               </tr>
               <tr>
               <td colspan="4">
                  Charges apply to credit card bookings - <a onclick="window.open('https://www.thomson.co.uk/editorial/legal/credit-card-payments.html','_blank','width=800,height=500,toolbar=yes,scrollbars=yes,resizable=yes')" href="javascript:void(0)">click here for details</a>.
               </td>
            </tr>

</table>
</div>
<!-- / Card Details -->

<!--maximum card charge and applicable charge cap -->
<input  name="cardDetailsFormBean.cardCharge" id="cardCharge" value="0.0" type="hidden"/>
<input  name="cardDetailsFormBean.cardLevyPercentage" id="cardLevyPercentage" value="0.0" type="hidden"/>

<input  name="updateFlag" id="updateFlag" value="false" type="hidden"/>

<input  type='hidden' id='totCalculatedAmount'  value='<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>'/>
<input  type='hidden' id='totamtpaidWithoutCardCharge'  name='payment_amtwithdisc' value='<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>'/>


<%-- *******Essential fields ********************  --%>

<input type="hidden" name="payment_0_chargeAmount" id="payment_0_chargeIdAmount"  value="0" />
<input type="hidden" name="payment_0_transamt"  id="payment_0_transamt"  value="0" />
<input type="hidden" name="total_transamt"  id="total_transamt"  value='NA' />

<%-- *******End Essential fields ********************  --%>
<%-- should carry payment method - Dcard etc  --%>
<input type="hidden" id="payment_0_paymentmethod" value="Dcard" name="payment_0_paymentmethod"/>

<%-- no of transaction is 1  --%>
<input type="hidden"  id="payment_0_totalTrans" value="1" name="payment_totalTrans"/>

<%-- should carry cardtype like AMERICAN_EXPRESS etc  --%>
<input type="hidden" id="payment_0_paymenttypecode" value="" name="payment_0_paymenttypecode"/>

<input type="hidden" name="token" id="token" value="${param.token}" />
<input type="hidden" name="tomcatInstance" id="tomcatInstance" value="${param.tomcat}" />

<script language="JavaScript" type="text/javascript">

   window.onload=
   function()
   {
      setToDefaultSelection();
      if(errorMesage != "")
      {
       AlertErrorsTextOnly();
      }
      if (newHoliday == "true")
      {
         initializeDepositSelection();

      }
   }
</script>