<%@include file="/common/commonTagLibs.jspf"%>

<div id="pers_details">

   <h6>Passenger Details</h6>
   <span class="personaldetails_payment_pay_img" id="confirmButton" style="display:block;">
   <a target=""
      href="javascript:updateEssentialFields(); javascript:ValidateForm(document.forms.paymentdetails)">
     <span id="confirmButton" style="display:block;">
      <img class="actionforward" border="0" title="Pay" alt="Pay" src="/cms-cps/greenfield/cruise/images/pay.gif"/>
     </span>
   </a>
   </span>

<p>Please enter passenger names to match those shown on your passports.</p>
<table cellspacing="0" cellpadding="0" border="0">
 <tbody>

<c:set var="numberOfPassengers" />
      <c:set var="passengerCount" value="0" />
   <c:forEach var="passenger" items="${bookingComponent.passengerRoomSummary}" varStatus="status">
   <tr>
       <c:set var="noOfRooms" value="${status.count}" />
       <c:if test="${noOfRooms ge 1}">
      <th colspan="6">
           <strong>Cabin&nbsp;<c:out value="${noOfRooms}" /></strong>
           &nbsp;
           <c:forEach var="rooms" items="${bookingComponent.rooms}" varStatus="room">
               <c:set var="roomCount" value="${room.count}" />
               <c:if test="${roomCount==noOfRooms}">
                  <c:out value="${rooms.roomDescription}"/>
               </c:if>
           </c:forEach>

           <br clear="all"/>
       </th>
        </c:if>
   </tr>
   <tr>
       <td width="70"/>
       <td width="55">Title</td>
       <td width="100">First Name</td>
       <td align="center" width="50">Initial</td>
       <td width="100">Surname</td>
       <td> </td>
     </tr>

      <c:forEach var="passengerRoom" items="${passenger.value}" varStatus="passen">
         <tr>
            <c:set var="numberOfPassengers" value="${passen.index}"/>
            <td>
               <c:out value="${passengerRoom.label}"/>

               <c:set var="identifier" value="passenger_${passengerCount}_identifier"/>
               <input type = "hidden" name="<c:out value='${identifier}'/>" value="<c:out value='${passengerRoom.identifier}'/>"/>
               <c:if test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR'}">
                  <c:if test = "${passengerCount == 0}">
                     <input name="leadPassenger" type="hidden"
                     value="<c:out value="${passengerRoom.identifier}"/>" />

                  </c:if>
               </c:if>
            </td>
            <td>
               <c:set var="titlekey" value="personaldetails_${passengerCount}_title"/>
               <select name="<c:out value='${titlekey}' />" tabindex="${numberOfPassengers}${passengerCount+1}" class="medium">
                  <c:choose>
                     <c:when test="${passengerRoom.ageClassification == 'INFANT' || passengerRoom.ageClassification == 'CHILD'}">
                     <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mr'}">selected</c:if>>Mr</option>
                     <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
                     <option value="Mstr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mstr'}">selected</c:if>>Mstr</option>
                     <option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mrs'}">selected</c:if>>Mrs</option>
                     </c:when>
                     <c:otherwise>
                        <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mr'}">selected</c:if>>Mr</option>
                        <option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mrs'}">selected</c:if>>Mrs</option>
                        <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
                        <option value="Ms" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Ms'}">selected</c:if>>Ms</option>
                        <option value="Dr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Dr'}">selected</c:if>>Dr</option>
                        <option value="Rev" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Rev'}">selected</c:if>>Rev</option>
                        <option value="Prof" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Prof'}">selected</c:if>>Prof</option>
                     </c:otherwise>
                  </c:choose>
               </select>
            </td>
            <!-- TODO: class for all the text boxes -->
          <td nowrap="nowrap">
               <c:set var="foreNameKey" value="personaldetails_${passengerCount}_foreName"/>
               <input type="text" tabindex="${numberOfPassengers}${passengerCount+1}" size="15"
               maxlength="15" value="<c:out value="${bookingComponent.nonPaymentData[foreNameKey]}"/>" name="<c:out value="${foreNameKey}"/>" alt="name details as shown on your passport.|Y|NAME"/>
               <span class="price">*</span>
            </td>

          <td align="center">
               <c:set var="middleNameKey" value="personaldetails_${passengerCount}_middleInitial"/>
               <input  type="text" maxlength="1"  size="2" maxlength="1" tabindex="${numberOfPassengers}${passengerCount+2}"
               value="<c:out value="${bookingComponent.nonPaymentData[middleNameKey]}"/>" name="<c:out value="${middleNameKey}"/>" alt="Initial|N|ALPHA"/>
            </td>
          <td nowrap="nowrap">
               <c:set var="lastNameKey" value="personaldetails_${passengerCount}_surName"/>
               <input  type="text" maxlength="15" size ="15" tabindex="${numberOfPassengers}${passengerCount+3}"
               id="lastname_<c:out value='${passengerCount}'/>" value="<c:out value="${bookingComponent.nonPaymentData[lastNameKey]}"/>" name="<c:out value="${lastNameKey}"/>" alt="name details as shown on your passport.|Y|NAME"/>
               <span class="price">*</span>
         </td>

            <td height="30" width="110" valign="bottom">
         <c:if test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR'}">
                  <c:if test = "${passengerCount == 0}">
                     <br/><strong>Lead Passenger</strong> (Must be 18 years or over)
                  </c:if>

         </c:if>
         </td>
         </tr>
         <c:set var="passengerCount" value="${passengerCount+1}" />
         </c:forEach>
      </c:forEach>

   </tbody>
</table>


<h5>Please provide Lead Passenger Details</h5>
<input type="hidden" value="true" name="deliveredToLeadPassenger"/>
<table cellspacing="0" cellpadding="0" border="0">
   <tbody>
   <tr>
      <td nowrap="nowrap">House Name</td>
      <td>
         <input type="text" alt="House Name|N|ADDRESS" id="HouseName"
            value="<c:out value="${bookingComponent.nonPaymentData['houseName']}"/>" tabindex="98" size="20" maxlength="20" name="houseName"/>
      </td>
      <td nowrap="nowrap">Daytime Phone No.</td>
      <td>
         <input type="text" alt="valid phone details|Y|PHONE"
            value="<c:out value="${bookingComponent.nonPaymentData['dayTimePhone']}"/>" tabindex="105" size="15" maxlength="23" name="dayTimePhone"/> <span class="mandatory">*</span>
      </td>
   </tr>
   <tr>
      <td>House No</td>
      <td><input type="text" alt="House No|N|ADDRESS" id="HouseNo" value="<c:out value="${bookingComponent.nonPaymentData['houseNo']}" />" tabindex="99" size="4" maxlength="4" name="houseNo"/></td>
      <td nowrap="nowrap">Mobile Phone No.</td>
      <td><input type="text" alt="valid phone details|N|PHONE" value="<c:out value="${bookingComponent.nonPaymentData['mobilePhone']}"/>" tabindex="106" size="15" maxlength="23" name="mobilePhone"/> </td>
      <input type="hidden" alt="Please enter a house name or number|Y|EITHER_OR|HouseName|HouseNo"/>
   </tr>
   <tr>
      <td>Address</td>
      <td nowrap="nowrap"><input type="text" alt="all required address fields|Y|ADDRESS"
         value="<c:out value="${bookingComponent.nonPaymentData['addressLine1']}"/>" tabindex="100" size="25" maxlength="25" name="addressLine1"/> <span class="mandatory">*</span> </td>
      <td> </td>
      <td> </td>
   </tr>
   <tr>
      <td> </td>
      <td><input type="text" alt="Address Line 2|N|ADDRESS" value="<c:out value="${bookingComponent.nonPaymentData['addressLine2']}"/>" tabindex="101" size="25" maxlength="25" name="addressLine2"/></td>
      <td>Email Address</td>
      <td nowrap="nowrap"><input type="text" alt="email address|Y|EMAIL" id="EmailAddress" value="<c:out value="${bookingComponent.nonPaymentData['emailAddress']}"/>" tabindex="107" size="23" maxlength="100" name="emailAddress"/> <span class="mandatory">*</span> </td>
   </tr>
   <tr>
      <td>Town/City </td>
      <td><input type="text" alt="all required address fields|Y|ADDRESS" value="<c:out value="${bookingComponent.nonPaymentData['city']}"/>" tabindex="102" size="25" maxlength="25" name="city"/> <span class="mandatory">*</span></td>
      <td valign="top">Re-enter Email Address</td>
      <td valign="top"><input type="text" alt="your email address again|Y|EMAIL" id="EmailAddress2" value="<c:out value="${bookingComponent.nonPaymentData['emailAddress1']}"/>" tabindex="108" size="23" maxlength="100" name="emailAddress1"/> <span class="mandatory">*</span></td>
      <p class="noheight">&nbsp;</p><input type="hidden" alt="The email addresses entered do not match.Please re-enter your details.|-|MATCH|EmailAddress|EmailAddress2"/>
   </tr>
   <tr>
      <td>County</td>
      <td>
         <input type="text" alt="all required address fields|N|ADDRESS" value="<c:out value="${bookingComponent.nonPaymentData['county']}"/>" tabindex="103" size="16" maxlength="16" name="county"/>
      </td>
      <td valign="top"> </td>
      <td> </td>
   </tr>
   <tr>
      <td>Post Code</td>
      <td>
         <input type="text" alt="Post Code|Y|POSTCODE" value="<c:out value="${bookingComponent.nonPaymentData['postCode']}"/>" tabindex="104" size="8" maxlength="8" name="postCode"/> <span class="mandatory">*</span>
      </td>
      <td valign="middle" align="right" colspan="2">All fields marked <span class="price">*</span> must be completed</td>
   </tr>
   </tbody>
</table>
</div>