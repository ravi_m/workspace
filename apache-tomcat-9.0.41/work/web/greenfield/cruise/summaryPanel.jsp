<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />

<!-- Price Panel -->
<jsp:include page="pricePanel.jsp"></jsp:include>
<!-- /Price Panel -->

<!-- Flights Or Ports Panel  -->
<c:choose>
	<c:when test="${bookingComponent.flightSummary.flightSelected == true}">
		<jsp:include page="flightsPanel.jsp"></jsp:include>
	</c:when>
	<c:otherwise>
		<jsp:include page="portPanel.jsp"></jsp:include>
	</c:otherwise>
</c:choose>
<!-- /Flights Or Ports Panel  -->

<!-- Cruise Panel -->
<jsp:include page="cruisePanel.jsp"></jsp:include>
<!-- /Cruise Panel -->

<table>
   <tbody><tr>
      <td>
         <p class="bookingconditions">
            <a href="javascript:void Popup('<c:out value='${bookingComponent.clientDomainURL}'/><c:out value='${bookingComponent.cruiseSummary.AToZUri}'/>',725,400,'scrollbars=yes')" align="center">A-Z Booking Information</a>
         </p>
      </td>
   </tr>
</tbody></table>

<div id="teaserPanel">
     <table height="204" width="209" cellspacing="0" cellpadding="0" border="0" style="border: 0px solid rgb(153, 153, 153); font-family: verdana; font-style: normal; font-variant: normal; font-weight: normal; font-size: 11px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; text-align: center;">
<tbody><tr>
<td/></tr>
</tbody></table>
<!-- Platypus area 1: cruise_payment_details_teaser.htm -->
      </div>