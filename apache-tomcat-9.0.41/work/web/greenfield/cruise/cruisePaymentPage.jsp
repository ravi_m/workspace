<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />

<%--***********Essential JSP settings. to be used through out the page ***********--%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="clientUrl" value='${bookingComponent.clientDomainURL}' scope="request"/>
<c:set var="currency" value="&pound;" scope="request"/>
<c:set var="isHolidayNew" value="${bookingInfo.newHoliday}" scope="session" />

<script>
 var clientDomainURL = "<c:out value='${clientUrl}'/>";
 var tomcatInstance= "<c:out value='${param.tomcat}' />";
 var token = "<c:out value='${param.token}' />";
</script>

<%--*********Essential JSP settings. to be used through out the page **************--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<version-tag:version/>
<title>Thomson Cruises - Booking Step 3 - Personal Details and Payment1</title>
<%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
%>

<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/thomson_global.css" type="text/css"/>
<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/footer_styles.css" type="text/css"/>
<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/thomson_booking.css" type="text/css"/>
<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/homepage.css" type="text/css"/>
<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/book_a_cruise_tab.css" type="text/css"/>
<link rel="stylesheet" href="/cms-cps/greenfield/cruise/css/content.css" type="text/css"/>

<script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>


<script type="text/javascript" src="/cms-cps/greenfield/common/js/paymentUpdate.js"></script>

<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/dynamic_core.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/dynamic_price.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/formvalidation.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/general.js"></script>

<!--this js needs to be included later-->
<!--<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/intellitracker.js"></script>-->

<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/menu.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/personaldetailsandpayment.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/popups.js"></script>
<script type="text/javascript" src="/cms-cps/greenfield/cruise/js/print.js"></script>


</head>

<BODY>

<div class="pagediv">
<%@include file="header.jsp"%>
<style type="text/css">
  #poh_header_container { padding: 0px 5px 0px 5px; height: 66px; margin: 0px; } /* height should not exceed 66px due to menu sizing restrictions */
  #poh_header_table p { margin: 0px; padding: 0px; }

  a.poh_headlink, a.poh_headlink:link, a.poh_headlink:active, a.poh_headlink:visited, a.poh_headlink:hover { font: normal 10px verdana,arial,sans-serif; color: #fff; }
  a.poh_headlink:hover { color: #ef0000; }

  .po_header_logo { padding-top: 10px; padding-bottom: 10px; }
  .po_header_logo img { display: block; }
  #po_header_book_section { height: 15px; border-right: 1px solid #3366cc; margin-top: 32px; background-image: url(/cms-cps/greenfield/cruise/images/icon_phone_17x13.gif); background-repeat: no-repeat; background-position: 0px 0px; }
  #po_header_book_section p { padding-left: 23px; font: bold 10px verdana,arial,sans-serif; color: #3366cc; }
  #po_header_register_section { margin-top: 32px; height: 15px; }
  #po_header_register_section p { padding-left: 15px; font: normal 10px verdana,arial,sans-serif; color: #3366cc; }
</style>
 <form name="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}'/>&tomcat=<c:out value='${param.tomcat}' />">
 <input type="hidden" name="isHolidayNew" value="<c:out value="${isHolidayNew}"/>">
  <table class="maintable" cellspacing="0" cellpadding="0" border="0">
  <tbody>
  <tr>
   <td valign="top" rowspan="2">
     <!-- Summary panel -->
      <jsp:include page="summaryPanel.jsp"></jsp:include>
   </td>
   <td height="141" valign="top" colspan="2">
     <!-- Navigator -->
   <jsp:include page="navigation.jsp"></jsp:include>
   </td>
  </tr>
  <tr>
    <td valign="top">
    <!-- Payment Panel -->
     <jsp:include page="paymentPanel.jsp"></jsp:include>
    </td>
  </tr>
</tbody>
</table>

</form>
<div id="spacerDiv" style="height: 32px;"></div>

<jsp:include page="footer.jsp"></jsp:include>
<div style="font-size: 14px; color: rgb(128, 181, 230); height: 25px;">  </div>
</div><!-- End of pagediv -->
</body>
</html>