<%@include file="/common/commonTagLibs.jspf"%>
<table cellspacing="0" cellpadding="0" border="0" class="pricepanel">
   <tbody><tr>
      <th><h2>Cruise</h2></th>
   </tr>
   <tr>
      <td>
         <table cellspacing="2" cellpadding="0" border="0">
	     <tbody><tr>
            <td width="80"><strong>Itinerary</strong></td>
                  <td>
                  <a href="javascript:popupItineraryDetails('<c:out value="${clientUrl}"/>','<c:out value="${bookingComponent.cruiseSummary.ship.resort}"/>','<c:out value="${bookingComponent.cruiseSummary.ship.shipId}"/>')"><c:out value="${bookingComponent.cruiseSummary.ship.resort}"/></a>
                  <br/><br/>
                  </td>
               </tr>
	        <tr>
               <td width="80"><strong>Ship</strong></td>
               <td><c:out value="${bookingComponent.cruiseSummary.ship.hotel}"/></td>
            </tr>
            <tr>
               <td width="80"><strong>Duration</strong></td>
               <td>
			   <c:choose>
				<c:when test="${bookingComponent.cruiseSummary.duration == 1}">
					<c:out value="${bookingComponent.cruiseSummary.duration}" /> night
				</c:when>
				<c:otherwise>
					<c:out value="${bookingComponent.cruiseSummary.duration}" /> nights
				</c:otherwise>
			</c:choose>
			   </td>
            </tr>
            <tr>
               <td><strong>Board Basis</strong></td>
               <td><c:out value="${bookingComponent.cruiseSummary.ship.boardBasis}"/></td>
            </tr>
         </tbody></table>
      </td>
   </tr>
</tbody></table>