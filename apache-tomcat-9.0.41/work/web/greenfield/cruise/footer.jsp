<%@ include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="url" value='${bookingComponent.clientDomainURL}'/>
<div id="footer_container">
  <div id="footer_strip_1">
    <div class="footer_tabular_cell"> <div class="vertical_align"><a title="Ask us a question or contact us" onclick="contactUsPop(); return false;" href="javascript:contactUsPop();">Ask us a Question / Contact us</a></div></div>
    <div class="footer_tabular_cell"><div class="vertical_align"><a title="Search for accommodation by code" href="<c:out value='${url}'/>/th/beach/invokeAccommodationSearch.do?ico=POHomeFootBrocCodeSch">Brochure code search</a> </div></div>
    <div class="footer_tabular_cell"><div class="vertical_align"><a onclick="window.open('http://tui.intelli-direct.com/e/PExit.dll?m=237&amp;p=104&amp;url=http%3a%2f%2fcz%2Emultimap%2Ecom%2fclients%2fplaces%2Ecgi%3fclient%3dtuisf_stores','name','height=745,width=820,toolbar=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no'); return false;" title="Find your nearest Thomson shop" target="name" href="http://tui.intelli-direct.com/e/PExit.dll?m=237&amp;p=104&amp;url=http%3a%2f%2fcz%2Emultimap%2Ecom%2fclients%2fplaces%2Ecgi%3fclient%3dtuisf_stores">Shop finder</a> </div></div>
    <div id="footer_cell_last" class="footer_tabular_cell"><div class="vertical_align"><a title="Receive exclusive offer information" href="<c:out value='${url}'/>/po/showContent.do?content=register.htm&amp;ICO=RegisterFooter">Register for offers</a> </div></div>
  </div>
  <div id="footer_strip_2">
    <div id="footer_rss"><a href="http://www.thomson.co.uk/po/showContent.do?content=rss.html&amp;ICO=rssfoot"><img height="15" width="32" border="0" title="Thomson RSS feed" alt="The RSS icon" src="/cms-cps/greenfield/cruise/images/rss_icon.gif"/></a></div>
    <div id="footer_navigation">
      <div id="footer_nav_container">
      <ul id="footer_menu">
      <li class="first"><a onclick="helpPagesPop(); return false;" title="Help" href="<c:out value='${url}'/>/po/showContent.do?content=help.html">Help</a></li>
        <li><a onclick="aboutUsPop(); return false;" title="Information about Thomson" href="javascript:aboutUsPop();">About Thomson</a></li>
        <li><a title="Find your way around the Thomson site" href="<c:out value='${url}'/>/po/showContent.do?content=sitemap.htm&amp;ico=POH_sitemap_footer">Sitemap</a></li>
        <li><a title="In depth information about Thomson's destinations" href="http://destinations.thomson.co.uk/">Destinations</a></li>
      </ul>
     </div>
    </div>
    <div id="footer_cards"><img height="15" width="118" title="Secure online booking with MasterCard, Visa, Delta, Switch/Maestro or Solo" alt="Accepted Credit Cards" src="/cms-cps/greenfield/cruise/images/cards.gif"/></div>
  </div>
  <div id="footer_content">
    <div id="footer_image_cluster"><a href="javascript:Popup('https://seal.verisign.com/splash?form_file=fdf/splash.fdf&amp;dn=www.thomson.co.uk&amp;lang=en',520,400);"><img height="41" width="76" title="This Web site has chosen one or more VeriSign SSL Certificate or online payment solutions to improve the security of e-commerce and other confidential communication" alt="The Verisign logo" src="/cms-cps/greenfield/cruise/images/logo_verisign.gif"/></a><a href="http://www.abta.com/scripts/verify.hts?+V5126"><img height="41" width="31" title="Verify ABTA membership - ABTA Number V5126" alt="The ABTA logo" src="/cms-cps/greenfield/cruise/images/logo_abta.gif"/></a><a href="http://www.caa.co.uk/applicationmodules/atol/atolverify.aspx?atolnumber=2524"><img height="41" width="37" border="0" title="ATOL Protected - ATOL Number 2524. Click on the ATOL Logo if you want to know more" alt="The ATOL logo" src="/cms-cps/greenfield/cruise/images/logo_atol.gif"/></a></div>
      <div id="footer_text_cluster">
         The air holiday packages shown are ATOL protected by the Civil Aviation Authority.  Our ATOL number is ATOL 2524.<br/>
         ATOL protection does not apply to all holiday and travel services shown on this website. Please see our booking conditions for more information.
      </div>
  </div>
  <div id="footer_copyright"> � 2009 TUI UK �<a onclick="termsUsPop(); return false;" title="Website and booking terms and conditions" href="javascript:termsUsPop();">Terms & Conditions</a><span id="footer_separator"> | </span><a onclick="privacyPop(); return false;" title="Thomson's privacy policy" href="javascript:privacyPop();">Privacy & Cookies Policy</a> </div>
</div>
 <!--  Intelli tracker - starts -->
   <jsp:include page="tracking.jsp"/>
  <!--  Intelli tracker - ends -->