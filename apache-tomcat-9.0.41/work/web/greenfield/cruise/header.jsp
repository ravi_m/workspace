<div class="screenonly" id="poh_header_container">
   <table width="758" cellspacing="0" cellpadding="0" border="0" id="poh_header_table">
     <tbody><tr>
      <td valign="top">
        <div class="po_header_logo"><a href="<c:out value='${clientUrl}'/>/th/cruise/getHomePageCruise.do"><img height="35" width="191" border="0" title="Thomson" alt="Thomson logo" src="/cms-cps/greenfield/cruise/images/tuithomson.gif"/></a></div>
      </td>

      <td width="200" valign="top">
         <div title="Book online or call 0871 231 5938" id="po_header_book_section">
            <p>UK Call Centre: 0871 231 5938</p>
         </div>
      </td>

      <td width="125" valign="top">
         <div title="Register to receive offers" id="po_header_register_section">
            <p><a class="poh_headlink" href="<c:out value='${url}'/>/po/showContent.do?content=register.htm&amp;ICO=CRHeader_register">Register for Offers</a></p>
         </div>
      </td>
     </tr>
   </tbody></table>
</div>