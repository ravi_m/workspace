<%@ include file="/common/commonTagLibs.jspf"%>

<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate var ="currentYear" type="date" value="${now}" pattern="yyyy"/>

<!-- Footer -->
<div id="footer">
  <div id="links">
    <a href="javascript:popChooser('/st/showContent.do?content=creditcardpaymentsandcharges')">Credit Card Fees</a>
    <a href="javascript:popChooser('/st/showContent.do?content=privacy-policy')">Privacy Policy</a>
    <a href="javascript:popChooser('/st/showContent.do?content=terms-and-conditions')">Terms &amp; Conditions</a>
    <a href="javascript:popChooser('/st/showContent.do?content=help')">Help</a>
    <a id="footerend" href="javascript:editorial('showContactUs')">Contact Us</a>
  </div>

   <div id="footerleft">
   <span>Secure online booking</span>

   <img src="/cms-cps/greenfield/simply/images/paymentcards.gif" alt="Secure on line booking with Visa, Switch, Mastercard, Visa Delta, Maestro, Solo" border="0">
   </div>

  <div id="copyright">
    <div>&copy;<c:out value="${currentYear}"/> TUI (UK) Ltd.</div>
  </div>
  <div id="footer_bottom">
 <c:choose>
	  	  <c:when test="${(bookingComponent.nonPaymentData['atol_flag']  != null) &&( bookingComponent.nonPaymentData['atol_flag']  )}">
				  <c:if test="${(bookingComponent.nonPaymentData['atol_protected'] != null ) &&(bookingComponent.nonPaymentData['atol_protected'] )}">
				  </c:if>
		  </c:when>
			  <c:otherwise>
    <p>The air holiday packages shown are ATOL protected by the Civil Aviation Authority.  Our ATOL number is ATOL 2524.<br/>
         ATOL protection does not apply to all holiday and travel services shown on this website. Please see our booking conditions for more information.
    </p>
	</c:otherwise>
	</c:choose>
	<c:choose> <c:when test="${(bookingComponent.nonPaymentData['atol_flag']  != null) &&( bookingComponent.nonPaymentData['atol_flag'] )&& (bookingComponent.nonPaymentData['atol_protected'] )}">
    <a onmouseover="imgOn('atol')" onmouseout="imgOff('atol')" href="http://www.caa.co.uk/cpg/atol/default.asp" target="_blank"><img id="footer_atol" src="/cms-cps/greenfield/simply/images/footer/atol_logo_off.gif" alt="ATOL Protected. ATOL number 2524. Click on the ATOL logo if you'd like to know more." border="0"></a>
	</c:when>  <c:when test="${(bookingComponent.nonPaymentData['atol_flag']  != null) &&( bookingComponent.nonPaymentData['atol_flag'] )&& (!bookingComponent.nonPaymentData['atol_protected'])}">  </c:when> <c:otherwise> <a onmouseover="imgOn('atol')" onmouseout="imgOff('atol')" href="http://www.caa.co.uk/cpg/atol/default.asp" target="_blank"><img id="footer_atol" src="/cms-cps/greenfield/simply/images/footer/atol_logo_off.gif" alt="ATOL Protected. ATOL number 2524. Click on the ATOL logo if you'd like to know more." border="0"></a> </c:otherwise> </c:choose>
    <a onmouseover="imgOn('abta')" onmouseout="imgOff('abta')" href="http://www.abta.com/scripts/verify.hts?+V1337" target="_blank"><img id="footer_abta" src="/cms-cps/greenfield/simply/images/footer/abta_logo_off.gif" alt="Verify ABTA membership - ABTA number V1337" border="0"></a>
    <a onmouseover="imgOn('iata')" onmouseout="imgOff('iata')"><img id="footer_iata" src="/cms-cps/greenfield/simply/images/footer/iata_logo_off.gif" alt="IATA" border="0"></a>
    <a onclick="window.open('https://seal.verisign.com/splash?form_file=fdf/splash.fdf&amp;dn=www.simplytravel.co.uk&amp;lang=en','','resizable=yes,toolbar=no,width=520,height=400');" href="#">
      <img width="96" height="48" border="0" title="This Web site has chosen one or more VeriSign SSL Certificate or online payment solutions to improve the security of e-commerce and other confidential communication" alt="The Verisign logo" src="/cms-cps/greenfield/simply/images/logo_verisign.gif"/></a>
  </div>
  <!-- / Footer -->
  <!--  Intelli tracker - starts -->
      <jsp:include page="/greenfield/simply/jspf/tracking.jsp"/>
  <!--  Intelli tracker - ends -->
<!--   Ensighten  - starts -->  
<jsp:include page="/greenfield/simply/jspf/tag.jsp"/>
<!--  Ensighten  - ends -->
  