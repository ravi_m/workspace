<%@ include file="/common/commonTagLibs.jspf"%>
<!-- Passenger Details -->
  <div class="bookingpanel">
  <c:set var="tabIndex" value="1" scope="page"/>

    <h3>Cardholder address</h3>
      <p>To help ensure your card details remain secure, please confirm the address of the cardholder. </br>If this is the same as the lead passenger address, you just need to check the box.</p>
      <!-- Passenger details -->

      <div class="bookinginput">

          <table border="0" cellpadding="0" cellspacing="0">
            <tbody>
              <tr>
                  <td nowrap="nowrap"></td>
                  <td colspan="4">
                    <input name = "autoCheckCardAddress" type="checkbox" id="autoCheckCardAddress" onclick="javascript:autoCompleteCardAddress()"/> Same address as lead passenger
                  </td>
              </tr>
              <tr>
                <td nowrap="nowrap">House Name</td>
                <td colspan="4">
                   <input name="payment_0_street_address1" maxlength="20" size="20" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/>" id="payment_0_street_address1"
                      alt="House Name|Y|ADDRESS" type="text">
                </td>

              </tr>
              <input alt="Please enter a house name or number|Y|EITHER_OR|HouseName|HouseNo" type="hidden">
              <tr>
                <td>House No.</td>
                <td><input name="cardHolderHouseNumber" id="cardHolderHouseNumber" maxlength="4" size="4" value="<c:out value="${bookingComponent.nonPaymentData['cardHolderHouseNumber']}" />" id="HouseNo" alt="House No|N|ADDRESS" type="text"></td>
              </tr>
              <tr>
                <td>Address</td>
                <td nowrap="nowrap"><input name="payment_0_street_address2" id="payment_0_street_address2" maxlength="25" size="25" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>" alt="all required address fields|Y|ADDRESS" type="text">&nbsp;<span class="mandatory">*</span>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td colspan="4"><input name="cardHolderAddressLine2" id="cardHolderAddressLine2" maxlength="25" size="25" value="<c:out value="${bookingComponent.nonPaymentData['cardHolderAddressLine2']}"/>" alt="Address Line 2|N|ADDRESS" type="text"></td>
              </tr>
              <tr>
                <td>Town/City&nbsp;</td>
                <td colspan="4"><input name="payment_0_street_address3" id="payment_0_street_address3" maxlength="25" size="25" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>" alt="all required address fields|Y|ADDRESS" type="text">&nbsp;<span class="mandatory">*</span></td>
              </tr>
              <tr>
                <td>County</td>
                <td><input name="payment_0_street_address4" id="payment_0_street_address4" maxlength="16" size="16" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address4']}"/>" alt="all required address fields|N|ADDRESS" type="text"></td>
                <td valign="top">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Post Code</td>
                <td><input name="payment_0_postCode" id="payment_0_postCode" maxlength="8" size="8" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/>" alt="Post Code|Y|POSTCODE" type="text">&nbsp;<span class="mandatory">*</span></td>
                <td colspan="2">&nbsp;</td>
                <input type="hidden" name="payment_0_selectedCountry" value="United Kingdom" />
              </tr>
            </tbody>
          </table>
        </div>
        <input alt="The email addresses entered do not match. Please re-enter your details|-|MATCH|EmailAddress|EmailAddress2" type="hidden">
      </div>
  <!-- / Passenger details -->