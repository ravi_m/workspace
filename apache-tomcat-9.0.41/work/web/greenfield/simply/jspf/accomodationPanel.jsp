<%@ include file="/common/commonTagLibs.jspf"%>
<!-- Accommodation Panel -->
<h2>Accommodation</h2>
<div class="pricepanel">
  <div>
    <p>
       <strong>
          <c:out value="${bookingComponent.accommodationSummary.accommodation.hotel}"/>
       </strong>
  </p>
  </div>
  <div>
    <p><strong>Duration:</strong>&nbsp;<c:out value="${bookingComponent.accommodationSummary.duration}"/> nights
    </p>
  </div>
  <div>
    <p><strong>Board Basis:</strong>&nbsp;<c:out value="${bookingComponent.accommodationSummary.accommodation.boardBasis}"/>
    </p>
  </div>
  <c:if test="${not empty bookingComponent.rooms }">
    <div>
      <p><strong>Room Type:&nbsp;</strong>
          <c:out value="${bookingComponent.rooms[0].roomDescription}"/>
      </p>
    </div>
  </c:if>
</div>
<p style="margin: 10px 0px 10px 10px;">
 <a align="center" href="javascript:void Popup('<c:out value='${clientUrl}'/><c:out value='${bookingComponent.accommodationSummary.AToZUri}'/>',600,450,'scrollbars=yes')">A-Z Booking Information</a>
</p>
<!-- / Accommodation Panel -->