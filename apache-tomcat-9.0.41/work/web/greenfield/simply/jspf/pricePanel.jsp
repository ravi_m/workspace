<%@ include file="/common/commonTagLibs.jspf"%>
<c:set var="currency" value="&pound;" scope="request"/>
<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" var="totalCostingLine"
                  type="number" pattern="##.##" maxFractionDigits="2" minFractionDigits="2"/>
<!-- Price -->
<h2>Price</h2>
<div class="pricepanel" id="divPricePanel">
  <c:set var="priceComponent" value="${bookingComponent.priceComponents}"/>
  <c:forEach var="index" items="${priceComponent}">
     <div>
        <p class="costingline">
    <c:choose>
    <c:when test="${fn:contains(index.itemDescription, 'Promotional')}">
        <c:out value="${currency}" escapeXml="false"/>-<fmt:formatNumber value="${index.amount.amount}"
        type="number" pattern="#,###" minIntegerDigits="1" maxFractionDigits="2" minFractionDigits="2"/>
    </c:when>
    <c:otherwise>
        <c:out value="${currency}" escapeXml="false"/><fmt:formatNumber value="${index.amount.amount}"
        type="number" pattern="#,###" minIntegerDigits="1" maxFractionDigits="2" minFractionDigits="2"/>
    </c:otherwise>
    </c:choose></p>
        <p><c:out value = "${index.itemDescription}"/>
           <c:if test="${index.quantity ne null}">
             (x<c:out value="${index.quantity}"/>)</strong>
           </c:if>
        </p>
     </div>
     <c:if test="${fn:contains(index.itemDescription, 'Saving')}">
        <div class="dividerline"></div><div></div>
     </c:if>
  </c:forEach>
  <div id="cardChargeDiv" style="display:none">
      <p escapeXml="false" class="costingline" id="cardChargeAmount">
      <c:out value="${currency}" escapeXml="false"/>0.0
    </p>
    <p class="hideIt" id="cardChargeText">Credit card charge
    </p>
  </div>
    <div id="promoDiscountDiv" style="display:none;">
          <span id="promoCodeDiscount" style="float:right;font-weight:700;">
      </span>
          <span id ="PromoDiscountText" style="display:none">Promotional Discount</span>
    </div>
  <div>
    <p class="pricebig">
  <fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}"
    type="number" var="totalCostingLineWithoutDiscount" maxFractionDigits="2" minFractionDigits="2"/>
           &pound;<span id="totalAmount" class="total"><c:out value="${totalCostingLineWithoutDiscount}"/></span></p>
    <p>
      <strong>Total Web Price</strong>
    </p>
  </div>
</div>