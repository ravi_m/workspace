<%@ include file="/common/commonTagLibs.jspf"%>
<!-- Promotional Code -->
  <div class="bookingpanel">
    <h3>Discount Code</h3>
    <p>If you have a discount code, please enter the code below and click 'Validate Code'</p>
    <table border="0" cellpadding="5" cellspacing="0">
      <tbody>
        <tr>
          <td width="380">Please enter your Promotional Code <input name="promotionalCode" maxlength="20" size="26" tabindex="109" value='<c:out value="${bookingComponent.nonPaymentData['promotionalCode']}"/>' id="promotionalCode" alt="a valid Promotional Code|N|ALPHANUMHYPHEN" type="text">
           <input type ="hidden"
            id="isPromoCodeApplied" name="isPromoCodeApplied"
            value="<c:out value="${bookingComponent.nonPaymentData['isPromoCodeApplied']}"/>"/>
            <input type ="hidden"
            id="promoDiscount" name="promoDiscount"
            value="<c:out value="${bookingComponent.nonPaymentData['promoDiscount']}"/>"/>
            </td>
          <td>
            <a href="javascript:updatePromotionalCode();"><img src="/cms-cps/greenfield/simply/images/validate_code.gif" alt="Validate Code" title="Validate Code" border="0"></a>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
<!-- /Promotional Code -->