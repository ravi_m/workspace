<%@ include file="/common/commonTagLibs.jspf"%>
<c:set var="currency" value="&pound;" scope="request"/>
<!-- Payment Amount -->
<div class="bookingpanel">
  <h3>Payment Amount</h3>
  <p>Your total holiday cost is&nbsp;
    <span class="total" id="TotalCostText"><c:out escapeXml="false" value="${currency}"/><c:out
          escapeXml="false" value="${bookingInfo.calculatedTotalAmount.amount}"/>
    </span>*
    <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}" varStatus="vardepost">
      <c:if test="${empty bookingComponent.depositComponents}">
        <td height="23" style="visibility:hidden">
        <input type="radio" tabindex="300" onclick="DynamicPriceUpdate(this.form, 'Payment')" value="FULL" />
        </td>
      </c:if>
    </c:forEach>
  </p>
  <!-- If deposit is available -->
  <script language="JavaScript" type="text/javascript">DepositAvailable=true;</script>
  <c:if test="${not empty bookingComponent.depositComponents}">
    <p>
    The deposit below is due today, but you can choose to pay the full balance if you wish.
    Please select your preferred payment amount:
    </p>
    <div class="bookinginput">
      <p>
        <c:if test="${not empty bookingComponent.depositComponents}">
          <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}"
                     varStatus="vardepost">
            <c:set var="depositType" value="${depositComponent.depositType} " scope="page"/>
            <c:set var="deposit" scope="page" value="${depositComponent.depositAmount.amount}"/>
            <fmt:formatDate value="${depositComponent.depositDueDate}" var="depositDueDate"
                            pattern="dd/MM/yyyy"/>
            <c:if test="${depositComponent.depositType == 'DEPOSIT'}">
              <input name="depositType" class="deposit" tabindex="300" value="DEPOSIT" checked="checked"
                     onclick="handleDepositSelection('DEPOSIT')" id="radioDeposit" type="radio">
              <input type="hidden" id="depositH" value="<fmt:formatNumber
                     value="${depositComponent.depositAmount.amount}"  type="number" maxFractionDigits="2"/>"/>
              Deposit:&nbsp;<span class="price"><span id="DepositCostText" name="depositAmounts" class="depositAmount"><c:out
                                  escapeXml="false" value="${currency}"/><c:out escapeXml="false" value='${deposit}'/></span></span>*
              <fmt:formatNumber value="${depositComponent.outstandingBalance.amount}" var="outstandingBalance" type="number" maxFractionDigits="2" minFractionDigits="2"/>
              (The outstanding balance of your holiday is
              <strong><span id="OutStandingBalanceText"><c:out escapeXml="false"
                    value="${currency}"/><c:out value='${outstandingBalance}'/></span></strong>**
              and is due on
              <span class="price"><c:out value='${depositDueDate}'/></span>
              )
            </c:if>
        </c:forEach>
      </c:if>
      </p>
      <p>
        <input name="depositType" class="deposit" id="radioFull" tabindex="301" value="fullCost" onclick="handleDepositSelection('fullCost')" type="radio">
        Full Balance:&nbsp;<span class="price"><span id="FullCostText" name="depositAmounts" class="depositAmount"><c:out escapeXml="false" value="${currency}"/><c:out escapeXml="false" value="${bookingInfo.calculatedTotalAmount.amount}"/></span></span>*
      </p>
    </div>
  </c:if>
  <p>
  * If you pay by credit card the charges will be updated in the price panel once you have selected your method of payment. To pay with Visa Electron please phone 0871 231 4050.
  </p>
  <p>**
  Please note if you choose to pay the outstanding balance by credit
  card, an additional credit card charge will apply at the time of
  payment.
  </p>
</div>
  <input type="hidden" id="totalHolidayCostH" name="totalHolidayCostH" value='<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>'/>
<!-- / Payment Amount -->
