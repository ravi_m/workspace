<title>Simply Travel Booking Step 3: Payment</title>

<!--flexibox-->
<link rel="stylesheet" type="text/css" href="/cms-cps/greenfield/simply/css/flexi-box/core.css" title="Thomson" media="screen" />
<!--END OF flexibox-->
<link href="/cms-cps/greenfield/simply/css/secure.css" rel="stylesheet" type="text/css" media="screen" />

<link href="/cms-cps/greenfield/simply/css/st_global.css" rel="stylesheet" type="text/css">
<link href="/cms-cps/greenfield/simply/css/st_teasers.css" rel="stylesheet" type="text/css">

<link type="image/x-icon" href="/cms-cps/greenfield/common/images/favicon.ico" rel="icon"/>
<link type="image/x-icon" href="/cms-cps/greenfield/common/images/favicon.ico" rel="shortcut icon"/>
<link href="/cms-cps/greenfield/simply/css/st_booking.css" rel="stylesheet" type="text/css">
   
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="clientUrl" value='${bookingComponent.clientDomainURL}' scope="request"/>
<c:set var="currency" scope="request">
  <c:choose>
    <c:when test='${bookingComponent.totalAmount.currency.currencyCode == "GBP"}'>&pound;</c:when>
    <c:when test='${bookingComponent.totalAmount.currency.currencyCode == "EUR"}'>&euro;</c:when>
    <c:when test='${bookingComponent.totalAmount.currency.currencyCode == "USD"}'>&#36;</c:when>
    <c:otherwise><c:out value="${bookingComponent.totalAmount.currency.currencyCode}"/></c:otherwise>
  </c:choose>
</c:set>
<c:set var="isHolidayNew" value="${bookingInfo.newHoliday}" scope="session" />
<c:set var='clientapp' value='${bookingComponent.clientApplication.clientApplicationName}'/>

<script>
   var clientDomainURL = "<c:out value='${clientUrl}'/>";
   var tomcatInstance= "<c:out value='${param.tomcat}' />";
   var token = "<c:out value='${param.token}' />";
</script>
<%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
    %>
    <script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/paymentPanelValidation.js"></script>

    <script type="text/javascript" src="/cms-cps/greenfield/common/js/paymentUpdate.js"></script>

    <script type="text/javascript" src="/cms-cps/greenfield/simply/js/personaldetailsandpayment.js"></script>
    <script type="text/javascript" src="/cms-cps/greenfield/simply/js/formValidation.js"></script>
    <script type="text/javascript" src="/cms-cps/greenfield/simply/js/general.js"></script>
    <script type="text/javascript" src="/cms-cps/greenfield/simply/js/editorial.js"></script>
    <script type="text/javascript">
      var bookingmode=true;
    </script>
    <script>
      var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
      var threeDCards = new Map();
      <c:forEach var="threeDCards" items="${bookingInfo.payDescription}">
            threeDCards.put("<c:out value="${threeDCards.key}"/>","<c:out value="${threeDCards.value}"/>");
      </c:forEach>
    </script>
