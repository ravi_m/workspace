<%@ include file="/common/commonTagLibs.jspf"%>
<!-- Flights Panel -->
<h2>Flights</h2>
<div class="pricepanel">
  <c:set var = "inboundFlight" value="${bookingComponent.flightSummary.inboundFlight}"/>
  <c:set var = "outboundFlight" value="${bookingComponent.flightSummary.outboundFlight}"/>
  <h5>Out</h5>
  <c:forEach var="index" items='${outboundFlight}'>
    <div>
      <p><strong>Depart</strong></p>
      <p><c:out value="${index.departureAirportName}"/>&nbsp;<fmt:formatDate value="${index.departureDateTime}" pattern="dd/MM/yy"/>, <fmt:formatDate value="${index.departureDateTime}" type="time" pattern="HH:mm"/></p>
    </div>
    <div>
      <p><strong>Arrive</strong></p>
      <p><c:out value="${index.arrivalAirportName}"/>&nbsp;<fmt:formatDate value="${index.arrivalDateTime}" pattern="dd/MM/yy"/>, <fmt:formatDate value="${index.arrivalDateTime}" type="time" pattern="HH:mm"/></p>
    </div>
  </c:forEach>

  <h5>Return</h5>
  <c:forEach var="index" items='${inboundFlight}'>
    <div>
      <p><strong>Depart</strong></p>
      <p><c:out value="${index.departureAirportName}"/>&nbsp;<fmt:formatDate value="${index.departureDateTime}" pattern="dd/MM/yy"/>, <fmt:formatDate value="${index.departureDateTime}" type="time" pattern="HH:mm"/></p>
    </div>
    <div>
      <p><strong>Arrive</strong></p>
      <p><c:out value="${index.arrivalAirportName}"/>&nbsp;<fmt:formatDate value="${index.arrivalDateTime}" pattern="dd/MM/yy"/>, <fmt:formatDate value="${index.arrivalDateTime}" type="time" pattern="HH:mm"/></p>
    </div>
  </c:forEach>
</div>
<!-- / Flights Panel -->