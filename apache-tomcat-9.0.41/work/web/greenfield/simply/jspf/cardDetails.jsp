<%@ include file="../../../common/commonTagLibs.jspf"%>
<script language="JavaScript" type="text/javascript">
var currencySymbol="�";
var newHoliday = "<c:out value='${bookingInfo.newHoliday}'/>";
var errorMesage = "<c:out value='${bookingComponent.errorMessage}'/>";

var PaymentInfo = new Object();
PaymentInfo.payableAmount  = null;
PaymentInfo.partialPaymentAmount = null;
<c:if test="${(bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_PEG') && bookingComponent.accommodationSummary.prepay != 'true'}">
PaymentInfo.payableAmount  = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
PaymentInfo.partialPaymentAmount = 'true';
</c:if>
PaymentInfo.totalAmount = '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>';

PaymentInfo.calculatedPayableAmount = '<c:out value="${bookingInfo.calculatedPayableAmount.amount}"/>';
PaymentInfo.calculatedTotalAmount = '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>';

PaymentInfo.maxCardChargeAmount = null;

//Let us give default values as null
PaymentInfo.calculatedDiscount = null;
PaymentInfo.chargePercent = null;

PaymentInfo.totalCardCharge = null;

var depositAmountsMap = new Map();
 <c:forEach var="depositComponent" items="${bookingComponent.depositComponents}">
   depositAmountsMap.put('<c:out value="${depositComponent.depositType}"/>', '<c:out value="${depositComponent.depositAmount.amount}"/>');
 </c:forEach>
 depositAmountsMap.put('fullCost', '<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>');


var cardChargeMap = new Map();
cardChargeMap.put('PleaseSelect', 0);
 <c:forEach var="cardCharges" items="${bookingInfo.cardChargeMap}">
   cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');
 </c:forEach>

var bookingConstants = {FULL_COST:"fullCost", TOTAL_CLASS:"total", PAYABLEAMOUNT_CLASS:"",PAY_CLASS:"pay", PAY_DESCRIPTION:"Pay"};

</script>


<div id="paymentFields_required">
     <input type='hidden'  id='panelType' value='CNP'/>
     <c:forEach var="cardCharge" items="${bookingComponent.cardChargesMap}">
           <c:out value='<input type="hidden" id="${cardCharge.key}_charge" value="${cardCharge.value} "/>' escapeXml='false'/>
     </c:forEach>
     <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
        <c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled' name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>
        <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>
        <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>
        <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>
     </c:forEach>
</div>
<!----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
  <!-- Card Details -->

  <div class="bookingpanel">
    <h3>Card Details</h3>
    <div class="bookinginput">
      <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
          <tr>
            <td width="20%">Card Type</td>
            <td>
              <select id="payment_type_0" name="payment_0_type" tabindex="199" alt="a card type|Y"
                 onchange='javascript:handleCardSelection(0);<c:if test="${bookingInfo.newHoliday}">javascript:changePayButton();</c:if>'>

                <option id="pleaseSelect" value="PleaseSelect" selected="selected">Card Type</option>
                <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
                <option
                        value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out
                         value="${paymentType.paymentDescription}" escapeXml='false' />
                 </option>
               </c:forEach>
              </select>&nbsp;<span class="mandatory">*</span>
            </td>
            <td nowrap="nowrap" width="15%">Expiry Date</td>
            <td nowrap="nowrap"><c:set var="expiryMonthSelected" value="" />
            <select name="payment_0_expiryMonth" tabindex="205"
                    id="payment_0_expiryMonthId"
                    alt="expiry date for your card|Y|CARDDATE|EXPIRY|ExpiryYear|????">
                <c:choose>
                  <c:when test="${expiryMonthSelected == '' }">
                    <option value="MM" selected="selected">MM</option>
                  </c:when>
                  <c:otherwise>
                    <option value="MM">MM</option>
                  </c:otherwise>
                </c:choose>
                <c:set var="expiryMonth" value="01" />
                <c:forEach begin="1" end="12" varStatus="loopStatus">
                  <fmt:formatNumber var="expiryMonth" value="${loopStatus.count}"
                                    type="number" minIntegerDigits="2" pattern="##" />
                  <c:choose>
                    <c:when test="${expiryMonthSelected ==  expiryMonth}">
                      <option value="<c:out value="${expiryMonth}"/>"
                              selected="selected"><c:out value="${expiryMonth}"/></option>
                    </c:when>
                    <c:otherwise>
                      <option value="<c:out value="${expiryMonth}"/>"><c:out
                              value="${expiryMonth}" /></option>
                    </c:otherwise>
                  </c:choose>
                </c:forEach>
              </select>&nbsp;
              <select name="payment_0_expiryYear" tabindex="206" id="ExpiryYear">
                    <option value="YYYY">YYYY</option>
                <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
                  <option value='<c:out value='${expiryYear}'/>'>20<c:out
                          value='${expiryYear}' /></option>
                </c:forEach>
              </select>&nbsp;<span class="mandatory">*</span>
            </td>
          </tr>
          <tr>
            <td nowrap="nowrap">Card Number</td>
            <td nowrap="nowrap">
              <input name="payment_0_cardNumber" maxlength="20" size="26" tabindex="200" value="" id="payment_0_cardNumberId" alt="card number|Y|LUHN" type="text" autocomplete="off"/>&nbsp;<span class="mandatory">*</span>
            </td>
            <td>
              <div style="display: none;"
                   name='payment_0_issueNumber' id="IssueNumberTitle">Issue Number</div>
            </td>
            <td>
              <div style="display: none;" id="IssueNumber">
                <input name="payment_0_issueNumber" maxlength="2" size="3" tabindex="207"
                       value="" id="IssueNumberInput" alt="Issue Number|N|NUMERIC" type="text" autocomplete="off"/>
                <input alt="Please enter an issue number for your card.|Y|NUMERIC" type="hidden" id="IssueNumberCaption">
              </div>
            </td>
          </tr>
          <tr>
            <td>Name on Card</td>
            <td colspan="4">
              <input name="payment_0_nameOnCard" id='payment_0_nameOnCardId' maxlength="25" size="20" tabindex="201" value="" alt="name on your card|Y|ALPHA" type="text" autocomplete="off"/>&nbsp;<span class="mandatory">*</span>
            </td>
          </tr>
          <tr>
            <td>Security code</td>
            <td colspan="4">
              <input id='payment_0_securityCodeId' name='payment_0_securityCode' maxlength="3" size="3" tabindex="209"  value="" alt="3 digit security number (the last 3 digits in the signature strip on the reverse of your card)|Y|SECURITY" type="text" autocomplete="off"/>&nbsp;<span class="mandatory">*</span>
            </td>
		</tr>
		<tr>
            <td colspan="2"><span id="securityNumberText">(last 3 digits in the signature strip on <br/> the reverse of your card)</span></td>

            <td colspan="2">
					<div id="threeDsecureimages">
					<div class="threeDlogos">
					    <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
					    <c:if test="${threeDLogos == 'mastercardgroup'}">
		    				<a href="javascript:void(0);" id="masterCardDetails" class="logosticky stickyOwner">
			<img src="/cms-cps/greenfield/common/images/th-mastercard-secure.gif" /></a>
		    			</c:if>
						<c:if test="${threeDLogos == 'visagroup'}">
    						<a style="margin-left:28px;" href="javascript:void(0);" id="visaDetails" class="logosticky stickyOwner">
			<img title="visa" src="/cms-cps/greenfield/common/images/th-visa.gif" /></a>
						</c:if>
						</c:forEach>
					</div>
			</div>
			</td>
		</tr>
		<tr>
            <td colspan="2">
                Charges apply to credit card bookings - <a href="javascript:editorial('showHelp','secure','4')">click here for details</a>.
            </td>

		   <td colspan="2">
					<div class="learnMore">
	       			<c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
				      <c:if test="${threeDLogos == 'mastercardgroup'}">
                            <a href="javascript:void(0);" id="masterCardDetails" class="sticky stickyOwner">Learn more</a>
						        <%@ include file="/common/mastercardLearnMoreSticky.jspf"%>
					  </c:if>
					  <c:if test="${threeDLogos == 'visagroup'}">
						<a style="margin-left:28px;" href="javascript:void(0);" id="visaDetails" class="sticky stickyOwner">Learn more</a>
						         <%@ include file="/common/visaLearnMoreSticky.jspf"%>
					  </c:if>
					</c:forEach>
					</div>
				</div>
	       		</td>
		</tr>
	</tbody>
      </table>
    </div>
  </div>

  <input type="hidden" id="payment_0_paymentmethod" value="Dcard" name="payment_0_paymentmethod"/>
  <input type="hidden" id="payment_0_paymenttypecode" value="" name="payment_0_paymenttypecode"/>
  <input type="hidden"  value=" " name="payment_0_issueNumber"/>
  <input type="hidden"  value=" " name="payment_0_startMonth"/>

  <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
      <input type='hidden' value=''id='${paymentType.paymentCode}_issueNo' />
 </c:forEach>

 <input  type='hidden' id='totCalculatedAmount'  value='<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>'/>
 <input  type='hidden' id='totamtpaidWithoutCardCharge'  name='payment_amtwithdisc' value='<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>'/>

 <%-- *******Essential fields ********************  --%>

 <input type="hidden" name="payment_0_chargeAmount" id="payment_0_chargeIdAmount"  value="0" />
 <input type="hidden" name="payment_0_transamt"  id="payment_0_transamt"  value="0" />
 <input type="hidden" name="total_transamt"  id="total_transamt"  value='NA' />

 <%-- *******End Essential fields ********************  --%>

<script language="JavaScript" type="text/javascript">
   window.onload=
   function()
   {
      setToDefaultSelection();
      if (newHoliday == "true")
      {
         initializeDepositSelection();
      }
	  remainingHolidayCost();
  }
   jQuery(document).ready(function()
		   {
		 	  simplyToggleOverlay();
		   });

</script>

  <!-- / Card Details -->
