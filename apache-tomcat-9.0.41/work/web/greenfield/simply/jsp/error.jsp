<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/common/commonTagLibs.jspf"%>
<%--***********Essential JSP settings. to be used through out the page ***********--%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="clientUrl" value='${bookingComponent.clientDomainURL}' scope="request"/>
<c:set var="currency" value="&pound;" scope="request"/>
<c:set var="url" value='${bookingComponent.clientDomainURL}'/>

<script>
   var clientDomainURL = "http://www.simplytravel.co.uk";
   var tomcatInstance= "<c:out value='${param.tomcat}' />";
   var token = "<c:out value='${param.token}' />";
</script>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <link href="/cms-cps/greenfield/simply/css/st_global.css" rel="stylesheet" type="text/css" />
  <link href="/cms-cps/greenfield/simply/css/st_teasers.css" rel="stylesheet" type="text/css" />

   <link type="image/x-icon" href="/cms-cps/greenfield/common/images/favicon.ico" rel="icon"/>
   <link type="image/x-icon" href="/cms-cps/greenfield/common/images/favicon.ico" rel="shortcut icon"/>


  <script type="text/javascript" language="javascript" src="/cms-cps/greenfield/simply/js/general.js" ></script>
  <script type="text/javascript" language="javascript" src="/cms-cps/greenfield/simply/js/editorial.js"></script>
  <script type="text/javascript" language="javascript" src="/cms-cps/greenfield/simply/js/st_popups.js"></script>
  <jsp:useBean id='CurrentDate1' class='java.util.Date'/>
         <fmt:formatDate var='currentYear' value='${CurrentDate1}' pattern='yyyy'/>
  <meta name="Author" content="Simply Travel" />
  <meta name="Copyright" content="Copyright (c) <c:out value="${currentYear}" /> TUI UK" />
  <meta name="Rating" content="General" />
  <meta name="Revisit-after" content="2 Days" /><style type="text/css">
   a.\33 60_Images {
      background: transparent url('/cms-cps/common/images/misc/360image.gif') no-repeat center left;
      padding: 5px 5px 5px 20px;
   }
 </style>
  <script type="text/javascript">
      var bookingmode=true;
  </script>
  <title>Simply Travel System Error</title>
    <link rel="stylesheet" href="/cms-cps/greenfield/simply/css/st_global.css" type="text/css">
    <link rel="stylesheet" href="/cms-cps/greenfield/simply/css/st_search.css" type="text/css">
  <style>
   #headerbottom
   {
      background: #fff url(/cms-cps/greenfield/simply/images/header_bottom_waiting.gif) no-repeat fixed top left !important;
   }
</style>
    <script>
      var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
    </script>
  </head>
  <body>

  <!-- Header -->
<style>
#offertext {
    font:bold 11px arial,helvetica,sans-serif;
    color:#E7E7FB;
    float:right;
    text-align:right;
    margin:20px 20px 0px 0px;
    letter-spacing:0px;
}
/* Header */
#header {
    color: #fff;
    width:770px;
    margin:0px auto;
    background: #2B0097 url(/cms-cps/greenfield/simply/images/header_top.gif) no-repeat scroll top right;
    padding:0px !important;
}
#headerwrap {
 float:right;
 width:450px;
 margin:0px;
 text-align:left;
}
#headerwrap table {
  background-color:#F3F3FD;
    text-align:right;
    margin:8px 10px 0px 0px;
  float:right;
  width:100%;
}
#headerwrap table td {
  background-color:#A3A5E5;
  vertical-align:middle;
}
#headerwrap table td a {
    font: bold 11px arial,helvetica,sans-serif;
    text-decoration:none;
    color:#fff;
    width:100%;
    height:18px;
    padding:3px 2px;
}
#headerwrap table td a:hover {
    color:#2B0097;
}
#brandlogo {
 display:block;
}
#brandlogopopup {
 display:none
}
</style>
    <div id="header">
  <div id="headerwrap">
    <p id="offertext">Extra savings when you book online</p>
    <table cellspacing="1" cellpadding="0" border="0" id="headermenu">
      <tbody><tr>
       <td id="home"><a href="http://www.simplytravel.co.uk/st/getHomePageSun.do">Home</a></td>
       <td id="destinations"><a href="http://www.simplytravel.co.uk/st/sun/viewDestinations.do">Our Destinations</a></td>
       <td id="specialoffers"><a href="http://www.simplytravel.co.uk/st/showSpecialOffers.do">Offers</a></td>
       <td id="about"><a href="javascript:editorialSimply('showAbout','http://www.simplytravel.co.uk')">About Us</a></td>
       <td id="brochure"><a href="http://www.simplytravel.co.uk/st/showContent.do?content=brochurerequest&amp;popup=false">Brochure Request</a></td>
     <td id="property"><a href="http://www.simplytravel.co.uk/st/showContent.do?content=villasearch&amp;popup=false">Property Search</a></td>
      </tr>
    </tbody></table>
  </div>
  <img border="0" alt="" src="/cms-cps/greenfield/simply/images/stlogo.gif" id="brandlogopopup"/>
  <a href="http://www.simplytravel.co.uk/st/getHomePageSun.do"><img border="0" alt="" src="/cms-cps/greenfield/simply/images/stlogo.gif" id="brandlogo"/></a>
   <img border="0" alt="" src="/cms-cps/greenfield/simply/images/text/hand_picked_properties.gif" id="brandimg"/>
  <div id="headerbottom"> </div>
</div>

    <table border="0" cellspacing="0" cellpadding="0" class="waiting_error_panel" align="center">
   <tr>
      <td><img src="/cms-cps/greenfield/simply/images/woman_with_sun_hat.jpg" alt="" style="margin:5px"/></td>
      <td valign="top">
			<c:choose>
				<c:when test="${not empty bookingInfo.trackingData.sessionId}">
					<div>
						A problem has occurred whilst processing your enquiry. Please try again, or contact our booking team on 0870 405 5005.
					</div>
				</c:when>
				<c:otherwise>
					<div>
						Your enquiry information has been removed due to an extended period of inactivity. Please begin your search again.
					</div>
				</c:otherwise>
			</c:choose>
   <!-- Navigation -->
      <p>
         <a href="http://www.simplytravel.co.uk/st/getHomePageSun.do"><img src="/cms-cps/greenfield/simply/images/home_button.gif" alt="" border="0" class="actionforward" /></a>
      </p>
   <!-- / Navigation -->
      </td>
   </tr>
</table>
    <jsp:include page="/greenfield/simply/jspf/errorFooter.jsp"/>
  </body>
</html>