<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<%--***********Essential JSP settings. to be used through out the page ***********--%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="clientUrl" value='${bookingComponent.clientDomainURL}' scope="request"/>
<c:set var="currency" scope="request">
  <c:choose>
    <c:when test='${bookingComponent.totalAmount.currency.currencyCode == "GBP"}'>&pound;</c:when>
    <c:when test='${bookingComponent.totalAmount.currency.currencyCode == "EUR"}'>&euro;</c:when>
    <c:when test='${bookingComponent.totalAmount.currency.currencyCode == "USD"}'>&#36;</c:when>
    <c:otherwise><c:out value="${bookingComponent.totalAmount.currency.currencyCode}"/></c:otherwise>
  </c:choose>
</c:set>
<c:set var="isHolidayNew" value="${bookingInfo.newHoliday}" scope="session" />
<c:set var='clientapp' value='${bookingComponent.clientApplication.clientApplicationName}'/>

<script>
   var clientDomainURL = "<c:out value='${clientUrl}'/>";
   var tomcatInstance= "<c:out value='${param.tomcat}' />";
   var token = "<c:out value='${param.token}' />";
</script>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
    <version-tag:version/>
    <%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
    %>
    <link href="/cms-cps/greenfield/simply/css/st_booking.css" rel="stylesheet" type="text/css">
    <link href="/cms-cps/greenfield/simply/css/st_global.css" rel="stylesheet" type="text/css">
    <link href="/cms-cps/greenfield/simply/css/st_teasers.css" rel="stylesheet" type="text/css">

   <link type="image/x-icon" href="/cms-cps/greenfield/common/images/favicon.ico" rel="icon"/>
   <link type="image/x-icon" href="/cms-cps/greenfield/common/images/favicon.ico" rel="shortcut icon"/>

	<script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/paymentPanelValidation.js"></script>

    <script type="text/javascript" src="/cms-cps/greenfield/common/js/paymentUpdate.js"></script>

    <script type="text/javascript" src="/cms-cps/greenfield/simply/js/personaldetailsandpayment.js"></script>
    <script type="text/javascript" src="/cms-cps/greenfield/simply/js/formValidation.js"></script>
    <script type="text/javascript" src="/cms-cps/greenfield/simply/js/general.js"></script>
    <script type="text/javascript" src="/cms-cps/greenfield/simply/js/editorial.js"></script>
    <script type="text/javascript">
      var bookingmode=true;
    </script>
    <meta name="Author" content="Simply Travel">
    <meta name="Copyright" content="Copyright (c) 2004 TUI UK">
    <meta name="Rating" content="General">
    <meta name="Revisit-after" content="2 Days">
    <title>Simply Travel Booking Step 3: Payment</title>

    <script>
      var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
      var threeDCards = new Map();
      <c:forEach var="threeDCards" items="${bookingInfo.payDescription}">
            threeDCards.put("<c:out value="${threeDCards.key}"/>","<c:out value="${threeDCards.value}"/>");
      </c:forEach>
    function autoCompleteCardAddress()
    {
        if ($("autoCheckCardAddress").checked)
        {
            $("payment_0_street_address1").value=$("HouseName").value;
            $("cardHolderHouseNumber").value=$("HouseNo").value;
            $("payment_0_street_address2").value=$("AddressLine1").value;
            $("cardHolderAddressLine2").value=$("AddressLine2").value;
            $("payment_0_street_address3").value=$("City").value;
            $("payment_0_street_address4").value=$("County").value;
            $("payment_0_postCode").value=$("PostCode").value;
        }
    }
    </script>
  </head>
  <body>
    <%@include file="/greenfield/simply/sprocket/header.jsp"%>
    <form name="paymentdetails" method="post" action="processPayment?b=24000&token=<c:out value='${param.token}'/>&tomcat=<c:out value='${param.tomcat}' />">
    <input type="hidden" name="isHolidayNew" value="<c:out value="${isHolidayNew}"/>">
    <input type="hidden" name="payment_0_selectedCountryCode" value="GB">
    <c:set var="visiturl" value="yes" scope="session" />
    <!-- Main -->
    <div id="main">
      <div id="container">
        <div id="content">
          <jsp:include page="summaryPanel.jsp"/>
          <jsp:include page="paymentPanel.jsp"/>
        </div> <!-- / main -->
      </div> <!-- / content -->
    </div> <!-- / container -->
    </form>
    <jsp:include page="/greenfield/simply/jspf/footer.jsp"/>
  </body>
</html>