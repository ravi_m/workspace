<%@ include file="/common/commonTagLibs.jspf"%>
<div id="main_column"><!-- Sub-Header -->
  <h2 id="telnumber">
    <div style="margin:16px 0 2px 3px;">
        Tel: 0871 231 4050
        <br clear="all"/>
        <span id="callCost">Calls Cost 10p per minute plus network extras.</span>
    </div>
  </h2>
  <h2 id="pagetitle">Customer Details &amp; Payment</h2>
  <!-- / Sub-Header -->
   <jsp:include page="../sprocket/breadCrumb.jsp"/>
  <div
  style="margin: 0px; float: left; display: inline; vertical-align: middle; padding-top: 10px;"
  id="mandatorytext">All fields marked <span class="mandatory">*</span>
  must be completed</div>
  <div style="margin: -5px; float: right; display: inline;">
    <!-- <a class="actionforward" target=""
    href="javascript:updateEssentialFields();javascript:ValidateForm(document.forms.paymentdetails);"><span
       id="confirmButton" style="display:block;">
      <img src="/cms-cps/greenfield/simply/images/pay_button.gif" alt="Pay"
           title="Pay" class="actionforward" border="0"></span>
    </a> -->
  </div>

  <div id="pricediff1" class="pageinfotext" <c:if test="${not empty bookingComponent.errorMessage}">style="color:#3366CC;"</c:if><c:if test="${empty bookingComponent.errorMessage}"> style="display:none" </c:if>>
         <c:out value="${bookingComponent.errorMessage}"/>
  </div>

  <jsp:include page="/greenfield/simply/jspf/passengerDetails.jsp" />
  <jsp:include page="/greenfield/simply/jspf/discountCode.jsp"/>
  <jsp:include page="/greenfield/simply/jspf/cardDetails.jsp" />
  <jsp:include page="/greenfield/simply/jspf/cardHolderAddress.jsp" />
  <jsp:include page="/greenfield/simply/jspf/paymentAmount.jsp" />
  <jsp:include page="/greenfield/simply/jspf/termsAndCondition.jsp" />
  <!-- CR Mondial Insurance -->
  <div class="actions">
  <c:if test="${bookingInfo.newHoliday}">
      <span id="confirmButton" style="margin-bottom: -23px;margin-left: -5px;margin-right: -5px; float: right; display: inline;">       
       <a class="actionforward" target=""
      href="javascript:updateEssentialFields();javascript:ValidateForm(document.forms.paymentdetails);"><img
  src="/cms-cps/greenfield/simply/images/pay_button.gif" alt="Pay"
   				title="Pay" border="0"></a>       
      </span>
  </c:if>  
  <a href="javascript:backButtonHandle('sun/book/BackToExtraOptionsInsurance')"><img
  src="/cms-cps/greenfield/simply/images/back_button.gif" alt="Back"
  title="Back" border="0"></a></div>
</div>
<!-- Main Column -->