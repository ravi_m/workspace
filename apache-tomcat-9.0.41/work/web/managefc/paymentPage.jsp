<%@ page import="com.tui.uk.config.ConfReader"%>
<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" id="firstchoice" class="ie6 "> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" id="firstchoice" class="ie7 "> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" id="firstchoice" class="ie8 "> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" id="firstchoice" class="ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="" id="firstchoice"> <!--<![endif]-->

<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request" />
<fmt:formatNumber value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}"
 var="amount" type="number" pattern="####.##" maxFractionDigits="0" minFractionDigits="0" />
 
 <fmt:formatNumber var="amountOutstanding"
    value="${bookingComponent.payableAmount.amount-bookingInfo.calculatedDiscount.amount}"
    type="number" maxFractionDigits="0" minFractionDigits="0"
    pattern="#####.##" />
    
                <c:if test="${not empty bookingComponent.flightSummary.outboundFlight}">
                    <c:forEach var="outboundFlight"
                        items="${bookingComponent.flightSummary.outboundFlight}">
                        <c:if test="${not empty outboundFlight}">
                            <c:set var="DepDate"
                                value="${outboundFlight.departureDateTime}" />
                            
                        </c:if>
                    </c:forEach>
               
                </c:if>
				
<fmt:formatDate value='${DepDate}' pattern='dd' var="onlyDate" />
<c:set var="strDepDate" value="${onlyDate}"/>
<fmt:formatDate value='${DepDate}' pattern='MM/yyyy' var="onlyMonthYear" />
<c:set var="strDepMonthYear" value="${onlyMonthYear}"/>

<html>
<head>
<link href="/cms-cps/managefc/css/main.css" rel="stylesheet"
    type="text/css" />
<link href="/cms-cps/managefc/css/main_header.css" rel="stylesheet"
    type="text/css" />
<link href="/cms-cps/managefc/css/print.css" rel="stylesheet"
    type="text/css" media="print" />
<link href="/cms-cps/managefc/css/ac-payment.css" rel="stylesheet"
    type="text/css" />
<link rel="icon" type="image/png" href="/cms-cps/managefc/images/favicon.png" />

<!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="/cms-cps/managefc/css/ie8-and-down.css" />
<![endif]-->
<script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
<script type="text/javascript"
    src="/cms-cps/common/js/commonAjaxCalls.js"></script>
<script type="text/javascript"
    src="/cms-cps/common/js/functions_toggle.js"></script>
<script type="text/javascript"
    src="/cms-cps/common/js/paymentPanelContent.js"></script>
<script type="text/javascript"
    src="/cms-cps/common/js/paymentPanelValidation.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
<script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
<script type="text/javascript"
    src="/cms-cps/firstchoice/hugo/js/prototype.js"></script>
<script type="text/javascript"
    src="/cms-cps/firstchoice/hugo/js/functions_checkout_payment_details.js"></script>
<script type="text/javascript"
    src="/cms-cps/managefc/js/summary_panel.js"></script>


    <script src="//nexus.ensighten.com/tui/Bootstrap.js"></script>
    <script type="text/javascript">
            var ensLinkTrack = function(){};
    </script>
</head>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag"%>
<version-tag:version />
<title>First Choice | Payment Details</title>
<!--#endif-->
        <!--[if IE 7 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /><![endif]-->
        <!--[if IE 8 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /><![endif]-->
        <!--[if IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" /><![endif]-->
        <!--[if (gt IE 9)|!(IE)]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><!--<![endif]-->
        <meta charset="utf-8" />

<c:set var='clientapp'
    value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName} ' />
<%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         clientApp = (clientApp!=null)?clientApp.trim():clientApp;
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
         String cvvEnabled = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".CV2AVS.Enabled", "false");
         pageContext.setAttribute("cvvEnabled", cvvEnabled, PageContext.REQUEST_SCOPE);
         String isExcursion =
            com.tui.uk.config.ConfReader.getConfEntry(clientApp+".summaryPanelWithExcursionTickets", "false");
         pageContext.setAttribute("isExcursion", isExcursion, PageContext.REQUEST_SCOPE);
      %>
	 		 <%
			 String getHomePageURL =(String)ConfReader.getConfEntry("managefc.homepage.url","");
				  pageContext.setAttribute("getHomePageURL",getHomePageURL);
				%>
			<c:choose>
					 <c:when test="${not empty bookingComponent.breadCrumbTrail['HOME']}">
					 <c:set var='homePageURL' value="${bookingComponent.breadCrumbTrail['HOME']}" />
				      </c:when>
				     <c:otherwise>
				  	  <c:set var='homePageURL' value='${getHomePageURL}'/>
				     </c:otherwise>
				</c:choose>
	  
<script type="text/javascript" src="/cms-cps/managefc/js/functions_managefc_paymentPage.js"></script>
<script type="text/javascript" src="/cms-cps/managefc/js/functions_manualAddress.js"></script>
<script type="text/javascript" language="javascript">
     //seconds
    var session_timeout= ${bookingInfo.bookingComponent.sessionTimeOut};
    var IDLE_TIMEOUT = (${bookingInfo.bookingComponent.sessionTimeOut})-(${bookingInfo.bookingComponent.sessionTimeAlert});
    var _idleSecondsCounter = 0;
    var IDLE_TIMEOUT_millisecond = IDLE_TIMEOUT *1000;
    var url="${bookingComponent.breadCrumbTrail['HOTEL']}";
    //var homePageUrl="${bookingComponent.breadCrumbTrail['HOME']}";

	
    var sessionTimeOutFlag =  false;
    
    window.addEventListener('pageshow', function(event) {
    	if(navigator.userAgent.indexOf("MSIE") == -1){
    		jQuery(document).ready(function()
            {
                    var d = new Date();
                    d = d.getTime();
                    if (jQuery('#reloadValue').val().length == 0)
                    {
                            jQuery('#reloadValue').val(d);
                            
                    }
                    else
                    {
                            jQuery('#reloadValue').val('');
    			location.reload();
    						
    						
                    }
            });
    		}
    		else{
    		if(event.currentTarget.performance.navigation.type == 2)
    		{
    			 location.reload();
    		}
    		}
	});

    function noback(){
        window.history.forward();
        $('FieldExpiryDateMonth').selectedIndex = 0;
        $('FieldExpiryDateYear').selectedIndex  = 0;
        document.getElementById("tandc-info").checked=false;
    }

    window.setInterval(CheckIdleTime, 1000);
    window.alertMsg();
    function alertMsg(){

    var myvar = setTimeout(function(){
    document.getElementById("sessionTime").style.visibility = "visible";
    document.getElementById("modal").style.visibility = "visible";
    document.getElementById("sessionTimeTextbox").style.visibility = "visible";

    var count = Math.round((session_timeout - _idleSecondsCounter)/60);
    document.getElementById("sessiontimeDisplay").innerHTML = count +"mins";
    },IDLE_TIMEOUT_millisecond);

    }

    function CheckIdleTime() {
    _idleSecondsCounter++;

    if (  _idleSecondsCounter >= session_timeout) {

        if (window.sessionTimeOutFlag == false){
        document.getElementById("sessionTime").style.visibility = "hidden";
        document.getElementById("modal").style.visibility = "hidden";
        document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
        document.getElementById("sessionTimeout").style.visibility = "visible";
        document.getElementById("modal").style.visibility = "visible";
        document.getElementById("sessionTimeOutTextbox").style.visibility = "visible";
        }else{
            document.getElementById("sessionTimeout").style.visibility = "hidden";
            document.getElementById("modal").style.visibility = "visible";
            document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";


        }
       //navigate to technical difficulty page

        }
    }
    function closesessionTime() {

        document.getElementById("sessionTime").style.visibility = "hidden";
        document.getElementById("modal").style.visibility = "hidden";
        document.getElementById("sessionTimeTextbox").style.visibility = "hidden";

    }
    function reloadPage(){
        window.sessionTimeOutFlag= true;
        document.getElementById("sessionTimeout").style.visibility = "hidden";
        document.getElementById("modal").style.visibility = "hidden";
        document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";
        window.noback();
        window.location.replace(window.url);
        return(false);

    }
    function homepage()
    {
       /* url="https://www.firstchoice.co.uk" */
		url="${homePageURL}";
        window.noback();

        window.location.replace(url);
    }
    function activestate()
    {

        document.getElementById("sessionTime").style.visibility = "hidden";
        document.getElementById("modal").style.visibility = "hidden";
        document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
        window.ajaxForCounterReset();
        window.alertMsg();
        window._idleSecondsCounter = 0;
    }
    function ajaxForCounterReset() {
        var token = "<c:out value='${param.token}' />";
        var url = "/cps/SessionTimeOutServlet?tomcat=<c:out value='${param.tomcat}'/>";
        var request = new Ajax.Request(url, {
            method : "POST"
        });
        window._idleSecondsCounter = 0;

    }

    function newPopup(url) {
        var win = window.open(url,"","width=700,height=600,scrollbars=yes,resizable=yes");
        if (win && win.focus) win.focus();
    }


    function makePayment() { 
        //document.getElementById('errorMessageToolTipCustom').style.display = "none";
		payValFlag =false; 
        if( document.getElementById("errorMsg") ){
            document.getElementById("errorMsg").style.display = "none";
        }
        var isvalid = true,
        cardValid = false;
        if(allnumeric('cardNumber') && validateCVV('cvv') && allCharacters('cardholderName') && getMonth('FieldExpiryDateMonth') && validateOtheramt(document.getElementById("part")) || validateISSUE('issuenum') ){
            cardValid = true;
        }
        

        
        if(checkBox()){
            isvalid = isValid();
        }
        if(!isvalid || !cardValid){
            //document.getElementById('errorMessageToolTipCustom').style.display = "block";
			payValFlag = true;
        }
        var paymentForm = $('CheckoutPaymentDetailsForm');
        var element = paymentForm.serialize();
        elementstring = element
                + "&token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />";
        var url = "/cps/processPostPayment?token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}'/>";
        elementstring = elementstring.replace("&title=&", "&title=" + leadTitle + "&");
        elementstring = elementstring.replace("payment_0_type=&", "");
        elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
        elementstring = elementstring.replace("payment_0_type=PleaseSelect&", "");
        if(isvalid && cardValid){
            document.getElementById("modal").style.visibility = "visible";
            var request = new Ajax.Request(url, {
                method : "POST",
                parameters : elementstring,
                onSuccess : showThreeDOverlay
            });
        }
		else{
			payValFlag = true;		
		}
    }
    

    function isValid(){
    var firstName =  checkFName("firstName"),
        title = checkTitle("styled-select"),
        surName = checkName("surName"),
        houseName = housenameCheck("houseDetails1"),
        street1 = streetline1Check("StreetLine1"),
        street2 = streetline2Check("Streetline2"),
        town = townorcityCheck("townorcity"),
        county = testCounty("county"),
        postCode = testPostCode("postalCode");

     if(!firstName || !title || !surName || !houseName || !street1 || !street2 || !town || !county || !postCode){
        return false;
     }
     return true;
    }
</script>
<body onload="setDefaultDepositOption();">
    <%@include file="manageFCConfigSettings.jspf"%>
    <div id="wrapper" class="amend-cancel">
        <jsp:include page="sprocket/header.jsp" />
        <div><style type="text/css">#mboxImported-default-F_Destinations_PromoStrip-0 {clear:both;}</style></div>
        <div class="mboxDefault">&nbsp;</div>
        <!-- <script type="text/javascript">
            mboxCreate(FC_PromoStrip");
        </script> -->
         <%@include file="breadCrumb.jspf"%>
        <form id="CheckoutPaymentDetailsForm"
            name="CheckoutPaymentDetailsForm" method="post"
            action="javascript:makePayment();" novalidate>
            <c:if test="${not empty bookingComponent.errorMessage}">
                    <c:if test="${fn:containsIgnoreCase(bookingComponent.errorMessage,'Whilst')}">
                        <div id="errorMsg">
                            <div class="alert-infosection">
                                <div class="info-section pax-name">
                                    <span class="fix_header">The price of your hoilday has changed</span><a class="closeLink fr" onclick="closeAlertMsg('errorMsg');">x</a> <br>
                                    <c:out value="${bookingComponent.errorMessage}"
                                        escapeXml="false" />
                                </div>
                            </div>
                            <br />
                        </div>
                    </c:if>
            </c:if>
            <div class="content-section">
                <c:if test="${not empty bookingComponent.errorMessage}">
                    <c:if test="${!fn:containsIgnoreCase(bookingComponent.errorMessage,'Whilst')}">
                        <div id="errorMsgBlock" class="newWarningBlock">
                            <div class="newWarningBlockInner">
                                <div class="error-notify info-section clear padding10px mb20px">
                                    <p>
                                        <strong><c:out
                                                value="${bookingComponent.errorMessage}" escapeXml="false" /></strong>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </c:if>
                <div id="errorMsg"></div>
                <%@include file="timeoutInfo.jspf"%>

                <div class="mask-interactivity"></div>

                <%@include file="importantInformation.jspf"%>

                <!--<%@include file="promotionCode.jspf"%>-->

                <%@include file="passengerDetails.jspf"%>

                <%@include file="paymentDetails.jspf"%>

                <%@include file="cardHolderAddress.jspf"%>

                <div class="clear"></div>

                <%@include file="termsAndCondition.jspf"%>
               
                <div class="clear"></div>
                <div class="final"></div>
                <div class="clear"></div>
                <div class="make-payment bookHoliday" >
                 <div class="buttonToolTip tooltipCustom fl  disNone" id="errorMessageToolTipCustom">
                        <h3 class="error-message-text">Hold on there...</h3>
                        <h5 class="error-message-subText">some details still haven't been entered, or are currently invalid</h5>
                    <span class="arrow"></span>
                </div>
                    <c:if test="${bookingInfo.newHoliday == true}">
                    <a href="${bookingComponent.prePaymentUrl}">&lt; Back to booking overview</a>
                    <input type="submit" id="submitPaymentButton" value ="Make a payment" class="button cta jumbo" onClick="return validateRequiredField();"/> 
                    </c:if>
                </div>
            </div>
        </form>
        <script type="text/javascript">
		  document.getElementById("CheckoutPaymentDetailsForm").reset();
		</script>
        <%@include file="summaryPanel.jspf"%>

    </div>
    <jsp:include page="sprocket/footer.jsp" />
    <div id="overlay" class="posFix"></div>
        <form novalidate name="postPaymentForm" method="post">
        <input type="hidden" name="emptyForm" class="disNone"></input>
    </form>
    	<script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.page.pageUid = "paymentPage";
				 tui.analytics.page.SrcBkgRef = "${bookingComponent.bookingData.bookingReference}";
				 tui.analytics.page.SrcBkgSys = "ATCOM";
				 tui.analytics.page.DepDate = "${strDepDate}";
				 tui.analytics.page.MonthYear = "${strDepMonthYear}";
				 tui.analytics.page.Amount	 = "${amount}";
				 tui.analytics.page.AmountOutstanding = "${amountOutstanding}";
  	 </script>
</body>
</html>