<!-- <!doctype html> -->
<!--[if lt IE 7 ]> <html lang="en" id="firstchoice" class="ie6 "> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" id="firstchoice" class="ie7 "> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" id="firstchoice" class="ie8 "> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" id="firstchoice" class="ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="" id="firstchoice"> <!--<![endif]-->
<!-- <head>
	<title></title> -->
	<!--[if IE 7 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /><![endif]-->
	<!--[if IE 8 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /><![endif]-->
	<!--[if IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" /><![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><!--<![endif]-->
	<!-- <meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1"> -->


	<link rel="icon" type="image/png" href="/cms-cps/managefc/images/favicon.png" />

		<!--[if lte IE 9]>
			<link rel="shortcut icon" href="/cms-cps/managefc/images/favicon.ico" type="image/x-icon" />
			<link rel="icon" href="/cms-cps/managefc/images/favicon.ico" />
			<![endif]-->

			<!-- HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->

		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
			<!-- javascript -->
			<!-- <script type="text/javascript"  src="../js/TuiConfigProd.js"></script>
			<script type="text/javascript" src="../js/Bootstrap.js"></script>
			<link href="../css/main.css" rel="stylesheet" type="text/css" />
			<link href="../css/main_header.css" rel="stylesheet" type="text/css" />
			<link href="../css/print.css" rel="stylesheet" type="text/css" media="print" /> -->
			<script>
			var ensLinkTrack = function(){};
			function displayHolidayTypes(ele){			
				var holiday_type = new Array();				
				holiday_type[0]="holidaytypes";
				holiday_type[1]="longhaul";
				holiday_type[2]="shorthaul";
				holiday_type[3]="allinclusive";				
				var get_holiday_type_length=holiday_type.length;				
				for(var i=0;i<get_holiday_type_length;i++){					
					document.getElementById(holiday_type[i]).style.display="none";
					document.getElementById(holiday_type[i]+"_li").className="";
				}					
				var split_type=ele.id.split("_");	
				document.getElementById(split_type[0]).style.display="block";	
				ele.className="active";
			}
			</script>
		<!-- </head>
		<body> -->
			<div id="wrapper">

				<div id="inner-footer">
					<ul id="footer-utils">
						<li>
							<h3>Holiday extras</h3>
							<ul>
								<li><a target="_blank" href="http://excursion.firstchoice.co.uk/gbp/" class="ensLinkTrack">Excursions</a></li>
								<li><a target="_blank" href="http://www.carhiremarket.com/firstchoice/" class="ensLinkTrack">Car hire</a></li>
								<li><a target="_blank" href="http://www.firstchoice.co.uk/information/holiday-extras/before-you-go/foreign-exchange/" class="ensLinkTrack">Foreign Exchange</a></li>
								<li><a target="_blank" href="http://www.holidayextras.co.uk/firstchoice/hotels-and-parking.html" class="ensLinkTrack">Airport parking</a></li>
								<li><a target="_blank" href="http://www.firstchoice.co.uk/information/holiday-extras/before-you-go/foreign-exchange/travel-money-card/" class="ensLinkTrack">Money Card</a></li>
								<li><a target="_blank" href="http://www.firstchoiceins.co.uk/travel/default.aspx" class="ensLinkTrack">Travel insurance</a></li>
							</ul>
						</li>
						<li>
							<h3>Find a local store</h3>
							<p><a target="_blank" href="http://www.firstchoice.co.uk/about-us/travel-shops/" class="ensLinkTrack">Shop Finder</a></p>
						</li>

						<li id="safe-hands">
							<h3>You're in safe hands</h3>
							<p>We're part of TUI Group - one of the world's leading travel companies. And all of our holidays are designed to help you Discover Your Smile.</p>
							<p class="authority">
								<a target="_blank" class="abta ensLinkTrack" href="http://www.abta.com/find-a-holiday/member-search/5736">ABTA</a>
								<a target="_blank" class="atol ensLinkTrack" href="http://www.caa.co.uk/default.aspx?catid=27">ATOL</a>
							</p>
						</li>
						<li id="questions">
							<h3>Have a question?</h3>
							<form action="https://www.firstchoice.co.uk/gsa/search" method="get">
								<div class="formrow">
									<textarea  name="q" class="textfield" placeholder="e.g. Where do I print my e-tickets?"></textarea>
									<!-- <input type="hidden" name="site" value="firstchoice_collection" />
									<input type="hidden" name="client" value="fc-hugo-main" />
									<input type="hidden" name="proxystylesheet" value="fc-hugo-main" />
									<input type="hidden" name="output" value="xml_no_dtd" />
									<input type="hidden" name="submit" value="Go" /> -->
									
									    <input type="hidden" value="default_collection" name="site">
							            <input type="hidden" value="production_frontend" name="client">
							            <input type="hidden" value="production_frontend" name="proxystylesheet">
							            <input type="hidden" value="xml_no_dtd" name="output">
							            <input type="hidden" value="Go" name="submit">
            
								</div>
								<div class="floater">
									<button class="button fr mt4 small">Search</button>
									<!-- <p class="help fl"><a target="_blank" href="http://www.firstchoice.co.uk/holiday/faqCategories" class="ensLinkTrack">Ask a question</a></p> -->
									<p class="contact-us fl"><a target="_blank" href="http://www.firstchoice.co.uk/contact-us" class="ensLinkTrack">Contact us</a></p>
								</div>
							</form>
						</li>
					</ul>
					<!-- <script type="text/javascript">
					dojoConfig.addModuleName("tui/widget/Tabs");
					</script> -->

					<div id="footer-seo">
						<div class="tabs-container">
							<ul class="tabs">
								<li onclick="displayHolidayTypes(this);" class="active" id="holidaytypes_li"><a target="_blank"  class="ensLinkTrack">Holiday Types</a><span class="arrow"></span></li>
								<li onclick="displayHolidayTypes(this);" id="longhaul_li"><a  target="_blank" class="ensLinkTrack">Mid/Long haul</a><span class="arrow"></span></li>
								<li onclick="displayHolidayTypes(this);" id="shorthaul_li"><a target="_blank"  id="ensLinkTrack">Short haul</a><span class="arrow"></span></li>
								<li onclick="displayHolidayTypes(this);" id="allinclusive_li"><a target="_blank"  id="ensLinkTrack">All inclusive</a><span class="arrow"></span></li>
							</ul>
							<div id="holidaytypes" class="menu" style="display:block">
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/deals/summer-2016-deal" class="ensLinkTrack" data-componentId="WF_COM_200-3">Summer holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/sun-holidays/winter-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Winter holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/product/holiday-village-FHV" class="ensLinkTrack" data-componentId="WF_COM_200-3">Holiday Villages</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/sun-holidays/all-inclusive-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">All inclusive holidays</a></li>
						         </ul>
						         <ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/sun-holidays/family-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Family holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/sun-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Sun holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cheap holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/last-minute-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Last minute holidays</a></li>
						         </ul>
						         <ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/sun-holidays/adult-holidays/beach-escapes/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Beach escapes</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk" class="ensLinkTrack" data-componentId="WF_COM_200-3">Package holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/last-minute-deals/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Late deals</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/sun-holidays/premier-holidays/premier-luxury/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Luxury holidays</a></li>
						         </ul>
							</div>
							<div id="longhaul" class="menu" style="display:none">
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Egypt-EGY" class="ensLinkTrack" data-componentId="WF_COM_200-3">Egypt holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Goa-001915" class="ensLinkTrack" data-componentId="WF_COM_200-3">Goa holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Mauritius-MUS" class="ensLinkTrack" data-componentId="WF_COM_200-3">Mauritius holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Kenya-KEN" class="ensLinkTrack" data-componentId="WF_COM_200-3">Kenya holidays</a></li>
						         </ul>
						         <ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Dominican-Republic-DOM" class="ensLinkTrack" data-componentId="WF_COM_200-3">Dominican Republic holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Cape-Verde-CPV" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cape Verde Islands holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Jamaica-JAM" class="ensLinkTrack" data-componentId="WF_COM_200-3">Jamaica holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Thailand-THA" class="ensLinkTrack" data-componentId="WF_COM_200-3">Thailand holidays</a></li>
						         </ul>
						         <ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Aruba-ABW" class="ensLinkTrack" data-componentId="WF_COM_200-3">Aruba holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Turkey-TUR" class="ensLinkTrack" data-componentId="WF_COM_200-3">Turkey holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Mexico-MEX" class="ensLinkTrack" data-componentId="WF_COM_200-3">Mexico</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Barbados-BRB" class="ensLinkTrack" data-componentId="WF_COM_200-3">Barbados holidays</a></li>
						         </ul>
							</div>
							<div id="shorthaul" class="menu" style="display:none">
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Tunisia-TUN" class="ensLinkTrack" data-componentId="WF_COM_200-3">Tunisia holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Cyprus-CYP" class="ensLinkTrack" data-componentId="WF_COM_200-3">Cyprus holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Malta-MLT" class="ensLinkTrack" data-componentId="WF_COM_200-3">Malta holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Morocco-MAR" class="ensLinkTrack" data-componentId="WF_COM_200-3">Morocco holidays</a></li>
						         </ul>
						         <ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Bulgaria-BGR" class="ensLinkTrack" data-componentId="WF_COM_200-3">Bulgaria holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Spain-ESP" class="ensLinkTrack" data-componentId="WF_COM_200-3">Spain holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Italy-ITA" class="ensLinkTrack" data-componentId="WF_COM_200-3">Italy holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Croatia-HRV" class="ensLinkTrack" data-componentId="WF_COM_200-3">Croatia holidays</a></li>
						         </ul>
						         <ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Greece-GRC" class="ensLinkTrack" data-componentId="WF_COM_200-3">Greece holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Crete-000800" class="ensLinkTrack" data-componentId="WF_COM_200-3">Crete holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Rhodes-000923" class="ensLinkTrack" data-componentId="WF_COM_200-3">Rhodes holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Corfu-000691" class="ensLinkTrack" data-componentId="WF_COM_200-3">Corfu holidays</a></li>
						         </ul>
							</div>
							<div id="allinclusive" class="menu" style="display:none">
								<ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/tenerife-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Tenerife holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/egypt-red-sea-holidays/sharm-el-sheikh/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Sharm el Sheikh holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/ibiza-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Ibiza holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/costa-del-sol-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Costa Del Sol holidays</a></li>
						         </ul>
						         <ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/zante-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Zante holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/gran-canaria-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Gran Canaria holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holiday/location/overview/Kefalonia-000864" class="ensLinkTrack" data-componentId="WF_COM_200-3">Kefalonia holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/costa-blanca-holidays/benidorm/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Benidorm holidays</a></li>
						         </ul>
						         <ul>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/lanzarote-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Lanzarote holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/majorca-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Majorca holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/destinations/canary-island-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Canary islands holidays</a></li>
						            <li><a target="_blank" href="http://www.firstchoice.co.uk/holidays/algarve-holidays/" class="ensLinkTrack" data-componentId="WF_COM_200-3">Algarve holidays</a></li>
						         </ul>
							</div>
						</div>
					</div></div>
				</div>
<!-- 			</div> -->
			<div id="footer">
				<ul>
					<li>&copy; <script> document.write((new Date()).getFullYear()) </script> <a target="_blank" href="http://www.tuitravelplc.com/" class="ensLinkTrack">TUI Travel plc</a></li>
					<li><a target="_blank" href="http://www.firstchoice.co.uk/our-policies/terms-of-use/" class="ensLinkTrack">Terms &amp; Conditions</a></li>
					<li><a target="_blank" href="http://www.firstchoice.co.uk/our-policies/accessibility/" class="ensLinkTrack">Accessibility</a></li>
					<li><a target="_blank" href="http://www.firstchoice.co.uk/our-policies/privacy-policy/" class="ensLinkTrack">Privacy Policy</a></li>
					<li><a target="_blank" href="http://www.firstchoice.co.uk/our-policies/statement-on-cookies/" class="ensLinkTrack">Statement on Cookies</a></li>
					<li><a target="_blank" href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges/" class="ensLinkTrack" rel="author" target="_blank">Credit Card Fees</a></li>
				</ul>
				<p style="text-align: left;"><span style="font-size: 8.5pt; font-family: TUIType, sans-serif; color: rgb(140, 140, 140);">Many of the flights and flight-inclusive holidays on this website are financially protected by the ATOL scheme. But ATOL protection does not apply to all holiday and travel services listed on this website. Please ask us to confirm what protection may apply to your booking. If you do not receive an ATOL Certificate then the booking will not be ATOL protected. If you do receive an ATOL Certificate but all the parts of your trip are not listed on it, those parts will not be ATOL protected. Please see our booking conditions for information or for more information about financial protection and the ATOL Certificate go to:<span class="apple-converted-space">&nbsp;</span></span><span style="font-family: TUIType, sans-serif;"><a target="_blank" href="http://www.atol.org.uk/ATOLCertificate"><span style="font-size: 8.5pt; color: rgb(102, 102, 102);">www.atol.org.uk/ATOLCertificate</span></a></span>&nbsp;</p>

<%-- 			<c:set var="nonWebAnalyticsData">Payment_NameOnCard_ToolTip,Payment_SecurityCode_ToolTip,Alt_Plus1Day_ToolTip,SpecialNeeds,PrivacyPolicy,WorldCare_ToolTip</c:set>
 --%>
 
 		<!-- 	<script type="text/javascript">
				 var tui = {};
				 tui.analytics = {};
				 tui.analytics.page = {};
				 tui.analytics.sessionID = "${bookingInfo.bookingComponent.bookingSessionIdentifier}";
				 tui.analytics.page.pageUid = "paymentPage";
				 <c:forEach var="analyticsDataEntry" items="${bookingInfo.bookingComponent.nonPaymentData}" varStatus="status">
					 <c:if test="${not fn:contains(nonWebAnalyticsData, analyticsDataEntry.key)}">
					 tui.analytics.page.${analyticsDataEntry.key} = "${analyticsDataEntry.value}";
				    </c:if>
			    </c:forEach>
				</script> -->
			</div>
	<!-- 	</body>
		</html> -->