<div class="summaryAccord open">
	<h3 class="summaryAccordSwitch"><span class="icon cruise"></span><span class="title">CRUISE</span></h3>
	<div class="summaryAccordCont">
		<div class="">
			<div class="copy">
				<div class="cruise-name">Ancient Wonders</div>
				<div class="ship-name">Thomson Spirit</div>
				<div class="cruise-duration">14 nights </div>
			</div>
			<ul class="item-list">						
				<li>
					<span class="fl">Inside cabin</span>
					<span class="fr">Included</span>
					<div class="clear"></div>
				</li>
				<li>
					<span class="fl">Deck 3 |&nbsp;4</span>
					<div class="clear"></div>
				</li>
				<li>
					<span class="fl">Full Board</span>
					<span class="fr">Included</span>
					<div class="clear"></div>
				</li>
			</ul>
		</div>
    
	</div>
</div>