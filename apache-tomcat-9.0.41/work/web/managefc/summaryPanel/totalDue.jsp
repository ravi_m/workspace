<c:if test="${not empty bookingComponent.pricingDetails}">
  <c:set var="dueDate" value="${bookingComponent.pricingDetails['DUE_DATE'][0].itemDescription}" />
  <c:set var="totalPaidDate" value="${bookingComponent.totalPaidTillDate.amount}"/>
  <c:set var="fullAmount"
    value="${bookingComponent.payableAmount.amount-bookingInfo.calculatedDiscount.amount}"/>	
<c:set var="totalcost" value="${fn:split(fullAmount, '.')}" />
<c:set var="totalcost_dec" value="${totalcost[1]}"/>
<c:if test="${empty totalcost_dec}">
<c:set var="totalcost_dec" value="00" />
</c:if>
<c:set var="lowDepositcost_dec" value="00" />

<div class="clear"></div>
<div class="stickyWrapper">
    <div class="due-accord">
        <div class="no-accord">
            <c:set var="priceBreakDown" value="${bookingComponent.pricingDetails['priceBreakDown']}" />
            <c:if test="${not empty priceBreakDown}">
                <div class="">
                    <div class="total-pay btm-dashed">
                        <span class="fl">TOTAL PAID TO DATE</span>
                        
                         <span id="total_paid_link" text="Includes any credit card charges paid" 
						 class="ac-ques-tooltip-link" 
						 onmouseover="totalprice_displayTooltipInfo_mouseover_custom('#total_paid_link','#total_paid_amt');" 
						 onmouseout="totalprice_displayTooltipInfo_mouseout_custom('#total_paid_amt');"></span> 
						 <div class="tooltip_visible_custom disNone fl" id="total_paid_amt" style="display:none;"> 	 	
							<ul class="item-list"> <li> 	 	
							<span>Includes any credit card charges paid</span></li></ul> 	 	
							<span class="arrow_price" style="left: 89px; top: 35px; display: block;"></span> </div> 
							
                        <span class="fr">&pound;  <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" 
                                            value="${totalPaidDate}" /></span>
                        <div class="clear"></div>
                        
                        
                    </div>
                    
					<div class="total-due">
                        <span class="lable">PAYMENTS DUE</span>
	</div>

        <div class="summaryAccordCont dashed">
            <div class="">			
                <ul class="item-list">

				<c:set var="zeroValue">0</c:set>
						<c:set var="lowDepositFlag">${zeroValue}</c:set>	
											
				<c:forEach var="depositComponent" items="${bookingComponent.depositComponents}" >
                    <c:set var="CONST_LOW_DEPOSIT">lowDeposit</c:set>
						  <c:set var="CONST_DEPOSIT">deposit</c:set>		
					<c:set var="depositType" value="${depositComponent.depositType}"
						scope="page" />
					<c:set var="deposit" value="${depositComponent.depositAmount.amount}"
						scope="page" />					
					<c:set var="outstandingBalance" value="${depositComponent.outstandingBalance.amount}"
						scope="page" />
					<fmt:formatDate var="depositDueDate"
                      value="${depositComponent.depositDueDate}" pattern="EE dd MMM yyyy" />          		
		           <c:if test="${depositType == CONST_LOW_DEPOSIT}">
		               <li>
                          <span class="fl">
									<span class="lable balck bold">
									<c:set var="lowDepositFlag">1</c:set>	
									<fmt:formatDate var="depositDueDate"
									value="${depositComponent.depositDueDate}" pattern="EE dd MMM yyyy" />
									${depositDueDate}
								</span>
						 </span>
                         <span class="fr balck bold" id="">
                                <span class="poundSize balck">&pound;</span>${outstandingBalance}.${lowDepositcost_dec}                              
                         </span> <div class="clear"></div>                          
                        </li>
                    </c:if>					
					<c:if test="${depositType == CONST_DEPOSIT}">              
		               <li>
                            <span class="fl balck">
							 <c:set var="lowDepositFlag">1</c:set>	
								<fmt:formatDate var="depositDueDate"
								value="${depositComponent.depositDueDate}" pattern="EE dd MMM yyyy" />
									${depositDueDate}
							</span>
							<span class="fr balck" id="">
							
							
							<c:set var="outstandingBalancecost" value="${fn:split(outstandingBalance, '.')}" />
							<c:set var="outstandingBalance_dec" value="${outstandingBalancecost[1]}"/>
							<c:if test="${empty outstandingBalance_dec}">
								<c:set var="outstandingBalance_dec" value="00" />
							</c:if>
														
							<c:choose>
							<c:when test ="${outstandingBalance_dec!='00'}">
								<span class="poundSize balck">&pound;</span>${outstandingBalance}
							</c:when>
							<c:otherwise>
								<span class="poundSize balck">&pound;</span>${outstandingBalance}.${outstandingBalance_dec}
							</c:otherwise>
							</c:choose>
							</span><div class="clear"></div>
		               </li>
		     		</c:if>
			    	
					
				<c:if test="${lowDepositFlag == '0'}">
						<li class="pad-botom">
                          <span class="fl">
								<span class="lable balck">
									${dueDate}
								</span>
						 </span>
						 
                         <span class="fr balck" id="">
                                <span class="poundSize balck">&pound;</span> ${totalcost[0]} .${totalcost_dec}
                         </span> <div class="clear"></div>                          
                        </li>
					</c:if>
						
				</c:forEach>
				
                </ul>			
            </div>
        </div>
		<div class="summaryAccordCont pad-total">					
			<span class="totalOutstanding fl balck">TOTAL OUTSTANDING
			 <span id="total_paid" text="This is the amount that's left to pay" 
						 class="ac-ques-tooltip-link" 
						 onmouseover="totalprice_displayTooltipInfo_mouseover_custom('#total_paid','#total_payment_due');" 
						 onmouseout="totalprice_displayTooltipInfo_mouseout_custom('#total_payment_due');"></span> 
						 <div class="tooltip_visible_custom disNone fl" id="total_payment_due" style="display:none;"> 	 	
							<ul class="item-list"> <li> 	 	
							<span>This is the amount that's left to pay</span></li></ul> 	 	
							<span class="arrow_price"></span> </div> 

							
			</span>
            <span class="fr">
			<span class="poundSize balck">&pound;</span>
			<span class="number balck"><c:out value="${totalcost[0]}"/></span>.<span class="decimal balck"><c:out value="${totalcost_dec}"/></span>							
			</span>
            <div class="clear"></div>			
		</div>
                        
                </div>
            </c:if>
        </div>
        <div class="grandTotalBG"></div>
    </div>
</div>
</c:if>
 