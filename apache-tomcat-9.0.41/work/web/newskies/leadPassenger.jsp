<h4>Lead passenger</h4>
<p class="leadNote"></p>
<ul class="formFormatTable">
   <%-----------------------Set the variables adultCount.For lead passenger the label and insurance text are set.--------------%>

    <c:set var="adultCount" value="${bookingComponent.nonPaymentData['passenger_ADT_count']}" scope="page"/>
    <c:set var="childrenCount" value="${bookingComponent.nonPaymentData['passenger_CHD_count']}" scope="page"/>
    <c:set var="infantCount" value="${bookingComponent.nonPaymentData['passenger_INF_count']}" scope="page" />
    <c:set var="totalnumberOfPassengers" value="${bookingComponent.nonPaymentData['passenger_total_count']}" scope="page" />

 <%-----------------------End of the logic for setting different variables and lead passenger details.--------------------------------------%>

 <%-----------------------Setting the script variable noOfPassengers------------------------------------%>
<script type="text/javascript">
Personaldetails.setNoOfPassengers('<c:out value="${totalnumberOfPassengers}"/>');
</script>

    <li id="leadPassenger" class="foreName_0Error surName_0Error passengerTitle0Error">
	 <label class="personalLabel"><div class="adult">Adult 1</div></label>
	 <div class="add_input">
	  <div class="formCol2 styleSelect">
 <c:set var="passengerCount" value="0" scope="request"/>
	     <c:set var="titlekey" value="passenger_${passengerCount}_title"/>
	     <select type="nonblank" id="passengerTitle0" name="passenger_0_title" gfv_required="required" alt="Lead Passenger Title" class="initials" style="width: 85px; margin-top: -3px; padding-top: 8px;">
	        <option value="" class="lead_title">Title</option>
	        <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mr'}">selected</c:if>>Mr</option>
   			<option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mrs'}">selected</c:if>>Mrs</option>
			<option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
			<option value="Ms" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Ms'}">selected</c:if>>Ms</option>
			<option value="Dr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Dr'}">selected</c:if>>Dr</option>
			<option value="Rev" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Rev'}">selected</c:if>>Rev</option>
			<option value="Prof" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Prof'}">selected</c:if>>Prof</option>
	     </select>

	  </div>
	 </div> 
	  <div class="formCol3">
	  <c:set var="foreNameKey" value="passenger_${passengerCount}_foreName"/>
	   <div class="add_input" style="margin-top:-5px;"><input type="alpha" id="foreName_0" name="<c:out value='${foreNameKey}'/>" value="<c:out value='${bookingComponent.nonPaymentData[foreNameKey]}'/>" class="textfield" gfv_required="required" maxlength="15" alt="Lead Passenger First name" placeholder="First name"/></div>
	  </div>
	  <div class="formCol4">
	   <c:set var="lastNameKey" value="passenger_${passengerCount}_lastName"/>
	    <div class="add_input"><input type="alpha" id="surName_0" name="<c:out value='${lastNameKey}'/>" value="<c:out value='${bookingComponent.nonPaymentData[lastNameKey]}'/>" class="textfield" gfv_required="required" maxlength="15" alt="Lead Passenger Surname" placeholder="Surname"/></div>
	  </div>

   </li>
</ul>
<div class="bottomImage"></div>
<c:set var="passengerCount" value="${passengerCount+1}"/>