<fieldset>
   <legend>Payment details</legend>
   <ul class="bgrWhite">
      <li class="pay_det  floatContainer">
	     <%@include file="cardDetails.jsp" %>
      </li>
	  <li class="holder_det">
         <%@include file="cardHolderAddressDetails.jsp" %>
      </li>
   </ul>
</fieldset>