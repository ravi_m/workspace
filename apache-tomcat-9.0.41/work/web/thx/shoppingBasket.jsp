<fmt:formatNumber var="totalAmount"
	value="${bookingInfo.calculatedTotalAmount.amount}" type="number"
	maxFractionDigits="2" minFractionDigits="2" groupingUsed="false" pattern="#,##,###.##" />
<div id="shopping_basket">
	<p>
	<div id="basket_image">
		<img src="/cms-cps/thx/images/basket_image.gif">
	</div>
	<div id="total">
		<b>Total Cost: <c:out value="${currencySymbol}" escapeXml="false" />
			<c:out value="${totalAmount}" />
		</b>
	</div>
	<c:set var="priceComponentLength"
		value="${fn:length(bookingComponent.priceComponents)}" />
	<c:if test="${priceComponentLength > 0}">

		<c:forEach var="priceComponent"
			items="${bookingComponent.priceComponents}">
			<a><c:out value="${priceComponent.itemDescription}"
					escapeXml="false" /> </a>
			<br />
			<div id="date">
			<b>Date:&nbsp;</b>
			<fmt:formatDate value="${priceComponent.excursionDate}" pattern="EEE dd MMM yyyy" />
			</div>		
				<div class="underlined"></div>	
		</c:forEach>
	</c:if>
	
</div>