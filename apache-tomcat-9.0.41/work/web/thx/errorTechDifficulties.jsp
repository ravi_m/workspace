 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <link rel="stylesheet" type="text/css" href="/cms-cps/thx/css/tech_difficult.css"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
    String techdifficultieshome = com.tui.uk.config.ConfReader.getConfEntry("thx.home" , "");
    pageContext.setAttribute("techdifficultieshome", techdifficultieshome, PageContext.REQUEST_SCOPE);
    %>
<html>
<head>
<title></title>

<%@include file="common.jspf"%>

</head>
<body>
	<div class="thxwrapper">
		<%@include file="errorHeader.jsp"%>

		<div class="Error500_sorryHeader">
			<p class="Error500_sorryText">
				We're really sorry,<br>we're having some technical problems
			</p>
		</div>
		<div class="Error500_Reason">
			Try refreshing the page or <a
				id="Error500_Link2" href="<c:out value='${techdifficultieshome}'/>">go back to the one you were just
				on.</a>
			<p class="Error500_GiveCall">... or give us a call on 0871 231 4289</p>
		</div>

		<div class="Error500_ImageDiv">
			<img class="Error500Img" src="/cms-cps/thx/images/TH-bookFlow-errors-500.png">
		</div>

		<%@include file="footer.jsp"%>
			</div>
</body>
</html>