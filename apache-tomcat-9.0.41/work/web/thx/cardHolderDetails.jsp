<li><label for="checkbox">Check box if the same as personal details</label>
	<input type="checkbox" value="det" name="checkbox" id="useAddress" onclick="AddressPopulationHandler.handle()">
	
</li>

<!-- <li class="personal-details"><label for="title">Title</label> <select
	class="styled" name="title" id="title"
	style="width: 66px; height: 27px;">
		<option value="1" selected="selected">Mr</option>
		<option value="2">Mrs</option>
		<option value="3">Miss</option>
</select></li>

<li><label for="firstname">First name </label> <input type="text"
	value="" class="textfield mandatory "
	data-validation-engine="validate[required, maxSize[50]]"
	name="firstname" id="firstname">
</li>

<li><label for="initial">Initial</label> <input type="text"
	value="" class="textfield xsmall" name="initial" id="initial">
</li>

<li><label for="surname">Surname </label> <input type="text"
	value="" class="textfield xlarge mandatory "
	data-validation-engine="validate[required, maxSize[50]]" name="surname"
	id="surname">
</li> -->

<li>
  <label for="housename">House Name/No. *</label> 
  <input type="text"  class="textfield mandatory "	name="payment_0_street_address1" id="cardHouseName" 
  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/>"
		maxlength="20" alt="House Name" data-validation-engine="validate[required, maxSize[20]]"/>
</li>

<li>
  <label for="address">Address *</label>
  <input type="text" class="textfield xxlarge mandatory "  name="payment_0_street_address2" id="cardAddress1" 
  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>"
		maxlength="25" alt="Address" data-validation-engine="validate[required,maxSize[25]], custom[address]]"/>
</li>


<li>
  <label for="address"></label>
  <input type="text" class="textfield xxlarge mandatory "  name="payment_0_cardHolderAddress2" id="cardAddress2" 
  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_cardHolderAddress2']}"/>"
		maxlength="25" data-validation-engine="validate[maxSize[25]],custom[address]]"/>
</li>

<li>
  <label for="town">Town / City *</label> 
  <input type="text"  class="textfield mandatory "  name="payment_0_street_address3" id="cardTownCity" 
  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>"
		maxlength="25" alt="Town/City" data-validation-engine="validate[required,maxSize[25]], custom[town]]"/>
</li>

<li>
  <label for="county">Country *</label> 
  <input type="text"  class="textfield mandatory "  name="payment_0_selectedCountry" id="cardCounty" 
  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_selectedCountry']}"/>"
		maxlength="50" alt="Country" data-validation-engine="validate[required, maxSize[50]],custom[county]]"/>
</li>

<li>
  <label for="postcode">Postcode *</label> 
  <input type="text"  class="textfield mandatory " name="payment_0_postCode" id="cardPostCode" 
  value="<c:out value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/>"
		maxlength="8" alt="Postcode" data-validation-engine="validate[required,maxSize[8]], custom[postcode]]"/>
</li>

<br />
</ul>

	</div>
</div>