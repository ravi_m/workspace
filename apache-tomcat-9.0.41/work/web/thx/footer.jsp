 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
 <%
 String footerlink = com.tui.uk.config.ConfReader.getConfEntry("thx.footerlink" , "");
 pageContext.setAttribute("footerlink", footerlink, PageContext.REQUEST_SCOPE);
%> 
<c:if test="${footerlink== 'true'}">
<div id="outer-footer">
<%
    String home = com.tui.uk.config.ConfReader.getConfEntry("thx.home" , "");
    pageContext.setAttribute("home", home, PageContext.REQUEST_SCOPE);
	String destinations = com.tui.uk.config.ConfReader.getConfEntry("thx.destinations" , "");
    pageContext.setAttribute("destinations", destinations, PageContext.REQUEST_SCOPE);
	String contactus = com.tui.uk.config.ConfReader.getConfEntry("thx.contactus" , "");
    pageContext.setAttribute("contactus", contactus, PageContext.REQUEST_SCOPE);
	String faq = com.tui.uk.config.ConfReader.getConfEntry("thx.faq" , "");
    pageContext.setAttribute("faq", faq, PageContext.REQUEST_SCOPE);
	
    %>
	
	<ul id="footer-links-bar">
		<li><a target="_blank" href="${home}">Home</a></li>
		<li><a target="_blank" href="${destinations}">Destinations</a></li>
		<li><a target="_blank" href="${contactus}">Contact us</a></li>
		<li><a target="_blank" href="${faq}">FAQ's</a></li>
	</ul>
	
</div>
</c:if>	

</div>
<div class="clearboth"></div>
<div id="outside-footer">
	<ul>
		<li>&copy; 2016 <a target="_blank" href="http://www.tuitravelplc.com/">TUI
				Travel plc</a></li>
		<li><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html">Terms
				&amp; Conditions</a></li>
		<li><a target="_blank" href="http://tuiuk-thomson.test.tuitravel-ad.com/gbp/our-policies/accessibility.html">Accessibility</a></li>
		<li><a target="_blank" href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html">Privacy Policy</a></li>
		<li class="last">
		<c:choose>
			<c:when test="${applyCreditCardSurcharge eq 'true'}">
		<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">CreditCard Fees</a>
			</c:when>
			<c:otherwise>
			<a target="_blank" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">Ways to Pay</a>
			</c:otherwise>
			</c:choose>	
				</li>
	</ul>
</div>