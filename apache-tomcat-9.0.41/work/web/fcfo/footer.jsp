<div class="destfooter" style="width: 540px;"></div>
<div id="Footer">
    <%--<a id="footer" name="footer"></a>--%>
    <div class="cols">
        <h6>
            <a rel="nofollow" href="http://www.firstchoice.co.uk/help/">Help</a>
        </h6>
        <ul>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/help/booking-online/">Booking online</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/help/before-you-go/">Before you go</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/help/amending-or-cancelling-your-booking/">Amending or cancelling</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/help/flights-and-flying-information/">Flights and flying</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/help/health-concerns/">Health concerns</a>
            </li>
        </ul>
    </div>
    <div class="cols">
        <span class="notitle"> </span>
        <ul>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/help/at-the-airport/">At the airport</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/help/on-holiday/">On holiday</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/help/luggage/">Luggage</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/help/travelling-with-children/">Travelling with children</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/help/on-your-return/">On your return</a>
            </li>
        </ul>
    </div>
    <div class="cols">
        <h6>
            <a rel="nofollow" href="http://www.firstchoice.co.uk/information/why-book-with-us/">Why book with us?</a>
        </h6>
        <ul>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/information/why-book-with-us/holidays-for-adults/">Best for adults</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/information/why-book-with-us/holidays-for-families/">Best for children</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/information/why-book-with-us/extra-legroom/">More legroom</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/information/why-book-with-us/holidays-forever/index.html">Holidays Forever</a>
            </li>
        </ul>
    </div>
    <div class="cols">
        <h6>
            <a href="http://www.firstchoice.co.uk/information/holiday-extras/">Holiday extras</a>
        </h6>
        <ul>
            <li>
                <a href="http://www.firstchoice.co.uk/information/holiday-extras/before-you-go/holiday-insurance/">Holiday Insurance</a>
            </li>
            <li>
                <a href="http://www.firstchoice.co.uk/information/holiday-extras/before-you-go/foreign-exchange/">Foreign exchange</a>
            </li>
            <li>
                <a href="http://www.firstchoice.co.uk/information/holiday-extras/during-your-flight/premium-upgrades-longhaul/">Premium upgrades</a>
            </li>
            <li>
                <a href="http://www.firstchoice.co.uk/information/holiday-extras/during-your-holiday/car-hire/">Car hire</a>
            </li>
            <li>
                <a href="http://www.firstchoice.co.uk/information/holiday-extras/during-your-holiday/excursions/">Excursions</a>
            </li>
        </ul>
    </div>
    <div class="cols">
        <ul class="helpFooterLast">
            <li>
            <a href="http://www.firstchoice.co.uk/sun-holidays/">Sun holidays</a>
            </li>
            <li>
            <a href="http://www.firstchoice-ski.co.uk/">Ski holidays</a>
            </li>
            <li>
            <a href="http://www.firstchoice.co.uk/last-minute-deals/">Last-minute deals</a>
            </li>
            <li>
            <a href="http://www.firstchoice.co.uk/flights/">Flights</a>
            </li>
            <li>
            <a href="http://www.firstchoice.co.uk/destinations/">Holiday destinations</a>
            </li>
        </ul>
    </div>
<!--    <div id="atol_abta">
            <a rel="nofollow" title="ABTA" href="http://www.abta.com/">
                <img class="foot_img_1" height="41" width="32" alt="ABTA logo" src="/cms-cps/fcfo/images/footer/abta_logo.gif"/>
            </a>
        </div> -->

    <div id="copyrightUtilityMenu">
        <ul>
            <li>
        <a rel="nofollow" href="http://www.firstchoice.co.uk/about-us/">About us</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/contact-us/">Contact us</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/help/">Help</a>
            </li>
            <li>
                <a href="http://www.firstchoice.co.uk/about-us/travel-shops/">Travel shops</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/our-policies/accessibility/">Accessibility</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/our-policies/terms-of-use/">Terms of use</a>
            </li>
            <li>
                <a rel="nofollow" href="http://www.firstchoice.co.uk/our-policies/security-policy/">Security policy</a>
            </li>
            <li >
                <a rel="nofollow" href="http://www.firstchoice.co.uk/our-policies/privacy-policy/">Privacy policy</a>
            </li>
            <li class="last">
                <a rel="nofollow" href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges/index.html">Credit Card Fees</a>
            </li>
        </ul>
    </div>
</div>
<%-- Modified to move ensighten from footer to header--%>