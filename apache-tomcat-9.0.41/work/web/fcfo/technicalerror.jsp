	<html>
		<head>
		<%@ page contentType="text/html; charset=UTF-8" %>
          <version-tag:version/>

			<title>First Choice | Details &amp; Buy |</title>
			<link rel="stylesheet" type="text/css" href="/cms-cps/fcfo/css/firstchoice.css" xmlns:myXsltExtension="urn:XsltExtension">

			<script type="text/javascript" src="/cms-cps/fcfo/js/common.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
			<script type="text/javascript" src="/cms-cps/fcfo/js/basic.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
			<script type="text/javascript" src="/cms-cps/fcfo/js/functions_flight_options.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
			<script type="text/javascript" src="/cms-cps/fcfo/js/fcfo.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
			<script type="text/javascript" src="/cms-cps/common/js/prototype.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
			  <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
			  	<script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>


					</head>

		<body>

			<div id="page_container">
				<div id="topcontainer">
					<div class="accessibility">
						<ul>
							<li>
							<a href="#main_content">Jump to main Content</a>
							</li>
							<li>
							<a href="#SearchPanel">Jump to Search</a>
							</li>
							<li>
							<a href="#masthead">Jump to Main Menu</a>
							</li>
							<li>
							<a href="#Footer">Jump to Secondary Menu</a>
							</li>
							<li>
							<a href="#">Jump to Accessibility Statement</a>
							</li>

						</ul>
						<p>
						If you are using a screen reader, we recommend that you
						<a href="#">disable JavaScript</a>
						.
						</p>

					</div>
						<div id="fcHeader">
								<div class="fcClearNM">
									<a id="fcLogo" title="First Choice" href="http://www.firstchoice.co.uk/">First Choice</a>
									<p>
									<a class="noicon" rel="nofollow" href="http://www.firstchoice.co.uk/help/">Help</a>
									|
									<a class="noicon" rel="nofollow" href="http://www.firstchoice.co.uk/contact-us/">Contact us</a>
									|
									</p>
								</div>
				</div>

								<br/>

									<div id="ProgressIndicator">
									<ul>
									<li class="stepPending">Flight Search</li>
									<li class="stepPending">Search Results</li>
									<li class="stepPending">Flight Extras</li>
									<li class="stepPending">Confirm</li>
									<li class="stepPending">Details & Buy</li>
									<li class="stepPendingLast">Fly!</li>
									</ul>
							</div>
									<br clear="all"/>

						<div id="contentBox">
                         <div id="contentBoxHead">
                            <h1>We're sorry ...</h1>

                        </div>
                        <div id="sb_bookingbox" class="sb_bookingbox">
                            <div style="margin-bottom: 10px; margin-top: -76px;" class="fullwidth">
                                <div style="margin-top: 76px;">We are currently unable to process your request.<br/><br/>This may be due to technical issues. Please try again later, or alternatively you can call us on <strong>0871 200 7799*</strong>. We're open at the following times:<br/><br/>Monday to Friday ~ 9am - 8pm<br/><br/>Saturday ~ 9 - 5.30pm<br/><br/>Sunday ~ 10 - 6pm<br/><br/>Please note that our website is unavailable for booking between 1.00am and 6.00am while our flights database is being updated.<br/><br/><span style="font-size: 92%; color: rgb(123, 123, 123);">* Please note for 0871 numbers calls cost 10p per minute and for 0844 numbers calls cost 5p per minute. Charges apply at all times from BTlandlines. Other operator networks may vary.</span></div>
                            </div>
                        </div>
                        <br clear="all"/>
                        <div id="contentBoxFoot"/>
                    </div>






				<div class="destfooter" style="width: 540px;"></div>
<div id="Footer">

	<div class="cols">
		<h6>
			<a rel="nofollow" href="http://www.firstchoice.co.uk/help/">Help</a>
		</h6>
		<ul>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/help/booking-online/">Booking online</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/help/before-you-go/">Before you go</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/help/amending-or-cancelling-your-booking/">Amending or cancelling</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/help/flights-and-flying-information/">Flights and flying</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/help/health-concerns/">Health concerns</a>
			</li>
		</ul>
	</div>
	<div class="cols">
		<span class="notitle"> </span>
		<ul>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/help/at-the-airport/">At the airport</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/help/on-holiday/">On holiday</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/help/luggage/">Luggage</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/help/travelling-with-children/">Travelling with children</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/help/on-your-return/">On your return</a>
			</li>
		</ul>
	</div>
	<div class="cols">
		<h6>
			<a rel="nofollow" href="http://www.firstchoice.co.uk/information/why-book-with-us/">Why book with us?</a>
		</h6>
		<ul>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/information/why-book-with-us/holidays-for-adults/">Best for adults</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/information/why-book-with-us/holidays-for-families/">Best for children</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/information/why-book-with-us/extra-legroom/">More legroom</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/information/why-book-with-us/lowest-price-guarantee/">Lowest price guarantee</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/information/why-book-with-us/responsible-tourism/">Responsible tourism</a>
			</li>
		</ul>
	</div>
	<div class="cols">
		<h6>
			<a href="http://www.firstchoice.co.uk/information/holiday-extras/">Holiday extras</a>
		</h6>
		<ul>
			<li>
				<a href="http://www.firstchoice.co.uk/information/holiday-extras/before-you-go/holiday-insurance/">Holiday Insurance</a>
			</li>
			<li>
				<a href="http://www.firstchoice.co.uk/information/holiday-extras/before-you-go/foreign-exchange/">Foreign exchange</a>
			</li>
			<li>
				<a href="http://www.firstchoice.co.uk/information/holiday-extras/during-your-flight/premium-upgrades/">Premium upgrades</a>
			</li>
			<li>
				<a href="http://www.firstchoice.co.uk/information/holiday-extras/during-your-holiday/car-hire/">Car hire</a>
			</li>
			<li>
				<a href="http://www.firstchoice.co.uk/information/holiday-extras/during-your-holiday/excursions/">Excursions</a>
			</li>
		</ul>
	</div>
	<div class="cols">
		<ul class="helpFooterLast">
			<li>
			<a href="http://www.firstchoice.co.uk/sun-holidays/">Sun holidays</a>
			</li>
			<li>
			<a href="http://www.firstchoice.co.uk/ski/">Ski holidays</a>
			</li>
			<li>
			<a href="http://www.firstchoice.co.uk/last-minute-deals/">Last-minute deals</a>
			</li>
			<li>
			<a href="http://www.firstchoice.co.uk/flights/">Flights</a>
			</li>
			<li>
			<a href="http://www.firstchoice.co.uk/destinations/">Holiday destinations</a>
			</li>
		</ul>
	</div>
	<div id="atol_abta">
			<a rel="nofollow" title="ABTA" href="http://www.abta.com/">
				<img class="foot_img_1" height="41" width="32" alt="ABTA logo" src="/cms-cps/fcfo/images/footer/abta_logo.gif"/>
			</a>

		</div>
	<div id="copyrightUtilityMenu">
		<ul>
			<li>
		<a rel="nofollow" href="http://www.firstchoice.co.uk/about-us/">About us</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/contact-us/">Contact us</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/help/">Help</a>
			</li>
			<li>
				<a href="http://www.firstchoice.co.uk/about-us/travel-shops/">Travel shops</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/our-policies/accessibility/">Accessibility</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/our-policies/terms-of-use/">Terms of use</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/our-policies/security-policy/">Security policy</a>
			</li>
			<li>
				<a rel="nofollow" href="http://www.firstchoice.co.uk/our-policies/privacy-policy/">Privacy policy</a>
			</li>
			<li class="last">
				<a rel="nofollow" href="http://www.firstchoice.co.uk/information/credit-card-payments-and-charges/index.html">Credit Card Fees</a>
			</li>
		</ul>
	</div>
</div>
			</div>
		</body>
	</html>