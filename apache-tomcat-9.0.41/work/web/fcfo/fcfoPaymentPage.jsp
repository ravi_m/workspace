<%@include file="/common/commonTagLibs.jspf"%>
<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>

<%--***********Essential JSP settings. to be used through out the page ***********--%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="session" />
<c:set var="currency" value="${bookingComponent.totalAmount.symbol}" scope="request"/>
<%@include file="/common/intellitrackerSnippet.jspf" %>
	<html>
		<head>
			 <%@ page contentType="text/html; charset=UTF-8" %>
			<version-tag:version/>
			<title>First Choice | Details &amp; Buy |</title>
			<link rel="stylesheet" type="text/css" href="/cms-cps/fcfo/css/firstchoice.css" xmlns:myXsltExtension="urn:XsltExtension">
			<%-- apply corrections in the style for ie below 8 --%>
			<!--[if lt IE 8]>
			<link rel="stylesheet" type="text/css" href="/cms-cps/fcfo/css/seven.css" />

			<![endif]-->
			<!--[if lt IE 7]>
			<link rel="stylesheet" type="text/css" href="/cms-cps/fcfo/css/six.css" />
			<![endif]-->
			<%
				String getHomePageURL =(String)ConfReader.getConfEntry("FCFO.homepage.url","");
				pageContext.setAttribute("getHomePageURL",getHomePageURL);
			%>
			 <c:choose>
			 	<c:when test="${not empty bookingInfo.bookingComponent.clientURLLinks.homePageURL}">
			  		<c:set var='homePageURL' value="${bookingInfo.bookingComponent.clientURLLinks.homePageURL}" />
			    </c:when>
			    <c:otherwise>
			 	  	<c:set var='homePageURL' value='${getHomePageURL}'/>
			    </c:otherwise>
			 </c:choose>
				<link type="image/x-icon" href="/cms-cps/fcfo/images/favicon.ico" rel="icon"/>
		<link type="image/x-icon" href="/cms-cps/fcfo/images/favicon.ico" rel="shortcut icon"/>

			<script type="text/javascript" src="/cms-cps/fcfo/js/common.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
	            <script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
			<script type="text/javascript" src="/cms-cps/fcfo/js/basic.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
			<script type="text/javascript" src="/cms-cps/fcfo/js/functions_flight_options.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
			<script type="text/javascript" src="/cms-cps/fcfo/js/fcfo.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
			<script type="text/javascript" src="/cms-cps/common/js/prototype.js" xmlns:myXsltExtension="urn:XsltExtension"></script>
			  <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
			  	<script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
			    <script type="text/javascript" language="javascript">
	//js variable to be used in the intellitracker pqry string.
         var previousPageVar ="${bookingComponent.nonPaymentData['prevPage' ]}";
	</script>

	<script type="text/javascript">
	 var session_timeout= ${bookingInfo.bookingComponent.sessionTimeOut};
	 var IDLE_TIMEOUT = (${bookingInfo.bookingComponent.sessionTimeOut})-(${bookingInfo.bookingComponent.sessionTimeAlert});
	 var _idleSecondsCounter = 0;
	 var IDLE_TIMEOUT_millisecond = IDLE_TIMEOUT *1000;
	 var url="${homePageURL}";
	 var sessionTimeOutFlag =  false;

	 window.setInterval(CheckIdleTime, 1000);
     window.alertMsg();

	 function noback()
	 {
		  window.history.forward();
	 }

	 function alertMsg()
	 {
		  var myvar = setTimeout(function(){
		  document.getElementById("sessionTime").style.visibility = "visible";
		  document.getElementById("modal").style.visibility = "visible";
		  document.getElementById("sessionTimeTextbox").style.visibility = "visible";
		  var count = Math.round((session_timeout - _idleSecondsCounter)/60);
		  document.getElementById("sessiontimeDisplay").innerHTML = count +"mins";
		  },IDLE_TIMEOUT_millisecond);
	 }
	
	 function CheckIdleTime()
	 {
		 _idleSecondsCounter++;
	
	    if (  _idleSecondsCounter >= session_timeout)
	    {
		   	if (window.sessionTimeOutFlag == false)
		   	{
		   		document.getElementById("sessionTime").style.visibility = "hidden";
		   		document.getElementById("modal").style.visibility = "hidden";
		   		document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
		   		document.getElementById("sessionTimeout").style.visibility = "visible";
		   		document.getElementById("modal").style.visibility = "visible";
		   		document.getElementById("sessionTimeOutTextbox").style.visibility = "visible";
		   	}
		   	else
		   	{
		   		document.getElementById("sessionTimeout").style.visibility = "hidden";
		   		document.getElementById("modal").style.visibility = "visible";
		   		document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";
		   	}
		   //navigate to technical difficulty page
		}
	}
	 
	function closesessionTime()
	{
		document.getElementById("sessionTime").style.visibility = "hidden";
	    document.getElementById("modal").style.visibility = "hidden";
		document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
	}
	
	function reloadPage()
	{
		 window.sessionTimeOutFlag= true;
		 document.getElementById("sessionTimeout").style.visibility = "hidden";
		 document.getElementById("modal").style.visibility = "hidden";
		 document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";
		 window.noback();
		 window.location.replace(window.url);
		 return(false);
	}
	
	function homepage()
	{
		 url="${homePageURL}";
		 window.noback();
		 window.location.replace(url);
	}
	function activestate()
	{
		 document.getElementById("sessionTime").style.visibility = "hidden";
		 document.getElementById("modal").style.visibility = "hidden";
		 document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
		 window.ajaxForCounterReset();
		 window.alertMsg();
		 window._idleSecondsCounter = 0;
	}
	
	function ajaxForCounterReset()
	{
		 var token = "<c:out value='${param.token}' />";
		 var url = "/cps/SessionTimeOutServlet?tomcat=<c:out value='${param.tomcat}'/>";
		 var request = new Ajax.Request(url, {
			 method : "POST"
		 });
		 window._idleSecondsCounter = 0;
	}
	
	function newPopup(url)
	{
		 var win = window.open(url,"","width=700,height=600,scrollbars=yes,resizable=yes");
		 if (win && win.focus) win.focus();
	}
	
	
var  newHoliday  = "<c:out value='${bookingInfo.newHoliday}'/>";

 var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />" ;
 var token = "<c:out value='${param.token}' />" ;


var PaymentInfo = new Object();
PaymentInfo.totalAmount =${bookingComponent.totalAmount.amount};

PaymentInfo.payableAmount = ${bookingComponent.payableAmount.amount};
PaymentInfo.calculatedPayableAmount = ${bookingInfo.calculatedPayableAmount.amount};
PaymentInfo.calculatedTotalAmount = ${bookingInfo.calculatedTotalAmount.amount};
PaymentInfo.selectedCardType = null;
PaymentInfo.currency = "${bookingComponent.totalAmount.symbol}";
PaymentInfo.currency1 =  "${bookingComponent.totalAmount.currency.currencyCode}"	;
<fmt:formatNumber var="ccAmount" value="${bookingInfo.calculatedCardCharge.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>

PaymentInfo.chargeamt =${ccAmount};

PaymentInfo.selectedCardType = null;

var cardChargeMap = new Map();

  var pleaseSelect = new Array();
   cardChargeMap.put('none', pleaseSelect);
  <c:forEach var="cardCharges" items="${bookingInfo.cardChargeMap}">
		     cardChargeMap.put('<c:out value="${cardCharges.key}"/>', '<c:out value="${cardCharges.value}"/>');
  </c:forEach>

		  var cardDetails = new Map();
 <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">
			    var cardCharge_Max_Min_Issue = new Array();
        	         	   cardCharge_Max_Min_Issue.push('${cardDetails.cardType.securityCodeLength}');
						    cardCharge_Max_Min_Issue.push('${cardDetails.cardType.minSecurityCodeLength}');
							 cardCharge_Max_Min_Issue.push('${cardDetails.cardType.isIssueNumberRequired}');

			  cardDetails.put('${cardDetails.paymentCode}',cardCharge_Max_Min_Issue);
   </c:forEach>
		window.onload= function(){ checkBookingButton("bookingcontinue");}
        //  The following map stores the pay button labels in case of 3D cards.
        var threeDCards = new Map();
        <c:forEach var="threeDCards" items="${bookingInfo.payDescription}">
            threeDCards.put("<c:out value="${threeDCards.key}"/>","<c:out value="${threeDCards.value}"/>");
        </c:forEach>

  jQuery(document).ready(function()
  {
	  toggleFCFOOverlay();
  })
  </script>
  <!-- Modified to move ensighten from footer to header -->
<%@include file="tag.jsp"%>
		</head>

		<body>
		<jsp:include page="/fcfo/cookielegislation.jsp"/>
		<c:set var="visiturl" value="yes" scope="session" />
            <c:set var='clientapp' value='${bookingInfo.bookingComponent.clientApplication.clientApplicationName} '/>
    	<div id="page_container">
				<div id="topcontainer">
					<div class="accessibility">
						<ul>
							<li>
							<a href="#main_content">Jump to main Content</a>
							</li>
							<li>
							<a href="#SearchPanel">Jump to Search</a>
							</li>
							<li>
							<a href="#masthead">Jump to Main Menu</a>
							</li>
							<li>
							<a href="#Footer">Jump to Secondary Menu</a>
							</li>
							<li>
							<a href="#">Jump to Accessibility Statement</a>
							</li>

						</ul>
						<p>
						If you are using a screen reader, we recommend that you
						<a href="#">disable JavaScript</a>
						.
						</p>

					</div>
					<jsp:include page="bookingHeader.jsp"/>

				</div><%--topcontainer ends--%>

				<jsp:include page="breadCrumb.jsp"/>
				<%-- breadCrumb.jsp--%>
				<div class="teaserBox">
					<%-- sidepanel.jsp--%>
					<jsp:include page="sidePanel.jsp"/>
				</div>
				<div id="contentBox" style="*clear:both;">
					<div id="contentBoxHead">
						<h1>Passenger details & payment</h1>
						<p/>
						<p>To confirm your selected flight, please enter your payment details and click Book flight now. Your booking will then be confirmed and any changes will incur an amendment fee.</p>
					</div>
					<noscript xmlns:myxsltextension="urn:XsltExtension"><font class="error">  ERROR: This service makes use of Javascript, which appears to be turned off. Click <a href='activateJavascript.htm' class='error'>here</a> to learn how to activate it. </font></noscript>
					<form id="SkySales" action="" name="SkySales">
	                <%-- This section was added to solve the problem caused at the sinnerschrader end --%>
	                <input type="hidden" name="payment_0_cardHoldersemailAddress" value=""/>
	                <input type="hidden" name="payment_0_cardHoldersmobilePhone" value=""/>
	                <input type="hidden" name="payment_0_street_address4" value=""/>
	                <input type="hidden" name="payment_0_cardHoldersemailAddress1" value=""/>
	                <input type="hidden" name="payment_0_cardHoldersdayTimePhone" value=""/>
	                <input type="hidden" name="payment_0_cardHolderAddress2" value=""/>
					   <input type="hidden" name="tomcatInstance" id="tomcatInstance" value="${param.tomcat}" />
					   <input type="hidden" name="token" id="token" value="${param.token}" />

      					<c:set var="error" value="${bookingComponent.errorMessage}"/>
							<c:if test="${(error != '' && error != null) || (requestScope.disablePaymentButton == 'true')}">
								<table cellspacing="0" cellpadding="5" border="0" style="border: 2px solid rgb(247, 146, 30); margin: 20px 30px; background: rgb(254, 245, 230) none repeat scroll 0% 0%; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial;">
									<tbody>
										<tr>
											<td nowrap="" align="right" valign="top">
											<img id="ani-error" style="margin-top: 15px;" src="/cms-cps/fcfo/images/ic_error.gif"/>
											</td>
											<td width="99%">
												<c:choose>
													<c:when test="${requestScope.disablePaymentButton == 'true'}">
								                        <c:out value="${requestScope.downTimeErrorMsg}" escapeXml="false"/>
					            					</c:when>
													<c:otherwise>
														<ul>
															<span class="error">
																<li>
																<typename>[<c:out value="${bookingComponent.errorMessage}" escapeXml="false"/>]</typename>
																</li>
															</span>
														</ul>
													</c:otherwise>
	 											</c:choose>
											</td>
										</tr>
									</tbody>
								</table>
							</c:if>
					<div class="sb_bookingbox">
                        <%-- Timeout Static Message Block timeoutInfo.jsp--%>
                        <jsp:include page="timeoutInfo.jsp"></jsp:include>
						<%-- Pricing details.jsp--%>
						<jsp:include page="pricingDetails.jsp"/>

						<%-- PassengerDetails.jsp --%>
						<jsp:include page="passengerDetails.jsp"/>

						<%-- Lead passenger Details--%>
						<jsp:include page="leadPassengerDetails.jsp"/>

					<%-- PaymentDetails.jsp--%>
					<jsp:include page="paymentDetails.jsp"/>
					<jsp:include page="addressDetails.jsp"/>
					<c:set var="outBoundFlight_operatingAirlineCode" value="${bookingComponent.nonPaymentData['outBoundFlight_operatingAirlineCode']} " scope="session" />
					<jsp:include page="fareRules.jsp">
					<jsp:param name="outBoundFlight_operatingAirlineCode" value="${outBoundFlight_operatingAirlineCode}"/>
					</jsp:include>

					<%-- fareRules.jsp--%>

						  <c:set var="backurl" value="${bookingComponent.prePaymentUrl}"/>
						<div class="fullwidth" style="margin: 20px 0pt; float: left;">
						<div class="leftfloat" style="width: 49%; font-weight: bold;"> <a href="<c:out value='${backurl}'/>"   title="Back">
						<img border="0"
							src="/cms-cps/fcfo/images/arrow_back.gif" /> Back to Confirm </a>
						</div>
						<c:if test="${bookingInfo.newHoliday}">
						<div class="leftfloat rightalign" style="width: 50%;*margin-left:-50px;">
							<c:choose>
								<c:when test="${requestScope.disablePaymentButton == 'true'}">
			                        <div class="Pay"
										id="CONTROLGROUPCONTACT_LinkButtonSubmit"
										title="Continue">
										<img border="0" alt="Book flight now" id="bookingcontinue"
										src="/cms-cps/fcfo/images/btn_bookflightnow_disabled.gif" /> </div>
					            </c:when>
								<c:otherwise>
									<div class="Pay" href="javascript:void(0);"
										id="CONTROLGROUPCONTACT_LinkButtonSubmit"
										title="Continue"
										onclick="return validateRequiredField() && validateAcct() && preventDoubleClick() && formSubmit();">
										<img border="0" alt="Book flight now" id="bookingcontinue"
										src="/cms-cps/fcfo/images/btn_bookflightnow.gif" /> </div>
								</c:otherwise>
	 						</c:choose>
						</div>
						</c:if>
					</div>

					</div><%-- sb_bookingbox ends--%>
					<br clear="all"/>
					   <script>
							if(PaymentInfo.chargeamt !=0) { displayFieldsWithValuesForCard(); }
						</script>
					<%--Modified for ensighten to remove script tag which includes vs_FCA.js --%>
					<script type="text/javascript">
						var itrMId = 345; var itrRqstH = "tui.intelli-direct.com";
					</script>

					<div id="contentBoxFoot"></div>
					</form>

				</div><%-- End Content Box--%>

				<%--Footer.jsp--%>
				<jsp:include page="footer.jsp"/>

			</div><%-- page_container ends --%>
		</body>
	</html>