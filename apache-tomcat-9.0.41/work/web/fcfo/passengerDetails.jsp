
<%@include file="/common/commonTagLibs.jspf"%>

<h2 class="passenger">Passenger name(s)</h2>
<div class="control">Passenger names must be entered exactly as
stated on photo ID / passport.
<table cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<c:set var="adultCount"
			value="${bookingComponent.nonPaymentData['passenger_ADT_count']}" />

		<c:if test="${adultCount >= 1}">
			<c:set var="passengerCount" value="1" />
			<tr>
				<td colspan="6">
			<br/>
				</td>
			</tr>
			<tr>
			<td valign="top" style="padding: 0pt 10px 0pt 0pt;"></td>


				<td nowrap="" valign="top" style="padding: 0pt 10px 0pt 0pt;">
					<c:if test="${not empty bookingComponent.nonPaymentData['passenger_ADTInsured_1']}">
				Travel Insurance
				</c:if>
				</td>

				<td nowrap="" valign="top" style="padding: 0pt 10px 0pt 0pt;">Title
				*</td>
				<td valign="top" style="padding: 0pt 10px 0pt 0pt;">First name
				*</td>
				<td valign="top" style="padding: 0pt 10px 0pt 0pt;">Last name *</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="6">
			<br/>
				</td>
			</tr>
			  	<c:set var="isinfantinsured"
						value="false" />
			<c:forEach var="count" begin="1" end="${adultCount}">
			<tr>
				<td colspan="6">
			<br/>
				</td>
			</tr>
				<tr>
				<td>Adult<c:out value="${count}"/>	<c:if test="${count == 1}"><br/>(Lead Passenger)</c:if>
				</td>
				<td>
				<c:set var="insurancecount"
						value="passenger_ADTInsured_${count}" />
								<c:set var="agecount"
						value="passenger_ADTAgeBand_${count}" />

							<c:if test="${fn:containsIgnoreCase(bookingComponent.nonPaymentData[insurancecount],'TRUE')}">Yes (Age:
							${bookingComponent.nonPaymentData[agecount]} )
							<c:if test="${not fn:containsIgnoreCase(bookingComponent.nonPaymentData[agecount],'16')}">
									 <c:set var="isinfantinsured"
						             value="true" />
									 </c:if>
							</c:if>
						   <c:if test="${fn:containsIgnoreCase(bookingComponent.nonPaymentData[insurancecount],'FALSE')}">No</c:if>
				 </td>



					<td style="padding: 5px 10px 0pt 0pt;"><c:set var="initial"
						value="passenger_${passengerCount}_title" /> <select
						name="<c:out value='${initial}'/>"
						id="<c:out value='${initial}'/>" required="true"
						description="Adult ${passengerCount}"
						requiredError="{description} : Title is required."
						requiredEmpty="none" style="width: 50px">
						<option value="none"></option>
						<option value="Mr"
							<c:if test="${bookingComponent.nonPaymentData[initial] == 'Mr'}">selected</c:if>>Mr.</option>
						<option value="Ms"
							<c:if test="${bookingComponent.nonPaymentData[initial] == 'Ms'}">selected</c:if>>Ms.</option>
						<option value="Mrs"
							<c:if test="${bookingComponent.nonPaymentData[initial] == 'Mrs'}">selected</c:if>>Mrs.</option>
						<option value="Dr"
							<c:if test="${bookingComponent.nonPaymentData[initial] == 'Dr'}">selected</c:if>>Dr.</option>
						<option value="Mstr"
							<c:if test="${bookingComponent.nonPaymentData[initial] == 'Mstr'}">selected</c:if>>Mstr.</option>
						<option value="Miss"
							<c:if test="${bookingComponent.nonPaymentData[initial] == 'Miss'}">selected</c:if>>Miss.</option>
					</select></td>
					<td style="padding: 5px 10px 0pt 0pt;"><c:set var="foreName"
						value="passenger_${passengerCount}_foreName" /> <input
						id="<c:out value='${foreName}'/>" type="text"
						style="width: 100px;"
						regexerror="Please enter only English alphabetic characters in the name fields."
						regex="^[A-Za-z ]+$"
						requirederror="{description}: First Name is required."
						required="true" description="Adult ${passengerCount}"
						maxlength="15" name="<c:out value='${foreName}' />"
						value="<c:out value='${bookingComponent.nonPaymentData[foreName]}'/>" />
					</td>
					<td style="padding: 5px 10px 0pt 0pt;"><c:set var="lastName"
						value="passenger_${passengerCount}_lastName" /> <input
						id="<c:out value='${lastName}'/>" type="text"
						style="width: 100px;"
						regexerror="Please enter only English alphabetic characters in the name fields."
						regex="^[A-Za-z ]+$"
						requirederror="{description}: Last Name is required."
						required="true" description="Adult ${passengerCount}"
						maxlength="15" name="<c:out value='${lastName}'/>"
						value="<c:out value='${bookingComponent.nonPaymentData[lastName]}'/>" />
					</td>


					<td style="padding: 5px 10px 0pt 0pt;"></td>
				</tr>
				<c:set var="passengerCount" value="${passengerCount+1}" />
			</c:forEach>
		</c:if>
		<%-- children --%>
		<c:set var="childCount"
			value="${bookingComponent.nonPaymentData['passenger_CHD_count']}" />
		<c:if test="${childCount > 0}">


			<c:forEach var="count" begin="1" end="${childCount}">
		<tr>
				<td colspan="6">
			<br/>
				</td>
			</tr>
					  <c:set var="childagecount"
						value="passenger_CHDAgeBand_${count}" />
						<c:set var="insurancechildcount"
						value="passenger_CHDInsured_${count}" />

				<tr>
						<td>Child<c:out value="${count}"/><br/>
						<c:if test="${not empty bookingComponent.nonPaymentData[insurancechildcount]}">
						(Age:	${bookingComponent.nonPaymentData[childagecount]})</td>
						   </c:if>
							<td>



							<c:if test="${fn:containsIgnoreCase(bookingComponent.nonPaymentData[insurancechildcount],'TRUE')}">Yes
							</c:if>
						   <c:if test="${fn:containsIgnoreCase(bookingComponent.nonPaymentData[insurancechildcount],'FALSE')}">No</c:if>
				 </td>

					<td style="padding: 5px 10px 0pt 0pt;"><c:set var="initial"
						value="passenger_${count}_childTitle" /> <select
						name="<c:out value='${initial}'/>"
						id="<c:out value='${initial}'/>" required="true"
						description="Child ${count}"
						requiredError="{description} : Title is required."
						requiredEmpty="none" style="width: 50px">
						<option value="none"></option>
						<option value="Mstr"
							<c:if test="${bookingComponent.nonPaymentData[initial] == 'Mstr'}">selected</c:if>>Mstr.</option>
						<option value="Miss"
							<c:if test="${bookingComponent.nonPaymentData[initial] == 'Miss'}">selected</c:if>>Miss.</option>
					</select></td>
					<td style="padding: 5px 10px 0pt 0pt;"><c:set
						var="childForeName"
						value="passenger_${count}_childForeName" /> <input
						id="<c:out value='${childForeName}'/>" type="text"
						style="width: 100px;"
						regexerror="Please enter only English alphabetic characters in the name fields."
						regex="^[A-Za-z ]+$"
						requirederror="{description}: First Name is required."
						required="true" description="Child ${count}" maxlength="15"
						name="<c:out value='${childForeName}'/>"
						value="<c:out value='${bookingComponent.nonPaymentData[childForeName]}'/>" />
					</td>
					<td style="padding: 5px 10px 0pt 0pt;"><c:set
						var="childLastName"
						value="passenger_${count}_childLastName" /> <input
						id="<c:out value='${childLastName}'/>" type="text"
						style="width: 100px;"
						regexerror="Please enter only English alphabetic characters in the name fields."
						regex="^[A-Za-z ]+$"
						requirederror="{description}: Last Name is required."
						required="true" description="Child ${count}" maxlength="15"
						name="<c:out value='${childLastName}'/>"
						value="<c:out value='${bookingComponent.nonPaymentData[childLastName]}'/>" />
					</td>
						 <td style="padding: 5px 10px 0pt 0pt;" nowrap="nowrap">
						 <c:set var="age" value="passenger_${count-1}_childAge" /> 		
							<%-- <select
						name="<c:out value='${age}'/>" id="<c:out value='${age}'/>"
						description="Child ${count}">
						<c:forEach var="count" begin="2" end="15">
							<c:set var="yearCount" value="${count}" />
							<option value="<c:out value='${count}'/>"
								<c:if test="${bookingComponent.nonPaymentData[age] == yearCount}">selected</c:if>><c:out
								value="${count}" /> years</option>
						</c:forEach>
					</select>
					--%>
					${bookingComponent.nonPaymentData[age]} years
					</td>
				</tr>
				<c:set var="passengerCount" value="${passengerCount+1}" />
			</c:forEach>
		</c:if>
	</tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<c:set var="infantCount"
			value="${bookingComponent.nonPaymentData['passenger_INF_count']}" />
		<c:if test="${infantCount > 0}">
		    <c:set var="passengerCount" value="1" />
			<tr>
				<td colspan="6">
				<h3>Infants</h3>
				</td>
			</tr>
			<tr>			   <td></td>
				<c:if test="${not empty bookingComponent.nonPaymentData['passenger_ADTInsured_1']}">
				<td style="padding: 0pt 10px 0pt 0pt;" valign="top">Travel Insurance</td>
				</c:if>
				<td style="padding: 0pt 10px 0pt 0pt;" valign="top">First name
				*</td>
				<td style="padding: 0pt 10px 0pt 0pt;" valign="top">Last name *</td>
				<td style="padding: 0pt 10px 0pt 0pt;" valign="top">Age<br>
				(on date of return)</td>
				<td style="padding: 0pt 10px 0pt 0pt;" valign="top">Traveling
				with</td>
			</tr>
			<c:forEach var="count" begin="1" end="${infantCount}">
				<tr>
					<c:set var="infantForeName"
						value="passenger_${count}_infantForeName" />
						<c:set var="countinf"
						value="${count}" />
					<td style="padding: 5px 10px 0pt 0pt;" nowrap="nowrap"><c:out value="${count}" /></td>
						 		<c:if test="${not empty bookingComponent.nonPaymentData['passenger_ADTInsured_1']}">
							 <c:if test="${fn:containsIgnoreCase(isinfantinsured,'true')}">
							<td style="padding: 5px 10px 0pt 0pt;" nowrap="nowrap">Yes</td></c:if>
							<c:if test="${fn:containsIgnoreCase(isinfantinsured,'false')}">
							<td style="padding: 5px 10px 0pt 0pt;" nowrap="nowrap">No</td></c:if>
						   </c:if>
					<td style="padding: 5px 10px 0pt 0pt;"><input
						name="<c:out value='${infantForeName}'/>" maxlength="15"
						id="<c:out value='${infantForeName}'/>"
						description="Infant ${count}" required="true"
						requirederror="{description}: First Name is required."
						regex="^[A-Za-z ]+$"
						regexerror="Please enter only English alphabetic characters in the name fields."
						style="width: 100px;" type="text"
						value="<c:out value='${bookingComponent.nonPaymentData[infantForeName]}'/>">
					</td>
					<td style="padding: 5px 10px 0pt 0pt;"><c:set
						var="infantLastName"
						value="passenger_${count}_infantLastName" /> <input
						name="<c:out value='${infantLastName}'/>" maxlength="15"
						id="<c:out value='${infantLastName}'/>"
						description="Infant ${count}" required="true"
						requirederror="{description}: Last Name is required."
						regex="^[A-Za-z ]+$"
						regexerror="Please enter only English alphabetic characters in the name fields."
						style="width: 100px;" type="text"
						value="<c:out value='${bookingComponent.nonPaymentData[infantLastName]}'/>">
					</td>
					<td style="padding: 5px 10px 0pt 0pt;" nowrap="nowrap"><c:set
						var="age" value="passenger_${count}_infantAge" /> <select
						name="<c:out value='${age}'/>" id="<c:out value='${age}'/>"
						description="Infant ${count}">
						<c:forEach var="count" begin="0" end="23">
							<c:set var="ageCount" value="${count}" />
							<option value="<c:out value='${count}'/>"
								<c:if test="${bookingComponent.nonPaymentData[age] == ageCount}">selected</c:if>><c:out
								value="${count}" /> months</option>
						</c:forEach>
					</select></td>
					<c:set var="adultCount"
						value="${bookingComponent.nonPaymentData.passenger_ADT_count}" />
					<td style="padding: 5px 10px 0pt 0pt;"><c:set var="travelWith"
						value="passenger_${countinf}_travelWith" /> <select
						name="<c:out value='${travelWith}'/>"
						id="<c:out value='${travelWith}'/>" description="Infant ${count}"
						class="input3Eighth" required="true"
						requirederror="{description}: Infant Adult Assignment is required."
						requiredempty="none">
						<option value="none"></option>
						<c:forEach var="count" begin="1" end="${adultCount}">
							<option value="Adult <c:out value='${count}'/>"
							<c:if test="${count == passengerCount}">selected</c:if>>Adult <c:out value="${count}"/></option>Adult <c:out value="${count}"/></option>
						</c:forEach>
					</select></td>
				</tr>
				<c:set var="passengerCount" value="${passengerCount+1}" />
			</c:forEach>
		</c:if>
	</tbody>
</table>
</div>