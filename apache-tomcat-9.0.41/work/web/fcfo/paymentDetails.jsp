<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="tomcatInstance" value="${param.tomcat}" />
<c:set var="token" value="${param.token}" />
<h2 class="payment">Payment details</h2>
<div class="control">
<table cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="165">
				<div class="rowseparator" />
			</td>
			<td width="190">
				<div class="rowseparator" />
			</td>
			<td width="150">
				<div class="rowseparator" />
			</td>
		</tr>
		<tr>
			<td class="rightpadding" nowrap="" width="120"><strong>Total
			amount due</strong></td>
			<td class="rightpadding"
				style="color: rgb(235, 13, 139); font-size: 18px;">
			<table cellspacing="0" cellpadding="0" border="0">
				<tbody>
					<tr>
						<td align="left"  style="color: rgb(235, 13, 139); font-size: 18px;"><strong><c:out value="${currency}" escapeXml="false"/></strong></td>
						<td nowrap="" style="color: rgb(235, 13, 139); font-size: 18px;" id='totalAmountDue'>
							<strong>&nbsp;&nbsp;<fmt:formatNumber value="${bookingInfo.calculatedTotalAmount.amount}" type="number" maxFractionDigits="2"
              minFractionDigits="2" pattern="#,##,###.##"/></strong>

						</td>
					</tr>
				</tbody>
			</table>
			</td>
			<td valign="top" rowspan="5" />
		</tr>
		<tr>
			<td colspan="3">
			<div class="rowseparator" />
			</td>
		</tr>
		<tr>
			<td class="rightpadding" nowrap="">Payment method*</td>
			<td nowrap="" colspan="2">

		    <select name="payment_0_type"
			<c:choose>
				<c:when test="${requestScope.disablePaymentButton == 'true'}">
                    onchange="javascript:updateCardChargeForFCFODisabled(),setIssueField(this)"
	            </c:when>
				<c:otherwise>
					onchange="javascript:updateCardChargeForFCFO(),setIssueField(this)"
				</c:otherwise>
	 		</c:choose>	
            id="payment_type_0" required="true"
            requiredError="Please select payment method." requiredEmpty="none" style="width:245px;">
            <option selected value="none">Please select</option>
            <c:forEach var="cardDetails" items="${bookingComponent.paymentType['CNP']}">
            <option value='${cardDetails.paymentCode}'>
                          ${cardDetails.paymentDescription} (+ <c:out value="${currency}" escapeXml="false"/><script>document.write(calculateSelectedCardCharge(<c:out value="${bookingInfo.calculatedTotalAmount.amount}"/>,'<c:out value="${cardDetails.paymentCode}"/>'));</script> fee)
            </option>
         </c:forEach>
         </select>
		</td>
		</tr>
		<tr>
			<td colspan="2">
			<div class="rowseparator" />
			</td>
		</tr>
		<tr>
			<td class="rightpadding" valign="top">Name of card holder*</td>
			<td colspan="2"><input
				id="payment_0_nameOnCard"
				class="inputWhole" type="text" style="width: 183px;"
				validationtypeerror="Card holder name is invalid."
				requirederror="Card holder name is required." required="true"
				maxlength="30"
				name="payment_0_nameOnCard" /></td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="rowseparator" />
			</td>
		</tr>
		<tr>
			<td class="rightpadding" valign="top">Card number*</td>
			<td colspan="2"><input
				id="payment_0_cardNumber"	  autocomplete="off"
				class="inputWhole" type="text" style="width: 183px;"
				validationtype="Mod10"
				minlengtherror="Card number is invalid. Too long?"
				maxlengtherror="Card number is invalid. Too short?"
				validationtypeerror="Card number is invalid."
				requirederror="Card number is required." required="true"
				name="payment_0_cardNumber"   /></td>
		</tr>

		<tr>
      <td colspan="2">
        <div class="rowseparator"></div>
      </td>
    </tr>

  <tr id="issuerequiredlabel"  style="display:none">
      <td>Issue Number</td>
      <td id="issuerequiredfield" >
        <input name="payment_0_issueNumber" 	 id="payment_0_issueNumber"
		type="text"
           id="payment_0_issueNumber"
        style="width: 83px;" maxlength="4" >
      </td>
    </tr>

		<tr>
			<td colspan="2">
				<div class="rowseparator" />
			</td>
		</tr>
		<tr>
				<jsp:useBean id="now" class="java.util.Date" />
    <fmt:formatDate var ="expiryMonthSelected1" type="date" value="${now}" pattern="MM"/>
    <tr>
   <td valign="top" class="rightpadding">Expiry date*</td>
      <td colspan="2">
      <select id="payment_0_expiryMonth" name="payment_0_expiryMonth"
         style="width: 85px; margin-right: 10px;" required="true" requiredError="Expiry date is required.">


         <c:forEach begin="1" end="12" varStatus="loopStatus">
            <fmt:formatNumber var="expiryMonth" value="${loopStatus.count}" type="number" minIntegerDigits="2" pattern="##"/>
            <c:choose>
               <c:when test="${expiryMonthSelected1 ==  expiryMonth}">
                  <option value="<c:out value="${expiryMonth}"/>" selected="selected"><c:out value="${expiryMonth}"/></option>
               </c:when>
               <c:otherwise>
                  <option value="<c:out value="${expiryMonth}"/>"><c:out value="${expiryMonth}" /></option>
               </c:otherwise>
            </c:choose>
         </c:forEach>
      </select>

      <select id="payment_0_expiryYear" name="payment_0_expiryYear" required="true"
         requiredError="Expiry Year is required." validationTypeError="Please select a valid year in the future." style="width: 86px;">

          <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
               <option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>
         </c:forEach>
       </select>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div class="rowseparator" />
			</td>
		</tr>
		<tr>
			<td class="rightpadding" valign="top">CVV/CID code*</td>
			<td colspan="2"><input
				id="payment_0_securityCode"	  autocomplete="off"
				class="inputWhole" type="text" style="width: 83px;"
				minlengtherror="CVC Code is invalid. Too short?" minlength="3"
				validationtypeerror="CVC Code is invalid." validationtype="Numeric"
				requirederror="CVV Code is a mandatory field." required="true"
				 maxlengtherror="CVC Code is invalid. Too Large?" maxlength="4"
				name="payment_0_securityCode" />
			More about <a class="hasPopup"
				onclick="window.open('http://flights.firstchoice.co.uk/en/popup_cvv_cid.html','cvc','width=570,height=495');"
				href="javascript:void(0);">CVV/CID Code</a></td>

		</tr>
      <tr>
			<td colspan="3">
				<div class="rowseparator" />
			</td>
		</tr>
		<tr>
      <td/>
      <td>
         <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
               <c:if test="${threeDLogos == 'mastercardgroup'}">
		      <div class="logoContainerMasterCard" >
                     <a href="javascript:void(0);" id="masterCardDetails" class="sticky stickyOwner">
                        <img src="/cms-cps/fcfo/images/flights-mastercard-secure.gif"/>
		            <div class="hasPopup learnMoreDiv" >
			       Learn more
                        </div>
                     </a>
			   <%@ include file="/common/mastercardLearnMoreSticky.jspf"%>
                  </div>
	         </c:if>
         </c:forEach>
         <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
		   <c:if test="${threeDLogos == 'visagroup'}">
		      <div class="logoContainerVisa">
                     <a href="javascript:void(0);" id="visaDetails" class="sticky stickyOwner">
                        <img style="margin-left:5px;" src="/cms-cps/fcfo/images/flights-visa.gif"/>
	                  <div class="hasPopup learnMoreDiv">
  			         Learn more
                        </div>
                     </a>
                     <%@ include file="/common/visaLearnMoreSticky.jspf"%>
                  </div>
		   </c:if>
	   </c:forEach>
      </td>

		</tr>

	</tbody>
</table>

</div>

<%-- *******Essential fields ********************  --%>
<input type="hidden" name="payment_0_chargeAmount" id="payment_0_chargeIdAmount"  value="0" />
<input type="hidden" name="total_transamt"  id="total_transamt"  value="0" />
<input type="hidden" name="payment_totamtpaid"  id="payment_totamtpaid"  value="<c:out value='${bookingInfo.calculatedPayableAmount.amount}'/>" />
<%-- no of transaction is 1  --%>
<input type="hidden"  id="payment_0_totalTrans" value="1" name="payment_totalTrans"/>
<%-- should carry cardtype like AMERICAN_EXPRESS etc  --%>
<input type="hidden" id="payment_0_paymenttypecode" value="" name="payment_0_paymenttypecode"/>

<%-- *******End Essential fields ********************  --%>