<%@include file="/common/commonTagLibs.jspf"%>


<c:set var="displayErrataSection" value="false" scope="page"/>

<c:if test="${not empty bookingComponent.importantInformation.errata}">
   <c:if test="${!(bookingComponent.packageType.code=='DP' && !bookingComponent.accommodationSummary.accommodationSelected)}">
      <c:forEach var="accomerrata" items="${bookingComponent.importantInformation.errata}">
         <c:if test="${not empty accomerrata && accomerrata !='No errata'}">
            <c:set var="displayErrataSection" value="true" scope="page"/>
         </c:if>
      </c:forEach>
   </c:if>
</c:if>

<c:choose>
   <c:when test="${bookingComponent.packageType.code =='DP'}">
      <div id="imp_info">
         <h2>Important Information</h2>
         <p>We offer your selection as a choice of separate components which you can choose individually and purchase at the same time or subsequently as separate items but not as a combination of services at an inclusive price.</p>
         <p>Here is some additional information about the holiday you have chosen. If you are happy to proceed please click to accept or <a href="<c:out value='${resultsPageUrl}'/>"><strong>return to your results</strong></a> to select an alternative.</p>
         <c:if test="${displayErrataSection}">
            <div class="errormessage">
               <c:forEach var="accomerrata" items="${bookingComponent.importantInformation.errata}">
                  <c:if test="${accomerrata != 'No errata'}">
                     <c:out value="${accomerrata}" escapeXml="false"/>
                  </c:if>
               </c:forEach>
            </div>
         </c:if>
         <c:if test="${(bookingComponent.flightSummary.flightSupplierSystem)=='Amadeus' && (bookingComponent.flightSummary.flightSelected) == 'true'}">
            <div class="container_importantinformation_text2">
               <p>In addition to any amendment or cancellation fees charged by your travel suppliers, Thomson charges a �25 per person fee for amendments and cancellations. Please refer to the Terms and Conditions for full details of your contractual arrangements with Thomson and your travel suppliers.</p>
            </div>
            <div class="amadeustermsandconditions">
               <p>Full terms and conditions of your flight product component supplier are available on request.  Any cancellation, amendments or changes may incur a charge of up to 100% of your booking price plus supplier&#146;s administrative expenses.</p>
               <p>If the cancellation fee is 100% and you wish to re-book, the price of your new flight will usually be based on the prices applicable on the day you ask to re-book, which may not be the same as when you first booked.</p>
               <p>International travel is subject to the liability rules of the Warsaw Convention and the Montreal Convention. To view the notice summarising the liability rules applied by EU community air carriers please read the <a href="javascript:void(0);" onclick="Popup('http://www.thomson.co.uk/editorial/legal/montreal-convention.html',795,595,'location=0,status=0,left=100,top=50,toolbar=0,menubar=0,titlebar=0,resizable=1,scrollbars=1')">Air Passenger Notice.</a></p>
            </div>
         </c:if>
         <p>Please read and accept the important additional information before continuing with your booking.</p>
         <div class="highlight">
            <p><input type="checkbox" name="importantinfo" value="" /><label for="add_info">I have read and accept the important information above.</label><br clear="all"/></p>
         </div>
      </div>
   </c:when>
   <c:otherwise>
	  <c:if test="${displayErrataSection}">
	     <div id="imp_info">
	        <h2>Important Information</h2>
	        <p>Here is some additional information about the holiday you have chosen. If you are happy to proceed please click to accept or <a href="<c:out value='${resultsPageUrl}'/>"><strong>return to your results</strong></a> to select an alternative.</p>
			<div class="errormessage">
			   <c:forEach var="accomerrata" items="${bookingComponent.importantInformation.errata}">
			      <c:if test="${accomerrata != 'No errata'}">
				     <c:out value="${accomerrata}" escapeXml="false"/>
				  </c:if>
			   </c:forEach><br clear="all"/>
			</div>
         </div>
      </c:if>
      <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
	     <c:if test="${description.key == 'CAR_HIRE'}">
		    <div class="errormessage">
			   <c:out value="${description.value}" escapeXml="false"/>
		    </div>
	     </c:if>
	  </c:forEach>
      <%-- TODO:need to change the corresponding paths. --%>
      <c:set var="flag" value="false" scope="page"/>
      <c:forEach var="extras" items="${bookingComponent.extraFacilities}">
         <c:if test="${(extras.key=='COACH_TRANSFER')||(extras.key=='TAXI_TRANSFER')}">
            <c:set var="flag" value="true" scope="page"/>
         </c:if>
      </c:forEach>
      <c:if test="${((not empty bookingComponent.extraFacilities)&&(flag=='true'))}">
         <div class="errormessage">
            <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
			   <c:set var="extrainfo" value="${description.value}" />
			   <c:out value="${extrainfo}" escapeXml="false"/><br clear="all"/>
		    </c:forEach>
         </div>
      </c:if>
      <%-- TODO:need to change the corresponding paths. --%>
      <c:if test="${(bookingComponent.flightSummary.flightSupplierSystem)=='NAV'}">
         <div class="errormessage">
            TRANSFERS ARE NOT INCLUDED IN THIS HOLIDAY.
         </div>
      </c:if>
      <c:choose>
	     <c:when test= "${displayErrataSection}">
	        <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
	           <c:set var="redundntInsTxt" value="${description.value}" />
			   <c:if test="${redundntInsTxt != null && description.key == 'redundntInsuranceTxt'}">
			      <p><c:out value="${redundntInsTxt}" escapeXml="false"/></p>
			   </c:if>
			</c:forEach>
	     </c:when>
		 <c:otherwise>
		    <c:forEach var="description" items="${bookingComponent.importantInformation.description}">
			   <c:set var="redundntInsTxt" value="${description.value}" />
               <c:if test ="${redundntInsTxt != null && description.key == 'redundntInsuranceTxt'}">
			      <div id="imp_info">
	   	             <h2>Important Information</h2>
	   	             <p>
			            <c:out value="${redundntInsTxt}" escapeXml="false"/>
			         </p>
                  </div>
			   </c:if>
			</c:forEach>
		 </c:otherwise>
      </c:choose>
      <c:if test="${displayErrataSection}">
	     <div>
		    <p>Please read and accept the important additional information before continuing with your booking.</p>
	        <div class="highlight">
		       <p id="AdditionalInformation"><input type="checkbox" name="importantinfo" value="" /><label  for="add_info"><span id="additionalinfo">I have read and accept the important additional information.</span></label><br clear="all"/></p>
	        </div>
	     </div>
      </c:if>
   </c:otherwise>
</c:choose>