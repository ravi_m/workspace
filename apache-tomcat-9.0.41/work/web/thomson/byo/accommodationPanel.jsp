<%@include file="/common/commonTagLibs.jspf"%>
<h3>Accommodation</h3>
<div <c:if test="${bookingComponent.accommodationSummary.accommodationSelected == 'false'}">class="inactive" </c:if> >
      <h4><c:out value="${bookingComponent.accommodationSummary.accommodation.hotel}"/>
<c:set var="isPlus">
   <c:choose>
          <c:when test="${bookingComponent.accommodationSummary.accommodation.rating == null}">
              <c:out value="true"/>
          </c:when>
          <c:otherwise>
             <c:out value="false"/>
          </c:otherwise>
   </c:choose>
</c:set>
<c:set var="trating" value="${bookingComponent.accommodationSummary.accommodation.rating}"/>
<c:set var="starrating" value="${bookingComponent.accommodationSummary.accommodation.rating}"/>
<c:choose>
<c:when test="${isPlus}">
<c:choose>
<c:when test="${bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem == 'HOPLA_PEG'}">
<c:if test="${starrating!=null}">
<img src="/cms-cps/thomson/byo/images/<c:out value='${starrating}'/>.gif" border="0"/>
</c:if>
</c:when>
<c:otherwise>
<c:if test="${trating!=null}">
<img src="/cms-cps/thomson/byo/images/<c:out value='${trating}'/>_plus.gif" border="0"/>
</c:if>
</c:otherwise>
</c:choose>
</c:when>
<c:otherwise>
<c:if test="${trating!=null}">
<img src="/cms-cps/thomson/byo/images/<c:out value='${trating}'/>.gif" border="0"/>
</c:if>
</c:otherwise>
</c:choose></h4>
      <dl class="accom_details">
         <dt>Country&#58;</dt>
         <dd><c:out value="${bookingComponent.accommodationSummary.accommodation.country}"/></dd>
         <dt>Destination&#58;</dt>
         <dd><c:out value="${bookingComponent.accommodationSummary.accommodation.destination}"/></dd>
         <dt>Resort&#58;</dt>
         <dd><c:out value="${bookingComponent.accommodationSummary.accommodation.resort}"/></dd>
         <dt>Duration&#58;</dt>
         <c:if test="${bookingComponent.accommodationSummary.duration !='0'}">
         	<c:choose>
               <c:when test="${bookingComponent.accommodationSummary.duration =='1'}">
                 <dd>
                 <c:out value="${bookingComponent.accommodationSummary.duration}"/>  night
                 </dd>
               </c:when>
               <c:otherwise>
               <dd>
               <c:out value="${bookingComponent.accommodationSummary.duration}"/> nights
               </dd>
               </c:otherwise>
        	</c:choose>
        </c:if>
         <dt>Board Basis&#58;</dt>
         <dd>
           <span id="boardBasisSummary">
              <c:out value="${bookingComponent.accommodationSummary.accommodation.boardBasis}"/>
           </span><br />
        </dd>
      </dl>
      <br clear="all" />
   </div><%-- Commented for addressing the bug 24653 --%>
<%--<div align="center"><a href="javascript:Popup('<c:out value="${bookingComponent.clientDomainURL}/thomson/page/shop/common/atoz/atoztandc.page"/>',755,584,'scrollbars=yes');">A-Z Booking Information</a></div>--%>