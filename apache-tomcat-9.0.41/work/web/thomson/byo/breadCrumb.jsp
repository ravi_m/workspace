<div id="breadcrumb">
   <c:set var="url" value='${bookingComponent.clientDomainURL}'/>
   <c:set var="accomDetailsUrl" value="${bookingComponent.nonPaymentData['accomDetailsUrl']}" scope="request"/>
   <c:set var="flightDetailsUrl" value="${bookingComponent.nonPaymentData['flightDetailsUrl']}" scope="request"/>
   <c:set var="clientUrlLinks" value="${bookingComponent.clientURLLinks}" scope="request"/>

   <%--TODO:This condition is added for bookMarking Change.Once bookMarking goes live this condition needs to be removed ie. empty check for accomDetailsUrl and flightDetailsUrl should be removed. --%>
   <c:choose>
   <c:when test="${empty accomDetailsUrl && empty flightDetailsUrl}">
   <c:set var="resultsPageUrl" value="${url}${clientUrlLinks.searchResultsURL}" scope="request"/>
   <c:set var="travelOptionsPageUrl" value="${url}${clientUrlLinks.holidayOptionsURL}" scope="request"/>
   <ul>
      <li><a href="<c:out value='${url}'/><c:out value='${clientUrlLinks.homePageURL}'/>">Search</a>&nbsp;&raquo;</li>
      <li><a href="<c:out value='${resultsPageUrl}'/>">Results</a>&nbsp;&raquo;</li>
      <li><a href="<c:out value='${url}'/><c:out value='${clientUrlLinks.accomDetailsURL}'/>" ><c:out value="${bookingComponent.accommodationSummary.accommodation.hotel}"/> details</a>&nbsp;&raquo;</li>
      <li><a href="<c:out value='${url}'/><c:out value='${clientUrlLinks.flightDetailsURL}'/>">Select flight</a>&nbsp;&raquo;</li>
      <li><a href="<c:out value='${travelOptionsPageUrl}'/>">Options</a>&nbsp;&raquo;</li>
      <li class="current">Payment</li><li>&nbsp;&raquo;</li>
      <li>Confirmation</li>
   </ul>
   </c:when>
   <c:otherwise>
   <c:set var="resultsPageUrl" value="${url}/thomson/page/byo/search/results.page?pageNumber=0" scope="request"/>
   <c:set var="travelOptionsPageUrl" value="${url}/thomson/page/byo/booking/traveloptions.page" scope="request"/>
   <ul>
      <li><a href="<c:out value='${url}'/>/thomson/page/byo/home/home.page" >Search</a>&nbsp;&raquo;</li>
      <li><a href="<c:out value='${resultsPageUrl}'/>">Results</a>&nbsp;&raquo;</li>
      <c:if test="${not empty accomDetailsUrl}">
         <li><a href="<c:out value='${url}'/>/thomson/page/byo/accomdetails/accomdetails.page?<c:out value='${accomDetailsUrl}'/>" ><c:out value="${bookingComponent.accommodationSummary.accommodation.hotel}"/> details</a>&nbsp;&raquo;</li>
      </c:if>
      <c:if test="${not empty flightDetailsUrl}">
         <li><a href="<c:out value='${url}'/><c:out value='${flightDetailsUrl}'/>">Select flight</a>&nbsp;&raquo;</li>
      </c:if>
      <li><a href="<c:out value='${travelOptionsPageUrl}'/>">Options</a>&nbsp;&raquo;</li>
      <li class="current">Payment</li><li>&nbsp;&raquo;</li>
      <li>Confirmation</li>
   </ul>
   </c:otherwise>
   </c:choose>
</div>