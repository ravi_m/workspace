<%@include file="/common/commonTagLibs.jspf"%>

<div id="pers_details">
   <h2>Personal Details</h2>
   <p>Please enter passenger names to match those shown on your passports.</p>

<%--   enter passenger names starts--%>
   <table cellspacing="0" cellpadding="0" class="highlight" border="0">
      <caption>
       Personal Details
      </caption>
      <tr>
         <th width="161">&nbsp;</th>
         <th width="105">Travel<br/> Insurance</th>
         <th width="80">Title</th>
         <th width="115">Forename</th>
         <th width="60">Middle Initial</th>
         <th>Surname</th>
      </tr>

<%--Needed for no of passengers --%>
<c:set var="numberOfPassengersNew" value="0"/>
<c:forEach var="passengerRoomSummary" items="${bookingComponent.passengerRoomSummary}" varStatus="status">
<c:set var="noOfRooms" value="${status.count}"/>
     <c:forEach var="passenger" items="${passengerRoomSummary.value}" varStatus="passen">
        <c:set var="numberOfPassengersNew" value="${numberOfPassengersNew+1 }"/>
    </c:forEach>
</c:forEach>

<c:set var="numberOfPassengers" />
<c:set var="passengerCount" value="0" />
  <c:forEach var="passenger" items="${bookingComponent.passengerRoomSummary}" varStatus="status">
     <c:if test="${noOfRooms > 1}">
        <tr>
           <td colspan="6"><strong>Room&nbsp;<c:out value="${status.count}" />&nbsp;:- </strong></td>
        </tr>
	 </c:if>
     <c:forEach var="passengerRoom" items="${passenger.value}" varStatus="passen">
        <tr>
           <c:set var="numberOfPassengers" value="${passen.index}"/>
           <td>
              <p>
              <c:out value="${passengerRoom.label}"/>
              <br/>
              <c:set var="identifier" value="passenger_${passengerCount}_identifier"/>
              <c:set var="ageText" value=""/>
		      <input type = "hidden" name="<c:out value="${identifier}"/>" value = "<c:out value = '${passengerRoom.identifier}'/>"/>
              <c:if test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR' || passengerRoom.ageClassification == 'SENIOR2'}">
                 <c:if test = "${passengerCount == 0}">
		            <input name="leadPassenger" type="hidden" value="<c:out value="${passengerRoom.identifier}"/>" />
				    <span class="smalltext">Lead passenger (must be 18 <br/>years or over)</span>
                 </c:if>
                 <c:choose>
                    <c:when test="${passengerRoom.ageClassification == 'ADULT'}">
                       <c:set var="ageText" value="(Age: 18-64)"/>
                    </c:when>

                    <c:when test="${passengerRoom.ageClassification == 'SENIOR'}">
                       <c:set var="ageText" value="(Age: 65-74)"/>
                    </c:when>

                    <c:otherwise>
                       <c:set var="ageText" value="(Age: 75+)"/>
                    </c:otherwise>
                 </c:choose>
              </c:if>


              <c:if test="${passengerRoom.ageClassification == 'CHILD'}">
			     <span class="smalltext">(Age:<c:out value="${passengerRoom.age}" />)</span>
			  </c:if>

              <c:if test="${passengerRoom.ageClassification == 'INFANT'}">
                 <span class="smalltext"> (Age: <2) </span>
              </c:if>
           </td>

           <td>
                  <c:choose>
                     <c:when test="${passengerRoom.isInsuranceSelected}">
                        <b> Yes </b> <c:out value="${ageText}"/>
                     </c:when>
                     <c:otherwise>
                        <b> No </b>
                     </c:otherwise>
                  </c:choose>
           </td>


           <td>
              <c:set var="titlekey" value="personaldetails_${passengerCount}_title"/>
              <select name="<c:out value='${titlekey}' />" class="small">
		         <c:choose>
			        <c:when test="${passengerRoom.ageClassification == 'INFANT' || passengerRoom.ageClassification == 'CHILD'}">
				       <option value="Mstr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mstr'}">selected</c:if>>Mstr</option>
				       <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
			        </c:when>
			        <c:otherwise>
				       <option value="Mr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mr'}">selected</c:if>>Mr</option>
   				       <option value="Mrs" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mrs'}">selected</c:if>>Mrs</option>
				       <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
				       <option value="Ms" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Ms'}">selected</c:if>>Ms</option>
				       <option value="Dr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Dr'}">selected</c:if>>Dr</option>
				       <option value="Rev" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Rev'}">selected</c:if>>Rev</option>
				       <option value="Prof" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Prof'}">selected</c:if>>Prof</option>
			        </c:otherwise>
		         </c:choose>
		      </select>
		   </td>

           <td>
              <c:set var="foreNameKey" value="personaldetails_${passengerCount}_foreName"/>
              <input type="text" maxlength="15" class="medium" value="<c:out value="${bookingComponent.nonPaymentData[foreNameKey]}"/>" id="<c:out value="${foreNameKey}"/>" name="<c:out value="${foreNameKey}"/>" alt="Please enter the names of all passengers as shown on your passports.|Y|NAME"/>
           </td>

           <td>
              <c:set var="middleNameKey" value="personaldetails_${passengerCount}_middleInitial"/>
              <input  type="text" maxlength="1" class="vsmall" value="<c:out value="${bookingComponent.nonPaymentData[middleNameKey]}"/>" name="<c:out value="${middleNameKey}"/>" alt="Initial|N|ALPHA"/>
           </td>

           <td>
              <c:set var="lastNameKey" value="personaldetails_${passengerCount}_surName"/>
              <input  type="text" onclick="javascript:unCheck()" maxlength="15" class="medium" value="<c:out value="${bookingComponent.nonPaymentData[lastNameKey]}"/>" id="lastname_<c:out value='${passengerCount}'/>"  name="<c:out value="${lastNameKey}"/>" alt="Please enter the names of all passengers as shown on your passports.|Y|NAME"/>
           </td>
        </tr>

        <tr>
           <td colspan="5">&nbsp;</td>
           <td colspan="2">
              <c:if test="${passen.first}">
                 <c:if test="${passengerCount == 0 && numberOfPassengersNew > 1}">
                    <c:if test="${passengerCount+1 != numberOfPassengers }">
			           <div class="ieStyle" >
                          <input class="autoCom" id="autoCheck_<c:out value='${passengerCount+1}'/>"
								type="checkbox"
								name="autoCheck_<c:out value='${passengerCount+1}'/>"
								onclick="javascript:autoCompletionSurname(<c:out value='${numberOfPassengersNew}'/>,<c:out value='1'/>)" />
					      <div class="margintopfive">Auto complete surnames</div>
					   </div>
             </c:if>
            </c:if>
            </c:if>&nbsp;
				</td>
				<%--td colspan="2">&nbsp;</td--%>
          </tr>
         <c:set var="passengerCount" value="${passengerCount+1}" />
         </c:forEach>
  </c:forEach>
</table>
<input type = "hidden" id="passengerCount" name = "passengerCount" value = "<c:out value = '${passengerCount}'/>"/>
</div>

<p>Please provide Lead Passenger Details</p>

<%--passenger address  starts--%>
<div class="highlight">
   <dl class="hangleft">
      <dt>House Name/No.</dt>
      <dd>
         <input name="houseName" id="houseName"
         value="<c:out value="${bookingComponent.nonPaymentData['houseName']}"/>"
         alt="We cannot validate the house name or number you have entered. Please check and re-enter.|Y|ADDRESS"
         type="text" class="medium" maxlength="20" /><span class="star">&#42;</span> </dd>
      <dt>Address</dt>
      <dd>
         <input name="addressLine1" id="addressLine1"
         value="<c:out value="${bookingComponent.nonPaymentData['addressLine1']}"/>"
         alt="Your address is required to complete your booking. Please enter your address|Y|ADDRESS"
         type="text" class="large" maxlength="25" /><span class="star">&#42;</span> </dd>
      <dt>&nbsp;</dt>
      <dd>
         <input name="addressLine2" id="addressLine2"
         value="<c:out value="${bookingComponent.nonPaymentData['addressLine2']}"/>"
         alt="Address Line 2|N|ADDRESS"
         type="text" class="large" maxlength="25" />
      </dd>
      <dt>Town/City&nbsp;</dt>
      <dd>
         <input name="city" id="city" value="<c:out value="${bookingComponent.nonPaymentData['city']}"/>"
         alt="Your home town/city is required to complete your booking. Please enter your home town/city.|Y|ALPHAADDRESS"
         type="text" class="large"  maxlength="25" /><span class="star">&#42;</span> </dd>
      <dt>County</dt>
      <dd>
         <input name="county" id="county" value="<c:out value="${bookingComponent.nonPaymentData['county']}"/>"
         alt="Your county is required to complete your booking. Please enter your county.|N|ALPHAADDRESS"
         type="text" class="medium"  maxlength="16" />
      </dd>
      <dt>Post Code</dt>
      <dd>
         <input name="postCode" id="postCode" value="<c:out value="${bookingComponent.nonPaymentData['postCode']}"/>"
         alt="Please enter the Post Code|Y|POSTCODE"
         type="text" class="small" maxlength="8" /><span class="star">&#42;</span> </dd>
      <dt> Country</dt>
      <dd>
      <select name="country" id="country" class="leadpassengerdetails_form_element_field_country">
         <option value="GB">United Kingdom</option>
      </select>
      <span class="star">&#42;</span> </dd>
   </dl>
   <dl class="hangright">
      <dt>Daytime Phone No.</dt>
      <dd>
      <input name="dayTimePhone" id="dayTimePhone" value="<c:out value="${bookingComponent.nonPaymentData['dayTimePhone']}"/>"
      alt="Please enter a valid telephone number.|Y|PHONE"
      type="text" class="medium" maxlength="15" />
      <span class="star">&#42;</span> </dd>
      <dt>Mobile Phone No. </dt>
      <dd>
      <input name="mobilePhone" id="mobilePhone" value="<c:out value="${bookingComponent.nonPaymentData['mobilePhone']}"/>"
      alt="Please enter a valid telephone number.|N|PHONE"
      type="text" class="medium" maxlength="15" />
      </dd>
      <dt>&nbsp;</dt>
      <dd>&nbsp;</dd>
      <dt>Email Address</dt>
      <dd>
      <input name="emailAddress" value="<c:out value="${bookingComponent.nonPaymentData['emailAddress']}"/>"
      id="EmailAddress"
      alt="Your email address is required to complete your booking. Please enter your email address.|Y|EMAIL"
      type="text" class="large" maxlength="100"  /><span class="star">&#42;</span> </dd>
      <dt>Verify Email</dt>
      <dd>
      <input name="emailAddress1"
      value="<c:out value="${bookingComponent.nonPaymentData['emailAddress1']}"/>"
      id="EmailAddress2"
      alt="We require you to confirm your email address for security purposes. Please re-enter your email address.|Y|EMAIL"
      type="text" class="large" maxlength="100" /><span class="star">&#42;</span> </dd>
   </dl>
   <p class="noheight">&nbsp;</p>
   <input type="hidden"
   alt="Your email and confirmation email addresses do not match. Please check and re-enter.|-|MATCH|EmailAddress|EmailAddress2"/>
   <div class="mandatory">
     Please complete all fields marked with <font color="red">*</font>
   </div>

</div>
<script type="text/javascript">
var totalPassenCount=<c:out value='${numberOfPassengers}'/>;
var totalPassengerCount=<c:out value='${numberOfPassengersNew}'/>;
var noPerson65 = "<c:out value='${over65count}' />";
var noPersons75 =  "<c:out value='${over76count}' />";
</script>
<%--    passenger address  starts--%>