<%@include file="/common/commonTagLibs.jspf"%>

   <div id="discount">
      <h2>Discount Code</h2>
      <p>If you have a discount code, please enter the code below and click 'Validate Code'.</p>
      <div class="highlight">
		 <div id="promocode">
            Please enter your discount code
            <input style="margin-top:10px" name="promotionalCode" id="promotionalCode"
            type="text" maxlength="21" class="pricedigit"
            value='<c:out value="${bookingComponent.nonPaymentData['promotionalCode']}"/>'/>
            <input type ="hidden"
            id="isPromoCodeApplied" name="isPromoCodeApplied"
            value='<c:out value="${bookingComponent.nonPaymentData['isPromoCodeApplied']}"/>'/>
            <input type ="hidden"
            id="promoDiscount" name="promoDiscount"
            value='<c:out value="${bookingComponent.nonPaymentData['promoDiscount']}"/>'/>
               <a onclick="javascript:updatePromotionalCode()" href="javascript:;" class="validateCodeButton">
	               <img src="/cms-cps/thomson/byo/images/validate_code_new.gif" alt="" align="top" border="0"/>
	            </a>
		 <div id="promoText1" style='display:none' ></div>
		 <c:forEach var="costingLine" items="${bookingComponent.priceComponents}">
	        <fmt:formatNumber var="promoAmount" value="${costingLine.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2"/>
	        <c:if test="${costingLine.itemDescription == 'Promotional Discount' && promoAmount != '0.00'}">
               <div id="promoText2">Amount of -&pound;<c:out value="${promoAmount}"/> has been deduced from the total cost.</div>
	        </c:if>
	     </c:forEach>
		</div>
      </div>
   </div>
