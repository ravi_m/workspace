<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div id="summaryPanel">
  <div class="genericSidesShadowRight">
    <div class="genericSidesShadowLeft">
      <jsp:include page="thaoPricePanel.jsp"/>
      <c:if test="${not empty bookingComponent.flightSummary || bookingComponent.flightSummary != null}">
        <jsp:include page="thaoFlightOptions.jsp"/>
      </c:if>
      <jsp:include page="thaoAccomDetails.jsp"/>
    </div><!-- END genericSidesShadowLeft -->
  </div><!-- END genericSidesShadowRight -->
  <jsp:include page="thaoGenericBottomshadow.jsp"/>
</div><!-- END summaryPanel -->