<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<fmt:setLocale value="en_GB" />
<div id="accommodationOptions" class="optionSummaryDetails">
  <h2>Accommodation options</h2>

  <div class="panelContainer">
    <h3 class="accomResort">
      <c:out value="${bookingComponent.accommodationSummary.accommodation.hotel}"/>
    </h3>

    <div class="rating">
      <jsp:include page="thaoRating.jsp"/>
    </div>

    <ul class="optionSummary">
      <c:if test="${bookingComponent.accommodationSummary.accommodation.accommodationType!='CITY'}">
        <li>
          <span class="header">Destination&#58;</span>
          <span class="info">
            <c:out value="${bookingComponent.accommodationSummary.accommodation.destination}"/>
          </span>
        </li>
      </c:if>
      <li>
        <c:if test="${bookingComponent.accommodationSummary.accommodation.accommodationType!='CITY'}">
          <span class="header">Resort&#58;</span>
        </c:if>
        <c:if test="${bookingComponent.accommodationSummary.accommodation.accommodationType=='CITY'}">
          <span class="header">City&#58;</span>
        </c:if>
        <span class="info">
          <c:out value="${bookingComponent.accommodationSummary.accommodation.resort}"/>
        </span>
      </li>
      <li>
        <span class="header">Check-in&#58;</span>
        <span class="info">
          <fmt:formatDate value="${bookingComponent.accommodationSummary.startDate}" pattern="dd/MM/yyyy"/>
        </span>
      </li>
      <li>
        <span class="header">Check-out&#58;</span>
        <span class="info">
          <fmt:formatDate value="${bookingComponent.accommodationSummary.endDate}" pattern="dd/MM/yyyy"/>
        </span>
      </li>
      <li>
        <span class="header">Duration&#58;</span>
        <c:choose>
          <c:when test="${bookingComponent.accommodationSummary.duration=='1'}">
            <span class="info">
              <c:out value="${bookingComponent.accommodationSummary.duration}"/> night
            </span><br clear="all"/>
          </c:when>
          <c:otherwise>
            <span class="info">
              <c:out value="${bookingComponent.accommodationSummary.duration}"/> nights
            </span><br clear="all"/>
          </c:otherwise>
        </c:choose>
      </li>
      <c:if test="${not empty bookingComponent.accommodationSummary.accommodation.boardBasis && bookingComponent.accommodationSummary.accommodation.accommodationType!='VILLA'}">
        <li>
          <span class="header">Board Basis&#58;</span>
          <span class="info">
            <c:out value="${bookingComponent.accommodationSummary.accommodation.boardBasis}"/><br />
          </span>
        </li>
      </c:if>
      <li>
        <span class="header">Rooms selected&#58;</span>
        <span class="info">
          <c:out value="${bookingComponent.accommodationSummary.accommodation.roomDescription}" escapeXml="false"/>
        </span>
      </li>
    </ul>

    <p><a class="arrow-link-right" title="View accommodation details" href="javascript:Popup('<c:out value="${bookingComponent.clientDomainURL}${bookingComponent.accommodationSummary.accommodationDetailsUri}"/>',800,620);">View accommodation details</a></p>
  </div><!-- END panelContainer -->
</div><!-- END Accommodation options-->
