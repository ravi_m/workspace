<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request"/>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Thomson.co.uk - Great value accommodation from the UK's no.1 tour operator</title>
    <link href="/cms-cps/thomson/thao/css/thao_cps.css" rel="stylesheet" type="text/css" media="screen,print" />
    <link href="/cms-cps/thomson/thao/css/thao_errors.css" rel="stylesheet" type="text/css" media="screen,print" />
    <!--[if lte IE 6]>
      <link type="text/css" rel="stylesheet" href="/cms-cps/thomson/thao/css/thao_ie6.css" media="screen,print" />
    <![endif]-->

    <!-- Include Payment related JS Here -->
    <script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
    <script type="text/javascript" src="/cms-cps/thomson/thao/js/genericFormValidation.js"></script>
    <script type="text/javascript" src="/cms-cps/thomson/thao/js/general.js"></script>
    <script type="text/javascript" src="/cms-cps/thomson/thao/js/paymentUpdation.js"></script>
    <script type="text/javascript" src="/cms-cps/thomson/thao/js/popups.js"></script>
    <script type="text/javascript" src="/cms-cps/thomson/thao/js/sifr.js"></script>
    <!-- End of JS files -->
    <link rel="shortcut icon" href="/cms-cps/thomson/thao/images/favicon/favicon.ico"/>
    <link rel="icon" href="/cms-cps/thomson/thao/images/favicon/favicon.ico"/>
    <!-- Modified to add ensighten in head section -->
    <%@include file="tag.jsp"%>
  </head>
  <body class="pageErrors short">
     <jsp:include page="/thomson/thao/cookielegislation.jsp"/>
    <div id="Page">
      <jsp:include page="thaoHeader.jsp"/>
      <div id="PageColumn1">
        <div id="MainContent">
          <!-- <div id="PrimaryColumn"></div> -->

          <%-- GENERIC ERROR - MAIN BODY OF MESSAGE --%>
          <div id="fullBodyWidth" class="pageError">
            <span class="warningIcon">&nbsp;</span>

            <div id="TitleArea">
              <h1>Sorry, we are currently unable to process your request.</h1>
            </div>

            <div class="errorContent">
              <div class="contentBlock">
                <p>This may be due to technical issues. Please try again later.</p>
                <p>Alternatively you can call us on 0871 231 4691 during the following hours:</p>
                <p>Monday to Friday - 09:00 to 21:00</p>
                <p>Saturday - 09:00 to 20:00</p>
                <p>Sunday - 10:00 to 20:00</p>
                <p>Please note that our website is unavailable for bookings between 01:00 and 06:00 while our holiday database is being updated.</p>
                <ul>
                  <li>
                    <a href="javascript:window.history.back()" title="Back">Back</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

        </div><%-- MainContent --%>
      </div><%-- PageColumn1 --%>
    </div><%-- Page --%>

    <script type="text/javascript">
    <!--
    if((window.opener) && (window.name == 'popupWindow')) {
          opener.location.replace(self.location.href);
          window.close()
    }
    else if (top.frames.length!=0) {
       if (window.location.href.replace)
          top.location.replace(self.location.href);
       else
          top.location.href=self.document.href;
    }
    //-->
    </script>
     <jsp:include page="thaoErrorFooter.jsp" />
  </body>
</html>
