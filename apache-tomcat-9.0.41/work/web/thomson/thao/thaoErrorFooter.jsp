<%@include file="/common/commonTagLibs.jspf"%>

<!-- START Footer -->
<div id="Footer">
  <div id="toyBFftr">
    <div id="toyBFftrLeft">
      <div class="toyBFexploreLinks">
        <ul>
          <li class="first"><a href="http://www.thomson.co.uk/package-holidays.html" title="Package holidays" rel="nofollow">Package holidays</a></li>
          <li><a href="http://flights.thomson.co.uk/en/index.html" title="Flights" rel="nofollow">Flights</a></li>
          <li><a href="http://www.thomson.co.uk/hotels.html" title="Hotels" rel="nofollow">Hotels</a></li>
          <li><a href="http://www.thomson.co.uk/cruise.html" title="Cruises" rel="nofollow">Cruises</a></li>

          <li><a href="http://www.thomson.co.uk/villas.html" title="Villas" rel="nofollow">Villas</a></li>
          <li><a href="http://www.thomson.co.uk/deals.html" title="Deals" rel="nofollow">Deals</a></li>
          <li><a href="http://www.thomson.co.uk/holiday-extras.html" title="Holiday Extras" rel="nofollow">Holiday Extras</a></li>
          <li class="lastOne"><a href="http://www.thomson.co.uk/holiday-destinations.html" title="Holiday Destinations" rel="nofollow">Holiday Destinations</a></li>
        </ul>
        <ul>
          <li class="first"><a href="http://www.thomson.co.uk/editorial/legal/about-thomson.html" title="About Thomson" rel="nofollow">About Thomson</a></li>
          <li><a href="http://www.thomson.co.uk/editorial/press-centre/news-releases.html" title="Press" rel="nofollow">Press</a></li>
          <li><a href="http://www.thomson.co.uk/jobs/travel-jobs.html" title="Travel Jobs" rel="nofollow">Travel Jobs</a></li>
          <li><a href="http://www.thomson.co.uk/partners/thomson-affiliate-programme.html" title="Affiliates" rel="nofollow">Affiliates</a></li>
          <li><a href="http://www.thomson.co.uk/destinations/sitemap/site-map.html" title="Sitemap">Sitemap</a></li>
          <li><a href="http://www.thomson.co.uk/editorial/ecrm/winaholiday.html" title="Register for offers" rel="nofollow">Register for offers</a></li>
          <li class="lastOne rss"><a href="http://www.thomson.co.uk/editorial/deals/rss.html" title="RSS" rel="nofollow">RSS</a></li>
        </ul>
      </div>
	  <div>
<p class="toyBFFooterDisclaimer"></p><p class="toyBFFooterDisclaimer"></p>
</div>
	  <div class="toyBFFooterCopyright">
         <jsp:useBean id='CurrDate2' class='java.util.Date'/>
         <fmt:formatDate var='currentYear' value='${CurrDate2}' pattern='yyyy'/>
        <ul>
          <li class="first lastOne">&copy; <c:out value="${currentYear}" /> TUI UK</li>
          <li><a href="http://www.thomson.co.uk/editorial/legal/website-terms-and-conditions.html" title="Terms &amp; Conditions" rel="nofollow">Terms &amp; Conditions</a></li>
          <li><a href="http://www.thomson.co.uk/editorial/legal/privacy-policy.html" title="Privacy &amp; Cookies Policy" rel="nofollow">Privacy &amp; Cookies Policy</a></li>
          <li><a href="http://www.tuitravelplc.com/" title="TUI Travel plc">TUI Travel Plc</a></li>
          <li class="lastOne"><a href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html" title="Credit Card Fees">Credit Card Fees</a></li>
          <li class="lastOne card">We accept:</li>
          <li class="lastOne cardType GB">Visa, Delta, MasterCard, Solo and American Express</li>
        </ul>
      </div>
    </div>
    <div id="toyBFftrRight">
      <div class="toyBFFooterHelp">
        <ul>
          <li><a href="http://www.thomson.co.uk/shopfinder/shop-finder.html" title="Shop finder" target="_new" rel="nofollow" onclick="window.open('http://www.thomson.co.uk/shopfinder/shop-finder.html','name','height=745,width=820,toolbar=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no'); return false;" >Shop finder</a></li>
          <li><a href="http://www.thomson.co.uk/editorial/faqs/faqs.html" title="Ask us a question" rel="nofollow">Ask us a question</a></li>
          <li><a href="http://www.thomson.co.uk/editorial/legal/contact-us.html" title="Contact us" rel="nofollow">Contact us</a></li>
          <li class="number">
            <script type="text/javascript">
              var cruiseCheck;
              if (cruiseCheck == true) {
                document.write('0871 231 5938');
              } else {
                document.write('0871 231 4691');
              }
            </script>
          </li>
          <li class="callCharges"><p>Calls cost 10p per minute plus network extras</p></li>
        </ul>
      </div>

      <ul class="toyBFFooterPartners">
        <li class="veriSign"><a href="https://seal.verisign.com/splash?form_file=fdf/splash.fdf&dn=www.thomson.co.uk&lang=en" title="This Web site has chosen one or more VeriSign SSL Certificate or online payment solutions to improve the security of e-commerce and other confidential communication">VeriSign Secured</a></li>
        <li class="abta">
          <a href="http://www.travelmatch.co.uk/scripts/verify.hts?+V5126" onclick="openWindow(this); return false;" title="Verify ABTA membership - ABTA Number V5126">Verify ABTA membership - ABTA Number V5126</a>
        </li>
		<li class="atol">
          <a href="http://www.caa.co.uk/applicationmodules/atol/atolverify.aspx?atolnumber=2524" onclick="openWindow(this); return false;" title="ATOL Protected - ATOL Number 2524. Click on the ATOL Logo if you want to know more">ATOL Procted - ATOL Number 2524</a>
        </li>
		</ul>
    </div>
  </div><!-- END #toyBFftr -->
</div><!--  END #Footer -->

<!--<script type="text/javascript">
  if(typeof sIFR == "function"){
    sIFR.replaceElement("#TitleArea h1", named({sFlashSrc: "/cms-cps/thomson/thao/images/media/tuifont.swf", sColor: "#333333", sWmode: "transparent"}));
  };
</script>-->
<!-- END Footer
 Modified to remove ensighten from footer -->