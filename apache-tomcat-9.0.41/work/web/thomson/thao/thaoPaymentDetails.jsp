<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<c:set var="cardChargeDetails" value="${bookingInfo.cardChargeDetails}" />
<c:set var="cardChargeArray" value="${fn:split(cardChargeDetails,',')}" />
<c:set var="cardCharge" value="${cardChargeArray[0]}" />
<c:set var="maxCardCharge" value="${cardChargeArray[2]}" />
<script type="text/javascript">
  var transamt="Transaction Amount";
  var transamtalert="Please enter a valid Transaction Amount";
  var voucheralert="Please enter a valid Voucher Code";
  var tottransamt="Total amount charged in this transaction:";
  var cashaccept="Cash Accepted";
  var cheqaccept="Cheque Accepted";
  var vouchercode="Voucher Code";
  var vouchervalue="Voucher Value";
  var addanothervoc="Add another voucher";
  var voucheraccept="Voucher Accepted";
  var amountpaid="Amount Paid";
  var amountstilldue="Amount still due";
  var totalamountdue="Total Amount due";
  var cardaccept="Card Payment Accepted";
  var poundsymbol="�";
  var totalvouchervalue="Total Voucher Value";
  var vocamtalert="Please Enter Valid Voucher Amount"
  var exceedalert="Amount entered exceeds total amount due. Please re-enter";
  var cardcode = "Auth code";
  var isHopla = false;
  var amtRec= "";
  var newHoliday = "<c:out value='${bookingInfo.newHoliday}'/>";
</script>

<c:set var="clientapp" value="${bookingComponent.clientApplication.clientApplicationName}"/>
<%
 String clientApp = (String)pageContext.getAttribute("clientapp");
 String cvvEnabled = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".CVV.CV2AVS.Enabled", "false");
 pageContext.setAttribute("cvvEnabled", cvvEnabled, PageContext.REQUEST_SCOPE);
%>

<c:choose>
  <c:when test="${bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_THM' || bookingComponent.accommodationSummary.accommodationInventorySystem== 'HOPLA_PEG'}">
    <script type="text/javascript">
      isHopla = true;
    </script>
    <input type='hidden'  name='payment_0_paymentmethod' id='payment_0_paymentmethod' value='Postpayment'/>
    <input  type='hidden' id='payment_totalTrans' name='payment_totalTrans' value='0'/>
  </c:when>
  <c:otherwise>
    <input type='hidden'  name='payment_0_paymentmethod' id='payment_0_paymentmethod' value='Dcard'/>
    <input  type='hidden' id='payment_totalTrans' name='payment_totalTrans' value='1'/>
  </c:otherwise>
</c:choose>
<div id="paymentFields_required">
  <input type='hidden'  id='panelType' value='CNP'/>
  <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
    <c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'     id='${paymentType.paymentCode}_enabled' name='payment_code_${paymentType.paymentCode} '/>"    escapeXml="false"/>
    <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>
    <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>
    <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>
  </c:forEach>
  <c:forEach var="paymentType" items="${bookingComponent.paymentType['PPG']}">
    <c:out value="<input type='hidden' value='${paymentType.paymentCode}_enabled'  id='${paymentType.paymentCode}_enabled'  name='payment_code_${paymentType.paymentCode} '/>" escapeXml="false"/>
    <c:out value="<input type='hidden' value='${paymentType.cardType.isIssueNumberRequired}'  id='${paymentType.paymentCode}_issueNo' />" escapeXml="false"/>
    <c:out value="<input type='hidden' value='${paymentType.cardType.securityCodeLength}'  id='${paymentType.paymentCode}_securityCodeLength' />" escapeXml="false"/>
    <c:out value="<input type='hidden' value='${paymentType.cardType.isStartDateRequired}'  id='${paymentType.paymentCode}_startDateRequired' />" escapeXml="false"/>
  </c:forEach>
</div>

<div class="subheaderBar">
  <h2>Card details</h2>
</div>

<div class="optionDetails">
  <p>The 'Lead Passenger' must be the passenger making the booking and providing credit card details.</p>

  <ul id="paymentSelections" class="cardDetails">
    <li>
      <p>Card type</p>
      <select id='payment_type_0' name='payment_0_type' onchange='handleCardSelection(0);changePayButton();' autocomplete="off" >
        <option title="pleaseSelect" value='' selected="selected">Card Type</option>
        <c:forEach var="paymentType" items="${bookingComponent.paymentType['CNP']}">
          <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/>'><c:out value="${paymentType.paymentDescription}" escapeXml='false'/></option>
        </c:forEach>
        <c:forEach var="paymentType" items="${bookingComponent.paymentType['PPG']}">
          <option value='<c:out value='${paymentType.paymentCode}' escapeXml='false'/>|<c:out value='${paymentType.paymentMethod}' escapeXml='false'/> '><c:out value="${paymentType.paymentDescription}" escapeXml='false'/></option>
        </c:forEach>
      </select>
      <span>*</span>
    </li>
    <li>
      <p>Card number</p>
      <input type="text"  maxlength='20' id='payment_0_cardNumberId' name='payment_0_cardNumber' value=''  autocomplete="off" />
      <span>*</span>
      <input type='hidden' />
    </li>
    <li class="nameOnCard">
      <p>Name on card</p>
      <input type="text"  maxlength='25' id='payment_0_nameOnCardId' name='payment_0_nameOnCard' value=''  autocomplete="off" />
      <span>*</span>
    </li>
    
  </ul>

  <ul class="cardDetails">
    <li class="expiryDate">
      <p>Expiry date</p>
      <select id='payment_0_expiryMonthId' name='payment_0_expiryMonth'>
        <option value="MM">MM</option>
        <option value='01'>01</option>
        <option value='02'>02</option>
        <option value='03'>03</option>
        <option value='04'>04</option>
        <option value='05'>05</option>
        <option value='06'>06</option>
        <option value='07'>07</option>
        <option value='08'>08</option>
        <option value='09'>09</option>
        <option value='10'>10</option>
        <option value='11'>11</option>
        <option value='12'>12</option>
      </select>
      <select  id='payment_0_expiryYear' name='payment_0_expiryYear'>
        <option value="YY">YY</option>
        <c:forEach var="expiryYear" items="${bookingInfo.expiryYearList}">
          <option value='<c:out value='${expiryYear}'/>'><c:out value='${expiryYear}'/></option>
        </c:forEach>
      </select>
      <span>*</span>
    </li>
    <li class="securityCode">
      <p>Security code</p>
      <input type="text" maxlength='4' id='payment_0_securityCodeId'  name='payment_0_securityCode' value='' autocomplete="off"/>
      <span>*</span>
    </li>
    <li class="securityDigits">
      <p id='securityCodeHelp0'></p>
    </li>
    <li id="debitcards0" class="issueNumber">
      <p id="IssueNumberTitle">Issue number </p>
      <input type='text' maxlength='2' id='IssueNumberInput' name='payment_0_issueNumber' value='' autocomplete="off"/>
      <p class="debitCardsOnly">(For Maestro / Solo Card users only)</p>
      <input type='hidden'  id='IssueNumberHiddenAlert'/>
    </li>
	</ul>
    <div class="payCards">
	<ul>
	<li class="cardSecurity">
       <c:set var="mastercardStickyLink" value="false"/>
       <c:set var="visaStickyLink" value="false"/>
	   <c:set var="amexStickyLink" value="false"/>
       <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
          <c:if test="${threeDLogos == 'mastercardgroup'}">
              <c:set var="mastercardStickyLink" value="true"/>
              <a class="masterCard stickyOwner" id="masterCardDetails" title="MasterCard SecureCode" href="javaScript:void(0);">MasterCard SecureCode</a>
          </c:if>
       </c:forEach>
       <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
          <c:if test="${threeDLogos == 'visagroup'}">
              <c:set var="visaStickyLink" value="true"/>
              <a class="visa stickyOwner" id="visaDetails" title="Verified by VISA" href="javaScript:void(0);">Verified by VISA</a>
          </c:if>
       </c:forEach>
	   <c:forEach var="threeDLogos" items="${bookingInfo.threeDEnabledLogos}">
          <c:if test="${threeDLogos == 'americanexpressgroup'}">
              <c:set var="amexStickyLink" value="true"/>
              <a class="amex stickyOwner" id="amexDetails" title="American Express SafeKey" href="javaScript:void(0);">American Express SafeKey</a>
          </c:if>
       </c:forEach>
    </li>

  <li class="cardSecurityLinks">
      <c:if test="${mastercardStickyLink == 'true'}">
      <div class="mastercardLearnMoreDiv">
        <a href="javascript:void(0);" id="masterCardDetails" class="sticky stickyOwner learnMoreI masterCardLink" title="Learn more">Learn more</a>
        <%@ include file="/common/mastercardLearnMoreSticky.jspf"%>
      </div>
      </c:if>
      <c:if test="${visaStickyLink == 'true'}">
      <div class="visaLearnMoreDiv">
        <a href="javascript:void(0);" id="visaDetails" class="sticky stickyOwner learnMoreI visaLink" title="Learn more">Learn more</a>
        <%@ include file="/common/visaLearnMoreSticky.jspf"%>
      </div>
    </c:if>
	<c:if test="${amexStickyLink == 'true'}">
      <div class="amexLearnMoreDiv">
        <a href="javascript:void(0);" id="amexDetails" class="sticky stickyOwner learnMoreI amexLink" title="Learn more">Learn more</a>
        <%@ include file="/common/amexLearnMoreSticky.jspf"%>
      </div>
    </c:if>
 </li>

  </ul>
    </div>

      <div class="cardCharges">
        <a title="Credit card charges" class="inline-link" href="javascript:PopupForFcao('https://www.thomson.co.uk/editorial/legal/credit-card-payments.html?ico=AOPayment_CardCharges&popup=true&section=holidays&topic=secure#4',800,600,'scrollbars=1,resizable=1');"><span>There are no additional charges when paying by Maestro, MasterCard Debit or Visa/ Delta debit cards.
         A fee of <fmt:formatNumber value="${cardCharge}" type="number" var="confCardCharge" maxFractionDigits="1" minFractionDigits="0"/><c:out value="${confCardCharge}"/>% applies to credit card payments, which is capped at �<c:out value="${maxCardCharge}"/> per transaction when using American Express, MasterCard Credit or Visa Credit cards
     </span></a>
      </div>

    <%@include file="thaoCardholderAddress.jsp"%>
</div><!-- END optionDetails -->

<script type="text/javascript">

  if(newHoliday=="false")
  {
   //Following amount is applicable for Hopla
   PaymentInfo.hoplaTotalAmount =0;
   if($("hoplaTotalAmount"))
   {
     PaymentInfo.hoplaTotalAmount =  stripChars(($("hoplaTotalAmount").innerHTML)," "+getCurrency());
   }
    if($("cardChargeAmount") && $("cardChargeAmount").innerHTML!="")
    {
      PaymentInfo.totalCardCharge = stripFirstChar($("cardChargeAmount").innerHTML);
      displayFieldsWithValuesForCard();
    }
  }

  window.onload = function()
  {
   checkBookingButton("checkConfirmButton");
   setToDefault();
   //Following amount is applicable for Hopla
   PaymentInfo.hoplaTotalAmount =0;
   if($("hoplaTotalAmount"))
   {
     PaymentInfo.hoplaTotalAmount =  stripChars(($("hoplaTotalAmount").innerHTML)," "+getCurrency());
   }

    if($("paymentSelections")== undefined)
    {
        return;
    }
    if($("importantInfo") != null )
    {
    if (trimSpaces($("importantInfo").innerHTML) == "" )
        $("importantInfo").style.display = "none";
    else
        $("importantInfo").style.display = "block";
    }
    $("securityCodeHelp0").innerHTML = "("+checkForDispCardText('',3)+")";
    <c:if test="${cvvEnabled=='true'}">
      $("confirmLeadPassengerAddress").style.display = "block";
    </c:if>
  }
  jQuery(document).ready(function()
  {
      toggleTHAOOverlay();
  });
</script>
<!--maximum card charge and applicable charge cap -->
<input  name="cardDetailsFormBean.cardCharge" id="cardCharge" value="0.0" type="hidden"/>
<input  name="cardDetailsFormBean.cardLevyPercentage" id="cardLevyPercentage" value="0.0" type="hidden"/>
<input  name="updateFlag" id="updateFlag" value="false" type="hidden"/>
<input  type='hidden' id='total_transamt' name='total_transamt' value='NA'/>
<input type="hidden"  id="payment_0_totalTrans" value="1" name="payment_totalTrans"/>
