<%@include file="/common/commonTagLibs.jspf"%>

<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request"/>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1" />
    <version-tag:version/>
    <title>Thomson Accommodation Only | Passenger details and payment</title>
    <c:set var="clientapp" value="${bookingComponent.clientApplication.clientApplicationName}"/>
    <%
      String clientApp = (String)pageContext.getAttribute("clientapp");
      String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
      pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
    %>
    <!-- Include Payment related CSS  here -->
    <link href="/cms-cps/thomson/thao/css/thao_cps.css" rel="stylesheet" type="text/css" media="screen,print" />
    <!--[if lte IE 6]>
          <link type="text/css" rel="stylesheet" href="/cms-cps/thomson/thao/css/thao_ie6.css" />

          <%--  This script is added to remove image caching in ie 6 which was a main cause for me
          losing sleep for days :-) This solves the flickering images problem in IE6.--%>

          <script type="text/javascript">
             try
             {
		        document.execCommand("BackgroundImageCache", false, true);
	         }
	         catch(err) { }
          </script>
    <![endif]-->
    <!-- End of css files -->

    <!-- Include Payment related JS Here -->
	<script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
    <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
    <script type="text/javascript" src="/cms-cps/thomson/thao/js/genericFormValidation.js"></script>
    <script type="text/javascript" src="/cms-cps/thomson/thao/js/general.js"></script>
    <script type="text/javascript" src="/cms-cps/thomson/thao/js/paymentUpdation.js"></script>
    <script type="text/javascript" src="/cms-cps/thomson/thao/js/popups.js"></script>
    <script type="text/javascript" src="/cms-cps/thomson/thao/js/sifr.js"></script>
    <!-- End of JS files -->
    <link rel="shortcut icon" href="/cms-cps/thomson/thao/images/favicon/favicon.ico"/>
    <link rel="icon" href="/cms-cps/thomson/thao/images/favicon/favicon.ico"/>
    <!-- Modified to add ensighten in head section -->
<%@include file="tag.jsp"%>
  </head>
  <body>
  <jsp:include page="/thomson/thao/cookielegislation.jsp"/>
  <c:set var="visiturl" value="yes" scope="session" />
    <%--Contains Essential configuration settings used thru out the page --%>
    <%@include file="thaoConfigSettings.jspf"%>

    <div id="Page">
      <jsp:include page="thaoHeader.jsp" />

      <div id="PageColumn1">
        <div id="MainContent">
          <form name="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}'/>&b=<c:out value='${param.b}'/>&tomcat=<c:out value='${param.tomcat}' />" onsubmit="updateEssentialFields();return  validateForm();">
            <div id="PrimaryColumn">
              <jsp:include page="thaoSummaryPanel.jsp"/>
            </div>
            <jsp:include page="thaoBreadCrumb.jsp" />
            <jsp:include page="thaoPassengerandPayment.jsp"/>
          </form>
          <!-- <img src="https://www.firstchoice.co.uk/canberra/sale?AMOUNT='ORDER REVENUE'&CID=46&OID='ORDER NUMBER'&TYPE=1" height="1" width="1" alt=""/> -->
        </div>

        <%-- Omniture implimentation starts --%>
        <%@include file="/common/intellitrackerSnippet.jspf" %>
        <script type='text/javascript'>isl=isl +"tui.intelli-direct.com/e/t3.dll?249&";</script>
        <%--Modified to remove existing tag --%>

        <%-- Omniture imlementation ends  --%>
      </div><%--  END #PageColumn1 --%>
    </div>
    <jsp:include page="thaoFooter.jsp"/>
  </body>
</html>