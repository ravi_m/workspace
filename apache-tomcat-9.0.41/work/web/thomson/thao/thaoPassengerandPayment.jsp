<%@include file="/common/commonTagLibs.jspf"%>
<c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" />
<div id="contentCol" class="paxPayment">
  <div id="TitleArea">
    <h1>Passenger details and payment</h1>
  </div>
  <%@include file="thaoWarning.jsp" %>
  <%@include file="thaoImportantInfo.jsp" %>

  <div class="optionSection">
    <%@include file="thaoPassengerDetails.jsp" %>
  </div><!-- END Personal details -->
  <div class="optionSection">
    <%@include file="thaoPaymentDetails.jsp" %>
  </div><!-- END Card details -->
  <div class="optionSection">
    <%@include file="thaoTandC.jsp" %>
  </div><!-- END Terms and conditions -->
  <div class="btnContinue">
    <a href="<c:out value='${bookingComponent.prePaymentUrl}' escapeXml='false'/>" class="arrow-link-left" title="Back to travel options">Back to travel options</a>

    <c:if test="${bookingInfo.newHoliday}">
      <span id="confirmButton">
          <input type="image" src="/cms-cps/thomson/thao/images/buttons/form/pay.gif" alt="Pay" title="Pay"/>
      </span>
    </c:if>
  </div>
</div><!-- END #contentCol -->