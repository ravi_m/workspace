<%@include file="/common/commonTagLibs.jspf"%>

  <div id="Header">
    <div id="toyBFhdrDiv">
      <a href="http://www.thomson.co.uk" title="www.thomson.co.uk" class="hdrLogo"><img src="/cms-cps/thomson/thao/images/icons/logo-thomson.gif" width="192" height="50" alt="Thomson" /></a>
      <ul>
        <li class="hdrPHolidays"><a href="http://www.thomson.co.uk/package-holidays.html" title="Package holidays">Package holidays</a></li>
        <li><a href="http://flights.thomson.co.uk/en/index.html" title="Flights">Flights</a></li>
        <li><a href="http://www.thomson.co.uk/hotels.html" title="Hotels">Hotels</a></li>
     
        <li><a href="http://www.thomson.co.uk/cruise.html" title="Cruises">Cruises</a></li>
        <li><a href="http://www.thomson.co.uk/villas.html" title="Villas">Villas</a></li>
        <li><a href="http://www.thomson.co.uk/deals.html" title="Deals">Deals</a></li>
        <li><a href="http://www.thomson.co.uk/holiday-extras.html" title="Extras">Extras</a></li>
        <li class="hdrDestinations"><a href="http://www.thomson.co.uk/holiday-destinations.html" title="Destinations">Destinations</a></li>
      </ul>
    </div>
  </div><!-- END Header -->