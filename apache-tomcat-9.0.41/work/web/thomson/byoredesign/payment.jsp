<%@include file="/common/commonTagLibs.jspf"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <title>Payments</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	  <!--[if lt IE 9]>
  <script type="text/javascript" src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
      <version-tag:version/>
      <%@include file="javascript.jspf" %>
      <%@include file="configSettings.jspf" %>
      <%@include file="css.jspf" %>
      <%@include file="tag.jsp"%>
      <%

         String clientApp = (String)pageContext.getAttribute("clientapp");
         String axaInsurance = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".axaInsuranceEnabled","false");
         pageContext.setAttribute("axaInsurance", axaInsurance, PageContext.SESSION_SCOPE);
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
      %>
	  
   </head>

   <body>
   <style>
   @font-face {
      font-family: 'TUITypeLightRegular';
      src: url('/cms-cps/thomson/byoredesign/css/webfont/webfont.tuitypelt.eot');
      src: local('TUITypeLightRegular'),
      url('/cms-cps/thomson/byoredesign/css/webfont/webfont.tuitypelt.woff') format('woff'),
      url('/cms-cps/thomson/byoredesign/css/webfont/webfont.tuitypelt.ttf') format('truetype'),
      url('/cms-cps/thomson/byoredesign/css/webfont/webfont.tuitypelt.svg#webfontXOtQpBc2') format('svg');
      font-weight: normal;
      font-style: normal;
   }
   @font-face {
      font-family: 'TUITypeRegular';
      src: url('/cms-cps/thomson/byoredesign/css/webfont/webfont.tuitype.eot');
      src: local('TUITypeRegular'),
      url('/cms-cps/thomson/byoredesign/css/webfont/webfont.tuitype.woff') format('woff'),
      url('/cms-cps/thomson/byoredesign/css/webfont/webfont.tuitype.ttf') format('truetype'),
      url('/cms-cps/thomson/byoredesign/css/webfont/webfont.tuitype.svg#webfontXOtQpBc2') format('svg');
      font-weight: normal;
      font-style: normal;
   }
   </style>
   <script>
$(document).ready(function () {
"use strict";
	$(".tabHeadings").on('click', function () {
	var $parent = $(this).parent();
	if (!$parent.hasClass("active")) {
	$(".footerTabHeader").removeClass("active");
	$parent.addClass("active");
	}

	return false;
	});
	
	jQuery("#breadcrumb a").click(function(event){
         event.preventDefault();
      });
});
</script>
   <!-- Modified for ensighten to remove DoubleClick Floodlight Tag -->
	  <div class="pageContainer" id="payments">
	     <%@include file="header.jspf" %>
	     <div class="mboxDefault">
         </div>
         <script type="text/javascript">
            mboxCreate("T_PackagePayment_Text");
         </script>
		 <div id="contentSection">
		    <%@include file="breadCrumb.jspf" %>
<div class="clearboth paddingtop10"></div>
			<h1>Passenger &amp; payment details</h1>
			<div class="clearboth paddingtop10">
            <p class="intro">Your holiday booking is nearly complete</p>

            <div id="contentCol2">
			   <form id="paymentdetails" action="/cps/processPayment?token=<c:out value='${param.token}'/>&amp;b=<c:out value='${param.b}'/>&amp;tomcat=<c:out value='${param.tomcat}' />" method="post" autocomplete="off">
			      <%@include file="topError.jspf" %>
				  <div class="flexiBox greyContainer_">
				     
					 <div class="middle1">
					    <span class="shadow">&nbsp;</span>
					    <div class="body">
			               <%@include file="passengerDetails.jspf" %>
			               <%@include file="promotionalCode.jspf" %>
			               <c:choose>
					          <c:when test="${depositOptionsPresent == 'true'}">
					             <%@include file="paymentOptions.jspf" %>
					          </c:when>
					          <c:otherwise>
					             <%@include file="fullPayment.jspf" %>
					          </c:otherwise>
					       </c:choose>
                           <%@include file="paymentDetails.jspf" %>
                           <%@include file="importantInformation.jspf" %>
					    </div>
					 </div>
				    <c:if test="${bookingInfo.newHoliday}">
			         <%@include file="bookHoliday.jspf" %>
                        </c:if>
				  </div>
                        
			   </form>
		    </div>
            <div id="contentCol1">
               <%@include file="summaryPanel.jspf" %>
	        </div>
			<!--<div class="secureImage">
               <img src="/cms-cps/thomson/byoredesign/images/footer/banner_help.png"/>
            </div>-->
		    
			</div>
       	 </div>
		  <div class="contentSection_footer">
		  <div id="footerSection">
		       <%@include file="footer.jspf" %>
	        </div>
      </div>
      <!-- Modified for ensighten to remove existing tag -->
     <script type="text/javascript">
       window.onload=
        function()
        {
    	   DepositTypeChangeHandler.focusSelectedDepositType();
        }
      </script>
	   

   </body>
</html>