<%@include file="/common/commonTagLibs.jspf"%>
<li>
   <c:if test="${passengerRoom.ageClassification == 'CHILD'}">
      <p class="paxNumber"><c:out value="${passengerRoom.label}"/><br/>(age:<c:out value="${passengerRoom.age}" />)</p>
   </c:if>
   <c:if test="${passengerRoom.ageClassification == 'INFANT'}">
      <p class="paxNumber"><c:out value="${passengerRoom.label}"/><br/>(age: <2)</p>
   </c:if>
   <c:set var="titlekey" value="personaldetails_${passengerCount}_title"/>
   <div class="title">
      <select name="<c:out value='${titlekey}' />" alt="Please select the title of passenger ${passengerCount+1}.|Y|TITLE">
	   <option value="" selected></option>
         <option value="Mstr" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Mstr'}">selected</c:if>>Mstr</option>
		 <option value="Miss" <c:if test="${bookingComponent.nonPaymentData[titlekey] == 'Miss'}">selected</c:if>>Miss</option>
      </select>
   </div>

   <c:set var="foreNameKey" value="personaldetails_${passengerCount}_foreName"/>
   <div class="firstName">
      <input type="text" maxlength="15" value="<c:out value="${bookingComponent.nonPaymentData[foreNameKey]}"/>" id="<c:out value="${foreNameKey}"/>" name="<c:out value="${foreNameKey}"/>" alt="Please enter the names of all passengers as shown on your passports.|Y|NAME"/>
   </div>

   <c:set var="middleNameKey" value="personaldetails_${passengerCount}_middleInitial"/>
   <div class="middleInitial">
      <input type="text" maxlength="1" value="<c:out value="${bookingComponent.nonPaymentData[middleNameKey]}"/>" name="<c:out value="${middleNameKey}"/>" alt="Initial|N|ALPHA"/>
   </div>

   <c:set var="lastNameKey" value="personaldetails_${passengerCount}_surName"/>
   <div class="surname">
      <input type="text" onclick="javascript:unCheck()" maxlength="15" class="medium" value="<c:out value="${bookingComponent.nonPaymentData[lastNameKey]}"/>" id="lastname_<c:out value='${passengerCount}'/>"  name="<c:out value="${lastNameKey}"/>" alt="Please enter the names of all passengers as shown on your passports.|Y|NAME"/>
   </div>
   <input type="hidden" name="personaldetails_${passengerCount}_ageClassificationCode" value="<c:out value = '${passengerRoom.ageClassification}'/>"/>
</li><%-- END Child or infant passenger details --%>