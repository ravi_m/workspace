<%@include file="/common/commonTagLibs.jspf"%>
<div class="roomAllocation">
   <c:set var="numberOfPassengersNew" value="0" scope="request"/>
   <c:forEach var="passengerRoomSummary" items="${bookingComponent.passengerRoomSummary}" varStatus="status">
      <c:set var="noOfRooms" value="${status.count}"/>
      <c:set var="noOfCabins" value="${status.count}"/>
      <c:forEach var="passenger" items="${passengerRoomSummary.value}" varStatus="passen">
         <c:set var="numberOfPassengersNew" value="${numberOfPassengersNew+1 }" scope="request"/>
      </c:forEach>
      <c:if test="${noOfRooms == '2'}">
   		<div class="newNotificationBlock">
			<p>
				Don't worry if the lead passenger isn't in Cabin 1. Complete your booking as normal then give us a call on 0871 230 2800, we'll make sure everyone's in the right cabin, at no extra charge.</p>
		</div>
   	</c:if>
   </c:forEach>
   <c:set var="numberOfPassengers" scope="request"/>
   <c:set var="passengerCount" value="0" scope="request"/>
   <c:forEach var="passenger" items="${bookingComponent.passengerRoomSummary}" varStatus="status">
      <c:if test="${noOfRooms > 1}">
	     <p class="roomNumber">Cabin&nbsp;<c:out value="${status.count}" />&nbsp;:- </p>
      </c:if>
      <c:forEach var="passengerRoom" items="${passenger.value}" varStatus="passen">
	     <c:set var="numberOfPassengers" value="${passen.index}"/>
         <c:set var="identifier" value="passenger_${passengerCount}_identifier"/>
         <input type = "hidden" name="<c:out value="${identifier}"/>" value = "<c:out value = '${passengerRoom.identifier}'/>"/>
         <ul class="roomConfig">
		    <c:choose>
		       <c:when test="${passengerRoom.ageClassification == 'ADULT' || passengerRoom.ageClassification == 'SENIOR' || passengerRoom.ageClassification == 'SENIOR2'}">
                  <%@include file="adultsPassengerDetailsWithoutInsuranceChanges.jsp" %>
		       </c:when>
		       <c:otherwise>
                  <%@include file="childPassengerDetailsWithoutInsuranceChanges.jsp" %>
		       </c:otherwise>
		    </c:choose>
         </ul><%-- END Room allocation details --%>
		 <c:set var="passengerCount" value="${passengerCount+1}" />
	  </c:forEach>
	  <c:if test="${(noOfCabins) > 1}"><div class="cabinGreyLineNoInsurance"></div></br>
	 	 <c:set var="noOfCabins" value="${noOfCabins-1}" />
	  </c:if>
   </c:forEach>
</div><%-- END roomAllocation --%>
<script type="text/javascript">
   var totalPassenCount=<c:out value='${numberOfPassengers}'/>;
   var totalPassengerCount=<c:out value='${numberOfPassengersNew}'/>;
</script>