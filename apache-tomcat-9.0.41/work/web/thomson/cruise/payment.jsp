<%@include file="/common/commonTagLibs.jspf"%>
<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://www.tui.com/tags-version" prefix="version-tag" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      <version-tag:version/>
      <title>Build Your Own Holidays  - Cruises - Payment</title>
      <c:set var="bookingComponent" value="${bookingInfo.bookingComponent}" scope="request"/>
      <c:set var="clientapp" value="${bookingComponent.clientApplication.clientApplicationName}"/>
      <c:set var="visiturl" value="yes" scope="session" />
      <%
         String clientApp = (String)pageContext.getAttribute("clientapp");
         String is3DSecure = com.tui.uk.config.ConfReader.getConfEntry(clientApp+".3DSecure", "");
         pageContext.setAttribute("is3DSecure", is3DSecure, PageContext.REQUEST_SCOPE);
      %>
      <%
		 String getHomePageURL =(String)ConfReader.getConfEntry("ThomsonCruise.homepage.url","");
		 pageContext.setAttribute("getHomePageURL",getHomePageURL);
	  %>
	  <c:choose>
		 <c:when test="${not empty bookingInfo.bookingComponent.clientURLLinks.homePageURL}">
		  	<c:set var='homePageURL' value="${bookingInfo.bookingComponent.clientURLLinks.homePageURL}" />
	     </c:when>
	     <c:otherwise>
	  	  	<c:set var='homePageURL' value='${getHomePageURL}'/>
	     </c:otherwise>
	  </c:choose>
      <link href="/cms-cps/thomson/cruise/css/cps.css" rel="stylesheet" type="text/css" />
      <link href="/cms-cps/thomson/cruise/css/error.css" rel="stylesheet" type="text/css" />
      <link type="image/x-icon" rel="icon" href="/cms-cps/thomson/cruise/images/favicon.ico"/>
      <link type="image/x-icon" rel="shortcut icon" href="/cms-cps/thomson/cruise/images/favicon.ico"/>
      <script type="text/javascript" src="/cms-cps/common/js/jquery-1.3.2.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/prototype.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/cardCharges.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/commonAjaxCalls.js"></script>
      <script type="text/javascript" src="/cms-cps/common/js/utilFunctions.js"></script>
      <script type="text/javascript" src="/cms-cps/thomson/cruise/js/paymentUpdation.js"></script>
      <script type="text/javascript" src="/cms-cps/thomson/cruise/js/formvalidation.js"></script>
      <script type="text/javascript" src="/cms-cps/thomson/cruise/js/popup.js"></script>
      <!--[if lte IE 6]>
       <%--  This script is added to remove image caching in ie 6 which was a main cause for me
       losing sleep for days :-) This solves the flickering images problem in IE6.--%>
       <script type="text/javascript">
         try
         {
	        document.execCommand("BackgroundImageCache", false, true);
         }
         catch(err) { }
       </script>
      <![endif]-->
      <!--[if lt IE 8]>
			<link rel="stylesheet" type="text/css" href="/cms-cps/thomson/cruise/css/ie6.css" />
	  <![endif]-->

<%-- ensighten starts --%>
   <%@include file="tag.jsp"%>
<%-- ensighten ends --%>
   </head>

   <body>

      <script type="text/javascript">
         var session_timeout= ${bookingInfo.bookingComponent.sessionTimeOut};
	  	 var IDLE_TIMEOUT = (${bookingInfo.bookingComponent.sessionTimeOut})-(${bookingInfo.bookingComponent.sessionTimeAlert});
	  	 var _idleSecondsCounter = 0;
	  	 var IDLE_TIMEOUT_millisecond = IDLE_TIMEOUT *1000;
	  	 //var url="${bookingComponent.breadCrumbTrail['HOTEL']}";
	  	 var url="${homePageURL}";
	  	 var sessionTimeOutFlag =  false;
         
	  	 window.setInterval(CheckIdleTime, 1000);
		 window.alertMsg();
		 
		 function noback()
		 {
			 window.history.forward();
		 }
		 
		 function alertMsg()
		 {
			 var myvar = setTimeout(function(){
			 document.getElementById("sessionTime").style.visibility = "visible";
			 document.getElementById("modal").style.visibility = "visible";
			 document.getElementById("sessionTimeTextbox").style.visibility = "visible";
			 var count = Math.round((session_timeout - _idleSecondsCounter)/60);
			 document.getElementById("sessiontimeDisplay").innerHTML = count +"mins";
			 },IDLE_TIMEOUT_millisecond);
		 }
		 
		 function CheckIdleTime()
		 {
			_idleSecondsCounter++;

		    if (  _idleSecondsCounter >= session_timeout)
		    {
		    	if (window.sessionTimeOutFlag == false)
		    	{
		    		document.getElementById("sessionTime").style.visibility = "hidden";
		    		document.getElementById("modal").style.visibility = "hidden";
		    		document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
		    		document.getElementById("sessionTimeout").style.visibility = "visible";
		    		document.getElementById("modal").style.visibility = "visible";
		    		document.getElementById("sessionTimeOutTextbox").style.visibility = "visible";
		    	}
		    	else
		    	{
		    		document.getElementById("sessionTimeout").style.visibility = "hidden";
		    		document.getElementById("modal").style.visibility = "visible";
		    		document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";
		    	}
			   //navigate to technical difficulty page

			}
		 }
		 function closesessionTime()
		 {
			 document.getElementById("sessionTime").style.visibility = "hidden";
		     document.getElementById("modal").style.visibility = "hidden";
			 document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
		 }
		 
		 function reloadPage()
		 {
			 window.sessionTimeOutFlag= true;
			 document.getElementById("sessionTimeout").style.visibility = "hidden";
			 document.getElementById("modal").style.visibility = "hidden";
			 document.getElementById("sessionTimeOutTextbox").style.visibility = "hidden";
			 window.noback();
			 window.location.replace(window.url);
			 return(false);
		 }
		 
		 function homepage()
		 { 
			 url="${homePageURL}";
			 window.noback();
			 window.location.replace(url);
		 }
		 function activestate()
		 {
			 document.getElementById("sessionTime").style.visibility = "hidden";
			 document.getElementById("modal").style.visibility = "hidden";
			 document.getElementById("sessionTimeTextbox").style.visibility = "hidden";
			 window.ajaxForCounterReset();
			 window.alertMsg();
			 window._idleSecondsCounter = 0;
		 }
		
		 function ajaxForCounterReset()
		 {
			 var token = "<c:out value='${param.token}' />";
			 var url = "/cps/SessionTimeOutServlet?tomcat=<c:out value='${param.tomcat}'/>";
			 var request = new Ajax.Request(url, {
				 method : "POST"
			 });
			 window._idleSecondsCounter = 0;
		 }

		 function newPopup(url)
		 {
			 var win = window.open(url,"","width=700,height=600,scrollbars=yes,resizable=yes");
			 if (win && win.focus) win.focus();
		 }
      
         var lessSeniorPassengersAged65To75Message="The number of passengers aged 65 - 74 are less than those advised on the previous page. Please check and amend.";
         var moreSeniorPassengersAged65To75Message="The number of passengers aged 65 - 74 exceed those advised on the previous page. Please check and amend.";
         var lessSeniorPassengersAged76To84Message="The number of passengers aged 75+ are less than those advised on the previous page. Please check and amend.";
         var moreSeniorPassengersAged76To84Message="The number of passengers aged 75+ exceed those advised on the previous page. Please check and amend.";
         var tomcatInstance= "&tomcat=<c:out value='${param.tomcat}' />";
         var token = "<c:out value='${param.token}' />";
         var threeDCards = new Map();
         <c:forEach var="threeDCards" items="${bookingInfo.payDescription}">
             threeDCards.put("<c:out value="${threeDCards.key}"/>","<c:out value="${threeDCards.value}"/>");
         </c:forEach>
         var BookingConstants = {PAY_CLASS:"pay", PAY_DESCRIPTION:"Pay"};
         jQuery(document).ready(function()
         {
        	 toggleCruiseOverlay();
         })
      </script>
<style>
   @font-face {
      font-family: 'TUITypeLightRegular';
      src: url('/cms-cps/thomson/cruise/css/webfont/webfont.tuitypelt.eot');
      src: local('TUITypeLightRegular'),
      url('/cms-cps/thomson/cruise/css/webfont/webfont.tuitypelt.woff') format('woff'),
      url('/cms-cps/thomson/cruise/css/webfont/webfont.tuitypelt.ttf') format('truetype'),
      url('/cms-cps/thomson/cruise/css/webfont/webfont.tuitypelt.svg#webfontXOtQpBc2') format('svg');
      font-weight: normal;
      font-style: normal;
   }
   @font-face {
      font-family: 'TUITypeRegular';
      src: url('/cms-cps/thomson/cruise/css/webfont/webfont.tuitype.eot');
      src: local('TUITypeRegular'),
      url('/cms-cps/thomson/cruise/css/webfont/webfont.tuitype.woff') format('woff'),
      url('/cms-cps/thomson/cruise/css/webfont/webfont.tuitype.ttf') format('truetype'),
      url('/cms-cps/thomson/cruise/css/webfont/webfont.tuitype.svg#webfontXOtQpBc2') format('svg');
      font-weight: normal;
      font-style: normal;
   }
</style>


   <%-- This section sets the page level flag which indicates whether insurance changes are applicable
        or not. --%>
   <c:set var="isInsuranceChangesApplicable" value="false"/>
   <c:forEach var="passenger" items="${bookingComponent.passengerRoomSummary}" varStatus="status">
      <c:forEach var="passengerRoom" items="${passenger.value}" varStatus="passen">
         <c:if test="${not empty passengerRoom.isInsuranceSelected}">
            <c:set var="isInsuranceChangesApplicable" value="true"/>
         </c:if>
      </c:forEach>
   </c:forEach>
   <script type="text/javascript">
      var isInsuranceChangesApplicable = <c:out value='${isInsuranceChangesApplicable}'/>
   </script>
   <%-- End of section --%>
 <jsp:include page="/thomson/cruise/cookielegislation.jsp"/>
      <div class="wrapper">
         <div id="header">
            <%@include file="header.jspf" %>
            <div class="mboxDefault">
            </div>
            <script type="text/javascript">
               mboxCreate("T_CruisePayment_Text");
            </script>
         </div><%-- END #header --%>
         <form name="paymentdetails" method="post" action="/cps/processPayment?token=<c:out value='${param.token}'/>&amp;brand=<c:out value='${param.brand}'/>&amp;tomcat=<c:out value='${param.tomcat}' />&amp;b=<c:out value='${param.b}'/>">
            <div id="contentContainer">
               <div class="sideColumn">
                  <%@include file="summaryPanel.jspf" %>
                  <%@include file="aToZinformation.jspf" %>
               </div><%-- END sideColumn --%>

               <div class="mainContent">
                  <%@include file="progressIndicators.jspf" %>
                  <div class="titleArea">
                     <h1>Payment Details</h1>
                  </div>
                  <c:if test="${requestScope.disablePaymentButton == 'true'}">
	                  <c:if test="${not empty requestScope.downTimeErrorMsg}">
		                  <div id="downtimeerror" class="warning">
		                  	<h2>
		                  		<c:out value="${requestScope.downTimeErrorMsg}" escapeXml="false"/>
		                  	</h2>
		                  </div>
	                  </c:if>
                  </c:if>
	              <div id="pricediff1" class="optionSection"  <c:if test="${fn:contains(bookingComponent.errorMessage, 'increased') || fn:contains(bookingComponent.errorMessage, 'decreased') || fn:contains(bookingComponent.errorMessage, 'Promotional') || fn:contains(bookingComponent.errorMessage, 'PROMOTIONAL') || fn:contains(bookingComponent.errorMessage, 'promotional')}"> style="color:#3366CC;"
                   </c:if> <c:if test="${empty bookingComponent.errorMessage}"> style="display:none" </c:if>>
                     <c:out value="${bookingComponent.errorMessage}" escapeXml="false"/>
                  </div>
				  <c:if test="${not empty bookingComponent.importantInformation.errata}">
                     <c:forEach var="errata" items="${bookingComponent.importantInformation.errata}">
                        <c:if test="${errata !='No errata' &&  not empty errata}">
                           <c:set var="displayImportantSection" value="true" scope="request"/>
                        </c:if>
                     </c:forEach>
                  </c:if>
                  <%@include file="timeoutInfo.jspf"%>
				  <c:if test="${displayImportantSection}">
                     <%@include file="importantInformation.jspf" %>
				  </c:if>
				  <c:choose>
				     <c:when test="${isInsuranceChangesApplicable}">
				        <%@include file="personalDetails.jspf" %>
				     </c:when>
				     <c:otherwise>
				        <%@include file="personalDetailsWithoutInsuranceChanges.jsp" %>
				     </c:otherwise>
				  </c:choose>

                  <%@include file="discountCode.jspf" %>
                  <%-- TODO Need to include CardDetails.jspf --%>
                  <%@include file="cardDetails.jspf" %>
	              <c:choose>
	                 <c:when test="${not empty bookingComponent.depositComponents && bookingComponent.packageType.code != 'DP' && bookingComponent.flightSummary.flightSupplierSystem != 'NAV'}">
                        <%@include file="paymentAmount.jspf" %>
	                 </c:when>
	                 <c:otherwise>
                        <%@include file="fullPayment.jspf" %>
                     </c:otherwise>
	              </c:choose>
                  <%@include file="termsAndConditions.jspf" %>

                  <div class="btnGroup">
                     <a href="<c:out value='${bookingComponent.prePaymentUrl}'/>" title="Back" class="arrowLinkLeft">Back</a>
                     <c:if test="${bookingInfo.newHoliday}">
                     	<c:choose>
							<c:when test="${requestScope.disablePaymentButton == 'true'}">
		                        <span id="confirmButton" >
		                           <a title="Pay" class="btnContinue"><img src="/cms-cps/thomson/cruise/images/buttons/btn-pay-disabled.gif" width="100" height="25" alt="Pay" class="pay"/></a>
				                </span>
				            </c:when>
							<c:otherwise>
								<span id="confirmButton" >
		                           <a href="javascript:void(0);" onclick="updateEssentialFields();CheckSeniorPassengerAndValidate();" title="Pay" class="btnContinue"><img src="/cms-cps/thomson/cruise/images/buttons/btn-pay.gif" width="100" height="25" alt="Pay" class="pay"/></a>
				                </span>
							</c:otherwise>
 						</c:choose>    
		             </c:if>
                  </div>
               </div><%-- END mainContent --%>
            </div><%-- END #contentContainer --%>
         </form>
         <div id="footer">
            <%@include file="footer.jspf" %>
         </div><%-- END #footer --%>
      </div><%-- END wrapper --%>
<%-- Existing tags has been removed for ensighten --%>
   </body>
</html>