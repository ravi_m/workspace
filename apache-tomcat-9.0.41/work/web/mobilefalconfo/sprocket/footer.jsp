<%@ page import="com.tui.uk.config.ConfReader"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


		<%
				String staySafeAbroad = ConfReader.getConfEntry("staySafeAbroad.mobile.falconfo" , "");
				pageContext.setAttribute("staySafeAbroad", staySafeAbroad, PageContext.REQUEST_SCOPE);

		%>
<div id="footer">
				<div id="call-us" class="hide">
					<div class="content-width">
						<i class="caret call white b"></i><h2 class="d-blue">BOOK NOW! <span>Call us on: <a href="tel:0203 636 1931">0203 636 1931</a></span></h2>
					</div>
				</div>
				<div id="search" class="b">
					<div class="content-width">
						<a href="#page" id="backtotop"><span>To top </span><i class="caret back-to-top white"></i></a>
					</div>
				</div>
				<div id="group" class="b falcon">
					<div class="content-width">
						<div class="copy">
							<!-- <span id="world-of-tui"><img alt="World of TUI" src="/cms-cps/mobilefalconfo/images/logo/wtui.png"></span> -->

                            <p>Just so you know, Falcon Holidays is now called TUI Holidays, and we're part of TUI Group - the world's leading travel company. All of our holidays are designed to help you 'Discover your smile'.</p>

						</div>
					<div class="logos">
				         <!--  <span id="world-of-tui" title="World of TUI"></span>  -->
				          <a href="http://www.aviationreg.ie/" target="_blank" id="c-ar" title="Commission for Aviation Regulation">RIO</a>

			        </div>
						<p class="commission">Licenced by the Commission for Aviation Regulation, TO 021</p>
					</div>
				</div>
				<div id="terms">
					<div class="content-width">
						<p>
			      <a href="https://www.tuiholidays.ie/f/info/about-us">About TUI</a>
				<a href="http://www.falconholidays.ie/our-policies/accessibility/">Accessibility</a>
				<a href="http://www.falconholidays.ie/our-policies/statement-on-cookies/index.html">Statement on cookies</a>
				<a href="http://www.falconholidays.ie/our-policies/terms-of-use/">Terms &amp; conditions</a>
				<a href="http://www.falconholidays.ie/our-policies/privacy-policy/">Privacy Policy</a>
				<c:choose>
					<c:when test="${applyCreditCardSurcharge eq 'true'}">
				<a href="http://www.falconholidays.ie/information/credit-card-payments-and-charges/">Credit card fees</a>
				</c:when>
				<c:otherwise>
				<a href="http://www.falconholidays.ie/information/credit-card-payments-and-charges/">Ways to Pay</a>
				</c:otherwise>
				</c:choose>
				<a class="desktopLinkHide" href="http://www.falconholidays.ie">Desktop site</a>
				<a href="http://tuijobsuk.co.uk/">Travel Jobs</a>
				<a href="https://blog.tuiholidays.ie/">TUI Blog</a>

				<a target="_blank" href="http://www.tuigroup.com/en-en"><jsp:useBean id='CurrentDate12' class='java.util.Date'/>
         	<fmt:formatDate var='currentYear' value='${CurrentDate12}' pattern='yyyy'/>
			&copy; <c:out value="${currentYear}" />  TUI Group</a>
						</p>
					</div>
				</div>
			</div>
			<div id="disclaimer">
			<c:out value="${staySafeAbroad}" escapeXml="false"/>
				<div class="content-width disclaim two-columns">
								<p>TUI is licenced by the Commission for Aviation Regulation under Licence T.O. 021, we have arranged an approved bond, therefore your money is secure with us. TUI is the part of the TUI Group of Companies.</p>


				</div>
			</div>
