<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" id="firstchoice" class="ie6 "> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" id="firstchoice" class="ie7 "> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" id="firstchoice" class="ie8 "> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" id="firstchoice" class="ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="" id="firstchoice"> <!--<![endif]-->
<head>
	<title></title>
	<!--[if IE 7 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /><![endif]-->
	<!--[if IE 8 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /><![endif]-->
	<!--[if IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" /><![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><!--<![endif]-->
	<meta charset="utf-8" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<meta name="viewport" content="width=device-width,  itial-scale=1">


	<link rel="icon" type="image/png" href="/cms-cps/manageth/images/favicon.png" />

		<!--[if lte IE 9]>
			<link rel="shortcut icon" href="/cms-cps/manageth/images/favicon.ico" type="image/x-icon" />
			<link rel="icon" href="/cms-cps/manageth/images/favicon.ico" />
			<![endif]-->

			<!-- HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->

			<!-- javascript -->

			<script>
			var ensLinkTrack = function(){};
			</script>
		</head>
		<body>
			<div id="wrapper">
				<div id="header">
					<!-- <div class="beta">
						<p>Welcome to our brand new website.</p>
						<p class="tell-us">
							<a href="http://www.thomson.co.uk/beta/send-us-an-email/provide-feedback.html" target="_blank" class="ensLinkTrack">Tell us what you think</a> or <a href="http://www.thomson.co.uk/beta/send-us-an-email/provide-feedback.html" target="_blank" class="ensLinkTrack" >Report a problem</a>
						</p>
						<a href="#" class="button">Switch to main site</a>
					</div> -->
					<div class="message-window" style="display:none" id="tellUsWhatYouThink">
						<a href="#" class="close ensLinkTrack"> <span class="text">Close</span></a>
						<div class="message-content small-pop beta-pop">
							<h3>Before you go...</h3>
							<p>We'd love to know what you made of the new site:</p>
							<ul>
								<li><a target="_blank" href="http://www.thomson.co.uk/" class="ensLinkTrack">Tell us what you think</a></li>
								<li><a target="_blank" href="http://www.thomson.co.uk/?customer=switch" class="ensLinkTrack">Switch to our main site</a></li>
							</ul>
						</div>
					</div>
					<div id="utils">
						<ul>
						 <li><a target="_blank"  title="Ask a Question" href="http://www.thomson.co.uk/destinations/faqCategories">Ask a Question</a></li>
							<li><a target="_blank" class="ensLinkTrack" title="Statement on Cookies" href="http://www.thomson.co.uk/editorial/legal/statement-on-cookies.html">Statement on Cookies</a></li>
							<li><a target="_blank" class="ensLinkTrack" title="Travel alerts" href="http://www.thomson.co.uk/editorial/alerts/thomsonfly-travel-alert.html">Travel alerts</a></li>
							<li><a target="_blank" class="ensLinkTrack" title="API login" href="http://eapi.thomson.co.uk/">API login</a></li>
							<li><a class="ensLinkTrack" title="Manage my booking" href="${bookingComponent.breadCrumbTrail['HOME']}">Manage my booking</a></li>
							<li><img width="50" height="70" src="/cms-cps/manageth/images/WOT_Endorsement_V2_3C.png" alt="Welcome to the world of TUI"></li>
						</ul>
					</div>

					<div id="logo">
						<a target="_blank" class="ensLinkTrack" href="http://www.thomson.co.uk">Thomson</a>

					</div>
					<div id="nav">
						<ul>
							<li class="homepage-main-nav"><a target="_blank" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/holidays.html">Holidays</a></li>
							<li class="cruise-main-nav"><a target="_blank" class="ensLinkTrack" href="http://www.thomson.co.uk/cruise.html">Cruises</a></li>
							<li class="flights-main-nav"><a target="_blank" class="ensLinkTrack" href="http://flights.thomson.co.uk/en/index.html">Flights</a></li>
							<li class="hotels-main-nav"><a target="_blank" class="ensLinkTrack" href="http://www.thomson.co.uk/deals.html">Deals</a></li>
							<li class="dest-main-nav"><a target="_blank" class="ensLinkTrack" href="http://www.thomson.co.uk/destinations/holiday-destinations.html">Destinations</a></li>
							<li class="extra-main-nav"><a target="_blank" class="ensLinkTrack" href="http://www.thomson.co.uk/holiday-extras.html">Extras</a></li>
						</ul>
					</div>
				</div>
			</div>
		</body>
	</html>