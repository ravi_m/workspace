(function($){

	tui.utils.WebForm = {

		validationEventType: null,

		messageMap: null,

		/**
		 * Object: validatorTypes
		 * Defines the rules for all validator input types.
		 */
        validatorTypes: {

			alpha: {
                test: function(inputElement){
                    var filter = /^[a-zA-Z -]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            numeric: {
                test: function(inputElement){
                    var filter = /^[0-9]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            alphanumeric: {
                test: function(inputElement){
                    var filter = /^[-a-zA-Z0-9 \\\/.,]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            address: {
                test: function(inputElement){
                    var filter = /^[-a-zA-Z0-9 ,\.\x27]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            phonenumber: {
                test: function(inputElement){
                    var filter = /(^-[0-9]+)|(^[0-9]*-$)|(^-[0-9]*-$)/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            postcode: {
                test: function(inputElement){
                    var filter = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val().toUpperCase())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            cardnumber: {
                test: function(inputElement){
            		if(inputElement.val().length >= 12 && inputElement.val().length <= 19){
            			return false;
            		}
                    var filter = /^[0-9]*$/;
                    if (inputElement.val() != '' && !filter.test(StringUtils.removeAllSpaces(inputElement.val()))) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            expirydate: {
                test: function(inputElement){
                    var filter = /^[0-9]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            cardtype: {
                test: function(inputElement){
                    var filter = /^[0-9]*$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },

            email: {
                test: function(inputElement){
                    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-]{2,})+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if (inputElement.val() != '' && !filter.test(inputElement.val())) {
                        return false;
                    }
                    return true;
                },
                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            }
        },

		/**
		 * Object: validators
		 * Defines the rules for all validator.
		 */
        validators: {
            wf_required : {
                test: function(inputElement){
                    if (this._isEmpty(inputElement)) {
                        return false;
                    }
                    return true;

                },

                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },
            wf_lengthmax : {
                test: function(inputElement){
                   var l = $(inputElement).attr("wf_lengthmax")

                   if (inputElement.val().length > l) {
                	   return false;
                   	}
                   return true;
                },

                errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },
            wf_luhncheck : {
                test: function(inputElement){
        	    var cardnumber = $(inputElement).val();
                var oddoreven = cardnumber.length & 1;
                var sum = 0;
                var addition = "";

                for (var count = 0; count < cardnumber.length; count++)
                {

                   var digit = parseInt(cardnumber.substr(count,1));
                   if (!((count & 1) ^ oddoreven))
                   {
                      digit *= 2;
                      if (digit > 9)
                      {
                         digit -= 9;
                         addition = addition + ' ' + digit;
                      }
                      else
                      {
                         addition = addition + ' ' + digit;
                      }
                      sum += digit;
                   }
                   else
                   {
                      sum += digit;
                      addition = addition + ' ' + digit;
                   }
                }
                if (sum % 10 != 0) {return false}
                   return true

            	},
            	errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },
            wf_securitycode : {
            	test: function(inputElement){
            		var filter = /^[0-9]{3}$/;

	                if (inputElement.val() != '' && !filter.test(inputElement.val())) {
	                    return false;
	                }
	                return true;
            	},
            	errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }
            },
            wf_dateafter : {
            	test: function(inputElement, type){

            		if (inputElement.attr("id")){
            			var identifier = inputElement.attr("id").split("-")[0];
            			var selectedDay = ($("#" + identifier + "-DD").length > 0) ? parseInt($("#" + identifier + "-DD").val(),10) : null;
            			var selectedMonth = ($("#" + identifier + "-MM").length > 0) ? parseInt($("#" + identifier + "-MM").val(),10) : null;
            			var selectedYear = ($("#" + identifier + "-YY").length > 0) ? parseInt($("#" + identifier + "-YY").val(),10) : null;

            			if (String(selectedYear).length < 4){
            				selectedYear = ($("#" + identifier + "-YY option:selected").attr("wf_year"))
            			}

            			var selectedDate = new Date(selectedYear,selectedMonth,selectedDay);
            			if (selectedDate == "Invalid Date") return false;
            			var currIdentifier = inputElement.attr("wf_dateafter").split("/");
            			var currDay = (selectedDay) ? parseInt(currIdentifier[0],10) : null;
            			var currMonth = (selectedMonth) ? parseInt(currIdentifier[1],10) : null;
            			var currYear = (selectedYear) ? parseInt(currIdentifier[2],10) : null;
            			var currDate = new Date(currYear,currMonth,currDay);
            			if (currDate == "Invalid Date") return false;

            			return (selectedDate > currDate);
            		}
            		return false;
            	},
            	errorDisplay: function(inputElement, type){
                    this._displayError(inputElement, type);
                }

            }


        },

        options: {
        	evaluateOnSubmit: true,
			evaluateFieldsOnBlur: true,
            showDefaultError: true,
            validateHidden: false
        },

        _displayError: function(inputElement, type, errorMessage){
			var webForm = this;
			var errorType = inputElement.attr("id") + webForm.validationEventType + "Error"+ tui.utils.String.capitalise(type);
			var errorMessage = (webForm.messageMap == null) ? null : webForm.messageMap[errorType];
            this._trigger("on" + webForm.validationEventType +"ErrorDisplay", null, [inputElement, type+"ErrorMsg", errorMessage]);
        },

        _init: function(){

			var webForm = this;
			if (webForm.options.evaluateOnSubmit){
            	$(this.element).bind('submit', function(event){
					event.preventDefault();
					webForm.validationEventType = "Submit";
					webForm._trigger("onBeforeSubmit", null, [$(this)]);
					var answer = webForm._onValidation(this);
					webForm._trigger("onSubmit", null, [answer, $(this)]);
					return false;
				})
			}

            if (webForm.options.evaluateFieldsOnBlur) {
                var inputElements = webForm._getInputFields();
                webForm.addEventHandlerOnBlur(inputElements)
            }

        },

        addEventHandlerOnBlur : function(inputElements) {
        	var webForm = this;
			$.each(inputElements, function(index, inputElement) {
				var type = (inputElement.tagName === "SELECT") ? "change" : "blur";
				$(inputElement)[type](function(event) {
					event.preventDefault();
					webForm.validationEventType = "Blur";
					webForm._trigger("beforeBlur", null, [$(this)]);
					webForm._validateInputFieldsType($(this), this.getAttribute('wf_type'))
					webForm._validateInputFields($(this));
				})
			});
		},

		/**
		 * Function: _getInputFields
		 * Get all input fields in a given form.
		 */
		_getInputFields: function(){
			var selector = (!this.options.validateHidden) ? 'input[type!=submit]:visible, textarea:visible, select:visible' : ':input[type!=submit], textarea, select';
            return $(this.element).find(selector);
		},

		/**
		 * Function: _onValidation
		 * Validation form onSubmit using rules specified.
		 *
		 * @param {DOM object} formElement: Given form element in which we want to validate.
		 */
        _onValidation: function(formElement){

  			var webForm = this;
            var passed = new Array();

            var inputElements = webForm._getInputFields();
            inputElements.each(function(i, inputElement){
                passed.push(webForm._validateInputFields($(inputElement)));
                passed.push(webForm._validateInputFieldsType($(inputElement), inputElement.getAttribute('wf_type')));
				webForm._trigger("beforeSubmit", null, [this.getAttribute('id')]);
            })

            if ($.inArray(false, passed) != -1) {
                return false;
            }

			return true;
        },

		/**
		 * Function: _validateInputFields
		 * Validation form input fields using rules specified.
		 *
		 * @param {DOM object} inputElement: Given input element, for validating
		 */
        _validateInputFields: function(inputElement){

			var webForm = this;
            var passed = new Array();

            for (property in webForm.validators) {

            	var value = inputElement.attr(property);

                if (value) {
                    if (!webForm.validators[property].test.apply(this, [inputElement, property.replace("wf_","")])) {
                        webForm.validators[property].errorDisplay.apply(this, [inputElement, property.replace("wf_","")]);
                        passed.push(false);
                    }
                }
            }

            if ($.inArray(false, passed) != -1) {
                return false;
            }

			return true;
        },

        /**
		 * Function: _validateInputFieldsType
		 * Validation form input fields using type specified.
		 *
		 * @param {DOM object} inputElement: Given input element, for validating
		 */
        _validateInputFieldsType: function(inputElement, type){
			var webForm = this
            for (property in webForm.validatorTypes) {
                if (property === type) {
               	 	if (!this.validatorTypes[type].test.apply(this, [inputElement, property.replace("wf_","")])) {
           				this.validatorTypes[type].errorDisplay.apply(this, [inputElement, type.replace("wf_","")]);
               			return false;
            		}
					return true;
                }
            }
		},

        /**
         * Method: _isEmpty
         * check if a input field is empty.
         */
        _isEmpty: function(inputElement){
            var type = inputElement.attr('type');
            var answer  = (type == 'checkbox') ? (inputElement[0].checked == false) : ($.trim(inputElement.val()) == '');
            return answer;
        },

        /**
         * Method: addValidatorType
         * adds rules to validatortype.
         */
        addValidatorType: function(type, rules, validatorRulesType){
        	type = (validatorRulesType ==  tui.utils.WebForm.TYPE_VALIDATOR) ? type : "wf_" + type
            var validatorTypes = (validatorRulesType === tui.utils.WebForm.TYPE_VALIDATOR) ? this.validatorTypes : this.validators
            validatorType = validatorTypes[type];
            validatorType = (validatorType) ? $.extend(true, validatorType, rules) : rules;
            this.validatorTypes[type] = validatorType;
        },

        /**
         * Method: addValidator
         * adds rules to validator.
         */
        addValidator: function(type, rules){
            var validator = this.validators[type];
            validator = (validator) ? $.extend(true, validator, rules) : rules;
            this.validators[type] = validator;
        },

		addErrorMessage: function(messageMap){
			var webForm = this;
			webForm.messageMap = messageMap;
		}
    }

	tui.utils.WebForm.TYPE_VALIDATOR = "TYPE_VALIDATOR";
	tui.utils.WebForm.VALIDATOR = "VALIDATOR";

    $.widget("ui.webForm", tui.utils.WebForm);

})(jQuery);