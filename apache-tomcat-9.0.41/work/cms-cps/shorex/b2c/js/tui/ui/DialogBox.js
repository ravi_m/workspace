( function($) {

	tui.ui.DialogBox = {

		events: [ "click", "hover" ],

		targetElements: null,

		targetElement: null,

		windowDimensions: null,

		currentWindowHeight: null,

		modalElement: null,

		modalObject: null,

		autoPositionObject: null,

		contentLoaderObject: null,

		options: {
			closeElement: "close",
			removeAltSelector: null,
			targetClassname : null,
			targetPrefix : "-link",
			event : "hover",
			autoPosition : null,
			modal : null,
			contentLoader : null,
			isModal : false,
			documentRelative : false,
			adjustWithWindowResize : false,
			closeClassSelector : "",
			stopPropagation : false
		},

		/**
		 * Method: _init
		 * Constructor which will initialise object variables on object creation.
		 */
		_init: function() {
			var dialogBox = this;
			dialogBox._trigger("beforeInit", null, [ dialogBox ]);
			var targetElements = (dialogBox.options.targetClassname || "#"
					+ dialogBox.element.attr("id")
					+ dialogBox.options.targetPrefix);
			dialogBox.targetElements = $(targetElements);
			if (dialogBox.options.removeAltSelector){
				dialogBox.targetElements.find(dialogBox.options.removeAltSelector).removeAttr("alt");
			}
			dialogBox._addEventHandler();
			if (dialogBox.options.adjustWithWindowResize){
				dialogBox._addWindowEventHandler();
			}
			dialogBox._trigger('afterInit', null,  [ dialogBox, dialogBox.targetElements ]);
		},

		/**
		 * Method: _addEventHandler
		 * Add an event listener to target link. Either an "onclick" or "hover" event depending on configured option.
		 */
		_addEventHandler: function(){
			var dialogBox = this;
			if (!dialogBox.targetElements.length > 0 && $.inArray(dialogBox.options.event, dialogBox.events) > -1) return;
			$(dialogBox.targetElements)[dialogBox.options.event](
				function(event) {
					event.preventDefault();
					if(dialogBox.options.stopPropagation){
						event.stopPropagation()
					}
					dialogBox.targetElement = $(this);
					if (dialogBox.targetElement.attr("id") === ""){
						DialogBox.Guid++
						dialogBox.targetElement.attr("id", "dialogBox" + DialogBox.Guid)
					}
					dialogBox._trigger('on' + dialogBox.options.event, null, [dialogBox, dialogBox.targetElements]);
					dialogBox._showBox();
				},
				function(event){
					event.preventDefault();
					if(dialogBox.options.stopPropagation){
						event.stopPropagation()
					}
					dialogBox._trigger('on' + dialogBox.options.event + 'Out');
					dialogBox._hideBox();
				}
			)

			dialogBox.element.find("." + this.options.closeElement).click(function(event) {
				event.preventDefault();
				if(dialogBox.options.stopPropagation){
					event.stopPropagation()
				}
				dialogBox._hideBox();
			})

			dialogBox._trigger('afterAddEventHandler', null,  [ dialogBox, dialogBox.targetElements ]);
		},

		/**
		 * Method: _addWindowEventHandler
		 * Adds eventHandler to the window.
		 */
		_addWindowEventHandler: function(){
			var dialogBox = this;
			dialogBox._setWindowDimensions();
			dialogBox.cacheResize = function(){
				dialogBox._resize();
			}
			dialogBox.cacheScroll = function(){
				dialogBox._resize();
			}
			$(window).resize(dialogBox.cacheResize);
			$(window).scroll(dialogBox.cacheScroll);
		},

		/**
		 * Method: resize
		 * @param {Boolean} forceResize
		 */
		_resize: function(forceResize) {
			var dialogBox = this;
			if (dialogBox.windowDimensions.w === $(window).height() &&
				dialogBox.windowDimensions.h === $(window).width() &&
				(!forceResize)) {
				return;
			}
			dialogBox._autoPostionBox();
			dialogBox._setWindowDimensions();
		},

		/**
		 * Method: setWindowDimensions
		 */
		_setWindowDimensions: function(){
			var dialogBox = this;
			dialogBox.windowDimensions = {w: $(window).width(), h: $(window).height()};
		},

		/**
		 * Method: showBox
		 */
		_showBox: function() {
			var dialogBox = this;

			dialogBox._trigger('beforeShow', null,  [ dialogBox ]);

			if (dialogBox.options.isModal) {
				dialogBox._showModal();
			}

			if (dialogBox.options.autoPosition) {
				dialogBox._autoPostionBox();
			}

			if (dialogBox.options.contentLoader) {
				dialogBox._contentLoaderBox();
			}

			if (dialogBox.element.bgiframe) {
				dialogBox.element.bgiframe();
			}

			dialogBox.element.show();

			dialogBox._trigger('afterShow', null,  [ dialogBox ]);
		},


		/**
		 * Method: showModal
		 */
		_showModal: function(){
			var dialogBox = this;
			dialogBox._createModalContainer();
			if (!dialogBox.modalObject) {
				dialogBox.options.modal = (dialogBox.options.modal) ? dialogBox.options.modal : {}
				dialogBox.options.modal["onModalHide"] = function (){
					dialogBox._hideBox(false);
				}
				dialogBox.modalObject = dialogBox.modalElement.modal(dialogBox.options.modal);
			}
			this.modalObject.modal("showBox");
		},

		/**
		 * Method: autoPostionBox
		 */
		_autoPostionBox: function(){
			var dialogBox = this;
			if (!dialogBox.autoPositionObject) {
				if (!dialogBox.options.documentRelative) {
					dialogBox.options.autoPosition.relative = (dialogBox.options.autoPosition.relative || "#" + dialogBox.targetElement.attr("id"));
				}
				dialogBox.autoPositionObject = dialogBox.element.autoPosition(dialogBox.options.autoPosition);
				return;
			}
			var options = dialogBox.options.autoPosition
			if (!dialogBox.options.documentRelative) {
				options.relative = "#" + dialogBox.targetElement.attr("id");
			}
			dialogBox.autoPositionObject.autoPosition("position", options);
		},

		/**
		 * Method: contentLoaderBox
		 */
		contentLoaderBox: function(){
			var dialogBox = this;
			if (!dialogBox.contentLoaderObject) {
				dialogBox.contentLoaderObject = dialogBox.element.contentLoader(dialogBox.options.contentLoader);
				return;
			}
			dialogBox.contentLoaderObject.contentLoader("loadContent");
		},

		/**
		 * Method: createModalContainer
		 */
		_createModalContainer: function(){
			var dialogBox = this;
			if (!dialogBox.modalElement) {
				$($(document)[0].body).append('<div id="dialogBoxModal' + dialogBox.element.attr("id")+ "_" + DialogBox.ModalCount +'"></div>');
				dialogBox.modalElement = $("#dialogBoxModal" + dialogBox.element.attr("id") + "_" + DialogBox.ModalCount);
				DialogBox.ModalCount++;
			}
	    },

		_hideBox: function(ignore){
	    	ignore = (ignore == undefined) ? true : ignore;
	    	var dialogBox = this;
	    	dialogBox._trigger('beforeHide', null,  [ dialogBox ]);
			if (dialogBox.options.isModal && ignore){
				dialogBox.modalObject.modal("hideBox");
			}
			dialogBox.element.hide();

			dialogBox._trigger('afterHide', null,  [ dialogBox ]);
		},

		hideBox: function(ignore){
			var dialogBox = this;
			dialogBox._hideBox(ignore);
		},

		destroy: function(){
			var dialogBox = this;
			dialogBox.modalObject.modal("destroy");
			dialogBox.modalObject = null;
			dialogBox.autoPositionObject = null;
			dialogBox.contentLoaderObject = null;
			$(window).unbind("scroll", dialogBox.cacheScroll);
			$(window).unbind("resize", dialogBox.cacheResize);
			dialogBox.element.remove();
		}
	}

	var DialogBox = {}
	DialogBox.ModalCount = 0
	DialogBox.Guid = 0
	DialogBox.Event = {}
	DialogBox.Event.CLICK = "click";
	DialogBox.Event.HOVER = "hover";

	$.widget("tui.dialogBox", tui.ui.DialogBox)

})(jQuery);