var PaymentView =
{
   /**
    * Show alert and focus the field.
    */
   showAlert:function(Msg, fieldObj)
   {
      alert(Msg);
	  if (fieldObj != null)
	  {
         fieldObj.focus();
	  }
      return false;
   },

   /**
    * refresh the field
    */
   refreshTheField:function(fieldObj)
   {
      if (fieldObj.type == 'text')
      {
	     fieldObj.value = "";
	  }
	  else if (fieldObj.type == 'checkbox')
	  {
	     fieldObj.checked = false;
	  }
   },

   /**
    * clear the text in a input field.
    *
    * @param fieldObj the field read as DOM
    */
   emptyTheField : function(fieldObj)
   {
      fieldObj.val("");
   },

   /**
    * sets dropDown to default selection(i.e. selected index will be 0)
    *
    * @param dropDownObj the drop down read as DOM
    */
   setDropDownsToDefault : function(dropDownObj)
   {
      dropDownObj.selectedIndex = 0;
   },

   /**
    ** Highlights the container whose id is provided
    ** passed in as a parameter.
    ** @param containerID the id of the container to be highlighted
    ** @param highlightClass the css class which highlights the section
    ** @param isToBeHighlighted switch to highlight/unhighlight a section
   **/
   highlightContainer:function (containerID, highlightClass,isToBeHighlighted)
   {
      if(isToBeHighlighted)
      {
         $("#"+containerID).addClass(highlightClass);
      }
      else
      {
         $("#"+containerID).removeClass(highlightClass);
      }
   },

	/**This function shall be responsible for updating the button caption of the Submit button*/
	changeCaption: function(caption)
	{
	   $("#bookingcontinue").val(caption);
		$("#bookingcontinue").attr("title",caption);
	},

	/**This function hides/unhides a field based on the flag sent*/
	displayContainer:function(containerID,isToBeHidden)
	{
		if(isToBeHidden)
		{
		   $("#"+containerID).addClass("hide");
		}
		else
		{
		   $("#"+containerID).removeClass("hide");
		}
	}
};


var DepositTypeChangeHandler =
{
   /**
   * Function for setting the selected deposit value
   * into PaymentInfo object and update the values
   * in UI according to selection made.
   * @param selectedDepositType Selected deposit value, ex- LowDeposit, Deposit
   */
   handle:function(selectedDepositType)
   {
	 this.removeFocusOfPreviouslySelectedDepositType();
	 PaymentInfo.depositType = selectedDepositType;
	 this.focusSelectedDepositType();
	 this.updatePaymentSection();
   },

   /** This function updates the amount in the payment section to the selected deposit amount.**/
   updatePaymentSection: function()
	{
	   //alert('updatepaymentsection')
	 	if(PaymentInfo.partialPaymentAmount == null || PaymentInfo.partialPaymentAmount == undefined)
	 	   {
	 	      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.totalAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
	 	   }
	 	   else
	 	   {
	 	      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
	 	   }
	   var calCardCharge = cardChargeHandler.getApplicableCardCharge(PaymentInfo.selectedDepositAmount,"Credit");
	   PaymentInfo.totalCardCharge = calCardCharge;
	   var amtWithCardCharge = MathUtils.roundOff(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge , 2);
	   $("#spanAmountWithCardCharge").html(PaymentInfo.currencySymbol + amtWithCardCharge);
		//   document.getElementById('spanAmountWithCardCharge').innerHTML	= PaymentInfo.currencySymbol + PaymentInfo.calculatedTotalAmount;

		   var debitCardCharge = cardChargeHandler.getApplicableCardCharge(PaymentInfo.selectedDepositAmount,"Debit");
		   $("#spanAmountWithoutCardCharge").html(PaymentInfo.currencySymbol + MathUtils.roundOff(1*PaymentInfo.selectedDepositAmount + 1*debitCardCharge , 2));
		  // alert(' test');

		  // $('totalAmountDue').innerHTML="<b>"+PaymentInfo.calculatedTotalAmount+"</b>";
			//  $('calculatedTotalAmount').innerHTML="<b>"+PaymentInfo.calculatedTotalAmount+"</b>";
	   if(PaymentInfo.selectedCardType)
	   {
		 var cardCharge = cardChargeHandler.calculateCardCharge(PaymentInfo.totalAmount);
		 PaymentInfo.calculatedPayableAmount = MathUtils.roundOff(1*PaymentInfo.totalAmount + 1*cardCharge , 2);

	   }

	},

   /**
    * This function removes the focus of previously selected deposit type.
    */
   removeFocusOfPreviouslySelectedDepositType:function()
   {
	 $("#"+PaymentInfo.depositType).removeClass("selected");
   },

   /**
    * This function adds focus to the selected deposit type.
    */
   focusSelectedDepositType:function()
   {
      $("#"+PaymentInfo.depositType).addClass("selected");
   },

   /**
    * Refreshes the PaymentInfo. A central function responsible for
    * keeping data integrity of different amounts in
    * in this payment for client side validation.
    */
    updatePaymentInfo:function()
    {
             //alert('updatepaymentinfo')

 	if(PaymentInfo.partialPaymentAmount == null || PaymentInfo.partialPaymentAmount == undefined)
 	   {
 	      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.totalAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
 	   }
 	   else
 	   {
 	      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
 	   }
//alert('card chrg'+PaymentInfo.totalAmount);
 	   calCardCharge = cardChargeHandler.calculateCardCharge(PaymentInfo.selectedDepositAmount);
 	   PaymentInfo.totalCardCharge = calCardCharge;
 	   PaymentInfo.calculatedPayableAmount = MathUtils.roundOff(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge , 2);
 	   PaymentInfo.calculatedTotalAmount = MathUtils.roundOff((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge , 2);
//alert('payable'+PaymentInfo.calculatedTotalAmount);
    }

};

/*updating total cost */
function displayFieldsWithValuesForCard()
{
//alert("test");
		   if(PaymentInfo.selectedCardType!="" && PaymentInfo.selectedCardType!=null  && calCardCharge>0)
		   {

			//  document.getElementById("creditcardcontent").style.display="block" ;
			  if(PaymentInfo.chargeamt == 0 || calCardCharge != 0)
			  {
				   $('creditCardSurcharge').innerHTML=cardChargeMap.get(PaymentInfo.selectedCardType).split(",")[4]+" card surcharge";
				 //  document.getElementById("surcharge").innerHTML= PaymentInfo.currency;

				   $('surcharge').innerHTML+=calCardCharge;
			   }
			   else
			   {

			  	   $('surcharge').innerHTML=PaymentInfo.currency;
			  	   $('surcharge').innerHTML+=PaymentInfo.chargeamt.toFixed(2);
			   }

		   }

		   else
			{
			   	 // document.getElementById("creditcardcontent").style.display="none" ;
			    $('creditCardSurcharge').innerHTML="";
				$('surcharge').innerHTML="";
			}

		   document.getElementById('outstandingamt').value=PaymentInfo.calculatedPayableAmount;
                  // alert('is nan'+PaymentInfo.calculatedPayableAmount);
                   document.getElementById('totamount').innerHTML=PaymentInfo.calculatedPayableAmount;
		  $('totamount').innerHTML="<b>"+PaymentInfo.calculatedPayableAmount+"</b>";
		  //document.getElementById('calculatedTotalAmount').innerHTML="<b>"+PaymentInfo.calculatedTotalAmount+"</b>";
		  //document.getElementById('totalExcursionAmount').innerHTML="<b>"+PaymentInfo.calculatedTotalAmount+"</b>";

		 // document.getElementById("payment_0_paymenttypecode").value= PaymentInfo.selectedCardType;
		  document.getElementById("total_transamt").value=PaymentInfo.calculatedPayableAmount;


}

var cardChargeHandler=
{
   /**
	 ** Calculates the card charges for the amount
	 ** passed in as a parameter.
	 ** @param amount Amount for which card charges to be calculated.
	 ** @return cardCharge Card charge for the amount sent in.
	 **/
   calculateCardCharge:function(amount)
	{
	   if (cardChargeMap.get(PaymentInfo.selectedCardType) == 0)
      {
         return 0.0;
      }
		var cardChargeData = cardChargeMap.get(PaymentInfo.selectedCardType).split(",");
		PaymentInfo.chargePercent = cardChargeData[0];
		PaymentInfo.minCardChargeAmount = cardChargeData[1];
		PaymentInfo.maxCardChargeAmount = cardChargeData[2];
		var cardCharge = MathUtils.roundOff(((amount * PaymentInfo.chargePercent)/100),2);
      if (PaymentInfo.minCardChargeAmount != null)
      {
         cardCharge = (cardCharge < 1*PaymentInfo.minCardChargeAmount) ? 1*PaymentInfo.minCardChargeAmount : cardCharge;
	   }
      if (PaymentInfo.maxCardChargeAmount != null && PaymentInfo.maxCardChargeAmount != "" && PaymentInfo.maxCardChargeAmount > 0)
      {
         cardCharge = (cardCharge > 1*PaymentInfo.maxCardChargeAmount) ? 1*PaymentInfo.maxCardChargeAmount : cardCharge;
	   }
	   return cardCharge;
	},

   getApplicableCardCharge:function(amount,chargeType)
   {
	   var cardChargeDetails = "";
		if (chargeType == "Credit")
		{
		   cardChargeDetails = creditCardChargeDetails;
		}
		else
		{
		   cardChargeDetails = debitCardChargeDetails;
		}
		var cardChargeArr = cardChargeDetails.split(",");
      var cardCharge = MathUtils.roundOff(((amount * cardChargeArr[0])/100),2);
      if (cardChargeArr[1] != null)
      {
         cardCharge = (cardCharge < 1*cardChargeArr[1]) ? 1*cardChargeArr[1] : cardCharge;
      }
      if (cardChargeArr[2] != null && cardChargeArr[2] != "")
      {
    	  if(cardChargeArr[2] > 0){
    		  cardCharge = (cardCharge > 1*cardChargeArr[2]) ? 1*cardChargeArr[2] : cardCharge;
      }
      }
      return cardCharge;
   }
};

var CardTypeChangeHandler =
{
   /**
    ** Highlights the appropriate amount section and updates essential fields
    **/
   handleCardSelection:function ()
   {
   var cardTypeSelected=document.getElementById("cardType").value
	
	if (cardTypeSelected=="MASTERCARD|Dcard"||cardTypeSelected=="VISA|Dcard"||cardTypeSelected=="AMERICAN_EXPRESS|Dcard")
	  {
		if(applyCreditCardSurcharge=='true'){
	  document.getElementById("notify").innerHTML="(includes credit card charge)";
		}
	  }
	  else{
	  document.getElementById("notify").innerHTML="";
	  }
      if(newHoliday == 'true')
      {
    	  //alert('handlecardselection')
         //var selectedCardValue = $("#cardType").val();
		   var selectedCardValue = document.getElementById("cardType").value;
    	   var selectedCardArray = selectedCardValue.split("|");
    	   PaymentInfo.selectedCardType = (selectedCardArray!=""&&selectedCardArray) ? selectedCardArray[0] : null;
		   CardTypeChangeHandler.handleIssueNumberSection(PaymentInfo.selectedCardType);
		   document.getElementById("payment_0_paymenttypecode").value= PaymentInfo.selectedCardType;
         CardTypeChangeHandler.updateSectionToBeHighlighted();
         CardTypeChangeHandler.updateButtonCaption();
         DepositTypeChangeHandler.updatePaymentInfo();
         DepositTypeChangeHandler.updatePaymentSection();
         displayFieldsWithValuesForCard();
      }
   },

   /**hides/unhides the issue number section.*/
   handleIssueNumberSection:function(cardType)
   {
	   if (cardType=="SWITCH" || cardType=="SOLO")
	   {
         PaymentView.displayContainer("issueNumber",false)
	   }
	   else
	   {
         PaymentView.displayContainer("issueNumber",true);
	   }
   },

   /**
    ** Highlights the appropriate amount section based on the card charge applicability
    **/
   updateSectionToBeHighlighted:function()
   {
      if (PaymentInfo.selectedCardType)
	   {
	      if(cardChargeMap.get(PaymentInfo.selectedCardType).split(",")[4] == "Credit")
         {
            PaymentView.highlightContainer("amountWithCardCharge","selected",true)
      	   PaymentView.highlightContainer("amountWithoutCardCharge","selected",false);
         }
         else
         {
      	   PaymentView.highlightContainer("amountWithCardCharge","selected",false);
      	   PaymentView.highlightContainer("amountWithoutCardCharge","selected",true);
         }
      }
	   else
	   {
	      PaymentView.highlightContainer("amountWithCardCharge","selected",false);
         PaymentView.highlightContainer("amountWithoutCardCharge","selected",false);
	   }
   },

	/**Updates the caption of the submit button based on 3DS*/
   updateButtonCaption:function()
	{
      var payButtonDescription = threeDCards.get(PaymentInfo.selectedCardType);
      if(payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup")
      {
	      PaymentView.changeCaption("Proceed to payment");
      }
      else
      {
         PaymentView.changeCaption("Pay now");
	   }
   }
};

/**
 ** Responsible for showing/hiding the overlays
 **/
var stickyOverlay =
{
   /**
    ** Responsible for showing/hiding the 3D overlays
    **/
   threeDOverlay: function()
   {
      var overlayZIndex = 99;
      var zIndex = 100;
      var prevOverlay;
      var stickyOpened = false;
      jQuery("a.threeDSstickyOwner").click(function(e)
      {
         var overlay = "#" + this.id + "Overlay";
    	   if (!stickyOpened)
    	   {
    	      prevOverlay = overlay;
    	   }
    	   if (prevOverlay != overlay)
    	   {
    	      jQuery(prevOverlay).hide();
    	      stickyOpened = false;
    	   }
    	   if(!StringUtils.isBlank(this.id)){
    		   var pos = jQuery("#"+this.id).offset();
    		   jQuery(overlay).show();
    	   }
    	   prevOverlay = overlay;
    	   stickyOpened = true;
    	   zIndex++;
    	   if (jQuery(overlay).parent(".overlay") != null){
    	      jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
    	      overlayZIndex++;
         }
         return false;
      });
      jQuery("a.close").click(function(){
         var overlay = this.id.replace("Close","Overlay");
         jQuery("#" + overlay).hide();
         return false;
      });
   },

   /**
    ** Responsible for showing/hiding the common overlays
    **/
   commonOverlay: function()
   {
      var overlayZIndex = 99;
      var zIndex = 100;
      var prevOverlay;
      var stickyOpened = false;
      jQuery("a.stickyOwner").click(function(e){
         var overlay = "#" + this.id + "Overlay";
    	   if (!stickyOpened)
    	   {
    	      prevOverlay = overlay;
    	   }
    	   if(!StringUtils.isBlank(this.id)){
    		   var pos = jQuery("#"+this.id).offset();
    		   jQuery(overlay).show();
    	   }
    	   prevOverlay = overlay;
    	   stickyOpened = true;
    	   jQuery(overlay + ".genericOverlay").css("z-index",zIndex);
    	   zIndex++;
    	   if (jQuery(overlay).parent(".overlay") != null){
    	      jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
    	      overlayZIndex++;
         }
         return false;
      });
      jQuery("a.close").click(function(){
         var overlay = this.id.replace("Close","Overlay");
         jQuery("#" + overlay).hide();
         return false;
      });
   },


   /**
    ** Responsible for showing/hiding the summary panel overlays
    **/
   summaryPanelOverlay :function()
   {
      var overlayZIndex = 99;
      var zIndex = 100;
      var prevOverlay;
      var stickyOpened = false;
	  // Don't remove this. Required for benefits overlay content.
	  jQuery('.summaryPanelMid-s li:last-child').css("padding", "8px 0");
	  jQuery('.summaryPanelMid-s li:last-child').css("background-image", "none");
      jQuery("a.stickyOwner").mouseover(function(e){
         var overlay = "#" + this.id + "Overlay";
    	 if (!stickyOpened)
    	 {
    	    prevOverlay = overlay;
    	 }
    	 if (prevOverlay != overlay)
    	 {
    	    jQuery(prevOverlay).hide();
    		stickyOpened = false;
    	 }
    	 if(!StringUtils.isBlank(this.id)){
    		 var pos = jQuery("#"+this.id).offset();
    		 var top = parseInt((1*pos.top-167),10);
    	 }
		 var left = 20;
         var width = 240;
		 if(StringUtils.equalsIgnoresCase(this.id, 'benefit'))
		 {
    	    width = 255;
		 }
		 if(StringUtils.equalsIgnoresCase(this.id, 'subPrice'))
		 {
    	    width = 190;
		 }
    	 jQuery(overlay).show();
		 jQuery(overlay).css("top",top);
    	 jQuery(overlay).css("width",width);
		 jQuery(overlay).css("left",left);
    	 prevOverlay = overlay;
    	 stickyOpened = true;
    	 jQuery(overlay + ".genericOverlay").css("z-index",zIndex);
    	 zIndex++;
    	 if (jQuery(overlay).parent(".overlay") != null){
    	    jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
    	    overlayZIndex++;
    	 }
         return false;
      });
      jQuery("a.stickyOwner").mouseout(function(e){
         var overlay = "#" + this.id + "Overlay";
    	 jQuery(overlay).hide();
         return false;
      });
   }


};
var Personaldetails =
{
	noOfPassengers : "",
	setNoOfPassengers : function(noOfPassengers)
	{
	this.noOfPassengers = noOfPassengers;
	},

	getNoOfPassengers : function()
	{
      return this.noOfPassengers;
	},

	/**
	 * This function copies the surname of the lead passenger to the
	 * rest of the passengers when the user checks 'same surname as lead passenger' check box.
	 */
	autoCompleteSurname : function(passengerCount)
	{
		if($("#sameSurname").attr('checked'))
		{
		  for(index=1; index < passengerCount ; index++)
		   {
			 $('#surName_'+index).val($('#surName_0').val());
			 if(!StringUtils.isBlank($('#surName_0').val()))
			 {
			    $('#surName_'+index).blur();
			 }
		   }
		}
	},

	/**
	 * This function unchecks the 'same surname as lead passenger' check box when the user
	 * changes the surname of other passengers.
	 */
	unCheck : function()
	{
	  if($("#sameSurname"))
	  {
	     $("#sameSurname").attr('checked', false);
	  }
	}
};


/***This function shall populate the cv2avs section with the personal details section.*/
var AddressPopulationHandler =
{
   handle:function()
   {
      if(document.getElementById("useAddress").checked)
      {
	      $("#cardHouseName").val();
		   $("#cardAddress1").val();
		   $("#cardAddress2").val();
		   $("#cardTownCity").val();
		   $("#cardCounty").val();
		   $("#cardPostCode").val();

		    document.getElementById("cardHouseName").value=streetaddress1;
		    document.getElementById("cardAddress1").value=streetaddress2;
			document.getElementById("cardTownCity").value=streetaddress3;
			document.getElementById("cardAddress2").value=streetaddress2_1;
			document.getElementById("cardAddress3").value=streetaddress2_2;
			document.getElementById("cardPostCode").value=cardBillingPostcode;
			document.getElementById("cardCounty").value=county;

			document.getElementById("payment_0_selectedCountryCode").value=selectedcountryforleadpassenger;

      }
   }
};

function openWindow(url)
{
   window.open(url,'blank');
	return false;
}

/** Function to redraw the specified section to avoid IE border breaking issue. */
function forceRedraw(){
   if ($.browser.msie){
      var element = $("#contentCol2")[0];
	   var emptyTextNode = document.createTextNode(' ');
	   element.appendChild(emptyTextNode);
	   emptyTextNode.parentNode.removeChild(emptyTextNode)
   }
}
function setIssueField()
{

  			 if(PaymentInfo.selectedCardType!='none' && PaymentInfo.selectedCardType != null && PaymentInfo.selectedType != "")
	 {
        // alert('setIssueField')

  				 document.getElementById('securityCode').setAttribute('maxlength',getCardDetails(PaymentInfo.selectedCardType)[1]);
				 document.getElementById('securityCode').setAttribute('minlength',getCardDetails(PaymentInfo.selectedCardType)[0]);
				 if(getCardDetails(PaymentInfo.selectedCardType)[1]==4)
				 {

						 document.getElementById('blinky').innerText = "CVV number is your last 4 digits on the signature strip on the reverse of your card.";
				 }
				 else{
				 document.getElementById('blinky').innerText = "CVV number is your last 3 digits on the signature strip on the reverse of your card.";

				 }

  				 /**
			  $('payment_0_securityCode').setAttribute('maxlength',getCardDetails(PaymentInfo.selectedCardType)[1]);
			   $('payment_0_securityCode').setAttribute('minlength',getCardDetails(PaymentInfo.selectedCardType)[0]);
               **/

			  if(getCardDetails(PaymentInfo.selectedCardType)[2] == 'true')
			  {

				document.getElementById("issuerequiredlabel").style.display="block";
				 document.getElementById("issuerequiredfield").style.display="block";

					 issueflag=true;
			 }
			 else
			 {
				  issueflag=false;
					document.getElementById("issuerequiredlabel").style.display="none";
					document.getElementById("issuerequiredfield").style.display="none";
			}
	 }
	 else
   {
      issueflag=false;
      document.getElementById("issuerequiredlabel").style.display="none";
	   document.getElementById("issuerequiredfield").style.display="none";
   }

}

/** Common implementation for client side on blur and submit validation error display */
function commonSubmitBlurErrorDisplay(args){
   var contentClass = $("." +args[0].attr("id")+ "Error");
   var id=args[0].attr("id")+ "Error";
   var topText = args[0].attr("alt");
   var topContainer = $("#topError");
   contentClass.addClass("formError");
   $("#errorSummary").removeClass("hide");
   if($("."+ id+" #"+id).length != 1)
   {
      if(id=="expiryDateMMError" || id=="expiryDateYYError")
      {
         if($("li ."+id+" p.formErrorMessage").length != 1)
	     {
	        $("<p class='formErrorMessage' id='"+id+"'>"+args[2] +"</p>").prependTo(contentClass);
		    $("<li id='"+args[0].attr("id")+"TopError'>"+topText+"</li>").appendTo(topContainer);
			forceRedraw();
	     }
	  }
	  else
	  {
	     if(id.indexOf("surName") != -1)
	     {
	        if($("li ."+id+" p.formErrorMessage").length == 1)
		    {
		       var pClass = $("." +args[0].attr("id")+ "Error p")
			   pClass.after($("<p class='formErrorMessage' id='"+id+"'>"+args[2] +"</p>"));
			}
			else
			{
			   $("<p class='formErrorMessage' id='"+id+"'>"+args[2] +"</p>").prependTo(contentClass);
			}
	     }
		 else
		    $("<p class='formErrorMessage' id='"+id+"'>"+args[2] +"</p>").prependTo(contentClass);
		 $("<li id='"+args[0].attr("id")+"TopError'>"+topText+"</li>").appendTo(topContainer);
		 forceRedraw();
	  }
   }
   $("#commonError").addClass("hide");
}