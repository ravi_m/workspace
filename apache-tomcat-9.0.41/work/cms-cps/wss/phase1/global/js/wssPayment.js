/***
 *  File containing functionality specific script for the WSS.
 *
 *  It contains following
 *  a. Constants (id, class name etc.)
 *  b. Common functions (functions used by more than one functionality)
 *
 *  It contains following functionalities
 *  1. On-load functionality
 *  2. Adding events for payment fields Unobtrusively
 *  3. Updating payment page for deposit type selection
 *  4. Updating payment page for values entered in amount to pay textfield
 *  5. Updating payment page for card type selection
 *  6. Browser back functionality
 *
 */

/*---------------------------a. Constants -------------------------------------------------*/
/**
 * Object containing Class names used in Payment Page.
 */
var PaymentCssClasses =
{
  /***Class names used for payment update on payment page */
  PAYABLE_AMT_CLASS      : "calPayableAmount",
  TOTAL_AMOUNT_CLASS     :"totalAmount",
  DEPOSIT_AMOUNT_CLASS   :"depositAmount",
  AMOUNT_PAID_CLASS      :"amountPaid",

  /** Deposit radio button class names */
  DEPOSIT_RADIO_CLASS : "deposits",

  HIDE                 :"hide",
  SHOW                 :"show",

  IN_ERROR             :"inError"

};

/***
 * Object holding ids in payment page
 */
var PaymentPageIds =
{
  /** card type id */
  CARD_TYPE_ID              : "cardType",
  /** Amount to Pay  Id . It allows user to enter amount if pay in part radio button is selected  */
  AMOUNT_TO_PAY_ID          : "amountToPay",
  TOTAL_AMT_PAYABLE         :"totalAmountPayable",

  EXPIRY_MONTH              :"expiryMonth",
  EXPIRY_YEAR               :"expiryYear",

  CARD_NUMBER               :"cardNumber",
  CARD_NAME                 :"nameOnCard",

  SECURITY_CODE             :"cardSecurityCode",
  /** Id of span/div tag containing the text which appears next to security code field. */
  SECURITY_CODE_CAPTION     :"cardSecurityCodeLink",

  ISSUE_NO                  :"issueNumber",
  ISSUE_NO_CONTROL_GROUP    :"issueNumberControlGroup",

  PAGE_OUTCOME         : "pageOutcome",
  ERROR_LIST           : "errorList",
  ERROR_LIST_CONTAINER :"errorListContainer",

  PAGE_MESSAGE         :"pageMessage",
  ERROR_INTRO          : "errorIntro"
}

var PaymentConstants =
{
  /**Full cost deposit radio option value */
  FULL_COST           : "fullCost",

  /** Pay in Part is a deposit radio option value, to allow user to enter deposit amount in amount to pay field */
  PAY_IN_PART          : "partialDeposit",

  DEFAULT_CARD_SELECTION    : "",
  DEFAULT_SECURITY_CODE_LEN : 3,
  TOP_ERROR_MESSAGE    :"We cannot process your payment",
  ERROR_INTRO_TEXT     : "Please check the following details are correct"
}

/*-----------------*****END constants *****-----------------------------------------*/

/*----------------------b. Common functions---------------------------------------------------------*/
/**
 * Gets the currency that is currently used for the page.
 */
function getCurrency()
{
  var currency = $("currencyText");

  if( currency )
  {
    currency = currency.value;
  }

  return currency;
}

/**
 * Updates amounts
 */
function updateAmountsOnPaymentPage()
{
	//Update the textfields
    displayTheAmountsInTextFields(PaymentCssClasses.DEPOSIT_AMOUNT_CLASS , PaymentInfo.selectedDepositAmount );
    displayTheAmountsInTextFields(PaymentCssClasses.PAYABLE_AMT_CLASS , PaymentInfo.calculatedPayableAmount );

    //display the amounts on payment page
    displayTheAmounts(PaymentCssClasses.TOTAL_AMOUNT_CLASS , 1*PaymentInfo.totalHolidayPrice + 1*PaymentInfo.totalCardCharge );
    displayTheAmounts(PaymentCssClasses.AMOUNT_PAID_CLASS , 1*PaymentInfo.amountPaid + 1*PaymentInfo.totalCardCharge );
}

/** Function for updating amount based on the
 ** style class and amount.
**/
function displayTheAmounts(amountClassName , amount)
{
  //Display the fields which have given class name with the amount.
   var amts = $$('.'+amountClassName);
   var currency = getCurrency();
   for(var i=0; i<amts.length; i++)
   {
     amts[i].innerHTML = currency + parseFloat(amount).toFixed(2);
   }
}

/** Function for updating amounts in text fields based on the
 **  style class and amount.
**/
function displayTheAmountsInTextFields(amountClassName , amount)
{
  //Display the fields which have given class name with the amount.
   var amts = $$('.'+amountClassName);
   for(var i=0; i<amts.length; i++)
   {
     amts[i].value = roundOff(1*amount ,2);
   }
}
/*-----------------*****END Common functions*****---------------------------------------------------*/


/*----------------------1. Onload functionality-------------------------------------------------*/
/**
 * Makes an ajax call to check whether a holiday is new or already booked
 *
 */
function initialisePaymentEvents()
{
  //If javascript is disabled following is not displayed
  amountToPayField = $(PaymentPageIds.AMOUNT_TO_PAY_ID);
  amountToPayField.value = PaymentInfo.firstDepositAmount;
  amountToPayField.readOnly = true;

  totalAmountPayableTextField = $("totalAmountPayableControlGroup");
  totalAmountPayableTextField.removeClassName(PaymentCssClasses.HIDE);
  totalAmountPayableTextField.addClassName(PaymentCssClasses.SHOW);

  $(PaymentPageIds.TOTAL_AMT_PAYABLE).value = PaymentInfo.firstDepositAmount;

  issueNoContainer = $(PaymentPageIds.ISSUE_NO_CONTROL_GROUP)
  if(!(issueNoContainer.hasClassName("inError")))
  {
	  issueNoContainer.addClassName(PaymentCssClasses.HIDE);
  }

  //Initialize payment events or backbutton functionality based on status of new holiday
  performAjaxUpdate( "ConfirmBookingServlet", null, responseCheckForNewHoliday );
}

/**
 * Call back function of checkForNewHolidayAndInitializeEvents().
 * Sets
 *
 * @param request
 * @return isNewHoliday
 */
function responseCheckForNewHoliday(request)
{
  if((request.readyState == 4) && (request.status == 200))
  {
    var result = request.responseText;
    initializeOnloadEvenets(result)
  }
}

/**
 * Initialises:
 * - Payment Events, Low Deposit events, Promotional Code event
 * - Setup the App config object
 * -
 *
 */
function initializeOnloadEvenets(isNewHoliday)
{
	if( isNewHoliday == "true" )
    {
      setToDefault();
      initializeDepositRadioButtons();
      initializeCardTypeDropDown();
      initializeAmountToPayTextField();

      //Show deposit amount  in amount to pay and total amount payable based on deposit radio selection when page loads
      //Commented, if needed uncommented
      updatePaymentInfo();
      displayTheAmountsInTextFields(PaymentCssClasses.DEPOSIT_AMOUNT_CLASS , PaymentInfo.selectedDepositAmount );
      displayTheAmountsInTextFields(PaymentCssClasses.PAYABLE_AMT_CLASS , PaymentInfo.selectedDepositAmount );
    }
    else
    {
    	backButtonFunctionality();
    }
}

/**
 * Intitializes onclick event for deposit type radio button
 *
 */
function initializeDepositRadioButtons()
{
   var depositRadioClassNames = $$('.'+PaymentCssClasses.DEPOSIT_RADIO_CLASS);
   for(var i=0,len = depositRadioClassNames.length ; i<len ; i++)
   {
      depositValue  = depositRadioClassNames[i].value;
      depositRadio = $(depositRadioClassNames[i].id);

      depositRadio.onclick = function(event)
      {
        //Non-ajax: handler in cardCharges.js will be called when radio button is clicked
    	//We need to send the selected radio button's value dynamically
          depositTypeHandler(this.value );
      }

      if(depositRadio.checked)
      {
        	PaymentInfo.depositType = trimSpaces(depositValue);
      }
   }

   if(PaymentInfo.depositType=="" || PaymentInfo.depositType==null)
   {
	   PaymentInfo.depositType = "fullCost";
   }
}

/**
 * Intitializes onchange event for card type drop down
 * @return
 */
function initializeCardTypeDropDown()
{
  $(PaymentPageIds.CARD_TYPE_ID).onchange = function( event )
  {
	cardTypeHandler();
  };
}

/**
 * Intitializes onblur event for amount to pay text field
 * @return
 */
function initializeAmountToPayTextField()
{
  field = $(PaymentPageIds.AMOUNT_TO_PAY_ID);
  if(field)
  {
	field.onchange = function( event )
    {
      amountToPayHandler();
    };
  }
}
/*-------------------------*****END Onload functionality *****-------------------*/


/*--------------------------2. Updating payment page for deposit type selection--------------------------------*/
/**
 * Function will be called from common function handleDepositSelection() when a radio button is selected
 **/
function depositTypeHandler(selectedDepositType)
{
	//Set depositValue to PaymentInfo.depositType.
	PaymentInfo.depositType = trimSpaces((selectedDepositType));

	PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
    displayTheAmountsInTextFields(PaymentCssClasses.DEPOSIT_AMOUNT_CLASS , PaymentInfo.selectedDepositAmount );

	amountTopayField = $(PaymentPageIds.AMOUNT_TO_PAY_ID);
	if(amountTopayField)
	{
	  //Refresh the field by removing any validation error.
	  //removeValidationError( PaymentPageIds.AMOUNT_TO_PAY_ID  );
	  if(PaymentInfo.depositType == PaymentConstants.PAY_IN_PART )
	  {
	    amountTopayField.value = "";
		$("totalAmountPayable").value = "";
		amountTopayField.readOnly = false;
		amountTopayField.focus();
	  }
	  else
	  {
	    amountTopayField.readOnly = true;
	    amountToPayHandler();
	  }
	}

}

/*--------------*****END Updating payment page for deposit type selection *****-------------------*/


/*-------------- Updating payment page for values entered in amount to pay textfield ----------------------*/
/**
*  This function will be called to update payment page based on value entered in "amount to pay" field
*/
function amountToPayHandler()
{
   var amountToPay = $(PaymentPageIds.AMOUNT_TO_PAY_ID);
   var amountToPayValue = amountToPay.value;

   //Test whether entered amount is valid number
   if(isNaN(1*amountToPayValue))
   {
	   $("totalAmountPayable").value = "";
   }
   //Test whether entered amount is negative
   else if(1*amountToPayValue<0)
   {
	 $("totalAmountPayable").value = "";
   }
   else
   {
	 depositAmountsMap.put(PaymentConstants.PAY_IN_PART , amountToPayValue);
     //We have to update payment info and show card charge etc
     updatePaymentInfo();
     updateAmountsOnPaymentPage();
   }
}
/*------------*****END Updating payment page for values entered in amount to pay textfield *****--------------*/

/*--------------------------3. Updating payment page for card type selection--------------------------------*/
/**
 *  This function is invoked from common function "handleCardSelection(index)"
 *   when user selects a card type in drop down
 **/
function cardTypeHandler()
{
  PaymentInfo.selectedCardType = getCardType();

  PaymentInfo.isAMEX = (PaymentInfo.selectedCardType &&
	                         (PaymentInfo.selectedCardType).indexOf("AMERICAN_EXPRESS") != -1)? true : false;

  var cardPaymentObject = toPaymentObject( $(PaymentPageIds.CARD_TYPE_ID).value);

  //Show issue number field if card is a debit card, otherwise hide it.
  showOrHideIssueNumber( cardPaymentObject );

  // Change max length of security number field to card's security code length
  refreshSecurityCodeSection( cardPaymentObject );

  //Call amount to pay handler to payment page
  amountToPayHandler();

  //Show or hide "include credit card charge"
  if(1*PaymentInfo.totalCardCharge>0.0)
  {
    $("totalAmountPayableCaption").removeClassName(PaymentCssClasses.HIDE);
  }
  else
  {
	  $("totalAmountPayableCaption").addClassName(PaymentCssClasses.HIDE);
  }
}

function getCardType()
{
  var selectedCardValue = $(PaymentPageIds.CARD_TYPE_ID).value;
  var selectedCardArray = selectedCardValue.split("|");
  var selectedCard = (selectedCardArray!=""&&selectedCardArray) ? selectedCardArray[0] : null;
  return selectedCard;
}

/**
 * Returns an object which represents the values contained for the parameter, cardTypeSelectionValue.
 *
 * The the value cannot be parsed then null will be returned.
 *
 * The properties are:
 * - cardType (VISA, AMERICAN_EXPRESS, etc)
 * - eNumValue (DCard, CCard, etc)
 * - securityCodeLength
 * - cardCharge
 * - hasIssueNumber (true or false)
 * - chargePercentage
 */
function toPaymentObject( cardTypeSelectionValue )
{
  var METHOD   = 0;
  var ENUM     = 1;
  var tmpArray = cardTypeSelectionValue.split( "|" );

  var cardPaymentObject = null;

  if( tmpArray.length > 1 )
  {
    cardPaymentObject                    = new Object();
    cardPaymentObject.cardType           = tmpArray[METHOD];
    cardPaymentObject.enumValue          = tmpArray[ENUM];
    cardPaymentObject.securityCodeLength = 3;
    cardPaymentObject.cardCharge         = 0.0;
    cardPaymentObject.hasIssueNumber     = false;
    cardPaymentObject.chargePercentage   = 0;

    var securityCodeLength = $( cardPaymentObject.cardType + "_securityCodeLength" );
    var cardCharge         = $( cardPaymentObject.cardType + "_charges" );
    var chargePercentage   = $( cardPaymentObject.cardType + "_charge" );
    var cardHasIssueNumber = $( cardPaymentObject.cardType + "_issueNo" );

    if( securityCodeLength )
    {
      cardPaymentObject.securityCodeLength = securityCodeLength.value;
    }

    if( cardCharge )
    {
      cardPaymentObject.cardCharge = cardCharge.value;
    }

    if( chargePercentage )
    {
      cardPaymentObject.chargePercentage = chargePercentage.value;
    }

    if( cardHasIssueNumber )
    {
      cardPaymentObject.hasIssueNumber = ( cardHasIssueNumber.value == "true" );
    }
  }

  return cardPaymentObject;
}

/** Displays or hides issue number fields based on card type selection */
function showOrHideIssueNumber( cardPaymentObject )
{
  // Show or hide issue number field
  if( cardPaymentObject && cardPaymentObject.hasIssueNumber )
  {
    Element.removeClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.HIDE);
    Element.addClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.SHOW);
    Element.removeClassName("issueNumberErrorMessage", PaymentCssClasses.SHOW);
    Element.addClassName("issueNumberErrorMessage", PaymentCssClasses.HIDE);
  }
  else
  {
    $(PaymentPageIds.ISSUE_NO).value = "";
    Element.removeClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.IN_ERROR);
    Element.addClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.HIDE);
    Element.addClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.HIDE);
    Element.removeClassName(PaymentPageIds.ISSUE_NO_CONTROL_GROUP, PaymentCssClasses.SHOW);
  }
}

/*** Based on card type selection
 * i. Changes maxLength attribute of security number field
 * ii. Updates security code message if applicable
 * @return
 */
function refreshSecurityCodeSection( cardPaymentObject )
{
  var securityCodeField = $(PaymentPageIds.SECURITY_CODE);

  // If the selected card has a different length security number than field allows
  if(cardPaymentObject && cardPaymentObject.securityCodeLength != securityCodeField.maxLength)
  {
    securityCodeField.clear();
    securityCodeField.maxLength = parseInt(cardPaymentObject.securityCodeLength);
    updateSecurityMessages( cardPaymentObject.cardType, cardPaymentObject.securityCodeLength );
  }
  else
  {
  //
  updateSecurityMessages(PaymentConstants.DEFAULT_CARD_SELECTION, PaymentConstants.DEFAULT_SECURITY_CODE_LEN);
  }
}

/* Updates the security number validation and info messages based
 * on selected card's security number length */
function updateSecurityMessages(cardType, securityNumLen)
{
  var infoMsg       = "The last " + securityNumLen + " digits on the reverse of your card";
  var validationMessage = PaymentFields['cardSecurityCode'].msgNonAmex;
  // If Amex card is selected, change validation and info messages

  if( PaymentInfo.isAMEX )
  {
    infoMsg       = "The 4 digits above your account number on the front of your card";
    validationMessage = PaymentFields['cardSecurityCode'].msgAmex;
  }

  Element.update( $(PaymentPageIds.SECURITY_CODE_CAPTION) , infoMsg );
  Element.update( $("cardSecurityCodeErrorMessage") , validationMessage );
}

/*-------------------------*****END Updating payment page for card type selection *****-------------------*/

/*----------------------Browser back functionality ------------------------------------------------*/
/**
 * function will be called when user clicks on the browser back button in the confirmation page
 * It makes all fields read only and changes submit button to action neutral (act as "Forward").
 */
function backButtonFunctionality()
{
  /*$(PaymentPageIds.PAGE_OUTCOME).removeClassName(PaymentCssClasses.HIDE);
  $(PaymentPageIds.PAGE_MESSAGE).innerHTML = "Your payment has been confirmed";
  $(PaymentPageIds.ERROR_LIST_CONTAINER).addClassName(PaymentCssClasses.HIDE);

  makeDropDownsReadOnly(PaymentPageIds.CARD_TYPE_ID);
  makeDropDownsReadOnly(PaymentPageIds.EXPIRY_MONTH);
  makeDropDownsReadOnly(PaymentPageIds.EXPIRY_YEAR);

  $(PaymentPageIds.CARD_NUMBER).readOnly = true;
  $(PaymentPageIds.CARD_NAME).readOnly = true;

  $(PaymentPageIds.SECURITY_CODE).readOnly = true;

  $(PaymentPageIds.ISSUE_NO).readOnly = true;
  */
  setToDefault();
  hidePayNowButton();
}

/**
 *
 *
 */
function hidePayNowButton()
{
  var payNowButtonSpan = $("forwardButton");
  if(payNowButtonSpan)
  {
	  payNowButtonSpan.innerHTML = "";
	  //payNowButtonSpan.innerHTML = '<input type="button" id="Forward" title="Forward" class="primary" alt="" value="Forward" onClick="history.forward()" />';
  }
}

/**
 * Make all drop downs read only. So user will not be able to change the drop down
 *
 * @param dropdownId
 * @return
 */
function makeDropDownsReadOnly(dropdownId)
{
  var dropdown = $(dropdownId);
  var chosen = dropdown.options.selectedIndex;
  dropdown.onchange = function()
  {
	dropdown.options.selectedIndex = chosen;
  }
  dropdown.className = "greyed";
}
/*-----------------*****END Browser back functionality *****----------------------------------------*/

/**
 *Updates essential fileds when user clicks submit button.
 */
function updateEssentialFields()
{
  $("total_transamt").value = PaymentInfo.calculatedPayableAmount;
  var sel = document.getElementById('country');
  document.getElementById('payment_0_selectedCountry').value = sel.options[sel.selectedIndex].value;
}


/**
 * Contains field names which has to be set to default when page loads.
 *
 */
function setToDefault()
{
  //Set to default

  $(PaymentPageIds.CARD_TYPE_ID).selectedIndex = 0;

   $(PaymentPageIds.EXPIRY_MONTH).selectedIndex = 0;
  $(PaymentPageIds.EXPIRY_YEAR).selectedIndex = 0;

  $(PaymentPageIds.CARD_NUMBER).value = "";
  $(PaymentPageIds.CARD_NAME).value = "";

  $(PaymentPageIds.SECURITY_CODE).value = "";

  $(PaymentPageIds.ISSUE_NO).value = "";

}



/*----------------------PopUp function -----------------------------------------------------------*/
/**
 * Shows the popup
 */
function Popup(popURL,popW,popH,attr)
{
   if (!popH) { popH = 350 }
   if (!popW) { popW = 600 }
   if(popURL.indexOf("http")==-1)
   {
     popURL = clientDomainURL + popURL;
   }
   var winLeft = (screen.width-popW)/2;
   var winTop = (screen.height-popH-30)/2;
   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
   popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
   popupWin.close()
   popupWin=window.open(popURL,"popupWindow",winProp);
   popupWin.window.focus()
}
/*----------------------*****END PopUp function *****-------------------------------------------------*/







