/*
 * Copyright (c) 2009 TUI Travel Limited
 *
 * Version 1.0.0
 * Demo: TBA
 * Author: Scott Dickson
 *
 * How this works:
 * - The script looks for input type hidden elements that have the class, tuiCalendar and then simply make them a JQuery datepicker.
 *
 * The id of element that has the class (tuiCalendar) will be used as the suffix to identify the day and month/year drop
 * down lists.  A coding convention has been used to make life simpler for the developer.
 *
 * The MonthYear drop down list values must be m yyyy format for it to work. e.g. <option value="7 2009">July 2009</option>
 *
 * An example of how it is used can be seen below:
 *
 * Element having the tuiCalendar class.
 *
 * <input type="hidden" id="departure" class="tuiCalendar" />
 *
 * <select id="departureDay" name="departureDay">
 * ...
 * </select>
 *
 * <select id="departureMonthYear" name="departureMonthYear">
 * ...
 * </select>
 *
 * Configurable properties:
 * - buttonImage
 * - buttonText
 * - dayNames (defaults ["Sun","Mon" ... etc] An array of String values)
 * - maxDate (optional)
 * - minDate (defaults to todays date)
 *
 * FUTURE Improvements:
 * - Prevent the user from selecting 31 days for a 30 day month or February.
 *
 * $LastChangedDate: 2009-02-08 00:28:12 +0000 (Sun, 08 Feb 2009) $
 * $Rev: 6185 $
 *
 * The noConflict() allows JQuery to be used with Prototype.
 * This must be placed after JQuery is loaded and before the other library is loaded. Otherwise it may not work in FF2.
 */

var $j = jQuery.noConflict();
var tuiDatePicker = new TuiDatePicker();

function TuiDatePicker()
{
  DAY_SUFFIX = "Day",
  MONTH_YEAR_SUFFIX = "MonthYear",
  buttonImage = "",
  buttonText = "Choose a date",
  dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  maxDate = '+18m',
  minDate = new Date(),

  /**
   * Sets up the default values for all instances of the date picker and also adds the date picker
   * to input fields that have the class, tuiCalendar
   */
  TuiDatePicker.prototype.init = function()
  {
    $j.datepicker.setDefaults(
     { beforeShow: tuiDatePicker.beforeShow,
       buttonImageOnly: true,
       buttonText: buttonText,
       changeMonth: true,
       changeYear: true,
       dateFormat: 'yy-m-d',
       dayNamesMin: dayNames,
       firstDay: 1,
       gotoCurrent: true,
       numberOfMonths: 1,
       onSelect: tuiDatePicker.dateSelected,
       showOn: 'button',
       minDate: minDate,
       maxDate: maxDate,
       showOtherMonths: true
       });

    $j("input[type=hidden].tuiCalendar").datepicker( {buttonImage: buttonImage} );
  },

  /*
   * Sets the button image to use.
   *
   * @param newBtnImg - Is the path and image file name.
   */
  TuiDatePicker.prototype.setButtonImage = function(newBtnImg)
  {
    buttonImage = newBtnImg;
  },

  /*
   * Sets the button text for the tool tip for the image.
   *
   * @param newBtnTxt - Is the button text tool tip.
   */
  TuiDatePicker.prototype.setButtonText = function(newBtnTxt)
  {
    buttonText = newBtnTxt;
  },

  /*
   * Sets the day names used for the headings of the days of the week.
   *
   * @param newDayNames - An array of string values.  One for each day of the week
   */
  TuiDatePicker.prototype.setDayNames = function(newDayNames)
  {
    dayNamesMin = newDayNames;
  },

  /*
   * Sets the minimum date available in the date picker.  By default the minimum date is set to today's date.
   *
   * @param newMinDate - Is a date object containing the minimum date available in the date picker.
   */
  TuiDatePicker.prototype.setMinDate = function(newMinDate)
  {
    minDate = newMinDate;
  },

  /*
   * Sets the maximum date available in the date picker.
   *
   * @param newMaxDate - Is a date object containing the maximum date available in the date picker.
   */
  TuiDatePicker.prototype.setMaxDate = function(newMaxDate)
  {
    maxDate = newMaxDate;
  },

  /*
   * Is called when the user selects a date.
   *
   * @param selectedDate - Is the date selected by the user, in the format of, 'dateformat'
   * @param dpInst - Is the date picker instance
   */
  TuiDatePicker.prototype.dateSelected = function(selectedDate, dpInst)
  {
    var dateParts = parseDate(selectedDate);
    updateDayName(selectedDate,dpInst);
    $j("#" + dpInst.id + DAY_SUFFIX).selectOptions(""+dateParts.day, true);
    $j("#" + dpInst.id + MONTH_YEAR_SUFFIX).selectOptions(dateParts.month + " " + dateParts.year, true);
    $j("#" + dpInst.id + DAY_SUFFIX).trigger('change');
  },

  /*
   * As its name implies, this function is called before the date picker is shown.
   *
   * @param selectedDate - Is the date selected field, which is the hidden field in our case.
   */
  TuiDatePicker.prototype.beforeShow = function(selectedDate, dpInst)
  {
    var day = "#" + dpInst.id + DAY_SUFFIX;
    var monthYear = "#" + dpInst.id + MONTH_YEAR_SUFFIX;

    var dayValues = $j(day).selectedValues();
    var monthYearValues = $j(monthYear).selectedValues();

    var monthYear = monthYearValues[0].split(" ");

    // Now uppdate the hidden field with the selected values in the drop down lists.
    $j(selectedDate).val(monthYear[1] + '-' + monthYear[0] + '-' + dayValues[0]);

    $j(this).val($j(selectedDate).val());

    if (dpInst.id == 'departure')
    {
       return {};
    }
    else
    {
      var departureDatevalue = "";
      var arrivalDatevalue ="";
      var dayName = $j('#departureDay').selectedValues();
      var mnthName = $j('#departureMonthYear').selectedValues();
      var mnthList = mnthName[0].split(" ");
      departureDatevalue = mnthList[0] + "/" + dayName + "/" + mnthList[1];
      arrivalDatevalue = monthYear[0] + "/" + dayValues[0] + "/" + monthYear[1];
      if (departureDatevalue == arrivalDatevalue)
      {
         return {minDate:new Date(arrivalDatevalue)};
      }
      else
      {
         return {minDate:new Date(departureDatevalue)};
      }
    }
  },

  /*
   * Parses the dateStr, using the date format, dateFormat, which is yy-m-d.
   *
   * @param dateStr - Is the string to parse.
   *
   * @return An object containing three fields, day, month & year or null if the date couldn't be parsed.
   */
  parseDate = function(dateStr)
  {
    var dateParts = dateStr.split("-");

    if(dateParts.length == 3)
    {
      return {day: dateParts[2], month: dateParts[1], year: dateParts[0]};
    }
    else
    {
      return null;
    }
  }

/*
 * Updates the day name of the selected from the calendar.
 */
  updateDayName = function(dateStr,dateInstance)
  {
    var days_of_week = new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
    var dateParts1 = dateStr.split("-");
    var dateVar = dateParts1[1] + "/" + dateParts1[2] + "/" +dateParts1[0];
    var calendar = new Date(dateVar);
    calendar.setMonth(dateParts1[1]-1);
    calendar.setYear(dateParts1[0]);
    calendar.setDate(dateParts1[2]);

    if (dateParts1[2] == calendar.getDate())
    {
       if (dateInstance.id=='departure')
       {
          var departureDayName = $j("#departWeekDay");
          departureDayName.text(days_of_week[calendar.getDay()]);
       }
       else if (dateInstance.id=='returning')
       {
          var arrivalDayName = $j("#returningWeekDay");
          arrivalDayName.text(days_of_week[calendar.getDay()]);
       }
     }
  }

};

$j(document).ready(function()
{
  tuiDatePicker.setButtonImage("/static/wss/images/icons/calendar.gif");
  tuiDatePicker.init();
});
