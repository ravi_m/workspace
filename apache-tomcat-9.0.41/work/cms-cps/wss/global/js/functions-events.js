var $j = jQuery.noConflict();

$j(document).ready(function(){
  loadExternalFile("/cms-cps/wss/global/css/js.css", "css");
  toggleOverlayWSS();
});

function toggleOverlayWSS(){
  var zIndex = 100;
  var overlayZIndex = 99;

  $j("a.blinkyOwner").click(function(e){
    var overlay = "#" + this.id + "Overlay";
    $j(overlay).show();
    $j(overlay + ".genericOverlay").css("z-index",zIndex);
    $j(overlay).closest(".overlay").css("z-index",overlayZIndex);
    $j(overlay).closest("li").css("z-index",overlayZIndex);
    zIndex++;
    overlayZIndex++;
    return false;
  });

  $j("a.close").click(function(){
    var overlay = this.id.replace("Close","Overlay");

    $j("#" + overlay).hide();
    return false;
  });
}

function loadExternalFile(fileName, fileType){
  if(fileType == "css"){
    var fileLink = "<link rel='stylesheet' type='text/css' href='" + fileName + "' media='screen'/>";
    $j("head").append(fileLink);
  }
}