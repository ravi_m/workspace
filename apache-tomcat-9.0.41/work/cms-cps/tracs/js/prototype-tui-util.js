/**
 * Provides utility functions that can be used in all branded applications.  No brand specific script can be placed in here as this is not the purpose of
 * this file.
 */
var TuiUtil =
{
  onLoadEventList : [],
  styleSheetList : [],

  /**
   * Initialisation function.
   */
  init: function()
  {
    onLoadEventList = new Array();
    styleSheetList = new Array();
  },

  /**
   * Programmatically load a CSS stylesheet
   *
   * @param filename - Is the CSS stylesheet to load.  It includes the path as well.
   * @param media - Is the media that the stylesheet will be applied to. i.e. screen,print
   */
  loadStylesheet : function( filename, media )
  {
    // ensure that the browser has all the DOM methods/properties we need
    if( !(document.getElementById) ||
      !(document.childNodes) ||
      !(document.createElement) ||
      !(document.getElementsByTagName) )
    {
      return;
    }

    // make a new link node to the stylesheet
    var link = document.createElement( "link" );
    link.href = filename;
    link.rel = "stylesheet";
    link.type = "text/css";
    link.media = media;

    // insert the stylesheet link into the document head
    document.getElementsByTagName('head')[0].appendChild(link);
  },

  /**
   * Programmatically load a script file.
   *
   * @param filename - Is the JavaScript file to load.  It includes the path as well.
   */
  loadJavaScript : function( filename )
  {
    // ensure that the browser has all the DOM methods/properties we need
    if( !(document.getElementById) ||
      !(document.childNodes) ||
      !(document.createElement) ||
      !(document.getElementsByTagName) )
    {
      return;
    }

    // make a new link node to the javascript file
    var link = document.createElement( "script" );
    link.src = filename;
    link.type = "text/javascript";

    // insert the stylesheet link into the document head
    document.getElementsByTagName('head')[0].appendChild(link);
  },

  /**
   * Load listener to load multiple functions onload
   *
   * @param func - Is the function to add to the onload event stack.
   */
  addLoadEvent : function( func )
  {
    var doesExist = ( onLoadEventList.indexOf( func ) != -1 );

    if( !doesExist )
    {
      onLoadEventList.push(func);

      // if there isn't currently an onload function...
      if( typeof window.onload != "function" )
      {
        // ...then just set it to this one
        window.onload = func;
      }
      else
      {
        // the old onload function
        var oldOnloadFn = window.onload;
        // and make a new onload function that will call the old one, and then func
        window.onload = function()
        {
          if( oldOnloadFn )
          {
            oldOnloadFn();
          }

          func();
          onLoadEventList.pop( func );
        };
      }
    }
    else
    {
      // Used for debugging purposes.  Simply uncomment.
      //alert( 'Duplicate function call.  The following function has already been added:\n\n' + func);
    }
  },

  stopEventBubbling : function( e )
  {
    if( !e )
    {
      var e = window.event;
    }
    e.cancelBubble = true; // IE
    if( e.stopPropagation )
    {
      e.stopPropagation()
    };
  }

};

TuiUtil.init();