/**
 ** This method handles the Deposit options display
**/
function selectedpaymentMode(id) {
	var getmode = id;
	var getfullDeposit = document.getElementById("full-amt");
	var gethalfDeposit = document.getElementById("half-amt");
	var getlowDeposit = document.getElementById("quarter-amt");
	var depositClass = document.getElementsByClassName("payment-details");
	if (getmode == "full-amt") {
		getfullDeposit.className += " highlighted-div";
		var fullCheck = document.getElementById("full");
		fullCheck.checked = "checked";
		if(gethalfDeposit != null){
			gethalfDeposit.className = "payment-details padding10pxnoBottom";
		}
		if(getlowDeposit != null){
			getlowDeposit.className = "payment-details padding10pxnoBottom";
		}
	} else if (getmode == "half-amt") {
		gethalfDeposit.className += " highlighted-div";
		var halfCheck = document.getElementById("half");
		halfCheck.checked = "checked";
		getfullDeposit.className = "payment-details padding10pxnoBottom";
		if(getlowDeposit != null){
			getlowDeposit.className = "payment-details padding10pxnoBottom";
		}
	} else {
		getlowDeposit.className += " highlighted-div";
		var lowdepositCheck = document.getElementById("initial");
		lowdepositCheck.checked = "checked";
		getfullDeposit.className = "payment-details padding10pxnoBottom";
		if(gethalfDeposit != null){
			gethalfDeposit.className = "payment-details padding10pxnoBottom";
		}
	}
}
function displayPaypalCharges() {
	$('CheckoutPaymentDetailsForm').action="javascript:makePayment();";
	var cardSectionElement = document.getElementById('card-details-hide');
	var isHidden = cardSectionElement.classList.contains('hide');
	var paybutton=document.getElementById('paypalbutton'); 
	isHidden ? cardSectionElement.classList.remove('hide') : '';
	jQuery('#paypalbutton').html('Book and Pay');
	paybutton.removeClassName('ppbutton');
}
/**
 ** This method handles
 *		1. Displaying of payment method
 *		2. Displaying the final Amount
 *		3. Card Type drop down toggling based on the condition
**/
function displayCreditCharges(id) {


	var getId = id;
	var getDiv = document.getElementById(id);
	var divClass = document.getElementsByClassName("paycard-details");
	var debitSection = document.getElementById("debitcardType");
	var creditSection = document.getElementById("creditcardType");
	var creditCharges= document.getElementById("creditCardCharges");
	var debitCharges = document.getElementById("debitCardCharges");
	var cardNumber = document.getElementById("cardNumber").value;
	var bookwithText= document.getElementById("paypalspanText");
        var paypalSection = document.getElementById("paypalType");
	var paypalCharges= document.getElementById("paypalCharges");

	//Reset to the default value
	document.getElementById('payment_type_credit').value = "PleaseSelect";
	document.getElementById('payment_type_debit').value = "PleaseSelect";

	// payment method change
	var creditCardAmt = document.getElementById("creditCardChargeAmt_fullCost").innerHTML;
	var debitCardAmt = document.getElementById("debitCardChargeAmt_fullCost").innerHTML;
        displayPaypalCharges();

	if (getId == 'creditcardType') {
		bookwithText.style.display = "none";
		var check = document.getElementById("credit");
		check.checked = "checked";
		if (check.checked) {
			creditCharges.style.display = "block";
			debitCharges.style.display = "none";
                        paypalCharges.style.display = "none";
			document.getElementById("creditCardFinalAmt").innerHTML=creditCardAmt;
			
		} else {
			creditCharges.style.display = "none";
			debitCharges.style.display = "none";
                        paypalCharges.style.display = "none";
		}
		creditSection.className += " highlighted-div";
		debitSection.className = "paycard-details";
        payPalFlag == true ? paypalSection.className = "paycard-details":'';


	} else if (getId == 'debitcardType') {
		bookwithText.style.display = "none";
		var check = document.getElementById("debit");
		check.checked = "checked";
		debitCharges.style.display = "block";
		creditCharges.style.display = "none";		
                paypalCharges.style.display = "none";	
		debitSection.className += " highlighted-div";
		creditSection.className = "paycard-details";
         payPalFlag == true ? paypalSection.className = "paycard-details":'';
		document.getElementById("debitCardFinalAmt").innerHTML=debitCardAmt;


	}
        else if (getId == 'paypalType') {
		var paypalAmt = document.getElementById("paypalChargeAmt_fullCost").innerHTML;
		var check = document.getElementById("paypal");
		check.checked = "checked";
		debitCharges.style.display = "none";
		paypalCharges.style.display = "block";
		creditCharges.style.display = "none";
		paypalSection.className += " highlighted-div";
		creditSection.className = "paycard-details";
		debitSection.className = "paycard-details";
		document.getElementById("paypalFinalAmt").innerHTML=paypalAmt;
		jQuery('#paypalbutton').html('');
		document.getElementById('paypalbutton').value = '';
		bookwithText.style.display = "block";
		var paybutton=document.getElementById('paypalbutton'); 
		paybutton.addClassName('ppbutton');
		
		$('CheckoutPaymentDetailsForm').action = "./paypal?token="+token + tomcatInstance;
		jQuery("#card-details-hide").addClass("hide");
	}

	if (document.getElementById('debitPaymentTypeCode').style.display == 'block'
			|| document.getElementById('creditPaymentTypeCode').style.display == 'block') {
		if (getId == 'creditcardType') {
			document.getElementById('debitPaymentTypeCode').style.display = 'none';
			document.getElementById('creditPaymentTypeCode').style.display = 'block';
		} else if (getId == 'debitcardType') {
			document.getElementById('debitPaymentTypeCode').style.display = 'block';
			document.getElementById('creditPaymentTypeCode').style.display = 'none';
		} /*else {
			document.getElementById('debitPaymentTypeCode').style.display = 'none';
			document.getElementById('creditPaymentTypeCode').style.display = 'none';
		}*/
	}
	if(payPalFlag==true){
		jQuery(".paycard-details").addClass("paycard-paypal");
	}
}

/*
This method handles
	1. Displaying of important information
	2. This method toggle depends upon the user click the header
*/
	var get_mod_value=1;
	function displayImportantInfo(id){
		var getId = document.getElementById(id);
		var mod_value = get_mod_value%2;
		get_mod_value++;
		if(mod_value==1){
			getId.className = "item-content";
			important_infosection.className = "item open";
		}
		if(mod_value==0){
			getId.className = "disNone";
			important_infosection.className = "item";
		}
	}
/* displayImportantInfo function- end */

/*
This method handles
	1. Displaying of tootltip information - total price
	2. This method using while mouseover and mouse out showing total price
*/
		function totalprice_displayTooltipInfo_mouseover(id){
			var displayTooltip = document.getElementById(id);
			//displayTooltip.style= "display: block; position: relative; left: 3px;";
			//displayTooltip.className="tooltip_visible";
			displayTooltip.style.display = "block";
		}
		/*displayTooltipInfo_mouseover - end */

		function totalprice_displayTooltipInfo_mouseout(id){
			var displayTooltip = document.getElementById(id);
			displayTooltip.style.display = "none";
		}
		/*displayTooltipInfo_mouseout - end */

/*Method end*/

/*
This method handles
	1. Displaying of tootltip information
	2. This method using while mouseover and mouse out showing card and security code information
*/
	function displayTooltipInfo_mouseover(id){
		var displayTooltip = document.getElementById(id);
		//displayTooltip.style= "display: block; position: relative; left: 3px;";
		displayTooltip.style.display = "block";
	}
	/*displayTooltipInfo_mouseover - end */

	function displayTooltipInfo_mouseout(id){
		var displayTooltip = document.getElementById(id);
		displayTooltip.style.display = "none";
	}
	/*displayTooltipInfo_mouseout - end */

/*Method end*/

	/*
	This method handles
		1. Hidden of alert message content
	*/
		function closeAlertMsg(id){
			document.getElementById(id).style.display="none";
		}
		/*closeAlertMsg method - end*/