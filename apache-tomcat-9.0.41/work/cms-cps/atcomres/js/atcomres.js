var submitterClicked = false;
var request;

$j("document").ready(function(e){
	summaryPanelOverlay();
	});
function summaryPanelOverlay()
{
 $j("#taxTerms").click(function(e)
 {
	var pos = $j(".taxesCharges").position();
	var height = $j(".taxesCharges").height();
	var top = parseInt((1*pos.top),10);
	$j("#taxTermsOverlay").css("top",top+height);
	return false;
 });}

function validateRequiredField()
{

   return true && passengerNamesInputValidation()
      && personalDetailsInputValidation()
      && paymentDetails_Validate()
      && contactAgreement_Validate()
	   && issuefieldvalidate()
	   && paymentAmount_Validate();
}

function preventDoubleClick()
{
   if (!submitterClicked)
   {
      submitterClicked = true;
      return true;
   }

   return false;
}

function passengerNamesInputValidation(ValidateEventArgs)
{
   if (!validateit('passenger'))
   {
      return false;
   }
   return true;
}

function personalDetailsInputValidation(ValidateEventArgs)
{
   if (!validateit('personaldetails'))
   {
      return false;
   }
   return true;
}

function paymentDetails_Validate(ValidateEventArgs)
{
   if (!validateit('payment'))
   {
      return false;
   }
  var expiryMonth = parseInt(document.getElementById('payment_expiryDateMonth').value,10);
   var expiryYear = parseInt(document.getElementById('payment_expiryDateYear').value,10);

   /* other field validation */
   var otherAmt = document.getElementById('othrAmt');
   var otherFlag = "";
   if(document.getElementById('other')){
   otherFlag = document.getElementById('other');
   }
   var selectedOption = document.getElementById("payment_0_cardType");

   var ns4 = (document.layers);
   var ns6 = (!document.all && document.getElementById);

   var today = new Date();
   currentMonth = parseInt(today.getMonth() + 1);
   var calib=(ns4 || ns6)?1900:0;
   currentYear = today.getYear() + calib;
   currentYear = currentYear + '';
   currentYear = currentYear.substring(2);
   currentYear = parseInt(currentYear);
   if (((expiryMonth < currentMonth) && (expiryYear == currentYear)))
   {
      alert("The expiry date specified has already passed.");
	  document.getElementById("payment_expiryDateMonth").focus();
      return false;
   }
   else if (expiryYear < currentYear)
   {
      alert("The expiry date specified has already passed.");
	  document.getElementById("payment_expiryDateYear").focus();
      return false;
   }

   if(selectedOption.value == "")
   {
     alert("Please select the Payment method")
	 selectedOption.focus();
      return false;
   }

   if (otherFlag != "" && otherFlag.checked)
   {
     if(parseFloat(otherAmt.value) < 0 || otherAmt.value == "")
	    {
    	 msg = "Other amount should be greater than zero ";
    	 addError(otherAmt,msg);
		  selectedOption.selectedIndex = 0;
		  PaymentView.highlightContainer("amountWithCardCharge","selected",false);
	      PaymentView.highlightContainer("amountWithoutCardCharge","selected",false);
	      document.getElementById("othrAmt").focus();
		  return false;
        }

   }

   return true;
}

function contactAgreement_Validate(ValidateEventArgs)
{
   var createSpan = document.createElement('span');
		createSpan.innerHTML = "";
	createSpan.id = " ";
	createSpan.className += "";

   if (!document['SkySales']['termAndCondition'].checked){
	  var msg = "\nSorry, but to proceed with your booking you\nmust click checkbox to accept our \nTerms and Conditions.\n";
   	 if(document.getElementById("errormsg")){
		document.getElementById("errormsg").remove();
	 }
		createSpan.id = "errormsg";
		createSpan.innerHTML="\nSorry, but to proceed with your booking you\nmust click checkbox to accept our \nTerms and Conditions.\n";
		document.getElementById("messg").appendChild(createSpan);
	  
	  //addError('termAndCondition',msg);
	  //document.getElementById("termAndCondition").focus();
	  //document.getElementById("messg").innerHTML = msg;
      return false;
   }
   return true;
}

function removeErr()
{
	if(document.getElementById("errormsg")){
		document.getElementById("errormsg").remove();
	 }
}
function askForPayment()
{
   return confirm("Thank you.\n\nWe are now ready to complete your reservation.\n\nPlease do not click the Pay, Refresh, Back or Stop buttons until the next page is displayed.\n\nIt may take up to 45 seconds to validate your payment information.  Thank you for your patience.\n\nAll booking details must be correct before proceeding. Please click the 'OK' button to begin authorisation.\n\nYour booking will then be final and not reversible.");
}
 var issueflag=false;
function formSubmit()
{
	document.getElementById("tourOperatorTermsAccepted").value = document.getElementById("termAndCondition").value;
  // document.getElementById('dayTimePhone').value = document.getElementById('personaldetails_phoneNumber').value ;

}

function updateEssentialFields()
{
   var totalAmountPaid = parseFloat(document.getElementById("payment_0_transamt").value+parseFloat(document.getElementById("payment_0_chargeIdAmount").value)).toFixed(2);
   document.getElementById("payment_0_transamt").value = document.getElementById("payment_totamtpaid").value;
   document.getElementById('payment_0_paymentmethod').value = document.getElementById("payment_0_paymentmethod").value;
   document.getElementById('payment_0_totalTrans').value = 1;
}

function filterPhoneField(inputField)
{
   inputField.value = inputField.value.replace(/[^0-9]/g, "");
}



/*DP function feesTaxes_breakDowns(taxDetails)
{
	var msg = '';
	msg = "Here is a breakdown of the taxes and fees\nthat apply to your purchase." + '\n\n\n';
	alert(msg + taxDetails);
}*/

/* updating card charges*/

function updateCardChargeForAtcomRes()
{

	changePayButton($('payment_type_0').value);
 if(PaymentInfo.totalAmount>0)
  {
	  handleDotNetCardSelection(0);
  }

  displayFieldsWithValuesForCard();
}
/*updating total cost */
function displayFieldsWithValuesForCard()
{

		   if(PaymentInfo.selectedCardType!="" && PaymentInfo.selectedCardType!=null  && calCardCharge>0)
		   {

			  document.getElementById("totalAmtDue").style.display="block" ;
			  /*if(PaymentInfo.chargeamt == 0 || calCardCharge != 0)
			  {
				   $('creditCardSurcharge').innerHTML=cardChargeMap.get(PaymentInfo.selectedCardType).split(",")[4]+" card surcharge";
				   document.getElementById("surcharge").innerHTML= PaymentInfo.currency;

				   $('surcharge').innerHTML+=calCardCharge;
			   }
			   else
			   {

			  	   $('surcharge').innerHTML=PaymentInfo.currency;
			  	   $('surcharge').innerHTML+=PaymentInfo.chargeamt.toFixed(2);
			   }*/

		   }

}
 function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
		return x1 + x2;
}
 function setIssueField(that)
 {
	var cardSelect = [];
	cardSelect[0] = that.value;

   	if(cardSelect[0])
	 {
			$('payment_0_securityCode').setAttribute('maxlength',getCardDetails(cardSelect)[1]);
			   $('payment_0_securityCode').setAttribute('minlength',getCardDetails(cardSelect)[0]);


			  if(getCardDetails(cardSelect)[2] == 'true')
			  {

					 document.getElementById("issuerequiredlabel").style.display="block";
					 document.getElementById("issuerequiredfield").style.display="block";

					 issueflag=true;
			 }
			 else
			 {

				  issueflag=false;
					document.getElementById("issuerequiredlabel").style.display="none";
					document.getElementById("issuerequiredfield").style.display="none";
			}
	 }
	 else
    {
	   issueflag=false;
       document.getElementById("issuerequiredlabel").style.display="none";
	   document.getElementById("issuerequiredfield").style.display="none";
    }

 }

  function issuefieldvalidate()
 {
     var cardType = document.getElementById("payment_0_cardType").value;

 	if (cardType=="SWITCH" || cardType=="SOLO")
	  {

		          var val = /^[0-9 ]*$/;
	     if(document.getElementById("issueNumber").value.length > 0)
	     {

		      if(!val.test(document.getElementById("issueNumber").value))
			 {
				msg = "Valid Issue Number Required"
				addError(issueNumber,msg);
					document.getElementById("issueNumber").focus();
				return false;
			 }
		    return true;
		}

	}
	return true;
  }

  function setnewsLetter()
 {
			// document.getElementById("newsLetter1").value	 = document.getElementById("newsLetter").checked ;
		 //document.getElementsByName("ecommunication_checked").value	 = document.getElementById("newsLetter").checked ;


 }

  /**This function changes the button based on whether the selected card belongs to the
   * Verified by Visa or Mastercard securecode*/
  function changePayButton(cardType)
  {
		var payButtonDescription = threeDCards.get(cardType);
		var element = document.getElementsByClassName('primary')[0];
		if (payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup")
		{
			element.value = "Proceed to payment";
		}
		else
		{
			element.value = "Pay" ;
		}

  }

function autoCompleteAddress()
{
	if (document.getElementById("autofill").checked == true) {
		document.getElementById("payment_0_street_address1").value = streetaddress1;
		document.getElementById("payment_0_street_address2").value = streetaddress2;
		document.getElementById("payment_0_street_address3").value = streetaddress3;
		document.getElementById("payment_0_postCode").value = cardBillingPostcode;
		document.getElementById("payment_0_selectedCountryCode").value = selectedcountryforleadpassenger;
		document.getElementById("payment_0_selectedCountry").value = selectedcountryforleadpassenger;
	} else {
		document.getElementById("payment_0_street_address1").value = '';
		document.getElementById("payment_0_street_address2").value = '';
		document.getElementById("payment_0_street_address3").value = '';
		document.getElementById("payment_0_postCode").value = '';
		document.getElementById("payment_0_selectedCountryCode").value = '';
		document.getElementById("payment_0_selectedCountry").value = '';
	}
	removeError(payment_0_street_address1);
	removeError(payment_0_street_address2);
	removeError(payment_0_street_address3);
	removeError(payment_0_postCode);
	removeError(payment_0_selectedCountryCode);
	removeError(payment_0_selectedCountry);
}

function paymentAmount_Validate(){

	var total_transamt = document.getElementById("total_transamt").value;
	if(0 > parseFloat(total_transamt)){
		alert("Payment amount should not be negative amount ");
		return false;
	}
	return true;
}

function resetPaymentField(){
    var payment_0_nameOnCard = document.getElementById("payment_0_nameOnCard");
	var payment_cardNumber = document.getElementById("payment_cardNumber");

	var payment_0_securityCode = document.getElementById("payment_0_securityCode");
	var payment_notes = document.getElementById("payment_notes");
	var issueNumber = document.getElementById("issueNumber");
    var todayDate = new Date();
		var payment_expiryDateMonth = document.getElementById("payment_expiryDateMonth");
	var payment_expiryDateYear = document.getElementById("payment_expiryDateYear");

	removeError(payment_0_nameOnCard);
	removeError(payment_cardNumber);
	removeError(payment_0_securityCode);
	removeError(payment_notes);
	removeError(issueNumber);

	payment_0_nameOnCard.value = '';
	payment_cardNumber.value = '';
	payment_notes.value = '';
	payment_0_securityCode.value = '';
	issueNumber.value = '';
	payment_expiryDateMonth.value = todayDate.getMonth()+1;
	payment_expiryDateYear.value = todayDate.getFullYear()%100;

}


function nonIntegratedPayValidation(){

	var paymentMethod = document.getElementById("payment_0_paymentmethod");

}

function paymentMethod_Update(){

 var selectedPayMethod = document.getElementById("paymentMethod");
 var slctPayMethodValue = selectedPayMethod.options[selectedPayMethod.selectedIndex].value;
 var integratedPayments = document.getElementById("integratedPayments");
 var nonIntegratedPayments = document.getElementById("nonIntegratedPayments");

 var integratedSubmit = document.getElementById("integratedSubmit");
 var nonIntegratedSubmit = document.getElementById("nonIntegratedSubmit");
 var nonIntegratedVoucher = document.getElementById("nonIntegratedVoucher");
 var nonIntegratedAuthcode = document.getElementById("nonIntegratedAuthcode");
 var nonIntegratedCardCharge = document.getElementById("nonIntegratedCardCharge");
 var totalTransaction = document.getElementById("payment_0_totalTransaction");
 var cardCharge = document.getElementById("payment_0_cardCharge");
 var totalAmount = document.getElementById("nonIntegrTransAmt");
 var contentCol1 = document.getElementById("integratedSummery");
 var nonIntegratedSummery = document.getElementById("nonIntegratedSummery");

 var paymentMethod = document.getElementById("payment_0_paymentmethod");
 paymentMethod.value=slctPayMethodValue;

  if(slctPayMethodValue == 'Dcard'){

      integratedPayments.show();
	  integratedSubmit.show();
	  contentCol1.show();
	  nonIntegratedPayments.hide();
	  nonIntegratedSubmit.hide();
	  nonIntegratedSummery.hide();

  }else if(slctPayMethodValue == 'Voucher'){

	  integratedPayments.hide();
	  integratedSubmit.hide();
	  totalTransaction.value = parseFloat(totalAmount.innerHTML);
	  nonIntegratedPayments.show();
	  nonIntegratedVoucher.show();
	  nonIntegratedSummery.show();
	  nonIntegratedAuthcode.hide();
	  nonIntegratedCardCharge.hide();
	  nonIntegratedCardCharge.hide();
	  nonIntegratedSubmit.show();
	  contentCol1.hide();

  }else if(slctPayMethodValue == 'Card'){

	  integratedPayments.hide();
	  integratedSubmit.hide();
	  nonIntegratedPayments.show();
	  nonIntegratedSummery.show();
	  nonIntegratedCardCharge.show();
	  totalTransaction.value = parseFloat(totalTransaction.value) + parseFloat(cardCharge.value);
	  nonIntegratedVoucher.hide();
	  nonIntegratedAuthcode.show();
	  nonIntegratedSubmit.show();
	  contentCol1.hide();

  }else{

	  integratedPayments.hide();
	  integratedSubmit.hide();
	  totalTransaction.value = parseFloat(totalAmount.innerHTML);
	  nonIntegratedPayments.show();
	  nonIntegratedSummery.show();
	  nonIntegratedVoucher.hide();
	  nonIntegratedAuthcode.hide();
	  nonIntegratedCardCharge.hide();
	  nonIntegratedSubmit.show();
	  contentCol1.hide();

	}
 }