var DepositAvailable;

function CardType() {
   var cardType = $F('PaymentCardType');
   if ((cardType == 'SWITCH') || (cardType == 'SOLO')) {
      $('IssueNumberTitle').style.display = 'inline';
      $('IssueNumber').style.visibility = 'visible';
   } else {
      $('IssueNumberTitle').style.display = 'none';
      $('IssueNumber').style.visibility = 'hidden';
      $('IssueNumberInput').value = '';
   }
   DynamicPriceUpdate('Payment');
}

function UpdateCostText() {
   var doc = window.frames["dynamicFrame"].document;
   $('TotalCostText').innerHTML = doc.getElementById('TotalCost').innerHTML;
   if (DepositAvailable) {
      // Deposit
      $('DepositCostText').innerHTML = doc.getElementById('DepositCost').innerHTML;
      // Full
      $('FullCostText').innerHTML = doc.getElementById('FullCost').innerHTML;
      // Outstanding Balance
      $('OutStandingBalanceText').innerHTML = doc.getElementById('OutStandingBalance').innerHTML;
   }
}

function showDataProtection() {
   $('data_protection').style.display = 'inline';
}

function CopyPrintData() {
   if ($('priceblock')) {
      $('pricepanelprint').innerHTML = $('divPricePanel').innerHTML;
      $('flightsblock').innerHTML = $('flights').innerHTML;
      $('accommodationblock').innerHTML = $('accommodation').innerHTML;
   }
}

function validatePromotionalCode() {
   var form = document.forms.PersonalDetailsAndPaymentFormBean;
   var val = TrimSpaces($F('promotionalCode'));// Trimspaces is in portal formvalidation.js
   var PromoPat = /^[-a-zA-Z0-9]*$/; // Check for allowed characters
   if (!PromoPat.test(val)) {
      return SetFocus('Please enter a valid Promotional Code. It accepts only Alphabets, Numerics and Hyphen(-)', $('promotionalCode'));
   }
   form.action = '/th/' + ProductType + '/book/ValidateCustomerPromotionalCode.do?promotionalCodeDetails.promotionalCode=' + val;
   form.submit();
}

// This function toggles the ALT tags required to validate the
// required fields in the select payment mode screen [Step2]
function checkOnlinePaymentMode(radio) {
   var paymentField = $('partPaymentAmount');
   if (radio.value == 'OTHER') {
      $('balancePaymentDiv').style.display = 'block';
      paymentField.disabled = false;
      paymentField.focus();
      paymentField.setAttribute('alt', 'Part payment amount|Y|DECIMAL');
   } else if (radio.value == 'FULL') {
      $('balancePaymentDiv').style.display = 'none';
      paymentField.setAttribute('alt', '');
   } else {
      paymentField.setAttribute('alt', '');
   }
}

// This function toggles the ALT tags required to validate the
// required fields in the select payment mode screen [Step2] onLoad
function enablePaymentText() {
   var form = document.forms.balancePaymentForm;
   var paymentField = $('partPaymentAmount');
   if (form.paymentMode[1].checked == true) {
      $('balancePaymentDiv').style.display = 'inline';
      form.amountPayable.disabled = false;
      paymentField.focus();
      paymentField.setAttribute('alt', 'Part payment amount|Y|DECIMAL');
   }
   if (form.paymentMode[0].checked == true) {
      paymentField.setAttribute('alt', '');
      paymentField.value = '';
   }
}

//This function validates value in the Part-Balance
// textfield in the Step2 of the Balance Payment.
function checkPartBalance(form) {
   if (form.paymentMode[1].checked == true) {
      var error;
      // get balance as a number: the replace() call removes commas, pound signs etc.
      var totalBalance  = +$F('totalBalance').replace(/[^-0-9.]/g, '');
      var paymentAmount = $F('partPaymentAmount');
      // check for pounds + optional decimal point and pence.
      var numberPattern = /^\d+(\.\d{1,2})?$/;
      if (!numberPattern.test(paymentAmount)) {
         error = 'Please enter a valid amount and only upto 2 decimal places';
      } else if (paymentAmount == 0) {
         error = 'Please enter an amount';
      } else if (paymentAmount > totalBalance) {
         error = 'The amount exceeds the total payment';
      }
      if (error) {
         return SetFocus(error, $('partPaymentAmount'));
      }
      ValidateForm(form);
   } else {
      form.submit();
   }
}

//This function shows and hides the DIV tags/table for the Error and
// the other alert messages in the first screen of Balance Payment
function showBookingRefTable(form) {
   var cbo = form.channel;
   var bookingReference    = $('bookingReference');
   var leadPassengerName   = $('leadPassengerName');
   for (var i = 0; i < cbo.length; i++) {
      if (cbo[i].checked && cbo[i].value == 'web') {
         bookingReference.disabled = leadPassengerName.disabled = false;
         bookingReference.style.backgroundColor = leadPassengerName.style.backgroundColor = 'white';
         bookingReference.setAttribute('alt', 'Valid Booking Reference Number|Y|NUMERIC');
         leadPassengerName.setAttribute('alt', 'Valid surname as entered during booking|Y|NAME');
         bookingReference.focus();
      }
      if (cbo[i].checked && cbo[i].value == 'shop') {
         alert('It is not possible to pay the balance of your booking online.\nPlease contact your local shop or our call centre on 0870 550 2590');
         bookingReference.disabled = leadPassengerName.disabled = true;
         bookingReference.style.backgroundColor = leadPassengerName.style.backgroundColor = '#d4d0c8';
         bookingReference.setAttribute('alt', '');
         leadPassengerName.setAttribute('alt', '');
      }
   }
}

// This function stops page from moving ahead when
// you select SHOP in the payment Radio Buttons
function checkPayment(form) {
   var cbo = form.channel;
   for (var i = 0; i < cbo.length; i++) {
      var radio = cbo[i];
      if (!(radio.checked && radio.value == 'web')) {
         return;
      } else {
         if (!$F('bookingReference') || !$F('leadPassengerName')) {
            return SetFocus('The details you have entered have not been recognised - please re-enter', $('bookingReference'));
         }
         ValidateForm(form)
      }
   }
}

// This function is used to check whether the passenger DOB
// is valid. This is used to catch errors when user enters
// a date which lies some time in the future.
function checkMinDOB(form,action) {
   var invalidDOB = true;
   for(var i=0;i<form.elements.length;i++) {
      if (form.elements[i].type=="select-one") {
         var elementName = form.elements[i].name;
         if (elementName.indexOf('day')>-1) {
            var passengerName = elementName.substring(0,elementName.indexOf('day'));
            var DOBDay = "";
            var DOBMonth = "";
            var DOBYear = "";
            for(var j=0;j<form.elements.length;j++) {
               var tempName = form.elements[j].name;
               if (tempName.indexOf(passengerName+'day')>-1) {
                  DOBDay = form.elements[j].value;
                  break;
               }
            }
            for(var j=0;j<form.elements.length;j++) {
               var tempName = form.elements[j].name;
               if (tempName.indexOf(passengerName+'month')>-1) {
                  DOBMonth = form.elements[j].value;
                  break;
               }
            }
            for(var j=0;j<form.elements.length;j++) {
               var tempName = form.elements[j].name;
               if (tempName.indexOf(passengerName+'year')>-1) {
                  DOBYear = form.elements[j].value;
                  break;
               }
            }
            if(DOBDay!='' || DOBMonth!='' || DOBYear!='') {//do validation only if child-dates entered
               var passengerDOB = DOBDay+"/"+DOBMonth+"/"+DOBYear;
               var Calendar = new Date();
               Calendar.setMonth(DOBMonth-1);
               Calendar.setYear(DOBYear);
               Calendar.setDate(DOBDay);
               if (DateValue(passengerDOB)>DateValue(minPassengerDOB) || (DOBDay!=Calendar.getDate())) {
                  return SetFocus('Please enter a valid date for child/infant', form.elements[i]);
                  invalidDOB = false;
                  break;
               }
            }
         }
      }
   }
   if (invalidDOB) {
      ValidateForm(action);
   }
}