var bookingmode = false;
var popupflag = false;

function editorial(actionname,topic,bookmark) {
var actionurl = clientDomainURL;

actionurl += '/st/' + actionname + '.do?'
params = '';
if ((topic) && (topic != '')) { params += '&topic=' + topic }
if ((bookmark) && (bookmark != '')) {	params += '#' + bookmark }
if (bookingmode) {
	Popup(actionurl + 'popup=true' + params,580,450,'scrollbars=1')
	} else {
	document.location.href = actionurl + 'popup=' + popupflag + params
	}
}

function editorialSimply(actionname,actionurl) {

	actionurl += '/st/' + actionname + '.do?'
	params = '';

	if (bookingmode) {
		Popup(actionurl + 'popup=true' + params,580,450,'scrollbars=1')
		} else {
		document.location.href = actionurl + 'popup=' + popupflag + params
		}
	}

function backButtonHandle(url) {
    var actionurl = clientDomainURL+'/st/'+ url + '.do?';
    document.location.href = actionurl;
	}

function popChooser(popUpURL) {
	popUpURL=clientDomainURL+popUpURL;
    if(bookingmode) {
      window.open(popUpURL,'_blank'); 
    } else {
      document.location.href=popUpURL; 
    }
}

// Set Booking Mode
function BookingMode() {
 bookingmode = true
}
