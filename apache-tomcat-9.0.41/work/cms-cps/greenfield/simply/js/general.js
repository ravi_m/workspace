var ns4 = (document.layers);
var ie4 = (document.all && !document.getElementById);
var ie5 = (document.all && document.getElementById);
var ns6 = (!document.all && document.getElementById);
var mac = (navigator.userAgent.indexOf("Mac") != -1);
var moz = (navigator.userAgent.indexOf("Netscape") == -1);
var popupWin = '';

function getElem(elemname) {
   // Explorer 4
   if(ie4){
      return document.all[elemname]
   }
   // W3C - Explorer 5+ and Netscape 6+
   else if(ie5 || ns6){
      return document.getElementById(elemname)
   }
}

function getElemName(elemname) {
 return document.getElementsByName(elemname)
}

/* compatability section */

// Array.prototype.push (IE5)
if (![].push) {
  Array.prototype.push = function() {
    for (var i = 0; i < arguments.length; i++) this[this.length] = arguments[i];
    return this.length;
  }
}

// Function.prototype.apply (IE5)
if (!Function.apply) Function.prototype.apply = function(o, a) {
  o = (o == null || typeof o == 'undefined') ? window : Object(o);
  a = a || [];
  var params = []; for ( var i = 0; i < a.length; i++ ) { params[i] = 'a[' + i + ']'; }
  o._fn = this;
  var rv, _throw = null;
  try { rv = eval('o._fn(' + params + ')') } catch (e) { _throw = e }
  try { delete o._fn } catch (e) { o._fn = null }
  if (_throw) throw _throw;
  return rv;
}

// String.prototype.replace(.., function) (IE5 & Safari)
if (''.replace(/^/, String)) (
  function(_replace, _global) {
    String.prototype.replace = function(sr,fn) {
      function global(s) { return s.indexOf('g', s.lastIndexOf('/')) > 0 }
      if (typeof fn != 'function') { return _replace.apply(this, arguments); }
      if (!(sr instanceof RegExp)) {
        var i = this.indexOf(sr);
        return (i < 0) ? this : _replace.apply(this, [sr, fn(sr, i, this)]);
      }
      var m, ns = '', s = this, e;
      if (_global ? sr.global : global(String(sr))) {
        while (m = sr.exec(s)) {
          ns += s.slice(0, m.index) + fn.apply(fn, m);
          if (s.length == 0) break;
          e = m.index + m[0].length;
          if (e == 0) { ns += s.charAt(0) }
          s = s.slice(e || 1);
        }
        return ns + s;
      } else {
        m = sr.exec(s);
        return s.slice(0, m.index) + fn.apply(fn, m) + s.slice(m.index + (m[0].length));
      }
    }
  }
)(String.prototype.replace, /^/g.global);

/* core section */
function $() {
  function $(a) {
    var r = [];
    for (var i = 0; i < a.length;) {
      var e = a[i++];
      r.push((typeof e == 'string') ? document.getElementById(e) : (e instanceof Array) ? $(e) : e);
    }
    return r;
  }
  var r = $(arguments);
  return r.length == 1 ? r[0] : r;
}

function $A(a) {
  var r = []; for (var i = 0; i < a.length;) { r.push(a[i++]); } return r;
}

function $F() {
  function $F(a) {
    var r = [];
    for (var i = 0; i < a.length;) {
      var e = $(a[i++]);
      r.push(e ? ((e instanceof Array) ? $F(e) : e.value) : e);
  }
    return r;
  }
  var r =  $F($A(arguments));
  return r.length == 1 ? r[0] : r;
}

function $N() {
  function $N(a) {
    var r = [];
    for (var i = 0; i < a.length;) {
      r.push($A(document.getElementsByName(a[i++])));
    }
    return r;
  }
  var r =  $N($A(arguments));
  return r.length == 1 ? r[0] : r;
}

// Date-handling
(function(o) {
  o = o || window;
  if (o.Dates) return;
  o.Dates = {
    monthsPerYear: 12,
    hoursPerDay: 24,
    minutesPerDay: 24 * 60,
    millisPerDay: 24 * 60 * 60 * 1000,
    millisPerHour: 60 * 60 * 1000,
    millisPerMinute: 60 * 1000,
    millisPerSecond: 1000,
    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    fromISO: function(s) {
      /* y-m-d h:m:s format (iso-8601) */
      var a = s.split(' ');
      var ymd = a[0].split('-');
      var y = +ymd[0];
      var mon = ymd.length > 1 ? +ymd[1] : 1;
      var d = ymd.length > 2 ? +ymd[2] : 1;
      if (a.length == 1) {
        return new Date(y, mon - 1, d);
      } else {
        var hms = a[1].split(':');
        var h = +hms[0];
        var min = hms.length > 1 ? +hms[1] : 0;
        var sec = hms.length > 2 ? +hms[2] : 0;
        return new Date(y, mon - 1, d, h, min, sec);
      }
    },
    fromSlashed: function(s) {
      /* d/m/y format */
      var a = s.split('/');
      var y = +a[2];
      var m = +a[1];
      var d = +a[0];
      return (a.length == 3) ? new Date(y < 1900 ? y + 1900 : y, m - 1, d) : null;
    },
    date: function(o) {
      var date = null;
      if (o instanceof Date) { date = o }
      else if (typeof o == 'number') { date = new Date(o) }
      else if (typeof o == 'string') {
        if (o.indexOf('/') > 0) {
          date = this.fromSlashed(o);
        } else {
          date = this.fromISO(o);
        }
      }
      return date;
    },
    ord: function(date, precision) {
      date = this.date(date);
      precision = precision || 3;
      var tz = date.getTimezoneOffset() * this.millisPerMinute;
      switch (precision) {
        case 1: return date.getFullYear();
        case 2: return date.getFullYear() * this.monthsPerYear + date.getMonth();
        case 3: return Math.floor((date.getTime() - tz) / this.millisPerDay);;
        case 4: return Math.floor((date.getTime() - tz) / this.millisPerHour);
        case 5: return Math.floor((date.getTime() - tz) / this.millisPerMinute);
        case 6: return Math.floor((date.getTime() - tz) / this.millisPerSecond);
        default: return date.getTime();
      }
    },
    diff: function(from, to, precision) {
      return this.ord(to, precision) - this.ord(from, precision);
    },
    millis: function(o) {
      return this.date(o).getTime();
    },
    day: function(o) {
      return this.ord(o);
    },
    dayName: function(o, length) {
      return this.days[this.date(o).getDay()].substr(0, length || 3);
    },
    monthName: function(o, length) {
      return this.months[this.date(o).getMonth()].substr(0, length || 3);
    },
    monthStart: function(date) {
      date = this.date(date);
      return new Date(date.getFullYear(), date.getMonth(), 1);
    },
    monthEnd: function(date) {
      date = this.date(date);
      return new Date(date.getFullYear(), date.getMonth() + 1, 0);
    },
    iso: function(date, fields) {
      function nn(n) {
        return (n < 0 || n > 9) ? String(n) : '0' + String(n);
      }
      fields = fields || 3;
      date = this.date(date);
      var s = nn(date.getFullYear());
      if (--fields > 0) s += '-' + nn(date.getMonth() + 1);
      if (--fields > 0) s += '-' + nn(date.getDate());
      if (--fields > 0) s += ' ' + nn(date.getHours());
      if (--fields > 0) s += ':' + nn(date.getMinutes());
      if (--fields > 0) s += ':' + nn(date.getSeconds());
      return s;
    },
    inRange: function(date, start, end) {
      return this.day(date) < this.day(start) ? -1 :
        this.day(date) > this.day(end) ? 1 :
        0;
    }
  };
  Date.prototype.before = function(that, precision) { return Dates.diff(this, that, precision) > 0 }
  Date.prototype.after  = function(that, precision) { return Dates.diff(this, that, precision) < 0 }
  Date.prototype.within  = function(start, end) { return Dates.inRange(this, start, end) == 0 }
  String.prototype.before = function(that, precision) { return Dates.date(String(this)).before(that, precision) }
  String.prototype.after  = function(that, precision) { return Dates.date(String(this)).after(that, precision) }
  String.prototype.within  = function(start, end) { return Dates.inRange(String(this), start, end) == 0 }
})(window['TUILibrary'])




function popup_flight_schedule(params) {
   Popup('/st/sun/viewFlightSchedule.do?'+params,600,350, 'scrollbars=yes')
}

function ShowWeather(dest_code) {
   Popup('/st/sun/viewWeather.do?'+dest_code,600,570,'scrollbars=yes')
}

function Popup(popURL,popW,popH,attr){
  if (!popH) { popH = 350 }
  if (!popW) { popW = 600 }
   attr = attr + ",resizable=yes";
  var winLeft = (screen.width-popW)/2;
  var winTop = (screen.height-popH-30)/2;
  var winProp='width='+popW+',height='+popH+',left='+(+winLeft)+',top='+winTop+','+attr;
  popupWin = window.open(popURL,'popupWindow',winProp)
  if (popupWin) {  popupWin.window.focus()
  } else {
    // There's an active popup-blocker!
    // The browser will indicate a popup was blocked and the user may to take remedial action to view it.
  }
}

function pagecall(url1,params,bkmark) {
 newurl =  url1 + '?p=' + popup
 if (params) { if (params != '') { newurl += '&' + params } }
 if (bkmark) { if (bkmark != '') { newurl += '#' + bkmark } }
 document.location.href = newurl
}

function FormFocus() {
   document.forms[0].elements[0].focus()
}

function PopupCheck(urlforward) {
if ((window.opener) && (window.name == 'popupWindow')) {
   window.opener.location.href = urlforward
   window.close()
   }
}

function windowResize() {
if (ns6) {
currentHeight = window.innerHeight
} else {
 currentHeight = document.body.clientHeight
}
newHeight = getElem('endcontent').offsetTop + 100
if (newHeight > currentHeight) {
  if (newHeight > 450) newHeight=450 // Set Limit
if(ns6) {
  window.resizeTo(window.innerWidth+26,newHeight)
  } else if(ie4 || ie5) {
   window.resizeTo(document.body.clientWidth+26,newHeight)
  }
}
}

/* Cache Footer Images */
atoloff = new Image();
atolon = new Image();
abtaoff = new Image();
abtaon = new Image();
iataoff = new Image();
iataon = new Image();
verisignoff = new Image();
verisignon = new Image();

atoloff.src ="/cms-cps/greenfield/simply/images/footer/atol_logo_off.gif";
atolon.src ="/cms-cps/greenfield/simply/images/footer/atol_logo_on.gif";
abtaoff.src ="/cms-cps/greenfield/simply/images/footer/abta_logo_off.gif";
abtaon.src ="/cms-cps/greenfield/simply/images/footer/abta_logo_on.gif";
iataoff.src ="/cms-cps/greenfield/simply/images/footer/iata_logo_off.gif";
iataon.src ="/cms-cps/greenfield/simply/images/footer/iata_logo_on.gif";
verisignoff.src ="/cms-cps/greenfield/simply/images/footer/veri_sign_off.gif";
verisignon.src ="/cms-cps/greenfield/simply/images/footer/veri_sign_on.gif";

function imgOn(objid)
{
   getElem('footer_' + objid).src = eval(objid + "on.src");
}

function imgOff(objid)
{
   getElem('footer_' + objid).src = eval(objid + "off.src");
}

function linkcheck(lnkin) {
  document.location.href = clientDomainURL + lnkin
}

function AlertErrorsWarnings() {
if (getElem('errorwarnings')) {
   alert(getElem('errorwarnings').innerHTML)
  }
}

/* This function is to strip HTML tags from the Alert Message for Errors and Warnings */
function AlertErrorsTextOnly() {
  if ($('pricediff1')) {
    alert(String($('pricediff1').innerHTML).replace(/<\/?[^>]+>/gi,''));
  }
}

function addOnload(fn) {
 if (window.addEventListener) {
  window.addEventListener('load', fn, false)
 } else {
  window.attachEvent('onload', fn);
 }
}

/* old-names section */
var getElem = $;
var getRef = $;
var getElemName = $N;
var DateValue = function(string) { try { return Dates.millis(string) } catch (e) { return 0 } };
