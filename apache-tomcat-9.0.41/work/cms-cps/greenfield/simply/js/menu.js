var hideTag = Object;
var showTag = Object;
var underlay = Object;

if (document.documentElement.currentStyle) {
  if (document.documentElement.currentStyle.writingMode) {
    underlay = function(overlay) {
      if (!overlay.underlay) {
        overlay.insertAdjacentHTML('afterEnd', '<iframe ' +
       'style="display: none; position: absolute; filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);" ' +
       'src="javascript:\'\';" frameborder="0" scrolling="no"></iframe>');
        overlay.underlay = overlay.nextSibling;
      }
      resizeUnderlay(overlay);
    }
  } else {
    hideTag = function(tagName) { hideForIE5(tagName, false) }
    showTag = function(tagName) { hideForIE5(tagName, true)  }
  }
}
function hideForIE5(tagName, show) {
  var tags = document.all.tags(tagName);
  for (var i = 0; i < tags.length; i++) {
    var tag = tags[i];
    if (!tag || !tag.offsetParent) continue;

    // Find the element's offsetTop and offsetLeft relative to the BODY tag.
    var left   = tag.offsetLeft;
    var top    = tag.offsetTop;
    var ancestor = tag.offsetParent;

    while (ancestor.tagName.toUpperCase() != 'BODY') {
      left += ancestor.offsetLeft;
      top  += ancestor.offsetTop;
      ancestor = ancestor.offsetParent;
    }

    // Check if tag could be obscured by dropdown menu
    if (left > 278 && top < 370) {
      tag.style.visibility = !show ? 'hidden' : 'visible';
    }
  }
}

function hideUnderlay(overlay) {
  if (overlay && overlay.underlay) {
    overlay.underlay.style.display = 'none';
  }
}

function resizeUnderlay(overlay) {
  function offset(element) {
    var top = 0, left = 0;
    do {
      top  += element.offsetTop  || 0;
      left += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);
    return { left: left, top: top};
  }
  var underlay = overlay.underlay;
  if (underlay) {
    var offsets = offset(overlay);
    underlay.style.top    = offsets.top + 'px';
    underlay.style.left   = offsets.left + 'px';
    underlay.style.width  = overlay.offsetWidth + 'px';
    underlay.style.height = overlay.offsetHeight + 'px';
    underlay.style.zIndex = overlay.currentStyle.zIndex - 1;
    underlay.style.display = '';
  }
}


// Menu Code
function menu_show(which) {
  move_menus();
  // when showing global nav, hide calendar
  if (document.getElementById('divcalendar')) { HideLayer() }
  hideTag('SELECT');
  underlay(document.getElementById(which));
  document.getElementById(which + 'tab').style.color = '#FFFF66';
  document.getElementById(which).style.visibility = 'visible';
}

function menu_hide(which) {
  document.getElementById(which + 'tab').style.color = '#FFFFFF';
  document.getElementById(which).style.visibility = 'hidden';
  hideUnderlay(document.getElementById(which));
  showTag('SELECT');
}

function move_menus() {
  if (ns6 && moz) {
    getElem('menuholidays').style.top =
    getElem('menuessentials').style.top =
    getElem('menuspecial').style.top =
    getElem('menulate').style.top = '64px';
  }
}

function menu_select(url) {
  if (url == 'TBA') {
    alert('T.B.A.')
  } else {
    if (/(LateDeals|Special)/.test(url)) {
      // capture the previous url for late deals
      url += (url.indexOf('?') < 0) ? '?' : '&';
      var s = document.location.href;
      var i = s.indexOf('?');
      url += 'previousPage=' + (i < 0 ? s : s.substr(0, i));
    }
    document.location.href = url;
  }
}
