
$j(document).ready(function(){
  // stylesheets to be loaded only when javascript is enabled
  loadStylesheet("/cms-cps/tflyredesign/css/style_js.css", "screen, print");

  toggleOverlay();
  toggleFields("select#payment_type_0", ".maestroGroup", "Switch");
  targetBlank();
  submitAction();

});

function toggleFields(owner, toggleField, testStr){

  $j(owner).change(function(){
  toggleGroup = $j(toggleField);
  var chosenoption= $j(owner+' option:selected');


    for (var i=0; i<toggleGroup.length; i++)
    {
      if(chosenoption.val() == testStr){
        $j(toggleGroup[i]).show();
      }else{
        $j(toggleGroup[i]).hide();
      }
    }
    return false;
  });

}

function loadStylesheet( filename, media ){
  // ensure that the browser has all the DOM methods/properties we need
  if( !(document.getElementById) ||
    !(document.childNodes) ||
    !(document.createElement) ||
    !(document.getElementsByTagName) ){
    return;
  }

  // make a new link node to the stylesheet
  var link = document.createElement( "link" );
  link.href = filename;
  link.rel = "stylesheet";
  link.type = "text/css";
  link.media = media;


  // insert the stylesheet link into the document head
  document.getElementsByTagName('head')[0].appendChild(link);
}



function toggleOverlay(){
  var overlayZIndex = 99;
  var zIndex = 100;

  $j("a.stickyOwner").click(function(e){

    var overlay = "#" + this.id + "Overlay";

    $j(overlay).show();
    $j(overlay + ".genericOverlay").css("z-index",zIndex);
    zIndex++;

    if ($j(overlay).parent(".overlay") != null){
      $j(overlay).parent(".overlay").css("z-index",overlayZIndex);
      overlayZIndex++;
    }
    return false;
  });

  $j("a.close").click(function(){
    var overlay = this.id.replace("Close","Overlay");
    $j("#" + overlay).hide();
    return false;
  });
}


//This method allows generic detection of all pop-up links
function targetBlank() {
  if (!document.getElementsByTagName) return;

  var anchors = $j("a.popUplink");
  for (var i=0; i<anchors.length; i++)
  {
    var anchor = anchors[i];
    if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external")
    {

      var theScript = 'JavaScript:void(openWindow("' + anchor.getAttribute('href') + '"));';

      anchor.setAttribute("href", theScript);

      // We don't want the target attribute as this will cause problems with the script.
      anchor.removeAttribute("target");
    }
  }
}

  $j(".genericOverlay").hide();


function openWindow(href) {
    var win = window.open(href,"","width=700,height=600,scrollbars=yes,resizable=yes");
    if (win && win.focus) win.focus();
}

function submitAction(){
  //validate the form for submisson

 /* $j("form#SkySales").submit(function(){

     var patternNumeric=/(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;

     var validationMessageStr = "Please correct the following:\n\n";
     var isRequiredText = " is required.";
     var isValidText = " is invalid.";
     var errorCount = 0;


     if($j("select#payment_type_0 option:selected").val().length < 1){
     	validationMessageStr += getLabel($j("#payment_type_0")) +isRequiredText+ "\n";
     	errorCount += 1;
     }


     if($j("#payment_0_nameOnCard").val().length < 1){
        validationMessageStr += getLabel($j("#payment_0_nameOnCard")) +isRequiredText+ "\n";
        errorCount += 1;
     }


     if($j("#payment_cardNumber").val().length < 1){
        validationMessageStr += getLabel($j("#payment_cardNumber")) +isRequiredText+ "\n";
        errorCount += 1;
     }


     var cardNumberValue = $j("#payment_cardNumber").val();
     if($j("#payment_cardNumber").val().length > 0){
	if(!patternNumeric.test(cardNumberValue)){
	   validationMessageStr += getLabel($j("#payment_cardNumber")) +isValidText+ "\n";
	   errorCount += 1;
	}
     }

     if($j("select#payment_expiryDateMonth option:selected").val().length < 1 || $j("select#payment_expiryDateYear option:selected").val().length < 1){
        validationMessageStr += getLabel($j("#payment_expiryDateMonth")) +isRequiredText+ "\n";
        errorCount += 1;
     }

     var cvvNumber = $j("#payment_0_securityCode").val();
     if($j("#payment_0_securityCode").val().length < 1){
        validationMessageStr += getLabel($j("#payment_0_securityCode")) +isRequiredText+ "\n";
        errorCount += 1;
     }

     //format for American express
     if(($j("#payment_0_securityCode").val().length > 0) && ($j("select#payment_type_0 option:selected").val() == "American")){
        if(($j("#payment_0_securityCode").val().length != 4) || (!patternNumeric.test(cvvNumber))){
           validationMessageStr += getLabel($j("#payment_0_securityCode")) +isValidText+ "\n";
           errorCount += 1;
        }
     }

     //format for non-American express
     if(($j("#payment_0_securityCode").val().length > 0) && ($j("select#payment_type_0 option:selected").val() != "American")){
        if(($j("#payment_0_securityCode").val().length != 3) || (!patternNumeric.test(cvvNumber))){
           validationMessageStr += getLabel($j("#payment_0_securityCode")) +isValidText+ "\n";
           errorCount += 1;
        }
     }

     var issueNumber = $j("#payment_0_issueNumber").val();
     if($j("select#payment_type_0 option:selected").val() == "Switch"){
        if(($j("#payment_0_issueNumber").val().length < 1) || (!patternNumeric.test(issueNumber))){
     	   validationMessageStr += getLabel($j("#payment_0_issueNumber")) +isRequiredText+ "\n";
     	   errorCount += 1;
     	}
     }

     if($j('input#termAndCondition:checkbox:checked').val() != "on"){
     	validationMessageStr += getLabel($j("#termAndCondition")) +isRequiredText+ "\n";
        errorCount += 1;
     }

     if (errorCount > 0){
     	alert(validationMessageStr);
     }else{
        alert("return true")
     }

    return false;
  });
*/

}


/*
function getLabel(field){

   var labels = $j("label");

   for (var i=0; i<labels.length; i++)
   {
    if (field.attr("id") == $j(labels[i]).attr("for")){
       return labels[i].innerHTML;
    }

  }

}*/
