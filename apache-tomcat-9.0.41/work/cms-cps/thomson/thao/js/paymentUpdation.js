/**
 * Contains field names which has to be set to default when page loads.
 *
 */
function setToDefault()
{
  //Set to default
  $('payment_type_0').selectedIndex = 0;

  $('payment_0_expiryMonthId').selectedIndex = 0;
  $('payment_0_expiryYear').selectedIndex = 0;

  $('payment_0_cardNumberId').value = "";
  $('payment_0_nameOnCardId').value = "";

  $('payment_0_securityCodeId').value = "";
  //$('payment_0_postCodeId').value = "";


  $('IssueNumberInput').value = "";


}

/* Returns DispCardText(which is displayed under security code caption)  based on selected payment method
*  @params selPaymentMethod - selected payment method i.e-  AMERICAN_EXPRESS, VISA etc
*              securityCodeLength  - Security code associated with this payment method
*/
function checkForDispCardText(selPaymentMethod, securityCodeLength )
{
   var dispCardText = '';
   if(selPaymentMethod.indexOf('AMERICAN_EXPRESS') ==0)
   {
      dispCardText = "the "+securityCodeLength+" digit code above the card number on the front of your card";
   }
   else
   {
      dispCardText = "last "+securityCodeLength+" digits in the signature strip on the reverse of your card";
   }
   return dispCardText;
}

/**
 * This function updates the essential field used for validation.
 */
function updateEssentialFields()
{
   $("total_transamt").value = PaymentInfo.calculatedPayableAmount;
   var sel = document.getElementById('CDcountryName');
   document.getElementById('payment_0_selectedCountry').value = sel.options[sel.selectedIndex].value;

}

/******************************************NON-AJAX IMPLEMENTATION***********************/
/***
 * This function is invoked from common function "handleCardSelection(index)"
 * Include function which are responsible for updating payment page
 */
function displayFieldsWithValuesForCard()
{
   /*displayCardRelatedFields();
   displayTheTotalAmounts(bookingConstants.TOTAL_PRICE);
   //displayTheTotalHoplaAmounts(bookingConstants.HOPLA_TOTAL_PRICE);
   //Following function is an existing brand specific function for displaying card charges in summary panel.
   updateCardChargesInSummaryPanel(PaymentInfo.totalCardCharge);
   */
   //New implementation
    displayCardRelatedFields();
    displayTheAmounts(bookingConstants.TOTAL_PRICE,PaymentInfo.calculatedTotalAmount );
    //Add card charges to hopla total amount and display hopla total amount.
    displayTheAmounts(bookingConstants.HOPLA_TOTAL_PRICE, 1*PaymentInfo.hoplaTotalAmount+ 1*PaymentInfo.totalCardCharge);
   //Following function is an existing brand specific function for displaying card charges in summary panel.
   updateCardChargesInSummaryPanel(PaymentInfo.totalCardCharge);

}


 /** Function for updating total amount based on the
 ** style class applied to it.
**/
function displayTheAmounts(amountClassName, amount)
{
   var currency = getCurrency();

   //Display the total amounts with calculated total amount.
   var amts = $$('.'+amountClassName);
   for(var i=0; i<amts.length; i++)
   {
   	 amts[i].innerHTML = currency+roundOff(amount,2);
   }
}
/** Function for updating total amount based on the
 ** style class applied to it.
**/
function displayTheTotalAmounts(totalAmountClassName)
{
   var currency = getCurrency();

   //Display the total amounts with calculated total amount.
   var totAmts = $$('.'+totalAmountClassName);
   for(var i=0; i<totAmts.length; i++)
   {
   	 totAmts[i].innerHTML = currency+PaymentInfo.calculatedTotalAmount;
   }
}

/** This function is responsible for updating card charges in summary panel
*   @params cardCharge -represents cardChargeAmt to be displayed for the transaction
*/
function updateCardChargesInSummaryPanel(cardCharge)
{
    if($('cardChargeText'))
     {
         if(cardCharge > 0)
        {
           $('cardChargeAmount').innerHTML="&pound;"+parseFloat(1*cardCharge).toFixed(2);
           $('cardChargeText').style.display='block';
           $('cardChargeAmount').style.display='block';
        }
        else
        {
           $('cardChargeText').style.display='none';
           $('cardChargeAmount').style.display='none';
         }
    }
}

/** This method is called on change of the select box. */
function displayCardRelatedFields()
{
   if(PaymentInfo.selectedCardType == null)
   {
      $("debitcards0").style.display = 'none';
      return;
   }
   if($(PaymentInfo.selectedCardType+'_issueNo').value == 'true')
   {
      $("debitcards0").style.display = 'block';
   }
   else
   {
     $("debitcards0").style.display = 'none';
   }
   var securityNumLen = $(PaymentInfo.selectedCardType+'_securityCodeLength').value;
   $('securityCodeHelp0').innerHTML = "(last "+securityNumLen+" digits in the signature strip on the reverse of your card)";
}

function getCurrency()
{
  var currency = $("currencyText");

  if( currency )
  {
    currency = currency.value;
  }

  return currency;
}


/**This function is to change the pay button label according to the card selected.*/
function changePayButton()
{
   if($("confirmButton"))
   {
      var payButtonDescription = threeDCards.get(PaymentInfo.selectedCardType);
      if(payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup" || payButtonDescription == "americanexpressgroup")
      {
         $("confirmButton").innerHTML = '<input type="image" src="/cms-cps/thomson/thao/images/buttons/form/proceed-to-payment-btn.gif" alt="Proceed to Payment" title="Proceed to Payment"/>'
      }
      else
      {
         $("confirmButton").innerHTML ='<input type="image" src="/cms-cps/thomson/thao/images/buttons/form/pay.gif" alt="Pay" title="Pay"/>';
      }
   }
}

function toggleTHAOOverlay()
{
  var overlayZIndex = 99;
  var zIndex = 100;
  var prevOverlay;
  var stickyOpened = false;
  jQuery("a.stickyOwner").click(function(e){
    var overlay = "#" + this.id + "Overlay";
	if (!stickyOpened)
	{
		prevOverlay = overlay;
	}
	if (prevOverlay != overlay)
	{
		jQuery(prevOverlay).hide();
		stickyOpened = false;
	}
	var pos = jQuery("#"+this.id).offset();
	var left = parseInt((1*pos.left-75),10);
	var top = parseInt((1*pos.top+50),10);
	jQuery(overlay).show();
	jQuery(overlay).css("left",left);
	jQuery(overlay).css("top",top);

	jQuery("#masterCardDetailsClose").html("&nbsp;");
	jQuery("#visaDetailsClose").html("&nbsp;");

	prevOverlay = overlay;
	stickyOpened = true;
	jQuery(overlay + ".genericOverlay").css("z-index",zIndex);
	zIndex++;

	if (jQuery(overlay).parent(".overlay") != null){
	  jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
	  overlayZIndex++;
	}
    return false;
  });

  jQuery("a.close").click(function(){
    var overlay = this.id.replace("Close","Overlay");
    jQuery("#" + overlay).hide();
    return false;
  });
}
