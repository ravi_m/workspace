// Validate form for submission
function ValidateForm(formobj)
{
   // Start of For loop
   for (i=0;i<formobj.elements.length;i++)
   {
      obj = formobj.elements[i]
      if(obj.name == 'importantinfo')
      {
         if ((obj.type == 'checkbox') && (!obj.checked) )
         {
            return setFocus('Please read and accept the important additional information before continuing with your booking',obj)
         }
      }
      if(obj.name == 'importantInformationChecked')
      {
         if ((obj.type == 'checkbox') && (!obj.checked) )
         {
            return setFocus('Please read and accept the important additional information before continuing with your booking',obj)
         }
      }
      if(trimSpaces(obj.value) == 'select') {
         return setFocus('Please select the nature of your e-mail.',obj)
      }
      if(obj.name == 'payment_0_type')
      {
         if(obj.options.selectedIndex==0)
         {
            return setFocus("Your card type is required to complete your booking.Please select your card type from the dropdown.",obj);
         }
      }

      //validating whether proper expiry date is selected.
      if(obj.name == 'payment_0_expiryMonth' || obj.name == 'payment_0_expiryYear')
      {
         if(obj.options.selectedIndex==0){
            return setFocus("Please select expiry date for your card",obj);
         }
         else
         {
            //cardmonth = obj.options[obj.options.selectedIndex].value;
            cardmonth = $("payment_0_expiryMonthId").options[$("payment_0_expiryMonthId").options.selectedIndex].value;
            cardyear = $("ExpiryYear").options[$("ExpiryYear").options.selectedIndex].value;
            var Calendar=new Date();
            todaysmonth =Calendar.getMonth()+1;
            var calib=(ns4 || ns6)?1900:0;
            todaysyear = Calendar.getYear()+calib;
            cardmonth = parseInt(cardmonth,10);
            cardyear = parseInt(cardyear,10);
            todaysyear=todaysyear+'';
            todaysyear=todaysyear.substring(2);
            if (((cardmonth<todaysmonth) && (cardyear == todaysyear)) || (cardyear<todaysyear)){
               return setFocus('You have entered an invalid Expiry Date. Please check and try again.',obj);
            }
         }
      }

      // Identify Validation required - check for disabled
      if (obj.getAttribute("alt"))
      {
         disabledlayer = false
         obj1 = obj

         // Find if obj is on a DIV
         while (obj1.tagName != 'BODY')
         {
            if (obj1.tagName == 'DIV')
            {
               if (obj1.style.display == 'none')
               {
                  disabledlayer = true
                  break;
               }
            }
            obj1 = obj1.parentNode
         }
         // Start of ALT
         if ((obj.getAttribute("alt") != '') && (!disabledlayer))
         {
            valset = obj.getAttribute("alt").split("|")
            obj.value = trimSpaces(obj.value)
            obj_value = obj.value
            obj_name = valset[0]
            if (valset[1] == 'Y')
            {
               obj_mandatory = true
            }
            else {
            obj_mandatory = false
            }
            obj_type = valset[2]
            obj_params = valset.slice(3)
            if (obj_mandatory)
            {
               if ((obj_value == '') && ((obj.type == 'text') || (obj.type == 'textarea') || (obj.type == 'password'))) return setFocus(obj_name, obj)
               if ((!obj.checked) && (obj.type == 'checkbox')) return setFocus(obj_name,obj)

               // Check selection has ben made on selection boxes
               if (obj.type.substr(0,6) == 'select')
               {
                  if (obj.options.selectedIndex==0)
                  {
                     return setFocus(obj_name, obj)
                  }
               }
            }

            // Validation Type
            if (obj_type)
            {
         switch (obj_type.toUpperCase())
         {
             // Alphabhetic 0-9 a-z A-Z Space Hyphen / \
            case 'ALPHA':
                    var Pat1 = /^[a-zA-Z -]*$/;
                    if (!Pat1.test(obj_value)) { return setFocus('Please enter valid details' ,obj) }
                    break;

            case 'ALPHANUMERIC':
                    var Pat1 = /^[-a-zA-Z0-9 \\\/.,]*$/;
                    if (!Pat1.test(obj_value)) { return setFocus('Please enter valid details',obj) }
                    break;

            // Alphanumeric with Extended characters
            case 'ANEXTENDED':
                    var Pat1 = /^[-a-zA-Z0-9 \\\/.,\x27!;@:"�\$%\^&\*()]*$/;
                    if (!Pat1.test(obj_value)) { return setFocus('Please enter valid details',obj) }
                    break;

            // Alphanumeric with Extended characters for Password
            case 'PASSWORD':
                    var Pat1 = /^[-a-zA-Z0-9 \\\/.,\x27!;@:"#�\$%\^&\*()]*$/;
                    if (!Pat1.test(obj_value)) { return setFocus('Please enter valid details',obj) }
                    break;

            // Insurer Name
            case 'INSURERDETAILS':
                    var Pat1 = /^[^&]*$/;
                    if (!Pat1.test(obj_value)) { return setFocus('Please enter valid details.\n(Invalid characters are &)',obj) }
                    break;

             // Numeric 0-9 NUMERIC|Min|Max
            case 'NUMERIC':
                    var Pat1 = /^[0-9]*$/;
                    if (!Pat1.test(obj_value)) {
                       if(valset[3]=='olbp') return setFocus('The details you have entered have not been recognised - please re-enter',obj);
                       else return setFocus('Please enter a valid number',obj);
                       }
                    // Perform Range Checking
                    if (obj_params != '') {
                     obj_value = parseFloat(obj_value)
                     // Minimum
                     if (obj_params[0]) {
                        if (obj_value < obj_params[0]) { return setFocus('Please enter a valid number',obj) }
                     }
                     // Maximum
                     if (obj_params[1]) {
                        if (obj_value > obj_params[1]) { return setFocus('Please enter a valid number',obj) }
                     }
                    }
                    break;

            // Email Address xxx@xxx.xxx
            case 'EMAIL':
                  obj_value = obj_value.toLowerCase();
                    obj.value = obj_value;
                    Pat1 = /^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/;
                  if (!Pat1.test(obj_value)) {
                          return setFocus( 'Please enter a valid email address \n\ne.g. name@place.com', obj)
                  }
                    break;

            // Address Fields
            case 'ADDRESS':
                    var Pat1 = /^[-a-zA-Z0-9 ,\.\x27]*$/;
                    apos = String.fromCharCode(39)
                    if (!Pat1.test(obj_value)) { return setFocus('Please enter a valid address.', obj) }
                    break;

            // Alphabetic address a-z A-Z Space Hyphen / \
            case 'ALPHAADDRESS':
                    var Pat1 = /^[a-zA-Z -]*$/;
                    if (!Pat1.test(obj_value)) { return setFocus('Please enter a valid address.' ,obj) }
                    break;

            // Name Fields
            case 'NAME':
                    var Pat1 = /^[-a-zA-Z\u00C0-\u00DD\u00E0-\u00FF \.\'\x27]*$/;
                    apos = String.fromCharCode(39)
                    if (!Pat1.test(obj_value)) {
                       if(valset[3]=='olbp') return setFocus('The details you have entered have not been recognised - please re-enter',obj);
                       else
                       {
                         return setFocus('Please enter a valid name',obj);
                       }
                     }
                     if (!Pat1.test(obj_value)||(stripChars(obj_value,"-.' ").length == 0 && valset[1] == 'Y')) {
                           return setFocus('Please enter valid details' ,obj) }
                     break;

            case 'REVIEWNAME':
                    var Pat1 = /^[-a-zA-Z&@ \"\',\.\x27]*$/;
                    apos = String.fromCharCode(39)
                    if (!Pat1.test(obj_value)) { return setFocus('Please enter a valid name.' , obj) }
                    break;

            // UK Postcode XX1 1XX
            case 'POSTCODE':
                       obj_value = obj_value.toUpperCase();
                  obj.value = obj_value;
                       Pat1 = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
                       if (!Pat1.test(obj_value)) {
                          return setFocus('We cannot validate the post code you have entered. Please check and re-enter. ',obj)
                   }
                    break;

            // Phone number
            case 'PHONE':
                  obj.value = removeAllSpaces(obj.value)
                    obj_value = obj.value
                    var val = /(^-[0-9]+)|(^[0-9]*-$)|(^-[0-9]*-$)/;
                    if(val.test(obj_value)){
                       return setFocus('Please enter a valid telephone number.', obj)
                    }
                    else {
                       obj_value=stripChars(obj_value,"()- ");
                       Pat1 = /^[0-9 ]*$/;
                       if ((!Pat1.test(obj_value)) && (obj_value != '')) {
                          return setFocus('Please enter a valid telephone number.', obj)
                       }
                    }
                    obj.value = obj_value;
                    break;

            // Credit/Debit Card number
            case 'CARDNO':
                 var idIndex = (obj.name).indexOf("_cardNumber");
                 var subString = (obj.name).substr(0,idIndex);
                 var object = (subString)+ "_type";
                 FieldObjCardType=$("payment_type_0");
               // FieldObjCardType=getElem(obj_params[0]);
               // var cardType=FieldObjCardType.options[FieldObjCardType.selectedIndex].value;
                 var cardType='';
                 if(FieldObjCardType)
                 {
                     var cardTypeValue =(FieldObjCardType.value).split("|");
                     cardType = cardTypeValue[0];
                 }
                if(cardType=="AMERICAN_EXPRESS")
                {
                    Pat1 = /^[0-9]{15}$/;
                }
                else
               {
                    Pat1 = /^[0-9]{16,20}$/;
               }
               obj.value = removeAllSpaces(obj.value)
                    obj_value = obj.value
                  if (!Pat1.test(obj_value)) {
                          return setFocus('Please enter a valid card number.',obj)
                   }
                    break;

            // Check to see if two values of 2 fields match
            case 'MATCH':
                   obj_1 = getElem(obj_params[0])
                   obj_2 = getElem(obj_params[1])
                  if (obj_1.value != obj_2.value) return setFocus(obj_name,obj_1)
                    break;

            case 'EITHER_OR':
                 obj_1 = getElem(obj_params[0])
                   obj_2 = getElem(obj_params[1])
                   if( obj_1.value == '' && obj_2.value == '' ) return setFocus(obj_name, obj_1)
                   break;
            case 'AND_OR':
                   obj_1 = getElem(obj_params[0])
                   obj_2 = getElem(obj_params[1])
                   if( obj_1.value == '' ) return setFocus(obj_name, obj_1)
                   break;
             case 'AND':
                 obj_1 = getElem(obj_params[0])
                   obj_2 = getElem(obj_params[1])
                   if( obj_1.value == '' || obj_2.value == '' ) return setFocus(obj_name, obj_1)
                   break;

              case 'RADIO_SELECTED':
                    num = obj_params.length;
                    selected = false;
                    for (z=0;z<num;z++)
                    {
                       if( getElem(obj_params[z]).checked )
                       {
                          selected = true;
                       }
                    }
                    if( selected == false ) return setFocus(obj_name, getElem(obj_params[num-1]))
                  break;

            case 'DATE':
                   day = getElemName(obj_params[0])[0].value
                   month = getElemName(obj_params[1])[0].value
                    year = getElemName(obj_params[2])[0].value
                    datein = day + '/' + month +'/' + year
                    var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;
                    var matchArray = datein.match(datePat);
                   if (matchArray == null) {
                       return setFocus(obj_name,obj)
                   } else {
                    if ((month==4 || month==6 || month==9 || month==11) && day==31) {
                          return setFocus(obj_name,obj)
                    }
                    if (month == 2) {
                        var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
                        if (day>29 || (day==29 && !isleap)) {
                                return setFocus(obj_name,obj)
                        }
                    }
                   }
                   break;

           case 'LUHN':
                  obj_1 = getElem(obj_params[0])
                 var cardnumber = getElem(obj_params[0]).value
                  var oddoreven = cardnumber.length & 1;
                 var sum = 0;
                 var addition = "";

                 for (var count = 0; count < cardnumber.length; count++)
                 {
                    var digit = parseInt(cardnumber.substr(count,1));
                    if (!((count & 1) ^ oddoreven))
                    {
                       digit *= 2;
                       if (digit > 9)
                       {
                          digit -= 9;
                          addition = addition + ' ' + digit;
                       }
                       else
                       {
                          addition = addition + ' ' + digit;
                       }
                       sum += digit;
                    }
                    else
                    {
                       sum += digit;
                       addition = addition + ' ' + digit;
                    }
                 }

                 if (sum % 10 != 0) {}
                     break;

           case 'SECURITY':
                 var cardType='';
               var securityCodeLength='';
               Pat1 = /^[0-9 ]*$/;
               var idIndex = (obj.name).indexOf("_securityCode");
               var subString = (obj.name).substr(0,idIndex);
               var object = (subString)+ "_securityCode";
               var indexValue = (object).split("_");
               cardType = $("payment_type_"+indexValue[1]).value;
               if(cardType && cardType != '')
               {
               cardTypeValue=cardType.split("|")[0];
               securityCodeLength=$(cardTypeValue+"_securityCodeLength").value;
               if( (obj.value.length != securityCodeLength)  || (!Pat1.test(obj.value)) )
               {
                  return setFocus('Please enter a valid security number (the last '+securityCodeLength+' digits in the signature strip on the reverse of your card)',obj)
               }
               }
               break;

            // Review >> Check for < and >
            //ALT defined as >> 0-EmptyAlert|1-Y/N|2-inputtype|3-Minlength|4-MinlengthAlert|5-Maxlength|6-MaxlengthAlert
            case 'REVIEW':
                    var Pat1 = /^[^<>]*$/;
                    if (!Pat1.test(obj_value)) { return setFocus('We cannot accept HTML or angle brackets in your review, please remove any HTML or \'<\' or \'>\' characters.',obj) }
                    if(valset[3]!='') { //check for the minlength attribute
                       if (obj_value.length<valset[3])
                       {
                          if(valset[4]!='')
                             { return setFocus(valset[4],obj);}
                          else
                             { return setFocus('You have entered '+obj_value.length+' characters and have exceeded the '+valset[3]+' character limit',obj); }
                       }
                    }
                    if(valset[5]!='') { //check for the maxlength attribute
                       if (obj_value.length>valset[5])
                       {
                          if(valset[6]!='')
                             { return setFocus(valset[6],obj);}
                          else
                             { return setFocus('This field has a minimum '+valset[5]+' character limit. You have entered only '+obj_value.length+' characters',obj); }
                       }
                    }
                 break;

           case 'ALPHANUMHYPHEN':
                 var Pat1 = /^[-a-zA-Z0-9]*$/;
                 if (!Pat1.test(obj_value)) { return setFocus('Please enter a valid Promotional Code. It accepts only Alphabets, Numerics and Hyphen(-)',obj) }
             break;

           case 'DECIMAL':
                 var Pat1 = /^[0-9.]*$/;//Check for Numeric and dot
                 var Pat2 = /^[0-9]+(\.\d{1,2})?$/;//Check for 2 optional decimals
                 if (!Pat1.test(obj_value) ||!Pat2.test(obj_value) ) { return setFocus('Please enter a valid amount and only upto 2 decimal places',obj) }
              break;
         } // switch
            } // if - mandatory
         } // if - Validation Required
      } // if alt tag exists
   } // for
   // Change button style
   $("confirmButton").innerHTML = "";
   var payButtonDescription = threeDCards.get(PaymentInfo.selectedCardType);
   if(payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup")
   {
      $("confirmButton").innerHTML = '<a href="javascript:void(0);" id="threeDPay"><img src="/cms-cps/thomson/byo/images/proceed-to-payment-btn.gif" alt="Proceed to payment" title="Proceed to payment"/></a>'
   }
   else
   {
      $("confirmButton").innerHTML ='<a href="javascript:void(0);" id="threeDPay"><img src="/cms-cps/thomson/byo/images/pay-btn.gif" title="Pay" border="0"/></a>';
   }
   formobj.submit();
}

function CheckSeniorPassengerAndValidate()
{
   var count65 = 0;
   var count75 = 0;
   obj = document.getElementsByName('importantinfo')[0];
   objHop =document.getElementsByName('importantInformationChecked')[0];
   objPromocode = $('promotionalCode');
   if($('isPromoCodeApplied'))
   {
      promoCodeValidated = $('isPromoCodeApplied').value;
   }
   if (objPromocode)
   {
      if ((objPromocode.value!='') && promoCodeValidated == '')
      {
         alert("Please validate your promotional code before you click on pay.");
         objPromocode.focus();
         return;
      }
      else if((objPromocode.value!='') && promoCodeValidated == 'false')
      {
         alert("Please validate your promotional code before you click on pay.");
         objPromocode.focus();
         return;
      }
   }
   if (!isInsuranceChangesApplicable)
   {
	   for (i=0; i<totalPassengerCount ; i++)
	   {
	      if(document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0]!=null && document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0].checked == true)
	      {
	         count65=count65+1;
	      }
	      if(document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0]!=null && document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0].checked == true)
	      {
	         count75=count75+1;
	      }
	   }
	   if(count65 !=noPerson65)
	   {
	      if(count65 < noPerson65)
	      {
	         for(i=0;i<=count65;i++)
	         {
	            return setFocus(lessSeniorPassengersAged65To75Message, document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0]);
	         }
	      }
	      else
	      {
	         for(i=0;i<=count65;i++)
	         {
	            return setFocus(moreSeniorPassengersAged65To75Message, document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0]);
	         }
	      }
	   }
	   else if(count75 != noPersons75)
	   {
	      if(count75 < noPersons75)
	      {
	         for(i=0;i<=count75;i++)
	         {
	            return setFocus(lessSeniorPassengersAged76To84Message, document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0]);
	         }
	      }
	      else
	      {
	         for(i=0;i<=count75;i++)
	         {
	            return setFocus(moreSeniorPassengersAged76To84Message, document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0]);
	         }
	      }
	   }
   }

   ValidateForm(document.paymentdetails);

}

function checkDateOfBirthValidation()
{
   dayArray = getElemName('day');
   monthArray = getElemName('month');
   yearArray = getElemName('year');
   for(var i=0;i<dayArray.length;i++)
   {
       datein = dayArray[i].value+ '/' + monthArray[i].value +'/' + yearArray[i].value;
       var datePat = /^(\d{1,2})(\/|-)(\d{1,2})\2(\d{4})$/;
       var matchArray = datein.match(datePat);
       if (matchArray == null)
       {
            return setFocus("Please enter the date of birth of each child",dayArray[i])
       }
       else {
          if ((monthArray[i].value==4 || monthArray[i].value==6 || monthArray[i].value==9 || monthArray[i].value==11) && dayArray[i].value==31) {
              return setFocus("Enter a Valid Date",dayArray[i])
          }
          if (monthArray[i].value == 2)
          {
                var isleap = (yearArray[i].value % 4 == 0 && (yearArray[i].value % 100 != 0 || yearArray[i].value % 400 == 0));
                if (dayArray[i].value>29 || (dayArray[i].value==29 && !isleap))
                {
                    return setFocus("Enter a Valid Date",dayArray[i])
                }
          }
       }
    }
}

function validatePassenegersOver65(passengerCount)
{
    var i = passengerCount;
         if((document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0] !=null && document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0].checked == true) && (document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0]!=null && document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0].checked == true))
         {
          document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0].checked = false;
          return;
         }
}

function validatePassenegersOver75(passengerCount)
{
      var i = passengerCount;
         if((document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0]!=null && document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0].checked == true) && (document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0]!=null && document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0].checked == true))
         {
          document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0].checked = false;
          return;
         }
}

/*This file contains functions related Validation of payment panel *
 */
/* Used to validate fields in payment methods */
function validatePaymentFields(inputObj)
{
   thisObjType=inputObj.id.split("_")[2];
   // Card number (method: Card,CNP)
   if(thisObjType=='cardNumberId'&&inputObj.value!='')
   {
      inputObj.value = removeAllSpaces(inputObj.value);
   }

   //Name on card (method: Card,CNP)
   if(thisObjType=='nameOnCardId'&&inputObj.value!='')
   {
      inputObj.value = trimSpaces(inputObj.value);
   }

   if(thisObjType=='postCodeId'&&inputObj.value!='')
   {
      inputObj.value = trimSpaces(inputObj.value);
   }

   //Security code (method: Card,CNP)
   if(thisObjType=='securityCodeId'&&inputObj.value!='')
   {
      inputObj.value = removeAllSpaces(inputObj.value);
   }
}