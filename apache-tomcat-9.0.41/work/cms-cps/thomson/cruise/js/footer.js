// Set Booking Mode
 isBookingMode = true

function contactUsPop() {
   if(isBookingMode){
      window.open('http://www.thomson.co.uk/po/showContent.do?content=contact_us.htm','blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.thomson.co.uk/po/showContent.do?content=contact_us.htm';
   }
}

function aboutUsPop() {
   if(isBookingMode){
      window.open('http://www.thomson.co.uk/po/showContent.do?content=about_us.htm','blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.thomson.co.uk/po/showContent.do?content=about_us.htm';
   }
}

function termsUsPop() {
   if(isBookingMode){
      window.open('http://www.thomson.co.uk/po/showContent.do?content=terms_conditions.htm','blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.thomson.co.uk/po/showContent.do?content=terms_conditions.htm';
   }
}

function helpPagesPop() {
   if(isBookingMode){
      window.open('http://www.thomson.co.uk/po/showContent.do?content=help.html','blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.thomson.co.uk/po/showContent.do?content=help.html';
   }
}
function privacyPop() {
   if(isBookingMode){
      window.open('http://www.thomson.co.uk/po/showContent.do?content=privacy.htm','_blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.thomson.co.uk/po/showContent.do?content=privacy.htm';
   }
}

function brochureSearchPop() {
   if(isBookingMode){
      window.open('http://www.thomsonbeach.co.uk/th/beach/invokeAccommodationSearch.do?ico=POHomeFootBrocCodeSch','_blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.thomsonbeach.co.uk/th/beach/invokeAccommodationSearch.do?ico=POHomeFootBrocCodeSch';
   }
}

function shopFinderPop() {
   window.open('http://tui.intelli-direct.com/e/PExit.dll?m=237&p=104&url=http%3a%2f%2fcz%2Emultimap%2Ecom%2fclients%2fplaces%2Ecgi%3fclient%3dtuisf_stores','name','height=500,width=800,toolbar=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes');
}

function registerForOffersPop() {
   if(isBookingMode){
      window.open('http://www.thomson.co.uk/po/showContent.do?content=register.htm&ICO=RegisterFooter','_blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.thomson.co.uk/po/showContent.do?content=register.htm&ICO=RegisterFooter';
   }
}

function rssPop()
{
   if(isBookingMode){
      window.open('http://www.thomson.co.uk/po/showContent.do?content=rss.html&ICO=rssfoot','_blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   }
   else
   {
      document.location.href='http://www.thomson.co.uk/po/showContent.do?content=rss.html&ICO=rssfoot';
   }
}

function thomsonLatestPop() {
   if(isBookingMode){
      window.open('http://destinations.thomson.co.uk/devolved/about-thomson/press_office.htm','_blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://destinations.thomson.co.uk/devolved/about-thomson/press_office.htm';
   }
}

function siteMapPop() {
   if(isBookingMode){
      window.open('http://www.thomson.co.uk/po/showContent.do?content=sitemap.htm&ico=POH_sitemap_footer','_blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.thomson.co.uk/po/showContent.do?content=sitemap.htm&ico=POH_sitemap_footer';
   }
}

function destinationsPop() {
   if(isBookingMode){
      window.open('http://destinations.thomson.co.uk/','_blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://destinations.thomson.co.uk/';
   }
}

function abtaPop() {
   if(isBookingMode){
      window.open('http://www.abta.com/scripts/verify.hts?+V5126','_blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.abta.com/scripts/verify.hts?+V5126';
   }
}

function holidaysPop() {
   if(isBookingMode){
      window.open('http://www.thomson.co.uk','_blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.thomson.co.uk';
   }
}

function flightsPop() {
   if(isBookingMode){
      window.open('http://www.thomsonfly.com','_blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.thomsonfly.com';
   }
}

function protectedPop() {
   if(isBookingMode){
      window.open('http://www.thomson.co.uk/editorial/legal/about-thomson.html','_blank','width=800,height=500,toolbar =yes,scrollbars=yes,resizable=yes');
   } else {
      document.location.href='http://www.thomson.co.uk/editorial/legal/about-thomson.html';
   }
}

function openWindow(url)
{
	window.open(url,'blank');
	return false;
}