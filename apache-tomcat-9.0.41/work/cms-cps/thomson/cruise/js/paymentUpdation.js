/**
 * This function is used to update the remaining holiday cost.
 */
function remainingHolidayCost()
{
   var totalamount = PaymentInfo.totalAmount;
   if($("highlightDepositCost"))
   {
      var depositAmount = 1*parseFloat(depositAmountsMap.get("DEPOSIT"));
      var remaingBalance = setCommaSeperated(roundOff(totalamount - depositAmount, 2));
      $('remainingBalance').innerHTML = "&pound;"+remaingBalance+"**";
   }
}

/*
 * This function calls promCodeServlet to calculate
 * Promotional code discounts and gets calculated amount
 * and discount amount.
 */
function updatePromotionalCode()
{
   var promo_obj =  $("promotionalCode");
   var promCode = promo_obj.value;
   if((promCode.length > 8 && promCode.length < 21) || promCode.length == 0)
   {
      return setFocus("Please enter a valid Promotional Code",promo_obj);
   }
   else if(promCode.length == 21)
   {
      //Use this pattern if promocode of length 20 requires first 17 as numeric and last 3 as alpha.
   // -- var Pat1 = /^[[0-9]{17}[a-zA-Z]{3}]*$/;
      var Pat1 = /^[0-9a-zA-Z]*$/;
      if (!Pat1.test(promCode))
      {
         return setFocus("Please enter a valid Promotional Code",promo_obj);
      }
   }
   else if(promCode.length <= 8)
   {
      var Pat2 = /^[0-9]*$/;
      if (!Pat2.test(promCode))
      {
         return setFocus("Please enter a valid Promotional Code",promo_obj);
      }
   }
   var balanceType = getBalanceType();
   if(newHoliday == "true")
   {
      var url="/cps/promCodeServlet?promotionalCode="+promCode+"&balanceType="+balanceType+"&token="+token;
      url=uncache(url);
      url = url+tomcatInstance;
      var request = new Ajax.Request(url,{method:"GET", onSuccess:responseUpdatePromotionalCodeDiscount});
   }
   else
   {
      return false;
   }
}

function responseUpdatePromotionalCodeDiscount(request)
{
   if ((request.readyState == 4) && (request.status == 200))
   {
      var temp=request.responseText;
      temp=temp.split("|");
      var promCodeDiscount= temp[0];
      var totalAmount=temp[1];
      var promotionalCode=temp[2];
      if(parseFloat(promCodeDiscount))
      {
         $('promoDiscount').value =promCodeDiscount;
         commonPromotionalDiscount('true', 'block', promCodeDiscount, totalAmount);
         displayPromoCodeSuccessMessage(promCodeDiscount);
      }
      else
      {
         commonPromotionalDiscount('false', 'none', 0, totalAmount);
         displayPromoCodeErrorMessage(promCodeDiscount, promotionalCode);
      }
   }
}

/** The common methods used when a valid or invalid promotional code is applied.*/
function commonPromotionalDiscount(booleanValue, identifier, promoCodeDiscount, totalAmount)
{
   $('isPromoCodeApplied').value =booleanValue;
   updatePromoDiscountInSummaryPanel(identifier , promoCodeDiscount);
   PaymentInfo.totalAmount = totalAmount;
   PaymentInfo.calculatedTotalAmount = totalAmount;
   depositAmountsMap.put(bookingConstants.FULL_COST, totalAmount);
   updatePaymentInfo();
   updateCardChargesInSummaryPanel(PaymentInfo.totalCardCharge);
   displayTotalAmounts(bookingConstants.TOTAL_CLASS);
   if($("highlightFullCost"))
      displayFullCost(totalAmount);
   remainingHolidayCost();
}

function displayFullCost(amount)
{
   var cardCharge = calculateCardCharges(amount);
   var fullCost = roundOff(1*amount+1*cardCharge,2);
   $("highlightFullCost").innerHTML = formatDepositAmount(fullCost);
   depositAmountsMap.put(bookingConstants.FULL_COST, amount);
}

function clearDiv(id)
{
   if($(id).innerHTML != "")
   {
      $(id).innerHTML="";
      $(id).style.display='none';
   }
}

/*Displays error message in payment page when promo code discount is not available
 *
 */
function displayPromoCodeErrorMessage(promCodeDiscount, promotionalCode)
{
   var promoCodeObj = promotionalCode;
   var noOfZeros='';
   if(promoCodeObj.length < 8)
   {
      var count = 8 - promoCodeObj.length;
      for(i=0;i< count; i++)
      {
        noOfZeros = noOfZeros + 0;
      }
   }
   promoCodeObj = noOfZeros + promoCodeObj;
   $('pricediff1').style.display='block';
   $('pricediff1').style.color='#3366CC';
   if (promCodeDiscount.indexOf('promotional') != -1)
   {
      $('pricediff1').innerHTML = promCodeDiscount ;
      $('promotionalCode').value = "";
      $('promotionalCode').focus();
   }
   else
   {
      $('pricediff1').innerHTML = promCodeDiscount + " " + promoCodeObj;
      $('promotionalCode').value = "";
      $('promotionalCode').focus();
   }
   clearDiv('promoText1');
   clearDiv('promoText2');
   clearDiv('promoContent');
}

/********************************************************************************************************/
/* Displays promocode in summary panel if promo discount is applicable
 * Else Hides promocode in summary panel if promo discount is not applicable
 *
 *@param display- display is either 'block' or 'hide'
 *@param promCodeDiscount - Discount to be shown in summary panel
 */
function updatePromoDiscountInSummaryPanel(display , promCodeDiscount)
{
   if($('promoText'))
   {
      if(1*promCodeDiscount > 0)
      {
         $('promoCodeDiscount').innerHTML="&#45;&pound;"+ promCodeDiscount;
         $('promoText').style.display='block';
         $('promoCodeDiscount').style.display='block';
      }
      else
      {
         $('promoText').style.display='none';
         $('promoCodeDiscount').style.display='none';
      }
   }
}

/*
*
*/
function displayPromoCodeSuccessMessage(promCodeDiscount)
{
   var divElement = $('pricediff1');
   if (divElement.innerHTML != "" && (divElement.innerHTML.indexOf('Promotional') != -1 || divElement.innerHTML.indexOf('PROMOTIONAL') != -1 || divElement.innerHTML.indexOf('promotional') != -1))
   {
      $('pricediff1').innerHTML= "";
      $('pricediff1').style.display='none';
   }

  if($('promoText1'))
  {
    $('promoText1').innerHTML="Amount of -&pound;"+promCodeDiscount+" has been deduced from the total cost.";
    $('promoText1').style.display='block';
  }
  clearDiv('promoText2');
  clearDiv('promoContent');
}

/**********************************************************************************************************/

function uncache(url)
{
   var d = new Date();
   var time = d.getTime();
   var newUrl = url + "&time="+time;
   return newUrl;
}
/*********************AJAX helpers ******************/

/** This function is responsible for updating card charges in summary panel
 *   @params cardCharge -represents cardChargeAmt to be displayed for the transaction
 */
function updateCardChargesInSummaryPanel(cardCharge)
{
   if($('cardChargeText'))
   {
      if(cardCharge > 0)
      {
         $('cardChargeAmount').innerHTML="&pound;"+roundOff(1*cardCharge,2);
         $('cardChargeText').style.display='block';
         $('cardChargeAmount').style.display='block';
      }
      else
      {
         $('cardChargeText').style.display='none';
         $('cardChargeAmount').style.display='none';
      }
   }
}


/* Function to get balance type.
*
*/
function getBalanceType()
{
   var bal=document.getElementsByName("depositType");
   var balanceType='';
   if(bal.length&&bal.length>0)
   {
      for(i=0;i<bal.length;i++)
      {

         if(bal[i].checked==true)
         {
            balanceType=bal[i].value;
         }
     }
      if(balanceType=='fullCost')
      {
        balanceType='FullBalance';
      }
   }//End if(outer if)
   else
   {
       balanceType='FullBalance';
   }
   return  balanceType;
}
/**********************End of AJAX Helpers ********/

/**********************End of AJAX  related functions *********/

/*********************For B2C *********************************/
function updateEssentialFields()
{
   $("total_transamt").value = PaymentInfo.calculatedPayableAmount;
}

/*********************End B2C *******************************/

/** The common function resonsible for updating the display fields.
 ** Listeners can be added or removed as necessary.
**/
function displayFieldsWithValuesForCard()
{
   displayCardRelatedFields();
   displayAllDepositOptionsWithCardCharge();
   displayTotalAmounts(bookingConstants.TOTAL_CLASS);
   //Following function is an existing brand specific function for displaying card charges in summary panel.
   updateCardChargesInSummaryPanel(PaymentInfo.totalCardCharge);

}

/** The common function resonsible for updating the display fields.
 ** Listeners can be added or removed as necessary.
**/
function displayFieldsWithValuesForDeposit()
{
   displayTotalAmounts(bookingConstants.TOTAL_CLASS);
   //Following function is an existing brand specific function for displaying card charges in summary panel.
   updateCardChargesInSummaryPanel(PaymentInfo.totalCardCharge);
}

function formatDepositAmount(amountForFormatting)
{
	var amount = setCommaSeperated(amountForFormatting);
	return amount;
}

function setCommaSeperated(value)
{
   if((roundOff(1*value,2)).length>6)
   {
      var valueLength = value.length;
      var count = (""+parseInt(value/1000)).length;
      value=parseInt(""+(value/1000))+","+value.substring(count, valueLength);
   }
   return value;
}

/** This method is called on change of the select box. */
function displayCardRelatedFields()
{
   // Show or hide issue number field
   $("payment_0_paymenttypecode").value=PaymentInfo.selectedCardType;
   if(PaymentInfo.selectedCardType == "PleaseSelect")
   {
      $("debitCards0").style.display = 'none';
      $('securityCodeHelp0').innerHTML = "(last 3 digits in the signature strip on the reverse of your card)";
      return;
   }
   if($(PaymentInfo.selectedCardType+'_issueNo').value == 'true')
   {
      $("debitCards0").style.display = 'block';
   }
   else
   {
     $("debitCards0").style.display = 'none';
   }
   var securityNumLen = $(PaymentInfo.selectedCardType+'_securityCodeLength').value;
   if(PaymentInfo.selectedCardType.indexOf('AMERICAN_EXPRESS') ==0)
      $('securityCodeHelp0').innerHTML = "(the "+securityNumLen+" digit code above the card number on the front of your card)";
   else
      $('securityCodeHelp0').innerHTML = "(last "+securityNumLen+" digits in the signature strip on the reverse of your card)";
   $('payment_0_securityCodeId').alt = 'Please enter a valid security number (the last '+securityNumLen+' digits in the signature strip on the reverse of your card)|Y|SECURITY';
}

/*Update the amount payable based on deposit radio button selection */
function initializeDepositSelection()
{
  var depositsTypes = document.getElementsByName("depositType");
  if(depositsTypes && depositsTypes != undefined && depositsTypes != null)
  {
    for(var i=0;i<depositsTypes.length; i++)
    {
       if(depositsTypes[i].checked)
       {
         PaymentInfo.depositType = trimSpaces(depositsTypes[i].value);
       }
    }
  }
}

/** Method to check the deposit radio button. */
function radioChecked()
{
  if(!$("lowDepositR") && $("depositR"))
  {
     $("depositR").checked = true;
  }
}

/* Returns DispCardText(which is displayed under security code caption)  based on selected payment method
*  @params selPaymentMethod - selected payment method i.e-  AMERICAN_EXPRESS, VISA etc
*              securityCodeLength  - Security code associated with this payment method
*/
function checkForDispCardText(selPaymentMethod, securityCodeLength )
{
   var dispCardText = '';
   if(selPaymentMethod.indexOf('AMERICAN_EXPRESS') ==0)
    {
       dispCardText = "the "+securityCodeLength+" digit code above the card number on the front of your card";
    }
    else
    {
       dispCardText = "last "+securityCodeLength+" digits in the signature strip on the reverse of your card";
    }
   return dispCardText;
}

function setToDefaultSelection()
{
   $('payment_type_0').selectedIndex = 0;
   $('payment_0_expiryMonthId').selectedIndex = 0;
   $('ExpiryYear').selectedIndex = 0;
   $('payment_0_cardNumberId').value="";
   $('payment_0_nameOnCardId').value="";
   $('payment_0_securityCodeId').value="";
   if($('issueNumberInput'))
   {
      $('issueNumberInput').value="";
   }
}

    //Auto Completion of surnames
function autoCompletionSurname(passengerCountBasedOnRooms, startAutoCompletion)
{
   if(newHoliday != "false")
   {
      if($('autoCheck_'+startAutoCompletion).checked) {
         for(index=startAutoCompletion; index<(startAutoCompletion+passengerCountBasedOnRooms-1); index++)
         {
            if ($('lastname_'+index))
            {
               $('lastname_'+index).value = $('lastname_0').value;
            }
         }
      }
   }
}

function unCheck()
{
   if(document.getElementById('autoCheck_1'))
   {
      document.getElementById('autoCheck_1').checked = false;
   }
}

/**This function is brand specific and changes the pay button based on the selected cards 3D scheme*/
function changePayButton()
{
	var cardType = $('payment_type_0').value.split('|');
	var payButtonDescription = threeDCards.get(cardType[0]);
	var element = document.getElementsByClassName('btnContinue')[0];
	if (payButtonDescription == "mastercardgroup")
	{
		element.innerHTML = "<img src='/cms-cps/thomson/cruise/images/payButtons/confirm-with-mastercard-securecode-btn.gif' alt='Confirm with Mastercard Securecode' id='ContinueText'   border='0' />";
	}
	else if(payButtonDescription == "visagroup")
	{
		element.innerHTML = "<img src='/cms-cps/thomson/cruise/images/payButtons/confirm-with-verified-by-visa-btn.gif' alt='Confirm with Verified by Visa' id='ContinueText'   border='0' />" ;
	}
	else if(payButtonDescription == "americanexpressgroup")
	{
		element.innerHTML = "<img src='/cms-cps/thomson/cruise/images/payButtons/confirm-with-amex-safekey-btn.gif' alt='Confirm with American Express SafeKey' id='ContinueText'   border='0' />" ;
	}
	else
	{
		element.innerHTML = "<img src='/cms-cps/thomson/cruise/images/buttons/btn-pay.gif' width='100' height='25' alt='Pay' />";
	}
}

function copyLeadPassengerAddress()
{
   if($('autoCompleteAddress').checked) {
         $('street_address1').value = $('houseName').value;
         $('street_address2').value = $('addressLine1').value;
         $('street_address3').value = $('city').value;
         $('street_address4').value = $('county').value;
         $('payment_0_postCode').value = $('postCode').value;
         $('country1').value = $('country').value;

   }
}

/**This function is brand specific and changes the pay button based on the selected cards 3D scheme*/
function changePayButton()
{
	var cardType = $('payment_type_0').value.split('|');
	var payButtonDescription = threeDCards.get(cardType[0]);
	var element = document.getElementsByClassName('btnContinue')[0];
	if (payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup" || payButtonDescription == "americanexpressgroup")
	{
		element.innerHTML = "<img src='/cms-cps/thomson/cruise/images/payButtons/proceed-to-payment-btn.gif' alt='Confirm with Mastercard Securecode' id='ContinueText'   border='0' />";
	}
	else
	{
		element.innerHTML = "<img src='/cms-cps/thomson/cruise/images/buttons/btn-pay.gif' width='100' height='25' alt='Pay' />";
	}
}

function toggleCruiseOverlay()
{
	  var overlayZIndex = 99;
	  var zIndex = 100;
	  var prevOverlay;
	  var stickyOpened = false;
	  jQuery("a.stickyOwner").click(function(e){
	    var overlay = "#" + this.id + "Overlay";
		if (!stickyOpened)
		{
			prevOverlay = overlay;
		}
		if (prevOverlay != overlay)
		{
			jQuery(prevOverlay).hide();
			stickyOpened = false;
		}
		var pos = jQuery("#"+this.id).offset();
		var left = parseInt((1*pos.left-195),10);
		var top = parseInt((1*pos.top+45),10);
		jQuery(overlay).show();
		jQuery(overlay).css("left",left);
		jQuery(overlay).css("top",top);
		prevOverlay = overlay;
		stickyOpened = true;
		jQuery(overlay + ".genericOverlay").css("z-index",zIndex);
		zIndex++;

		if (jQuery(overlay).parent(".overlay") != null){
		  jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
		  overlayZIndex++;
		}
	    return false;
	  });

	  jQuery("a.close").click(function(){
	    var overlay = this.id.replace("Close","Overlay");
	    jQuery("#" + overlay).hide();
	    return false;
	  });
}

