/*
   Global Ajax variables.
*/
var ajax_xmlHttp;
var _global_targetId;
var _global_debug=false;


var _const_replace_replace=1;
var _const_replace_overwrite=2;
var _const_replace_append=3;

/*
   Creates the XMLHttpRequest object depending on which browser is being used.
*/
function createXMLHttpRequest()
{         
  try
  {    
     // Firefox, Opera 8.0+, Safari    
     ajax_xmlHttp = new XMLHttpRequest();       
  }
  catch (e)
  {    
     // Internet Explorer    6.0+
     try
     {      
        ajax_xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");             
     }
     catch (e)
     {      
        try
        {        
          // Internet Explorer    5.5
          ajax_xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");                
        }
        catch (e)
        {        
           alert("Your browser does not support AJAX!");                   
        }      
     }    
  }       
}

/*
    return the standard lightbox parameters.
*/
function ajax_getLightboxParams(modal, width, height)
{
    var lightboxParams = "#TB_inline?height="+height+"&amp;width="+width+"&amp;inlineId=lightBoxDiv";
    if (modal)
    {
        lightboxParams = lightboxParams + "&amp;modal=true";
    }
    return lightboxParams;
}

/*
    Display the specified page via ajax.
*/
function ajax_displayPage(page, modal, height, width, parentId)
{
    var params = ajax_getLightboxParams(modal, height, width);
    if (parentId == null) parentId = "lightBoxDiv";
    var callbackFunction = "general_displayContent(ajax_xmlHttp.responseXML.getElementsByTagName('result')[0].firstChild.nodeValue,'"+ parentId +"','"+params+"')";
    general_showWait();
    ajax_makeAjaxCall(page, callbackFunction);
    return false;
}

/*
    Display a message using ajax and thickbox.
*/
function ajax_displayMessage(msg, modal, height, width)
{
    height = 300;
    width = 100;
    var params = ajax_getLightboxParams(modal, height, width);
    general_displayContent(msg,'lightBoxDiv', params, "Message");
    return false;
}

/*
    Load the specified content into a div on the page.
*/
function ajax_doLoadContent(page, parentId, replace, postFunction)
{
    if (_global_debug)alert("ajax_doLoadContent: parentId=" + parentId + ", replace=" + replace + ", postFunction=" + postFunction);
	var callbackFunction = "ajax_setResponseInTarget(ajax_xmlHttp.responseXML.getElementsByTagName('result')[0].firstChild.nodeValue,'"+parentId+"',"+replace;
    if (postFunction != null)
	{
		callbackFunction += ",'" + postFunction + "'";
	}
	callbackFunction += ")";
			
    ajax_makeAjaxCall(page, callbackFunction);
    return false;
}

/*
    Load the content into a data div against the specified parentid.
*/
function ajax_loadContent(page,parentId, postFunction)
{
if (_global_debug)alert("overwriting content in " + parentId);
    return ajax_doLoadContent(page,parentId, _const_replace_overwrite, postFunction);
}

/*
    Append the content in page into content div against the parentid.
*/
function ajax_appendContent(page, parentId)
{
if (_global_debug)alert("appending content to " + parentId);
    return ajax_doLoadContent(page,parentId, _const_replace_append);
}

/*
    Replace all the content in parentId with the content from the page.
*/
function ajax_replaceContent(page, parentId)
{
if (_global_debug)alert("appending content to " + parentId);
    return ajax_doLoadContent(page,parentId, _const_replace_replace);
}

/*
    Make the call to ajax.
*/
function ajax_makeAjaxCall(page, callback_function)
{
    if (_global_debug)alert("ajax_makeAjaxCall: callback_function=" + callback_function);
	url = _global_url + page;
	createXMLHttpRequest();
	ajax_xmlHttp.open("GET", url, true);
	
	ajax_xmlHttp.onreadystatechange = function()
	{
	   if (_global_debug)alert("onreadystatechange");
       if (ajax_xmlHttp.readyState == 4) 
       {
           if (_global_debug)alert("ajax_xmlHttp.readyState == 4");
      	   general_clearWait();
           if (ajax_xmlHttp.status == 200) 
           {   
               if (_global_debug)alert("ajax_xmlHttp.status == 200, callback_function=" + callback_function);
               try
               {                       
                  eval(callback_function);
               }   
               catch (e)
               {    
                 if (_global_debug)alert("Ajax Call Back Function Error : "+callback_function);                                                           
               }      
                              
           } else 
           {
               alert('There was a problem with the request.(Code: ' + ajax_xmlHttp.status + ')');
           }
       }
   } 

   ajax_xmlHttp.send(null);
}

/**
 * Check if the first node is an element node
 */
function getFirstElement(n)
{
  var x=n.firstChild;
  while (x && x.nodeType!=1)
  {
    x=x.nextSibling;
  }
  return x;
}

/**
 *   set the response in the parentid div.
 */
function ajax_setResponseInTarget(responseXML,parentId, replace, postFunction)
{   
   contentHolder = ajax_createContentDiv(responseXML, parentId, replace);     
   var node = getFirstElement(contentHolder);   
   if(node && node.id == "ajaxErrorMsg")
   {            
      win_showMessageWindow("ajaxError");
      ajax_error_clear();
   }
   else if (postFunction != null)
	{
		eval(postFunction);// call postFunction if present	
	}   

   return false;
}
function ajax_error_clear()
{   
    document.getElementById("ajaxErrorMsg").innerHTML = "";
}
/*
    Test method to ensure that calls are being serviced through this js.
*/
function ajax_test(msg)
{
    alert(msg);
}

 /**
  * gets the contents of the form as a URL encoded String
  * suitable for appending to a url
  * @param formName to encode
  * @return string with encoded form values , beings with &
  */ 
   function ajax_getFormAsString(formName){
   
      //Setup the return String
      returnString ="";
      
      //Get the form values
      formElements=document.forms[formName].elements;
      
      // loop through the array , building up the url
      // in the form /strutsaction.do&name=value
      // - loop forwards, otherwise fields
      // are sent in reverse order (which is a problem
      // for multi-valued fields). 
      for ( var i = 0; i < formElements.length; i++ )
      {
         currentField = formElements[i];
         // test that field is valid
         if (struts_fieldValid(currentField, true))
         {
            //we escape (encode) each value
            var fldvalue=struts_getFieldValue(formElements[i]);
            returnString=returnString+"&"+escape(currentField.name)+"="+escape(fldvalue);
         }
      }
      
      //return the values
      return returnString; 
   }