/** This is the refactored abstraction of BRAC js functionality */
/** Please use the following guildlines while coding in this js */
/** 1) The onload calls need to be handled in the onLoadHandler and not in the jsp */
/** 2) The view altering code should be pushed into the PaymentView JSON */
/** 3) The domain/static data is pushed into PaymentInfo JSON */
/** 4) Each  transaction needs to be created as a TransactionDetails JSON */
/** 5) Business logic o "what needs to be done when something haapens" needs to go into the respective handlers */
/** 6) Please smile and have fun while coding as it reflects in your code :-) */  


/** The payment page onload handler (the name is enough... Do u need any explanation?)*/
var PaymentPageLoadHandler =
{
	handle: function () 
	{
		//Condition checks based on the datacashEnabled attribute.
	   if (callType=='callCenter')
	   {
	      $("cp").enabled=true;
	      $("cnp").checked='true';
	      setCustomerType('payment_CNP');
	      $("cnp").enabled=true;
	      $("cpna").disabled=true;
	      return false;
	   }
	   if ($("datacashEnabled").value == "false")
	   {
	      if(callType=='callCP'||callType=='Retail')
	      {
	         $("cp").checked='true';
	         setCustomerType('payment_CP');
	         $("cp").disabled=true;
	         $("cnp").disabled=true;
	         $("cpna").disabled=true;
	         return false;
	      }
	   }
	   else
	   {
	      if (callType =='callCenter')
	      {
	         $("cp").enabled=true;
	         $("cnp").checked='true';
	         setCustomerType('payment_CNP');
	         $("cnp").enabled=true;
	         $("cpna").disabled=true;
	         return false;
	      }
	      if(callType =='Retail')
	      {
	         $("cp").enabled=true;
	         $("cnp").checked='true';
	         setCustomerType('payment_CNP');
	         $("cnp").enabled=true;
	         $("cpna").enabled=true;
	         return false;
	      }
	   }
	   /**These two need to be replaced with refactored code.*/
	   PaymentInfo.init();
	   PaymentInfo.refresh();
	   noOfPaymentsHandler(1);
	   PaymentView.displayAmt();
	}
}
/**End of PaymentPageLoadHandler*/


/** This section shall house all the view related code.*/
var PaymentView = 
{
	displayAmt: function()
	{
	   totamtdue = amtRec;
	   totAmtDedVal=amtRec;
	   if (!($("chkbox0") && $("chkbox0").checked))
	   {
	      if(isAdditional)
	      {
	         $("transamt0").disabled = false;
	      }
	      else
	      {
	         $("transamt0").disabled=true;
	      }
	      $("transamt0").value=currencySymbol+amtRec;
	      $("transamt_carry0").value=amtRec;
	      $("tottransamt0").innerHTML=currencySymbol+amtRec;
	      $("total_amount_0").value=amtRec;
	      updateCardCharges(0);
	   }
	},
	
  /**
    * Add payment content like Cash, Dcard, Voucher, Cheque or GiftCard based on paymentMode parameter passed
    *
    * @param index -index of the transaction
    * @param paymentMode -DCard, Cash etc
    * @param paymentType -Cash, Cheque, MasterCard etc
    */
   addPaymentContent : function(index, paymentMode, paymentType)
   {
      //We are calling a method dynamically. Note we need to pass argument name and then value  for the particular method
      $("#paymentOptionDetails"+index).html("");
      if(StringUtils.isNotBlank(paymentMode) )
      {
         var paymentContent = PaymentDetailsContent[paymentMode+"PaymentContent"].call(index,index,paymentMode,paymentType);
         $(paymentContent).appendTo("#paymentOptionDetails"+index);
      }
   }
}
/** End of PaymentView*/

/** This section contains all the data. Probably will ring a bell if i call it the domain object.*/
var PaymentInfo =
{

   /** total amount which will not be updated with card charges. */
   totalAmount :"",

   /** Payable amount which will not be updated with card charges. */
   payableAmount : "",

   /** TotalAmount which will  be updated with card charges. */
   calculatedTotalAmount : "",

   /** Payable amount which will  be updated with card charges. */
   calculatedPayableAmount : "",

   /** Discounts like promo code discounts. */
   calculatedDiscount:0,

   /** max CardCharge Amount allowed for a card transaction. Initial value is zero */
   maxCardChargeAmount : null,

   /** total card charges calculated for the payment . */
   calculatedCardCharge  :0.0,


   /** used to hold selected deposit type value .  */
   depositType:"",

   /** summation of trans amounts+calculatedCardCharge.  */
   paidAmount : 0.0,

   /** summation of trans amounts(excluding last transamount). Initially it is 0.0.*/
   accumulatedAmount : 0.0,

   /** map to contain transactionDetails values with key as (transaction)index.  */
   transactionDetailsMap : null,

   /** Total voucher value. */
   totalVoucherValue : 0.0,

   setTotalAmount:function(totalAmount) { this.totalAmount = totalAmount},
   getTotalAmount:function() {return this.totalAmount },

   setMaxCardChargeAmount:function(maxCardChargeAmount) { this.maxCardChargeAmount = maxCardChargeAmount},
   getMaxCardChargeAmount:function() {return this.maxCardChargeAmount },

   setPayableAmount:function(payableAmount) { this.payableAmount = payableAmount},
   getPayableAmount:function() {return this.payableAmount },

   setCalculatedTotalAmount:function(calculatedTotalAmount) { this.calculatedTotalAmount = calculatedTotalAmount},
   getCalculatedTotalAmount:function() { return this.calculatedTotalAmount},

   setCalculatedPayableAmount:function(calculatedPayableAmount){this.calculatedPayableAmount = calculatedPayableAmount },
   getCalculatedPayableAmount:function(){return this.calculatedPayableAmount},

   setCalculatedDiscount: function(calculatedDiscount) {this.calculatedDiscount = calculatedDiscount },
   getCalculatedDiscount: function(){this.calculatedDiscount = calculatedDiscount },

   /** Caclulated card charge for selectedCardType. */
   getCalculatedCardCharge: function() {return this.calculatedCardCharge},
   getPaidAmount : function() {return this.paidAmount ;  },

   getAccumulatedAmount : function()  {    return this.accumulatedAmount;  },
   setDepositType:function(depositType) { this.depositType = depositType},
   getDepositType:function() { return this.depositType },

   /** Following functions return maps which are created on jsp and populated from server side values and */
   getCardChargeMap : function() { return cardChargeMap} ,

   /** Returns DepositAmount Map */
   getDepositAmountMap : function() { return depositAmountsMap},

   getTransactionDetailsMap : function() {return this.transactionDetailsMap },

   /*
    * Function to be called from JSP to initialize values are which are not sent
    * from server side in-order to avoid script errors.
    */
   init: function()
   {
      if(isNaN(1*this.calculatedDiscount))
      {
         this.calculatedDiscount = 0
      }
      if(isNaN(1*this.maxCardChargeAmount))
      {
         this.maxCardChargeAmount = null;
      }
   },

   /**
    *
    */
   refresh: function()
   {
      this.paidAmount = 0.0;
      this.accumulatedAmount = 0.0;
      this.calculatedCardCharge = 0.0;
      this.calculatedPayableAmount = this.payableAmount;
      this.calculatedTotalAmount = this.totalAmount;
      this.transactionDetailsMap = null;
      this.transactionDetailsMap = new Map();
   },

   resetTransactionDetails : function(index)
   {
	   var transactionDetMapValueList = this.transactionDetailsMap.getAllValuesList();
	   paymentType = transactionDetMapValueList[index].paymentType;
	   paymentMode = transactionDetMapValueList[index].paymentMode;
	   transactionAmount = transactionDetMapValueList[index].transactionAmount;
	   cardCharge = 0;
	   isTransactionCommitted = false;
	   this.transactionDetailsMap.put(index,new TransactionDetails(paymentType, paymentMode,  transactionAmount, cardCharge, isTransactionCommitted));
   },

   /**
    *
    */
   updatePaymentInfo:function()
   {
      this.updatePayableAmount();
	  this.updateCalculatedAmounts();
   },

   /**
    *  Based on depositType selection we shall update payableAmount
    */
   updatePayableAmount :  function()
   {
	   if(PaymentInfo.getDepositType() != undefined)
	   {
         this.payableAmount =  this.getDepositAmountMap().get(this.depositType);
	     this.totalAmount =this.getDepositAmountMap().get(this.depositType);
	     if(this.payableAmount==null)
	     {
	        parseFloat(1*this.totalAmount  - 1*this.calculatedDiscount).toFixed(2)
	     }
	   }
   },


   /**
    *
    */
   updateCalculatedAmounts : function()
   {
      var transactionDetMapValueList = this.transactionDetailsMap.getAllValuesList();
      var totalCardCharge = 0.0;
      var totalTransAmount = 0.0;
      var totalAccumulatedAmount = 0.0;
      for(var index in transactionDetMapValueList)
      {
    	 //alert("Card Charge:"+ transactionDetMapValueList[index].cardCharge +"\n TransAmt:" + transactionDetMapValueList[index].transactionAmount+"\n isTxnCommitted:"+ transactionDetMapValueList[index].isTransactionCommitted)
         totalCardCharge += 1*transactionDetMapValueList[index].cardCharge;
         totalAccumulatedAmount += 1*transactionDetMapValueList[index].transactionAmount;
         if(transactionDetMapValueList[index].isTransactionCommitted)
         {
            totalTransAmount += 1*transactionDetMapValueList[index].transactionAmount;
         }
      }
      this.calculatedCardCharge = totalCardCharge;
	  if (transactionDetMapValueList[index].isTransactionCommitted )
	  {
         this.paidAmount = totalTransAmount+ totalCardCharge;
	     this.calculatedPayableAmount = MathUtils.roundOff(1*this.payableAmount + 1*this.calculatedCardCharge , 2);
         this.calculatedTotalAmount = MathUtils.roundOff((1*this.totalAmount - 1*this.calculatedDiscount) + 1*this.calculatedCardCharge , 2);
	  }
      this.accumulatedAmount = totalAccumulatedAmount;
      //alert("CalculatedCardCharge:"+this.calculatedCardCharge+"\nAmount Paid:"+ this.paidAmount +"\n total amt:"+this.calculatedTotalAmount)
   },

   /**
    *
    */
   addTransactionDetails : function(index,paymentType, paymentMode, transactionAmount, isTransactionCommitted)
   {
      var cardCharge = this.getCardCharge(paymentType, transactionAmount);
      this.transactionDetailsMap.put(index,
                                   new TransactionDetails(paymentType, paymentMode,  transactionAmount, cardCharge, isTransactionCommitted));
   },

   addTransactionDetailsWithCardCharge : function(index,paymentType, paymentMode, transactionAmount, isTransactionCommitted, cardCharge)
   {
	   this.transactionDetailsMap.put(index,
               new TransactionDetails(paymentType, paymentMode,  transactionAmount, cardCharge, isTransactionCommitted));
   },

   updateTransAmtTransactionDetails : function(index, transactionAmount)
   {
      transactionDetails = this.transactionDetailsMap.get(index);
      var paymentType = transactionDetails.paymentType;
      var cardCharge = this.getCardCharge(paymentType, transactionAmount);
      transactionDetails.setTransactionAmount(transactionAmount);
      transactionDetails.setCardCharge(cardCharge);
      this.transactionDetailsMap.put(index, transactionDetails);
   },

   updateTransactionDetails : function(index, isTransactionCommitted)
   {

      transactionDetails = this.transactionDetailsMap.get(index);
      transactionDetails.setIsTransactionCommitted(isTransactionCommitted);
      this.transactionDetailsMap.put(index, transactionDetails);
   },

   updateTransactionDetailsWithCardCharge : function(index, cardCharge)
   {
	  transactionDetails = this.transactionDetailsMap.get(index);
	  transactionDetails.cardCharge = cardCharge;
	  this.transactionDetailsMap.put(index, transactionDetails);
   },

   getCardCharge : function(paymentType, amount)
   {
      //If amount is not a number then return 0 as card charge
      if(isNaN(1*amount))
      {
         return 0.0;
      }

      //Calculate card charge for the amount provided
      var chargePercent = this.getCardChargeMap().get(paymentType);

      var cardCharge = MathUtils.roundOff((1*amount * (chargePercent/100)),2);
      if (this.maxCardChargeAmount != null)
      {
         cardCharge = (cardCharge > 1*this.maxCardChargeAmount) ? 1*this.maxCardChargeAmount : cardCharge;
      }
      return cardCharge;
   },


   accumulateVoucherAmount : function(obj, voucherIndex, index)
   {
      var max = vocIndex[index];
      var value= obj.value;
      // this block is to remove the currency symbol if any exist in the field
      if(!(value.indexOf(PaymentDetailsContent.currencySymbol) == -1))
	  {
	     value = StringUtils.stripFirstChar(value);
	  }
	  if(StringUtils.isNotEmpty(value))
	  {
         var validationResult = FormValidator.validateField(obj,'',voucherIndex);
	     if (StringUtils.isNotEmpty(validationResult.errorMsg))
	     {
	        PaymentView.showAlert(validationResult.errorMsg,validationResult.element);
            return PaymentView.refreshTheField(validationResult.element);
	     }
	  }
      var  totalVocAmount= 0;
      var vocAmount = 0;
	  for (i=1; i<max; i++)
      {
         if ($("#voucherAmount_"+index+"_"+i) && StringUtils.isNotBlank($("#voucherAmount_"+index+"_"+i).val().replace(PaymentDetailsContent.currencySymbol, "")))
         {
            if ($("#voucherAmount_"+index+"_"+i).val().indexOf(PaymentDetailsContent.currencySymbol)!=-1)
            {
               vocAmount = $("#voucherAmount_"+index+"_"+i).val().replace(PaymentDetailsContent.currencySymbol, "");
            }
            else
            {
               vocAmount = $("#voucherAmount_"+index+"_"+i).val().replace(PaymentDetailsContent.currencySymbol, "");
            }
            $("#voucherCarry"+index+"_"+i).val(vocAmount);
            if (vocAmount.length > 0)
               totalVocAmount+=parseFloat(StringUtils.stripChars(vocAmount,PaymentDetailsContent.currencySymbol));
         }
         this.totalVoucherValue =  parseFloat(totalVocAmount);
      }
	  $("#totalVoucherVal_"+index).html(PaymentDetailsContent.currencySymbol + MathUtils.roundOff(this.totalVoucherValue,2));
   }
};
/** End of Payment Info.*/

/** NoOfPaymentsHandler handles all the processing required to display the payment sections(skeleton
 * ie without the inner content).*/
 /** End of NoOfPaymentsHandler */
 
 /**=====================TransactionDetails=========================================================*/
/*
 * isTransactionCommitted : true or false ,
 */
function TransactionDetails(paymentType, paymentMode,  transactionAmount, cardCharge, isTransactionCommitted)
{
   this.paymentType = paymentType;
   this.paymentMode = paymentMode;
   this.transactionAmount = transactionAmount;
   this.cardCharge = cardCharge;
   this.isTransactionCommitted = isTransactionCommitted;
   this.setPaymentType = function (paymentType)
   {
      this.paymentType = paymentType;
   };
   this.setTransactionAmount = function(transactionAmount)
   {
      this.transactionAmount = transactionAmount;
   };
   this.setIsTransactionCommitted = function(isTransactionCommitted)
   {
      this.isTransactionCommitted = isTransactionCommitted;
   };
   this.setCardCharge = function(cardCharge)
   {
      this.cardCharge = cardCharge;
   }
}
/**=====================End of TransactionDetails=========================================================*/
 
/** NoOfPaymentsHandler */
/**=====================NoOfPaymentsChangeHandler==================================================*/
/**
 * Handler for NoOfPayments drop down change
 */
var NoOfPaymentsChangeHandler =
{
   /** Holds NoOfPayments selection. */
   existingSelection:0,

   /**
    *once payment is taken, we can not change no of payments for non Dcard payments.
    *This variable should be set to false in that case.
    */
   isNoOfPaymentsChangeAllowed:true,

   /**
    * Function to handle the change in NoOfPayments drop down to increase or decrease,
    * no of payment transactions
    *
    * @param newSelection - selected value in drop down.
    */
   handle:function(newSelection)
   {
      //If selection is not allowed(e.g- booking is done in case of non-dcards)
	  //or existingSelection is equal to newSelection then no action should take place.
      if(!this.isNoOfPaymentsChangeAllowed || (this.existingSelection==newSelection))
      {
	     PaymentView.showAlert("This action is not allowed.", $("#noOfPayments"));
	     $("#noOfPayments").val(this.existingSelection);
         return false;
      }

      //Remove existing payment transaction sections
      PaymentView.removePaymentTransactionSections();

      this.existingSelection = newSelection;

      //Add new PaymentTransactionSections
      for(var index=newSelection-1;index>=0;index--)
      {
         var obj = PaymentDetailsContent.paymentSectionTemplate(index,newSelection);
         $(obj).insertAfter("#noOfPaymentsBlock");
	     this.disableLastTransAmount();
      }

      // Update payment info
      PaymentInfo.refresh();
      PaymentView.showCardChargeInSummaryPanel(0);

      //Update payment page with changed amounts
      PaymentView.updatePaymentSection(PaymentInfo);
      PaymentView.updateLastTransactionAmountField(PaymentInfo.getPayableAmount());

   },

   disableLastTransAmount : function()
   {
      $(".transAmt:last").attr("disabled", "disabled");
	  $(".paidAmount").attr("disabled", "disabled");
	  $(".amountStillDue").attr("disabled", "disabled");
	  $(".calculatedPayableAmount").attr("disabled", "disabled");
   },

   /**
    * Refreshes the instance variables in NoOfPaymentsChangeHandler
    */
   refresh : function()
   {
      this.existingSelection = 0;
      PaymentView.setDropDownsToDefault(document.getElementById("noOfPayments"));
      this.isNoOfPaymentsChangeAllowed = true;
   }
};
/** Endo of NoOfPaymentsHandler*/