var lv_paymentType = null;


/**
 * Handler for change to the payment amount selection. This is called
 * when the user changes from, for example, deposit payment to full balance
 * payment.
 */
function lf_onChangePaymentAmountType(lv_paymentTypeIn)
{
   refreshToDefault();
   lv_paymentType = lv_paymentTypeIn;
   lf_updateTotals(lv_paymentTypeIn);
}


function If_showReaccreditationData()
{
  if(document.getElementById("consultantId")!=null && document.getElementById("consultantId").value == "")
  {
   document.getElementById("authenticationUserID").value ="";
   document.getElementById("authenticationPassword").value="";
   document.getElementById("authenticationUserID").disabled=true;
   document.getElementById("authenticationPassword").disabled=true;
  }
  else
  {
  document.getElementById("authenticationUserID").disabled=false;
  document.getElementById("authenticationPassword").disabled=false;
  }
}
/**
 * Updates the totals fields. These fields are the amount payable, the amount
 * paid so far and the amount still due before the form can be submitted.
 */
function lf_updateTotals(lv_paymentTypeIn)
{
// alert("Updating totals");

   var amountPayable = 0;
   switch(lv_paymentTypeIn)
   {
      case '1': amountPayable = additionalPaymentOptionBalanceDue ; break;
      case '2': amountPayable = depositOptionBalanceDue; break;
      case '3': amountPayable = fullBalanceAmount;  break;
       case '4': amountPayable = refundPaymentOptionBalanceDue; break;

   }

    if(isNaN(amountPayable))
   {
      amountPayable = "?";
      canCalc = false;
   }
   else
   {
      amountPayable = new Number(amountPayable).toFixed(2);
   }

    amtRec = amountPayable;
      selOption(1);
      displayAmt();
      depositAmout=amountPayable;
      //Makes an ajax call to DepositAmountServlet to update deposit amount
     // updateTransAmtForDepositAmountChange(value);
}


function general_returnToCheckpoint(clientDomainURL)
{
    var checkpoint_url =clientDomainURL+"/brac/page/amend/checkpoint.page";
   if (confirm("Are you sure you want to exit this flow?"))
   {
     window.location.replace(checkpoint_url);
    }
    return false;
}

function general_Testprint(clientDomainURL)
{
   var checkpoint_url;
	if(document.getElementById("printAtolLocally")!=null)
	{
checkpoint_url=clientDomainURL+"/brac/page/reports/testprintemail.page?"+"generateCertificate=true";
	}
	else{
checkpoint_url=clientDomainURL+"/brac/page/reports/testprintemail.page?"+"generateCertificate=false";
	}

	window.open(checkpoint_url);
}

/*Displays pop-up for the given URL
*@params popURL - URL of this popup
*        popW   - Width of this popup
*        popH   - Height of this popup
*        attr    -Any other attributes of popup like scrollbars etc.
*/
function Popup(popURL,popW,popH,attr)
{
   if (!popH) { popH = 350 }
   if (!popW) { popW = 600 }
   var winLeft = (screen.width-popW)/2;
   var winTop = (screen.height-popH-30)/2;
   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;

   popupWin=window.open(popURL,"popupWindow",winProp);
   popupWin.window.focus()
}