var currOption=0;
var vocIndex= new Array(8);
var amtLastDis=0;
var totamtdue=0;
var totamtduecardper = 0;
var isCollectPayment = true;
var isAdd = true;
var totalvouchers = 0;
var isOneTime = false;
var cashtotval=0;
var validatetrans = true;
var totAmtDedVal=0;
var totCardCharge=0;
discountVal =0;
var clientApplication='';
var discountType='';
/* following variable becomes false  when a payment is made */
var isPaymentNotMadeFlag=true;
var selectedDepositOption='';
var tranSactionSelected = 0;
var isRefund = false;
var isAdditional= false;
var radioCheckCount = 0;
var radioAlert = true;
var tranSactionSelected = 0;
var payableAmount = 0;
var payableAmountWithoutCardCharges = 0;
var isDepositRefund = false;

for(i=0;i<vocIndex.length;i++)
{
vocIndex[i]=0;
}


/*   */
function onLoadZeroPaymentHandler()
{
   $("payment_type_header").style.display="none";
   $("payment_trans_txt").style.display="none";
   $("movepaymentrans").style.display="none";
   $("movepaymentrans").innerHTML="";
   $("paymentSelections").style.display="none";
   $("hoplaDisplay").innerHTML=paymentConfigHoplaDisplay(0,payment_PPG);
   $('paymentFields_required').innerHTML+=requiredFields_ppg;
   $("paymentAccumlation").innerHTML=accumlatedAmtContent(0);
   var amountDisp=0.00;
   amountDisp=currencySymbol+amountDisp.toFixed(2)
   $("totamtpaid").value=amountDisp
   $("amtstilltxt").value=amountDisp
   $("totamtduetxt").value=amountDisp
   $("totamtpaid").disabled=true;
   $("amtstilltxt").disabled=true;
   $("totamtduetxt").disabled=true;
}


/*   */
function onLoadPaymentHandler()
{
	//Condition checks based on the datacashEnabled attribute.
   if (callType=='callCenter')
   {
      $("cp").disabled=true;
      $("cnp").checked='true';
      setCustomerType('payment_CNP');
      $("cnp").enabled=true;
      $("cpna").disabled=true;
      return false;
   }
   if ($("datacashEnabled").value == "false")
   {
      if(callType=='callCP'||callType=='Retail')
      {
         $("cp").checked='true';
         setCustomerType('payment_CP');
         $("cp").disabled=true;
         $("cnp").disabled=true;
         $("cpna").disabled=true;
         return false;
      }
   }
   else
   {
      if (callType =='callCenter')
      {
         $("cp").disabled=true;
         $("cnp").checked='true';
         setCustomerType('payment_CNP');
         $("cnp").enabled=true;
         $("cpna").disabled=true;
         return false;
      }
      if(callType =='Retail')
      {
         $("cp").enabled=true;
         $("cnp").checked='true';
         setCustomerType('payment_CNP');
         $("cnp").enabled=true;
         $("cpna").enabled=true;
         return false;
      }
   }
   noOfPaymentsHandler(1);
   displayAmt();
}

/* Function to call deposit amount servlet with selected radio button value.
*
*/
function initializeDepositSelection()
{
   var depositsTypes = document.getElementsByName("depositType");
   if(depositsTypes && depositsTypes !=undefined && depositsTypes !=null)
   {
      for(var i=0;i<depositsTypes.length; i++)
      {
         if(depositsTypes[i].checked)
         {
            setSelectedOption(depositsTypes[i].value);
         }
      }
   }
}

/*    */
function setCustomerType(customerType)
{
   var panelType="";
   $('paymentFields_required').innerHTML='';
   if(customerType=='payment_CP')
   {
      paymenttypeoptions=payment_CP;
      panelType="CP";
      $('paymentFields_required').innerHTML=requiredFields_cp+"<input type='hidden'  id='panelType' value='"+panelType+"'/>";
   }
   else if(customerType=='payment_CNP')
   {
      paymenttypeoptions=payment_CNP;
      panelType="CNP";
      $('paymentFields_required').innerHTML=requiredFields_cnp+"<input type='hidden'  id='panelType' value='"+panelType+"'/>";
   }
   else
   {
      paymenttypeoptions=payment_CPNA;
      panelType="CPNA";
      $('paymentFields_required').innerHTML=requiredFields_cpna+"<input type='hidden'  id='panelType' value='"+panelType+"'/>";
   }
   if(isHopla && requiredFields_ppg !=null && requiredFields_ppg != '')
   {
      paymenttypeoptionsHopla=payment_PPG;
      $('paymentFields_required').innerHTML+=requiredFields_ppg;
   }
   if ($("pinpadMsgDetails"+trans_Index) && $("pinpadCardDetails"+trans_Index))
   {
		if ($("pinpadMsgDetails"+trans_Index).innerHTML != '' && $("pinpadCardDetails"+trans_Index).innerHTML != '')
		{
			$("pinpadMsgDetails"+trans_Index).innerHTML = "";
			$("pinpadCardDetails"+trans_Index).innerHTML = "";
		}
   }
   if (!radioAlert && (panelType == 'CP' || panelType == 'CPNA'))
    radioAlert = true;
   refreshToDefault();
   noOfPaymentsHandler(1);
   assignPaymentMethods(1);
   //Don't remove this. it is necesary for CNP
   refreshToDefault();
}

/* This function is used to set payment panel to default display. it is usually called from
*   setCustomerType() function,  when user switches among CP, CNP or CPNA
 */
function refreshToDefault()
{
   noOfTrans=$("paymentTrans").value;
   intialUpdatePaymentInfo();
   updateTotalAmtInSummaryPanel(PaymentInfo.totalAmount);
   refreshToDefaultForCNP();
   if(isAdditional && noOfTrans == 1)
   {
      $("transamt0").disabled = false;
   }
   for(i=0;i<noOfTrans;i++)
   {
      if ($("cardRefundArea"+i))
      {
         radioMaxCount =0;
         radioCheckCount=0;
         $("cardRefundArea"+i).style.display = "none";
      }
      if($("format"+i))
      {
         $("format"+i).innerHTML='';
      }
      if($("cardCharges"+i))
      {
         $("cardCharges"+i).innerHTML='';
      }
   }
   if($("payment_type_0"))
   {
      $("payment_type_0").selectedIndex=0;
      $("payment_type_0").disabled=false;
   }
   selOption(1);
   if(isHopla && requiredFields_ppg !=null && requiredFields_ppg != '' )
   {
      $("formatHopla").innerHTML='';
   }
}

/* function to be called when values have to  be refreshed when CNP
*  is selected
*/
function refreshToDefaultForCNP()
{
   var panelType=$("panelType").value;
   //If CNP then  fresh start
   if(panelType=='CNP')
   {
      newSelectedOption=$("paymentTrans").value;
      //If existing no of payments option is less than newly Selected Option then subtract card charges.
      isPaymentNotMadeFlag=true;
      if($("totamtpaid"))
      {
         $("totamtpaid").value=currencySymbol+"0.00";
         $("totamtpaid_carry").value="0.00";
         $("amtstilltxt").value=currencySymbol+parseFloat((amtRec>0)?amtRec:0.0).toFixed(2);
      }
   }
}

function refreshToDefaultDepositOptions(calculatedPayableAmount)
{
   if($("totalholidaycost"))
   {
      var val = stripChars($("totalHolidayCostH").value,currencySymbol+''+','+' ');
      if(calculatedPayableAmount ==0)
      {
         calculatedPayableAmount = (parseFloat(val).toFixed(2));
      }
      $("totalholidaycost").innerHTML = calculatedPayableAmount;
   }
   if($("lowdeposit"))
   {
      var val = stripChars($("lowDepositH").value,currencySymbol+''+','+' ');
      val = (parseFloat(val).toFixed(2));
      $("lowdeposit").innerHTML = val;
   }
   if($("deposit"))
   {
      var val =  stripChars($("depositH").value,currencySymbol+''+','+' ');
      val = (parseFloat(val).toFixed(2));
      $("deposit").innerHTML = val;
   }
   if($("fullCost"))
   {
      var val = stripChars($("totalHolidayCostH").value,currencySymbol+''+','+' ');
      if(calculatedPayableAmount ==0)
      {
         calculatedPayableAmount = (parseFloat(val).toFixed(2));
      }
      $("fullCost").innerHTML = calculatedPayableAmount;
   }
}

function getTrsancationThreshold()
{
   var panelType=$("panelType").value;
   for(i=currOption ;i>0;i--)
   {
      if($("chkbox"+i) && $("chkbox"+i).checked)
      {
         return i;
      }
      else if(panelType=='CNP'&&isPaymentNotMadeFlag==false )
      {
         return i;
      }
   }
   return 0;
}

function getTrsancationThresholdCNP()
{
   var panelType=$("panelType").value;
   for(i=currOption ;i>0;i--)
   {
      if(panelType=='CNP'&&$('payment_type_'+i) && $('payment_type_'+i).selectedIndex>0)
      {
         return false
      }
   }
   return true;
}

function getNode(option)
{
   return $("paymentSelections");//:$("transactionId_"+(currOption)));
}

function allowChangeNoOfPayments(value)
{
   for (i=0;i<currOption;i++ )
   {
      if (($("chkbox"+i) && $("chkbox"+i).checked) || ((($("payment_type_"+i).value).indexOf("CardPinPad ")!= -1) && paymentStatus))
      {
      return false;
      }
   }
   return true;
}

function noOfPaymentsHandler(value)
{
   var allowChange = allowChangeNoOfPayments(value);
   PaymentInfo.totalTransactions = $("paymentTrans").value;
   if($('error0'))
	     $('error0').style.display="none";
	  if(PaymentInfo.totalTransactions > 1)
	  {
	    if($('authcodeText0'))
	    {
	      $('authcodeText0').style.display="none";
	      $('payment_0_authCodeId').style.display="none";
	    }
	  }
   if(allowChange)
   {
      refreshToDefault();
      //Check for CNP
      // refreshToDefaultForCNP();
      selOption(value);
   }
   else
   {
      alert("This action is not allowed");
      $("paymentTrans").selectedIndex = currOption-1;
   }
}


//Assign payment methods to select box when user clicks a radio button
function assignPaymentMethods(obj)
{
   var index=$("paymentTrans").value;
   var op="<option value=''>Please Select</option>" ;
   for(var i=0;i<index;i++)
   {
      if($("transactionId_"+i)!=undefined)
      {
         $("selectBox"+i).innerHTML='';
         $("selectBox"+i).innerHTML=paymentTypeDropDownDisplay(i,paymenttypeoptions);
      }
   }
}

/* This function is called when user selects "Please Select" option in payment Method DropDown
 * This function changes the panel to default for this transaction
 * @param paymentMethodDropDown- Select box object(contains payment methods) which called this function
                paymentMethodValue- Selected option value of the select box
                index- index of this transaction(transaction index begins at zero)
*/
function updateForInitialSelection(paymentMethodDropDown,paymentMethodValue,index)
{
   var noOfTransaction = $("paymentTrans").value;
   var transamt='';
   var disabledDisplay=true;
   if($("paymentTrans").value == 1)
   {
      //This has been commented out because the transaction amount was coming as undefined
      //when the user selects any one of the payment type in the drop down and goes back to "please select" option.
      //transamt = (!($("chkbox"+(currOption-1)) && $("chkbox"+(currOption-1)).checked))?$("transamt"+(currOption-1)).value:currencySymbol;
      transamt=$("transamt"+(currOption-1)).value;
      transamt = stripChars(transamt,currencySymbol);
      disabledDisplay = true;
   }
   //If it is last transaction
   else if(noOfTransaction-1 == index)
   {
      transamt = $("transamt"+index).value;
      disabledDisplay = true;
   }
   else
   {
      transAmtField = $("transamt"+index);
      transAmtField.disabled=false;
      updateLastTransactionAmount();
      disabledDisplay = false;
   }
   handleCardSelection(index);
   $("innerContainer"+index).innerHTML = noPaymentMethodSelectedDisplay(index,transamt);
   $("transamt"+index).disabled=disabledDisplay;
   //Ajax Call to deduct previous card charge and update with card charge for this selection
   showPayDetails();
}

/* This function is called when user selects a payment method in select box
 *  this function calls different functions which are necessary for rendering payment panel
 * based on payment method selection
 * @param paymentMethodDropDown- Select box object(contains payment methods) which called this function
                paymentMethodValue- Selected option value of the select box
                index- index of this transaction(transaction index begins at zero)
*/
var count = 0;
function paymentPanelController(paymentMethodDropDown,paymentMethodValue,index)
{
   var panelType=$("panelType").value;
   PaymentInfo.currentIndex = index;
   var payTypeArray;
   if (paymentMethodValue.indexOf("|") != -1)
   {
      payTypeArray = paymentMethodValue.split("|");
      PaymentInfo.selectedCardType = trimSpaces(payTypeArray[0]);
      PaymentInfo.selectedType = trimSpaces(payTypeArray[1]);
   }
   if(checkFields(paymentMethodDropDown,paymentMethodValue,index))
   {
      if ($("clientApplication") && ($("clientApplication").value).indexOf("BRAC") != -1)
      {
         if (paymentMethodDropDown.value.indexOf('CardRefund') !=-1)
            ++radioCheckCount;
         if (paymentMethodDropDown.value.indexOf('CardRefund') !=-1 && (radioMaxCount == 0 || radioCheckCount <= radioMaxCount))
         {
            generateTransactionRadios(index);
         }
         else if ((radioMaxCount != 0 && radioCheckCount > radioMaxCount) && paymentMethodDropDown.value.indexOf('CardRefund') !=-1)
         {
            alert("All transactions with refund were selected. Please try with other payment modes.");
            if ($("cardRefundArea"+index))
            {
               $("cardRefundArea"+index).innerHTML = "";
               $("cardRefundArea"+index).style.display = "none";
            }
            --radioCheckCount;
            paymentMethodDropDown.selectedIndex = 0;
         }
      }
      //When your selection is default option( i.e. 'Please Select' option in paymentMethodDropDown)
      if(paymentMethodDropDown.selectedIndex == 0)
      {
         if ($("pinpadMsgDetails"+index) && $("pinpadCardDetails"+index))
         {
            if ($("pinpadMsgDetails"+index).innerHTML != '' && $("pinpadCardDetails"+index).innerHTML != '')
            {
               $("pinpadMsgDetails"+index).innerHTML = "";
               $("pinpadCardDetails"+index).innerHTML = "";
            }
         }
         $('cancelDiv'+index).innerHTML = '';
         //This has been commented out to fix the script error which was coming when the selection of the
         //card type is switched to default option ie. please select when more than one transactions is selected.
         //refreshToDefault();
         updateForInitialSelection(paymentMethodDropDown,paymentMethodValue,index);
      }
      else //When you select a payment method from drop down
      {
         if (panelType =='CP' && ($("pinpadMsgDetails"+index) && $("pinpadCardDetails"+index)))
         {
            $("pinpadMsgDetails"+index).style.display = "none";
            $("pinpadCardDetails"+index).style.display = "none";
         }
         if (panelType =='CP' && (paymentMethodDropDown.value.indexOf('CardPinPad') != -1 && paymentMethodDropDown.value.indexOf('GiftCard') != -1))
         {
            $('cancelDiv'+index).innerHTML = '';
         }
         var type = processSelctedPaymentType(paymentMethodDropDown,index);
         if(paymentMethodDropDown.value.indexOf('NoPayment') !=-1 && PaymentInfo.selectedCardType.indexOf('RR') == -1)
         {
            $("transamt"+index).value=currencySymbol+"0";
            $("transamt_carry"+index).value=0;
            $("tottransamt"+index).innerHTML=0;
            $("total_amount_"+index).value=0;
            updateForInitialSelection(paymentMethodDropDown,paymentMethodValue,index);
            $("transamt"+index).value=currencySymbol+"0";
            $("transamt_carry"+index).value=0;
            $("tottransamt"+index).innerHTML=0;
            $("total_amount_"+index).value=0;
            selectedPaymentType(paymentMethodDropDown,index);
            showPayDetails();
         }
         else
         {
            handleCardSelection(index);
            adjustTransAmount(index);
            selectedPaymentType(paymentMethodDropDown,index);
            cardPerAmt(paymentMethodValue,index);
            if (panelType =='CP' && (trimSpaces(type[1]) != 'CardPinPad' && trimSpaces(type[1]) != 'GiftCard'))
            {
               $('cancelDiv'+index).innerHTML = '';
            }
            if (noOfTrans == 1)
            {
               if(!(panelType =='CP' || (panelType=='CPNA' && trimSpaces(type[1]) != 'Dcard') || (panelType=='CNP' && trimSpaces(type[1]) == 'CardRefund')))
               {
                  collectPayment(index);
               }
            }
            //Collect the payment if selected payment type is Dcard
            else if(!(panelType =='CP' || (panelType=='CPNA' && trimSpaces(type[1]) != 'Dcard') || (panelType=='CNP' && trimSpaces(type[1]) == 'CardRefund')) && type[1]!= undefined && $("transamt"+index).disabled==true )
            {
               collectPayment(index);
            }
         }
      }//End of else part
   }
   else
   {
      paymentMethodDropDown.selectedIndex = 0;
      alert("Please select transaction fields in sequential order!");
   }
   if($('error0'))
	      $('error0').style.display="none";

}

/* When payment method changes from "save no refund" to some other payment method
* We have to update transamount and total transaction amount fields
* @param index- index of this transaction(transaction index begins at zero)
*/
function adjustTransAmount(index)
{
   var noOfTransaction = $("paymentTrans").value;
   //exit from this function, if transaction amount is not 0
   if($("total_amount_"+index).value !=0)
   {
      return false;
   }
   //If there is only one transaction
   if(index ==0 && noOfTransaction==1)
   {
      var totamtduetxt = $("totamtduetxt").value;
      $("transamt"+index).value=totamtduetxt;
      $("tottransamt"+index).innerHTML=totamtduetxt;
      $("total_amount_"+index).value=totamtduetxt;
   }
   //for last transaction
   else if(index>0 && index == noOfTransaction-1 )
   {
      var totamtduetxt = $("totamtduetxt").value;
      if(isAdditional)
      {
      $("transamt"+index).disabled = false;
         $("transamt"+index).value ="";
         $("tottransamt"+index).innerHTML="";
         $("total_amount_"+index).value="";
      }
      else
      {
         totamtduetxt = parseFloat(stripChars(totamtduetxt,currencySymbol)-collectAllTotalAmount()).toFixed(2);
         $("transamt"+index).value=totamtduetxt;
         $("tottransamt"+index).innerHTML=totamtduetxt;
         $("total_amount_"+index).value=totamtduetxt;
      }
   }
   //If the transaction is not last and there are more than one transaction
   else
   {
      $("transamt"+index).value=currencySymbol;
      $("tottransamt"+index).innerHTML=currencySymbol;
      $("total_amount_"+index).value='';
   }
}

/**
*/
function updateLastTransactionAmount()
{
   var totalTransValue = 0;
   noOfTransactions = $("paymentTrans").value
   lastIndex = noOfTransactions-1;

   for (var i = 0; i < noOfTransactions; i++)
   {
      var isSelectedArray = $("payment_type_"+i).value.split("|");
      var isSelected = isSelectedArray[1];
      if (i != lastIndex && $("transamt"+i).value != "" && selectedTypeIndexCRData != "CardRefund")
      {
         totalTransValue += 1*stripChars($("transamt"+i).value,currencySymbol+''+','+' ');
      }
   }
   //The last transaction amount was not being updated correctly for refund transactions.This has been added to update the amount correctly.
   if (isRefund && totalTransValue > 0)
   {
      totalTransValue = 0 - Math.abs(totalTransValue);
   }
   if (Math.abs(totalTransValue) < Math.abs(PaymentInfo.basePayableAmount))
   {
      $("transamt"+lastIndex).value = currencySymbol+parseFloat(1*PaymentInfo.basePayableAmount - 1*totalTransValue).toFixed(2);
      $("transamt_carry"+lastIndex).value = parseFloat(1*PaymentInfo.basePayableAmount - 1*totalTransValue).toFixed(2);
      $("tottransamt"+lastIndex).innerHTML = currencySymbol+parseFloat(1*PaymentInfo.basePayableAmount - 1*totalTransValue).toFixed(2);
   }
}


/* This function is called when user enters some value in transamt field(the field where transaction amount is entered  */
/*@param index represents transaction index(index starts with 0)
*@param transEnt  represents total no of transactions
*@param element represents object which is calling this method
*/
function transamtHandler(index,transEnt,element)
{
  if (checkCardRefundRadio(index))
   {
    var panelType=$("panelType").value;
      var type = processSelctedPaymentType($("payment_type_"+index),index);
      if(type[1]!= undefined || type[1] != null)
      {
         if(trimSpaces(type[1]) == 'NoPayment' && stripChars(element.value,currencySymbol) != 0 && !isRefund)
         {
            element.value == currencySymbol+ 0.0;
            setFocus("Save only transaction amount should be zero",element);
         }//CARD_REFUND
         else if(trimSpaces(type[1]) =='CardRefund' && ((parseFloat(1*stripChars(element.value,currencySymbol))) > (parseFloat(1*getCheckedRadioValue(index)).toFixed(2))))
         {
            element.value = currencySymbol;
            SetCursorToTextEnd(element);
            setFocus("Amount should not exceed transaction amount.",element);
         }
         else if(panelType=='CP' ||(panelType=='CPNA' && trimSpaces(type[1]) != 'Dcard'))
         {
            if(!isAdditional)
            updateLastTransactionAmount();
            amtEnter(index,transEnt,element);
			/*if (panelType=='CPNA' && trimSpaces(type[1]) == 'CardRefund')
			{
	            collectPayment(index); //changed as part of refactoring.
			}*/
         }
         else if($('payment_type_'+index).selectedIndex>0)
         {
            if(!isAdditional)
            updateLastTransactionAmount();
            amtEnter(index,transEnt,element);
            //Collect the payment if selected payment type is Dcard
            collectPayment(index);
         }
      }
      else
      {
         alert("Please select a Payment method");
         element.value=currencySymbol;
        // element.focus();
         return false;
      }
   }
   else
   {
      element.value=currencySymbol;
      alert("Please check the transaction radio before proceeding.")
   }
}

/*function to create payment divisions based on no of payment selected
* @param obj represents no of selections made
*/
function selOption(obj)
{
   if (checkUpdateTrue() && $("preTrans"))
   {
      $("paymentTrans").selectedIndex = $("preTrans").value;
      return;
   }
   try
   {
      //To display the amount in the updated transaction
      $("paymentTrans").selectedIndex = (obj-1);
      $("preTrans").value = (obj-1);
      if (getTrsancationThreshold()<obj)
      {
         var transamt_last = 0;
         if($("transamt"+(currOption-1)))
            if(!isAdditional)
            {
               transamt_last = (!($("chkbox"+(currOption-1)) && $("chkbox"+(currOption-1)).checked))?$("transamt"+(currOption-1)).value:currencySymbol;
            }
            else
            {
               transamt_last=currencySymbol;
            }
            parentNode = $("paymentSelections"); //eval((currOption == 0)?$("paymentSelections"):$("paymentFor"+(currOption-1)));
            var paymentConfig="";
            if (obj > currOption)
            {
               for (var i=currOption;i<obj;i++)
               {
                  paymentConfig="";
                  var index=i;
                  if(obj>1 && i>=1)
                     paymentConfig+="<hr class='line_val'>";
                  //Get the display content from paymentPanelContent.js
                  paymentConfig+=paymentConfigDisplay(index,paymenttypeoptions);
                  chdNode = document.createElement("span");
                  chdNode.setAttribute("id", "trans_block_"+i);
                  chdNode.innerHTML=paymentConfig;
                  parentNode.appendChild(chdNode);
               }
            }
            else if(obj<currOption)
            {
               var transamt_last_val = 0;
               transamt_last_val=transamt_last;
               for(i=currOption;i>obj;i--)
               {
                  chNode=$('trans_block_'+(i-1));
                  var val =$("paymentSelections").removeChild(chNode);
                  validatetrans = true;
               }
               if(!($("chkbox"+(obj-1)) && $("chkbox"+(obj-1)).checked))
               {
                  transamt_last=currencySymbol+transamt_last_val;
                  $("transamt"+(obj-1)).disabled=false;
               }
            }
            else
            {
               chdNode = document.createElement("span");
               chdNode.innerHTML=paymentConfig;
               parentNode.appendChild(chdNode);
            }
            if (validatetrans)
            {
               if (obj>1)
               {
                  amtDisplay(obj,transamt_last);
                  isOneTime = true;
               }
               else
               {
                  if(obj== 1)
                     displayAmt();
               }
            }
            currOption=obj;
            if(isHopla && requiredFields_ppg !=null && requiredFields_ppg != '')
            {
               if($("formatHopla"))
               {
                  $("formatHopla").innerHTML='';
               }
               var hoplaIndex=obj;
               var paymentConfigHopla="<hr class='line_val'>"+ paymentConfigHoplaDisplay(hoplaIndex,paymenttypeoptionsHopla);
               $("hoplaDisplay").innerHTML=paymentConfigHopla;
            }
            addAccumlation(obj);
            totalvouchers = 0;
      }
      else
      {
         alert("This action is not allowed");
         $("paymentTrans").selectedIndex = currOption-1;
      }
   }
   catch (ex)
   {
      //do nothing
      alert("error"+ex);
   }
   showPayDetails();
}
/***********************End of Controller for payment page*********************************************/

/*****************************************************************************/


 /* returns an Array containing
  * selVal e.g- BQ,VISA_DEBIT, SWITCH etc.
  * selValEnumValue   e.g-Cheque, Card, Cash etc
  * securityCodeLength
  * selValCardCharge
  *@param currVal- it is an object of "payment type drop down"
          transVal- index of this particular transaction
*/
function processSelctedPaymentType(currVal,transVal)
{
   var selectedIndex = currVal.selectedIndex;
   var selValArr=currVal.options[selectedIndex].value;
   //Contains payment method type(e.g-Cheque, Card, Cash etc)
   selValEnumValue=selValArr.split("|")[1];
   //Contains payment method code(e.g- BQ,VISA_DEBIT, SWITCH etc)
   selVal=selValArr.split("|")[0];
   selVal=trimSpaces(selVal);
   var selValCardCharge =0.0;
   if($(selVal+"_charges"))
   {
      selValCardCharge= (selVal.length >0)?parseFloat($(selVal+"_charges").value):0.0;
   }
   securityCodeLength = 0 ; //Default value
   if($(selVal+"_securityCodeLength"))
   {
      securityCodeLength=$(selVal+"_securityCodeLength").value;
   }
   var paymentRequiredValues=new Array();
   //paymentRequiredValues.push(selVal,selValEnumValue,selValCardCharge
   paymentRequiredValues.push(selVal);
   paymentRequiredValues.push(selValEnumValue);
   paymentRequiredValues.push(securityCodeLength);
   paymentRequiredValues.push(selValCardCharge);
   return paymentRequiredValues;
}

/* Controls the Display when a payment method is selected
 * e.g- When cash payment is selected, the display  will be cash (payment) specific
   @param currVal- it is an object of "payment type drop down"
   @param transVal- index of this particular transaction
*/
function selectedPaymentType(currVal,transVal)
{
   //Gets type of radio button selection.  Display depends on  this variable
   var panelType =$("panelType").value;
   vocIndex[transVal]=0;
   if (checkUpdateTrue())
   {
      currVal.selectedIndex = 0;
   }

   paymentvalues = processSelctedPaymentType(currVal,transVal);
   if (paymentvalues[0] != "PINPAD")
   {
      var selVal=paymentvalues[0]
      var selValEnumValue=paymentvalues[1];
      var securityCodeLength = paymentvalues[2];
      var selValCardCharge=  paymentvalues[3];
      var selValCode = selVal;
      if ($("pinpadMsgDetails"+transVal) && $("pinpadCardDetails"+transVal))
      {
         if ($("pinpadMsgDetails"+transVal).innerHTML != '' && $("pinpadCardDetails"+transVal).innerHTML != '')
         {
            $("pinpadMsgDetails"+transVal).innerHTML = "";
            $("pinpadCardDetails"+transVal).innerHTML = "";
         }
      }
   }
   else
   {
      var selVal=paymentvalues[0]
      var selValEnumValue=paymentvalues[1];
      var securityCodeLength = 3;
      var selValCardCharge=  configurableCardCharge;
      var selValCode = selVal;
   }
   var cardConfig=cardConfigDisplay(selVal,selValEnumValue,transVal);
   var cnpcardConfig=cnpcardConfigDisplay(selVal,selValEnumValue,transVal,securityCodeLength,requiredFields_startYear,requiredFields_expiryYear);
   var cashConfig=cashConfigDisplay(selVal,selValEnumValue,transVal);
   var chequeConfig=chequeConfigDisplay(selVal,selValEnumValue,transVal);
   var vouchConfig=vouchConfigDisplay(selVal,selValEnumValue,transVal,vocIndex);
   var saveOnlyNoRefundConfig= saveOnlyNoRefundConfigDisplay(selVal,selValEnumValue,transVal,panelType);

   var giftCardConfig=giftCardConfigDisplay(selVal,selValEnumValue,transVal,securityCodeLength);

   var pinPadCardConfig = pinPadCardConfigDisplay(selVal,selValEnumValue,transVal);

   var tnsVal=transVal;
   var formtTt="format"+transVal;
   for(var m=0;m<=transVal;m++)
   {
      if (transVal==tnsVal)
      {
         if(selValEnumValue != undefined)
         {
            selValEnumValue = trimSpaces(selValEnumValue.toUpperCase());

            if ($("cardRefundArea"+transVal))
            {
               radioMaxCount =0;
               radioCheckCount=0;
               $("cardRefundArea"+transVal).style.display = "none";
            }
            switch(selValEnumValue)
            {
               case 'CARD' :
                  $(formtTt).innerHTML=cardConfig;
               break;

               case 'CARDPINPAD' :
                  $(formtTt).innerHTML=pinPadCardConfig;
		          var startButtonArea = '';
		          if (isRefund)
		          {
		             startButtonArea += '<input type="button" name="start" value="Start" onclick="javascript:startRefundTransaction('+transVal+');setIndex('+transVal+')"/>';
		          }
		          else
		          {
		             startButtonArea += '<input type="button" name="start" value="Start" onclick="javascript:startTransaction('+transVal+');setIndex('+transVal+')"/>';
		          }
		          $('cancelDiv'+transVal).innerHTML = startButtonArea;
                  paymentStatus = false;
               break;

               case 'GIFTCARD' :
                  if (panelType == 'CNP' || panelType == 'CPNA')
                  {
                     $(formtTt).innerHTML=giftCardConfig;
                     if ($("cardCharges"+transVal).style.display == 'none')
                        $("cardCharges"+transVal).style.display = "block";
                     break;
                  }
                  else if (panelType == 'CP')
                  {
                     $(formtTt).innerHTML=pinPadCardConfig;
		             var startButtonArea = '';
		             if (isRefund)
		             {
		                startButtonArea += '<input type="button" name="start" value="Start" onclick="javascript:startRefundTransaction('+transVal+');setIndex('+transVal+')"/>';
		             }
		             else
		             {
		                startButtonArea += '<input type="button" name="start" value="Start" onclick="javascript:startTransaction('+transVal+');setIndex('+transVal+')"/>';
		             }
		             $('cancelDiv'+transVal).innerHTML = startButtonArea;
                     paymentStatus = false;
                     break;
                  }
               case 'DCARD' :
                  $(formtTt).innerHTML=cnpcardConfig;
                  if ($(selVal+"_startDateRequired").value == "true" || $(selVal+"_issueNo").value == "true")
                  {
                     $("debitcards"+transVal).style.display='block';
                  }
                     // To Remove the Mandatory Symbol for the postcode as it is not required for BRAC.
                  if($("payment_"+transVal+"_postCodeId"))
                  {
                     $("payment_"+transVal+"_postCodeId").alt ="";
                     $("postcodemandatory"+transVal).innerHTML="";
                  }
                  if ($("cardCharges"+transVal).style.display == 'none')
                     $("cardCharges"+transVal).style.display = "block";
               break;

               case 'VOUCHER' :
                  $(formtTt).innerHTML=vouchConfig;
                  vocIndex[transVal] = (vocIndex[transVal])+1;
                  if(panelType=="CNP")
                  {
                     $("chkboxcontainer"+transVal).style.display='none';
                  }
               break;

               case 'CASH' :
                  $(formtTt).innerHTML=cashConfig;
                  if (selVal.indexOf('VI')!= -1 || selVal.indexOf('SW')!= -1 || selVal.indexOf('CA') == -1)
                  {
                     $("chk_box_text").innerHTML="Payment Accepted";
                  }
               break;

               case 'CHEQUE' :
                  $(formtTt).innerHTML=chequeConfig;
                  if(panelType=="CNP")
                  {
                     $("chkboxcontainer_"+transVal).style.display='none';
                  }
               break;

               case 'NOPAYMENT' :
                  $(formtTt).innerHTML=saveOnlyNoRefundConfig;
                  if (selVal.indexOf('RR')!= -1)
                  {
                     if ($("chk_box_text"))
                     {
                       $("chk_box_text").innerHTML="Refund Cheque";
                     }
                  }
                  if ($('cardChargePercentage'+PaymentInfo.currentIndex))
                  {
                     $('cardChargePercentage'+PaymentInfo.currentIndex).style.display='none';
                  }
                  if ($('cardChargesText'))
                  {
                     $('cardChargesText').style.display='none';
                  }
                  if(panelType=="CNP")
                  {
                     $("chkboxcontainer"+transVal).style.display='none';
                  }
               break;

               case 'CARDREFUND':
                  $(formtTt).innerHTML=saveOnlyNoRefundConfig;
                  if ($("cardRefundArea"+transVal))
                  {
                     $("cardRefundArea"+transVal).style.display = "block";
                     $("cardRefundArea"+transVal).innerHTML = requiredFields_rfndCard;
                     if ($("cardCharges"+transVal).style.display == '' || $("cardCharges"+transVal).style.display == 'block')
                        $("cardCharges"+transVal).style.display = "none";
                  }
                  if(panelType=="CNP" || panelType=='CPNA')
                  {
                     $("chkboxcontainer"+transVal).style.display='none';
					 $("chkbox"+transVal).style.display = "none";
                  }
               break;

               default  :
               break;
            }
         }
      }
   }
   totalvouchers = 0;
}

 //For HOPLA
 /** function to display card details when a HOPLA payment method is selected
 @param currVal- it is an object of "payment type drop down"
                    transVal- index of this particular transaction
 */
function selectedPaymentTypeHoplaDisplay(currVal,transVal)
{
   if (checkUpdateTrue())
   {
      currVal.selectedIndex = 0;
   }
   paymentvalues = processSelctedPaymentType(currVal,transVal);
   var selVal=paymentvalues[0]
   var selValEnumValue=paymentvalues[1];
   var securityCodeLength = paymentvalues[2];
   var selValCardCharge=  paymentvalues[3];
   var selValCode = selVal;

   var cnpcardConfig=cnpcardConfigDisplay(selVal,selValEnumValue,transVal,securityCodeLength,requiredFields_startYear,requiredFields_expiryYear);

   //Display
   $("formatHopla").innerHTML=cnpcardConfig;
   $("chkboxcontainer"+transVal).style.display='none';
   $("securityCodeHelp"+transVal).style.display='block';
   $("postcodeText"+transVal).innerHTML="Post Code";

   if($("payment_"+transVal+"_postCodeId"))
   {
      $("payment_"+transVal+"_postCodeId").alt ="";
      $("postcodemandatory"+transVal).innerHTML="";
   }
   if (selVal=="SOLO"||selVal=="MAESTRO" )
   {
      $("debitcards"+transVal).style.display='block';
   }
   $("hangrightList"+transVal).className="hangright  hoplaStyle";
   $("payment_"+transVal+"_securityCodeId").className="hoplaSecurityCodeStyle smallField";
   $("hoplaText"+transVal).className="lessTopMargin";
}

/*Populates  "paymentAccumlation" div, which contains total amount paid , amount due etc.
*
*@param- transVal is index of this transaction
*/
function addAccumlation(transVal)
{
   if ($("paymentAccumlation").innerHTML!="") return 0;
   $("paymentAccumlation").innerHTML =accumlatedAmtContent(transVal);
}

/*-
*
*
*/
function displayAmt()
{
   totamtdue = amtRec;
   totAmtDedVal=amtRec;
   if (!($("chkbox0") && $("chkbox0").checked))
   {
      if(isAdditional)
      {
         $("transamt0").disabled = false;
      }
      else
      {
         $("transamt0").disabled=true;
         }
         $("transamt0").value=currencySymbol+amtRec;
         $("transamt_carry0").value=amtRec;
         $("tottransamt0").innerHTML=currencySymbol+amtRec;
         $("total_amount_0").value=amtRec;
         updateCardCharges(0);
      }
 }

/* This function is to display the amounts in the last payment type based on the transaction value
@param amtDis- No of transactions
             transamt_last- Amount to be displayed in last transaction field
*/
function amtDisplay(amtDis,transamt_last)
{
  if(amtDis>currOption && !($("chkbox"+(currOption-1)) && $("chkbox"+(currOption-1)).checked))
   {
      $("transamt"+(currOption-1)).value=currencySymbol;
      $("transamt_carry"+(currOption-1)).value='';
      $("tottransamt"+(currOption-1)).innerHTML=currencySymbol;
      $("cardCharges"+(currOption-1)).innerHTML='';
   }
   for (i=0;i<amtDis;i++ )
      if (!($("chkbox"+i) && $("chkbox"+i).checked))
         $("transamt"+i).disabled=false;
      i--;
   if ($("transamt"+(i-1)))
   {
      if(!($("chkbox"+(i-1)) && $("chkbox"+(i-1)).checked))
      {
         $("total_amount_"+i).value=transamt_last;
         if (!($("chkbox"+(i)) && $("chkbox"+(i)).checked))
         {
            $("transamt"+i).value=transamt_last;
            var totTransAmount = currencySymbol;
            if (transamt_last.length > 1)
            {
               totTransAmount+=parseFloat(transamt_last).toFixed(2);
               $("transamt"+i).disabled=true;
               updateCardCharges(i);
            }
            if($("tottransamt"+i))
            {
               $("tottransamt"+i).innerHTML=totTransAmount
            }
         }
      }
   }
}

/* Function is used to create card charges section. (Displays card charge for CP only until
*  Server call is returned
*
*/
function cardPerAmt(currVal,index)
{
   var panelType=$("panelType").value;
   var maxCardCharge=parseFloat(PaymentInfo.maxCardChargeAmount).toFixed(2);
   if(currVal!=null)
   {
      currVal=trimSpaces(currVal);
   }
   temp=currVal;
   if(temp!=null && temp.split("|"))
   {
      currVal=temp.split("|")[1];
      cp_currVal = temp.split("|")[0];
   }
   if ($("transamt"+index).value!="")
   {
      if (currVal == "Dcard" || currVal == "Card" || currVal == "GiftCard")
      {
         var totcaramt =   0;
         var cardChargeAmt = 0;
         totcaramt = Number($("transamt"+index).value);
         var cardChargesPercentage=0.0;
         if($(cp_currVal+"_charges"))
         {
            cardChargesPercentage=$(cp_currVal+"_charges").value;
         }
         var cardChargeAmt;
         if (!isRefund)
         {
            cardChargeAmt = calculateCardCharges(totcaramt);
            if(cardChargeAmt<0)
            {
               //If card charge amount is -ve, we need absolute value
               cardChargeAmt = Math.abs(cardChargeAmt);
            }
         }
         else
         {
            cardChargeAmt = 0.0
         }
         if(1*cardChargeAmt > 1*maxCardCharge)
         {
            cardChargeAmt = maxCardCharge;
         }
         totcaramtperval = parseFloat(totcaramt) + parseFloat(cardChargeAmt);
         //displays authcode section. applicable for CP only
         var authCodeDisplay = "";
         if (panelType == 'CP' && currVal != 'GiftCard')
         {
            authCodeDisplay = authCodeDisplayConfig(index);
         }
         //Displays cardCharge section for CP, CNP and CPNA
         var cardChargesDisplay = cardChargeSectionConfig(index,cardChargeAmt);
         if(panelType=='CNP'|| panelType=='CPNA')
         {
            //Display card Charge section only
            if($("cardCharges"+index).innerHTML=='' && $("cardCharges"+index).innerHTML.indexOf('charge')==-1)
            {
               $("cardCharges"+index).innerHTML =cardChargesDisplay;
               $('cardChargePercentage'+index).innerHTML = currencySymbol+parseFloat(cardChargeAmt).toFixed(2);
            }
         }
         else if (panelType == 'CP' && currVal != 'GiftCard')
         {
            //Display both auth code section and card charge section
            $("cardCharges"+index).innerHTML = authCodeDisplay + cardChargesDisplay;
            $('cardChargePercentage'+index).innerHTML = currencySymbol+parseFloat(cardChargeAmt).toFixed(2);
         }
         if ($("payment_"+index+"_chargeIdAmount"))
         {
            $("payment_"+index+"_chargeIdAmount").value = cardChargeAmt;
         }
         $("tottransamt"+index).innerHTML=currencySymbol+totcaramtperval.toFixed(2);
         $("total_amount_"+index).value=totcaramtperval.toFixed(2);
         totamtdue += cardChargeAmt;
      }
      else if (currVal == "Cash" || currVal =="Cheque" || currVal =="Voucher" )
      {
         var tranamtcrd = $("transamt"+index).value;
         $("tottransamt"+index).innerHTML=tranamtcrd;
         $("total_amount_"+index).value=tranamtcrd;
         $("cardCharges"+index).innerHTML="";
      }
   }
   if (panelType != 'CP' && panelType != 'CPNA' && currVal != 'CardPinPad')
   {
      showPayDetailsCR(index);
   }
}

/* This function is to update the amount in Total Transaction field */
/*@param amtEnt represents transaction index(index starts with 0)
*@param transEnt  represents total no of transactions
*@param element represents object which is calling this method
*/
function amtEnter(amtEnt,transEnt,element)
{
   var fieldAmt = element.value;
   var fieldVal = fieldAmt;
   var fieldValAmt = fieldAmt.replace(currencySymbol,"");
   var valSet = element.getAttribute("alt").split("|");
   var alertMsg = valSet[0];
   var type = valSet[1];
   var pat = /^[0-9]*[\.\{0,1}]?[0-9]*$/;
   var Pat1 = /^[0-9.]*$/;//Check for Numeric and dot
   var Pat2 = /^[0-9]+(\.\d{1,2})?$/;//Check for 2 optional decimals
   var selectedType = $("payment_type_"+amtEnt).value;
   var selectedTypeIndex = selectedType.split("|");

   if(fieldValAmt != "")
   {
      if (fieldValAmt.match(Pat1) && fieldValAmt.match(Pat2)&& fieldAmt.match(pat))
      {
         transEnt--;
         var amtVal = $("transamt"+amtEnt).value;
         var amt = amtVal;
         var changeAmt, maxLimit;
         if (!isRefund && selectedTypeIndex[1] != "CardRefund")
         {
            if(isAdditional)
            {
              if( !totalAmountEntered())
               {
                  alert(exceedalert);
                  element.value = currencySymbol;
                  return false;
               }
            }
            else
            {
               changeAmt = parseFloat(parseFloat($("amtstilltxt").value.replace(currencySymbol,""))-amt).toFixed(2);
               maxLimit = parseFloat(collectAllTotalAmount())+parseFloat(amt);
               if ($("tottransamt"+amtEnt).innerHTML.length > 1)
               {
                  maxLimit-=parseFloat($("tottransamt"+amtEnt).innerHTML);
               }
               if (changeAmt < 0 || maxLimit > parseFloat($("totamtduetxt").value))
               {
                  alert(exceedalert);
                  element.value = currencySymbol;
                  return false;
               }
            }
         }
         else if (isRefund && selectedTypeIndex[1] != "CardRefund")
         {
            changeAmt = parseFloat(parseFloat(Math.abs($("amtstilltxt").value.replace(currencySymbol,"")))-1*amt).toFixed(2);
            maxLimit = parseFloat(collectAllTotalAmount())+parseFloat(amt);
            if ($("tottransamt"+amtEnt).innerHTML.length > 1)
            {
               maxLimit-=parseFloat($("tottransamt"+amtEnt).innerHTML);
            }
            if (Math.abs(changeAmt) < 0 || maxLimit > Math.abs(parseFloat($("totamtduetxt").value)))
            {
               alert(exceedalert);
               element.value = currencySymbol;
               return false;
            }
         }
         else
         {
            changeAmt = parseFloat(parseFloat($("amtstilltxt").value.replace(currencySymbol,""))+1*amt).toFixed(2);
            maxLimit = parseFloat(collectAllTotalAmount())+parseFloat(amt);
            if ($("tottransamt"+amtEnt).innerHTML.length > 1)
            {
               maxLimit-=parseFloat($("tottransamt"+amtEnt).innerHTML);
            }
            if (Math.abs(changeAmt) < 0 || maxLimit > Math.abs(parseFloat($("totamtduetxt").value)))
            {
               alert(exceedalert);
               element.value = currencySymbol;
               return false;
            }
         }
         $("tottransamt"+amtEnt).innerHTML = amtVal;
         if (noOfTrans==1 && isAdditional)
         {
            $("transamt_carry"+transEnt).value = $("transamt"+amtEnt).value;
            //totAmtDedVal = currencySymbol +parseFloat(parseFloat($("totamtduetxt").value)-maxLimit).toFixed(2);
            totAmtDedVal = currencySymbol +parseFloat($("transamt"+transEnt).value).toFixed(2);
            var currSel = $("payment_type_"+amtEnt).options[$("payment_type_"+amtEnt).selectedIndex].value;
            $("tottransamt"+transEnt).innerHTML = totAmtDedVal;
            var currSelLast = $("payment_type_"+transEnt).options[$("payment_type_"+transEnt).selectedIndex].value;
            cardPerAmt(currSel,amtEnt);
            updateCardCharges(amtEnt);
         }
         else if (amtEnt != transEnt && !($("chkbox"+transEnt) && $("chkbox"+transEnt).checked))
         {
            var ccCharges = parseFloat(collectCharges(false)).toFixed(2);
            if (parseFloat(parseFloat($("totamtduetxt").value)-maxLimit-ccCharges).toFixed(2) < 0)
            {
               ccCharges =parseFloat(0);
            }
            // $("transamt"+transEnt).value = currencySymbol +parseFloat(parseFloat($("totamtduetxt").value)-maxLimit-ccCharges).toFixed(2);
            if (!isRefund && trimSpaces(selectedTypeIndex[1]) != "CardRefund")
            {
               if(isAdditional)
               {
                  var currSel = $("payment_type_"+amtEnt).options[$("payment_type_"+amtEnt).selectedIndex].value;
               }
               else
               {
                  $("transamt"+transEnt).value = currencySymbol +parseFloat(parseFloat($("totamtduetxt").value)-maxLimit).toFixed(2);
                  $("transamt_carry"+transEnt).value = $("transamt"+transEnt).value;
                  totAmtDedVal = $("transamt"+transEnt).value;
                  $("tottransamt"+transEnt).innerHTML=$("transamt"+transEnt).value;
                  var currSel = $("payment_type_"+amtEnt).options[$("payment_type_"+amtEnt).selectedIndex].value;
               }
            }
            else if (isRefund && selectedTypeIndex[1] != "CardRefund")
            {
               $("transamt"+transEnt).value = currencySymbol +parseFloat(parseFloat($("totamtduetxt").value)+maxLimit).toFixed(2);
               $("transamt_carry"+transEnt).value = $("transamt"+transEnt).value;
               totAmtDedVal = $("transamt"+transEnt).value;
               $("tottransamt"+transEnt).innerHTML=$("transamt"+transEnt).value;
               var currSel = $("payment_type_"+amtEnt).options[$("payment_type_"+amtEnt).selectedIndex].value;
            }
            else
            {
               $("transamt"+transEnt).value = currencySymbol +parseFloat(parseFloat($("totamtduetxt").value)+maxLimit).toFixed(2);
               $("transamt_carry"+transEnt).value = $("transamt"+transEnt).value;
               totAmtDedVal = $("transamt"+transEnt).value;
               $("tottransamt"+transEnt).innerHTML=$("transamt"+transEnt).value;
               var currSel = $("payment_type_"+amtEnt).options[$("payment_type_"+amtEnt).selectedIndex].value;
            }
         }
         var currSelLast = $("payment_type_"+transEnt).options[$("payment_type_"+transEnt).selectedIndex].value;
         cardPerAmt(currSel,amtEnt);
         cardPerAmt(currSelLast,transEnt);
         updateCardCharges(amtEnt);
         updateCardCharges(transEnt);
      }
      else
      {
         element.value = currencySymbol;
         alert(alertMsg);
         SetCursorToTextEnd(element);
         return false;
      }
   }
   return true;
}

//flag is set to true if omitting card charges which are part of unaccepted payment is reqd
function collectCharges(flag)
{
   var totalChargeAmountPaid = 0;
   for (i=0;i<currOption;i++)
   {
      var index =i;
      if(($("chkbox"+index) && $("chkbox"+index).checked && flag) && $("cardChargePercentage"+index))
      {
         var totalChargeAmount = $("cardChargePercentage"+index).innerHTML;
         totalChargeAmountPaid+= parseFloat(totalChargeAmount);
      }
      if($("cardChargePercentage"+index) && !flag && !(($("chkbox"+index) && $("chkbox"+index).checked)))
      {
         var totalChargeAmount = $("cardChargePercentage"+index).innerHTML;
         totalChargeAmountPaid+= parseFloat(totalChargeAmount);
      }
   }
   return totalChargeAmountPaid;
}

/* Calculates total amount paid. Considers all payment transactions
*
*/
function collectTotalAmountPaid()
{
   var panelType=$("panelType").value;
   var totalAmountPaid = 0;
   for (i=0;i<currOption;i++)
   {
      var index = i;
      if(($("chkbox"+index) && $("chkbox"+index).checked) || (($("payment_type_"+index).value).indexOf("CardPinPad")!= -1))
      {
         if (!isRefund && selectedTypeIndexCRData != "CardRefund")
            totalAmountPaid = totalAmountPaid + collectTotalAmountPaidHelper(index);
         else if (isRefund && selectedTypeIndexCRData != "CardRefund")
            totalAmountPaid = -(Math.abs(totalAmountPaid) + Math.abs(collectTotalAmountPaidHelper(index)));
     /*else if (reverseAmountStatus)
       totalAmountPaid = collectTotalAmountPaidHelper(index);*/
         else
             totalAmountPaid = -(Math.abs(totalAmountPaid) + Math.abs(collectTotalAmountPaidHelper(index)));
      }
      else if((panelType=='CNP' || panelType=='CPNA') && $('payment_type_'+index)&& $('payment_type_'+index).selectedIndex>0)
      {
         if (!isRefund && selectedTypeIndexCRData != "CardRefund")
            totalAmountPaid = totalAmountPaid + collectTotalAmountPaidHelper(index);
         else if (isRefund && selectedTypeIndexCRData != "CardRefund")
            totalAmountPaid = -(Math.abs(totalAmountPaid) + Math.abs(collectTotalAmountPaidHelper(index)));
         else
             totalAmountPaid = -(Math.abs(totalAmountPaid) + Math.abs(collectTotalAmountPaidHelper(index)));
      } //End of else if
   }
   return parseFloat(totalAmountPaid).toFixed(2);
}

/* Function is a helper used to calculate amountpaid for a single transaction+card charges(if applicable) for CP,CNP and CPNA
*
* @params index- index of this Transaction
*/
function collectTotalAmountPaidHelper(index)
{
   var totalAmountPaid = 0;
   var totalTransactionAmount = $("transamt"+index).value;
   totalTransactionAmount= trimSpaces(totalTransactionAmount)
   if(totalTransactionAmount=='' || totalTransactionAmount==currencySymbol)
   {
      totalTransactionAmount=currencySymbol+'0.0';
   }
   totalAmountPaid = parseFloat(totalTransactionAmount);

   //If card charge is applicable for this payment then add it.
   if($("cardChargePercentage"+index))
   {
      var totalChargeAmount = $("cardChargePercentage"+index).innerHTML;
      totalAmountPaid= totalAmountPaid + parseFloat(totalChargeAmount);
   }
   return totalAmountPaid;
}

function collectAllTotalAmount()
{
   var totalAmountSum = 0;
   //don't consider last transaction
   var iterMax = ($("transamt"+(currOption-1)).disabled)?currOption-1:currOption;
   for (i=0;i<iterMax;i++)
   {
      var index =i;
      if($("tottransamt"+index))
      {
         var totalTransactionAmount = $("tottransamt"+index).innerHTML.replace(" ","");
         if (totalTransactionAmount.length >= 1)
         {
            totalAmountSum+= parseFloat(totalTransactionAmount);
         }
      }
   }
   return parseFloat(totalAmountSum).toFixed(2);
}

/*shows Pay details in accumulation section*/
var selectedTypeIndexCRData = "";
function showPayDetails()
{
   var amtdue;
   if(isAdditional && amtRec==0)
   {
      totamtstilldue=0;
   }
   else
   {
      totamtstilldue=parseFloat(amtRec)+parseFloat(collectCharges(false))+parseFloat(collectCharges(true));
   }
   if (!isRefund && selectedTypeIndexCRData != "CardRefund" && !reverseAmountStatus)
   {
      amtdue = totamtstilldue - collectTotalAmountPaid();
   }
   else if (reverseAmountStatus)
   {
     totamtstilldue = parseFloat(totamtstilldue - collectCharges(false));
     amtdue = parseFloat(totamtstilldue);
   }
   else if (isRefund && selectedTypeIndexCRData != "CardRefund")
   {
      amtdue = -(Math.abs(totamtstilldue) - Math.abs(collectTotalAmountPaid()));
   }
   else
   {
      amtdue = -(Math.abs(totamtstilldue) - Math.abs(collectTotalAmountPaid()));
   }
   if (reverseAmountStatus)
      PaymentInfo.amtPaid = collectCharges(false);
   else
      PaymentInfo.amtPaid = Math.abs(collectTotalAmountPaid());
   PaymentInfo.amtDue = amtdue;
   PaymentInfo.payableAmount = totamtstilldue;
   $("totamtpaid").value=currencySymbol+(1*PaymentInfo.amtPaid).toFixed(2);
   $("totamtpaid_carry").value=parseFloat(1*PaymentInfo.amtPaid).toFixed(2);
   $("totamtpaid").disabled=true;
   $("amtstilltxt").value=currencySymbol+parseFloat(1*PaymentInfo.amtDue).toFixed(2);
   $("amtstilltxt").disabled=true;
   $("totamtduetxt").value=currencySymbol+parseFloat(1*PaymentInfo.payableAmount).toFixed(2);
   $("totamtduetxt").disabled=true;
}

function showPayDetailsCR(index)
{
   setSelectedType(index);
   showPayDetails();
}

function setSelectedType(index)
{
   var selectedTypeCR = $("payment_type_"+index).value;
   selectedTypeIndexCR = selectedTypeCR.split("|");
   selectedTypeIndexCRData = selectedTypeIndexCR[1];
}

/* collect the payment from the total transaction amount and updated the Amount summary
* @params amtNo represents index of transaction
*/
function collectPayment(amtNo)
{
   isPaymentNotMadeFlag=false;
   var panelType=$("panelType").value;
   $("total_amount_"+amtNo).value = 1*stripChars($("transamt"+amtNo).value,currencySymbol+''+','+' ');
   PaymentInfo.currentIndex = amtNo;
   var totalTrans = $("paymentTrans").value;
   var selectedTypeValue = $("payment_type_"+amtNo).value;
   var selectedTypeArray = selectedTypeValue.split("|");
   var selectedType = selectedTypeArray[1];
   var cardChargesValue = 0;
   var transAmtValue = 0;
   var totalTransAmtValue = 0;
   var noOfTransactions = $("paymentTrans").value
   var lastIndex = noOfTransactions-1;
   var isLastSelectedArray;
   var isLastSelected;
   var isSelectedArray;
   var isSelected;
   var index;
   if (trimSpaces(selectedType) == "Voucher" && (panelType == "CP" || panelType == "CPNA"))
   {
      var voucherTotal = collectTotalVoucherAmount(amtNo);
      if(1*$("total_amount_"+amtNo).value > 1*voucherTotal)
      {
         alert("The Voucher Value(s) entered must be equal to the Transaction Amount. Please adjust the appropriate value before proceeding.")
         $("chkbox"+amtNo).checked=false;
         return;
      }
   }
   if (trimSpaces(selectedType) != "CardPinPad")
   {
      handleCardSelection(amtNo);
   }
   var transAmount = $$('input.c_total_amount');
   var cardChargeValues = $$('input.cc_total_amount');
   if (checkUpdateTrue())
   {
      $("chkbox"+amtNo).checked=false;
      return;
   }

   var transactionAmount = $("transamt"+amtNo).value;
   if(transactionAmount == "" || !validateTransactionAmount(transactionAmount,amtNo))
   {
      return;
   }
   if (!isAdditional)
    $("transamt"+amtNo).disabled = true;

   for (var i = 0; i < noOfTransactions; i++)
   {
      isLastSelectedArray = $("payment_type_"+i).value.split("|");
      isLastSelected = isLastSelectedArray[1];
      index = i;
      if(isLastSelected == undefined)
      {
         break;
      }
   }

   for (var i = 0; i <= index; i++)
   {
      isSelectedArray = $("payment_type_"+i).value.split("|");
      isSelected = isSelectedArray[1];
      if($("transamt"+i).value != "" && isSelected != undefined)
      {
         if (isRefund)
            transAmtValue = (stripChars($("transamt"+i).value,currencySymbol+''+','+' ') > 0)? -(stripChars($("transamt"+i).value,currencySymbol+''+','+' ')):stripChars($("transamt"+i).value,currencySymbol+''+','+' ');
         else
            transAmtValue = stripChars($("transamt"+i).value,currencySymbol+''+','+' ');
         totalTransAmtValue = parseFloat(1*totalTransAmtValue + 1*transAmtValue).toFixed(2);
      }

   }
   if (!pinpadReverseCompleted)
   {
     for (var i = 0; i < cardChargeValues.length; i++)
     {
      cardChargesValue = parseFloat(1*cardChargesValue + 1*cardChargeValues[i].value).toFixed(2);
     }
     PaymentInfo.accumulatedCardCharge = cardChargesValue;
    }
   PaymentInfo.payableAmountWithoutCardCharges = totalTransAmtValue;
   PaymentInfo.amtPaid = parseFloat(1*PaymentInfo.payableAmountWithoutCardCharges + 1*PaymentInfo.accumulatedCardCharge).toFixed(2);
   if (!isAdditional || (isAdditional && amtRec > 0))
   {
     amtDue = parseFloat(1*PaymentInfo.basePayableAmount - 1*PaymentInfo.payableAmountWithoutCardCharges).toFixed(2);
     PaymentInfo.payableAmount = parseFloat(1*PaymentInfo.basePayableAmount + 1*PaymentInfo.accumulatedCardCharge).toFixed(2);
   }
   else if (isAdditional && amtRec == 0)
   {
     amtDue = -(parseFloat(1*PaymentInfo.payableAmountWithoutCardCharges +  + 1*PaymentInfo.accumulatedCardCharge).toFixed(2));
     PaymentInfo.payableAmount = parseFloat(0.00).toFixed(2);
   }
   PaymentInfo.amtDue = parseFloat(amtDue).toFixed(2);
   updatePaymentInfoForMultipleTransactions();
   displayTotalAmounts(bookingConstants.TOTAL_CLASS);
   updateTransactionAmountCarryField(transactionAmount,amtNo);
   if(!(noOfTrans==1))
   {
      $("transamt"+amtNo).disabled =true;
   }
   for (var i = 0; i < document.getElementsByName("refund_"+amtNo).length; i++)
   {
      if (!document.getElementsByName("refund_"+amtNo)[i].checked)
      {
         document.getElementsByName("refund_"+amtNo)[i].disabled =true;
      }
   }
   if($("chkbox"+amtNo)!=undefined && $("chkbox"+amtNo).checked)
   {
      $("chkbox"+amtNo).disabled =true;
      $("payment_type_"+amtNo).disabled = true;
   }
   else
   {
      $("payment_type_"+amtNo).disabled=false;
   }
   var selectedType = $("payment_type_"+amtNo).value;
   var selectedTypeIndex = selectedType.split("|");
   if ((panelType == "CNP" || panelType == "CPNA")&& selectedTypeIndex[1] == "CardRefund")
      showPayDetailsCR(amtNo);
   else
      showPayDetails();
}

/**
 ** Function for updating the PaymentInfo object and displaying its original state,
 ** incase of switching between customer types and payment types.
**/
function intialUpdatePaymentInfo()
{
   var cardChargesValue = 0;
   var transAmtValue = 0;
   var totalTransAmtValue = 0;
   var transAmount = $$('input.c_total_amount');
   var cardChargeValues = $$('input.cc_total_amount');
   for (var i = 0; i < transAmount.length; i++)
   {
      transAmtValue = stripChars(transAmount[i].value,currencySymbol+''+','+' ');
      totalTransAmtValue = parseFloat(1*totalTransAmtValue + 1*transAmtValue).toFixed(2);
   }
   for (var i = 0; i < cardChargeValues.length; i++)
   {
      cardChargesValue = parseFloat(1*cardChargesValue + 1*cardChargeValues[i].value).toFixed(2);
   }
   PaymentInfo.accumulatedCardCharge = null;
   PaymentInfo.totalCardCharge = null;
   PaymentInfo.payableAmountWithoutCardCharges = null;
   PaymentInfo.selectedCardType = null;
   PaymentInfo.payableAmount = parseFloat(1*PaymentInfo.basePayableAmount + 1*PaymentInfo.accumulatedCardCharge).toFixed(2);
   PaymentInfo.amtPaid = parseFloat(1*PaymentInfo.payableAmountWithoutCardCharges + 1*PaymentInfo.accumulatedCardCharge).toFixed(2);

   amtDue = parseFloat(1*PaymentInfo.basePayableAmount- 1*PaymentInfo.payableAmountWithoutCardCharges).toFixed(2);
   PaymentInfo.amtDue = parseFloat(amtDue).toFixed(2);
   displaySelectedDepositOptionsWithCardCharge();
   displayTotalAmounts(bookingConstants.TOTAL_CLASS);
}


function updateCardCharges(transVal)
{
   var transAmntVal = $("transamt"+transVal).value;
   var transAmnt =0;

   if(transAmntVal != "" || transAmntVal != currencySymbol)
   {
      transAmnt = transAmntVal;
   }
   if (PaymentInfo.selectedType && !isRefund)
   {
      if(cardChargeMap.get(PaymentInfo.selectedCardType) != null)
      {
         var cardCharges = calculateCardCharges(transAmnt);

         if ($("cardChargePercentage"+transVal))
            $("cardChargePercentage"+transVal).innerHTML=currencySymbol+parseFloat(cardCharges).toFixed(2);
         if ($("payment_"+transVal+"_chargeIdAmount"))
            $("payment_"+transVal+"_chargeIdAmount").value=parseFloat(cardCharges).toFixed(2);
         cardPerAmt(PaymentInfo.selectedCardType,transVal);
      }
   }
}

function validateTransactionAmount(transactionAmount,amtNo)
{
   if(!isAdditional)
   {
	   if (stripChars(transactionAmount,currencySymbol+''+'.'+' ').length < 1 || $("payment_type_"+amtNo).value == "")
      {
         if($("chkbox"+amtNo))
         {
            $("chkbox"+amtNo).checked=false;
         }
         return false;
      }
      else if((Math.abs(parseFloat(stripChars(transactionAmount,currencySymbol+''+' '))) > Math.abs(parseFloat($("amtstilltxt").value.replace(currencySymbol,"")))))
      {
         var panelType=$("panelType").value;
         if(panelType == 'CNP' || panelType == 'CPNA')
         {
         }
         else
         {
            if (($("payment_type_"+amtNo).value).indexOf("CardPinPad") == -1)
            {
               alert(exceedalert);
               $("chkbox"+amtNo).checked=false;
               $("transamt"+amtNo).disabled =false;
               return false;
            }
         }
      }
   }
   return true;
}

function checkPoundSymbol(obj)
{
   obj.value=currencySymbol;
   SetCursorToTextEnd(obj);
}

/* Updates transaction amount carry field,
 @params transactionAmount - amount of this particular transaction
         index             -index of this particular transaction
*/
function updateTransactionAmountCarryField(transactionAmount,index)
{
   var totalTransactions = $("paymentTrans").value;
   var lastTransactionIndex = totalTransactions-1;
   var transamt_carry= $("transamt_carry"+index);
   if (!isRefund && selectedTypeIndexCRData != "CardRefund")
   {
      transamt_carry.value = transactionAmount;
   }
   else if (isRefund && selectedTypeIndexCRData != "CardRefund")
   {
      transamt_carry.value = (-1*transactionAmount);
   }
   else
   {
      transamt_carry.value = (-1*transactionAmount);
   }
   if (isRefund && (index == lastTransactionIndex))
   {
      transamt_carry.value = -1 * transamt_carry.value;
   }
}

function totalVoucherChange(totalValue,totalTransValue)
{
   var pat = "";
   var valSet = totalValue.getAttribute("alt").split("|");
   var alertMsg = valSet[0];
   var type = valSet[1];

   if (type = "NUMERIC")
   {
      pat = /^[0-9{.}]*$/;
   }
   else if (type = "ALPHANUMERIC")
   {
      pat = /^[-a-zA-Z0-9 \\\/.,]*$/;
   }
   if (totalValue.value!='')
   {
      if (totalValue.value.match(pat))
      {
         var voucherValuetxt = totalValue.value;
         totalvouchers+=Number(totalValue.value);
         $("totalvaluechg"+totalTransValue).innerHTML=currencySymbol+collectTotalVoucherAmount(totalTransValue);
      }
      else
      {
         alert(alertMsg);
         totalValue.value=currencySymbol;
         SetCursorToTextEnd(totalValue);
      }
   }
}

function collectTotalVoucherAmount(totalTransValue)
{
   var max = vocIndex[totalTransValue];
   var  totalVocAmount= 0;
   var vocAmount = 0;
   for (i=0;i<max;i++)
   {
      if ($("voucherAmount"+totalTransValue+"_"+i))
      {
         if ($("voucherAmount"+totalTransValue+"_"+i).value.indexOf(currencySymbol)!=-1)
         {
            vocAmount = $("voucherAmount"+totalTransValue+"_"+i).value;
            //Add to voucher amount hidden field
            $("voucherCarry"+totalTransValue+"_"+i).value = stripChars($("voucherAmount"+totalTransValue+"_"+i).value,currencySymbol+''+','+' ');
         }
         else
         {
            vocAmount = $("voucherAmount"+totalTransValue+"_"+i).value;
            //Add to voucher amount hidden field
            $("voucherCarry"+totalTransValue+"_"+i).value = stripChars($("voucherAmount"+totalTransValue+"_"+i).value,currencySymbol+''+','+' ');
         }
         vocAmount = (vocAmount.indexOf(currencySymbol)!=-1)?vocAmount.replace(currencySymbol, ""):vocAmount;
         if (vocAmount.length > 0)
            totalVocAmount+=parseFloat(vocAmount);
      }
   }
   return parseFloat(totalVocAmount).toFixed(2);
}

function checkcurrencySymbol(obj)
{
   obj.value=currencySymbol;
   SetCursorToTextEnd(obj);
}

function checkUpdateTrue()
{
   if ($("updateFlag") && $("updateFlag").value == 'true')
   {
      return true;
   }
   return false;
}
/*********************End of Payment Display********************************************************/



/***********************Controller for payment page*********************************************/
  /* deposit amount section controller */
function setSelectedOption(description)
{
   selectedDepositOption=description;
   if(description == "ADDITIONAL" || description == "Additional")
   {
      isAdditional =true;
   }
   else
   {
      isAdditional = false;
   }
   if (description == 'REFUND' || description == 'Refund')
   {
        isRefund = true;
        isDepositRefund = true;
   }
   var panelType=$("panelType").value;
   if($('error0'))
	     $('error0').style.display="none";
	   if($('authcodeText0'))
	   {
	      $('authcodeText0').style.display="none";
	      $('payment_0_authCodeId').style.display="none";
	   }
   if (checkPayments())
   {
      PaymentInfo.selectedCardType = null;
      PaymentInfo.depositType = (description).toUpperCase();
      handleDepositSelection(PaymentInfo.depositType);
      amtRec = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
      PaymentInfo.basePayableAmount = PaymentInfo.selectedDepositAmount;
      refreshToDefault();
      selOption(1);
      displayAmt();
      updatePaymentInfoForMultipleTransactions();
      intialUpdatePaymentInfo();
      showPayDetails();
      updateCardChargesInSummaryPanel(0);
      refreshToDefaultDepositOptions(PaymentInfo.totalAmount);
      displayTotalAmounts(bookingConstants.TOTAL_CLASS);
   }
}

function checkPayments()
{
   var panelType=$("panelType").value;
   if ($('paymentTrans'))
   {
      var paymentFlag = false;
      var totalPayments = Number($('paymentTrans').value);
      for (var i = 0; i < totalPayments; i++)
      {
         if ($('chkbox'+i) && $('chkbox'+i).checked || (($("payment_type_"+i).value).indexOf("CardPinPad ")!= -1))
         {
            paymentFlag = true;
            isPaymentNotMadeFlag=false;
            break;
         }
         else
         {
            refreshToDefault();
            paymentFlag = false;
            break;
         }
      }
   }
   return true;
}
/*End of deposit amount controller */


//Controller for discounts( price beat and today's saving sections)
function discountHandler(obj,clientApp)
{
   clientApplication=clientApp;
   var panelType=$("panelType").value;
   var bool=false;
   //validate the discounts
   if(obj.id=="priceBeat")
   {
      bool= handlePriceBeat();
   }
   else
   {
      bool=handleTodaySaving();
   }

   //Update in server
   if(bool)
   {
      if(isPaymentNotMadeFlag)
      {
         updatePriceBeatAndDiscountValue(obj.id);
      }
      else
      {
         if(panelType=='CNP')
         {
            refreshToDefault();
            updatePriceBeatAndDiscountValue(obj.id);
         }
         else
         {
            obj.value = "";
            setFocus("Discounts are not allowed once payment is made",obj);
         }
      }
   }
}

/******************************************************************************************************************/

/* Check for the booking button */
function checkBookingButton()
{
   var tomcatInstance = $("tomcatInstance").value;
   var url="/cps/ConfirmBookingServlet?tomcat="+tomcatInstance;
   var request = new Ajax.Request(url,{method:"GET", onSuccess: responseShowConfirmBooking});
}

function responseShowConfirmBooking(request)
{
   if((request.readyState == 4) && (request.status == 200))
   {
      var result=request.responseText;
     if (result == "true")
      {
        if ($("confirmButton") != undefined)
           $("confirmButton").style.display = "block";
        if ($("confirmButton1") != undefined)
         $("confirmButton1").style.display = "block";
      }
     else
     {
        if ($("confirmButton") != undefined)
           $("confirmButton").style.display = "none";
        if ($("confirmButton1") != undefined)
         $("confirmButton1").style.display = "none";
     }
   }
}

function disableOtherDiscounts()
{
   if ($("priceBeat")&& $("priceBeat").value != "")
   {
      $("todaySaving").disabled = true;
   }
   else if ($("todaySaving")&& $("todaySaving").value != "")
   {
      $("priceBeat").disabled = true;
   }
}

function checkForPaymentMade(name)
{
   var panelType=$("panelType").value;
   if ($('paymentTrans'))
   {
      var paymentFlag = false;
      var totalPayments = Number($('paymentTrans').value);
      for (var i = 0; i < totalPayments; i++)
      {
         if ($('chkbox'+i) && $('chkbox'+i).checked || (($("payment_type_"+i).value).indexOf("CardPinPad ")!= -1))
         {
            paymentFlag = true;
            isPaymentNotMadeFlag=false;
            break;
         }
         else if(panelType=='CNP')
         {
            refreshToDefault();
            paymentFlag = false;
            break;
         }
      }
   }
   if (paymentFlag && !isPaymentNotMadeFlag)
   {
      alert ("Discounts are not allowed once payment is made.");
      if (name == "priceBeat")
      {
         $("priceBeat").value = "";
      }
      if (name == "todaySaving")
      {
         $('todaySaving').value = "";
      }
      if (name == "Promotional Code")
      {
         $('promotionalCode').value = "";
      }
      return false;
   }
   return true;
}

function setValueForInput(id , value)
{
   if($(id))
   {
      $(id).value = value;
   }
}

function setDisplay(id , value)
{
   if($(id))
   {
      $(id).style.display = value;
   }
}

function getInputIntValue(id)
{
   var value  = 0;
   if($(id))
   {
      value = $(id).value;
   }
   if(parseFloat(value))
   {
      return (parseFloat(value));
   }
   else
   {
      return 0;
   }
}

function checkCardRefundRadio(index)
{
   var selectedType = $("payment_type_"+index).value;
   var selectedTypeIndex = selectedType.split("|");
   var divContent, divElements, elm, radioLen;
   if (selectedTypeIndex[1] !=null && trimSpaces(selectedTypeIndex[1]) == "CardRefund")
   {
      if (isRefund && trimSpaces(selectedTypeIndex[1]) == "CardRefund")
      {
         divContent = $("cardRefundArea"+index);
         divElements = divContent.getElementsByTagName("input");
         for(var j = 0; j < divElements.length; j++)
         {
            elm = divElements[j];
            switch (elm.type)
            {
               case 'radio':
               for (var i = 0; i < document.getElementsByName(elm.name).length; i++)
               {
                  if (document.getElementsByName(elm.name)[i].checked)
                  {
                     return true;
                     break;
                  }
               }
            }
         }
      }
      return false;
   }
   else
   {
      return true;
   }
}

function validateRadioChecking(index,dataCashreference)
{
   if($("payment_"+index+"_datacashreference"))
   {
   $("payment_"+index+"_datacashreference").value =dataCashreference;
   }
   var totalTransactions = $("paymentTrans").value;
   var lastTransactionIndex = totalTransactions-1;
   var currentTransaction = index;
   var previousTransaction = (currentTransaction == 0) ? currentTransaction : (currentTransaction - 1);
   var previousTransactionForIndex = currentTransaction - 1;
   var currentIndexRadioLength = document.getElementsByName("refund_"+index).length;
   var cardRefundSelectedIndex = -1;
   var radioCheckedStatus = false;

    if (index == lastTransactionIndex)
   {
      var transamt_carry= $("transamt_carry"+index).value;
      if(Math.abs(stripChars(transamt_carry,currencySymbol+''+','+' ')) > parseFloat(1*getCheckedRadioValue(index)).toFixed(2))
      {
         alert(" Transaction Amount exceeds selected transaction amount.");
     for (var i = 0; i < totalTransactions; i++)
     {
      for (var j = 0; j < currentIndexRadioLength; j++)
      {
        document.getElementsByName("refund_"+currentTransaction)[j].checked = false;
            radioAlert=false;
      }
     }
         return false;
      }
   }

   for (var x = 0; x < totalTransactions; x++)
   {
      if (trimSpaces($("payment_type_"+x).value) == "VI|CardRefund")
      {
         ++cardRefundSelectedIndex;
      }
   }
   for (var i = 0; i < totalTransactions; i++)
   {
      for (var j = 0; j < currentIndexRadioLength; j++)
      {
         for (var k = 0; k < cardRefundSelectedIndex; k++)
         {
            if (document.getElementsByName("refund_"+currentTransaction)[j].value == document.getElementsByName("refund_"+(k))[j].value)
            {
               if ((document.getElementsByName("refund_"+currentTransaction)[j].checked && (document.getElementsByName("refund_"+(k))[j].checked) && !document.getElementsByName("refund_"+(k))[j].enabled))
               {
                  alert("Same transaction cannot be checked.");
                  document.getElementsByName("refund_"+currentTransaction)[j].checked = false;
                  return false;
                  continue;
               }
            }
         }
      }
   }
   if (index == lastTransactionIndex)
   {
      collectPayment(index);
   }
}

function getCheckedRadioValue(index)
{
   for (var i = 0; i < document.getElementsByName("refund_"+index).length; i++)
   {
      if (document.getElementsByName("refund_"+index)[i].checked)
      {
         return document.getElementsByName("refund_"+index)[i].value;
      }
   }
}

/////////////////////////////////////////////// Check for transaction mandatory fields starts here ////////////////////////////////////////////////////////////////
function checkFields(obj,objValue,index)
{
   var panelType=$("panelType").value;
   if($("paymentTrans")==undefined)
   {
     return true;
   }
   setSelectedType(index);
   var noOfPayments = $("paymentTrans").value;
   var divContent, divElements, elm;
   if (index != 0 && obj.value != "")
   {
      divContent = $("transactionId_"+(index-1));
      divElements = divContent.getElementsByTagName("*");
      for(var j = 0; j < divElements.length; j++)
      {
         elm = divElements[j];
         switch(elm.type)
         {
            case "text":
            if(!(($(elm.id).id).indexOf("payment_"+(index-1)+"_postCodeId")!= -1))
            {
             if ($(elm.id).id =='IssueNumberInput')
            {
              if (((($(elm.id).value).length == 1 && $(elm.id).value == currencySymbol) || ($(elm.id).value).length == 0) && $(elm.id).style.display == "block")
               {

                   if($("StartMonth").selectedIndex==0 || $("StartYear").selcetedIndex == 0)
                   {
                   return false;
                   }
               }
            }
            else if ((($(elm.id).value).length == 1 && $(elm.id).value == currencySymbol) || ($(elm.id).value).length == 0 && $(elm.id).style.display == "block")
               {
                  return false;
               }
            }
            break;
            case "checkbox":
               if (panelType != 'CNP')
               {
                  if ($(elm.id) && ($(elm.id).style.display == "block" || $(elm.id).style.display == "") && $(elm.id).checked != true && trimSpaces(selectedTypeIndexCRData) != "CardRefund")
                  {
                     return false;
                  }
                  if ($(elm.id) && ($(elm.id).style.display == "block" || $(elm.id).style.display != "") && $(elm.id).checked != true && trimSpaces(selectedTypeIndexCRData) == "CardRefund")
                  {
                     return false;
                  }
               }
               break;
            case "select-one":
               if ($(elm.id).id != "StartMonth" && $(elm.id).id != "StartYear")
               {
                  if ($(elm.id).selectedIndex == 0)
                  {
                     return false;
                  }
               }
               break;
         }
      }
   }
   return true;
}
////////////////// function to calculate the total amt entered in the fields to compare with additional amount incase of additional deposit option.
function totalAmountEntered()
{
   var totamtentered=0;
   var amt =0;
   var totalTransactions = $("paymentTrans").value;
   for(i=0;i<=totalTransactions;i++)
   {
      if( $("transamt"+i)!= null && $("transamt"+i).value != "")
      {
         totamtentered = parseFloat($("transamt"+i).value)+parseFloat(totamtentered);
      }
   }
   if(parseFloat(totamtentered) >parseFloat(additionaltotamount))
   {
      return false;
   }
   return true;
}

function updateEssentialFieldsForBRAC()
{
   var totalTransactions = $("paymentTrans").value;
   for(i=0;i<totalTransactions;i++)
   {
      var payType = $('payment_type_'+i).value.split("|");
      var total_amount = $("total_amount_"+i).value;
      if(isRefund && searchType == "CSS Mode" &&  payType[1]!=null)
      {
        if((trimSpaces(payType[1]) == "Cheque" && payType[0]=="CQ") || (trimSpaces(payType[1]) == "Cash" && payType[0]=="SW") || (trimSpaces(payType[1]) == "Cash" && payType[0]=="VI")|| (trimSpaces(payType[1]) == "Cash" && payType[0]=="CA"))
         {
            $("total_amount_"+i).value = "-" + Math.abs(total_amount);
         }
         else if (payType[0] == "VI" && trimSpaces(payType[1]) == "CardRefund")
         {
           $("total_amount_"+i).value = (total_amount.indexOf("-") != -1) ? total_amount : "-" + Math.abs(total_amount);
         }
      }
    //Modified the else if for bug # 65841
      else if (isRefund && ((payType[0] == "VI" && trimSpaces(payType[1]) == "CardRefund") || (trimSpaces(payType[1]) == "Cheque" && payType[0]=="CQ") || (trimSpaces(payType[1]) == "Cash" && payType[0]=="SW") || (trimSpaces(payType[1]) == "Cash" && payType[0]=="VI") || (trimSpaces(payType[1]) == "Cash" && payType[0]=="CA")))
      {
           $("total_amount_"+i).value = (total_amount.indexOf("-") != -1) ? total_amount : "-" + Math.abs(total_amount);
      }
      if(isRefund && totalTransactions == 1 && payType[1]!=null && trimSpaces(payType[1]) == "NoPayment" && payType[0]=="RR")
      {
         $("transamt"+i).value=currencySymbol+"0";
         $("transamt_carry"+i).value=0;
         $("tottransamt"+i).innerHTML=0;
         $("total_amount_"+i).value=0;
         PaymentInfo.calculatedPayableAmount=0;
      }
   }
   $("total_transamt").value = PaymentInfo.calculatedPayableAmount;
}
