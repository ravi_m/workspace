/**============================= PaymentPageLoadHandler============================================*/
var PaymentPageLoadHandler =
{
  handle:function()
  {
   if (StringUtils.equalsIgnoresCase($("#datacashEnabled").val(), "false"))
   {
      $("#cp")[0].checked='true';
      PaymentDetailsContent.setCustomerType('CP');
      $("#cp")[0].disabled=true;
      $("#cnp")[0].disabled=true;
      $("#cpna")[0].disabled=true;
   }
   else
   {
      if (!StringUtils.equalsIgnoresCase(callType, 'DEFAULT'))
      {
         $("#cnp")[0].checked='true';
         PaymentDetailsContent.setCustomerType('CNP');
         $("#cp")[0].disabled=true;
         $("#cnp")[0].disabled=true;
         $("#cpna")[0].disabled=true;
         return false;
      }
      $("#cp")[0].enabled=true;
      $("#cnp")[0].enabled=true;
      $("#cpna")[0].enabled=true;
   }
  }
};
/**=====================END PaymentPageLoadHandler=================================================*/

//++++++++++++++++++++++++++View Layer+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/**=====================PaymentView================================================================*/
/**
 * The object representing View layer. All the DOM manipulation should be done
 * in functions defined in this object
 */
var PaymentView =
{
   /**
    * Show alert and focus the field.
    */
   showAlert:function(Msg, fieldObj)
   {
      alert(Msg);
	  if (fieldObj != null)
	  {
         fieldObj.focus();
	  }
      return false;
   },

   /**
    * Display the authCode section only for CP Card type payments.
    */
   displayAuthCodeSection:function(index,paymentMode)
   {
	  if (paymentMode == "Card")
	  {
	     $('#authCode_'+index).removeClass("hide");
	  }

	  else
	  {
         $('#authCode_'+index).addClass("hide");
	  }
   },

   /**
    * Display the issueNumber section only for Switch,Maestro or Solo cards.
    */
   displayIssueNumberSection:function(index,paymentType)
   {
	 if(paymentType == "SOLO" || paymentType == "MAESTRO" || paymentType == "SWITCH")
     {
	     $("#issueNumberSection_"+index).removeClass("hide");
	     $("#issueNumber_"+index).removeClass("hide");
     }
   },

   /**
    * Add the amount as 0 in case of "NoPayment" option selection as payment type.
    */
   displayTransAmt:function(index,paymentMode,paymentType)
   {
     if (StringUtils.equalsIgnoresCase(paymentMode, "NoPayment") && StringUtils.equalsIgnoresCase(paymentType, "CA"))
	 {
		 $("#transAmt_"+index).val(PaymentDetailsContent.currencySymbol+"0.00")
	 }
   },

   /**
    *  Alters the message to be displayed below the security code.
    */
   displaySecurityCodeText:function(index,paymentType)
   {
	  if(paymentType == "AMERICAN_EXPRESS")
	  {
	     $(".smalltext_"+index).html("(the 4 digit code above<br/> the card number on the front of your card)");
	  }
   },

   /**
    * Removes the currency symbol from the value if it exists.
    */
   removeCurrencySymbol:function(value)
   {
     if (!(value.indexOf(PaymentDetailsContent.currencySymbol) == -1))
	 {
    	 value = StringUtils.stripFirstChar(value);
	 }
     return value;
   },

   /**
    * refresh the field
    */
   refreshTheField:function(fieldObj)
   {
      if (fieldObj.type == 'text')
      {
	     var temp = StringUtils.substringBefore(fieldObj.id, "_");
		 if(FormValidationRules[temp].isAmount)
		 {
		    fieldObj.value =PaymentDetailsContent.currencySymbol;
		    StringUtils.SetCursorToTextEnd(fieldObj);
		 }
		 else if(FormValidationRules[temp].isToBeRefreshed)
		 {
			 fieldObj.value = "";
		 }
	  }
	  else if (fieldObj.type == 'checkbox')
	  {
	     fieldObj.checked = false;
	  }
   },

   /**
    * Add payment content like Cash, Dcard, Voucher, Cheque or GiftCard based on paymentMode parameter passed
    *
    * @param index -index of the transaction
    * @param paymentMode -DCard, Cash etc
    * @param paymentType -Cash, Cheque, MasterCard etc
    */
   addPaymentContent : function(index, paymentMode, paymentType)
   {
      //We are calling a method dynamically. Note we need to pass argument name and then value  for the particular method
      $("#paymentOptionDetails"+index).html("");
      if(StringUtils.isNotBlank(paymentMode) )
      {
         var paymentContent = PaymentDetailsContent[paymentMode+"PaymentContent"].call(index,index,paymentMode,paymentType);
         $(paymentContent).appendTo("#paymentOptionDetails"+index);
      }
   },

   /**
    * clear the text in a input field.
    *
    * @param fieldObj the field read as DOM
    */
   emptyTheField : function(fieldObj)
   {
      fieldObj.html("");
   },

   /**
    * sets dropDown to default selection(i.e. selected index will be 0)
    *
    * @param dropDownObj the drop down read as DOM
    */
   setDropDownsToDefault : function(dropDownObj)
   {
      dropDownObj.selectedIndex = 0;
   },

   /**
    * Removes payment Transaction Sections.
    * This function is called whenever user changes no. of payment transactions.
    *
    */
   removePaymentTransactionSections : function()
   {
      $("div").remove(".paymentTransactionSections");
   },

   /**
    * Displays following amounts on payment page
    *  calculatedPayableAmount,
    *  calculatedTotalAmount,
    *  calculatedCardCharge ,
    *  paidAmount etc.
    *
    * @param PaymentInfo- The object which contains all the amounts to be displayed on payment page
    */
   updatePaymentPage : function(PaymentInfo )
   {
      //Update the deposit sections
	  this.updateDepositSection(PaymentInfo);

      //Display card charge in summary panel.
      this.showCardChargeInSummaryPanel(PaymentInfo.getCalculatedCardCharge());


      //Update payment section details like card charge, total trans amount
      this.updatePaymentSection(PaymentInfo);
   },

   /**
    * Refreshes the other two deposit sections other than the selected deposit type.
    */
   refreshTheOtherDepositTypes : function()
   {
      for (var index in depositTypes)
	  {
         if (depositTypes[index]!= PaymentInfo.getDepositType())
		 {
		    $("."+depositTypes[index]).html(depositAmountsMap.get(depositTypes[index]));
		 }
	  }
   },

   updateDepositSection: function(PaymentInfo)
   {
      //Update the total package amount in the deposit section.
	  $(".totalholidaycost").html(MathUtils.roundOff(((1*depositAmountsMap.get(bookingConstants.FULL_COST))+(1*PaymentInfo.getCalculatedCardCharge())),2));

      //Update the amount in the deposit section.
      if(StringUtils.isNotBlank(PaymentInfo.depositType))
      {
	     $("."+PaymentInfo.depositType).html(PaymentInfo.getCalculatedPayableAmount());
	  }

      //This function refreshes the other two deposit options to their default value (required in the scenario when card charges are updated on a particular deposit section
	  //and then the deposit type is changed)
	  this.refreshTheOtherDepositTypes();
   },

   /**
    * Displays card charge in summary panel if card charge more than zero.
    * @param cardCharge.
    */
   showCardChargeInSummaryPanel : function(cardCharge)
   {
      $(".totalholidaycostSummaryPanel").html(PaymentDetailsContent.currencySymbol+MathUtils.roundOff((1*depositAmountsMap.get('FULLCOST')+1*cardCharge),2));
	  $(".calculatedCardChargeSummaryPanel").html(PaymentDetailsContent.currencySymbol+MathUtils.roundOff(1*cardCharge,2));
      if(cardCharge > 0)
      {
         $(".cardchargeBlock").removeClass("hide");
      }
      else
      {
         $(".cardchargeBlock").addClass("hide");
      }
   },

   /**
    * Update payment section details like card charge, total trans amount for each transaction.
    */
   updatePaymentSection : function(PaymentInfo)
   {
      //Update payment page with  calculatedPayableAmount
      $(".calculatedPayableAmount").val(PaymentDetailsContent.currencySymbol+MathUtils.roundOff(PaymentInfo.getCalculatedPayableAmount(),2));

      //Update payment page with  calculatedTotalAmount
      $(".calculatedTotalAmount").html(PaymentDetailsContent.currencySymbol+MathUtils.roundOff(1*PaymentInfo.getCalculatedTotalAmount(),2));

      //Update payment page with calculatdCardCharge
      $(".calculatedCardCharge").html(PaymentDetailsContent.currencySymbol+MathUtils.roundOff(PaymentInfo.getCalculatedCardCharge(),2));

      //Update payment page with paidAmount
      $(".paidAmount").val(PaymentDetailsContent.currencySymbol+MathUtils.roundOff(PaymentInfo.getPaidAmount(),2));

      //Update payment page with AmountStill Due

      $(".amountStillDue").val(PaymentDetailsContent.currencySymbol+((MathUtils.roundOff(PaymentInfo.getCalculatedPayableAmount(),2))-(MathUtils.roundOff(PaymentInfo.getPaidAmount(),2))));

	  //TODO: for single transaction, no need to refresh all payment section
	  //rather we can pass index and update only that particular transaction.
      var transactionDetMapValueList = PaymentInfo.getTransactionDetailsMap().getAllValuesList();
      for(var index in transactionDetMapValueList)
      {
         cardCharges = 1*transactionDetMapValueList[index].cardCharge;
         transAmtWithCardCharge = 1*transactionDetMapValueList[index].transactionAmount + cardCharges;
         $("#totalTransAmount_"+index).html(PaymentDetailsContent.currencySymbol+MathUtils.roundOff(1*transAmtWithCardCharge,2));

         paymentMode = transactionDetMapValueList[index].paymentMode;
         if(this.isCardChargeApplicable(paymentMode))
         {
            $("#cardAmtBlock_"+index).removeClass("hide");
            $("#cardCharge_"+index).html(PaymentDetailsContent.currencySymbol+MathUtils.roundOff(1*cardCharges,2));
         }
         else
         {
            $("#cardAmtBlock_"+index).addClass("hide");
         }
      }
   },

   /**
    * Update last transaction amount and total transaction amount.
    *
    * @param lastTransactionAmount- The amount to be displayed in last transaction field
    */
   updateLastTransactionAmountField : function(lastTransactionAmount)
   {
      $(".transAmt:last").val(PaymentDetailsContent.currencySymbol+MathUtils.roundOff(lastTransactionAmount,2));
      $(".totalTransAmt:last").html(PaymentDetailsContent.currencySymbol+MathUtils.roundOff(lastTransactionAmount,2));
   },

   /**
    * Determines whether card charge is applicable for selected payment mode(e.g Dcard, Card etc).
    *
    * @param paymentMode - Dcard, card etc.
    */
   isCardChargeApplicable : function(paymentMode)
   {
      var cardChargeApplicable = false;
      switch(paymentMode)
      {
         case 'Dcard' :  cardChargeApplicable = true;
                               return  cardChargeApplicable;

         case 'Card': cardChargeApplicable = true;
                               return  cardChargeApplicable;

         case 'GiftCard': cardChargeApplicable = true;
                               return  cardChargeApplicable;

         case 'CardPinPad': cardChargeApplicable = true;
                               return  cardChargeApplicable;
      }
      return cardChargeApplicable;
   },

   displayStartButton : function(index)
   {
	   $('#buttonDiv'+index).html('<input id="button" type="button" value="Start" onclick="startTransaction('+index+');setIndex('+index+')">');

   },

   displayCancelButton : function (index,disableAttributeSetter)
   {
	   if (disableAttributeSetter != null && disableAttributeSetter)
	   {
		   $('#buttonDiv'+index).html('<input id="button" type="button" value="Cancel" disabled="disabled" onclick="javascript:doStopButton();">');
	   }
	   else
	   {
		   $('#buttonDiv'+index).html('<input id="button" type="button" value="Cancel" onclick="javascript:doStopButton();">');
	   }
   },

   displayPinpadLI : function (index, isToBeHidden)
   {
	   if (isToBeHidden)
	   {
		   $("#pinpadCardDetails"+index) .addClass("hide");
		   $("#pinpadMsgDetails"+index) .addClass("hide");
	   }
	   else
	   {
	      $("#pinpadCardDetails"+index) .removeClass("hide");
	      $("#pinpadMsgDetails"+index) .removeClass("hide");
	   }
   },

   removeStartButton : function (index)
   {
	   $('#buttonDiv'+index).html('')
   }
};
/**=====================END PaymentView============================================================*/

/**=====================PaymentDetailsContent======================================================*/
/**
 * Object holds HTML content for Payment page which needs to be dynamically displayed. Part of view layer
 */
var PaymentDetailsContent =
{
   /**
	*	Variable which will hold cp, cnp or cpna.
    * Used to populate paymentType drop down. Which varies for each customer type
    **/
   customerType : "",

   /** The currency symbol to be used*/
   currencySymbol: "",

   /** The setter for currency symbol*/
   setCurrencySymbol: function(currencySymbol)
   {
      this.currencySymbol = currencySymbol;
   },

   /** Reset paymentTypeDropDown as applicable for customer type  */
   setCustomerType:function(customerType)
   {
      this.customerType = customerType;
   },

   /**
    * Returns PaymentType DropDown Options for cp
    * (drop down options are constructed on jsp with values sent from server side)
	**/
   CP_PaymentTypedropDownOptions : function()
   {
      return CP_PaymentTypeDropDownOptions;
   },

   /**
    * Returns PaymentType DropDown Options for cnp
    * (drop down options are constructed on jsp with values sent from server side)
	**/
   CNP_PaymentTypedropDownOptions : function()
   {
      return CNP_PaymentTypeDropDownOptions;
   },

   /**
    * Returns PaymentType DropDown Options for cpna
    * (drop down options are constructed on jsp with values sent from server side)
	**/
   CPNA_PaymentTypedropDownOptions : function()
   {
      return CPNA_PaymentTypeDropDownOptions;
   },

   /**
	*	Returns PaymentType DropDown Options for Refund Card
	*	(drop down options are constructed on jsp with values sent from server side)
	**/
   REFUNDCARD_PaymentTypeDropDownOptions : function()
   {
      return REFUNDCARD_PaymentTypeDropDownOptions;
   },

   /**
	*	Returns PaymentType DropDown Options for PPG
	*	(drop down options are constructed on jsp with values sent from server side)
	**/
   PPG_PaymentTypeDropDownOptions : function()
   {
   	  return PPG_PaymentTypeDropDownOptions;
   },

   paymentSectionTemplateIndexed: function (index)
   {
      var paymentTemplateSection = '<li id="paymentTypeBlock" class="paymentTypeBlock">'+
	     '<input type="hidden" name="payment_'+index+'_paymenttypecode" id="paymenttypecode_'+index+'" value="">'+
         '<input type="hidden" name="payment_'+index+'_paymentmethod" value="" id="paymentmethod_'+index+'">'+
		 '<div class="selectionSection" style="width:380px;"><select style="float:left" class="payment_type" id="paymentType_'+index+'" onchange="PaymentTypeChangeHandler.handle(this,'+index+')">'+
		 '<option value="">Please Select</option>';
      if (this.customerType != "")
	  {
	     paymentTemplateSection += this[this.customerType+"_PaymentTypedropDownOptions"].call();
	  }
	  paymentTemplateSection += '</select><div id="buttonDiv'+index+'" class="buttonDiv"></div></div>' +
      '</li>'+
      '<li id="transactionAmtBlock" class="transactionAmtBlock">'+
         '<span>Transaction Amount</span>'+
		 '<input type="text" id="transAmt_'+index+'"  class="transAmt medium" value="'+PaymentDetailsContent.currencySymbol+'" onBlur="TransAmtHandler.handle(this, this.value, '+index+')"/>'+
      '</li>'+
      '<li id="cardAmtBlock_'+index+'" class="cardAmtBlock hide">'+
	     '<span>Card Charges</span>'+
		 '<span id="cardCharge_'+index+'"></span>'+
      '</li>'+
	  '<li id="totalAmtBlock" class="totalAmtBlock">'+
         '<span>Total amount charged in this transaction:</span>'+
		 '<span class ="totalTransAmt" id="totalTransAmount_'+index+'">'+PaymentDetailsContent.currencySymbol+'</span>'+
		 '<input type="hidden" id="transAmountPaid_'+index+'" name="payment_'+index+'_amountPaid" value="0"/>'+
		 '<input type="hidden" id="transCardCharge_'+index+'" name="payment_'+index+'_chargeAmount" value="0"/>'+
      '</li>'+
      //Section to contain paymentOptionDetails(card details, cash details, check details etc)
	  '<li id="paymentOptionDetails'+index+'" class="paymentOptionDetails"></li>'
	  return paymentTemplateSection;
   },

   /**
    * paymentTransactionSections
	**/
   paymentSectionTemplate :function(index,newSelection)
   {
      var paymentTemplateSection = '<div id="paymentTransactionSections'+index+'" class="paymentTransactionSections">'+
	     '<div id="transactionHeading"><strong><label class="transactionNumber">Transaction '+(index+1)+'</label></strong><br clear="all"/></div>'+
		 '<ul id="paymentSection'+index+'" class="paymentSection" >'+
		 '<input type="hidden" name="payment_'+index+'_paymenttypecode" id="paymenttypecode_'+index+'" value="">'+
         '<input type="hidden" name="payment_'+index+'_paymentmethod" value="" id="paymentmethod_'+index+'">'+
		 '<li id="paymentTypeBlock" class="paymentTypeBlock">'+
		 '<div class="selectionSection" style="width:380px;"><select style="float:left" class="payment_type" id="paymentType_'+index+'" onchange="PaymentTypeChangeHandler.handle(this,'+index+')">'+
		 '<option value="">Please Select</option>';
      if (this.customerType != "")
	  {
	     paymentTemplateSection += this[this.customerType+"_PaymentTypedropDownOptions"].call();
	  }
	  paymentTemplateSection += '</select><div id="buttonDiv'+index+'" class="buttonDiv"></div></div>' +
      '</li>'+
      '<li id="transactionAmtBlock" class="transactionAmtBlock">'+
         '<span>Transaction Amount</span>'+
		 '<input type="text" id="transAmt_'+index+'"  class="transAmt medium" value="'+PaymentDetailsContent.currencySymbol+'" onBlur="TransAmtHandler.handle(this, this.value, '+index+')"/>'+
      '</li>'+
      '<li id="datacashError_0" class="datacashError hide">'+
         '<span id="error_0">'+responseMsg+'</span>'+
      '</li>';
      if(responseMsg == null || responseMsg == ""){
	    paymentTemplateSection += '<li id="authCode_'+index+'" class="authCode hide">'+
	     '<span>Auth Code</span>'+
		  '<input type="text" id="authCode_'+index+'" name="payment_'+index+'_referenceNumber"  class="medium"/>'+
	     '</li>';
	  }
      paymentTemplateSection +='<li id="cardAmtBlock_'+index+'" class="cardAmtBlock hide">'+
	     '<span>Card Charges</span>'+
		 '<span id="cardCharge_'+index+'"></span>'+
      '</li>'+
	  '<li id="totalAmtBlock" class="totalAmtBlock">'+
         '<span>Total amount charged in this transaction:</span>'+
		 '<span class ="totalTransAmt" id="totalTransAmount_'+index+'">'+PaymentDetailsContent.currencySymbol+'</span>'+
		 '<input type="hidden" id="transAmountPaid_'+index+'" name="payment_'+index+'_amountPaid" value="0"/>'+
		 '<input type="hidden" id="transCardCharge_'+index+'" name="payment_'+index+'_chargeAmount" value="0"/>'+
      '</li>'+
      //Section to contain paymentOptionDetails(card details, cash details, check details etc)
	  '<li id="paymentOptionDetails'+index+'" class="paymentOptionDetails"></li>'+
      // Section to contain paymentOptionDetails(card details, cash details, check details etc)
      '<li class="pinpadCardDets hide" id="pinpadCardDetails'+index+'"></li>'+ //div for PINPAD cardtype
      '<li class="pinpadMsgDets hide" id="pinpadMsgDetails'+index+'"></li>'+ //div for PINPAD message
	  '<li id="pinpadCardNumber'+index+'" style="display:none;"></li>'+ //div for pinpad card number

	  '</ul>';
	  if (index != newSelection-1)
	  {
         paymentTemplateSection +='<hr class="line_val"/>';
	  }
	  paymentTemplateSection +='</div>';
	  return paymentTemplateSection;
   },

   cardChargeAndTotalAmountContent : function(index)
   {
      return '<li id="cardAmtBlock_'+index+'" class="cardAmtBlock hide">'+
	     '<span class="calculatedCardCharge">Card Charges</span>'+
		 '<span id="cardCharge_'+index+'"></span>'+
		 '</li>'+
		 '<li id="totalAmtBlock" class="totalAmtBlock">'+
		 '<span>Total amount charged in this transaction:</span>'+
		 '<span id="totalTransAmount_'+index+'"></span>'+
		 '<input type="hidden" id="transAmountPaid_'+index+'" name="payment_'+index+'_amountPaid" value="0"/>'+
		 '<input type="hidden" id="transCardCharge_'+index+'" name="payment_'+index+'_chargeAmount" value="0"/>'+
		 '</li>';
   },

   /**
    * HTML content for DCard.
    */
   DcardPaymentContent :function(index)
   {
      return '<fieldset id="dCardContent" class="dCardContent">'+
	  /*---------------Card Number,Name on Card, Postcode----------------------------*/
      '<ul class="leftColumn">'+
         '<li>'+
            '<label for="cardNumber">Card Number</label>'+
			'<input type="text" id="cardNumber_'+index+'" name="payment_'+index+'_cardNumber" class="large" onblur="FormHandler.handleIndividualValidations(this,'+index+');" autocomplete="off"/>'+
         ' <font class="red"> * </font></li>'+
         '<li>'+
            '<label for="nameOnCard">Name on Card</label>'+
			'<input type="text" id="nameOnCard_'+index+'" name="payment_'+index+'_nameOnCard" class="medium" onBlur="FormHandler.handleIndividualValidations(this,'+index+');" autocomplete="off"/>'+
         ' <font class="red"> * </font> </li>'+
         '<li>'+
            '<label for="postCode" class="postCodecaption">Postcode of billing address for this card</label>'+
			'<input type="text" id="postCode_'+index+'" name="payment_'+index+'_postCode" class="medium" onBlur="FormHandler.handleIndividualValidations(this,'+index+');" autocomplete="off"/>'+
         ' <font class="red"> * </font> </li>'+
         '<li id="authCodeReferred_0" class="authCode hide">'+
         '<label for="authCode" class="authCodecaption">AuthCode</label>'+
         '<input type="text" class="medium" maxlength="25" id="authCodeReferred_0" name="payment_0_referenceNumber" autocomplete="off"/>'+
         '</li>'+
      '</ul><!-- END leftColumn Div-->'+
      /*---------------END Card Number,Name on Card, Postcode----------------------------*/
      /*---------------Expiry Date, Security code-----------------------------------------*/
      '<ul class="rightColumn">'+
         '<li>'+
		    '<label for="expiryDate_'+index+'">Expiry Date</label>'+
			'<select id="expiryMonth_'+index+'" name="payment_'+index+'_expiryMonth">'+
               "<option value=''>MM</option>"+
               "<option value='01'>01</option>"+
               "<option value='02'>02</option>"+
               "<option value='03'>03</option>"+
               "<option value='04'>04</option>"+
               "<option value='05'>05</option>"+
               "<option value='06'>06</option>"+
               "<option value='07'>07</option>"+
               "<option value='08'>08</option>"+
               "<option value='09'>09</option>"+
               "<option value='10'>10</option>"+
               "<option value='11'>11</option>"+
               "<option value='12'>12</option>"+
            '</select>'+
			'<select id="expiryYear_'+index+'" name="payment_'+index+'_expiryYear">'+
               requiredFields_expiryYear+
            '</select>'+
         ' <font class="red"> * </font> </li>'+
         '<li>'+
            '<label for="securityCode">Security code<br/><span id="smalltext" class="smalltext_'+index+' smallFont ">(last 3 digits in the signature strip on the reverse of your card)</span></label>'+
			'<input type="text" id="securityCode_'+index+'" name="payment_'+index+'_securityCode" class="small" onBlur="FormHandler.handleIndividualValidations(this,'+index+');" autocomplete="off"/>'+
         ' <font class="red"> * </font></li>'+
      '</ul><!-- END rightColumn Div-->'+
      /*---------------END Expiry Date, Security code------------------------------------*/
      '<br clear="all">'+
      /*--------Debit card section ----------------------------------------------------*/
	  '<fieldset id="issueNumberSection_'+index+'" class="debitCards hide">'+
         '<p>For Maestro / Solo Card users only - Please enter the issue number of your card. </p>'+
         '<ul class="leftColumn">'+
            '<li>'+
               '<label for="issueNumber">Issue Number</label>'+
			   '<input type="text" id="issueNumber_'+index+'" name="payment_'+index+'_issueNumber" class="small hide" maxlength="2" onBlur="FormHandler.handleIndividualValidations(this,'+index+');" autocomplete="off"/>'+
            '</li>'+
         '</ul>'+
         '<ul class="rightColumn">'+
            '<li class="validFrom">'+
               '<label for="validFrom">Valid From</label>'+
			   '<select id="validityMonth_'+index+'" name="payment_'+index+'_startMonth">'+
                  "<option value=''>MM</option>"+
                  "<option value='01'>01</option>"+
                  "<option value='02'>02</option>"+
                  "<option value='03'>03</option>"+
                  "<option value='04'>04</option>"+
                  "<option value='05'>05</option>"+
                  "<option value='06'>06</option>"+
                  "<option value='07'>07</option>"+
                  "<option value='08'>08</option>"+
                  "<option value='09'>09</option>"+
                  "<option value='10'>10</option>"+
                  "<option value='11'>11</option>"+
                  "<option value='12'>12</option>"+
               '</select>'+
			   '<select id="validityYear_'+index+'" name="payment_'+index+'_startYear">'+
                  requiredFields_startYear+
               '</select>'+
            '</li>'+
         '</ul>'+
      '</fieldset>'+
      /*--------END Debit card section --------------------------------------------------*/
   '</fieldset>'; //END dCardContent
   },

   CashPaymentContent :function(index, paymentMode)
   {
      return '<div class="paymentAcceptedCheckBox"><div class="cash"><input type="checkbox"  id="paymentAcceptedCheckBox_'+index+'" onclick="PaymentTakenChkBoxHandler.handle(this, this.value,'+index+')"/><span>'+
	     paymentMode+' Accepted</span></div><div>';
   },

   ChequePaymentContent : function(index, paymentMode)
   {
      return '<div class="paymentAcceptedCheckBox"><div class="cheque"><input type="checkbox"  id="paymentAcceptedCheckBox_'+index+'" onclick="PaymentTakenChkBoxHandler.handle(this, this.value,'+index+')"/><span>'+
	     paymentMode+' Accepted</span></div><div>';
   },

   VoucherPaymentContent : function(index, paymentMode)
   {
      var voucherContent = '<fieldset class="voucher">'+
	     '<div><input type="hidden" name="payment_'+index+'_vocIndex" id="payment_'+index+'_vocIndex" value="0" />'+
		 '<div class="leftColumn"><label for="cardNumber">Voucher Code</label>'+
		 '<input type="text" id="voucherCodes_'+index+'['+vocIndex[index]+']" name="payment_'+index+'_voucherSeriesCodes['+(vocIndex[index]-1)+']" maxlength="8" onBlur="FormHandler.handleNonEmptyIndividualValidations(this, '+index+');" class="voucherCode medium"/>'+
		 '</div><div class="rightColumn"><label for="voucherAmount_'+index+'_'+vocIndex[index]+'">Voucher Value</label>'+
		 '<input type="text" id="voucherAmount_'+index+'_'+vocIndex[index]+'" maxlength="8" value="'+PaymentDetailsContent.currencySymbol+'" onblur=" PaymentInfo.accumulateVoucherAmount(this,'+vocIndex[index]+','+index+');" onclick="PaymentView.refreshTheField(this);" class="voucherValue medium" />'+
		 '<input type="hidden" id="voucherCarry'+index+'_'+vocIndex[index]+'" name="payment_'+index+'_amountsPaidByVouchers['+(vocIndex[index]-1)+']" class="" />'+
		 '</div></div><div id="addanother'+index+'"></div>'+
		 '<span><label><a href="javascript:void(0);" onclick="PaymentDetailsContent.addAnotherVoucher('+index+');">Add another voucher</a></label></span><br clear="all"/>'+
		 '<div><label> Total Voucher Value</label><span id="totalVoucherVal_'+index+'" class="totalVoucherVal"></span></div>'+
			'<br clear="all"/><div class="paymentAcceptedCheckBox">'+
			'<input type="checkbox"  id="paymentAcceptedCheckBox_'+index+'" onclick="PaymentTakenChkBoxHandler.handle(this, this.value,'+index+')"/><span>'+paymentMode+' Accepted</span>'+
		 '</div></fieldset>';
      vocIndex[index] = (vocIndex[index])+1;
	  return voucherContent;
   },

   addAnotherVoucher : function(index)
   {
	  var anotherVoucher = '<br clear="all"/>'+
         '<div class="leftColumn"><label for="cardNumber">Voucher Code</label>'+
	     '<input type="text" id="voucherCodes_'+index+'['+(vocIndex[index])+']" name="payment_'+index+'_voucherSeriesCodes['+(vocIndex[index]-1)+']" maxlength="8" class="voucherCode medium" onBlur="FormHandler.handleNonEmptyIndividualValidations(this, '+index+');"/>'+
         '</div>'+
         '<div class="rightColumn">'+
         '<label for="voucherAmount_'+index+'_'+(vocIndex[index])+'">Voucher Value</label>'+
		 '<input type="text" id="voucherAmount_'+index+'_'+(vocIndex[index])+'" maxlength="8" class="voucherValue medium" value="'+PaymentDetailsContent.currencySymbol+'" onblur="PaymentInfo.accumulateVoucherAmount(this,'+(vocIndex[index]-1)+','+index+');" onclick="PaymentView.refreshTheField(this);"/>'+
		 '<input type="hidden" id="voucherCarry'+index+'_'+(vocIndex[index])+'" name="payment_'+index+'_amountsPaidByVouchers['+(vocIndex[index]-1)+']" class="" />'+
	     '</div>'+'<br clear="all"/>';
	  $("#payment_"+index+"_vocIndex").val(vocIndex[index]-1);
	  var vocValue = vocIndex[index]++;
	  $(anotherVoucher).appendTo("#addanother"+index);
   },

   GiftCardPaymentContent : function(index)
   {
      return '<fieldset class="giftCard"><div>'+
	     '<ul class="leftColumn">'+
	     '<li class="giftCard">'+
         '<label for="cardNumber">Card Number</label>'+
         '<input type="text" id="cardNumber" name="payment_'+index+'_cardNumber" onblur="FormHandler.handleIndividualValidations(this,'+index+');"/>'+
         '<font class="red"> * </font>'+
	     '</div></li></ul>'+'<ul class="rightColumn"><li><div>'+
         '<label for="cardNumber">Security code<br/><span id="smalltext" class="smalltext_'+index+' smallFont">(last 4 digits in the signature strip on the reverse of your card)</span></label>'+
         '<input type="text" id="securityCode_'+index+'" name="payment_'+index+'_securityCode" class="small" onBlur="FormHandler.handleIndividualValidations(this,'+index+');"/>'+
         '<font class="red"> * </font>'+
	     '</div></li></ul></fieldset>';
   },

   CardRefundPaymentContent : function(index)
   {
      var requiredFields_rfndCard = '';
	  var token = $("#token").val();
	  var tomcatInstance = $("#tomcatInstance").val();
	  var url="/cps/RefundOptionServlet?transactionIndex="+index+"&token="+token;
	  url = uncache(url);
	  url += "&tomcat="+tomcatInstance;
	  indexForROResponse = index;
	  var request = $.ajax({url: url, type:'get', async: false}).responseText;
	  var index = indexForROResponse;
	  var temp = request;
	  var responseTextValue = temp.split("~");
	  radioMaxCount = responseTextValue[0];
	  requiredFields_rfndCard += responseTextValue[1];
	  var refundContent = '<fieldset class="CardRefund"><div id="cardRefundArea"'+index+'">'+requiredFields_rfndCard+'</div></fieldset>';
	  return refundContent;
   },

   CardPaymentContent : function(index, paymentMode)
   {
      return '<fieldset class="giftCard">'+
	     ''+ //PaymentDetailsContent.cardChargeAndTotalAmountContent(index)+
			 '<div class="paymentAcceptedCheckBox" ><div ><input type="checkbox"  id="paymentAcceptedCheckBox_'+index+'" onclick="PaymentTakenChkBoxHandler.handle(this, this.value,'+index+')"/><span>'+
		 paymentMode+' Payment Accepted</span></div></fieldset>';
   },

   NoPaymentPaymentContent : function(index, paymentMode, paymentType)
   {
      var noPaymentContentText = '';
	  if (StringUtils.equalsIgnoresCase(paymentType, "RR") && StringUtils.equalsIgnoresCase(paymentMode, "NoPayment"))
	  {
	     noPaymentContentText += 'Refund Cheque';
	  }
	  else
	  {
		 noPaymentContentText += 'Payment Accepted';
	  }
	  return '<fieldset class="giftCard">'+
		'<div><input type="checkbox" class="paymentAcceptedCheckBox" id="paymentAcceptedCheckBox_'+index+'" onclick="PaymentTakenChkBoxHandler.handle(this, this.value,'+index+')"/>'+noPaymentContentText+' </div>'+
         '</fieldset>';
   },

   CardPinPadPaymentContent : function()
   {
	   return "<div id='pinpadSection'></div>";
   }
};
/**=====================PaymentDetailsContent======================================================*/
//++++++++++++++++++++++END View Layer ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


//++++++++++++++++++++++Controller Layer+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**=====================GlobalHandler==============================================================*/
/**
 * Global Handler to contain global/common functions of handlers
 */
var GlobalHandler =
{
   //This variable is to check if any of the payments are committed. Used in deposit type change handler.
   isAnyTransactionCommitted : false,

   //setter for isAnyTransactionCommitted
   setIsAnyTransactionCommitted: function(transCommittedStatus)
   {
      this.isAnyTransactionCommitted = transCommittedStatus;
   },

   /** Common function to check whether data entry is being done sequentially. If user has selected 2 transactions,
    * he has to finish entering first transaction details first and then go for second transaction
    */
   sequentialDataEntryCheck : function(index)
   {
      if(index>0)
      {
         blockToBeValidated = document.getElementById("paymentSection"+(index-1));
         var errorObject = FormValidator.validateBlocks(blockToBeValidated,'',index-1);
         if(StringUtils.isNotBlank(errorObject.errorMsg))
         {
            return PaymentView.showAlert("Please select transaction fields in sequential order!",errorObject.element);
         }
      }
      return true;
   },

   refreshTheIndexedSection: function(index)
   {
      $('#paymentSection'+index).html(PaymentDetailsContent.paymentSectionTemplateIndexed(index));
      PaymentView.updateLastTransactionAmountField(1*PaymentInfo.getCalculatedPayableAmount()-1*PaymentInfo.getPaidAmount());
	  NoOfPaymentsChangeHandler.disableLastTransAmount()
   	  PaymentView.updateDepositSection(PaymentInfo);
	  PaymentView.showCardChargeInSummaryPanel(1*PaymentInfo.getCalculatedPayableAmount() - 1*PaymentInfo.getTotalAmount());
   },

   /**
    *
    */
   updateAmounts:function()
   {

   },

   /**
    *
    */
   refreshToDefault : function()
   {
      NoOfPaymentsChangeHandler.refresh();
      NoOfPaymentsChangeHandler.handle(1);
	  PaymentView.updateDepositSection(PaymentInfo);
	  PaymentView.showCardChargeInSummaryPanel(0);
   },

  // Gets all the radio buttons from the div with class depositSection, finds which one is set and update that deposit type to
  // PaymentInfo.depositType
   updateDepositType: function()
   {
      PaymentInfo.setDepositType ($('div .depositSection input[type=radio]:Checked').val());
   }
   ,

   setToPreviousSelection : function()
   {
      if (StringUtils.equalsIgnoresCase($("#datacashEnabled").val(), "false") || !StringUtils.equalsIgnoresCase(callType, 'DEFAULT'))
	  {
	     typeOfCustomer = PaymentDetailsContent.customerType;
	  }
      jQuery('#'+typeOfCustomer.toLowerCase()+'').attr("checked", "checked");
      PaymentDetailsContent.setCustomerType(typeOfCustomer);
      GlobalHandler.setIsAnyTransactionCommitted(false);
      jQuery('#noOfPayments').val(1);
      NoOfPaymentsChangeHandler.handle(1);
      if(selectedDepositType != null && selectedDepositType != "")
      {
         var depositsTypes = document.getElementsByName("depositType");
         if(depositsTypes && depositsTypes !=undefined && depositsTypes !=null)
         {
            for(var i=0;i<depositsTypes.length; i++)
            {
               if(depositsTypes[i].value == selectedDepositType)
               {
                  PaymentInfo.depositType = selectedDepositType.toUpperCase();
                  depositsTypes[i].checked = true;
               }
            }
         }
       }
       this.setPaymentType(0);
       PaymentTypeChangeHandler.handle(document.getElementById("paymentType_0"), 0);
       var expiryArray=expiryDate;
       expiryArray=expiryArray.split("/");
       var expiryMonth= expiryArray[0];
       var expiryYear=expiryArray[1];
       $('#expiryMonth_0').val(expiryMonth);
       $('#expiryYear_0').val(expiryYear);
       $('#cardNumber_0').val(maskedCardNumber);
       $('#nameOnCard_0').val(name);
       $('#postCode_0').val(postCode);
       $('#securityCode_0').val(maskedCvv);
       if($('#issueNumber_0'))
          $('#issueNumber_0').val(issueNumber);
       if(responseMsg == "REFERRED")
       {
          $('#authCodeReferred_0').removeClass("hide");
       }
       $('#datacashError_0').removeClass("hide");
   },

   setPaymentType : function(index)
   {
      for(var j=0;j<document.getElementById('paymentType_'+index).length;j++)
      {
         if((document.getElementById('paymentType_'+index).options[j].value.split("|"))[0]==cardType)
         {
            document.getElementById('paymentType_'+index).selectedIndex=j;
         }
      }
   }
};
/**=====================END GlobalHandler==========================================================*/

/**=====================CustomerTypeChangeHandler==================================================*/
var CustomerTypeChangeHandler=
{
   handle: function(value)
   {
      GlobalHandler.setIsAnyTransactionCommitted(false);
      PaymentDetailsContent.setCustomerType(value);
      GlobalHandler.refreshToDefault();
   }

};
/**=====================END CustomerTypeChangeHandler==============================================*/

/**=====================NoOfPaymentsChangeHandler==================================================*/
/**
 * Handler for NoOfPayments drop down change
 */
var NoOfPaymentsChangeHandler =
{
   /** Holds NoOfPayments selection. */
   existingSelection:0,

   /**
    *once payment is taken, we can not change no of payments for non Dcard payments.
    *This variable should be set to false in that case.
    */
   isNoOfPaymentsChangeAllowed:true,

   /**
    * Function to handle the change in NoOfPayments drop down to increase or decrease,
    * no of payment transactions
    *
    * @param newSelection - selected value in drop down.
    */
   handle:function(newSelection)
   {
      //If selection is not allowed(e.g- booking is done in case of non-dcards)
	  //or existingSelection is equal to newSelection then no action should take place.
      if(!this.isNoOfPaymentsChangeAllowed || (this.existingSelection==newSelection))
      {
	     PaymentView.showAlert("This action is not allowed.", $("#noOfPayments"));
	     $("#noOfPayments").val(this.existingSelection);
         return false;
      }

      //Remove existing payment transaction sections
      PaymentView.removePaymentTransactionSections();

      this.existingSelection = newSelection;

      //Add new PaymentTransactionSections
      for(var index=newSelection-1;index>=0;index--)
      {
         var obj = PaymentDetailsContent.paymentSectionTemplate(index,newSelection);
         $(obj).insertAfter("#noOfPaymentsBlock");
	     this.disableLastTransAmount();
      }

      // Update payment info
      PaymentInfo.refresh();
      PaymentView.showCardChargeInSummaryPanel(0);

      //Update payment page with changed amounts
      PaymentView.updatePaymentSection(PaymentInfo);
      PaymentView.updateLastTransactionAmountField(PaymentInfo.getPayableAmount());

   },

   disableLastTransAmount : function()
   {
      $(".transAmt:last").attr("disabled", "disabled");
	  $(".paidAmount").attr("disabled", "disabled");
	  $(".amountStillDue").attr("disabled", "disabled");
	  $(".calculatedPayableAmount").attr("disabled", "disabled");
   },

   /**
    * Refreshes the instance variables in NoOfPaymentsChangeHandler
    */
   refresh : function()
   {
      this.existingSelection = 0;
      PaymentView.setDropDownsToDefault(document.getElementById("noOfPayments"));
      this.isNoOfPaymentsChangeAllowed = true;
   }
};
/**=====================END NoOfPaymentsChangeHandler==============================================*/

/**=====================PaymentTypeChangeHandler===================================================*/
/**
 * Handler for PaymentType drop down change
 */
var PaymentTypeChangeHandler =
{
   /**
    *
    */
   handle : function(paymentTypeObj, index)
   {
      if($('#datacashError_'+index))
	     $('#datacashError_'+index).addClass("hide");
	  $('#authCodeReferred_'+index).addClass("hide");
      //Checks whether data is being entered sequentially
      if(!GlobalHandler.sequentialDataEntryCheck(index))
      {
         PaymentView.setDropDownsToDefault(paymentTypeObj);
         PaymentView.refreshTheField(paymentTypeObj);
         return false;
      }
      if(StringUtils.isBlank(paymentTypeObj.value))
      {
	     // If any transaction is committed then disallow the change else refresh the page to default.
		 if (GlobalHandler.isAnyTransactionCommitted)
		 {
		    PaymentView.showAlert("This action is not allowed", paymentTypeObj)
			GlobalHandler.refreshTheIndexedSection(index);
		 }
		 else
		 {
		    GlobalHandler.refreshToDefault();
		 }
    	 return false;
	  }

      var tmpArray = paymentTypeObj.value.split("|");
      //VISA, AMERICAN_EXPRESS
      var paymentType = tmpArray[0];
      //Cash, Dcard etc.
      var paymentMode = tmpArray[1];
      if (paymentMode == "Voucher")
	  {
         vocIndex[index]=0;
         vocIndex[index] = (vocIndex[index])+1;
      }
	  //Hide the auth code section if paymenttype = Card
      PaymentView.displayAuthCodeSection(index,paymentMode);

      if(paymentMode == 'CardPinPad')
      {
    	  PaymentView.displayPinpadLI(index,false);
          PaymentView.displayStartButton(index,paymentMode);
      }
      //Add payment content like DCard, cash , cheque voucher based on payment type selection and display it.
      PaymentView.addPaymentContent(index, paymentMode, paymentType);
      PaymentView.displayIssueNumberSection(index, paymentType);
      PaymentView.displaySecurityCodeText(index, paymentType);



	  PaymentView.displayTransAmt(index, paymentMode, paymentType);


      var transactionAmount = PaymentView.removeCurrencySymbol($("#transAmt_"+index).val());

      if(NoOfPaymentsChangeHandler.existingSelection == 1)
      {
         transactionAmount = PaymentInfo.getPayableAmount();
      }

      var isTransactionCommitted = true;
      if(document.getElementById("paymentAcceptedCheckBox_"+index) || paymentMode == 'CardPinPad')
      {
         isTransactionCommitted = false;
      }

      //Add the TransactionDetails to PaymentInfo and update amounts
      PaymentInfo.addTransactionDetails(index,paymentType, paymentMode, transactionAmount, isTransactionCommitted);
      PaymentInfo.updateCalculatedAmounts();

      //Update Payment page with latest amounts
      //PaymentView.updatePaymentPage(PaymentInfo);

      // This section is added because for cp transaction card charge is to be displayed in the summary panel after commitment whereas for cnp/cpna
      PaymentView.updatePaymentSection(PaymentInfo);

      if (PaymentDetailsContent.customerType != "CP")
      {
         //Update the deposit sections
         PaymentView.updateDepositSection(PaymentInfo);

         //Display card charge in summary panel.
         PaymentView.showCardChargeInSummaryPanel(PaymentInfo.getCalculatedCardCharge());
      }
   }
};
/**=====================END PaymentTypeChangeHandler===============================================*/

/**=====================TransAmtHandler============================================================*/
var TransAmtHandler =
{
   handle : function(transAmtObj, transAmt, index)
   {
      //---------------------------------------validation-----------------------------
      //Check whether data entry is made sequentially
      if(!GlobalHandler.sequentialDataEntryCheck(index))
      {
         //PaymentView.refreshTheField(transAmtObj);
         return false;
      }

      //Check whether entered amount is a valid input.
      var validationResult = FormValidator.validateField(transAmtObj,'',index);
      if(StringUtils.isNotBlank(validationResult.errorMsg))
      {
         PaymentView.showAlert(validationResult.errorMsg,validationResult.element);
         return PaymentView.refreshTheField(validationResult.element);
      }

      //---------------------------------------Validation-----------------------------------
      //This logic is to remove the pound symbol
      if (!(transAmt.indexOf(PaymentDetailsContent.currencySymbol) == -1))
      {
	     transAmt = StringUtils.stripFirstChar(transAmt);
      }

      //Update TransactionDetails to PaymentInfo and update amounts
      PaymentInfo.updateTransAmtTransactionDetails(index,transAmt);

      PaymentInfo.updateCalculatedAmounts();
      if(PaymentInfo.getAccumulatedAmount() > PaymentInfo.getPayableAmount())
      {
         PaymentView.showAlert("Amount entered exceeds total amount due. Please re-enter",transAmtObj);
         return PaymentView.refreshTheField(transAmtObj);
      }

      //Update payment page and last transaction field
      PaymentView.updatePaymentPage(PaymentInfo);
      PaymentView.updateLastTransactionAmountField(PaymentInfo.getPayableAmount()-PaymentInfo.getAccumulatedAmount());
   }
};
/**=====================END TransAmtHandler=========================================================*/
/**
 *
 */
var PaymentTakenChkBoxHandler =
{
   /**
    *
    */

   handle : function(checkBoxObj , checkBoxObjValue, index )
   {
	      var transactionAmount = PaymentView.removeCurrencySymbol($('#transAmt_'+index).val());
		  if (StringUtils.isNotEmpty(transactionAmount))
		  {
			  PaymentInfo.updateTransactionDetails(index, true);
			  PaymentInfo.updateCalculatedAmounts();
			  PaymentView.updatePaymentSection(PaymentInfo);
			  PaymentView.showCardChargeInSummaryPanel(PaymentInfo.getCalculatedCardCharge());
			  PaymentView.updateDepositSection(PaymentInfo);
			  this.disableAfterPayments(index);
			  NoOfPaymentsChangeHandler.isNoOfPaymentsChangeAllowed = false;
			  GlobalHandler.setIsAnyTransactionCommitted(true);
		  }
		  else
		   {
	          $('#paymentAcceptedCheckBox_'+index).attr('checked',false);
		   }
   },

   disableAfterPayments: function(index)
   {
      $('#transAmt_'+index).attr("disabled", "disabled");
	  $("#paymentType_"+index).attr("disabled", "disabled");
	  $("#paymentAcceptedCheckBox_"+index).attr("disabled", "disabled");
   }
};
/**=====================END PaymentTakenChkBoxHandler==============================================*/

/**=====================DepositTypeChangeHandler===================================================*/
var DepositTypeChangeHandler =
{
   /**
    *
    */
   handle: function(depositTypeObj, depositTypeValue)
   {
      if (!GlobalHandler.isAnyTransactionCommitted)
      {
         PaymentInfo.setDepositType(depositTypeValue.toUpperCase());
         PaymentInfo.updatePayableAmount();
         GlobalHandler.refreshToDefault();
      }
      else
      {
         var Msg = 'Change from ';
         for (var i = 0; i < document.paymentdetails.depositType.length; i++)
         {
            if (PaymentInfo.getDepositType() == document.paymentdetails.depositType[i].value)
            {
               document.paymentdetails.depositType[i].checked = true;
               Msg += (document.paymentdetails.depositType[i].value).toUpperCase();
               break;
            }
         }
         Msg += ' is not applicable, once payment is done.';
         PaymentView.showAlert(Msg,depositTypeObj)
      }
   }
};
/**=====================END DepositTypeChangeHandler===============================================*/

/**=====================FormSubmissionHandler======================================================*/
var FormHandler =
{
   /**
    * This method is responsible for submitting the payment page with necessary client side validations.
    */
   handleSubmission:function(formObj)
   {
	  var amountsCheckDone = false;
	  var paymentSections = $(".paymentSection");
	  for (index=0; index < paymentSections.length ; index++)
      {
	     var ValidationResult = FormValidator.validateBlocks(paymentSections[index],'', index);
		 if(StringUtils.isNotBlank(ValidationResult.errorMsg))
         {
            PaymentView.showAlert(ValidationResult.errorMsg,ValidationResult.element);
		    return PaymentView.refreshTheField(ValidationResult.element);
		 }
      }

      if($("#noOfPayments").val() ==1)
	  {
		  amountsCheckDone = true;
		  if (!(this.validateAmounts()))
		  {
			 PaymentView.showAlert("Amount Paid should match total amount.",null);
			 return false;
	      }
	  }

      if (!amountsCheckDone)
	  {
	     if (!(this.validateAmounts()))
		 {
		    PaymentView.showAlert("Amount Paid should match total amount.",null);
			return false;
	     }
	  }
      this.updateEssentialFields();
      formObj.submit();
   },

   /**
    *  This function validates the total amount paid and payable amount.
    */
   validateAmounts : function()
   {
      if (MathUtils.roundOff(PaymentInfo.getCalculatedPayableAmount(),2) != MathUtils.roundOff(PaymentInfo.getPaidAmount(),2))
	  {
         return false;
	  }
	  return true;
   },

   /**
    *  This function updates the total_transamt which is used for the amounts verification between calculated amounts and total amount.
    */
   updateEssentialFields : function()
   {
      $("#total_transamt").val(PaymentInfo.getCalculatedPayableAmount());
      var transactionDetMapValueList = PaymentInfo.getTransactionDetailsMap().getAllValuesList();
      for(var index in transactionDetMapValueList)
      {
         cardCharges = 1*transactionDetMapValueList[index].cardCharge;
         transAmtWithCardCharge = 1*transactionDetMapValueList[index].transactionAmount + cardCharges;

		 //Set the input parameters for use in the server side. (hidden fields)
		 $("#transAmountPaid_"+index).val(MathUtils.roundOff(1*transactionDetMapValueList[index].transactionAmount,2));
		 $("#transCardCharge_"+index).val(MathUtils.roundOff(1*cardCharges,2));
		 $("#paymenttypecode_"+index).val(transactionDetMapValueList[index].paymentType);
		 $("#paymentmethod_"+index).val(transactionDetMapValueList[index].paymentMode);

	  }
   },

   /**
    * This function handles the onBlur validations of all fields.
    */
   handleIndividualValidations: function(fieldObj, index)
   {
      var validationResult = FormValidator.validateField(fieldObj,'',index);
      if(StringUtils.isNotBlank(validationResult.errorMsg))
      {
          PaymentView.showAlert(validationResult.errorMsg,validationResult.element);
          return PaymentView.refreshTheField(validationResult.element);
      }
   },

   /**
    * This function handles the onBlur validations of fields with values.
    */
   handleNonEmptyIndividualValidations: function(fieldObj, index)
   {
      if(StringUtils.isNotEmpty(fieldObj.value))
      {
         var validationResult = FormValidator.validateField(fieldObj,'',index);
         if(StringUtils.isNotBlank(validationResult.errorMsg))
         {
            PaymentView.showAlert(validationResult.errorMsg,validationResult.element);
            return PaymentView.refreshTheField(validationResult.element);
         }
      }
   }
};
/**=====================FormSubmissionHandler======================================================*/

//++++++++++++++++++++++END Controller Layer+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//++++++++++++++++++++++Model Layer++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**=====================PaymentInfo================================================================*/
var PaymentInfo =
{

   /** total amount which will not be updated with card charges. */
   totalAmount :"",

   /** Payable amount which will not be updated with card charges. */
   payableAmount : "",

   /** TotalAmount which will  be updated with card charges. */
   calculatedTotalAmount : "",

   /** Payable amount which will  be updated with card charges. */
   calculatedPayableAmount : "",

   /** Discounts like promo code discounts. */
   calculatedDiscount:0,

   /** max CardCharge Amount allowed for a card transaction. Initial value is zero */
   //maxCardChargeAmount : null,

   /** total card charges calculated for the payment . */
   calculatedCardCharge  :0.0,


   /** used to hold selected deposit type value .  */
   depositType:"",

   /** summation of trans amounts+calculatedCardCharge.  */
   paidAmount : 0.0,

   /** summation of trans amounts(excluding last transamount). Initially it is 0.0.*/
   accumulatedAmount : 0.0,

   /** map to contain transactionDetails values with key as (transaction)index.  */
   transactionDetailsMap : null,

   /** Total voucher value. */
   totalVoucherValue : 0.0,

   setTotalAmount:function(totalAmount) { this.totalAmount = totalAmount},
   getTotalAmount:function() {return this.totalAmount },

   /*setMaxCardChargeAmount:function(maxCardChargeAmount) { this.maxCardChargeAmount = maxCardChargeAmount},
   getMaxCardChargeAmount:function() {return this.maxCardChargeAmount },*/

   setPayableAmount:function(payableAmount) { this.payableAmount = payableAmount},
   getPayableAmount:function() {return this.payableAmount },

   setCalculatedTotalAmount:function(calculatedTotalAmount) { this.calculatedTotalAmount = calculatedTotalAmount},
   getCalculatedTotalAmount:function() { return this.calculatedTotalAmount},

   setCalculatedPayableAmount:function(calculatedPayableAmount){this.calculatedPayableAmount = calculatedPayableAmount },
   getCalculatedPayableAmount:function(){return this.calculatedPayableAmount},

   setCalculatedDiscount: function(calculatedDiscount) {this.calculatedDiscount = calculatedDiscount },
   getCalculatedDiscount: function(){this.calculatedDiscount = calculatedDiscount },

   /** Caclulated card charge for selectedCardType. */
   getCalculatedCardCharge: function() {return this.calculatedCardCharge},
   getPaidAmount : function() {return this.paidAmount ;  },

   getAccumulatedAmount : function()  {    return this.accumulatedAmount;  },
   setDepositType:function(depositType) { this.depositType = depositType},
   getDepositType:function() { return this.depositType },

   /** Following functions return maps which are created on jsp and populated from server side values and */
   getCardChargeMap : function() { return cardChargeMap} ,

   /** Returns DepositAmount Map */
   getDepositAmountMap : function() { return depositAmountsMap},

   getTransactionDetailsMap : function() {return this.transactionDetailsMap },

   /*
    * Function to be called from JSP to initialize values are which are not sent
    * from server side in-order to avoid script errors.
    */
   init: function()
   {
      if(isNaN(1*this.calculatedDiscount))
      {
         this.calculatedDiscount = 0
      }
      /*if(isNaN(1*this.maxCardChargeAmount))
      {
         this.maxCardChargeAmount = null;
      }*/
   },

   /**
    *
    */
   refresh: function()
   {
      this.paidAmount = 0.0;
      this.accumulatedAmount = 0.0;
      this.calculatedCardCharge = 0.0;
      this.calculatedPayableAmount = this.payableAmount;
      this.calculatedTotalAmount = this.totalAmount;
      this.transactionDetailsMap = null;
      this.transactionDetailsMap = new Map();
   },

   resetTransactionDetails : function(index)
   {
	   var transactionDetMapValueList = this.transactionDetailsMap.getAllValuesList();
	   paymentType = transactionDetMapValueList[index].paymentType;
	   paymentMode = transactionDetMapValueList[index].paymentMode;
	   transactionAmount = transactionDetMapValueList[index].transactionAmount;
	   cardCharge = 0;
	   isTransactionCommitted = false;
	   this.transactionDetailsMap.put(index,new TransactionDetails(paymentType, paymentMode,  transactionAmount, cardCharge, isTransactionCommitted));
   },

   /**
    *
    */
   updatePaymentInfo:function()
   {
      this.updatePayableAmount();
	  this.updateCalculatedAmounts();
   },

   /**
    *  Based on depositType selection we shall update payableAmount
    */
   updatePayableAmount :  function()
   {
	   if(PaymentInfo.getDepositType() != undefined)
	   {
         this.payableAmount =  this.getDepositAmountMap().get(this.depositType);
	     this.totalAmount =this.getDepositAmountMap().get(this.depositType);
	     if(this.payableAmount==null)
	     {
	        parseFloat(1*this.totalAmount  - 1*this.calculatedDiscount).toFixed(2)
	     }
	   }
   },


   /**
    *
    */
   updateCalculatedAmounts : function()
   {
      var transactionDetMapValueList = this.transactionDetailsMap.getAllValuesList();
      var totalCardCharge = 0.0;
      var totalTransAmount = 0.0;
      var totalAccumulatedAmount = 0.0;
      for(var index in transactionDetMapValueList)
      {
    	 //alert("Card Charge:"+ transactionDetMapValueList[index].cardCharge +"\n TransAmt:" + transactionDetMapValueList[index].transactionAmount+"\n isTxnCommitted:"+ transactionDetMapValueList[index].isTransactionCommitted)
         totalCardCharge += 1*transactionDetMapValueList[index].cardCharge;
         totalAccumulatedAmount += 1*transactionDetMapValueList[index].transactionAmount;
         if(transactionDetMapValueList[index].isTransactionCommitted)
         {
            totalTransAmount += 1*transactionDetMapValueList[index].transactionAmount;
         }
      }
      this.calculatedCardCharge = totalCardCharge;
	  if (transactionDetMapValueList[index].isTransactionCommitted )
	  {
         this.paidAmount = totalTransAmount+ totalCardCharge;
	     this.calculatedPayableAmount = MathUtils.roundOff(1*this.payableAmount + 1*this.calculatedCardCharge , 2);
         this.calculatedTotalAmount = MathUtils.roundOff((1*this.totalAmount - 1*this.calculatedDiscount) + 1*this.calculatedCardCharge , 2);
	  }
      this.accumulatedAmount = totalAccumulatedAmount;
      //alert("CalculatedCardCharge:"+this.calculatedCardCharge+"\nAmount Paid:"+ this.paidAmount +"\n total amt:"+this.calculatedTotalAmount)
   },

   /**
    *
    */
   addTransactionDetails : function(index,paymentType, paymentMode, transactionAmount, isTransactionCommitted)
   {
      var cardCharge = this.getCardCharge(paymentType, transactionAmount);
      this.transactionDetailsMap.put(index,
                                   new TransactionDetails(paymentType, paymentMode,  transactionAmount, cardCharge, isTransactionCommitted));
   },

   addTransactionDetailsWithCardCharge : function(index,paymentType, paymentMode, transactionAmount, isTransactionCommitted, cardCharge)
   {
	   this.transactionDetailsMap.put(index,
               new TransactionDetails(paymentType, paymentMode,  transactionAmount, cardCharge, isTransactionCommitted));
   },

   updateTransAmtTransactionDetails : function(index, transactionAmount)
   {
      transactionDetails = this.transactionDetailsMap.get(index);
      var paymentType = transactionDetails.paymentType;
      var cardCharge = this.getCardCharge(paymentType, transactionAmount);
      transactionDetails.setTransactionAmount(transactionAmount);
      transactionDetails.setCardCharge(cardCharge);
      this.transactionDetailsMap.put(index, transactionDetails);
   },

   updateTransactionDetails : function(index, isTransactionCommitted)
   {

      transactionDetails = this.transactionDetailsMap.get(index);
      transactionDetails.setIsTransactionCommitted(isTransactionCommitted);
      this.transactionDetailsMap.put(index, transactionDetails);
   },

   updateTransactionDetailsWithCardCharge : function(index, cardCharge)
   {
	  transactionDetails = this.transactionDetailsMap.get(index);
	  transactionDetails.cardCharge = cardCharge;
	  this.transactionDetailsMap.put(index, transactionDetails);
   },

   getCardCharge : function(paymentType, amount)
   {
      //If amount is not a number then return 0 as card charge
      if(StringUtils.isEmpty(amount) || isNaN(1*amount))
      {
         return 0.0;
      }

      //Calculate card charge for the amount provided

      if (this.getCardChargeMap() == null || this.getCardChargeMap().get(paymentType) == null || this.getCardChargeMap().get(paymentType) == 0)
      {
         return 0.0;
      }

      var chargeData = this.getCardChargeMap().get(paymentType).split(",");

      var chargePercent = chargeData[0];
      var minCardCharge = chargeData[1];
      var maxCardCharge = chargeData[2];

      var cardCharge = MathUtils.roundOff((1*amount * (chargePercent/100)),2);

      if (minCardCharge != null)
      {
         cardCharge = (cardCharge < 1*minCardCharge) ? 1*minCardCharge : cardCharge;
      }

      if (maxCardCharge != null)
      {
         cardCharge = (cardCharge > 1*maxCardCharge) ? 1*maxCardCharge : cardCharge;
      }
      return cardCharge;
   },


   accumulateVoucherAmount : function(obj, voucherIndex, index)
   {
      var max = vocIndex[index];
      var value= obj.value;
      // this block is to remove the currency symbol if any exist in the field
      if(!(value.indexOf(PaymentDetailsContent.currencySymbol) == -1))
	  {
	     value = StringUtils.stripFirstChar(value);
	  }
	  if(StringUtils.isNotEmpty(value))
	  {
         var validationResult = FormValidator.validateField(obj,'',voucherIndex);
	     if (StringUtils.isNotEmpty(validationResult.errorMsg))
	     {
	        PaymentView.showAlert(validationResult.errorMsg,validationResult.element);
            return PaymentView.refreshTheField(validationResult.element);
	     }
	  }
      var  totalVocAmount= 0;
      var vocAmount = 0;
	  for (i=1; i<max; i++)
      {
         if ($("#voucherAmount_"+index+"_"+i) && StringUtils.isNotBlank($("#voucherAmount_"+index+"_"+i).val().replace(PaymentDetailsContent.currencySymbol, "")))
         {
            if ($("#voucherAmount_"+index+"_"+i).val().indexOf(PaymentDetailsContent.currencySymbol)!=-1)
            {
               vocAmount = $("#voucherAmount_"+index+"_"+i).val().replace(PaymentDetailsContent.currencySymbol, "");
            }
            else
            {
               vocAmount = $("#voucherAmount_"+index+"_"+i).val().replace(PaymentDetailsContent.currencySymbol, "");
            }
            $("#voucherCarry"+index+"_"+i).val(vocAmount);
            if (vocAmount.length > 0)
               totalVocAmount+=parseFloat(StringUtils.stripChars(vocAmount,PaymentDetailsContent.currencySymbol));
         }
         this.totalVoucherValue =  parseFloat(totalVocAmount);
      }
	  $("#totalVoucherVal_"+index).html(PaymentDetailsContent.currencySymbol + MathUtils.roundOff(this.totalVoucherValue,2));
   }
};
/**=====================PaymentInfo================================================================*/

/**=====================CardDetails================================================================*/
/**
 *
 */
function CardDetails(securityCodeLength, minSecurityCodeLength, isIssueNumberRequired)
{
   this.securityCodeLength = securityCodeLength;
   this.minSecurityCodeLength = minSecurityCodeLength;
   this.isIssueNumberRequired = isIssueNumberRequired;
}
/**=====================CardDetails================================================================*/

/**=====================TransactionDetails=========================================================*/
/*
 * isTransactionCommitted : true or false ,
 */
function TransactionDetails(paymentType, paymentMode,  transactionAmount, cardCharge, isTransactionCommitted)
{
   this.paymentType = paymentType;
   this.paymentMode = paymentMode;
   this.transactionAmount = transactionAmount;
   this.cardCharge = cardCharge;
   this.isTransactionCommitted = isTransactionCommitted;
   this.setPaymentType = function (paymentType)
   {
      this.paymentType = paymentType;
   };
   this.setTransactionAmount = function(transactionAmount)
   {
      this.transactionAmount = transactionAmount;
   };
   this.setIsTransactionCommitted = function(isTransactionCommitted)
   {
      this.isTransactionCommitted = isTransactionCommitted;
   };
   this.setCardCharge = function(cardCharge)
   {
      this.cardCharge = cardCharge;
   }
}
/**=====================TransactionDetails=========================================================*/

/**=====================AppConfig==================================================================*/
var AppConfig =
{
   /** BRAC, WiSH etc. */
   appName : ""
};
/**=====================END AppConfig==============================================================*/

/**=====================FormValidator==============================================================*/
/**
 *
 **/
var FormValidationRules  =
{
   transAmt:
   {
      msgBlank :" Please enter the Transaction Amount",
	  msgInvalid :" Please enter a valid Transaction Amount",
      isOptional:false,
      dependencyFields: "paymentType_",
      specificValidationExists:false,
	  isAmount : true,
	  isToBeRefreshed: true,
	  regularExpression : /^[0-9.]*$/
   },

   paymentAcceptedCheckBox:
   {
      msgBlank :" ",
	  msgInvalid :" ",
      isOptional:false,
      dependencyFields: " ",
      specificValidationExists:false,
      specificValidationFunction : function() {      }
   },

   paymentType:
   {
      msgBlank :"Please select a payment type",
	  msgInvalid :"Please select a payment type",
      isOptional:false,
      dependencyFields: " ",
      specificValidationExists:false,
      specificValidationFunction : function()
	  {

      }
   },

   voucherAmount:
   {
      msgBlank :"Please enter a valid Amount.",
	  msgInvalid :"Enter a valid voucher amount.",
      isOptional:false,
	  isToBeRefreshed:false,
      dependencyFields: "",
      specificValidationExists:false,
	  isAmount:true,
      regularExpression : /^[0-9.]*$/
   },

   voucherCodes:
   {
      msgBlank :"Enter a valid voucher series code.",
	  msgInvalid :"Enter a valid voucher series code.",
	  msgInvalidSpecific : "Please enter voucher code length as 8",
      isOptional:false,
	  isToBeRefreshed:false,
      dependencyFields: "",
      specificValidationExists:true,
      isAmount:false,
      specificValidationFunction : function(voucherCode,index)
	  {
	     if (StringUtils.isNotEmpty(voucherCode.value) && (voucherCode.value.length < 8))
		 {
		    return false;
		 }
		 return true;
	  },
      regularExpression : /^[-a-zA-Z0-9 \\\/.,]*$/
   },

   cardNumber:
   {
      msgBlank :"Your card number is required to complete your booking. Please enter your card number.",
	  msgInvalid :"Please enter a valid card number.",
      isOptional:true,
      dependencyFields: "",
      specificValidationExists:true,
	  isAmount:false,
	  isToBeRefreshed:true,
	  specificValidationFunction : function(cardNumber, index)
	  {
	  	 if (StringUtils.substringBefore(cardNumber.id, "_") == 'cardNumber' && (maskedCardNumber != cardNumber.value))
	     {
	     if (StringUtils.isNotBlank(cardNumber.value))
		 {
		    //TODO:The specific regular expressions can be configured here too instead of configuring the max and min lengths
		    var minLength = 16;
		    var maxLength = 20;
		    if ($("#paymentType_"+index).val() == 'AMERICAN_EXPRESS|Dcard')
		    {
			   maxLength = 15;
			   minLength = 15;
		    }
		    if (cardNumber.value.length> maxLength || cardNumber.value.length < minLength)
		    {
			   return false;
		    }
		 }
		 }
		 return true;
	  },
	  luhnCheck : function(cardnumber)
	  {
         var oddoreven = cardnumber.length & 1;
         var sum = 0;
         var addition = "";

         for (var count = 0; count < cardnumber.length; count++)
         {
            var digit = parseInt(cardnumber.substr(count,1));
            if (!((count & 1) ^ oddoreven))
	        {
               digit *= 2;
               if (digit > 9)
               {
                  digit -= 9;
                  addition = addition + ' ' + digit;
               }
               else
               {
                  addition = addition + ' ' + digit;
               }
               sum += digit;
            }
            else
            {
               sum += digit;
               addition = addition + ' ' + digit;
            }
         }
         return ((sum % 10 != 0)?  false : true);
	  },
      regularExpression : /^[0-9*]+$/
   },

   nameOnCard:
   {
      msgBlank :"The name on your card is required to complete your booking. Please enter the name on your card.",
	  msgInvalid :"Please enter a valid name.",
      isOptional:true,
      dependencyFields: "",
      specificValidationExists:true,
	  isAmount:false,
	  isToBeRefreshed:true,
	  specificValidationFunction : function( fieldObj, index)
	  {
	     if(StringUtils.isNotBlank(fieldObj.value))
		 {
		    var nameOnCard = fieldObj.value;
		    if (StringUtils.isBlank(StringUtils.stripChars(nameOnCard,"-.' ")))
		    {
			   return false;
		    }
		 }
		 return true;
	  },
      regularExpression : /^[-a-zA-Z\u00C0-\u00DD\u00E0-\u00FF \.\'\x27]*$/
   },

   postCode:
   {
      msgBlank :"We cannot validate the payment card post code you have entered. Please check and re-enter.",
	  msgInvalid :"We cannot validate the post code you have entered. Please check and re-enter.",
      isOptional:true,
      dependencyFields: "",
      specificValidationExists:true,
	  isAmount:false,
	  	isToBeRefreshed:true,
      specificValidationFunction: function (postCodeObject, index)
      {
	     if (StringUtils.isNotBlank(postCodeObject.value))
		 {
		    var regex = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
			postCode = postCodeObject.value.toUpperCase()
			return (regex.test(postCode));
		 }
		 return true;
      }
   },

   authCode:
   {
      msgBlank :"Enter a valid cardreference Number.",
      isOptional:true,
      dependencyFields: "",
      specificValidationExists:false,
	  isAmount:false,
	  isToBeRefreshed:false
   },

   authCodeReferred:
   {
      msgBlank :"Please enter an authCode",
      isOptional:true,
      dependencyFields: "",
      specificValidationExists:false,
	  isAmount:false,
	  isToBeRefreshed:false
   },

   securityCode:
   {
      msgBlank :"The 3 digit security number is required to complete your booking. This can be found to the right on the reverse of your card. Please enter your 3 digit security number.",
      msgInvalid :"Please enter a valid security number (the last 3 digits in the signature strip on the reverse of your card)",
	  isOptional:true,
      dependencyFields: "",
      specificValidationExists:true,
      isAmount:false,
	  isToBeRefreshed:true,
      specificValidationFunction : function( fieldObj, index)
      {
         if(StringUtils.substringBefore(fieldObj.id, "_") == 'securityCode' && maskedCvv != fieldObj.value)
         {
         var securityCode = fieldObj.value;
         var maxLength = 3;
         var regex =/^[0-9*]*$/;
         this.msgBlank = "The 3 digit security number is required to complete your booking. This can be found to the right on the reverse of your card. Please enter your 3 digit security number.";
         this.msgInvalid = "Please enter a valid security number (the last 3 digits in the signature strip on the reverse of your card)";
         if ($("#paymentType_"+index).val() == 'AMERICAN_EXPRESS|Dcard')
         {
            this.msgBlank = "The 4 digit security number is required to complete your booking. This can be found above the card number on the front of your card. Please enter your 4 digit security number.";
            this.msgInvalid = "Please enter a valid security number (the 4 digit code above the card number on the front of your card)";
            maxLength = 4;
         }
         else if ($("#paymentType_"+index).val() == 'GiftCard|GiftCard')
         {
            this.msgBlank = "The 4 digit security number is required to complete your booking. This can be found to the right on the reverse of your card. Please enter your 4 digit security number.";
            this.msgInvalid = "Please enter a valid security number (the last 4 digits in the signature strip on the reverse of your card)";
            maxLength = 4;
         }

         // The empty check is bypassed here because security code is not empty checked on blur
         //empty check is done  on submit for all the card related fields.
         if (securityCode.length != maxLength && StringUtils.isNotEmpty(securityCode))
         {
            return false;
         }
         if (!(regex.test(securityCode)))
         {
            return false;
         }
         }
         return true;
      }
   },

   issueNumber:
   {
      msgBlank : "Please enter the issue number of your card",
	  msgInvalid: "Please enter a valid number.",
	  isOptional: true,
	  isToBeRefreshed:true,
	  dependencyFields:"",
      regularExpression : /^[0-9]{1,2}$/
   },

   expiryMonth:
   {
      msgBlank : "Please select expiry date for your card",
	  msgInvalid: "Please enter a valid date.",
	  isOptional: true,
	  dependencyFields:"",
	  specificValidationExists:true,
	  specificValidationFunction : function(fieldObj,index)
	  {
	     var Calendar=new Date();
         todaysmonth =parseInt(Calendar.getMonth()+1,10);
         todaysyear = parseInt(Calendar.getFullYear(),10);
         cardmonth = parseInt(fieldObj.value,10);
		 //direct "id" dependency on expiryYear_index
         cardyear = parseInt("20" + $("#expiryYear_"+index).val(), 10);
		 this.msgInvalid = "You have entered an invalid Expiry Date. Please check and try again.";
		 if (StringUtils.isBlank($("#expiryYear_"+index).val()))
		 {
		    this.msgInvalid = "Please enter a valid date.";
         }
		 if ((cardyear == todaysyear) || (cardyear<todaysyear))
         {
            if ((cardmonth<todaysmonth))
            {
               return false;
            }
         }
		 return true;
      }
   }
};

var FormValidator =
{
   /*
    1. FormValidator validates block tags such as <form>, <div> etc
    2. Gets all tags inside block tags and stores them in an array using document.getElementsByTagName

    3. Iterate over tags array and call validateField as below.
    4. Returns an errorObject with errorMessage and errorField object is not empty
    5. Returns null if validation is successful for the block tag
   */
   validateBlocks : function(blockToBeValidated, defaultMessage, index)
   {
      //Get all tags inside pamentDetailsList
      var tags = blockToBeValidated.getElementsByTagName("*");
      for(var i = 0; i< tags.length; i++)
      {
         elem = tags[i];
         //We need to call validate function for user input tags only
         switch(elem.type)
         {
            case "text" : validationResult  =  this.validateEmptyField(elem,defaultMessage, index);
                          if(StringUtils.isNotBlank(validationResult.errorMsg))
                          {
                             return {errorMsg:validationResult.errorMsg,element:validationResult.element} ;
                          }
 						  //This section is included to incorporate luhn check for the card Number field only.
						  if (StringUtils.substringBefore(elem.id, "_") == 'cardNumber' && $('#cardNumber_'+index).val() != elem.value)
						  {
						     if (!(FormValidationRules["cardNumber"].luhnCheck(elem.value)))
							 {
                                return ({errorMsg:FormValidationRules["cardNumber"].msgInvalid,element:elem})
							 }
						  }
                          break;
            case "checkbox" : if(!elem.checked)
                              {
                                 return {errorMsg:"Amount Paid should match total amount.",element:elem} ;
                              }
                              break;
            case "select-one" : emptyValidationResult =  this.validateEmptyField(elem,defaultMessage, index);
                                if(StringUtils.isNotBlank(emptyValidationResult.errorMsg))
                                {
                                   return {errorMsg:emptyValidationResult.errorMsg,element :emptyValidationResult.element} ;
                                }
                                validationResult =  this.validateField(elem,defaultMessage, index);
                                if(StringUtils.isNotBlank(validationResult.errorMsg))
                                {
                                   return {errorMsg:validationResult.errorMsg,element :validationResult.element} ;
                                }
                                break;
         }
      }
      //Returns null if there is no error.
      return {errorMsg : "", element : null };
   },

   // This method is called on submit of the form. It just does all the empty checks on all the fields.
   validateEmptyField : function(fieldObj, index)
   {
      if(!($("#"+fieldObj.id).hasClass('hide')))
	  {
 	     if (StringUtils.substringBefore(fieldObj.id, "_") == 'issueNumber')
		 {
			 return this.validateField(fieldObj,"",index);
		 }
    	 var fieldRule = FormValidationRules[StringUtils.substringBefore(fieldObj.id, "_")];
	     if (fieldRule != undefined)
	     {
	        //Remove currency symbol if it exists.
		    var fieldValue =  StringUtils.stripChars(fieldObj.value , PaymentDetailsContent.currencySymbol);
		    if (StringUtils.isEmpty(fieldValue))
		    {
		       return {errorMsg: fieldRule.msgBlank ,element :fieldObj}
		    }
	     }
	  }
	  //Returns null if there is no error.
	  return {errorMsg:"",element:null};
   },

   /*
    1. Gets field obj
    2. Gets rule for the field id
    3.
    4. Returns ""/null(to be determined) if validation is successful
    */
   validateField : function(fieldObj,defaultMessage,index)
   {
      //Returns rule
      var fieldRule = FormValidationRules[StringUtils.substringBefore(fieldObj.id, "_")];
      if (fieldRule != undefined )
	  {
         // Validate the dependency field first if one exists
         if (StringUtils.isNotEmpty(fieldRule.dependencyFields))
	     {
		    var dependencyFieldName = fieldRule.dependencyFields+index;
	        var result = this.validateField(document.getElementById(dependencyFieldName),'',index);
		    if (StringUtils.isNotEmpty(result.errorMsg))
		    {
               return result;
		    }
	     }
 	     var value= fieldObj.value;
         // this block is to remove the currency symbol if any exist in the field
         if(!(value.indexOf(PaymentDetailsContent.currencySymbol) == -1))
	     {
		    value = StringUtils.stripFirstChar(value);
	     }
         if(!fieldRule.isOptional )
         {
		    if (StringUtils.isEmpty(value))
            {
               return ({errorMsg:fieldRule.msgBlank,element:fieldObj} );
		    }
         }
		 if (fieldRule.regularExpression != undefined && StringUtils.isNotBlank(value))
	     {
		    if (!(fieldRule.regularExpression.test(value)))
	        {
			   return ({errorMsg:fieldRule.msgInvalid, element:fieldObj});
            }
         }
         // This is for those fields  which have a specific validation other than regular expression check (ex luhn check for cards)//
	     if(fieldRule.specificValidationExists)
	     {
		    if (!(fieldRule.specificValidationFunction(fieldObj, index)))
		    {
               return ((fieldRule.msgInvalidSpecific != undefined)?{errorMsg:fieldRule.msgInvalidSpecific, element:fieldObj} : {errorMsg:fieldRule.msgInvalid, element:fieldObj});
		    }
	     }
	  }
      //Returns null if no error.
	  return {errorMsg:"",element:null};
   }
};