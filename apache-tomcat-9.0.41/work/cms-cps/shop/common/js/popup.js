function Popup(popURL,popW,popH,attr){
   if (!popH) { popH = 350 }
   if (!popW) { popW = 600 }
   var winLeft = (screen.width-popW)/2;
   var winTop = (screen.height-popH-30)/2;
   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
   popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
   popupWin.close()

   if(popURL.indexOf("www.360travelguide.net")> -1)
   {
      popupWin=window.open(popURL,"popupWindowVideoTour",winProp);
   }
   else
   {
      popupWin=window.open(popURL,"popupWindow",winProp);
   }

   popupWin.window.focus()
}

function viewAccomDetails(clientURL,accomUniqueId, accomId, alternateAccom)
{
   url = clientURL+"/thomson/page/shop/byo/details/detailspopup.page?packageNumber="+accomUniqueId;
   if(accomId)
   {
      url = url + "&AccommodationId=" + accomId;
   }
   if(alternateAccom)
   {
       url = url + "&alternateAccom=" + alternateAccom;
   }
   Popup(url,755,584,'scrollbars=yes');
}

function popUpBookingConditions(accInv, flightInv,deptDate,clientDomainURL)
{
   var d = new Date();
   var currentDate = d.getDate();
   var currentMonth = d.getMonth();
   currentMonth++;
   currentDate = currentDate+"";
   currentMonth = currentMonth+"";
   if (currentDate.length == 1)
   {
      currentDate = "0"+currentDate;
   }

   if (currentMonth.length == 1)
   {
      currentMonth = "0"+currentMonth;
   }
      if (accInv=="ThomsonSun")
   {
      accInv="TRACS"
   }
   if (accInv =="HOPLA_THM" || accInv =="HOPLA_PEG")
   {
      accInv="HOPLA"
   }
   if (flightInv=="Amadeus")
   {
      flightInv = flightInv.toUpperCase();
   }
   if (flightInv=="TRACSA")
   {
      flightInv = "TRACS";
   }
   if (flightInv=="NAV")
   {
      flightInv="NAVITARE";
   }
   var formattedDate = d.getFullYear()+"-"+currentMonth +"-"+currentDate;
   var url=clientDomainURL+"/thomson/page/common/tandc/tandc.page?"+"accommInvSys="+accInv+"&"+"flightInvSys="+flightInv+"&"+"date="+formattedDate+"&"+"lang="+"en"+"&"+"deptDate="+deptDate
  //Check if T & c url is not empty
    if(tandCurl!="")
    {
      url = tandCurl;
    }
   window.open(url, "", "width=750,height=500,scrollbars=yes");
}

function openPlusPopup(whichLayer)
{
   var style2 = document.getElementById(whichLayer).style;
   style2.display = "block";
}
function closePlusPopup(whichLayer)
{
   if (document.powerSearchResult2 != null)
   {
      if (document.powerSearchResult2.sortOption != null)
      {
         if (document.powerSearchResult2.sortOption.style.display =='none')
         {
            document.powerSearchResult2.sortOption.style.display ='inline';
         }
      }
   }
   var style2 = document.getElementById(whichLayer).style;
   style2.display = "none";
}
function showHiddenInformation(elemId){
   div = document.getElementById(elemId);
   if(div.style.display=='none')
   {
      div.style.display = "block";
   }
   else
   {
      div.style.display = "none";
   }
}

function openTitlePopup(whichLayer)
{
    if(document.getElementById(whichLayer))
    {
	   var style2 = document.getElementById(whichLayer).style;
	   var pos = jQuery("#totalText").offset();
	   var left;
	   var top;
	   clientApp = StringUtils.trimSpaces(clientApp);
	   if(StringUtils.equalsIgnoresCase(clientApp,"WiSHAO"))
	   {
          left = parseInt((1*pos.left-262),10);
	      top = parseInt((1*pos.top-297),10);
	   }
	   else if (StringUtils.equalsIgnoresCase(clientApp, "WiSHBYO"))
	   {
          left = parseInt((1*pos.left+2),10);
	      top = parseInt((1*pos.top-157),10);
       }
	   else if (StringUtils.equalsIgnoresCase(clientApp, 'Cruise'))
	   {
          left = parseInt((1*pos.left+2),10);
	      top = parseInt((1*pos.top-15),10);
	   }
	   style2.top = top;
	   style2.left = left;
	   style2.display = "block";
	}
}

function closeTitlePopup(whichLayer)
{
    if(document.getElementById(whichLayer))
    {
	   var style2 = document.getElementById(whichLayer).style;
	   style2.display = "none";
	}
}
