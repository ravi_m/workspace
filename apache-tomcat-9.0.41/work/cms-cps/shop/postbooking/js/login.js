
function showError(toggle)
{

   var elem = 'loginError';
   var elemID = document.getElementById(elem);
   if (toggle)
   {
      elemID.style.display = 'block';
       // document.getElementById('loginErrorDisplayed').value = 'true';
   }
   else
   {
      elemID.style.display = 'none';
        //document.getElementById('loginErrorDisplayed').value = 'false';
      formobj = document.forms[0];
      for (i=0;i<formobj.elements.length;i++)
      {
      obj = formobj.elements[i];
      obj_name = TrimSpaces(obj.name);
        if(obj_name == 'password')
        {
         obj.value = "";
        }
     }
   }
}


function ValidateLogin() {
   formobj = document.forms[0];
   var flag = false;
   for (i=0;i<formobj.elements.length;i++) {
      obj = formobj.elements[i];
      obj_value = TrimSpaces(obj.value);
      obj_name = TrimSpaces(obj.name);
       if(obj_value=="")
       {
         showError(true); flag=true;
      }
      switch (obj_name) {
         case 'shopNumber':
               var Pat1 = /^[0-9]{4}$/;
               if (!Pat1.test(obj_value)) { showError(true); flag = true;}

               break;
         case 'consultantId' :
            var Pat1 = /^[a-zA-Z0-9]{5}$/;
               if (!Pat1.test(obj_value)) { showError(true); flag = true;}
               break;
         case 'password' :
                var Pat1 = /^[-a-zA-Z0-9 \\\/.,\x27!;@:"#�\$%\^&\*()]*$/;
                  if (!Pat1.test(obj_value)) { showError(true); flag = true;}
                  break;

      }
   }
   if (!flag)
   {
      formobj.submit();
   }


}

// Trim spaces from start and end of string
function TrimSpaces(objval) {
  // Remove Space at start
  RESpace = /^\s*/i;
  objval = objval.replace(RESpace, '');
  // Remove Space at end
  RESpace = /\s*$/i;
  objval = objval.replace(RESpace, '');
  return objval;
}


function setCallType(calltype)
{
   var call_Type = document.getElementById("call_Type");
   calltypevalue = call_Type.options[call_Type.selectedIndex].value;
   var path = "/thomson/page/shop/login/calltype.page";
   path = path+"?callType="+calltypevalue;
   var homepagename = document.getElementById("homepagename").value;
   path = path+"&homePageName=" + homepagename;
   window.parent.location.href = path;
   //document.getElementById("callType").value = calltypevalue;
}
function logoffDialog()
{
   document.getElementById('logoffCheck').style.display='block';
}
function cancelLogin()
{
   document.getElementById('logoffCheck').style.display='none';
}

function logOff(url)
{
   window.parent.document.location.replace(url+'/thomson/page/shop/login/logoff.page');
}


document.onkeydown = function() {
      return alertkey(window.event);
};
function alertkey(e) {
  if( !e ) {
    if( window.event ) {
     //Internet Explorer
      e = window.event;
    } else {
      return;
    }
  }
  if( typeof( e.keyCode ) == 'number'  ) {
    //DOM
      e = e.keyCode;
  } else if( typeof( e.which ) == 'number' ) {
    //NS 4 compatible
      e = e.which;
  } else if( typeof( e.charCode ) == 'number'  ) {
    //also NS 6+, Mozilla 0.9+
      e = e.charCode;
  } else {
    //total failure, we have no way of obtaining the key code
    return;
  }
  if(e==13) {

      var loginErrorDisplayedObj = document.getElementById('loginErrorDisplayed');

      if (loginErrorDisplayedObj)
      {
         var loginErrorDisplayed = loginErrorDisplayedObj.value;

         if (loginErrorDisplayed == 'true')
         {
            element_to_process = document.getElementById('error_ok');
         }
         else
         {
            element_to_process = document.getElementById('continue');
         }
         document.getElementById('loginErrorDisplayed').value = 'false';
         element_to_process.click();
      }
  }
}

function closeLoginScreen()
{
self.opener = top;
self.close();
}
function goToHomePage()
{
   window.parent.location.href = "/thomson/page/shop/login/newcustomer.page";
}