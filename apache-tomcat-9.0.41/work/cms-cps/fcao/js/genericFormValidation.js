
// TO be refactored
var childDayArray = new Array();
//TODO: Following is a utility function and should be moved accordingly
/**
 * String Utils functions
 *
 * Contains following functions
 * isNotEmpty:
 * isEmpty:
 * trim:
 *
 */
var StringUtils = {

  /**
   * returns true if the value passed is not empty/null/undefined
   *
   */
  isNotEmpty : function(value)
  {
	return(value!=undefined && value!=null && value.length>0 )
  },

  /**
   * returns true if the value passed is  empty/null/undefined
   *
   */
  isEmpty : function(value)
  {
	  return(value==undefined || value==null || this.trim(value).length==0 )
  },

  /**
   * Trims spaces from the string
   *
   */
  trim : function(value)
  {
	  return value.replace( /^\s+|\s+$/g, "" );
  }

};

/**
 * A util function to validate a field for given regex
 *
 * @param id
 * @param regex
 * @param validationMessage
 * @return
 */
function validate(id,regex)
{
  if(!regex.test($(id).value))
  {
	  return false;
  }
  return true;

}

/**
* We shall divide form validation into four parts
* i. Passenger Names
* ii. Lead passenger details
* iii. Credit card details
* iv. Other validation like terms and condition
*
*/

/************************Declaration of object literals******************************************************/
/**
 * Following Object literals contain all the information necessary to validate payment page.
 * Following object literals are defined
 * PassengerNameFields:
 * LeadPassengerFields:
 * CardDetailsFields:
 * OtherPaymentFields:
 *
 */

 /**
  * Encapsulate Passenger Names section
  * Should contain all the information necessary to validate fields in passenger name section
  */
var PassengerNameFields =
{
   /////Only contains part of id. index has to be added to get complete id.
   //Senior passenger identification checkbox
   seniorCheck65 :{id:"personaldetails_ageBetween65To75_" , msgBlank:"Enter the first name of passenger ", msgInvalid:"Enter a valid first name of passenger " },
   seniorCheck75 :{id:"personaldetails_ageBetween76To84_" , msgBlank:"Enter the first name of passenger ", msgInvalid:"Enter a valid first name of passenger " },

      //Name section
   foreName         :{id:"foreName_",regex :/^[a-zA-Z\u00C0-\u00DD\u00E0-\u00FF \.\'\x27]*$/, msgBlank:"Enter the first name of passenger ", msgInvalid:"Enter a valid first name of passenger " },
   surName          :{id:"surName_",regex :/^[a-zA-Z\u00C0-\u00DD\u00E0-\u00FF \.\'\x27]*$/, msgBlank:"Enter the surname of passenger ", msgInvalid:"Enter a valid sur name of passenger " },
   middleName       :{id:"middleName_" ,regex :/^[a-zA-Z]*$/ , optional:true, msgBlank:"Enter a middle name of passenger  ", msgInvalid:"Enter a valid middle name of passenger " },

   //Child date of birth section
   dayOfBirth       :{id:"spanDay"},
   monthOfBirth     :{id: "spanMonth" },
   yearOfBirth      :{id: "spanYear", msgBlank:"Enter the date of birth of passenger ", msgInvalid:"Enter a valid date of birth of passenger " },

   //Default regex will be used unless specified
   //defaultRegex     : /^[a-zA-Z\u00C0-\u00DD\u00E0-\u00FF \.\'\x27]*$/
   defaultRegex     : /^[A-Za-z]+([ -�][A-Za-z]+)*$/
};

/**
 * Encapsulate Lead passenger details
 * Should contain all the information necessary to validate fields in lead passenger details section
 */
var LeadPassengerFields =
{
   // validation for lead passenger address details.
   houseNameOrNo     :{id:"houseName", msgBlank:"Enter your house name or number ", msgInvalid:"Enter a valid house name or number" },
   addressLine1      : {id:"houseAddress1", msgBlank:"Enter your address", msgInvalid:"Enter a valid address" },
   addressLine2      : {id:"houseAddress2", msgBlank:"Enter house address2 ", msgInvalid:"Enter valid house address2 " },
   town              : {id:"townOrCity", msgBlank:"Enter your town or city", msgInvalid:"Enter a valid town or city" },
   county            : {id:"countyName", msgBlank:"Enter your county", msgInvalid:"Enter a valid county" },
   country           : {id:"countryName"},
   //Post code will be validated separately
   postCode          :{id:"postCode", msgBlank:"Enter your postcode ", msgInvalid:"Enter a valid postcode " },

  //Phone will have its own regex pattern
   phone             :{id:"contactTelephone" ,optional:false, phoneRegex: /^[0-9 ]*$/, msgBlank:"Enter your contact telephone number", msgInvalid:"Enter a valid telephone number" },
   mobile            :{id:"mobileTelephone" ,optional:true, phoneRegex: /^[0-9 ]*$/, msgBlank:"Enter mobile time phone ", msgInvalid:"Enter a valid mobile number" },

   //TODO: Should be seen whether we can combine the following
   email             :{id:"EmailAddress" ,emailRegex:/^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/, msgBlank:"Enter your email address", msgInvalid:"Enter a valid email address \n\ne.g. name@place.com" },
   confirmEmail      :{id:"EmailAddress2" ,emailRegex:/^([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+$/, msgBlank:"Confirm your email address ", msgInvalid:"Enter a valid confirm email address e.g. name@place.com" , msgNotMatching:"The email addresses entered do not match. Re-enter your details" },

   //Unless explicitly specified fields will use this regex pattern
   defaultRegex      :/^[-a-zA-Z0-9 ,\.\x27]*$/
};


/**
 * Encapsulate Credit card details.
 * Should contain all the information necessary to validate fields in credit card section
 */
var CardDetailsFields =
{
   cardType          :{id:"payment_type_0", msgBlank:"Select your card type from the list"},
   cardNumber        :{id:"payment_0_cardNumberId", regex:"" ,msgBlank:"Enter your card number", msgInvalid:"Enter a valid card number ", msgMOD:" "},
   cardName          :{id:"payment_0_nameOnCardId", regex:/^[A-Za-z\ ]*$/ ,msgBlank:"Enter the name on the card", msgInvalid:"Enter the valid name on card"},
   startMonth        :{id:" ", optional:true },
   startYear         :{id:" ", optional:true, msgBlank:" ", msgInvalid:" "},
   expiryMonth       :{id:"payment_0_expiryMonthId"},
   expiryYear        :{id:"payment_0_expiryYear", msgBlank:"Enter an expiry date", msgInvalid:"Enter a valid expiry date"},
   securityCode      :{id:"payment_0_securityCodeId",regex:/^[0-9 ]*$/ ,msgBlank:"Enter the security code", msgInvalid:"Enter a valid security code"},
   issueNo           :{id:"IssueNumberInput",optional:true,regex:/^[0-9]*$/ ,msgBlank:"Enter the Issue Number ", msgInvalid:" Enter a valid issue number"},
   defaultRegex     : /^[a-zA-Z]*$/
};//End of CardDetailsFields

var CardHolderFields =
{
   street_address1     :{id:"payment_0_street_address1", msgBlank:"Enter card holder house name or number ", msgInvalid:"Enter a valid card holder house name or number"},
   street_address2     : {id:"payment_0_street_address2", msgBlank:"Enter card holder address", msgInvalid:"Enter a valid card holder address"},
   street_address3     : {id:"payment_0_street_address3", msgBlank:"Enter card holder town or city", msgInvalid:"Enter a valid card holder town or city" },
   street_address4     : {id:"payment_0_street_address4", msgBlank:"Enter card holder county", msgInvalid:"Enter a valid card holder county" },
   country		       : {id:"CHcountryName"},
   postCode            : {id:"payment_0_postCode", msgBlank:"Enter card holder postcode", msgInvalid:"Enter a valid card holder postcode"},

   //Unless explicitly specified fields will use this regex pattern
   defaultRegex      :/^[-a-zA-Z0-9 ,\.\x27]*$/
};

/**
 * Other fields
 */
var OtherPaymentFields =
{

};

/************************End of declaration of object literals*********************************/

//App configuration
var appConfig =
{
	showValidationAtTheTop: true

};


/************************End of declaration of objects******************************************************/


/**********************************************************************************************/
/**
 * The main function for form validation
 * @return true if validation is successful
 */
//validation messages should be added to this list
var errorMessages = $A( [] );

function validateForm()
{
   errorMessages = $A( [] );

   //Check for Important information checkbox is checked if it exists
  if($("importantInformationChecked")!= null)
  {
   validateTermsAndConditions("importantInformationChecked","You have to read and accept our important additional information");
  }

   validatePassengerNames();
   validateLeadPassengerDetails();
   validateCreditCardDetails();
   validateCardHolderAddressDetails();
   validateTermsAndConditions("bookingConditionsChecked","You have to agree to the Terms and Conditions to proceed");

   //If validation is not successful then show warnings at the top(if applicable)
   if(StringUtils.isNotEmpty(errorMessages))
   {
     showValidationAtTheTop();
     return false;
   }
   if($("confirmButton"))
   {
      var payButtonDescription = threeDCards.get(PaymentInfo.selectedCardType);
      if(payButtonDescription == "mastercardgroup" || payButtonDescription == "visagroup" || payButtonDescription == "americanexpressgroup")
      {
         $("confirmButton").innerHTML = '<img src="/cms-cps/fcao/images/buttons/form/proceed-to-payment-btn.gif" alt="Proceed to payment" hspace="0" vspace="0" border="0" title="Proceed to payment" />'
      }
      else
      {
         $("confirmButton").innerHTML ='<img src="/cms-cps/fcao/images/buttons/form/pay.gif" alt="Pay" hspace="0" vspace="0" border="0" title="Pay" />';
      }
   }
   	//called the new function for disabling the pay button for 3D change.
	//disableThreeDSecureButton();
  return true;
}

/**
 * Function for validating passenger name section
 *
 * @return
 */
function validatePassengerNames()
{
    var passengerCount = getPassengerCount();
    CheckSeniorPassengerAndValidate(passengerCount);
   //TODO: get passenger count and re factor
   for( var index = 1; index <= passengerCount; index++ )
   {
     for(var item in PassengerNameFields)
     {
        switch(item)
        {
          case 'defaultRegex' : break;
          //Already we have taken care about senior passenger details
          case 'seniorCheck65':break;
          case 'seniorCheck75':break;
          case 'middleName'      :validateField(item, PassengerNameFields, PassengerNameFields.middleName.regex, index);
          case 'dayOfBirth'      :break;
          case 'monthOfBirth'    :break;
          case 'yearOfBirth'     :dateOfBirthValidation(index); break;
          default                :validateField(item, PassengerNameFields, regex, index);
        }
      }
    }
   var regex = PassengerNameFields.defaultRegex;
}


/**
 * Function for validating lead passenger details
 * @return
 */
function validateLeadPassengerDetails()
{
  var isValidEmail='';
  for(var item in LeadPassengerFields)
  {
      switch(item)
      {

        case 'defaultRegex' : break;
        case 'postCode'     : validatePostCode(LeadPassengerFields);
                              break;
        case 'phone'        : validatePhoneNo("phone");
                              break;
        case 'mobile'       : validatePhoneNo("mobile");
                              break;
        case 'email'        : validateEmail("email");
                              break;
        case 'confirmEmail' : validateEmail("confirmEmail");
                              break;
        case 'country'      : break;

        default             : validateField(item, LeadPassengerFields, null, null);validateField
     }
   }
}

/**
 * Validate all fields in credit card details section
 *
 * @return
 */
function validateCreditCardDetails()
{
//	for(var index=0,noOfPayments = $("payment_totalTrans").value; index<noOfPayments; index++)
//	{
      for(var item in CardDetailsFields)
      {
         switch(item)
         {
            //check whether a card type is selected
            case 'cardType'     : //validateField(item, CardDetailsFields, null, null)
                                  if(StringUtils.isEmpty($(CardDetailsFields.cardType.id).value))
                                  {
                                    var messageBlank = CardDetailsFields.cardType.msgBlank;
                                    addValidationWarnings(CardDetailsFields.cardType.id, messageBlank);
                                  }
                                 break;
            //Card number involves MOD check also
            case 'cardNumber'   :  var cardRegex = (PaymentInfo.selectedCardType=="AMERICAN_EXPRESS") ? /^[0-9]{15}$/ : /^[0-9]{16,20}$/;
                                   var flag = validateField(item, CardDetailsFields, cardRegex, null);
                                   if(flag)
                                   {
                                     luhnValidation(item);
                                   }
                                   break;

            //expiry month and expiry year should be validated together
            case 'expiryMonth'  :break;
            case 'expiryYear'   :var inputMonth=$(CardDetailsFields.expiryMonth.id).value;
                                 var inputYear=$(CardDetailsFields.expiryYear.id).value;
                                 var messageBlank = CardDetailsFields.expiryYear.msgBlank;
                                 var messageInvalid = CardDetailsFields.expiryYear.msgInvalid;
                                 if((inputMonth == 'MM' || inputYear =='YY'))
                                 {
                                   addValidationWarnings(CardDetailsFields.expiryYear.id, messageInvalid);
                                 }
                                 if(!validateDate(inputMonth,inputYear,"expiryDate"))
                                 {
                                   addValidationWarnings(CardDetailsFields.expiryYear.id, messageInvalid);
                                 }
                                 break;

            case 'cardName'     : nameRegex=CardDetailsFields.cardName.regex;
                                  validateField(item, CardDetailsFields, nameRegex, null);
                                 break;

            case 'securityCode' :validateSecurityCode(item);
                                 break;

            case 'postCode'     :validatePostCode(CardDetailsFields);
                                 break;
             //Start month and start year should be validated together
            //case 'startMonth'   :break;
           // case 'startYear'    :if(!validateDate(inputMonth,inputYear,"startDate"))
                                // {
                                // }
                               //  break;

            case 'issueNo'      : issueNumberRegex = CardDetailsFields.issueNo.regex;
                                  if($(CardDetailsFields.cardType.id).value == 'SOLO|Dcard' ||$(CardDetailsFields.cardType.id).value == 'SWITCH|Dcard' )
                                  {
                                     validateTheValue($(CardDetailsFields.issueNo.id).value, item, CardDetailsFields, issueNumberRegex, null);
                                    }
                                    break;

	        case 'street_address1'   : validateField(item,CardDetailsFields,CardDetailsFields.street_address1.regex,null);
												 break;

            default               : validateField(item, CardDetailsFields, null, null);
         }
	  }
}

/**
 * Validate all fields in card holder address details section
 *
 * @return
 */
function validateCardHolderAddressDetails()
{
   for(var item in CardHolderFields)
   {
      switch(item)
      {
         case 'defaultRegex' : break;
         case 'postCode'     : validatePostCode(CardHolderFields);
                               break;
         case 'country'      : break;

         default             : validateField(item, CardHolderFields, null, null);validateField
      }
   }
}

/***********************************Validation functions*******************************************/


/**
 * Following function constructs id from the item of object literals and then reads their value and validates it
 * @param item item
 * @param objectLiteral means LeadPassengerFields etc.
 * @param regex regex of the item
 * @param index index of the iteration
 * @return
 */
function validateField(item, objectLiteral, regex, index)
{
   id = (index!=null) ? $(objectLiteral[item].id + index) : $(objectLiteral[item].id) ;

   if(id)
   {
	//If regex is null then use the default regex
	regex = (regex==null)?objectLiteral.defaultRegex : regex;
	//Remove space from both ends of the value
	id.value = StringUtils.trim(id.value);
    value = id.value;
    //Once you get value , you can call following function to validate the value
    return validateTheValue(value, item,  objectLiteral, regex, index);
   }
}

/**
 * Validates the provided value and adds validation warning if applicable.
 * @param value
 * @param item
 * @param objectLiteral
 * @param regex
 * @param index
 * @return
 */
function validateTheValue(value, item, objectLiteral, regex, index)
{
	msgBlank = (index!=null) ? objectLiteral[item].msgBlank + index : objectLiteral[item].msgBlank ;
    msgInvalid = (index!=null) ? objectLiteral[item].msgInvalid + index : objectLiteral[item].msgInvalid ;

    if(StringUtils.isEmpty(value))
    {
      if(objectLiteral[item].optional != true )
      {
        addValidationWarnings(id, msgBlank);
        return false;
      }
    }
    else if(!regex.test(value))
    {
      addValidationWarnings(id, msgInvalid);
      return false;
    }
    return true;
}

/**
 *
 *
 * @param id
 * @param validationMessage
 * @return
 */
function addValidationWarnings(id, validationMessage)
{
	errorMessages.push(validationMessage);
}


/**
 * After validation is completed, this function is called to show message at the top.
 * @return
 */
function showValidationAtTheTop()
{
   var documentParent = $("paymentPageWarnings");
   $("paymentPageWarnings").innerHTML = "";
   var newLI ="";
   var newText = "";
   for(var i=0,len=errorMessages.length;i<len;i++)
   {
	    newLI = document.createElement("li");
	    newText = document.createTextNode(errorMessages[i]);
	    newLI.appendChild(newText);
	    documentParent.appendChild(newLI);
   }
   $("ValidationWarningBlock").className = "newWarningBlock show";
   $("ValidationWarningBlock").style.display = 'block';
   setTimeout("window.scrollTo(0, 0)",1); //Added to avoid the IE6 bug
}
/***********************************End of Validation functions*******************************************/


/**
 * Validating for phone no
 */
function validatePhoneNo(item)
{
	phoneRegex = LeadPassengerFields[item].phoneRegex;
    var phoneNumber = $(LeadPassengerFields[item].id).value;
    $(LeadPassengerFields[item].id).value= StringUtils.trim(phoneNumber);
    phoneNumber= stripChars(phoneNumber,"()- ");
    validateTheValue(phoneNumber, item, LeadPassengerFields, phoneRegex, null);
}

/**
 * Validating for email
 * @param emailItem
 * @return
 */
function validateEmail(emailItem)
{
    var emailObj = ($(LeadPassengerFields[emailItem].id));
    emailObj.value = StringUtils.trim(emailObj.value);
    val = emailObj.value;
    if(StringUtils.isEmpty(val))
    {
       addValidationWarnings(LeadPassengerFields[emailItem].id, LeadPassengerFields[emailItem].msgBlank);
       return false;
    }
    else if(!isValidEmail(val))
    {
       addValidationWarnings(LeadPassengerFields[emailItem].id, LeadPassengerFields[emailItem].msgInvalid);
       return false;
    }
    else if(emailItem == "confirmEmail")
    {
      var emailEntered = $(LeadPassengerFields.email.id).value
      if(emailEntered!=val)
      {
           addValidationWarnings(LeadPassengerFields[emailItem].id, LeadPassengerFields[emailItem].msgNotMatching);
      }
    }
    return true;
}

function isValidEmail(value)
{
	// get rid of any leading or trailing spaces
	  var value = StringUtils.trim( value );

	  // make sure it only allows valid characters
	 // var usesValidChars = /^[0-9A-Za-z@\.\-_+]*$/.test( value );
	  var usesValidChars = /^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/.test( value );
	  if( !usesValidChars ){ return false; }

	  // make sure we have at least seven characters
	  if( value.length < 7 ){ return false; }

	  // make sure we have exactly one "@" symbol
	  var atPosition = value.indexOf( "@" );
	  if( atPosition == -1 ){ return false; }
	  if( value.lastIndexOf( "@" ) != atPosition ){ return false; }

	  // get the portions of the address before and after the @ sign
	  var beforeAt = value.substr( 0, atPosition );
	  var afterAt = value.substr( atPosition + 1 );

	  // make sure we have something to the left of the @
	  if( beforeAt.length < 1 ){ return false; }

	  // find the last period after the @ sign
	  var lastPeriodPos = afterAt.lastIndexOf( "." );
	  if( lastPeriodPos == -1 ){ return false; }

	  // fine the first period after the @ sign
	  var firstPeriodPos = afterAt.indexOf( "." );
	  if( firstPeriodPos == -1 ){ return false; }

	  // make sure there are no dots side by side
	  for(var i = 1; i < afterAt.length; i++) {
	     if((afterAt.charAt(i) == '.') && (afterAt.charAt(i-1) == '.'))
	       return false;
	  }

      // get the portions of the domain before and after the final period
	  var beforePeriod = afterAt.substr( 0, lastPeriodPos );
	  var afterPeriod = afterAt.substr( lastPeriodPos + 1 );

	  // get portion of domain after '@' and before first period
	  var beforeFirstPeriod = afterAt.substr( 0, firstPeriodPos );

	  // make sure domain name itself is at least two characters long
	  if( beforeFirstPeriod.length < 2 ) { return false; }

	  // make sure the top-level domain has two or three characters
	  if( (afterPeriod.length < 2) || (afterPeriod.length > 3) ){ return false; }

	  // make sure there are at least two characters between the @ and the final period
	  if( beforePeriod.length < 2 ){ return false; }

	  // if it passes all the above tests then return true
	  return true;
	}



function validatePostCode(objectLiteral)
{
  var msgInvalid = objectLiteral.postCode.msgInvalid;
  var msgBlank = objectLiteral.postCode.msgBlank;
  postCodeID = objectLiteral.postCode.id;
  countryID = objectLiteral.country.id;

  $(postCodeID).value= StringUtils.trim($(postCodeID).value);	//This is done in a separate line of code since the trimming needs to reflect back on the jsp once its done
  postCode= ($(postCodeID).value).toUpperCase();				//Since the prerequesite for the regex designed for united kingdom postCode is that the postcode needs to be in upperCase, this is done;
  																// however since it shouldn't reflect bcak in the jsp, we have not clubbed it into the previous line.
  if(StringUtils.isNotEmpty(postCode))
  {
	if($(countryID).value == "United Kingdom" || $(countryID).value == "GB")
	{
		Pat1 = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXYU0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
		if (!Pat1.test(postCode))
		{
			addValidationWarnings(postCodeID, msgInvalid);
		}
	}
	else
	{
		Pat = /[^a-zA-Z0-9-,\.\\\\/ ]/;
		if (Pat.test(postCode)&& trimSpaces(postCode)!="")
		{
			addValidationWarnings(postCodeID, msgInvalid);
        }
	}
  }
  else
  {
	  addValidationWarnings(postCodeID, msgBlank);
  }
}

/**
 * Function to perform validation for security code entered.
 * @param item - item here represents the security code
 *
 */
function validateSecurityCode(item)
{
  var cardType='';
  var securityCodeLength='';
  var securityCode=$(CardDetailsFields.securityCode.id).value;
  var regex = /^[0-9 ]*$/;
  cardType = $(CardDetailsFields.cardType.id).value;
  if (validateField(item, CardDetailsFields, regex, null))
  {
	if(cardType != null)
	{
	   cardTypeValue=cardType.split("|")[0];
	   if(cardType!= '' )
	   {
		  securityCodeLength=$(cardTypeValue+"_securityCodeLength").value;
	   }
	   if (securityCode.length != securityCodeLength)
	   {
		   addValidationWarnings(CardDetailsFields.securityCode.id, CardDetailsFields.securityCode.msgInvalid);
	   }
	}
  }
}

/**
 * This function does the luhn check for the entered card number.
 * @param item
 * @return
 */
function luhnValidation(item)
{
   obj_1 = $(CardDetailsFields.cardNumber.id).value;
   var cardnumber = $(CardDetailsFields.cardNumber.id).value;
   var oddoreven = cardnumber.length & 1;
   var sum = 0;
	var addition = "";

	for ( var count = 0; count < cardnumber.length; count++)
	{
	  var digit = parseInt(cardnumber.substr(count, 1));
	  if (!((count & 1) ^ oddoreven))
	  {
	    digit *= 2;
	    if (digit > 9)
	    {
	      digit -= 9;
	      addition = addition + ' ' + digit;
	    }
	    else
	    {
	      addition = addition + ' ' + digit;
	    }
	    sum += digit;
	  }
	  else
	  {
	  sum += digit;
	  addition = addition + ' ' + digit;
	  }
}

if (sum % 10 != 0) {
	 addValidationWarnings(CardDetailsFields.cardNumber.id, "Enter a valid card number");
}

}

/**
*
* @param inputDate
* @param inputMonth
* @param inputYear
* @param typeOfDate whether expiry or start date
* @return true if date is valid
*/
function validateDate(inputMonth,inputYear,typeOfDate)
{
   var currentYear = year2digit();
   var currentTime = new Date();
   var currentMonth = currentTime.getMonth() + 1;
   currentYear = 1*currentYear;

   //By default expiryDate testing condition is given
   var dateTestCondition = (inputYear < currentYear) || ( (inputYear == currentYear) && (inputMonth < currentMonth) );
   if(typeOfDate=="startDate")
   {
   	dateTestCondition = (inputYear > currentYear ) || ( (inputYear == currentYear) && (inputMonth > currentMonth) );
   }
   if (dateTestCondition)
   {
      return false;
   }

   return true;
 }

/* Returns 2 digit year **/
function year2digit()
{
  RightNow = new Date();
  return /..$/.exec(RightNow.getYear())
}

/**
 * Function to check the check box validations
 * @return
 */
function validateTermsAndConditions(id,message)
{
  var isChecked = ($(id).checked);
  if(!isChecked)
  {
    addValidationWarnings(id, message);
  }
}

function getPassengerCount()
{
  return $("passengerCount").value;//return totalPassenCount;
}

/**
 * date of birth validation for child and infant.
 * @return
 */
function dateOfBirthValidation(index)
{
	if(!$('personaldetails_'+index+'_day'))
	{
	  return true;
	}
	birthDay = $('personaldetails_'+index+'_day').value;
    birthMonth = $('personaldetails_'+index+'_month').value;
    birthYear = $('personaldetails_'+index+'_year').value;

    if(StringUtils.isEmpty(birthDay) || StringUtils.isEmpty(birthMonth) || StringUtils.isEmpty(birthYear))
    {
    	addValidationWarnings(PassengerNameFields.yearOfBirth.id, PassengerNameFields.yearOfBirth.msgBlank+index);
        return false;
    }
    else
    {
    	if ((birthMonth=="4" || birthMonth=="6" || birthMonth=="9" || birthMonth=="11") && birthDay=="31")
        {
          addValidationWarnings(PassengerNameFields.yearOfBirth.id, PassengerNameFields.yearOfBirth.msgInvalid+index);
          return false;
        }
    	else if (birthMonth == "2")
        {
           var isleap = (1*birthYear % 4 == 0 && (1*birthYear % 100 != 0 || 1*birthYear % 400 == 0));
           if (1*birthDay>29 || (1*birthDay==29 && !isleap))
           {
             addValidationWarnings(PassengerNameFields.yearOfBirth.id, PassengerNameFields.yearOfBirth.msgInvalid+index);
             return false;
           }
        }
    	if(!compareDateWithAge(1*birthYear, 1*birthMonth, 1*birthDay, index))
    	{
    		addValidationWarnings(PassengerNameFields.yearOfBirth.id, PassengerNameFields.yearOfBirth.msgInvalid+index);
    	    return false;
    	}
    }
}

function  compareDateWithAge(birthYear, birthMonth, birthDay, index)
{
  var birthTime = Date.UTC(birthYear, birthMonth-1, birthDay);  // specified time (UTC)
  var cheOutYearArr = checkOutDate.split("/");

  checkOutYear = 1*cheOutYearArr[2]
  checkOutMonth = 1*cheOutYearArr[1];
  checkOutDay = 1*cheOutYearArr[0];

  var checkOutDateTime = Date.UTC(checkOutYear, checkOutMonth-1, checkOutDay);  // specified time (UTC)

  var ageCalculated = 0;
  var spareDys = 0;
  if ((checkOutMonth > birthMonth) || ((checkOutMonth == birthMonth) && (checkOutDay >= birthDay)))
  {
    ageCalculated = checkOutYear - birthYear;

    spareDys = parseInt((checkOutDateTime - Date.UTC(checkOutYear,birthMonth-1,birthDay))/(3600000*24));
    if ((birthMonth == 2 && birthDay == 29)  && ((checkOutYear%4 != 0) || (checkOutYear%100 == 0 &&  checkOutYear%400 != 0)))
    {
      spareDys = spareDys + 1
    }
  }
  else
  {
    ageCalculated = checkOutYear - birthYear-1;

    spareDys = parseInt((checkOutDateTime - Date.UTC(checkOutYear-1,birthMonth-1,birthDay))/(3600000*24));
    if ((birthMonth == 2 && birthDay == 29)  && (((checkOutYear-1)%4 != 0) || ((checkOutYear-1)%100 == 0 && (checkOutYear-1)%400 != 0)))
    {
       spareDys = spareDys + 1
    }
   }

   if($("passenger_"+index+"_isInfant").value=="true")
   {
     if(ageCalculated>=2)
     {
    	return false;
      }
   }
   else
   {
      ageInput = 1*$("passenger_"+index+"_age").value;

      if(ageCalculated !=ageInput)
      {
    	return false;
      }
   }

   return true;

}

/** Function to load the dates for child age drop down boxes.*/
function loadDates(identifier)
{

   var date = new Date();
   var monthArray = new Array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
   var year = parseInt(date.getFullYear(),10);
   var month = date.getMonth();
   var day = date.getDate();
   var marginYear = parseInt((year - 18),10);
   var yearSelect = '';
   var monthSelect = '';
   var daySelect = '';

   childDayArray.push(identifier);

   yearSelect = '<select name="personaldetails_'+identifier+'_year" id="personaldetails_'+identifier+'_year" class="personaldetails_form_element_title_field"><option value="">YYYY</option>';
   for (var j = year; j >= marginYear; j--)
   {
      yearSelect += '<option value="'+j+'">'+j+'</option>';
   }
   yearSelect += '</select>';
   $('spanYear'+identifier).innerHTML = yearSelect;
   $('spanYear'+identifier).className = "childDatesSelect";

   monthSelect = '<select name="personaldetails_'+identifier+'_month" id="personaldetails_'+identifier+'_month" class="personaldetails_form_element_title_field margin_right_tenpixel"><option value="">MM</option>';
   for (var j = 1; j < monthArray.length; j++)
   {
      if (monthArray[j] != undefined)
         monthSelect += '<option value="'+j+'">'+monthArray[j]+'</option>';
   }
   monthSelect += '</select>';
   $('spanMonth'+identifier).innerHTML = monthSelect;
   $('spanMonth'+identifier).className = "childDatesSelect";

   daySelect = '<select name="personaldetails_'+identifier+'_day" id="personaldetails_'+identifier+'_day" class="personaldetails_form_element_title_field margin_right_tenpixel"><option value="">DD</option>';
   for (var j = 1; j <= 31; j++)
   {
      if (j < 10)
         daySelect += '<option value="'+j+'">0'+j+'</option>';
      else
         daySelect += '<option value="'+j+'">'+j+'</option>';
   }
   daySelect += '</select>';
   $('spanDay'+identifier).innerHTML = daySelect;
   $('spanDay'+identifier).className = "childDatesSelect";
}


/**
*
* @param passengerCount
* @return
*/
function CheckSeniorPassengerAndValidate(passengerCount)
{
  var count =0;
  var count65=0;
  var count75=0;
  for (i=1; i<=passengerCount ; i++)
  {
     if(document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0]!=null && document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0].checked == true)
	   //if($("personaldetails_ageBetween65To75_"+i).value[0]!=null && $("personaldetails_ageBetween65To75_"+i)[0].checked == true)
     {
       count65=count65+1;
     }
     if(document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0]!=null && document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0].checked == true)
     //if($("personaldetails_ageBetween76To84_"+i)[0]!=null && $("personaldetails_ageBetween76To84_"+i)[0].checked == true)
     {
       count75=count75+1;
     }
  }
  if(count65 !=noPerson65)
  {
    if(count65 < noPerson65)
    {
   	 addValidationWarnings(null, lessSeniorPassengersAged65To75Message);
    }
    else
    {
      addValidationWarnings(null, moreSeniorPassengersAged65To75Message);
    }
  }
  if(count75 != noPersons75)
  {
    if(count75 < noPersons75)
    {
      addValidationWarnings(null, lessSeniorPassengersAged76To84Message);
    }
    else
    {
      addValidationWarnings(null, moreSeniorPassengersAged76To84Message);
    }
  }
}


/**
 * Do not delete this function though this function is nowhere used in any of the .js files since its
 * called from fcaoPassengerDetails.jsp to cross check the number of above65 passenngers
 */
function validatePassenegersOver65(passengerCount)
{
   var i = passengerCount;
   if((document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0] !=null && document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0].checked == true) && (document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0]!=null && document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0].checked == true))
   {
	  document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0].checked = false;
	  return;
   }
}

/**
 * Do not delete this function though this function is nowhere used in any of the .js files since its
 * called from fcaoPassengerDetails.jsp to cross check the number of above75 passenngers
 */
function validatePassenegersOver75(passengerCount)
{
   var i = passengerCount;
   if((document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0]!=null && document.getElementsByName('personaldetails_'+i+'_ageBetween76To84')[0].checked == true) && (document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0]!=null && document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0].checked == true))
   {
	  document.getElementsByName('personaldetails_'+i+'_ageBetween65To75')[0].checked = false;
	  return;
   }
}
/*********************************End of Over-ridden by client appications************************************************************/
