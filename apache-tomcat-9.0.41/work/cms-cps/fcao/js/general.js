function updateFormElementFromCheckBoxMarketing(checkBoxObj, formElement)
{
	if ($(formElement))
     {
	   if (checkBoxObj.checked)
         {
	      $(formElement).value = false;
	     }
	   else
         {
		  $(formElement).value = true;
		 }
     }
 }

   //Auto Completion of surnames
function autoCompletionSurname(passengerCountBasedOnRooms, startAutoCompletion)
{
   if($('autoCheck['+startAutoCompletion+']').checked) {
      for(index=startAutoCompletion; index<=(passengerCountBasedOnRooms); index++)
      {
         if ($('surName_'+index))
         {
         $('surName_'+index).value = $('surName_1').value;
      }
   }
}
}

function copyLeadPassengerAddress()
{
   if($('LeadPassengerAddressCopy').checked) {
         $('payment_0_street_address1').value = $('houseName').value;
         $('payment_0_street_address2').value = $('houseAddress1').value;
         $('payment_0_street_address3').value = $('townOrCity').value;
         $('payment_0_street_address4').value = $('countyName').value;
         $('payment_0_postCode').value = $('postCode').value;
         $('CHcountryName1').value = $('countryName1').value;
         $('CHcountryName').value = $('countryName1').value;
   }
}

function showDataProtection(elemId)
{
   obj = $(elemId);
   if (currentStyle(obj, 'display')=="block")
   {
      obj.style.display = "none";
   }else{
      obj.style.display = "block";
   }
}

function PopupForFcao(popURL,popW,popH,attr)
{
	 if (!popH) { popH = 350 }
	   if (!popW) { popW = 600 }
	   var winLeft = (screen.width-popW)/2;
	   var winTop = (screen.height-popH-30)/2;
	   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
	   popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
	   popupWin.close()
	  if(popURL.indexOf("www.360travelguide.net")> -1)
	  {
	     popupWin=window.open(popURL,"popupWindowVideoTour",winProp);
	  }
	  else
	  {
	     popupWin=window.open(popURL,"popupWindow",winProp);
	   }
	     popupWin.window.focus()
}

function showHiddenInformation(elemId){
   div = $(elemId);
   if(div.style.display=='none')
   {
      div.style.display = "block";
   }
   else
   {
      div.style.display = "none";
   }
}