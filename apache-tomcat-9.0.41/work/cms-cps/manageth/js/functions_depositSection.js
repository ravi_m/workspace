updateChargesForDeposits();
/*
 * Updates the card charge amounts for each of the deposit types.
 */
function updateChargesForDeposits()
{
  var depositTypes = $$('input[name=depositType]');
  var deposCardChargeDivs =  $$('#creditCardChargeAmt');
  var depositAmount = $$('#depositAmt');
  var cardCharge = null;
  
  var check_length=document.getElementsByClassName("payment-details").length-1;
  for(var i=0; i<check_length; i++)
  {
	  if(depositTypes[i]!= null){
	    depositAmount = (depositTypes[i].value)?depositAmountsMap.get(depositTypes[i].value): depositAmountsMap.get("fullCost");
	    cardCharge = calculateCardCharge(depositAmount);
	    cardCharge = parseFloat(depositAmount) + parseFloat(cardCharge);
	    document.getElementById("creditCardChargeAmt_"+depositTypes[i].value+"").innerHTML =parseFloat(cardCharge).toFixed(2);
	    	//if(deposCardChargeDivs[i]){
	    		//deposCardChargeDivs[i].innerHTML = parseFloat(cardCharge).toFixed(2);
	    	//}
	  }
  }
}

/*
 * Updates the card charge amounts for each of the other deposit types.
 */
function otherAmountUpdation()
{
	// chk other amount validate or not only if its selected.
	if(otherAmtValidateNt()){
		depositAmountsMap.put('PART_PAYMENT', document.getElementById("part").value);
		if(validateOtheramt(document.getElementById("part"))){
			depositAmountsMap.put('PART_PAYMENT', document.getElementById("part").value);
		}
	}
}

/**
 ** This method calculates the card charge for the deposit amount.
**/
function calculateCardCharge(amount)
{
  var applicableCharges= new Array();
  var cardCharge=0.0;

    applicableCharges = configurableCardCharge.split(",");


    if(applicableCharges!=null)
    {
     if(applicableCharges[0] > 0.0)
      {
      cardCharge = parseFloat(amount * (applicableCharges[0]/100));
    }
       if (parseFloat(applicableCharges[1]) > 0.0 && parseFloat(cardCharge) < parseFloat(applicableCharges[1]))
      {
        cardCharge = parseFloat(applicableCharges[1]);
      }
      if (parseFloat(applicableCharges[2]) > 0.0 && parseFloat(cardCharge) > parseFloat(applicableCharges[2]))
      {
         cardCharge = parseFloat(applicableCharges[2]);
      }
    }
    return cardCharge.toFixed(2);
}


/**
 ** Refreshes the PaymentInfo. A central function responsible for
 ** keeping data integrity of different amounts in
 ** in this payment for client side validation.
**/
function updatePaymentInfo(selectedDepositType)
{
PaymentInfo.depositType = trimSpaces((selectedDepositType));
if(PaymentInfo.depositType=="PART_PAYMENT")
   {
  /* if(document.getElementById("part").value=="0")
   {
   document.getElementById("otheramount").innerHTML = 0;
   }*/

	if(!document.getElementById("part").value){
		depositAmountsMap.put('PART_PAYMENT', 0);
	}
   }

   if(PaymentInfo.partialPaymentAmount == null || PaymentInfo.partialPaymentAmount == undefined)
   {
      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.totalAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
   }
   else
   {
      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
   }
   if(PaymentInfo.depositType=="PART_PAYMENT")
   {
   PaymentInfo.selectedDepositAmount = (depositAmountsMap.get('PART_PAYMENT') != null) ? depositAmountsMap.get('PART_PAYMENT') : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
   }
   calCardCharge  = calculateCardCharges(PaymentInfo.selectedDepositAmount);
   PaymentInfo.totalCardCharge = calCardCharge;
   PaymentInfo.calculatedPayableAmount = roundOff(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge , 2); //parseFloat(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge).toFixed(2);
   PaymentInfo.calculatedTotalAmount = roundOff((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge , 2); //parseFloat((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge).toFixed(2);

   var totalCharge = parseFloat(PaymentInfo.selectedDepositAmount) + parseFloat(calCardCharge);
     
	 /*document.getElementById("creditCardChargeFinalAmt").innerHTML = totalCharge.toFixed(2);*/
   
    if(PaymentInfo.depositType=="PART_PAYMENT"){
		if(validateOtheramt(document.getElementById("part")))
		{	
			document.getElementById("otheramount").innerHTML = totalCharge.toFixed(2);
		}else{
			document.getElementById("otheramount").innerHTML = 0;
		}
   }
   if(isNaN(PaymentInfo.selectedDepositAmount)){
	document.getElementById("debitCardFinalAmt").innerHTML = 0;
   }
   else{
   document.getElementById("debitCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
   }
   
   if(isNaN(PaymentInfo.selectedDepositAmount)){
	document.getElementById("giftCardFinalAmt").innerHTML = 0;
   }
   else{
   document.getElementById("giftCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
   }
   
    if(isNaN(PaymentInfo.selectedDepositAmount)){
	document.getElementById("creditCardChargeFinalAmt").innerHTML = 0;
   }
   else{
   document.getElementById("creditCardChargeFinalAmt").innerHTML = totalCharge.toFixed(2);
   }
   
    if(isNaN(PaymentInfo.selectedDepositAmount)){
    	document.getElementById("thomsonCardFinalAmt").innerHTML = 0;
     }else{
        document.getElementById("thomsonCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
      }
   
   
   
   
 
  /* document.getElementById("giftCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);*/
}

/**
 ** This method sets the default deposit option and payment method on load of the payment page.
**/
function setDefaultDepositOption(){
	if(isNewHoliday == 'false'){
		document.CheckoutPaymentDetailsForm.reset();
	}
	displayCreditCharges('debitcardType');
	/*if(depositAmountsMap.get('lowDeposit') != null){
		updatePaymentInfo('lowDeposit');
		selectedpaymentMode('quarter-amt');
	}else*/ 
	if(depositAmountsMap.get('deposit') != null){
		updatePaymentInfo('deposit');
		selectedpaymentMode('quarter-amt');
	}else{
		updatePaymentInfo('fullCost');
		selectedpaymentMode('full-amt');
	}
}


/**
 ** This method validate the other Amount.
**/
function validateOtheramt(input) {	

	if( (document.getElementById("getOtheramt").value=="true") ){
			var floatRegex = /^-?\d+\.?\d*$/,
			$parentEl = jQuery(input).parent(),
			maxVal = parseFloat($parentEl.find('.subTxt').text().match(/[+-]?\d+\.\d+/g)[0]);
			
				
						
		if(floatRegex.test(input.value)){
			
			if($parentEl.find('.subTxt1').text().length==0){
				if(input.value > 0 && input.value <= maxVal){
				removeError(input, true);
				
				}else{ 
				removeError(input, false);
				msg = 'Please enter valid amount';
				addError(input,msg);
				return false;
				}
			}else{
			
			depositVal = parseFloat($parentEl.find('.subTxt1').text().match(/[+-]?\d+\.?\d*/g)[0]);
				if(input.value > 0 && input.value <= maxVal){
					if(input.value >= depositVal){
						removeError(input, true);
					}else{ 
						removeError(input , false);
						msg = 'Minimum amount to be paid is the low deposit top up amount';
						alpha = "alpha";
						addError(input,msg,alpha);
						return false;
					}
					 
				}else{ 
				removeError(input, false);
				msg = 'Please enter valid amount';
				num = " idNum";
				addError(input,msg,num);
				return false;
				}
			}
			
		}
		else{
				removeError(input, false);
				msg = 'Please enter valid amount';
				num = " idNum";
				addError(input,msg,num);
				return false;
			}
		
	}
	return true;
}
// chk other amount validate or not only if its selected.
function otherAmtValidateNt(){	
	var id = jQuery('.payment-details.highlighted-div').attr('id');
	return id == "other-amt" ? true : false;
}

function addError(input,erMsg,variableClass){
	var $parentEl = jQuery(input).parent(),
		erSpan = '<span id="validation-fail" class="ml5px"></span>',
		//erMsg = 'Please enter valid amount',
		msgEl = '<div  class=" error-notify clear'  +variableClass+ '">'+erMsg+'</div>',
		validFailEl = $parentEl.find('#validation-fail'),
		erNotify = $parentEl.find('.error-notify'),
		validFlLn = validFailEl.length,
		erNotifyLn = erNotify.length,
		validEl = $parentEl.find('#validation-pass');
	if(validEl.length > 0){
		validEl.remove();
	}
	if(validFlLn == 0 && erNotifyLn==0){
		jQuery(msgEl).insertAfter(jQuery(input));
		jQuery(erSpan).insertAfter(jQuery(input));
		return false;
	}
	
	return false;
}

function removeError(input, flag){
	var $parentEl = jQuery(input).parent(),
		validFailEl = $parentEl.find('#validation-fail'),
		erNotify = $parentEl.find('.error-notify'),
		validSpan = '<span id="validation-pass" class="ml5px"></span>',
		validEl = $parentEl.find('#validation-pass');
		validFailEl.remove();
		erNotify.remove();
		if(validEl.length==0 && flag){
			jQuery(validSpan).insertAfter(jQuery(input));
		}
		
}

//Added by Prince : fix for validation-pass icon being displayed on page load.
//function to reset the other amt input field.

function resetInput(input) {
	var $parentEl = jQuery(input).parent(),
		validFailEl = $parentEl.find('#validation-fail'),
		erNotify = $parentEl.find('.error-notify'),
		validSpan = '<span id="validation-pass" class="ml5px"></span>',
		validEl = $parentEl.find('#validation-pass');
		
		if(validFailEl.length != 0) {
			validFailEl.remove();
			erNotify.remove();
		}
		if(validEl.length != 0) {
			validEl.remove();
		}
}

