updateChargesForDeposits();
/*
 * Updates the card charge amounts for each of the deposit types.
 */
function updateChargesForDeposits()
{
  var depositTypes = $$('input[name=depositType]');
  var deposCardChargeDivs =  $$('#creditCardChargeAmt');
  var depositAmount = $$('#depositAmt');
  var cardCharge = null;
  var amendmentCardCharge = null;
  var check_length = document.getElementsByClassName("deposite-box").length;
  if(PaymentInfo.amendmentChargeAmount != '')
  {
      amendmentCardCharge = calculateCardCharge(PaymentInfo.amendmentChargeAmount);
      amendmentCardCharge = parseFloat(PaymentInfo.amendmentChargeAmount) + parseFloat(amendmentCardCharge);
      if(applyCreditCardSurcharge=='true'){
		document.getElementById("amendmentChargeWithCC").innerHTML = parseFloat(amendmentCardCharge).toFixed(2);
		document.getElementById("creditCardChargeAmt_fullCost").innerHTML = parseFloat(amendmentCardCharge).toFixed(2);
		}
      	if(document.getElementById("debitCardChargeAmt_fullCost") != null){
			document.getElementById("debitCardChargeAmt_fullCost").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
		}
		if(document.getElementById("thomsonCardChargeAmt_fullCost") != null){
			 document.getElementById("thomsonCardChargeAmt_fullCost").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
		}
		if(document.getElementById("giftCardChargeAmt_fullCost") != null){
			 document.getElementById("giftCardChargeAmt_fullCost").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
		}
  }
  for(var i=0; i<check_length; i++)
  {
	  if(depositTypes[i]!= null){
		 if((depositTypes[i].value != 'PART_PAYMENT') && (depositTypes[i].value != 'onlyAmendCharge'))
		 {
		    depositAmount = (depositTypes[i].value)?depositAmountsMap.get(depositTypes[i].value): depositAmountsMap.get("fullCost");
		    cardCharge = calculateCardCharge(depositAmount);
		    cardCharge = parseFloat(depositAmount) + parseFloat(cardCharge);
		    if(applyCreditCardSurcharge=='true'){
		    document.getElementById("creditCardChargeAmt_"+depositTypes[i].value+"").innerHTML =parseFloat(cardCharge).toFixed(2);
		    	//if(deposCardChargeDivs[i]){
		    		//deposCardChargeDivs[i].innerHTML = parseFloat(cardCharge).toFixed(2);
		    	//}
		 }
	  }
  }
}
}

/*
 * Updates the card charge amounts for each of the other deposit types.
 */
function otherAmountUpdation()
{
	var otherAmount = null;
	if(document.getElementById("part")){
		otherAmount = document.getElementById("part");
	}else if(document.getElementById("partUnchangeable")){
		otherAmount = document.getElementById("partUnchangeable");
	}
	// chk other amount validate or not only if its selected.
	if(otherAmtValidateNt())
	{
		depositAmountsMap.put('PART_PAYMENT', otherAmount.value);
		if(validateOtheramt(otherAmount))
		{
			depositAmountsMap.put('PART_PAYMENT', otherAmount.value);
		}
	}
}

/**
 ** This method calculates the card charge for the deposit amount.
**/
function calculateCardCharge(amount)
{
  var applicableCharges= new Array();
  var cardCharge=0.0;

    applicableCharges = configurableCardCharge.split(",");


    if(applicableCharges!=null)
    {
     if(applicableCharges[0] > 0.0)
      {
      cardCharge = parseFloat(amount * (applicableCharges[0]/100));
    }
       if (parseFloat(applicableCharges[1]) > 0.0 && parseFloat(cardCharge) < parseFloat(applicableCharges[1]))
      {
        cardCharge = parseFloat(applicableCharges[1]);
      }
      if (parseFloat(applicableCharges[2]) > 0.0 && parseFloat(cardCharge) > parseFloat(applicableCharges[2]))
      {
         cardCharge = parseFloat(applicableCharges[2]);
      }
    }
    return parseFloat(Math.round(cardCharge * 100) / 100).toFixed(2);
}


/**
 ** Refreshes the PaymentInfo. A central function responsible for
 ** keeping data integrity of different amounts in
 ** in this payment for client side validation.
**/
function updatePaymentInfo(selectedDepositType)
{
	var otherAmount = null;
	if(document.getElementById("part")){
		otherAmount = document.getElementById("part");
	}else if(document.getElementById("partUnchangeable")){
		otherAmount = document.getElementById("partUnchangeable");
	}
	
   PaymentInfo.depositType = trimSpaces((selectedDepositType));
   if(PaymentInfo.depositType=="PART_PAYMENT")
   {
      if(!otherAmount.value){
	     depositAmountsMap.put('PART_PAYMENT', 0);
	  }
   }

   if(PaymentInfo.partialPaymentAmount == null || PaymentInfo.partialPaymentAmount == undefined)
   {
      if(PaymentInfo.depositType == 'onlyAmendmentCharge')
      {
         PaymentInfo.selectedDepositAmount = PaymentInfo.amendmentChargeAmount;
      }
	  else
	  {
      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.totalAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
		 PaymentInfo.selectedDepositAmount = parseFloat(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.amendmentChargeAmount).toFixed(2);
	  }
   }
   else
   {
      if(PaymentInfo.depositType == 'onlyAmendmentCharge')
      {
         PaymentInfo.selectedDepositAmount = PaymentInfo.amendmentChargeAmount;
   }
   else
   {
      PaymentInfo.selectedDepositAmount = (depositAmountsMap.get(PaymentInfo.depositType) != null) ? depositAmountsMap.get(PaymentInfo.depositType) : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
		 PaymentInfo.selectedDepositAmount = parseFloat(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.amendmentChargeAmount).toFixed(2);
	  }
   }
   if(PaymentInfo.depositType=="PART_PAYMENT")
   {
	   if(PaymentInfo.depositType == 'onlyAmendmentCharge')
      {
         PaymentInfo.selectedDepositAmount = PaymentInfo.amendmentChargeAmount;
      }
	  else
	  {
	   PaymentInfo.selectedDepositAmount = (depositAmountsMap.get('PART_PAYMENT') != null) ? depositAmountsMap.get('PART_PAYMENT') : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
		 PaymentInfo.selectedDepositAmount = parseFloat(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.amendmentChargeAmount).toFixed(2);
	  }
   }
   calCardCharge  = calculateCardCharges(PaymentInfo.selectedDepositAmount);

   PaymentInfo.totalCardCharge = calCardCharge;
   PaymentInfo.calculatedPayableAmount = roundOff(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge , 2); //parseFloat(1*PaymentInfo.selectedDepositAmount + 1*PaymentInfo.totalCardCharge).toFixed(2);
   PaymentInfo.calculatedTotalAmount = roundOff((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge , 2); //parseFloat((1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount) + 1*PaymentInfo.totalCardCharge).toFixed(2);

   var totalCharge = parseFloat(PaymentInfo.selectedDepositAmount) + parseFloat(calCardCharge);

   if(PaymentInfo.depositType=="PART_PAYMENT")
   {
	   if(validateOtheramt(otherAmount))
	   {
           var partPaymentAmount = (depositAmountsMap.get('PART_PAYMENT') != null) ? depositAmountsMap.get('PART_PAYMENT') : parseFloat(1*PaymentInfo.payableAmount  - 1*PaymentInfo.calculatedDiscount).toFixed(2);
           var partPayCC = calculateCardCharges(partPaymentAmount);
		   var partPaymentAmountCC = parseFloat(partPaymentAmount) + parseFloat(partPayCC);
		   if(applyCreditCardSurcharge=='true'){
		   document.getElementById("otheramount").innerHTML = "&pound;"+partPaymentAmountCC.toFixed(2)+" if paying by credit card";
		   }
		    if(PaymentInfo.aboutToPayBlock == "creditCardCharges")
			{
			    document.getElementById("creditCardCharges").style.display = "block";
			}
			if(PaymentInfo.aboutToPayBlock == "debitCardCharges")
			{
                document.getElementById("debitCardCharges").style.display = "block";
			}
			if(PaymentInfo.aboutToPayBlock == "thomsonCardCharges")
			{
                document.getElementById("thomsonCardCharges").style.display = "block";
			}
			if(PaymentInfo.aboutToPayBlock == "giftCardCharges")
			{
                document.getElementById("giftCardCharges").style.display = "block";
			}
			if(document.getElementById("paypalCharges").style.display == "block")
			{
				PaymentInfo.aboutToPayBlock = "paypalCharges";
			}
	   }
	   else
	   {
		   document.getElementById("otheramount").innerHTML = "";
		   if(PaymentInfo.amendmentChargeAmount == "")
		   {
		       document.getElementById("creditCardCharges").style.display = "none";
		       document.getElementById("debitCardCharges").style.display = "none";
		       document.getElementById("thomsonCardCharges").style.display = "none";
		       document.getElementById("giftCardCharges").style.display = "none";
			   document.getElementById("paypalCharges").style.display = "none";
		   }
	   }
   }
   if(isNaN(PaymentInfo.selectedDepositAmount))
   {
	   document.getElementById("debitCardFinalAmt").innerHTML = 0;
	   document.getElementById("giftCardFinalAmt").innerHTML = 0;
	   document.getElementById("creditCardChargeFinalAmt").innerHTML = 0;
	   document.getElementById("thomsonCardFinalAmt").innerHTML = 0;
	   document.getElementById("paypalFinalAmt").innerHTML = 0;
	   document.getElementById("paypalcreditFinalAmt").innerHTML = 0;
   }
   else
   {
       try{
       document.getElementById("debitCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
	   document.getElementById("giftCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
	   document.getElementById("creditCardChargeFinalAmt").innerHTML = totalCharge.toFixed(2);
	   document.getElementById("thomsonCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
	   document.getElementById("paypalFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
	   document.getElementById("paypalcreditFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
			 }catch(ex){
				 console.log(ex);
			 }
	   
	   if(PaymentInfo.depositType != "PART_PAYMENT")
	   {
	        if(PaymentInfo.aboutToPayBlock == "creditCardCharges")
			{
			    document.getElementById("creditCardCharges").style.display = "block";
			}
			if(PaymentInfo.aboutToPayBlock == "debitCardCharges")
			{
                document.getElementById("debitCardCharges").style.display = "block";
			}
			if(PaymentInfo.aboutToPayBlock == "thomsonCardCharges")
			{
                document.getElementById("thomsonCardCharges").style.display = "block";
			}
			if(PaymentInfo.aboutToPayBlock == "giftCardCharges")
			{
                document.getElementById("giftCardCharges").style.display = "block";
			}
			if(document.getElementById("paypalCharges").style.display == "block")
			{
				PaymentInfo.aboutToPayBlock = "paypalCharges";
			}
	   }
	   else
	   {
	       if(otherAmtValidateNt() && (document.getElementById("otheramount").innerHTML == "") && PaymentInfo.amendmentChargeAmount != "")
		   {
		       document.getElementById("debitCardFinalAmt").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
               document.getElementById("giftCardFinalAmt").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
	           document.getElementById("creditCardChargeFinalAmt").innerHTML = parseFloat(parseFloat(PaymentInfo.amendmentChargeAmount) + parseFloat(calculateCardCharge(PaymentInfo.amendmentChargeAmount))).toFixed(2);
	           document.getElementById("thomsonCardFinalAmt").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
			   document.getElementById("paypalFinalAmt").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
			   document.getElementById("paypalcreditFinalAmt").innerHTML = parseFloat(PaymentInfo.amendmentChargeAmount).toFixed(2);
			   
		   }
	   }
   }
   /*document.getElementById("creditCardChargeFinalAmt").innerHTML = totalCharge.toFixed(2);
   document.getElementById("debitCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
   document.getElementById("giftCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);*/
	if (depositAmountsMap.get('lowDepositPlusDD') != null) {
		jQuery(".ddInfoText").removeClass("hide");
	} else {
		jQuery(".ddInfoText").addClass("hide");
	}
}


/*Toggle radio button when Paypalcredit is selected on default*/

function removeRadioActive(id){
	var selectedOption= [];
	var parentNode= [];
	var selectText ='';
	
	selectedOption=jQuery('#'+id+'.radio.active');
	if(selectedOption.length > 0)
	{
	selectedOption.removeClass('active');
	parentNode= selectedOption.parents();
	parentNode[1].removeClassName('active');
	selectText=parentNode[0].getElementsByClassName("rtext")[0];
	selectText.innerHTML = 'selected' ? selectText.innerHTML="select":'';
	}
}

function selectedpaymentMode(id) {
	var getmode = id;
	var getfullDeposit = document.getElementById("full-amt");
	var gethalfDeposit = document.getElementById("half-amt");
	var getlowDeposit = document.getElementById("quarter-amt");
	var getpartDeposit = document.getElementById("other-amt");
	var depositClass = document.getElementsByClassName("payment-details");
	var paypalcreditSelection=document.getElementById("payPalCreditAmt");
	var mobSelection=document.getElementById("payPalCredit");
	var addActive=document.getElementById("paypalcreditSelect");
	var paypalCreditTandC= document.getElementsByClassName("paypalcreditTandC hide");
	var paypalDisabled= document.getElementById("paypalcreditType");
	var paypalSelectionText=document.getElementById("paypalSelection");

	if(document.getElementById("creditCardCharges").style.display == "block")
	{
		PaymentInfo.aboutToPayBlock = "creditCardCharges";
	}
	if(document.getElementById("debitCardCharges").style.display == "block")
	{
		PaymentInfo.aboutToPayBlock = "debitCardCharges";
	}
	if(document.getElementById("thomsonCardCharges").style.display == "block")
	{
		PaymentInfo.aboutToPayBlock = "thomsonCardCharges";
	}
	if(document.getElementById("giftCardCharges").style.display == "block")
	{
		PaymentInfo.aboutToPayBlock = "giftCardCharges";
	}
	if(document.getElementById("paypalCharges").style.display == "block")
	{
		PaymentInfo.aboutToPayBlock = "paypalCharges";
	}
	
	/*PayPalCredit changes*/
	if(paypalcreditSelection){
		paypalcreditSelection.removeClassName('active');
		mobSelection.removeClassName('active');
		addActive.innerHTML="PayPal credit is only available if you choose to pay your remaining holiday balance.";
		paypalCreditTandC.length > 0 ? paypalCreditTandC[0].addClassName("hide"):'';
		paypalDisabled.addClassName("paypalDisabled");
		mobSelection.addClassName("paypalradioDisabled");
		addActive.parentNode.addClassName("paypalcreditUnselect");
	
	}
	
	
    if (getmode == "noThanksDiv")
	{
		var noThanksVar = document.getElementById("noThanks");	
		noThanksVar.checked = "checked";
		document.getElementById("getOtheramt").value="false";
		document.getElementById("otheramount").innerHTML = "";
		resetInput("part");

	}
    else if (getmode == "full-amt")
	{
		var fullCheck = document.getElementById("full");
		fullCheck.checked = "checked";
		document.getElementById("getOtheramt").value="false";
		document.getElementById("otheramount").innerHTML = "";
		resetInput("part");
		
		/*PayPalCredit changes*/

		if(paypalcreditSelection){

			paypalcreditSelection.addClassName('active');
			mobSelection.addClassName('active');
			addActive.innerHTML="selected";
			addActive.parentNode.removeClassName("paypalcreditUnselect");
			mobSelection.removeClassName("paypalradioDisabled");
			displayCreditCharges("paypalcreditType");
			paypalCreditTandC.length > 0 ? paypalCreditTandC[0].removeClassName("hide"):'';
			removeRadioActive("debit");
			removeRadioActive("credit");
			removeRadioActive("gift");
			removeRadioActive("paypal");
		}
	}   

	else if (getmode == "half-amt")
	{
		var halfCheck = document.getElementById("half");
		halfCheck.checked = "checked";
		document.getElementById("getOtheramt").value="false";
		document.getElementById("otheramount").innerHTML = "";
		resetInput("part");
	}
	else if (getmode == "other-amt")
	{
		PaymentInfo.selectedDepositAmount = PaymentInfo.amendmentChargeAmount;
		if(document.getElementById("otheramount").innerHTML == "")
        {
			if(isNaN(PaymentInfo.selectedDepositAmount) || (PaymentInfo.selectedDepositAmount == ""))
			{
				document.getElementById("creditCardCharges").style.display = "none";
				document.getElementById("debitCardCharges").style.display = "none";
				document.getElementById("thomsonCardCharges").style.display = "none";
				document.getElementById("giftCardCharges").style.display = "none";
				document.getElementById("paypalCharges").style.display = "none";
			}
			else
			{
				document.getElementById("debitCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
				document.getElementById("giftCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
				document.getElementById("creditCardChargeFinalAmt").innerHTML = parseFloat(parseFloat(PaymentInfo.selectedDepositAmount) + parseFloat(calculateCardCharge(PaymentInfo.selectedDepositAmount))).toFixed(2);
				document.getElementById("thomsonCardFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
				document.getElementById("paypalFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
				document.getElementById("paypalcreditFinalAmt").innerHTML = parseFloat(PaymentInfo.selectedDepositAmount).toFixed(2);
				
			}
		}
		var partCheck = document.getElementById("other");
		partCheck.checked = "checked";
		document.getElementById("getOtheramt").value="true";

	}
	else
	{
		var lowdepositCheck = document.getElementById("initial");
		lowdepositCheck.checked = "checked";
		document.getElementById("getOtheramt").value="false";
		document.getElementById("otheramount").innerHTML = "";
	}
	if(id==='other-amt'){
		jQuery("#part").attr( "gfv_required","required");
	}else{
		resetInput("part");
	}
}


/**
 * * To send the selected deposit types to the backend
 */
function findPaymentOption(despositType) {

	console.log("despositType****    " + despositType);

	var url = "/cps/DepositTypeFinderServlet?token=" + token + tomcatInstance;
	parameterString = "depositType=" + despositType + "&token=" + token + tomcatInstance;
	if(despositType == 'PART_PAYMENT'){
		if(document.getElementById('part')){
			parameterString += '&deposit='+document.getElementById('part').value;
		}else if(document.getElementById('partUnchangeable')){
			parameterString += '&deposit='+document.getElementById('partUnchangeable').value;
		}
	}
	var request = new Ajax.Request(url, {
		method : "POST",
		parameters : parameterString,
		onSuccess : showCardType
	});
}



/**
 ** This method sets the default deposit option and payment method on load of the payment page.
**/
function setDefaultDepositOption(){
	if (document.getElementById('issue')) {
		document.getElementById('issue').style.display = 'none';
	}

	if(isNewHoliday == 'false'){
		document.CheckoutPaymentDetailsForm.reset();
	}
	//displayCreditCharges('debitcardType');
	/*if(depositAmountsMap.get('lowDeposit') != null){
		updatePaymentInfo('lowDeposit');
		selectedpaymentMode('quarter-amt');
	}else*/
    if(document.getElementById("noThanksDiv") != null){
	    updatePaymentInfo('onlyAmendmentCharge');
		selectedpaymentMode('noThanksDiv');
		findPaymentOption('noThanksDiv');
	}else if(depositAmountsMap.get('PART_PAYMENT') != null 
		&& (depositAmountsMap.get('deposit') == null || depositAmountsMap.get('deposit') == '0')
		&& (depositAmountsMap.get('fullCost') == null || depositAmountsMap.get('fullCost') == '0')){
		findPaymentOption('PART_PAYMENT');
		updatePaymentInfo('PART_PAYMENT');
	}else if(depositAmountsMap.get('deposit') != null){
		updatePaymentInfo('deposit');
		selectedpaymentMode('quarter-amt');
		findPaymentOption('deposit');
	}else{
		updatePaymentInfo('fullCost');
		selectedpaymentMode('full-amt');
		findPaymentOption('fullCost');
	}

}

/**
 ** This method validate the other Amount.
**/
function validateOtheramt(input) {

	if(document.getElementById("getOtheramt") && (document.getElementById("getOtheramt").value=="true") ){
			var floatRegex = /^-?\d+\.?\d*$/,
			$parentEl = jQuery(input).parent().parent(),
			maxVal = parseFloat($parentEl.find('.subTxt').text().match(/[+-]?\d+\.\d+/g)[1]);



		if(floatRegex.test(input.value)){

			if($parentEl.find('.subTxt1').text().length==0){
				if(input.value > 0 && input.value <= maxVal){
				removeError(input, true);

				}else{
				removeError(input, false);
				msg = 'Please enter valid amount';
				addError(input,msg);
				return false;
				}
			}else{

			depositVal = parseFloat($parentEl.find('.subTxt1').text().match(/[+-]?\d+\.?\d*/g)[0]);
				if(input.value > 0 && input.value <= maxVal){
					if(input.value >= depositVal){
						removeError(input, true);
					}else{
						removeError(input , false);
						msg = 'Minimum amount to be paid is the low deposit top up amount';
						alpha = "alpha";
						addError(input,msg,alpha);
						return false;
					}
				}else{
				removeError(input, false);
				msg = 'Please enter valid amount';
				num = " idNum";
				addError(input,msg,num);
				return false;
				}
			}

		}
		else{
				removeError(input, false);
				msg = 'Please enter valid amount';
				num = " idNum";
				addError(input,msg,num);
				return false;
			}

	}
	return true;
}
// chk other amount validate or not only if its selected.
function otherAmtValidateNt(){
	var id = jQuery('.deposite-box').find('.active').parent().attr('id');
	return id == "other-amt" ? true : false;
}

function addError(input,erMsg,variableClass){
	var $parentEl = jQuery(input).parent(),
		erSpan = '<span id="validation-fail" class="ml5px"></span>',
		//erMsg = 'Please enter valid amount',
		msgEl = '<div  class="message error-notify clear'  +variableClass+ '" style="float:none;width:150%">'+erMsg+'</div>',
		validFailEl = $parentEl.find('#validation-fail'),
		erNotify = $parentEl.find('.error-notify'),
		validFlLn = validFailEl.length,
		erNotifyLn = erNotify.length,
		validEl = $parentEl.find('#validation-pass');
		input.parentNode.addClassName("error");
		input.parentNode.removeClassName("valid");
	if(validEl.length > 0){
		validEl.remove();
	}
	if(validFlLn == 0 && erNotifyLn==0){
		jQuery(msgEl).insertAfter(jQuery(input));
		jQuery(erSpan).insertAfter(jQuery(input));
		return false;
	}

	return false;
}

function removeError(input, flag){
	var $parentEl = jQuery(input).parent(),
		validFailEl = $parentEl.find('#validation-fail'),
		erNotify = $parentEl.find('.message'),
		validSpan = '<span id="validation-pass" class="ml5px"></span>',
		validEl = $parentEl.find('#validation-pass');
		input.parentNode.removeClassName("error");
		input.parentNode.addClassName("valid");
		validFailEl.remove();
		erNotify.remove();
		if(validEl.length==0 && flag){
			jQuery(validSpan).insertAfter(jQuery(input));
		}

}

//Added by Prince : fix for validation-pass icon being displayed on page load.
//function to reset the other amt input field.

function resetInput(id) {
	/*var $parentEl = jQuery(input).parent(),
		validFailEl = $parentEl.find('#validation-fail'),
		erNotify = $parentEl.find('.error-notify'),
		validSpan = '<span id="validation-pass" class="ml5px"></span>',
		validEl = $parentEl.find('#validation-pass');

		if(validFailEl.length != 0) {
			validFailEl.remove();
			erNotify.remove();
		}
		if(validEl.length != 0) {
			validEl.remove();
		}*/

		var mainNode=jQuery("#other-amt");
		mainNode.find('.message').remove();
		mainNode.find('.row').hasClass( "error" )?mainNode.find('.row').removeClass( "error" ):"";
		mainNode.find('.row').hasClass( "valid" )?mainNode.find('.row').removeClass( "valid" ):"";
		jQuery("#"+id)[0].value="";
		jQuery("#"+id).removeAttr( "gfv_required");

}