

/* updateChargesInDropDown() */
function updateChargesInDropDown()
{
	var currencyEntity = String.fromCharCode(163); //for � symbol we can use String.fromCharCode(163)

  //Options in payment method drop down
  var options = $('payment_type_0').options;

  var totalTransAmount;
  // Total price is either deposit or full amount
  radioDeposit = $('radioDeposit');
  if( (radioDeposit != null) && (radioDeposit.checked) )
      totalTransAmount = $('depositH').value;
  else
      totalTransAmount = $('totalHolidayCostH').value;
      totalTransAmount = stripChars(totalTransAmount,",");
  for(var i=0; i<options.length ; i++)
  {
     //Get payment method e.g- MASTERCARD, VISA, SWITCH etc
     var method = options[i].value.split("|")[0];
     if(method=='' || method==null )
     {
       continue;
     }
     if($(method+"_charge")==null)
     {
    	 continue;
     }
     chargePercentage = $(method+"_charge").value;
     
     chargeCalc = ((chargePercentage / 100) * totalTransAmount).toFixed(2);
     if(chargeCalc>0)
     {
       options[i].text  = '';
       // Append new charge to card string
       options[i].text = paymenttypeoptions[i]+ " (" + currencyEntity + chargeCalc + " Charge)";
     }
  }


}