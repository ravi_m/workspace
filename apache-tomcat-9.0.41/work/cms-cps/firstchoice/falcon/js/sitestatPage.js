/* Sitestat page tracking with e-com functions placejust befor closing body tag */
sitestatPageTracker();

function sitestatPageTracker(){

	if (typeof( window[ 'sitestatpagename' ] ) == 'undefined'){
	
	var urlForSitestat = document.location.href;
		
		if(urlForSitestat.indexOf('.ie/') != -1){
		var urlForSitestat = urlForSitestat.split('.ie/');
		}
		
		if(urlForSitestat.indexOf('.co.uk/') != -1){
		var urlForSitestat = urlForSitestat.split('.co.uk/');
		}
		
		if(urlForSitestat.indexOf('8080/') != -1){//for Dev
		var urlForSitestat = urlForSitestat.split('8080/');
		}

		if(urlForSitestat.indexOf('8090/') != -1){//for Dev
		var urlForSitestat = urlForSitestat.split('8090/');
		}

		if(urlForSitestat.indexOf('10.40.16.64/') != -1){
		return false;
		}
		
		if(urlForSitestat[1]){
		
		var urlForSitestatSplit = urlForSitestat[1]
		var urlForSitestatSub = urlForSitestatSplit.toLowerCase();
		
		urlForSitestatSub = urlForSitestatSub.replace(/\s/g,"-");
		urlForSitestatSub = urlForSitestatSub.replace(/\//g,".");
		urlForSitestatSub = urlForSitestatSub.replace(/\/\//g,".");
		urlForSitestatSub = urlForSitestatSub.replace("..",".");
		urlForSitestatSub = urlForSitestatSub.replace("index.html","");
		urlForSitestatSub = urlForSitestatSub.replace("index.htm","");
		urlForSitestatSub = urlForSitestatSub.toArray();
		
			for(i=0; i<urlForSitestatSub.length; i++){
				if(urlForSitestatSub[i] == "." && urlForSitestatSub[i + 1] == "."){
				urlForSitestatSub[i] = "";
				}
			}
			
		urlForSitestatSub = urlForSitestatSub.toString();
		urlForSitestatSub = urlForSitestatSub.replace(/\,/g,"");

			for(i=0; i<urlForSitestatSub.length; i++){
				if(urlForSitestatSub.charAt(0) == "."){
				urlForSitestatSub = urlForSitestatSub.substr(1,urlForSitestatSub.length);
				}
			}
		}
		else{
		var urlForSitestatSub = urlForSitestat[1];
		urlForSitestatSub = "homepage.";
		}
		
		if(urlForSitestatSub == ""){
		urlForSitestatSub = "homepage.";
		}
			
	urlForSitestatSub += 'page';
	sitestatpagename = urlForSitestatSub;
	}

	/* for sitestat forms
	if (document.getElementById('SearchPanelForm') || document.getElementById('holiday_search')){
	IncludeJavaScript('/js-sitestat/sitestatforms.js');
	IncludeJavaScript('/js-sitestat/form.js');
	IncludeJavaScript('/js-sitestat/sitestatformsNewForm.js'); 
	} */

	//sitestat string js variable = sun.main.confirmation.page (or whatever the conf page id is
	if (sitestatpagename == 'sun.main.confirmation'){
 	IncludeJavaScript('/js-sitestat/sitestat_ecommerce.js');
 	/*IncludeJavaScript('/js-sitestat/ecommerce.js');*/
	ns_prod_price = ns_prod_price.replace(/,/g,"");
		if(document.location.href.indexOf('falconholidays.ie/') != -1){
		ns_myOrder = new ns_order('https://uk.sitestat.com/firstchoice/falconsouth/s?sun.main.comfirmation',ns_client_id,ns_order_id);
		}
		else{
		ns_myOrder = new ns_order('https://uk.sitestat.com/firstchoice/test-falcon/s?sun.main.comfirmation',ns_client_id,ns_order_id);	
		}
	ns_myOrder.addLine(ns_prod_id,ns_brand,ns_prod_grp,ns_shop,ns_qty,ns_prod_price);
	ns_myOrder.sendOrder();
	}

var secureModeCheck=window.location.href;
	if(document.location.href.indexOf('falconholidays.ie/') != -1){
sitestatPageViewString="http"+(secureModeCheck.indexOf('https:')==0?'s':'')+"://uk.sitestat.com/firstchoice/falconsouth/s?"+sitestatpagename;
		if(sitestatpagename == 'sun.main.confirmation'){
		sitestatPageViewString = sitestatPageViewString + "&order_id=" + ns_order_id;
		}
	}
	else{
	sitestatPageViewString="http"+(secureModeCheck.indexOf('https:')==0?'s':'')+"://uk.sitestat.com/firstchoice/test-falcon/s?"+sitestatpagename;	
		if(sitestatpagename == 'sun.main.confirmation'){
		sitestatPageViewString = sitestatPageViewString + "&order_id=" + ns_order_id;
		}
	}
	
	if((document.location.href.indexOf('8080/') != -1)){//for Dev	
	window.status = sitestatPageViewString;
	}
	
	if((document.location.href.indexOf('8090/') != -1)){//for Dev	
	window.status = sitestatPageViewString;
	}

	if(document.location.href.indexOf('tuifalconpat.ie') != -1){//for PAT
	window.status = sitestatPageViewString;
	}

sitestat(sitestatPageViewString);
}

function IncludeJavaScript(jsFile){
document.write('<script type=\"text/javascript\" src=\"'+ jsFile + '\"><\/script>');
}

function sitestat(ns_l){
ns_l+="&amp;ns__t="+(new Date()).getTime();ns_pixelUrl=ns_l;
ns_0=document.referrer;
ns_0=(ns_0.lastIndexOf("/")==ns_0.length-1)?ns_0.substring(ns_0.lastIndexOf("/"),0):ns_0;
	if(ns_0.length>0)ns_l+="&amp;ns_referrer="+escape(ns_0);
	if(document.images){ns_1=new Image();ns_1.src=ns_l;}
	else
	document.write('<img src=\"'+ns_l+'\" width=\"1\" height=\"1\" />');
}

function ns_order(ns_customerID,ns_client_id,ns_order_id){
this.id = ns_customerID;
this.ns_client_id = ns_client_id;
this.ns_order_id = ns_order_id;
this.orders = new Array();
this.addLine = ns_addLine;
this.sendOrder = ns_sendOrder;
}

function ns_addLine(prod_id,brand,prod_grp,shop,qty,prod_price){
this.orders[this.orders.length] = new Array(prod_id,brand,prod_grp,shop,qty,prod_price);
}

function ns_cookieVal(cookieName){
var thisCookie = document.cookie.split("; ");
	for (var i=0; i<thisCookie.length; i++){
		if (cookieName == thisCookie[i].split("=")[0]){
		return thisCookie[i].split("=")[1];
		}
	}
}

function ns_isEmpty(s){
return ((s == null) || (s.length == 0))
}

function ns_isSignedFloat (s){
var s = "" + s + "";
var i=0;
var seenDecimalPoint = false;
if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") ) i++;
if (s.charAt(i) == ".") return false;
	for (i ; i < s.length; i++){
    var c = s.charAt(i);
	if ((c == ".") && !seenDecimalPoint) seenDecimalPoint = true;
	else if ( (c > "9") || (c < "0") ) return false;
	}
	if (s.charAt(i-1)==".") return false;
	return true;
}

function ns_sendOrder(){
	if (ns_cookieVal("ns_order_id_"+this.ns_order_id)!="true"){
	document.cookie = "ns_order_id_"+this.ns_order_id+"=true";
	var keys = new Array('ns_prod_id','ns_brand','ns_prod_grp','ns_shop','ns_qty','ns_prod_price');
	var ns__t="&ns__t="+(new Date()).getTime();
		for (var i=0; i < this.orders.length; i++){
		var start = this.id;
		start += ns__t + '&ns_commerce=true&ns_type=hidden&ns_client_id=' + this.ns_client_id + '&ns_order_id=' + this.ns_order_id ;
		start += '&ns_orderlines=' + this.orders.length;
		start += '&ns_orderline_id=' + ((i*1)+1);
			for (var t=0; t < keys.length; t++){
			if (ns_isEmpty(this.orders[i][t])) this.orders[i][t]="ns_undefined";
				if ( (keys[t]=="ns_qty") || (keys[t]=="ns_prod_price") ){
			    if (!ns_isSignedFloat(this.orders[i][t])) this.orders[i][t]="ns_undefined";
				}
			eval("start += '&"+keys[t]+"="+escape(this.orders[i][t])+"';");
			}
		eval("nedstat"+i+" = new Image();");
		eval("nedstat"+i+".src = start;");
		}
	}
}

