var indexForCCResponse;
var totCardCharge=0;

/****************************DEPOSIT TYPE AJAX CALL*****************************/

//For deposit amount
function updateTransAmtForDepositAmountChange(value)
{
   var value  = trimSpaces(value);
   var params = "depositType=" + value;

   performAjaxUpdate( "DepositAmountServlet", params, responseUpdtTrAmtDepositAmountChange );
}

function responseUpdtTrAmtDepositAmountChange(request)
{
  if( (request.readyState == 4) && (request.status == 200) )
  {
    var result = request.responseText;

    var tmpArray    = result.split("|");
    var calculatedTotalAmount = tmpArray[1];
    var calculatedPayableAmount = tmpArray[0];

    //refresh Total holiday price in payment panel (at the top of the page)
    refreshCheckoutPaymentTotalAmount(calculatedPayableAmount );
  }
}
/****************************END DEPOSIT TYPE AJAX CALL*****************************/

/****************************CARD TYPE AJAX CALLS*****************************/
//For card charges
/*This function calls  CardChargesServlet for subtracting all card charges
 *
*/
function subtractCardChargesAll()
{
	updateAllCardCharges(0);
}



 /* function to update card charge of the single card.
  *e.g- When selection is changed in payment method drop down for CNP , card charge for earlier selection
  * is subtracted
  *
  *@param index represents the index of this transaction
  */
function updateAllCardCharges(index)
{
  var cardPaymentObject     = getCardPaymentObject();
  var depositType           = getBalanceType();
  var selCardName    = "";
  var selPaymentType = "";

  if( cardPaymentObject )
  {
	//selCardName ex - AMERICAN_EXPRESS  , VISA_DEBIT etc
    selCardName        = cardPaymentObject.cardType;
   //selCardPaymentType ex - Card , DCard etc
    //selCardPaymentType = cardPaymentObject.enumValue;
  }
  indexForCCResponse = index;
  var params ="selectedCard=" + selCardName + "&depositType=" + depositType;
  performAjaxUpdate( "CardChargesServlet", params, responseUpdateAllCardCharges );
}

function responseUpdateAllCardCharges(request)
{
  if(request.readyState==4 && (!request.status || request.status==200))
  {
    var result        = request.responseText;
    var rawChargeData = result.split("|");

/*
    var totalAmount = result.split("|")[0]*1;

    totalAmount = totalAmount.toFixed(2);

    var transactionAmount = result.split("|")[1]*1;
    var updatedCardCharge = result.split("|")[2]*1;
*/

    var index            = indexForCCResponse;
    var chargeDataObject = toCardChargeObject( rawChargeData );

    //refresh Total holiday price in payment panel (at the top of the page)
    refreshCheckoutPaymentTotalAmount(chargeDataObject.totalHolidayPriceWithCardCharge );
    
    //Update essential field:totalAmountPaid.
    updateTotalAmountPaid(chargeDataObject.totalHolidayPriceWithCardCharge );
    
    updateChargesForDeposits( chargeDataObject );
  }
}

/*
 * Updates the card charge amounts for each of the deposit types.
 */
/*function updateChargesForDeposits( chargeDataObject )
{
  var allDepositCreditCardChargeAmts = $$( ".creditCardChargeAmt" );

 //Get the array containing card charges if chargeDataObject is not null
  var cardChargesDeposit = chargeDataObject?chargeDataObject.depositCardCharges:null;
  if( allDepositCreditCardChargeAmts )
  {
    for( var i = 0, len = allDepositCreditCardChargeAmts.length; i < len; i++ )
    { 
      //Take charge as zero If cardChargesDeposit array  is null
      amount = cardChargesDeposit ? parseFloat(cardChargesDeposit[i]).toFixed(2):"0.00" ;
      $( allDepositCreditCardChargeAmts[i] ).innerHTML = amount;
    }
  }
}*/

function toCardChargeObject( chargeData )
{
  var cardChargeObject = new Object();
 
  cardChargeObject.depositCardCharge = new Object();
  
  cardChargeObject.totalAmountPayableWithCardCharge = chargeData[0] * 1;
  cardChargeObject.totalHolidayPriceWithCardCharge  = chargeData[1] * 1;
  //
   cardChargeObject.depositCardCharges = chargeData.slice(2);
  //
  return cardChargeObject;
}

/****************************END CARD TYPE AJAX CALL*****************************/
/****************************PROMOTIONAL CODE AJAX CALL*****************************/
/*
 * This function calls promCodeServlet to calculate
 * Promotional code discounts and gets calculated amount
 * and discount amount.
*/
function updatePromotionalCode(promoCode, balanceType)
{
   var params = "promotionalCode=" + promoCode + "&balanceType=" + balanceType;

   performAjaxUpdate( "promCodeServlet", params, responseUpdatePromotionalCodeDiscount );
}

function responseUpdatePromotionalCodeDiscount(request)
{
  if ((request.readyState == 4) && (request.status == 200))
  {
    var temp = request.responseText;

    temp = temp.split("|");

    var promCodeDiscount   = temp[0];
    var totalAmount        = temp[1];
    var isPromoCodeApplied = $("isPromoCodeApplied");

    // if(promCodeDiscount != 'Promotional code discount not available.')
    if( parseFloat(promCodeDiscount) )
    {
       ///Hidden fields
       isPromoCodeApplied.value = "true";
       $('promoDiscount').value = promCodeDiscount;
       displayPromoCodeSuccessMessage(promCodeDiscount);
       updateAmountDetails(totalAmount,promCodeDiscount);
       //Call Throbber
       priceThrob();
       displayPromoCodeInSummaryPanel(promCodeDiscount,"block");
    }
    else
    {
      //Hidden field
      isPromoCodeApplied.value = "false";
      ///Dispay error message
      displayPromoCodeErrorMessage(promCodeDiscount);
      displayPromoCodeInSummaryPanel(promCodeDiscount,"none");

      if( appConfig.disableRedeemBtn )
      {
        enableRedeemButton();
      }
      else
      {
        refreshPromoCodeField();
      }
    }
  }
  else
  {
    // Because the AJAX returned an error in the response we need to re-enable the redeem button.
    enableRedeemButton();
  }
}

/* Update total amount payable */
function updateAmountDetails(totalAmount,promCodeDiscount)
{
  PaymentInfo.calculatedDiscount = promCodeDiscount;
    
  var totalAmountMinusDiscount = parseFloat(1*PaymentInfo.totalAmount - 1*PaymentInfo.calculatedDiscount).toFixed(2);
  depositAmountsMap.put(payableConstants.FULL_COST ,totalAmountMinusDiscount);
  updatePaymentInfo();
  
  //Updates amounts 
  displayTheAmounts(payableConstants.TOTAL_AMOUNT_CLASS , totalAmountMinusDiscount);

  displayTheAmounts(payableConstants.PAYABLE_AMT_CLASS , totalAmountMinusDiscount);
  
  //Update outstanding balance in deposit amount section with promoCodeDiscount
  updateOutStandingBalances(promCodeDiscount);  
  
  //After applying promotional code refresh the card charges in card type drop down(if applicable).
  if( appConfig.updateCardCharges )
  {
    updateChargesInDropDown();
  }
  updateChargesForDeposits();
}

/**
 * Displays promotional code discount redeemed in summary panel
 * 
 * @param promCodeDiscount the amount which is redeemed
 * @param isDisplay indicates whether romCodeDiscount should be displayed or not 
 */
function displayPromoCodeInSummaryPanel(promCodeDiscount,isDisplay)
{
  promCodeDiscount = parseFloat(promCodeDiscount).toFixed(2);

  var promoCodeCaption = $( "promoCodeCaption" );

  if( promoCodeCaption )
  {
    Element.addClassName( promoCodeCaption, "promoCodeCaption");

    promoCodeCaption.style.display = isDisplay;

    if(isDisplay == "block")
    {
      $('promocodeDiscountAmt').innerHTML = promCodeDiscount;
    }
  }
  else if(isDisplay == "block")
  {
    var pricePanelItems = $("pricePanelList");
    if($('displayPromoLi')==undefined)
	{
      var promoLi         = document.createElement('li');
    
      promoLi.setAttribute("id","displayPromoLi");
 
      prmoText = document.createTextNode( getCurrency() + promCodeDiscount + " promo code discount" );

      promoLi.appendChild(prmoText);

      var captionLi = document.createElement('li');

      captionLi.setAttribute("id","captionli");
      captionliText = document.createTextNode("and all taxes and charges");
      captionLi.appendChild(captionliText);

      pricePanelItems.removeChild($("captionli"));
      pricePanelItems.appendChild(promoLi);
      pricePanelItems.appendChild(captionLi);
	}
  }
}

/**
 * Displays promotional code success message when promo code is successfully applied
 * Hides Promotional code section below success message(promotion code text field and redeem button)
 *
 */
function displayPromoCodeSuccessMessage(promCodeDiscount)
{
  var promoCodeSuccess = $("promoCodeSuccess");

  if( promoCodeSuccess )
  {
    var success = "Thanks, a discount of " + getCurrency() + promCodeDiscount + " has been applied to your booking";

    $('successMsg').innerHTML = success;

    // Lets now show the success message
    promoCodeSuccess.removeClassName( "hidden" );
    // TODO: Not sure if we need this as I believe the above line should handle the showing.
    promoCodeSuccess.show();

    // Lets hide the failure container and also the promotional code validation container.
    $( "promoCodeFailed" ).hide();
    $( "PromotionalCodeValidationContainer").innerHTML = "";
    $( "PromotionalCodeValidationContainer" ).hide();
    $( "promoCodeFlag" ).hide();
  }
}

/**
 * Updates outstanding balance which is displayed on payment page for low deposit and standard deposits
 * @param totalAmountMinusDiscount is the total holiday cost minus  promocode discount
 * 
 */
function updateOutStandingBalances(promCodeDiscount)
{
	var outstandingBalanceSpans = $$('span.outstandingBalance');
	for(var i=0,len = outstandingBalanceSpans.length; i< len;i++)
	{
        outstandingBalanceSpans[i].innerHTML -= 1*promCodeDiscount;
	}
}

/**
 * Displays error message in payment page when promo code discount is not available
 *
 */
function displayPromoCodeErrorMessage(promCodeError)
{
  //For Hugo etc
  var promoCodeFailed = $("promoCodeFailed");

  if( promoCodeFailed )
  {
    // Add message to error div and display (hiding other divs)
    $('failedMsg').innerHTML = promCodeError;
    promoCodeFailed.removeClassName( "hidden" );
    promoCodeFailed.show();
  }
}
/****************************END OF PROMOTIONAL CODE AJAX CALL*****************************/

/*********************AJAX helpers ******************/

///////////////////////////////////functions related to Updating Payment panel //////////////////////////////////////////////


/* Refreshes total amount payable to default
*/
function refreshCheckoutPaymentTotalAmount(calculatedPayableAmount)
{
	//TODO:NOT SURE ABOUT THIS
	//No UI refresh to be implemented for FC or FALCON.
	//calculatedPayableAmount = parseFloat(calculatedPayableAmount).toFixed(2);
	//$("CheckoutPaymentTotalAmount").innerHTML = calculatedPayableAmount;
}


/** Updates updateTotalAmountPaid field. This field is compared with server side amount.
 *  Exception will be shown if these amounts are not matching.
 *   
 **/
//TODO:This function is not applicable any more. To be removed.
function updateTotalAmountPaid(calculatedPayableAmount)
{
  // No UI refresh to be implemented for FC or FALCON.
 // $("payment_totamtpaid").value = calculatedPayableAmount;
}

///////////////////////////////////End of functions related to Updating Payment panel //////////////////////////////////////////////

/* Get the depositType selected
*
*/
function getBalanceType()
{
  var selectDepositValue = getDepositSelectedValue();

  if( !selectDepositValue )
  {
    selectDepositValue = appConfig.defaultDepositType;
  }

  return selectDepositValue;
}
/**********************End of AJAX Helpers ********/

/**********************End of AJAX  related functions *********/

/*********************************************************************/

function collectPayment(index)
{
  updateAllCardCharges(index);
}

/*
 * Gets the currency that is currently used for the page.
 */
function getCurrency()
{
  var currency = $("currencyText");

  if( currency )
  {
    currency = currency.value;
  }

  return currency;
}

/**************************************************************************/
