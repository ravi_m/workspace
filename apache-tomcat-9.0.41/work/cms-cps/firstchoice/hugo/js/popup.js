/* This is for popup functionalities for Detailed search results. */
function Popup(popURL,popW,popH,attr)
{
	popURL = clientDomainURL + popURL;

	PopupNew(popURL,popW,popH,attr);
}

/* This is for popup functionalities for Detailed search results. */
function PopupNew(popURL,popW,popH,attr)
{
   if (!popH) { popH = 350 }
   if (!popW) { popW = 600 }

   var winLeft = (screen.width-popW)/2;
   var winTop = (screen.height-popH-30)/2;
   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
   //popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
   //popupWin.close();
   popupWin=window.open(popURL,"popupWindow",winProp);
   popupWin.window.focus();
}



function popUpBookingConditionsHugo(clientUrl, accInv, flightInv, depDate)
{
 var d = new Date();
 var currentDate = d.getDate();
 var currentMonth = d.getMonth();
 currentMonth++;
 currentDate = currentDate+"";
 currentMonth = currentMonth+"";
 if (currentDate.length == 1)
 {
  currentDate = "0"+currentDate;
 }

 if (currentMonth.length == 1)
 {
  currentMonth = "0"+currentMonth;
 }
  if (accInv=="ThomsonSun")
 {
  accInv="FIRST_CHOICE"
 }
 if (flightInv=="TRACSA")
 {
  flightInv = "FIRST_CHOICE";
 }
 var formattedDate = d.getFullYear()+"-"+currentMonth +"-"+currentDate;
 var url=clientUrl+"/fcsun/page/booking/tandc.page?date="+formattedDate+"&"+"lang="+"en";
    if (accInv.length > 0)
    {
       url += "&accommInvSys=" + accInv;
    }
    if (flightInv.length > 0)
    {
       url += "&flightInvSys=" + flightInv;
    }
   url += "&depDate=" + depDate;
   //Check if T & c url is not empty
   if(tandCurl!="")
   {
     url = tandCurl;
   }
 window.open(url, "", "width=700,height=600,scrollbars=yes,resizable=yes");
}
function openPolicyWindows(href)
{
   var win = window.open(href,"","width=700,height=600,scrollbars=yes,resizable=yes");
    if (win && win.focus) win.focus();
}
function PopupSprocket(popURL,popW,popH,attr)
{
   if (!popH) { popH = 350 }
   if (!popW) { popW = 600 }
   popURL = popURL;

   var winLeft = (screen.width-popW)/2;
   var winTop = (screen.height-popH-30)/2;
   var winProp='width='+popW+',height='+popH+',left='+parseInt(winLeft)+',top='+winTop+','+attr;
   popupWin=window.open('',"popupWindow","\'"+winProp+"\'");
   popupWin.close()
  if(popURL.indexOf("www.360travelguide.net")> -1)
  {
     popupWin=window.open(popURL,"popupWindowVideoTour",winProp);
  }
  else
  {
     popupWin=window.open(popURL,"popupWindow",winProp);
   }
     popupWin.window.focus()
}