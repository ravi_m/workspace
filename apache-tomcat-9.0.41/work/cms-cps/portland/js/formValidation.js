

/*  Trim spaces from start and end of string   */
function trimSpaces(objval)
{
  // Remove Space at start
  RESpace = /^\s*/i;
  objval = objval.replace(RESpace, '');
  // Remove Space at end
  RESpace = /\s*$/i;
  objval = objval.replace(RESpace, '');
  return objval;
}
//Strips the characters passed through the function.
function stripChars(s, bag)
{
   var i;
   var returnString = "";
   // Search through string's characters one by one.
   // If character is not in bag, append to returnString.
   for (i = 0; i < s.length; i++)
   {
      // Check that current character isn't whitespace.
      var c = s.charAt(i);
      if (bag.indexOf(c) == -1) returnString += c;
   }
   return returnString;
}

function uncache(url)
{
   var d = new Date();
   var time = d.getTime();
   var newUrl;
   if (url.indexOf("?") != -1)
      newUrl = url + "&time="+time;
   else
      newUrl = url + "?time="+time;

   return newUrl;
}


/*This file contains functions related Validation of payment panel *
 */
/* Used to validate fields in payment methods */
var checkCardNumber=false;
var checkName=false;
var checkPostCode=false;
var checkAddressField1=false;
var checkAddressField2=false;
function validatePaymentFields(inputObj)
{

   thisObjType=inputObj.id.split("_")[2];

   //get unique method  identifier for this payment method
   var temp=(inputObj.id).split("_");
   var uniqueIdentifier=temp[0]+"_"+temp[1];
   var transVal=temp[1];
   var paymentType=document.getElementById("payment_type_0").value;

    // Card number (method: Card,CNP)

   if(thisObjType=='cardNumberId'&&inputObj.value!='')
   {
      checkCardNumber=true;
      if(paymentType.indexOf("AMERICAN_EXPRESS")>=0)
      {
         Pat1 = /^[0-9]{15}$/;
      }
      else
      {
            Pat1 = /^[0-9]{16,20}$/;
      }
      inputObj.value = removeAllSpaces(inputObj.value)
      obj_value = inputObj.value
      if (!Pat1.test(obj_value))
      {
         checkCardNumber=false;
             return setFocus('Please enter a valid card number.',inputObj)
      }
   }

    //Name on card (method: Card,CNP)
    if(thisObjType=='nameOnCardId'&&inputObj.value!='')
    {
      checkName=true;
             var Pat1 = /^[-a-zA-Z&@ \"\',\.\x27]*$/;
             if (!Pat1.test(inputObj.value))
      {
            checkName=false;
         return setFocus('Please enter a valid name.' , inputObj)
      }
    }

    //post code (method: Card,CNP)
    if(thisObjType=='postCodeId'&&inputObj.value!='')
    {
       checkPostCode=true;
       obj_value = inputObj.value.toUpperCase();
       inputObj.value = inputObj.value.toUpperCase();
       Pat1 = /^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {0,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$/;
       if (!Pat1.test(obj_value))
       {
      checkPostCode=false;
         return clearAndFocus('We cannot validate the post code you have entered. Please check and re-enter. ',inputObj);
       }
    }

     //Security code (method: Card,CNP)
    if(thisObjType=='securityCodeId'&&inputObj.value!='')
    {

    checkCardSecurityCode=true;
        // method=document.getElementById("payment_"+transVal+"_paymenttypecode").value;
        // securityCodeLength=document.getElementById(method+"_securityCodeLength").value;
       if (paymentType!='')
       {

         var cardtype= paymentType.split("|")[0];
         var securityCodeLength=document.getElementById(cardtype+"_securityCodeLength").value
         Pat1 = /^[0-9 ]*$/;
         if(paymentType.indexOf("AMERICAN_EXPRESS")>=0 && inputObj.value.length != securityCodeLength)
         {
       checkCardSecurityCode=false;
             return clearAndFocus('Please enter a valid security number (the '+securityCodeLength+' digit code above the card number on the front of your card)', inputObj);
         }
         else if( (inputObj.value.length != securityCodeLength)  || (!Pat1.test(inputObj.value)))
         {
       checkCardSecurityCode=false;
             return clearAndFocus('Please enter a valid security number (the last '+securityCodeLength+' digits in the signature strip on the reverse of your card)',inputObj)
         }
       }
    }

    //Start date
   if(thisObjType=='startDateId'&&inputObj.value!='')
   {
       //ddmm
     var val=document.getElementById(inputObj.id).value;
     var datePattern=/^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[0-9]|2[0-9])$/;
     if(!val.match(datePattern))
     {
          return clearAndFocus('Please enter date in mmyy format only',inputObj)
     }
   }

    //Expiry date
   if(thisObjType=='expiryDateId'&&inputObj.value!='')
   {
      //ddmm
     var val=document.getElementById(inputObj.id).value;
     checkStartDate(expiryDateObj);
   }



    //Issue number (method: Card,CNP)
    if(thisObjType=='issueNumberId'&&inputObj.value!='')
    {
               //TODO
    }


    //Transaction no  (method: Card using PDQ )
    if(thisObjType=='transactionNoId' &&inputObj.value!='')
    {
               //TODO
    }

    //Voucher no.  (method: Voucher)
    if(thisObjType=='voucherId' &&inputObj.value!='')
    {
        if(inputObj.value.length != 8)
        {
           return clearAndFocus('Please enter voucher no length as 8',inputObj)
        }
        else
        {

        }
    }

    //Bank sort code (method: Cheque  )
    if(thisObjType=='bankSortCodeId' &&inputObj.value!='')
    {
               //TODO
    }

    //Account no.  (method: Cheque )
    if(thisObjType=='accountNoId' &&inputObj.value!='')
    {
               //TODO
    }

     //Name on cheque  (method:  Cheque  )
    if(thisObjType=='nameOnChequeId' &&inputObj.value!='')
    {
             var Pat1 = /^[-a-zA-Z&@ \"\',\.\x27]*$/;
             if (!Pat1.test(inputObj.value)) { return setFocus('Please enter a valid name.' , inputObj) }
    }

    //Address Field 1
    if(thisObjType=='streetAddress1'&&inputObj.value!='')
    {
    	checkAddressField1=true;
    	var Pat1 = /^[A-Za-z0-9'\./()\-& ]+$/;
    	obj_value = inputObj.value;
        if (!Pat1.test(obj_value))
        {
    	  checkAddressField1=false;
          return clearAndFocus('Address 1 is incorrect.',inputObj);
        }
    }

    //Address Field 2
    if(thisObjType=='streetAddress2'&&inputObj.value!='')
    {
       checkAddressField2=true;
   	   var Pat1 = /^[A-Za-z0-9'\./()\-& ]+$/;
	   obj_value = inputObj.value;
       if (!Pat1.test(obj_value))
       {
    	 checkAddressField2=false;
         return clearAndFocus('Address 2 is incorrect.',inputObj);
       }
    }


    //Address Field 3
    if(thisObjType=='streetAddress3'&&inputObj.value!='')
    {
    	var Pat1 = /^[A-Za-z0-9'\./()\-& ]+$/;
    	obj_value = inputObj.value;
        if (!Pat1.test(obj_value))
        {
          return clearAndFocus('Address 3 is incorrect.',inputObj);
        }
    }

    //Address Field 4
    if(thisObjType=='streetAddress4'&&inputObj.value!='')
    {
    	var Pat1 = /^[A-Za-z0-9'\./()\-& ]+$/;
    	obj_value = inputObj.value;
       if (!Pat1.test(obj_value))
       {
         return clearAndFocus('Address 4 is incorrect.',inputObj);
       }
    }


}
 var checkCardSecurityCode=false;
function validateSecurityCode(thisObj)
{
  /* if(paymentType.indexOf("AMERICAN_EXPRESS")>=0 && inputObj.value.length != securityCodeLength)
         {
             return clearAndFocus('Please enter a valid security number (the '+securityCodeLength+' digit code above the card number on the front of your card)', inputObj);
         }
         else if( (inputObj.value.length != securityCodeLength)  || (!Pat1.test(inputObj.value)))
         {
             return clearAndFocus('Please enter a valid security number (the last '+securityCodeLength+' digits in the signature strip on the reverse of your card)',inputObj)
         }*/
     if(cardChange){
  if(thisObj.value.length<3 || thisObj.value.length>4)
  {
    checkCardSecurityCode=false;
     return clearAndFocus('Please enter a valid security number (the '+securityCodeLength+' digit code above the card number on the front of your card)', thisObj);
  }
  else if(thisObj.value.length != securityCodeLength)
  {
     checkCardSecurityCode=false;
     return clearAndFocus('Please enter a valid security number (the last '+securityCodeLength+' digits in the signature strip on the reverse of your card)',thisObj)
  }
   else
  {
     checkCardSecurityCode=true;
  }
     }
}

/* Remove all spaces in value */
function removeAllSpaces(objval)
{
   RESpace = /\s/ig;
  objval = objval.replace(RESpace, '');
  return objval
}

  /* Set Focus to field and show message */
function setFocus(alertmessage,obj)
{
  alert(alertmessage);
   obj.focus();
   return false;
}
function clearAndFocus(alertmessage,obj)
{
 alert(alertmessage);
   obj.value='';
   obj.focus();
   return false;
}
 function checkIssueNumber(inputObj)
 {
   if(inputObj.value!='')
	 {
			  Pat1 = /^[0-9 ]*$/;
			   obj_value = inputObj.value;
		       if (!Pat1.test(obj_value))
		       {

		         return clearAndFocus('Issue Number is incorrect.',inputObj);
		       }
		    }
	 }
