

var TotalPrice;


var iCurrentYear;
var iCurrentMonth;

function GoToNextPage()
{

	//convert the postcode to upper case
	var postcode = document.getElementById('payment_0_postCodeId').value;
	document.getElementById('payment_0_postCodeId').value = postcode.toUpperCase();
	//do an empty check on submit for postcode/address1/address2 field since they have been autofilled
	if (removeAllSpaces(document.getElementById('payment_0_postCodeId').value) != '' )
	{
		checkPostCode = true;
	}
	if (removeAllSpaces(document.getElementById('payment_0_streetAddress1').value) != '' )
	{
		checkAddressField1 = true;
	}
	if (removeAllSpaces(document.getElementById('payment_0_streetAddress2').value) != '' )
	{
		checkAddressField2 = true;
	}
	  if(!cardChange)
	{
		   alert('Please select a card .') ;
		   return;
	}
		else if(!checkCardNumber)
	{
			 alert('Please enter card number') ;
			  document.getElementById("payment_0_cardNumberId"). focus();
		   return;
	}
	else if(!checkCardSecurityCode)
	{
			 alert('Please enter security number') ;
			 document.getElementById("payment_0_securityCode"). focus();
		   return;
	}
		else if(!checkName)
	{
			 alert('Please enter Name') ;
			 document.getElementById("payment_0_nameOnCardId"). focus();
		   return;
	}
		else if(!checkExpiryMonth)
	{
			 alert('Please Select Expiry Month') ;
		   return;
	}
		else if(!checkExpiryYear)
	{
			 alert('Please Select Expiry Year') ;
		   return;
	}
		else if(!checkAddressField1)
		{
			alert('Please enter Address Line 1.\nPlease note that the following characters will not be accepted:\n! ? $ % ^ & * ( ) _ + = - ; # ~ : < > , / \ |')
		}
		else if(!checkAddressField2)
		{
			alert('Please enter Address Line 2.\nPlease note that the following characters will not be accepted:\n! ? $ % ^ & * ( ) _ + = - ; # ~ : < > , / \ |')
		}
		else if(!checkPostCode)
		{
				 alert('Please enter Post Code') ;
				 document.getElementById("payment_0_postCodeId"). focus();
			   return;
		}
	else
	{
     var tomcatInstance = document.getElementById("tomcatInstance").value;
		    var token = document.getElementById("token").value;
		document.paymentdetails.method="post";
	document.paymentdetails.action="/cps/processPayment?token="+token+"&b=17000"+"&tomcat="+tomcatInstance;
	//disables the pay button.
	disablePayButton();
	document.paymentdetails.submit();
	}
}

function autoCompleteAddress()
{
	jQuery('#payment_0_streetAddress1').val(street_address1);
	jQuery('#payment_0_streetAddress2').val(street_address2);
	jQuery('#payment_0_streetAddress3').val(street_address3);
	jQuery('#payment_0_streetAddress4').val(street_address4);
	jQuery('#payment_0_postCodeId').val(postCode);
}

function disablePayButton()
{
	jQuery('#continue').html('<img src="/cms-cps/portland/images/confirm.gif" alt="Confirm" id="ContinueText" class = "pay" width="71" height="17" border="0" />');
}

function toggleOverlayPortland()
{
  var overlayZIndex = 99;
  var zIndex = 100;
  var prevOverlay;
  var stickyOpened = false;
  jQuery("a.stickyOwner").click(function(e){
    var overlay = "#" + this.id + "Overlay";
	if (!stickyOpened)
	{
		prevOverlay = overlay;
	}
	if (prevOverlay != overlay)
	{
		jQuery(prevOverlay).hide();
		stickyOpened = false;
	}
	var pos = jQuery("#"+this.id).offset();
	// The top and left offset are browser dependent and are set in the payment page depending on the
	// browser. The values can be set in the payment page.
	var left = parseInt((1*pos.left-leftOffset),10);
	var top = parseInt((1*pos.top+topOffset),10);
	jQuery(overlay).show();
	jQuery(overlay).css("left",left);
	jQuery(overlay).css("top",top);
	prevOverlay = overlay;
	stickyOpened = true;
	jQuery(overlay + ".genericOverlay").css("z-index",zIndex);
	zIndex++;

	if (jQuery(overlay).parent(".overlay") != null){
	  jQuery(overlay).parent(".overlay").css("z-index",overlayZIndex);
	  overlayZIndex++;
	}
    return false;
  });

  jQuery("a.close").click(function(){
    var overlay = this.id.replace("Close","Overlay");
    jQuery("#" + overlay).hide();
    return false;
  });
}