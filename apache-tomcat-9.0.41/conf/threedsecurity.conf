#================================================================================
# 3DSecure configuration details - Seperated from main configuration file cps.conf
# This file requires for cps12.00.00 and higher version releases.
#================================================================================

# NOTE - THIS FILE SHOULD CONTAIN ALL CONFIGURATION ITEMS IRRESPECTIVE OF
#        ENVIRONMENT SPECIFIC OR NOT. ONLY VALUES SHOULD CHANGE.


#================================================================================
#History:
# 15/06/10 01 Ramesh Babu   Initial file created to deploy cps12.00.00 release into
#			    PAT environment.
# 30/06/10 02 Ramesh Babu   Added 3DSecure for all cards for THAO, FCAO & webCruise brands as per yasin request.
# 30/06/10 03 Ramesh Babu   Removed Masterlistlogos item as per cps12.01.00 release.
# 09/07/10 04 Ramesh Babu   Enabled 3DSecure for WSS to keep sync with PRD.
# 11/08/10 05 Ramesh Babu   Enabled the 3DSecure for WSS for all card types.
# 20/08/10 07 Ramesh Babu   Enabled the 3DSecure for GREENFIELD & PORTLAND for all card types. - Chris mail Tue 17/08/2010 14:02
# 25/08/10 05 Ramesh Babu   Enabled the 3DSecure for Tfly & FCFO for all card types. - Chris mail Tue 24/08/2010 16:45
# 01/09/10 09 Ramesh Babu   Enabled the 3DSecure for Thomson BYO & FCSUN for all card types. - Marc PRD signoff mail Wed 01/09/2010 07:45
# 01/09/10 10 Ramesh Babu   Added new item (Kronos call center validation list) as per cps13.00.00 release.
# 06/09/10 11 Ramesh Babu   Added new fatal error number (163) as per Gillian mail - Mon 06/09/2010 16:15
# 08/09/10 12 Ramesh Babu   Enabled 3DSecure for GF simply & Beach.
# 09/09/10 13 Ramesh Babu   Removed 163 number from fatalError list as per Gillian mail - Thu 09/09/2010 08:57
# 04/10/10 14 Ramesh Babu   Moved 187 error code from non-fatal to fatal Error list - SR3243385
# 16/11/10 15 Ramesh Babu   Added new items as per cps14.00.00 release.
# 08/12/10 16 Ramesh Babu   Enabled 3DSecure for Thomson new payment page (ThomsFHPI) as per OLD payment page for TH R8 release.
# 16/03/11 17 Ramesh Babu   Added new items as per cps15.00.00 to .02 releases.
# 22/03/11 18 Ramesh Babu   Updated Tfly and FCFO fatal & non fatal error codes same as other application as per Marc mail - Mon 21/03/2011 17:16
# 12/08/11 19 Ramesh Babu   Amex card included in 3DSecure list for THAO & FCAO as per CPS R17.1 release signoff call.
# 12/08/11 20 Ramesh Babu   Rearranged FCAO validationlist as per production file to keep in sync.
# 22/08/11 21 Ramesh Babu   Enabled AMERICAN_EXPRESS card suppor for TFly, FCFO and Thomson BYO as per SR-SR0006093683
# 09/09/11 22 Ramesh Babu   Enabled AMERICAN_EXPRESS card suppor for FCSUN BYO and Thomson Cruise as per INC000006203958
# 13/10/11 23 Ramesh Babu   Added new properties for Shorex applicaiton and added eci properties as per cps18.00.00 to .04 releases.
# 09/11/11 24 Ramesh Babu   Modified B2C and B2B properties to support Cuise excursions applications as per Savitha mail - Wed 09/11/2011 11:34
#================================================================================

# Default 3D Security Fatal and non fatal error codes
# If any client application needs brand specific Fatal and non fatal error codes then brand specific entries should be added.
#Eg:cps.TFly.fatalError=157,159,160,162,173
#Eg:cps.TFly.nonFatalError=187
# Fatal and non fatal error codes(added for 3D security).
cps.fatalError=151,152,153,154,155,156,158,161,164,165,166,167,168,169,170,171,172,174,175,176,177,178,179,180,181,182,183,184,185,186,188,189,600,601,602,603,604,605,606,607,608,609,610
cps.nonFatalError=187,157,163,160
cps.3dNonFatalError=159,160,162,173
cps.hccNonFatalError=159,160,162,173,187,157,163


# Categories of cards
cps.securelogo.applicablecardgroup=MasterCardGroup,VisaGroup,americanexpressgroup
cps.securelogo.mastercardgroup=Mastercard,Switch,Maestro,Debit Mastercard
cps.securelogo.visagroup=VISA,VISA Delta,VISA Electron,VISA Purchasing
cps.securelogo.americanexpressgroup=American Express

cps.eci.mastercardgroup.Authorized=02
cps.eci.mastercardgroup.Attempted=01
cps.eci.mastercardgroup.NotAuthorized=07
cps.eci.visagroup.Authorized=05
cps.eci.visagroup.Attempted=06
cps.eci.visagroup.NotAuthorized=0
cps.eci.JCB.Authorized=05
cps.eci.JCB.Attempted=06
cps.eci.americanexpressgroup.Authorized=05
cps.eci.americanexpressgroup.Attempted=06
cps.eci.americanexpressgroup.NotAuthorized=0


#Below is brand specific non payment data validation list.
#In any brand, if certain field is mandatory in UI and it has to be validated on server side
#then that field has to be given in the below list so that it can be validated as part of non payment data validation.
#Value should match with one of the NonPaymentDataPanel enumeration value.
cps.GENERICJAVA.validationList=
cps.GENERICDOTNET.validationList=
cps.B2CShorex.validationList=
cps.NEWSKIES.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.NEWSKIESM.validationList=foreName,country,houseName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.FCX.validationList=houseName,foreName,sureName,title,addressLine1,city,dayTimePhone,emailAddress,country,postCode
cps.THX.validationList=houseName,foreName,sureName,title,addressLine1,city,dayTimePhone,emailAddress,country,postCode
cps.FIRSTCHOICE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.THOMSON.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.AtcomRes.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.MANAGEFIRSTCHOICE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.MANAGETHOMSON.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.HYBRISTHCRUISE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.HYBRISFALCON.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.HYBRISTHFO.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.MFLIGHTONLY.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.THOMSONMOBILE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.FIRSTCHOICEMOBILE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.FALCONMOBILE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.ANCTHOMSON.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.ANCFIRSTCHOICE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.ANCTHFO.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.ANCTHCRUISE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.ANCFALCON.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.ANCFJFO.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.TUITH.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.HYBRISFALCONFO.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.FALCONFOMOBILE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.CRUISEMOBILE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.TUIFC.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.TUIFALCON.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.TUITHCRUISE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.TUICRUISEDEALS.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.TUIHMCRUISE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.TUITHSHOREX.validationList=
cps.TUITHRIVERCRUISE.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.TUITHFO.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.TUIFALCONFO.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.TUICS.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.TUIES.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted


#Flag  which indicates whether 3D secure should be enabled or not
cps.GENERICJAVA.3DSecure=false
cps.GENERICDOTNET.3DSecure=false
cps.B2CShorex.3DSecure=true
cps.NEWSKIES.3DSecure=true
cps.NEWSKIESM.3DSecure=true
cps.FCX.3DSecure=true
cps.THX.3DSecure=true
cps.FIRSTCHOICE.3DSecure=true
cps.THOMSON.3DSecure=true
cps.MANAGEFIRSTCHOICE.3DSecure=true
cps.MANAGETHOMSON.3DSecure=true
cps.HYBRISTHCRUISE.3DSecure=true
cps.HYBRISFALCON.3DSecure=true
cps.HYBRISTHFO.3DSecure=true
cps.MFLIGHTONLY.3DSecure=true
cps.THOMSONMOBILE.3DSecure=true
cps.FIRSTCHOICEMOBILE.3DSecure=true
cps.FALCONMOBILE.3DSecure=true
cps.ANCTHOMSON.3DSecure=true
cps.ANCFIRSTCHOICE.3DSecure=true
cps.ANCTHFO.3DSecure=true
cps.ANCTHCRUISE.3DSecure=true
cps.ANCFALCON.3DSecure=true
cps.ANCFJFO.3DSecure=true
cps.TUITH.3DSecure=true
cps.HYBRISFALCONFO.3DSecure=true
cps.FALCONFOMOBILE.3DSecure=true
cps.CRUISEMOBILE.3DSecure=true
cps.TUIFC.3DSecure=true
cps.TUIFALCON.3DSecure=true
cps.TUITHCRUISE.3DSecure=true
cps.TUICRUISEDEALS.3DSecure=true
cps.TUIHMCRUISE.3DSecure=true
cps.TUITHSHOREX.3DSecure=true
cps.TUITHRIVERCRUISE.3DSecure=true
cps.TUITHFO.3DSecure=true
cps.TUIFALCONFO.3DSecure=true
cps.TUICS.3DSecure=true
cps.TUIES.3DSecure=true

# Below are List of schemes for 3DS. For supporting 3D security by scheme,
# card schemes should be added here with comma as separator.
# Following schemes are supported for 3DS by CPS and Datacash.
# Switch,Debit Mastercard,VISA,Maestro,VISA Delta,Mastercard,VISA Electron,VISA Purchasing,American express
cps.default.3DCardScheme=Switch
cps.GENERICJAVA.3DCardScheme=Switch
cps.GENERICDOTNET.3DCardScheme=Switch
cps.B2CShorex.3DCardScheme=Switch,Debit Mastercard,VISA,Maestro,VISA Delta,Mastercard,VISA Electron,TUI MASTERCARD
cps.NEWSKIES.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA,TUI MASTERCARD
cps.NEWSKIESM.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA,TUI MASTERCARD
cps.MANAGEFIRSTCHOICE.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.MANAGETHOMSON.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.HYBRISTHCRUISE.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA,TUI MASTERCARD
cps.HYBRISFALCON.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA
cps.HYBRISTHFO.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.MFLIGHTONLY.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.THOMSONMOBILE.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.FIRSTCHOICEMOBILE.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.FALCONMOBILE.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA
cps.FCX.3DCardScheme=Switch,Debit Mastercard,VISA,Maestro,VISA Delta,Mastercard,VISA Electron,American Express,TUI MASTERCARD
cps.THX.3DCardScheme=Switch,Debit Mastercard,VISA,Maestro,VISA Delta,Mastercard,VISA Electron,American Express,TUI MASTERCARD
cps.ANCTHOMSON.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.ANCFIRSTCHOICE.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.ANCTHFO.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.ANCTHCRUISE.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA,TUI MASTERCARD
cps.ANCFALCON.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA
cps.ANCFJFO.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA
cps.HYBRISFALCONFO.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA
cps.FALCONFOMOBILE.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA
cps.FIRSTCHOICE.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.THOMSON.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.CRUISEMOBILE.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.TUITH.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD,MASTERCARD GIFT
cps.TUIFC.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.TUIFALCON.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA
cps.TUITHCRUISE.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.TUICRUISEDEALS.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.TUIHMCRUISE.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.TUITHSHOREX.3DCardScheme=Switch,Debit Mastercard,VISA,Maestro,VISA Delta,Mastercard,VISA Electron,TUI MASTERCARD
cps.TUITHRIVERCRUISE.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.TUITHFO.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.TUIFALCONFO.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA
cps.TUICS.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD,MASTERCARD GIFT
cps.TUIES.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA


# term url protocol - Local
cps.termURLProtocol=https
# term url protocol - SAT
#cps.termURLProtocol=https

#To increase cps session time out while moving to the bank page for 3D Security, set the proper value here.
cps.preAuthenticationSessionTimeOut.3DSecure=3600

#To decrease cps session time out after coming back from bank page with successful authentication in case of 3D Security, set this value.
cps.postAuthenticationSessionTimeOut.3DSecure=1800

# To check whether the ACS URL�s MD parameter validation is required in live environment or not.
# If set as true, the MD parameter will be validated and if set as false, the MD parameter will not be validated.
# By default, it will be validated always.
cps.3DSecure.MD.validation=true




cps.3dDuplicateTxError=168,51

cps.3DSecure.ACS.doublePost.handler.switch=true

cps.duplicate.thread.sleeptime=500






cps.ANCSKICS.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted
cps.ANCSKIES.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,dayTimePhone,emailAddress,tourOperatorTermsAccepted


cps.ANCSKICS.3DSecure=true
cps.ANCSKIES.3DSecure=true

cps.ANCSKICS.3DCardScheme=VISA,Mastercard,Debit Mastercard,VISA Delta,VISA Electron,American Express,TUI MASTERCARD
cps.ANCSKIES.3DCardScheme=Switch,American Express,Mastercard,VISA Delta,Laser,VISA




# Final line comment, to prevent the final data line (above) not being automatically substituted at deploy time.
# Any new entries should go above this final line comment and make sure that this final line doesn't contain any carriage return.