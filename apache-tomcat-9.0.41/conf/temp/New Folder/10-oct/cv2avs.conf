#================================================================================
# CV2AVS configuration details - Seperated from main configuration file cps.conf
# This file requires for cps12.00.00 and higher version releases.
#================================================================================

# NOTE - THIS FILE SHOULD CONTAIN ALL CONFIGURATION ITEMS IRRESPECTIVE OF
#        ENVIRONMENT SPECIFIC OR NOT. ONLY VALUES SHOULD CHANGE.


#================================================================================
#History:
# 06/05/10 01 Ramesh Babu   Initial file created as per Naresh mail - Wed 05/05/2010 12:20.
# 01/10/10 02 Ramesh Babu   Enabled CV2AVS for ThomsonAO & FCAO as per PAT & PRD.
# 07/10/10 02 Ramesh Babu   Enabled CV2AVS for Thomson BYO & FCSUN as per PAT & PRD.
# 07/10/10 03 Ramesh Babu   Disabled CV2AVS for Thomson BYO & HUGO and Enabled CV2AVS for Portland, GF brands.
# 25/10/10 04 Ramesh Babu   New items (ThomsonFHPI) added as per cps14.00.00 release
# 09/09/11 05 Ramesh Babu   New items (B2C Shorex) added as per cps18.00.00 release
#================================================================================

# To enable CV2AVS for client applications, set below entries.
# If entry is not there, then make sure that policies are also not set
cps.B2CShorex.CV2AVS.Enabled=false
cps.B2BShorex.CV2AVS.Enabled=false
cps.NEWSKIES.CV2AVS.Enabled=false
cps.NEWSKIESM.CV2AVS.Enabled=false
cps.FCX.CV2AVS.Enabled=false
cps.THX.CV2AVS.Enabled=false
cps.FIRSTCHOICE.CV2AVS.Enabled=false
cps.THOMSON.CV2AVS.Enabled=false
cps.MANAGEFIRSTCHOICE.CV2AVS.Enabled=false
cps.MANAGETHOMSON.CV2AVS.Enabled=false
cps.HYBRISTHCRUISE.CV2AVS.Enabled=false
cps.HYBRISFALCON.CV2AVS.Enabled=false
cps.HYBRISTHFO.CV2AVS.Enabled=false
cps.MFLIGHTONLY.CV2AVS.Enabled=false
cps.THOMSONMOBILE.CV2AVS.Enabled=false
cps.FIRSTCHOICEMOBILE.CV2AVS.Enabled=false
cps.FALCONMOBILE.CV2AVS.Enabled=false
cps.ANCTHOMSON.CV2AVS.Enabled=false
cps.ANCFIRSTCHOICE.CV2AVS.Enabled=false
cps.ANCTHFO.CV2AVS.Enabled=false
cps.ANCTHCRUISE.CV2AVS.Enabled=false
cps.ANCFALCON.CV2AVS.Enabled=false
cps.ANCFJFO.CV2AVS.Enabled=false
cps.HYBRISFALCONFO.CV2AVS.Enabled=false
cps.FALCONFOMOBILE.CV2AVS.Enabled=false
cps.CRUISEMOBILE.CV2AVS.Enabled=false
cps.TUITH.CV2AVS.Enabled=false
cps.TUIFC.CV2AVS.Enabled=false
cps.TUIFALCON.CV2AVS.Enabled=false

# Countries which require post code validations. It would be comma separated value.
cps.countries.CV2AVS.applicable=gbr

# To set the cv2avs policies, below entries should be modified.
# The order of extended policy is notprovided, notchecked, matched, notmatched, partialmatch
# To TURN ON any policy please use the string : reject,reject,accept,reject,reject
# To TURN OFF any policy please use the string : accept,accept,accept,accept,accept

# Newskies configs are Placed in KRONOS as per the ACSS Development
cps.NEWSKIES.AVS.policies=accept,accept,accept,accept,accept
cps.NEWSKIES.PCODE.policies=accept,accept,accept,accept,accept

# Newskiesm configs are Placed in KRONOS as per the ACSS Development
cps.NEWSKIESM.AVS.policies=accept,accept,accept,accept,accept
cps.NEWSKIESM.PCODE.policies=accept,accept,accept,accept,accept

#FCX
cps.FCX.AVS.policies=accept,accept,accept,accept,accept
cps.FCX.PCODE.policies=accept,accept,accept,accept,accept

#THX
cps.THX.AVS.policies=accept,accept,accept,accept,accept
cps.THX.PCODE.policies=accept,accept,accept,accept,accept

# First Choice Book flow configuration
cps.FIRSTCHOICE.AVS.policies=accept,accept,accept,accept,accept
cps.FIRSTCHOICE.PCODE.policies=accept,accept,accept,accept,accept

# Thomson Book flow configuration
cps.THOMSON.AVS.policies=accept,accept,accept,accept,accept
cps.THOMSON.PCODE.policies=accept,accept,accept,accept,accept


# TUITH Book flow  configuration
cps.TUITH.AVS.policies=accept,accept,accept,accept,accept
cps.TUITH.PCODE.policies=accept,accept,accept,accept,accept

# First Choice book flow configuration
cps.MANAGEFIRSTCHOICE.AVS.policies=accept,accept,accept,accept,accept
cps.MANAGEFIRSTCHOICE.PCODE.policies=accept,accept,accept,accept,accept


# Thomson Book flow configuration
cps.MANAGETHOMSON.AVS.policies=accept,accept,accept,accept,accept
cps.MANAGETHOMSON.PCODE.policies=accept,accept,accept,accept,accept


#--------------------------------------------------------------------------
# Hybris Thomson Cruise configuration
#--------------------------------------------------------------------------
cps.HYBRISTHCRUISE.AVS.policies=accept,accept,accept,accept,accept
cps.HYBRISTHCRUISE.PCODE.policies=accept,accept,accept,accept,accept

#--------------------------------------------------------------------------
# Hybris Falcon configuration
#--------------------------------------------------------------------------
cps.HYBRISFALCON.AVS.policies=accept,accept,accept,accept,accept
cps.HYBRISFALCON.PCODE.policies=accept,accept,accept,accept,accept

# Thomson Book flow configuration
cps.HYBRISTHFO.AVS.policies=accept,accept,accept,accept,accept
cps.HYBRISTHFO.PCODE.policies=accept,accept,accept,accept,accept

# Thomson Mobile Book flow  configuration
cps.MFLIGHTONLY.AVS.policies=accept,accept,accept,accept,accept
cps.MFLIGHTONLY.PCODE.policies=accept,accept,accept,accept,accept

# Thomson Mobile Book flow  configuration
cps.THOMSONMOBILE.AVS.policies=accept,accept,accept,accept,accept
cps.THOMSONMOBILE.PCODE.policies=accept,accept,accept,accept,accept

# Firstchoice Mobile Book flow  configuration
cps.FIRSTCHOICEMOBILE.AVS.policies=accept,accept,accept,accept,accept
cps.FIRSTCHOICEMOBILE.PCODE.policies=accept,accept,accept,accept,accept

# Falcon Mobile Book flow  configuration
cps.FALCONMOBILE.AVS.policies=accept,accept,accept,accept,accept
cps.FALCONMOBILE.PCODE.policies=accept,accept,accept,accept,accept

# Amend and cancel THOMSON  configuration
cps.ANCTHOMSON.AVS.policies=accept,accept,accept,accept,accept
cps.ANCTHOMSON.PCODE.policies=accept,accept,accept,accept,accept

# Amend and cancel FIRSTCHOICE  configuration
cps.ANCFIRSTCHOICE.AVS.policies=accept,accept,accept,accept,accept
cps.ANCFIRSTCHOICE.PCODE.policies=accept,accept,accept,accept,accept

# Amend and cancel THFO  configuration
cps.ANCTHFO.AVS.policies=accept,accept,accept,accept,accept
cps.ANCTHFO.PCODE.policies=accept,accept,accept,accept,accept

# Amend and cancel THCRUISE  configuration
cps.ANCTHCRUISE.AVS.policies=accept,accept,accept,accept,accept
cps.ANCTHCRUISE.PCODE.policies=accept,accept,accept,accept,accept

# Amend and cancel FALCON  configuration
cps.ANCFALCON.AVS.policies=accept,accept,accept,accept,accept
cps.ANCFALCON.PCODE.policies=accept,accept,accept,accept,accept
# Amend and cancel FJFO  configuration
cps.ANCFJFO.AVS.policies=accept,accept,accept,accept,accept
cps.ANCFJFO.PCODE.policies=accept,accept,accept,accept,accept
# CRUISE Mobile configuration
cps.CRUISEMOBILE.AVS.policies=accept,accept,accept,accept,accept
cps.CRUISEMOBILE.PCODE.policies=accept,accept,accept,accept,accept
#--------------------------------------------------------------------------
# Hybris Falcon FO configuration
#--------------------------------------------------------------------------
cps.HYBRISFALCONFO.AVS.policies=accept,accept,accept,accept,accept
cps.HYBRISFALCONFO.PCODE.policies=accept,accept,accept,accept,accept

#--------------------------------------------------------------------------
# Mobile Falcon FO configuration
#--------------------------------------------------------------------------
cps.FALCONFOMOBILE.AVS.policies=accept,accept,accept,accept,accept
cps.FALCONFOMOBILE.PCODE.policies=accept,accept,accept,accept,accept

#--------------------------------------------------------------------------
# New items (B2C Shorex) added as per cps18.00.00 release
#--------------------------------------------------------------------------
cps.B2CShorex.AVS.policies=accept,accept,accept,accept,accept
cps.B2CShorex.PCODE.policies=accept,accept,accept,accept,accept

cps.B2BShorex.AVS.policies=accept,accept,accept,accept,accept
cps.B2BShorex.PCODE.policies=accept,accept,accept,accept,accept


# TUIFC Book flow  configuration
cps.TUIFC.AVS.policies=accept,accept,accept,accept,accept
cps.TUIFC.PCODE.policies=accept,accept,accept,accept,accept


#TUIFALCON Book flow  configuration
cps.TUIFALCON.AVS.policies=accept,accept,accept,accept,accept
cps.TUIFALCON.PCODE.policies=accept,accept,accept,accept,accept



# Final line comment, to prevent the final data line (above) not being automatically substituted at deploy time.
# Any new entries should go above this final line comment and make sure that this final line doesn't contain any carriage return.